<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'mailer'             => array(
        'smtp_options' => array(
            'host' => 'mailserver',
            'port' => 25
        )
    ),
    'assetic_configuration' => array (
        // Use on production environment
        'debug'          => false,
        'buildOnRequest' => false,
        'combine'        => true,
        'webPath'        => realpath('public'),
    )
);
