
-- Suppression du contenu des tables 
-- -----------------------------------------------------
DELETE FROM groupe ; 
DELETE FROM perimetre_geo ; 
DELETE FROM dispositif ; 

-- Import de la table dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif (id, intitule, intitule_conseiller, financeur, type_perimetre, exclut_CEE, code_type_dispositif, descriptif,  debut_validite, fin_validite, evaluer_regle, code_etat ) 
 VALUES ( 1, 'Habiter Mieux (ANAH)', 'Habiter Mieux (ANAH)', 'ANAH', 'NAT', 1, 'PUBLIC', 'Le projet  doit améliorer les performances énergétiques du logement ou du bâtiment d''au moins 25% pour un propriétaire occupant et 35% pour un propriétaire bailleur..
Une évaluation énergétique du logement sera faite lors d''une visite technique pour vérifier le potientiel d''économie d''énergie.
Les travaux ne doivent pas être commencés avant le dépôt de la demande de subvention.
Montant minimum de travaux de 1 500 € HT sauf pour les propriétaires occupants aux ressources très modestes, aucun seuil n''est exigé.
Votre logement a plus de 15 ans à la date où est acceptée votre demande d’aide ;
Vous n''avez pas bénéficié d''un PTZ (Prêt à taux zéro pour l''accession à la propriété) depuis 5 ans.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 2, 'Aide de Solidarité Ecologique', 'Prime ASE', 'Etat', 'NAT', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.
Elle ne peut pas être accordée indépendamment d''une aide de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 3, 'Chèque Energies - Région', 'Chèque Energies - Région', 'Région Haute-Normandie', 'REG', 1, 'PUBLIC', 'Obligation de réaliser un audit énergétique au préalable par un bureau d''études conventionné avec la Région Haute Normandie. 
 Les travaux doivent être conformes aux préconisations de l’audit.
Les travaux doivent être effectués par des professionnels qualifiés.
 La fourniture  et la pose doivent être réalisés par une même entreprise.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 4, 'Aide du Département de l''Eure', 'CG 27', 'Département de l''Eure', 'DEP', 1, 'PUBLIC', 'Les travaux doivent être réalisés par une entreprise ou un artisan qualifié. 
Le projet  doit améliorer les performances énergétiques du logement ou du bâtiment d''au moins 25% 
 Les travaux ne doivent pas commencer avant accord écrit du Département', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 5, 'Aide à l''habitat Durable du Département de Seine-Maritime', 'Habitat Durable - CG76', 'Département de Seine-Maritime', 'DEP', 1, 'PUBLIC', 'Les travaux doivent être réalisés par une entreprise ou un artisan qualifié. 
 Les travaux ne doivent pas commencer avant accord écrit du Département
 L''aide à l''habitat durable peut être accordée tous les 10 ans.
 Dans le cadre d''une multipropriété et quand les travaux son adopté par la dernère assemblee plénière, chaque propriétaire peut solliciter l''aide à l''habitat durable proportionnellement à son logement.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 6, 'Programme d''intérêt général du Département de Seine-Maritime', 'PIG CG76', 'Département de Seine-Maritime', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 7, 'Programme d''Intérêt Général de la Métropole Rouen Normandie', 'PIG CREA', 'Metropole Rouen Normandie', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.
 Certains secteurs du territoire de la Métropole (Elbeuf sur Seine, Quartier Pasteur de la ville de Rouen) sont également concernés par une OPAH. L''aide accordée est alors portée par la commune. Les conditions et les montants de l''aide sont inchangés.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 8, 'Programme d''Intérêt Général de la Communauté de l''Agglomération Havraise', 'PIG CODAH', 'Communauté de l''agglomération havraise', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.
 Certains secteurs du territoire de la CODAH (centre ancien et quartier de l''Eure de la ville du Havre) sont également concernés par une OPAH. L''aide accordée est alors portée par la commune.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 9, 'Programme d''Intérêt Général de l''Agglomération Dieppe-Maritime', 'PIG Dieppe-Maritime', 'Communauté d''agglomération de la Région Dieppoise', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.
 Une partie de la ville de Dieppe est concernée par une OPAH-RU. L''aide accordée est alors portée par la commune.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 10, 'Programme d''Intérêt Général de la Communauté d''Agglomération Seine-Eure', 'PIG CASE', 'Communauté d''agglomération Seine-Eure', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 11, 'Programme d''Intérêt Général du Grand Evreux Agglomération', 'PIG GEA', 'Grand Evreux Agglomération', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 12, 'Opération Programmée d''Amélioration de l''Habitat de la Communauté d''Agglomération des Portes de l''Eure', 'OPAH CAPE', 'Communauté d''agglomération des Portes de l''Eure', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 13, 'Soutien aux audits énergétiques de la Communauté d''Agglomération des Portes de l''Eure', 'CAPE - audit', 'Communauté d''agglomération des Portes de l''Eure', 'EPCI', 1, 'PUBLIC', '• Les conditions d''attribution sont identiques à celles du programme Chèques Energies de la Région Haute-Normandie.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 14, 'Coup de pouce - Dieppe Maritime', 'Coup de pouce - Dieppe Maritime', 'Communauté d''agglomération de la Région Dieppoise', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Chèques Energies de la Région Haute-Normandie.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 15, 'Communauté de Communes Caux estuaire', 'CC Caux estuaire', 'Communauté de communes Caux Estuaire', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles de l''aide à l''Habitat Durable du Département de Seine-Maritime', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 16, 'Communauté de Communes Caux Vallée de Seine', 'CC Caux Vallée de Seine', 'Communauté de communes Caux Vallée de Seine', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 17, 'Communauté de Communes du Pays Neufchatelois', 'CC du Pays Neufchatelois', 'Communauté de communes du Pays Neufchatelois', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 18, 'Communauté de Communes Côte d''Albatre', 'CC Côte d''Albatre', 'Communauté de communes Côte d''Albatre', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 19, 'Communauté de Communes du canton de Lyons la Forêt', 'CC du canton de Lyons la Forêt', 'Communauté de communes du canton de Lyons la Forêt', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 20, 'Communauté de Communes du canton du Neubourg', 'CC du canton du Neubourg', 'Communauté de communes du canton du Neubourg', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 21, 'Communauté de Communes du roumois Nord', 'CC du roumois Nord', 'Communauté de communes du roumois Nord', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 22, 'Communauté de Communes du Canton de Rugles', 'CC canton de Rugles', 'Communauté de communes canton de Rugles', 'EPCI', 1, 'PUBLIC', 'Les conditions d''attribution sont identiques à celles du programme Habiter Mieux de l''Anah.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 23, 'Communauté de Communes Eure Madrie Seine', 'CCEMS', 'Communauté de communes Eure-Madrie-Seine', 'EPCI', 1, 'PUBLIC', '', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 24, 'Pont de l''Arche', 'Pont de l''Arche', 'Commune de Pont de l''Arche', 'COM', 1, 'PUBLIC', 'Exonération possible de 50% de la taxe foncière sur les propriétés bâties. 
Les critères d''éligibilité techniques sont identiques à ceux du Crédit d''Impôt Transition Energétique.
Renseignez-vous auprès de votre Mairie', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 25, 'Caudebec en Caux', 'Caudebec en Caux', 'Commune de Caudebec en Caux', 'COM', 1, 'PUBLIC', 'Les critères d''éligibilité techniques sont identiques à ceux du Crédit d''Impôt Transition Energétique.
Renseignez-vous auprès de votre Mairie', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 26, 'Lillebonne', 'Lillebonne', 'Commune de Lillebonne', 'COM', 0, 'PUBLIC', 'Les critères d''éligibilité techniques sont identiques à ceux du Crédit d''Impôt Transition Energétique.
Renseignez-vous auprès de votre Mairie', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 27, 'Bolbec', 'Bolbec', 'Commune de Bolbec', 'COM', 0, 'PUBLIC', 'Exonération possible de 50% de la taxe foncière sur les propriétés bâties. 
Les critères d''éligibilité techniques sont identiques à ceux du Crédit d''Impôt Transition Energétique.
Renseignez-vous auprès de votre Mairie', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 28, 'Mont saint Aignan', 'Mont saint Aignan', 'Commune de Mont saint Aignan', 'COM', 0, 'PUBLIC', 'Exonération possible de 50% de la taxe foncière sur les propriétés bâties. 
Les critères d''éligibilité techniques sont identiques à ceux du Crédit d''Impôt Transition Energétique.
Renseignez-vous auprès de votre Mairie', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 29, 'Grand Couronne', 'Grand Couronne', 'Commune de Grand Couronne', 'COM', 0, 'PUBLIC', 'Renseignez-vous auprès de votre Mairie', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 30, 'Grand Quevilly', 'Grand Quevilly', 'Commune de Grand Quevilly', 'COM', 0, 'PUBLIC', 'Renseignez-vous auprès de votre Mairie', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 31, 'Certificats d’Economie d’Energie', 'Certificats d’Economie d’Energie', 'Aides privées', 'NAT', 0, 'CEE', 'Cette aide / prime peut être attribuée par une quarantaine de grandes entreprises distributrices d’électricité, gaz, chaleur et froid (EDF, GDF, CPCU…), plus de 2000 distributeurs de fioul domestique et une quarantaine de metteurs à la consommation de carburants automobiles (compagnies pétrolières, et entreprises de la grande distribution telles que TOTAL, BP, SIPLEC…) 

Le logement doit être achevé depuis plus de 2 ans.
Les matériaux et les équipements achetés doivent être fournis par l''entreprise qui effectue leur installation.
Ils doivent répondre à des exigences techniques précises.
Pour bénéficier de ce dispositif, l''entreprise qui met en oeuvre les travaux doit être titulaire de la mention RGE.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ,
 ( 32, 'CITE (crédit d''impôt)', 'CITE (crédit d''impôt)', 'Etat', 'NAT', 0, 'PUBLIC', 'Le logement doit être achevé depuis plus de 2 ans
Les matériaux et les équipements achetés doivent être fournis par l''entreprise qui effectue leur installation.
Ils doivent répondre à des exigences techniques précises.
Le crédit d''impôt est calculé sur le montant TTC des dépenses éligibles, déduction faite des aides et subventions perçuess par ailleurs.
Le montant des dépenses est calculé sur une période de 5 années consécutives comprises entre le 1er janvier 2005 et 31 décembre 2015.
Vous pouvez également bénéificer d''un éco-prêt à taux zéro si le montant des revenus de l''année N-2 du foyer fiscal n''excède pas 25 000 € pour une personne, 35 000 € pour un couple soumis à imposition commune et 7 500 € supplémentaire par persone en charge.
Pour bénéficier de ce dispositif, l''entreprise qui met en oeuvre les travaux doit être titulaire de la mention RGE.', '2015-01-01', '2016-12-31', 1, 'ACTIVE' )  ; 
 
-- Import de la table perimetre_geo 
-- -----------------------------------------------------
INSERT INTO perimetre_geo (id_dispositif, code_region, code_departement, code_commune, code_epci) 
 VALUES ( 3, '23', null, null, null) ,
 ( 4, null, '27', null, null) ,
 ( 5, null, '76', null, null) ,
 ( 6, null, null, null, '247600588') ,
 ( 6, null, null, null, '247600646') ,
 ( 6, null, null, null, '247600562') ,
 ( 6, null, null, null, '247600778') ,
 ( 6, null, null, null, '247600216') ,
 ( 6, null, null, null, '247600505') ,
 ( 6, null, null, null, '247600331') ,
 ( 6, null, null, null, '247600620') ,
 ( 6, null, null, null, '247600745') ,
 ( 6, null, null, null, '247600539') ,
 ( 6, null, null, null, '247600703') ,
 ( 6, null, null, null, '247600802') ,
 ( 6, null, null, null, '247600794') ,
 ( 6, null, null, null, '247600729') ,
 ( 6, null, null, null, '247600760') ,
 ( 6, null, null, null, '247600679') ,
 ( 6, null, null, null, '247600711') ,
 ( 6, null, null, null, '247600687') ,
 ( 6, null, null, null, '247600497') ,
 ( 6, null, null, null, '247600661') ,
 ( 6, null, null, null, '247600695') ,
 ( 6, null, null, null, '247600604') ,
 ( 6, null, null, null, '247600570') ,
 ( 6, null, null, null, '247600455') ,
 ( 6, null, null, null, '247600398') ,
 ( 6, null, null, null, '247600463') ,
 ( 6, null, null, null, '247600653') ,
 ( 6, null, null, null, '247600737') ,
 ( 6, null, null, null, '247600638') ,
 ( 6, null, null, null, '247600448') ,
 ( 7, null, null, null, '200023414') ,
 ( 8, null, null, null, '247600596') ,
 ( 9, null, null, null, '247600786') ,
 ( 10, null, null, null, '200035665') ,
 ( 11, null, null, null, '242700573') ,
 ( 12, null, null, null, '242700649') ,
 ( 13, null, null, null, '242700649') ,
 ( 14, null, null, null, '247600786') ,
 ( 15, null, null, null, '247600539') ,
 ( 16, null, null, null, '200010700') ,
 ( 17, null, null, null, '247600513') ,
 ( 18, null, null, null, '247600380') ,
 ( 19, null, null, null, '242700417') ,
 ( 20, null, null, null, '242700607') ,
 ( 21, null, null, null, '242700292') ,
 ( 22, null, null, null, '242700375') ,
 ( 23, null, null, null, '242700623') ,
 ( 24, null, null, '27469', null) ,
 ( 25, null, null, '76164', null) ,
 ( 26, null, null, '76384', null) ,
 ( 27, null, null, '76114', null) ,
 ( 28, null, null, '76451', null) ,
 ( 29, null, null, '76319', null) ,
 ( 30, null, null, '76322', null) ; 
 
-- Import de la table groupe 
-- -----------------------------------------------------
INSERT INTO groupe (id_dispositif, code_region, numero_ordre ) 
 VALUES ( 1, '42', 1)  ,
 ( 1, '72', 1)  ,
 ( 1, '83', 1)  ,
 ( 1, '25', 1)  ,
 ( 1, '26', 1)  ,
 ( 1, '53', 1)  ,
 ( 1, '24', 1)  ,
 ( 1, '21', 1)  ,
 ( 1, '94', 1)  ,
 ( 1, '43', 1)  ,
 ( 1, '23', 1)  ,
 ( 1, '11', 1)  ,
 ( 1, '91', 1)  ,
 ( 1, '74', 1)  ,
 ( 1, '41', 1)  ,
 ( 1, '73', 1)  ,
 ( 1, '31', 1)  ,
 ( 1, '52', 1)  ,
 ( 1, '22', 1)  ,
 ( 1, '54', 1)  ,
 ( 1, '93', 1)  ,
 ( 1, '82', 1)  ,
 ( 2, '42', 2)  ,
 ( 2, '72', 2)  ,
 ( 2, '83', 2)  ,
 ( 2, '25', 2)  ,
 ( 2, '26', 2)  ,
 ( 2, '53', 2)  ,
 ( 2, '24', 2)  ,
 ( 2, '21', 2)  ,
 ( 2, '94', 2)  ,
 ( 2, '43', 2)  ,
 ( 2, '23', 2)  ,
 ( 2, '11', 2)  ,
 ( 2, '91', 2)  ,
 ( 2, '74', 2)  ,
 ( 2, '41', 2)  ,
 ( 2, '73', 2)  ,
 ( 2, '31', 2)  ,
 ( 2, '52', 2)  ,
 ( 2, '22', 2)  ,
 ( 2, '54', 2)  ,
 ( 2, '93', 2)  ,
 ( 2, '82', 2)  ,
 ( 3, '23', 3)  ,
 ( 4, '23', 4)  ,
 ( 5, '23', 5)  ,
 ( 6, '23', 6)  ,
 ( 7, '23', 7)  ,
 ( 8, '23', 8)  ,
 ( 9, '23', 9)  ,
 ( 10, '23', 10)  ,
 ( 11, '23', 11)  ,
 ( 12, '23', 12)  ,
 ( 13, '23', 13)  ,
 ( 14, '23', 14)  ,
 ( 15, '23', 15)  ,
 ( 16, '23', 16)  ,
 ( 17, '23', 17)  ,
 ( 18, '23', 18)  ,
 ( 19, '23', 19)  ,
 ( 20, '23', 20)  ,
 ( 21, '23', 21)  ,
 ( 22, '23', 22)  ,
 ( 23, '23', 23)  ,
 ( 24, '23', 24)  ,
 ( 25, '23', 25)  ,
 ( 26, '23', 26)  ,
 ( 27, '23', 27)  ,
 ( 28, '23', 28)  ,
 ( 29, '23', 29)  ,
 ( 30, '23', 30)  ,
 ( 31, '42', 3)  ,
 ( 31, '72', 3)  ,
 ( 31, '83', 3)  ,
 ( 31, '25', 3)  ,
 ( 31, '26', 3)  ,
 ( 31, '53', 3)  ,
 ( 31, '24', 3)  ,
 ( 31, '21', 3)  ,
 ( 31, '94', 3)  ,
 ( 31, '43', 3)  ,
 ( 31, '23', 31)  ,
 ( 31, '11', 3)  ,
 ( 31, '91', 3)  ,
 ( 31, '74', 3)  ,
 ( 31, '41', 3)  ,
 ( 31, '73', 3)  ,
 ( 31, '31', 3)  ,
 ( 31, '52', 3)  ,
 ( 31, '22', 3)  ,
 ( 31, '54', 3)  ,
 ( 31, '93', 3)  ,
 ( 31, '82', 3)  ,
 ( 32, '42', 4)  ,
 ( 32, '72', 4)  ,
 ( 32, '83', 4)  ,
 ( 32, '25', 4)  ,
 ( 32, '26', 4)  ,
 ( 32, '53', 4)  ,
 ( 32, '24', 4)  ,
 ( 32, '21', 4)  ,
 ( 32, '94', 4)  ,
 ( 32, '43', 4)  ,
 ( 32, '23', 32)  ,
 ( 32, '11', 4)  ,
 ( 32, '91', 4)  ,
 ( 32, '74', 4)  ,
 ( 32, '41', 4)  ,
 ( 32, '73', 4)  ,
 ( 32, '31', 4)  ,
 ( 32, '52', 4)  ,
 ( 32, '22', 4)  ,
 ( 32, '54', 4)  ,
 ( 32, '93', 4)  ,
 ( 32, '82', 4)  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_01_ANAH_habiter_mieux.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 1, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(1, 'E', null, '$L.age >= 15') , 
(1, 'E', null, '$F.ptz === false') , 
(1, 'M', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" )', '( $D.cout_ht_to * 35 ) / 100') , 
(1, 'M', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "TRES_MODESTE" )', '( $D.cout_ht_to * 50 ) / 100') , 
(1, 'M', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '( $D.cout_ht_to * 25 ) / 100') , 
(1, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" )', '( 20000 * 35 ) / 100') , 
(1, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "TRES_MODESTE" )', '( 20000 * 50 ) / 100') , 
(1, 'P', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '( 750 * $L.surface * 25 ) / 100') , 
(1, 'P', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '15000')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 1, 'T01', 'Ouvrants à menuiserie coulissante: Uw ≤ 2,6 W/m2 K 
Autres types d''ouvrants: Uw ≤2,3 W/m2 K') , 
(1, 'T02', 'Résistance thermique ≥3,7 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(1, 'T03', 'Résistance thermique ≥3,7 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(1, 'T04', 'Résistance thermique ≥7 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(1, 'T05', 'Résistance thermique ≥6 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(1, 'T06', 'Résistance thermique ≥6 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(1, 'T08', 'Résistance thermique ≥3 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(1, 'T10', 'Coefficient de transmission thermique, Ud ≤ 1,7 W/m2 K') , 
(1, 'T11', 'Le rendement minimum doit être conforme aux exigences de la réglementation thermique des bâtiments existants (arrêté du 3 mai 2007)') , 
(1, 'T12', 'Le rendement minimum doit être conforme aux exigences de la réglementation thermique des bâtiments existants (arrêté du 3 mai 2007)') , 
(1, 'T13', 'A saisir') , 
(1, 'T14', 'La consommation de l''installation de ventilation devra être inférieure à 0,25 Wh/m³/ventilateur (cette valeur peut être portée à 0,4 en présence de filtres F5 à F9)') , 
(1, 'T15', 'La consommation de l''installation de ventilation devra être inférieure à 0,25 Wh/m³/ventilateur (cette valeur peut être portée à 0,4 en présence de filtres F5 à F9)') , 
(1, 'T20', 'Le rendement de l''appareil installé doit être supérieur à 65%.') , 
(1, 'T21', 'Le rendement minimum doit être conforme aux exigences de la réglementation thermique des bâtiments existants (arrêté du 3 mai 2007)') , 
(1, 'T22', 'Le Coefficient de performance (COP) de la Pompe à chaleur doit être supérieur à 3,2') , 
(1, 'T23', 'Le Coefficient de performance (COP) de la Pompe à chaleur doit être supérieur à 3,2') , 
(1, 'T24', 'Le Coefficient de performance (COP) de la Pompe à chaleur doit être supérieur à 3,2') , 
(1, 'T25', null) , 
(1, 'T26', null) , 
(1, 'T27', null) , 
(1, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_02_prime_ASE.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 2, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(2, 'E', null, '$L.age >= 15') , 
(2, 'E', null, '$F.ptz === false') , 
(2, 'M', null, '$D.cout_ht_to') , 
(2, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" )', '1600') , 
(2, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "TRES_MODESTE" )', '2000') , 
(2, 'P', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '1600')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 2, 'T01', null) , 
(2, 'T02', null) , 
(2, 'T03', null) , 
(2, 'T04', null) , 
(2, 'T05', null) , 
(2, 'T06', null) , 
(2, 'T08', null) , 
(2, 'T10', null) , 
(2, 'T11', null) , 
(2, 'T12', null) , 
(2, 'T13', null) , 
(2, 'T14', null) , 
(2, 'T20', null) , 
(2, 'T21', null) , 
(2, 'T22', null) , 
(2, 'T23', null) , 
(2, 'T24', null) , 
(2, 'T25', null) , 
(2, 'T26', null) , 
(2, 'T27', null) , 
(2, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_03_cheque_energie.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 3, 'E', null, '$F.revenu <= 45000 && $F.statut == "PROP_RES_1" && $L.age > 10 && $L.type == "M"')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 3, 'T01', 'Fenêtres PVC : coefficient de transmission thermique, Uw ≤ 1,4 W/m2 K 
Fenêtres bois : coefficient de transmission thermique, Uw ≤ 1,6 W/m2 K 
Fenêtres métalliques : coefficient de transmission thermique, Uw ≤ 1,8 W/m2 K
Doubles fenêtres : coefficient de transmission thermique, Ug ≤ 1,8 W/m2 K 
Fenêtres de toit: coefficient de transmission thermique, Uw ≤ 1,5 W/m2 K
Vitrages à isolation renforcée : coefficient de transmission thermique, Ug ≤1,1 W/m2K') , 
(3, 'T02', 'La Résistance thermique, R ≥ 3,7 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(3, 'T03', 'La Résistance thermique, R ≥ 3,7 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(3, 'T04', 'La Résistance thermique, R ≥ 7 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(3, 'T05', 'La Résistance thermique, R ≥ 6 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(3, 'T06', 'La Résistance thermique, R ≥ 6 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(3, 'T08', 'La Résistance thermique, R ≥ 3 m².k/W et le matériau doit posséder un n° Acermi ou équivalent') , 
(3, 'T10', 'Le coefficient de transmission thermique, Ud ≤ 1,7 W/m2 K') , 
(3, 'T11', 'Il doit s''agir du remplacement d''un système de chauffage de plus de 15 ans. 
Les travaux doivent inclure la pose d''un programmateur et calorifugeage des réseaux (R ≥ 1,2 m2 K/W)') , 
(3, 'T12', 'Il doit s''agir du remplacement d''un système de chauffage de plus de 15 ans. 
Les travaux doivent inclure la pose d''un programmateur et calorifugeage des réseaux (R ≥ 1,2 m2 K/W)') , 
(3, 'T14', null) , 
(3, 'T20', 'Votre logement doit afficher dans les résultats de l''audit, une consommation d''énergie primaire ≤  150 kW/h par m2 par an.
L’appareil doit avoir un rendement énergétique ≥  85%.') , 
(3, 'T21', 'Votre logement doit afficher dans les résultats de l''audit, une consommation d''énergie primaire inférieure ou égale à 150 kW/h par m2 par an.

• Équipements à chargement manuel :
- rendement énergétique supérieur ou égal à 80% ;
• Équipements à chargement automatique :
- rendement énergétique supérieur ou égal à 85% ;
• L''installation de chaudière doit s’accompagner de la pose d’un programmateur
de chauffage et du calorifugeage des réseaux ( R≥ 1,2 m2 K/W).') , 
(3, 'T27', 'L''installation solaire doit être certifiée CSTBat ou Solar Keymark ou équivalent.
Le calorifugeage des réseaux doit avoir un coefficient R≥ 1,2 m2 K/W.') , 
(3, 'T29', 'L''audit énergétique doit être réalisé par un Bureau d''Etudes conventionné avec la Région.')  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T01'), 'MT', null, '( $T.nb_fenetres * 1.62 + $T.nb_portes_fenetres * 2.58 ) * 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T01'), 'P', null, '2000') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T02'), 'MT', null, '$T.tech[SURFACE_ISOLANT_MURS_INT] * 12') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T02'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T03'), 'MT', null, '$T.tech[SURFACE_ISOLANT_MURS_EXT] * 50') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T03'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T04'), 'MT', null, '$T.tech[SURFACE_ISOLANT_COMBLES] * 10') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T04'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T05'), 'MT', null, '$T.tech[SURFACE_ISOLANT_TOIT_INT] * 32') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T05'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T06'), 'MT', null, '$T.tech[SURFACE_ISOLANT_TOIT_EXT] * 45') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T06'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T08'), 'MT', null, '$T.tech[SURFACE_ISOLANT_PLANCHER_BAS] * 10') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T08'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T10'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T10'), 'P', '( $T.montant_to[T10] + $T.montant_to[T01] ) >= 2000', '2000 - $T.montant_to[T01]') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T10'), 'P', '( $T.montant_to[T10] + $T.montant_to[T01] ) < 2000', '200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T11'), 'MO', null, '$T.cout_ttc_mo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T11'), 'P', null, '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T12'), 'MO', null, '$T.cout_ttc_mo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T12'), 'P', null, '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T14'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T14'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T20'), 'MO', null, '$T.cout_ttc_mo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T20'), 'P', null, '380') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T21'), 'MO', null, '$T.cout_ttc_mo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T21'), 'P', null, '2100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T27'), 'MO', null, '$T.cout_ttc_mo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T27'), 'P', null, '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T29'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 3 AND code_travaux = 'T29'), 'P', null, '400')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_04_CG27.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 4, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(4, 'E', null, '$L.age > 15') , 
(4, 'E', '$F.tranche[CG27] == "HORS_TRANCHE"', 'false') , 
(4, 'M', '$F.tranche[CG27] == "SOCIAUX"', '$D.cout_ttc_to') , 
(4, 'M', '$F.tranche[CG27] == "TRES_SOCIAUX"', '( $D.cout_ttc_to * 30 / 100 ) + 500') , 
(4, 'M', '$D.montant_to < 200', '0') , 
(4, 'P', '$F.tranche[CG27] == "SOCIAUX"', '500') , 
(4, 'P', '$F.tranche[CG27] == "TRES_SOCIAUX"', '2900')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 4, 'T01', null) , 
(4, 'T02', null) , 
(4, 'T03', null) , 
(4, 'T04', null) , 
(4, 'T05', null) , 
(4, 'T06', null) , 
(4, 'T08', null) , 
(4, 'T10', null) , 
(4, 'T11', null) , 
(4, 'T12', null) , 
(4, 'T13', null) , 
(4, 'T14', null) , 
(4, 'T20', null) , 
(4, 'T21', null) , 
(4, 'T22', null) , 
(4, 'T23', null) , 
(4, 'T24', null) , 
(4, 'T25', null) , 
(4, 'T26', null) , 
(4, 'T27', null) , 
(4, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_05_CG76.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 5, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(5, 'E', '$F.tranche[CG76] == "HORS_TRANCHE"', 'false') , 
(5, 'P', '$D.montant_to < 200', '0') , 
(5, 'P', '$F.tranche[CG76] == "CG1"', '6000 * 20 / 100') , 
(5, 'P', '$F.tranche[CG76] == "CG2"', '6000 * 25 / 100') , 
(5, 'P', '$F.tranche[CG76] == "CG3"', '6000 * 30 / 100')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 5, 'T01', null) , 
(5, 'T02', null) , 
(5, 'T03', null) , 
(5, 'T04', null) , 
(5, 'T05', null) , 
(5, 'T06', null) , 
(5, 'T07', null) , 
(5, 'T08', null) , 
(5, 'T09', null) , 
(5, 'T10', null) , 
(5, 'T11', null) , 
(5, 'T12', null) , 
(5, 'T14', null) , 
(5, 'T15', null) , 
(5, 'T20', null) , 
(5, 'T21', null) , 
(5, 'T26', null) , 
(5, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T01'), 'MF', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ht_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T01'), 'MF', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ht_fo * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T01'), 'MF', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ht_fo * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T01'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '1500 * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T01'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '1500 * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T01'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '1500 * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T02'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T02'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T02'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T02'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T02'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T02'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T03'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T03'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T03'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T03'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T03'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T03'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T05'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T05'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T05'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T05'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T05'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T05'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T06'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T06'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T06'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T06'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T06'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T06'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T07'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T07'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T07'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T07'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T07'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T07'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T08'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T08'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T08'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T08'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T08'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T08'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T09'), 'MF', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ht_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T09'), 'MF', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ht_fo * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T09'), 'MF', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ht_fo * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T09'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '1500 * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T09'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '1500 * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T09'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '1500 * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T10'), 'MF', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ht_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T10'), 'MF', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ht_fo * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T10'), 'MF', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ht_fo * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T10'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '800 * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T10'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '800 * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T10'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '800 * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T11'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T11'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T11'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T11'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T11'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T11'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T12'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T12'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T12'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T12'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T12'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T12'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T14'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T14'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T14'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T14'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T14'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T14'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T15'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T15'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T15'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T15'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T15'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T15'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T20'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T20'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T20'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T20'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '3000 * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T20'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '3000 * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T20'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '3000 * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T21'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T21'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T21'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T21'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T21'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T21'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T26'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T26'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T26'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T26'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T26'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T26'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '1800') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T27'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T27'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T27'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T27'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '1200') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T27'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 5 AND code_travaux = 'T27'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '1800')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_06_PIG_CG76.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 6, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(6, 'E', null, '$L.age >= 15') , 
(6, 'E', null, '$F.ptz === false') , 
(6, 'M', '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"', '$D.cout_ht_to') , 
(6, 'P', '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"', '500')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 6, 'T01', null) , 
(6, 'T02', null) , 
(6, 'T03', null) , 
(6, 'T04', null) , 
(6, 'T05', null) , 
(6, 'T06', null) , 
(6, 'T08', null) , 
(6, 'T10', null) , 
(6, 'T11', null) , 
(6, 'T12', null) , 
(6, 'T13', null) , 
(6, 'T14', null) , 
(6, 'T20', null) , 
(6, 'T21', null) , 
(6, 'T22', null) , 
(6, 'T23', null) , 
(6, 'T24', null) , 
(6, 'T25', null) , 
(6, 'T26', null) , 
(6, 'T27', null) , 
(6, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_07_PIG_CREA.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 7, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(7, 'E', null, '$L.age >= 15') , 
(7, 'E', null, '$F.ptz === false') , 
(7, 'M', '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"', '$D.cout_ht_to') , 
(7, 'P', '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"', '500')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 7, 'T01', null) , 
(7, 'T02', null) , 
(7, 'T03', null) , 
(7, 'T04', null) , 
(7, 'T05', null) , 
(7, 'T06', null) , 
(7, 'T08', null) , 
(7, 'T10', null) , 
(7, 'T11', null) , 
(7, 'T12', null) , 
(7, 'T13', null) , 
(7, 'T14', null) , 
(7, 'T20', null) , 
(7, 'T21', null) , 
(7, 'T22', null) , 
(7, 'T23', null) , 
(7, 'T24', null) , 
(7, 'T25', null) , 
(7, 'T26', null) , 
(7, 'T27', null) , 
(7, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_08_PIG_CODAH.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 8, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(8, 'E', null, '$L.age >= 15') , 
(8, 'E', null, '$F.ptz === false') , 
(8, 'M', '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"', '$D.cout_ht_to') , 
(8, 'P', '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"', '500')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 8, 'T01', null) , 
(8, 'T02', null) , 
(8, 'T03', null) , 
(8, 'T04', null) , 
(8, 'T05', null) , 
(8, 'T06', null) , 
(8, 'T08', null) , 
(8, 'T10', null) , 
(8, 'T11', null) , 
(8, 'T12', null) , 
(8, 'T13', null) , 
(8, 'T14', null) , 
(8, 'T20', null) , 
(8, 'T21', null) , 
(8, 'T22', null) , 
(8, 'T23', null) , 
(8, 'T24', null) , 
(8, 'T25', null) , 
(8, 'T26', null) , 
(8, 'T27', null) , 
(8, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_09_PIG_Dieppe_Maritime.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 9, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(9, 'E', null, '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"') , 
(9, 'E', null, '$L.age >= 15') , 
(9, 'M', '$F.tranche[ANAH] == "MODESTE"', '( $D.cout_ht_to * 20 / 100 ) + 500') , 
(9, 'M', '$F.tranche[ANAH] == "TRES_MODESTE"', '( $D.cout_ht_to * 25 / 100 ) + 500') , 
(9, 'P', '$F.tranche[ANAH] == "MODESTE"', '1700') , 
(9, 'P', '$F.tranche[ANAH] == "TRES_MODESTE"', '2000')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 9, 'T01', null) , 
(9, 'T02', null) , 
(9, 'T03', null) , 
(9, 'T04', null) , 
(9, 'T05', null) , 
(9, 'T06', null) , 
(9, 'T08', null) , 
(9, 'T10', null) , 
(9, 'T11', null) , 
(9, 'T12', null) , 
(9, 'T13', null) , 
(9, 'T14', null) , 
(9, 'T20', null) , 
(9, 'T21', null) , 
(9, 'T22', null) , 
(9, 'T23', null) , 
(9, 'T24', null) , 
(9, 'T25', null) , 
(9, 'T26', null) , 
(9, 'T27', null) , 
(9, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_10_PIG_CASE.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 10, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(10, 'E', null, '$L.age >= 15') , 
(10, 'E', null, '$F.ptz === false') , 
(10, 'M', '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"', '$D.cout_ht_to') , 
(10, 'P', '$F.tranche[ANAH] == "MODESTE"', '500') , 
(10, 'P', '$F.tranche[ANAH] == "TRES_MODESTE"', '800')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 10, 'T01', null) , 
(10, 'T02', null) , 
(10, 'T03', null) , 
(10, 'T04', null) , 
(10, 'T05', null) , 
(10, 'T06', null) , 
(10, 'T08', null) , 
(10, 'T10', null) , 
(10, 'T11', null) , 
(10, 'T12', null) , 
(10, 'T13', null) , 
(10, 'T14', null) , 
(10, 'T20', null) , 
(10, 'T21', null) , 
(10, 'T22', null) , 
(10, 'T23', null) , 
(10, 'T24', null) , 
(10, 'T25', null) , 
(10, 'T26', null) , 
(10, 'T27', null) , 
(10, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_11_PIG_GEA.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 11, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(11, 'E', null, '$L.age >= 15') , 
(11, 'E', null, '$F.ptz === false') , 
(11, 'M', '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"', '$D.cout_ht_to') , 
(11, 'P', '$F.tranche[ANAH] == "MODESTE"', '500') , 
(11, 'P', '$F.tranche[ANAH] == "TRES_MODESTE"', '800')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 11, 'T01', null) , 
(11, 'T02', null) , 
(11, 'T03', null) , 
(11, 'T04', null) , 
(11, 'T05', null) , 
(11, 'T06', null) , 
(11, 'T08', null) , 
(11, 'T10', null) , 
(11, 'T11', null) , 
(11, 'T12', null) , 
(11, 'T13', null) , 
(11, 'T14', null) , 
(11, 'T20', null) , 
(11, 'T21', null) , 
(11, 'T22', null) , 
(11, 'T23', null) , 
(11, 'T24', null) , 
(11, 'T25', null) , 
(11, 'T26', null) , 
(11, 'T27', null) , 
(11, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_12_OPAH_CAPE.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 12, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(12, 'E', null, '$L.age >= 15') , 
(12, 'E', null, '$F.ptz === false') , 
(12, 'M', '$F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE"', '$D.cout_ht_to') , 
(12, 'P', '$F.tranche[ANAH] == "MODESTE"', '500') , 
(12, 'P', '$F.tranche[ANAH] == "TRES_MODESTE"', '800')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 12, 'T01', null) , 
(12, 'T02', null) , 
(12, 'T03', null) , 
(12, 'T04', null) , 
(12, 'T05', null) , 
(12, 'T06', null) , 
(12, 'T08', null) , 
(12, 'T10', null) , 
(12, 'T11', null) , 
(12, 'T12', null) , 
(12, 'T13', null) , 
(12, 'T14', null) , 
(12, 'T20', null) , 
(12, 'T21', null) , 
(12, 'T22', null) , 
(12, 'T23', null) , 
(12, 'T24', null) , 
(12, 'T25', null) , 
(12, 'T26', null) , 
(12, 'T27', null) , 
(12, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_13_CAPE_audit.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 13, 'E', null, '$F.revenu <= 45000') , 
(13, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(13, 'E', null, '$L.age > 10') , 
(13, 'E', null, '$L.type == "M"')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 13, 'T29', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 13 AND code_travaux = 'T29'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 13 AND code_travaux = 'T29'), 'P', null, '120')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_14_Coup_de_pouce_Dieppe_Maritime.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 14, 'E', null, '$F.revenu <= 45000') , 
(14, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(14, 'E', null, '$L.age > 10') , 
(14, 'E', null, '$L.type == "M"') , 
(14, 'E', '( ( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI" ) && $L.age >= 15 && $F.ptz === false', 'false')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 14, 'T01', null) , 
(14, 'T02', null) , 
(14, 'T03', null) , 
(14, 'T04', null) , 
(14, 'T05', null) , 
(14, 'T06', null) , 
(14, 'T08', null) , 
(14, 'T10', null) , 
(14, 'T11', null) , 
(14, 'T12', null) , 
(14, 'T14', null) , 
(14, 'T20', null) , 
(14, 'T21', null) , 
(14, 'T27', null) , 
(14, 'T29', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T01'), 'MT', null, '( $T.nb_fenetres * 1.62 + $T.nb_portes_fenetres * 2.58 ) * 75') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T01'), 'P', null, '1500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T02'), 'MT', null, '$T.tech[SURFACE_ISOLANT_MURS_INT] * 8') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T02'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T03'), 'MT', null, '$T.tech[SURFACE_ISOLANT_MURS_EXT] * 20') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T03'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T04'), 'MT', null, '$T.tech[SURFACE_ISOLANT_COMBLES] * 8') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T04'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T05'), 'MT', null, '$T.tech[SURFACE_ISOLANT_TOIT_INT] * 15') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T05'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T06'), 'MT', null, '$T.tech[SURFACE_ISOLANT_TOIT_EXT] * 20') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T06'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T08'), 'MT', null, '$T.tech[SURFACE_ISOLANT_PLANCHER_BAS] * 6') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T08'), 'P', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T10'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T10'), 'P', null, '150') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T11'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T11'), 'P', null, '750') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T12'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T12'), 'P', null, '750') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T14'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T14'), 'P', null, '150') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T20'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T20'), 'P', null, '300') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T21'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T21'), 'P', null, '1600') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T27'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T27'), 'P', null, '300') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T29'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 14 AND code_travaux = 'T29'), 'P', null, '120')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_15_CC_Caux_estuaire.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 15, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(15, 'E', '$F.tranche[CG76] == "HORS_TRANCHE"', 'false') , 
(15, 'P', null, '400')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 15, 'T01', null) , 
(15, 'T02', null) , 
(15, 'T03', null) , 
(15, 'T04', null) , 
(15, 'T05', null) , 
(15, 'T06', null) , 
(15, 'T07', null) , 
(15, 'T08', null) , 
(15, 'T09', null) , 
(15, 'T10', null) , 
(15, 'T11', null) , 
(15, 'T12', null) , 
(15, 'T14', null) , 
(15, 'T15', null) , 
(15, 'T20', null) , 
(15, 'T21', null) , 
(15, 'T26', null) , 
(15, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T01'), 'MF', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ht_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T01'), 'MF', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ht_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T01'), 'MF', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ht_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T01'), 'P', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T01'), 'P', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T01'), 'P', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T02'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T02'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T02'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T03'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T03'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T03'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T05'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T05'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T05'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T06'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T06'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T06'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T07'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T07'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T07'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T08'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T08'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T08'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T09'), 'MF', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T09'), 'MF', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ttc_to * 25 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T09'), 'MF', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ttc_to * 30 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T09'), 'P', null, '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T09'), 'P', null, '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T09'), 'P', null, '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T10'), 'MF', '$F.tranche[CG76] == "CG1" && $L.age >= 20', '$T.cout_ht_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T10'), 'MF', '$F.tranche[CG76] == "CG2" && $L.age >= 20', '$T.cout_ht_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T10'), 'MF', '$F.tranche[CG76] == "CG3" && $L.age >= 20', '$T.cout_ht_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T10'), 'P', null, '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T10'), 'P', null, '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T10'), 'P', null, '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T11'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T11'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T11'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T12'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T12'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T12'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T14'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T14'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T14'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T15'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T15'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T15'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T20'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T20'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T20'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T21'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T21'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T21'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T26'), '', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T26'), '', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T26'), '', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T27'), 'MT', '$F.tranche[CG76] == "CG1" && $L.age >= 10', '$T.cout_ttc_to * 10 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T27'), 'MT', '$F.tranche[CG76] == "CG2" && $L.age >= 10', '$T.cout_ttc_to * 12.5 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 15 AND code_travaux = 'T27'), 'MT', '$F.tranche[CG76] == "CG3" && $L.age >= 10', '$T.cout_ttc_to * 20 / 100')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_16_CC_Caux_Vallee_de_Seine.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 16, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(16, 'E', null, '$L.age >= 15') , 
(16, 'E', null, '$F.ptz === false') , 
(16, 'M', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" )', '( $D.cout_ht_to * 15 / 100 ) + 500') , 
(16, 'M', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "TRES_MODESTE" )', '( $D.cout_ht_to * 20 / 100 ) + 500') , 
(16, 'M', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '$D.cout_ht_to') , 
(16, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" )', '2500') , 
(16, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "TRES_MODESTE" )', '2500') , 
(16, 'P', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '1500')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 16, 'T01', null) , 
(16, 'T02', null) , 
(16, 'T03', null) , 
(16, 'T04', null) , 
(16, 'T05', null) , 
(16, 'T06', null) , 
(16, 'T08', null) , 
(16, 'T10', null) , 
(16, 'T11', null) , 
(16, 'T12', null) , 
(16, 'T13', null) , 
(16, 'T14', null) , 
(16, 'T20', null) , 
(16, 'T21', null) , 
(16, 'T22', null) , 
(16, 'T23', null) , 
(16, 'T24', null) , 
(16, 'T25', null) , 
(16, 'T26', null) , 
(16, 'T27', null) , 
(16, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_17_CC_du_Pays_Neufchatelois.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 17, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(17, 'E', null, '$L.age >= 15') , 
(17, 'M', null, '$D.cout_ht_to') , 
(17, 'P', null, '500')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 17, 'T01', null) , 
(17, 'T02', null) , 
(17, 'T03', null) , 
(17, 'T04', null) , 
(17, 'T05', null) , 
(17, 'T06', null) , 
(17, 'T08', null) , 
(17, 'T10', null) , 
(17, 'T11', null) , 
(17, 'T12', null) , 
(17, 'T13', null) , 
(17, 'T14', null) , 
(17, 'T20', null) , 
(17, 'T21', null) , 
(17, 'T22', null) , 
(17, 'T23', null) , 
(17, 'T24', null) , 
(17, 'T25', null) , 
(17, 'T26', null) , 
(17, 'T27', null) , 
(17, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_18_CC_cote_d_Albatre.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 18, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(18, 'E', null, '$L.age >= 15') , 
(18, 'M', null, '$D.cout_ht_to') , 
(18, 'P', null, '500')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 18, 'T01', null) , 
(18, 'T02', null) , 
(18, 'T03', null) , 
(18, 'T04', null) , 
(18, 'T05', null) , 
(18, 'T06', null) , 
(18, 'T08', null) , 
(18, 'T10', null) , 
(18, 'T11', null) , 
(18, 'T12', null) , 
(18, 'T13', null) , 
(18, 'T14', null) , 
(18, 'T20', null) , 
(18, 'T21', null) , 
(18, 'T22', null) , 
(18, 'T23', null) , 
(18, 'T24', null) , 
(18, 'T25', null) , 
(18, 'T26', null) , 
(18, 'T27', null) , 
(18, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_19_CC_du_canton_de_Lyons_la_Foret.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 19, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(19, 'E', null, '$L.age >= 15') , 
(19, 'M', null, '$D.cout_ht_to') , 
(19, 'P', null, '800')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 19, 'T01', null) , 
(19, 'T02', null) , 
(19, 'T03', null) , 
(19, 'T04', null) , 
(19, 'T05', null) , 
(19, 'T06', null) , 
(19, 'T08', null) , 
(19, 'T10', null) , 
(19, 'T11', null) , 
(19, 'T12', null) , 
(19, 'T13', null) , 
(19, 'T14', null) , 
(19, 'T20', null) , 
(19, 'T21', null) , 
(19, 'T22', null) , 
(19, 'T23', null) , 
(19, 'T24', null) , 
(19, 'T25', null) , 
(19, 'T26', null) , 
(19, 'T27', null) , 
(19, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_20_CC_du_canton_du_Neubourg.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 20, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI"') , 
(20, 'E', null, '$L.age >= 15') , 
(20, 'M', null, '$D.cout_ht_to') , 
(20, 'P', null, '500')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 20, 'T01', null) , 
(20, 'T02', null) , 
(20, 'T03', null) , 
(20, 'T04', null) , 
(20, 'T05', null) , 
(20, 'T06', null) , 
(20, 'T08', null) , 
(20, 'T10', null) , 
(20, 'T11', null) , 
(20, 'T12', null) , 
(20, 'T13', null) , 
(20, 'T14', null) , 
(20, 'T20', null) , 
(20, 'T21', null) , 
(20, 'T22', null) , 
(20, 'T23', null) , 
(20, 'T24', null) , 
(20, 'T25', null) , 
(20, 'T26', null) , 
(20, 'T27', null) , 
(20, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_21_CC_du_roumois_Nord.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 21, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(21, 'E', null, '$L.age >= 15') , 
(21, 'E', null, '$F.ptz === false') , 
(21, 'M', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" )', '$D.cout_ht_to * 10 / 100') , 
(21, 'M', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "TRES_MODESTE" )', '$D.cout_ht_to * 10 / 100') , 
(21, 'M', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '$D.cout_ht_to * 10 / 100') , 
(21, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" )', '2000') , 
(21, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "TRES_MODESTE" )', '2000') , 
(21, 'P', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '2000')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 21, 'T01', null) , 
(21, 'T02', null) , 
(21, 'T03', null) , 
(21, 'T04', null) , 
(21, 'T05', null) , 
(21, 'T06', null) , 
(21, 'T08', null) , 
(21, 'T10', null) , 
(21, 'T11', null) , 
(21, 'T12', null) , 
(21, 'T13', null) , 
(21, 'T14', null) , 
(21, 'T20', null) , 
(21, 'T21', null) , 
(21, 'T22', null) , 
(21, 'T23', null) , 
(21, 'T24', null) , 
(21, 'T25', null) , 
(21, 'T26', null) , 
(21, 'T27', null) , 
(21, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_22_CC_canton_de_Rugles.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 22, 'E', null, '( ( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" || $F.tranche[ANAH] == "TRES_MODESTE" ) ) || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(22, 'E', null, '$L.age >= 15') , 
(22, 'E', null, '$F.ptz === false') , 
(22, 'M', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" )', '$D.cout_ht_to * 10 / 100') , 
(22, 'M', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "TRES_MODESTE" )', '$D.cout_ht_to * 10 / 100') , 
(22, 'M', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '$D.cout_ht_to * 10 / 100') , 
(22, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "MODESTE" )', '1000') , 
(22, 'P', '( $F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" ) && ( $F.tranche[ANAH] == "TRES_MODESTE" )', '1000') , 
(22, 'P', '$F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"', '1500')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 22, 'T01', null) , 
(22, 'T02', null) , 
(22, 'T03', null) , 
(22, 'T04', null) , 
(22, 'T05', null) , 
(22, 'T06', null) , 
(22, 'T08', null) , 
(22, 'T10', null) , 
(22, 'T11', null) , 
(22, 'T12', null) , 
(22, 'T13', null) , 
(22, 'T14', null) , 
(22, 'T20', null) , 
(22, 'T21', null) , 
(22, 'T22', null) , 
(22, 'T23', null) , 
(22, 'T24', null) , 
(22, 'T25', null) , 
(22, 'T26', null) , 
(22, 'T27', null) , 
(22, 'T28', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_23_CCEMS.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------

-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 23, 'T25', 'L''installateur doit être titulaire de la qualification Qualisol') , 
(23, 'T27', 'L''installateur doit être titulaire de la qualification Qualisol')  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 23 AND code_travaux = 'T25'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 23 AND code_travaux = 'T25'), 'P', null, '1000') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 23 AND code_travaux = 'T27'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 23 AND code_travaux = 'T27'), 'P', null, '500')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_24_Pont_de_l_Arche.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 24, 'E', null, '$F.statut != "PROP_BAIL" && $F.statut != "PROP_BAIL_SCI"') , 
(24, 'E', null, '$L.age >= 2') , 
(24, 'E', null, '$D.cout_ttc_to > 10000')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 24, 'T01', null) , 
(24, 'T02', null) , 
(24, 'T03', null) , 
(24, 'T04', null) , 
(24, 'T05', null) , 
(24, 'T06', null) , 
(24, 'T07', null) , 
(24, 'T08', null) , 
(24, 'T09', null) , 
(24, 'T10', null) , 
(24, 'T11', null) , 
(24, 'T12', null) , 
(24, 'T13', null) , 
(24, 'T20', null) , 
(24, 'T21', null) , 
(24, 'T23', null) , 
(24, 'T24', null) , 
(24, 'T25', null) , 
(24, 'T26', null) , 
(24, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_25_Caudebec_en_Caux.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 25, 'E', null, '$F.statut != "PROP_BAIL" && $F.statut != "PROP_BAIL_SCI"') , 
(25, 'E', null, '$L.age >= 2')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 25, 'T11', null) , 
(25, 'T12', null) , 
(25, 'T20', null) , 
(25, 'T21', null) , 
(25, 'T23', null) , 
(25, 'T24', null) , 
(25, 'T25', null) , 
(25, 'T26', null) , 
(25, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T11'), 'MF', null, '$T.cout_ttc_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T11'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T12'), 'MF', null, '$T.cout_ttc_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T12'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T20'), 'MF', null, '$T.cout_ttc_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T20'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T21'), 'MF', null, '$T.cout_ttc_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T21'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T23'), 'MF', null, '$T.cout_ttc_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T23'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T24'), 'MF', null, '$T.cout_ttc_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T24'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T25'), 'MF', null, '$T.cout_ttc_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T25'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T26'), 'MF', null, '$T.cout_ttc_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T26'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T27'), 'MF', null, '$T.cout_ttc_fo * 20 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 25 AND code_travaux = 'T27'), 'P', null, '500')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_26_Lillebonne.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 26, 'E', null, '$F.statut != "PROP_BAIL" && $F.statut != "PROP_BAIL_SCI"') , 
(26, 'E', null, '$L.age >= 2')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 26, 'T11', null) , 
(26, 'T12', null) , 
(26, 'T20', null) , 
(26, 'T21', null) , 
(26, 'T23', null) , 
(26, 'T24', null) , 
(26, 'T25', null) , 
(26, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T11'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T11'), 'P', null, '650') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T12'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T12'), 'P', null, '650') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T20'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T20'), 'P', null, '450') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T21'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T21'), 'P', null, '600') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T23'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T23'), 'P', null, '1000') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T24'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T24'), 'P', null, '1000') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T25'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T25'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T27'), 'MF', null, '$T.cout_ttc_fo') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 26 AND code_travaux = 'T27'), 'P', null, '450')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_27_Bolbec.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 27, 'E', null, '$F.statut != "PROP_BAIL" && $F.statut != "PROP_BAIL_SCI"') , 
(27, 'E', null, '$L.age >= 2') , 
(27, 'E', null, '$D.cout_ttc_to > 10000')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 27, 'T01', null) , 
(27, 'T02', null) , 
(27, 'T03', null) , 
(27, 'T04', null) , 
(27, 'T05', null) , 
(27, 'T06', null) , 
(27, 'T07', null) , 
(27, 'T08', null) , 
(27, 'T09', null) , 
(27, 'T10', null) , 
(27, 'T11', null) , 
(27, 'T12', null) , 
(27, 'T13', null) , 
(27, 'T20', null) , 
(27, 'T21', null) , 
(27, 'T23', null) , 
(27, 'T24', null) , 
(27, 'T25', null) , 
(27, 'T26', null) , 
(27, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_28_Mont_saint_Aignan.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 28, 'E', null, '$F.statut != "PROP_BAIL" && $F.statut != "PROP_BAIL_SCI"') , 
(28, 'E', null, '$L.age >= 2') , 
(28, 'E', null, '$D.cout_ttc_to > 10000')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 28, 'T01', null) , 
(28, 'T02', null) , 
(28, 'T03', null) , 
(28, 'T04', null) , 
(28, 'T05', null) , 
(28, 'T06', null) , 
(28, 'T07', null) , 
(28, 'T08', null) , 
(28, 'T09', null) , 
(28, 'T10', null) , 
(28, 'T11', null) , 
(28, 'T12', null) , 
(28, 'T13', null) , 
(28, 'T20', null) , 
(28, 'T21', null) , 
(28, 'T23', null) , 
(28, 'T24', null) , 
(28, 'T25', null) , 
(28, 'T26', null) , 
(28, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_29_Grand_Couronne.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 29, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(29, 'E', null, '$L.age >= 2') , 
(29, 'M', '$L.bbc === true', '$D.montant_to + 1000')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 29, 'T01', null) , 
(29, 'T04', 'Les isolants doivent être d''origine minérale ou d''origine bio-sourcée.') , 
(29, 'T10', null) , 
(29, 'T25', null) , 
(29, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T01'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T01'), 'P', null, '( $T.nb_fenetres + $T.nb_portes_fenetres ) * 50') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T04'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T04'), 'P', null, '400') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T10'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T10'), 'P', null, '$T.tech[NB_PORTES] * 50') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T25'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T25'), 'P', null, '500') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T27'), 'MT', null, '$T.cout_ttc_to') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 29 AND code_travaux = 'T27'), 'P', null, '500')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_30_Grand_Quevilly.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 30, 'E', null, '$F.statut == "PROP_RES_1" || $F.statut == "PROP_RES_1_SCI" || $F.statut == "PROP_BAIL" || $F.statut == "PROP_BAIL_SCI"') , 
(30, 'E', null, '$L.age >= 2')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 30, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 30 AND code_travaux = 'T27'), 'MT', null, '$T.cout_ttc_to * 15 / 100') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 30 AND code_travaux = 'T27'), 'P', null, '1000')  ; 
 
-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_31_CEE.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 31, 'E', null, '$L.age > 2')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 31, 'T01', 'Les caractéristiques thermiques (Coefficient de transmission surfacique Uw et facteur Solaire Sw) doivent respecter les conditions suivantes:
Fenêtres de toiture: Uw ≤ 1,5 W/m2 K et  Sw ≥ 0,36 
Autres fenêtres ou porte-fenêtres: Uw ≤1,3 W/m2 K et Sw ≥ 0,3 ou Uw ≤1,7 W/m2 K et Sw ≥ 0,36') , 
(31, 'T02', 'La résistance thermique R de l''isolation installée doit être ≥ 3,7 m².K/W') , 
(31, 'T03', 'La résistance thermique R de l''isolation installée doit être ≥ 3,7 m².K/W') , 
(31, 'T04', 'La résistance thermique R de l''isolation installée doit être ≥ 7 m².K/W') , 
(31, 'T05', 'La résistance thermique R de l''isolation installée doit être ≥ 6 m².K/W') , 
(31, 'T06', 'La résistance thermique R de l''isolation installée doit être ≥ 6 m².K/W') , 
(31, 'T07', 'La résistance thermique R de l''isolation installée doit être ≥ 4,5 m².K/W') , 
(31, 'T08', 'La résistance thermique R de l''isolation installée doit être ≥ 3 m².K/W') , 
(31, 'T09', 'La résistance thermique additionnelle de la fermeture isolante doit être > 0,22 m².K/W') , 
(31, 'T11', 'L''efficacité énergétique (Etas) de la chaudière à condensation doit être ≥ 90%.') , 
(31, 'T12', 'L''efficacité énergétique (Etas) de la chaudière à condensation doit être ≥ 90%.') , 
(31, 'T13', 'L''équipement doit posséder des fonctions de programmation d''intermittence.') , 
(31, 'T14', 'Il peut s''agir d''une ventilation hygroréglable de type A ou B. 
Le caisson de ventilation doit avoir puissance électrique absorbée  ≤15 WThC dans une configuration T4 avec une salle de bain et un WC.') , 
(31, 'T15', null) , 
(31, 'T18', 'Les radiateurs doivent être dimensionnés avec un delta de température nominal ≤ 40 K.') , 
(31, 'T19', null) , 
(31, 'T20', 'Le rendement énergétique, ŋ, doit être ≥ 70 %
La concentration moyenne de monoxyde de carbone, E, doit être ≤ 0,3%
L''indice de performance environnemental, I, doit être ≤ 2') , 
(31, 'T21', 'Le chaudière doit être de classe 5 au sens de la norme NF EN 303.5 ou doit bénéficier du label flamme verte.') , 
(31, 'T22', 'La pompe à chaleur doit posséder un SCOP (coefficient de performance saisonnier) ≥ 3,9.') , 
(31, 'T23', 'L''efficacité énergétique (Etas) de la pompe à chaleur (PAC) doit être:
 ≥ 102% pour les PAC moyenne et haute température
 ≥ 117% pour les PAC basse température') , 
(31, 'T24', 'L''efficacité énergétique (Etas) de la pompe à chaleur (PAC) doit être:
 ≥ 102% pour les PAC moyenne et haute température
 ≥ 117% pour les PAC basse température') , 
(31, 'T25', 'En attente révision fiche') , 
(31, 'T26', 'Le COP (coefficient de performance) doit être:
> 2,5 pour une installation sur air extrait
> 2,4 pour toute autre installation') , 
(31, 'T27', null)  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Fichier : C:/SIMULAIDES/Data/stock/dispositifs/HNO_32_CITE_credit_impot.xls
-- -----------------------------------------------------

-- Import dans la table regle_dispositif 
-- -----------------------------------------------------
INSERT INTO dispositif_regle (`id_dispositif`, `type`, `condition_regle`, `expression` )
 VALUES ( 32, 'E', null, '$F.statut != "PROP_BAIL" && $F.statut != "PROP_BAIL_SCI" && $F.statut != "PROP_RES_2"') , 
(32, 'E', null, '$L.age >= 2') , 
(32, 'P', '$F.nb_adultes == 1', '( 8000 - $F.m_credit_impot + ( $F.nb_pers_charge * 400 ) + ( $F.nb_enf_gardealt * 200 ) ) * 30 / 100') , 
(32, 'P', '$F.nb_adultes > 1', '( 16000 - $F.m_credit_impot + ( $F.nb_pers_charge * 400 ) + ( $F.nb_enf_gardealt * 200 ) ) * 30 / 100')  ; 
 
-- Import dans la table aide 
-- -----------------------------------------------------
INSERT INTO dispositif_travaux (`id_dispositif`, `code_travaux`, `eligibilite_specifique` )
 VALUES ( 32, 'T01', 'Fenêtres de toiture: Uw ≤ 1,5 W/m2 K et  Sw ≤ 0,36
Autres fenêtres ou porte-fenêtres: Uw ≤1,3 W/m2 K et Sw ≥ 0,3 ou Uw ≤1,7 W/m2 K et Sw ≥ 0,36 
Les travaux doivent conduire à isoler la moitié des parois vitrées du logement en nombre de fenêtres') , 
(32, 'T02', 'Résistance thermique, R ≥3,7 m².k/W 
Les travaux doivent conduire à isoler au moins 50% de la surface totale des murs donnant sur l''extérieur') , 
(32, 'T03', 'Résistance thermique, R ≥3,7 m².k/W 
Les travaux doivent conduire à isoler au moins 50% de la surface totale des murs donnant sur l''extérieur') , 
(32, 'T04', 'Résistance thermique, R ≥7 m².k/W') , 
(32, 'T05', 'Résistance thermique, R ≥6 m².k/W') , 
(32, 'T06', 'Résistance thermique, R ≥6 m².k/W') , 
(32, 'T07', 'Résistance thermique, R ≥4,5 m².k/W e') , 
(32, 'T08', 'Résistance thermique, R ≥3 m².k/W') , 
(32, 'T09', 'Résistance thermique, R ≥ 0,22 m2.K/W') , 
(32, 'T10', 'Coefficient de transmission thermique, Ud ≤ 1,7 W/m2 K') , 
(32, 'T11', 'Pose d''un programmateur et calorifugeage des réseaux (R ≥ 1,2 m2 K/W)') , 
(32, 'T12', 'Pose d''un programmateur et calorifugeage des réseaux (R ≥ 1,2 m2 K/W)') , 
(32, 'T13', null) , 
(32, 'T20', 'Concentration moyenne de monoxyde de carbone E ≤ 0,3%
Rendement énergétique ŋ ≥ 70 %') , 
(32, 'T21', 'Chaudières avec puissance < à 30kW respectant les seuils de rendement énergétique et émissions de polluants de la classe 5 de la norme NF EN 303.5') , 
(32, 'T23', 'Coefficient de performance, COP > 3,4') , 
(32, 'T24', 'Coefficient de performance, COP > 3,4') , 
(32, 'T25', 'Certification CSTBat ou Solar Keymark ou équivalent') , 
(32, 'T26', 'Le Coefficient de Performance, COP, doit être:
>2,4 pour une installation sur air ambiant ou air extérieur
>2,5 pour une installation sur air extrait
>2,3 pour une installation géothermique
avec une température d’eau chaude de référence de 52,2°C') , 
(32, 'T27', 'Certification CSTBat ou Solar Keymark ou équivalent')  ; 
 
-- Import dans la table regle_aide 
-- -----------------------------------------------------
INSERT INTO disp_travaux_regle (`id_disp_travaux`, `type`, `condition_regle`, `expression` )
 VALUES (  (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T01'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T01] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T02'), 'MT', null, '30 / 100 * ( $T.cout_ttc_to - $T.cumul_to[T02] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T02'), 'P', null, '30 / 100 * ( $T.tech[SURFACE_ISOLANT_MURS_INT] * 100 )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T03'), 'MT', null, '30 / 100 * ( $T.cout_ttc_to - $T.cumul_to[T03] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T03'), 'P', null, '30 / 100 * ( $T.tech[SURFACE_ISOLANT_MURS_EXT] * 150 )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T04'), 'MT', null, '30 / 100 * ( $T.cout_ttc_to - $T.cumul_to[T04] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T04'), 'P', null, '30 / 100 * ( $T.tech[SURFACE_ISOLANT_COMBLES] * 100 )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T05'), 'MT', null, '30 / 100 * ( $T.cout_ttc_to - $T.cumul_to[T05] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T05'), 'P', null, '30 / 100 * ( $T.tech[SURFACE_ISOLANT_TOIT_INT] * 100 )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T06'), 'MT', null, '30 / 100 * ( $T.cout_ttc_to - $T.cumul_to[T06] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T06'), 'P', null, '30 / 100 * ( $T.tech[SURFACE_ISOLANT_TOIT_EXT] * 150 )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T07'), 'MT', null, '30 / 100 * ( $T.cout_ttc_to - $T.cumul_to[T07] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T07'), 'P', null, '30 / 100 * ( $T.tech[SURFACE_ISOLANT_TOIT_TERRASSE] * 150 )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T08'), 'MT', null, '30 / 100 * ( $T.cout_ttc_to - $T.cumul_to[T08] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T08'), 'P', null, '30 / 100 * ( $T.tech[SURFACE_ISOLANT_PLANCHER_BAS] * 100 )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T09'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T09] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T10'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T10] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T11'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T11] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T12'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T12] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T13'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T13] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T20'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T20] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T21'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T21] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T23'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T23] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T24'), 'MT', null, '30 / 100 * ( $T.cout_ttc_to - $T.cumul_to[T24] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T25'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T25] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T26'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T26] )') , 
( (SELECT id FROM dispositif_travaux WHERE id_dispositif = 32 AND code_travaux = 'T27'), 'MF', null, '30 / 100 * ( $T.cout_ttc_fo - $T.cumul_fo[T27] )')  ; 
 
-- Suppression du contenu des tables 
-- -----------------------------------------------------
DELETE FROM tranche_revenu ; 
DELETE FROM tranche_groupe ; 

-- Import de la table tranche_groupe 
-- -----------------------------------------------------
INSERT INTO tranche_groupe (code_groupe, intitule  ) 
 VALUES ( 'ANAH', 'Tranches de l''ANAH' )  ,
 ( 'CG27', 'Tranches du CG27' )  ,
 ( 'CG76', 'Tranches du CG76' )  ; 
 
-- Import de la table tranche_revenu 
-- -----------------------------------------------------
INSERT INTO tranche_revenu (code_groupe, code_tranche, intitule, mt_1_personne, mt_2_personne,  mt_3_personne, mt_4_personne, mt_5_personne, mt_6_personne, mt_personne_supp) 
 VALUES ( 'ANAH', 'TRES_MODESTE', 'Tres Modeste', 14300, 20913, 25152, 29384, 33633, 37872, 4239 ) ,
 ( 'ANAH', 'MODESTE', 'Modeste', 18332, 26811, 32242, 37669, 43117, 48548, 5431 ) ,
 ( 'CG27', 'TRES_SOCIAUX', 'Très sociaux', 11058, 16112, 19374, 21558, 25223, 28425, 3170 ) ,
 ( 'CG27', 'SOCIAUX', 'Sociaux', 13270, 19334, 23249, 25870, 30268, 34110, 3804 ) ,
 ( 'CG76', 'CG3', 'CG 76 Tranche 3', 9509, 13908, 16727, 19541, 22367, 25185, 2818 ) ,
 ( 'CG76', 'CG2', 'CG 76 Tranche 2', 11187, 16362, 19679, 22989, 26314, 29629, 3315 ) ,
 ( 'CG76', 'CG1', 'CG 76 Tranche 1', 13424, 19634, 23615, 27587, 31577, 35555, 3978 ) ; 
 