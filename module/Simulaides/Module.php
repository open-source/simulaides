<?php

namespace Simulaides;

use Simulaides\Constante\SessionConstante;
use Simulaides\EventListener\DispatchEventListener;
use Simulaides\EventListener\ResponseEventListener;
use Simulaides\EventListener\SessionEventListener;
use Simulaides\Service\SessionContainerService;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

/**
 * Classe Module
 *
 * Projet : Simulaides 2015
 
 *
 * @copyright Copyright © Ademe 2015, All Rights Reserved
 * @author
 * @package   Simulaides
 */
class Module
{
    /**
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        //Ajout du listener pour la gestion de la session
        $sessionListener = new SessionEventListener();
        $sessionListener->attach($eventManager);

        $responseListener = new ResponseEventListener();
        $responseListener->attach($eventManager);

        //Ajout du listener pour la gestion du layout
        $layoutListener = new DispatchEventListener();
        $layoutListener->attach($eventManager);
        /** @var SessionContainerService $session */
        $session = $e->getApplication()->getServiceManager()->get(SessionContainerService::SERVICE_NAME);
        $viewModel = $e->getViewModel();
        if (!$session->offsetExists(SessionConstante::JS_ACTIVE)) {
            $session->addOffset(SessionConstante::JS_ACTIVE, true);
        }
        if (!$session->offsetExists(SessionConstante::CGU)) {
            $session->addOffset(SessionConstante::CGU, false);
        }
        $jsActive = $session->offsetGet(SessionConstante::JS_ACTIVE);
        $viewModel->jsActive = ($jsActive === true);
        $viewModel->phpsessid = $session->getManager()->getId();

        $isConseillerConnecte = $session->estConseillerConnecte();
        $viewModel->isConseillerConnecte = ($isConseillerConnecte === true);


        // Gestion de l'ajout ou non du script de Google Analytics
        $tabConfig = $e->getApplication()->getServiceManager()->get(
            'ApplicationConfig'
        );
        $useGoogleAnalytics = $tabConfig['use-google-analytics'];
        $viewModel->useGoogleAnalytics = $useGoogleAnalytics;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . '/autoload_classmap.php',
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }
}
