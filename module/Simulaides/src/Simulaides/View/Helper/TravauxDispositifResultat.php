<?php
namespace Simulaides\View\Helper;

use Simulaides\Constante\TableConstante;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Zend\Form\View\Helper\AbstractHelper;

/**
 * Classe TravauxDispositifResultat
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class TravauxDispositifResultat extends AbstractHelper
{
    /**
     * Permet de générer la liste des travaux pour un dispositif
     *
     * @param array  $listLignes   Liste des informations des lignes
     * @param boolean  $affichageMontant   affichage des montants
     *
     * @return string
     */
    public function __invoke($listLignes,$affichageMontant)
    {
        $render = $this->renderTableauHtml($listLignes,$affichageMontant);

        return $render;
    }

    /**
     * Permet de générer un tableau html
     *
     * @param array  $listLignes   Liste des informations des lignes
     * @param boolean  $affichageMontant   affichage des montants
     *
     * @return string
     */
    private function renderTableauHtml($listLignes, $affichageMontant)
    {

        $render = '';
        if ($affichageMontant) {

            //Ajout de l'en-tête du tableau
            $render .= '<div class="row table-head print-affiche">';
            $render .= '<div class="col-sm-6 print-affiche"></div>';
            $render .= '<div class="col-sm-3 text-right print-affiche">Montant de l\'aide</div>';
            $render .= '<div class="col-sm-3 text-right print-affiche">Coût HT</div>';
            $render .= '</div>';
        }


        //Construction des lignes du tableau
        /** @var  TableLigne $eltLigne */
        foreach ($listLignes as $eltLigne) {
            $cellule = $eltLigne->getTabCellule();
            $render .= '<div class="row form-row panel-travaux print-affiche">';
            $render .= '<div class="row table-row print-affiche">';
            if ($affichageMontant && isset($cellule[2])) {
                $render .= '<h4 class="panel-title col-sm-6 print-affiche">' . $cellule[0]->getIntitule() . '</h4>';
                $render .= '<div class="col-sm-3 text-right print-affiche">' . $cellule[2]->getIntitule() . '</div>';
                $render .= '<div class="col-sm-3 text-right print-affiche">' . $cellule[3]->getIntitule() . '</div>';
            } else {
                $render .= '<h4 class="panel-title print-affiche">' . $cellule[0]->getIntitule() . '</h4>';
            }
            $render .= '</div>';
            if (isset($cellule[1]) && !empty($cellule[1]) && $cellule[1]->getIntitule()!="") {
                $render .= '<p class="table-description print-affiche">' . $cellule[1]->getIntitule() . '</p>';
            }
            $render .= '</div>';

        }

        return $render;
    }

}
