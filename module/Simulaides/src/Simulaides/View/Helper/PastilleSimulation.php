<?php

namespace Simulaides\View\Helper;

use Simulaides\Constante\MainConstante;
use Zend\Form\View\Helper\AbstractHelper;

/**
 * Classe PastilleSimulation
 * Aide de vue pour l'affichage des pastilles au dessus de la simulation
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class PastilleSimulation extends AbstractHelper
{
    /**
     * @var bool
     */
    protected $isComparatif;

    /**
     * Etape active à afficher
     * @var string
     */
    private $etapeActive;

    /**
     * Les urls pour les pastilles
     * @var array
     */
    private $urlsRetour;

    /**
     * Retourne les pastilles à afficher au dessus de la simulation
     *
     * @param string $etapeActive Etape active
     * @param array  $urlsRetour  urls de retour pour les pastilles
     *
     * @return string
     */
    public function __invoke($etapeActive, $urlsRetour = [], $isComparatif = false)
    {
        //Affectation de létape active à afficher
        $this->etapeActive = $etapeActive;
        //Affectation de l'url de retour
        $this->urlsRetour = $urlsRetour;
        //Comparatif
        $this->isComparatif = $isComparatif;

        //Création des pastilles
        return $this->renderPastille();
    }

    /**
     * Créé les pastilles avec le libellé et le numéro
     *
     * @return string
     */
    private function renderPastille()
    {

        $render =   ' <div class="row stepper-compartif hidden-xs">
                    <div class="col-lg-12">
                        <ul class="stepper">';
        $render .= $this->getPastille('Ma situation',MainConstante::SIMUL_ETAPE_CARACTERISTIQUE);
        $render .= $this->getPastille('Mon projet',MainConstante::SIMUL_ETAPE_PROJET);
        $render .= $this->getPastille('Mes travaux',MainConstante::SIMUL_ETAPE_MES_TRAVAUX);
        if( $this->etapeActive == MainConstante::SIMUL_ETAPE_MES_OFFRES){
            $render .= $this->getPastille('Mes offres',MainConstante::SIMUL_ETAPE_MES_OFFRES);
        }
        $render .= $this->getPastille('Mon résultat',MainConstante::SIMUL_ETAPE_RESULTAT);

        $render .='    </ul>
                    </div>
                </div>';

        return $render;
    }


    /**
     *
     * @return string
     */
    private function getPastille($titre,$etape)
    {
        $class= 'step-pending';
        $url = isset($this->urlsRetour[$etape])? $this->urlsRetour[$etape] : '';
        if($etape == $this->etapeActive){
            $class= 'step-active';
        }else if($etape < $this->etapeActive){
            $class= 'step-completed';
        }
        if($url){
            $render = '<li class="'.$class.'">
                                <a href="'.$url.'">
                                    <div class="circle"></div>
                                    <span class="label">'.$titre.'</span>
                                </a>
                            </li>';
        }else{
            $render = '<li class="' . $class . '">                                 
                            <div class="circle"></div>
                            <span class="label">' . $titre . '</span>
                      </li>';
        }


        return $render;
    }
}
