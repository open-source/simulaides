<?php

namespace Simulaides\View\Helper;

use Doctrine\Common\Collections\Collection;
use Exception;
use Simulaides\Domain\Entity\DispTravauxRegle;
use Simulaides\Service\DispositifTravauxService;
use Simulaides\Service\RegleService;
use Zend\View\Helper\AbstractHelper;

/**
 * Classe TravauxDispositif
 * l'aide de vue du bandeau de l'application
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class TravauxDispositif extends AbstractHelper
{
    /**
     * Permet de faire l'affichage des règles des travaux du dispositif suivant Montants des aides pour les types de travaux, Montant aide main
     * d'oeuvre, Montant aide financière, Plafond de l'aide
     *
     * @param $listeTravauxRegles
     * @param $dispositifTravauxService
     * @param $regleService
     *
     * @return string
     * @throws Exception
     */
    public function __invoke($listeTravauxRegles, $dispositifTravauxService, $regleService)
    {
        return $this->renderTravauxDispositif($listeTravauxRegles, $dispositifTravauxService, $regleService);
    }

    /**
     * Permet de créer l'entête
     *
     * @param Collection               $listeTravauxRegles
     * @param DispositifTravauxService $dispositifTravauxService
     * @param RegleService             $regleService
     *
     * @return string
     * @throws Exception
     */
    private function renderTravauxDispositif($listeTravauxRegles, $dispositifTravauxService, $regleService)
    {
        $render = '';
        /* @var $regle DispTravauxRegle */
        foreach ($listeTravauxRegles->getIterator() as $regle) {
            //Cellule du type
            $libelle = $dispositifTravauxService->getLibelleTypeRegleTravaux($regle->getType());

            //Cellule de la formule
            $formule   = '';
            $condition = $regle->getConditionRegle();
            if (!empty($condition)) {
                $conditionLitterale = $regleService->getChaineLitterale($condition, false);
                $formule            = "Si " . htmlspecialchars($conditionLitterale, ENT_DISALLOWED) . "<br> Alors ";
            }

            $formule .= htmlspecialchars($regleService->getChaineLitterale($regle->getExpression(), false), ENT_DISALLOWED);

            $render .= '<tr>';
            $render .= '<td width= "80" align="right">';
            $render .= '<label><b>' . "Type :" . '</b></label></td>';
            $render .= '<td width= "80">' . $libelle . '</td>';
            $render .= '<td width= "80" align="right"><label><b>' . "Formule :" . '</b></label></td>';
            $render .= '<td width= "230">' . str_replace(['&amp;#8805;', '&amp;#8800;', '&amp;#8804'], ['>=', '!=', '<='], $formule) . '</td>';
            $render .= '</tr>';

        }
        if ($render) {
            $render = "<tr><td colspan=\"2\"><table cellpadding=\"5\" cellspacing=\"0\">" . $render . "</table></td></tr>";
        }

        return $render;
    }
}

