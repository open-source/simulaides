<?php
namespace Simulaides\View\Helper\Factory;

use Simulaides\Service\SessionContainerService;
use Simulaides\View\Helper\BandeauEntete;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe BandeauEnteteFactory
 * Factory pour l'aide de vue du bandeau de l'application
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper\Factory
 */
class BandeauEnteteFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Récupération des codes couleurs dans la session
        /** @var SessionContainerService $session */
        $session = $serviceLocator->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);

        //Création du helper
        $enteteHelper = new BandeauEntete();
        $enteteHelper->setAccesPris($session->accesPris());
        $conseiller = $session->getConseiller();
        if(!$conseiller){
            $enteteHelper->setARoleADEME(false);
            $enteteHelper->setARoleDrADEME(false);
        }else {
            $enteteHelper->setARoleADEME($session->getConseiller()->aRoleADEME());
            $enteteHelper->setARoleDrADEME($session->getConseiller()->aRoleDrADEME());
        }
        return $enteteHelper;
    }
}
