<?php
namespace Simulaides\View\Helper\Factory;


use Simulaides\Service\SessionContainerService;
use Simulaides\Service\VersionService;
use Simulaides\View\Helper\VersionAppHelper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe VersionAppHelperFactory
 * Factory pour créer l'aide de vue pour le paramétrage des couleurs
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper\Factory
 */
class VersionAppHelperFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Création du helper
        $versionAppHelper = new VersionAppHelper();
        /** @var SessionContainerService $session */
        $session  = $serviceLocator->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        $versionAppHelper->setVersion($session->getVersion());
        //$versionService = $serviceLocator->get(VersionService::SERVICE_NAME);
        return $versionAppHelper;
    }
}
