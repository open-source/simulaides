<?php
namespace Simulaides\View\Helper\Factory;

use Simulaides\Service\SessionContainerService;
use Simulaides\View\Helper\ColorHelper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ColorHelperFactory
 * Factory pour créer l'aide de vue pour le paramétrage des couleurs
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper\Factory
 */
class ColorHelperFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Récupération des codes couleurs dans la session
        /** @var SessionContainerService $session */
        $session  = $serviceLocator->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        $colorN1  = $session->getCouleurFondTitreN1();
        $colorN2  = $session->getCouleurFondTitreN2();
        $colorN3  = $session->getCouleurFondTitreN3();
        $colorBtn = $session->getCouleurFondBouton();

        //Création du helper
        $colorHelper = new ColorHelper();
        //Afectation des couleurs
        $colorHelper->setCouleurFondTitreN1($colorN1);
        $colorHelper->setCouleurFondTitreN2($colorN2);
        $colorHelper->setCouleurFondTitreN3($colorN3);
        $colorHelper->setCouleurFondBouton($colorBtn);

        return $colorHelper;
    }
}
