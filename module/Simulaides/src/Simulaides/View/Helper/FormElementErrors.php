<?php
namespace Simulaides\View\Helper;

use Zend\Form\ElementInterface;

/**
 * Classe FormElementErrors
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class FormElementErrors extends \Zend\Form\View\Helper\FormElementErrors
{
    /**
     * Templates for the close for message tags
     *
     * @var string
     */
    protected $messageCloseString = '</li></ul>';

    /**
     * Templates for the open for message tags
     *
     * @var string
     */
    protected $messageOpenFormat = '<ul class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button><li>';

    /**
     * Templates for the separators for message tags
     *
     * @var string
     */
    protected $messageSeparatorString = '</li><li>';

    /**
     * Render validation errors for the provided $element
     *
     * @param ElementInterface $element
     * @param array            $attributes
     *
     * @return string
     */
    public function render(ElementInterface $element, array $attributes = array())
    {
        return parent::render($element, $attributes);
    }
}
