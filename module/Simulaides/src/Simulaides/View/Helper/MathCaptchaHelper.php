<?php
namespace Simulaides\View\Helper;

use Simulaides\Components\MathCaptcha;
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\Captcha\AbstractWord;

/**
 * Classe MathCaptchaHelper
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class MathCaptchaHelper extends AbstractWord
{
    /**
     * Form element errors helper instance
     *
     * @var FormElementErrors
     */
    protected $elementErrorsHelper;

    /**
     * @param ElementInterface $element
     *
     * @return string
     */
    public function render(ElementInterface $element)
    {
        return $this->renderMathCaptcha($element);
    }

    /**
     * Permet de faire le render du captcha
     *
     * @param ElementInterface $element
     *
     * @return string
     */
    private function renderMathCaptcha(ElementInterface $element)
    {
        $captcha = $element->getCaptcha();
        $render = '<button
            name="matchCaptcha"
            type="button"
            id="start_simul"
            class="simul-btn col-lg-12 col-md-12 col-xs-12 col-sm-12 big-btn g-recaptcha hidden"
            data-sitekey="'.$captcha->getPublicKey().'"
            data-callback="submitFormCheckCguAccueil">
            Je lance une simulation <span class="chevron"></span>
        </button>';

        $render .= $this->getElementErrorsHelper()->render($element);

        return $render;
    }

    /**
     * Retrieve the FormElementErrors helper
     *
     * @return FormElementErrors
     */
    protected function getElementErrorsHelper()
    {
        if ($this->elementErrorsHelper) {
            return $this->elementErrorsHelper;
        }

        if (method_exists($this->view, 'plugin')) {
            $this->elementErrorsHelper = $this->view->plugin('form_element_errors');
        }

        if (!$this->elementErrorsHelper instanceof FormElementErrors) {
            $this->elementErrorsHelper = new FormElementErrors();
        }

        return $this->elementErrorsHelper;
    }
}
