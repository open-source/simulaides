<?php
namespace Simulaides\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\XmlRpc\Value\String;

/**
 * Classe BandeauEntete
 * l'aide de vue du bandeau de l'application
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class BandeauEntete extends AbstractHelper
{
    /**
     * Indique si on a demandé l'accès à PRIS
     * @var boolean
     */
    private $accesPris;

    /**
     * Indique si l'utilisateur a le rôle ADEME
     * @var boolean
     */
    private $aRoleADEME;


    /**
     * Indique si l'utilisateur a le rôle DrADEME
     * @var boolean
     */
    private $aRoleDrADEME;

    /**
     * Lien de l'url vers la page d'accueil
     * @var String
     */
    private $urlAccueil;

    /**
     * savoir si c'est la page d'accueil
     * @var boolean
     */
    private $isPageAccueil;

    /**
     * Permet de faire le bandeau de l'entête SIMUL'AIDE
     *
     * @param String $urlAccueil Url de la page d'accueil
     *
     * @return string
     */
    public function __invoke($urlAccueil = null,$isPageAccueil = false)
    {
        $this->urlAccueil = $urlAccueil;
        $this->isPageAccueil = $isPageAccueil;
        return $this->renderEntete();
    }

    /**
     * Permet de créer l'entête
     *
     * @return string
     */
    private function renderEntete()
    {
        if($this->isPageAccueil){
         return '';
        }

        if(!$this->accesPris && !$this->aRoleADEME && !$this->aRoleDrADEME ){
            $render = '<div class="container background-colorN1 bandeau-radius">';
            $render .= '<div class="row">';

            $render .= '<div class="col-xs-4 col-sm-4 simul-bandeau-pic-home-wrapper">
                        <div class="simul-bandeau-pic-home-no-image">
                        <p><a class="lien-bandeau" href="' . $this->urlAccueil . '" title="Accueil">Simul\'Aid€s</a></p><p class="small-text-head">Estimez le montant des aides pour rénover votre logement.</p>
                        </div>
                        </div><!-- Fin bloc 2 -->';
            // Libellé
            $render .= '<div class="col-xs-4 col-sm-6 simul-bandeau-titre-faire-n1">';
            $render .= '<div class="simul-spacer-inside-titre-n2">';
            $render .= '
                <a href="http://www.ademe.fr" title="Ademe">
                    <div class="simul-bandeau-logo-ademe2 col-xs-6 col-md-3">Accéder au site de l\'ADEME</div>
                </a>
            </div>';
            $render .= '</div><!-- Fin bloc 3 -->';
            $render .= '</div> <!-- Fin row -->';
            $render .= '</div><!-- Fin container -->';
            return $render;
        }


        $render = '<div class="container background-colorN1 enteteWrapper">';
        $render .= '<div class="row">';
        // Logo SIMULAIDES

        if (!$this->accesPris) {
            //cliquable vers l'accueil pour le particulier
            $render .= '<a href="' . $this->urlAccueil . '" title="Accueil">';
        }
        $render .= '<div class="col-xs-4 col-sm-3 simul-bandeau-logo color-transparent">';
        if (!$this->accesPris) {
            $render .= 'Retour à l\'accueil';
        }
        $render .= '</div>';
        if (!$this->accesPris) {
            //cliquable vers l'accueil pour le particulier
            $render .= '</a>';
        }
        $render .= '<div class="col-xs-4 col-sm-5 simul-bandeau-pic-home-wrapper"><div class="simul-bandeau-pic-home"></div></div>';
        // Libellé
        $render .= '<div class="col-xs-4 simul-bandeau-titre-n1">';
        $render .= '<div class="simul-spacer-inside-titre-n1 row">';
        $render .= '
                <a href="http://www.ademe.fr" title="Ademe">
                    <div class="simul-bandeau-logo-ademe col-xs-6 col-md-3">Accéder au site de l\'ADEME</div>
                </a>
            </div>';

        if($this->aRoleADEME){
            $render .= '<div class="hidden-xs simul-bandeau-inside-titre-n1"> ESPACE ADEME</div>';
        }
        elseif($this->aRoleDrADEME){
            $render .= '<div class="hidden-xs simul-bandeau-inside-titre-n1"> ESPACE DR ADEME</div>';
        }
        elseif ($this->accesPris) {
            $render .= '<div class="hidden-xs simul-bandeau-inside-titre-n1"> ESPACE CONSEILLER</div>';
        } else {
            $render .= '<div class="hidden-xs simul-bandeau-inside-titre-n1"> estimez le montant des aides pour
                                                                    rénover votre logement</div>';
        }
        $render .= '</div>';
        $render .= '</div>';
        $render .= '</div>';

        if($this->aRoleADEME){
            $render .= '<div class="container border-colorN1 simul-bandeau-estimez-lg"><div class="row border-colorN1 hidden-lg hidden-md hidden-sm"> ESPACE ADEME</div></div>';
        }
        elseif($this->aRoleDrADEME){
            $render .= '<div class="container border-colorN1 simul-bandeau-estimez-lg"><div class="row border-colorN1 hidden-lg hidden-md hidden-sm"> ESPACE DR ADEME</div></div>';
        }
        elseif ($this->accesPris) {
            $render .= '<div class="container border-colorN1 simul-bandeau-estimez-lg"><div class="row border-colorN1 hidden-lg hidden-md hidden-sm"> ESPACE CONSEILLER</div></div>';
        } else {
            $render .= '<div class="container border-colorN1 simul-bandeau-estimez-lg"><div class="row border-colorN1 hidden-lg hidden-md hidden-sm">estimez le montant des aides pour
                                                                                                                    rénover votre logement</div></div>';
        }

        return $render;
    }

    /**
     * @param boolean $accesPris
     */
    public function setAccesPris($accesPris)
    {
        $this->accesPris = $accesPris;
    }

    /**
     * @param bool $aRoleADEME
     */
    public function setARoleADEME($aRoleADEME)
    {
        $this->aRoleADEME = $aRoleADEME;
    }

    /**
     * @param bool $aRoleDrADEME
     */
    public function setARoleDrADEME($aRoleDrADEME)
    {
        $this->aRoleDrADEME = $aRoleDrADEME;
    }
}
