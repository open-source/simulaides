<?php
namespace Simulaides\View\Helper;

use Simulaides\Constante\SessionConstante;
use Zend\View\Helper\AbstractHelper;

/**
 * Classe ColorHelper
 * Aide de vue pour le paramétrage des couleurs
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class ColorHelper extends AbstractHelper
{
    /**
     * Couleur de fond des titres de niveau 1
     * @var string
     */
    private $couleurFondTitreN1;

    /**
     * Couleur de fond des titres de niveau 2
     * @var string
     */
    private $couleurFondTitreN2;

    /**
     * Couleur de fond des titres de niveau 3
     * @var string
     */
    private $couleurFondTitreN3;

    /**
     * Couleur de fond des boutons
     * @var string
     */
    private $couleurFondBouton;

    /**
     * Retourne le code couleur
     *
     * @param string $typeColor Type de la couleur demandée
     *
     * @return string
     */
    public function __invoke($typeColor)
    {
        $render = '';

        switch ($typeColor) {
            case SessionConstante::NAME_FOND_TITRE_N1:
                $render = $this->couleurFondTitreN1;
                break;
            case SessionConstante::NAME_FOND_TITRE_N2:
                $render = $this->couleurFondTitreN2;
                break;
            case SessionConstante::NAME_FOND_TITRE_N3:
                $render = $this->couleurFondTitreN3;
                break;
            case SessionConstante::NAME_FOND_BOUTON:
                $render = $this->couleurFondBouton;
                break;
        }
        return $render;
    }

    /**
     * @param string $couleurFondBouton
     */
    public function setCouleurFondBouton($couleurFondBouton)
    {
        $this->couleurFondBouton = $couleurFondBouton;
    }

    /**
     * @param string $couleurFondTitreN1
     */
    public function setCouleurFondTitreN1($couleurFondTitreN1)
    {
        $this->couleurFondTitreN1 = $couleurFondTitreN1;
    }

    /**
     * @param string $couleurFondTitreN2
     */
    public function setCouleurFondTitreN2($couleurFondTitreN2)
    {
        $this->couleurFondTitreN2 = $couleurFondTitreN2;
    }

    /**
     * @param string $couleurFondTitreN3
     */
    public function setCouleurFondTitreN3($couleurFondTitreN3)
    {
        $this->couleurFondTitreN3 = $couleurFondTitreN3;
    }
}
