<?php
namespace Simulaides\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\Exception\DomainException;
use Zend\Form\LabelAwareInterface;

/**
 * Classe FormLabel
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class FormLabel extends \Zend\Form\View\Helper\FormLabel
{

    public function __invoke(ElementInterface $element = null, $labelContent = null, $position = null)
    {
        if (!$element) {
            return $this;
        }

        $openTag = $this->openTag($element);
        $label   = '';
        if ($labelContent === null || $position !== null) {
            $label = $element->getLabel();
            if (empty($label)) {
                throw new DomainException(
                    sprintf(
                        '%s expects either label content as the second argument, ' .
                        'or that the element provided has a label attribute; neither found',
                        __METHOD__
                    )
                );
            }

            if (null !== ($translator = $this->getTranslator())) {
                $label = $translator->translate($label, $this->getTranslatorTextDomain());
            }

            if (!$element instanceof LabelAwareInterface || !$element->getLabelOption('disable_html_escape')) {
                $escapeHtmlHelper = $this->getEscapeHtmlHelper();
                $label            = $escapeHtmlHelper($label);
            }

            if ($element->getAttribute('required') === true || $element->getOption('required') === true) {
                $spanLabelRequired = '<span class="required"> * </span>';
                $label = $label.$spanLabelRequired;
            }
        }

        if ($label && $labelContent) {
            switch ($position) {
                case self::APPEND:
                    $labelContent .= $label;
                    break;
                case self::PREPEND:
                default:
                    $labelContent = $label . $labelContent;
                    break;
            }
        }

        if ($label && null === $labelContent) {
            $labelContent = $label;
        }

        return $openTag . $labelContent . $this->closeTag();
    }
}
