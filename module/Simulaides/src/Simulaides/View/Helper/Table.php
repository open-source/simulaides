<?php
namespace Simulaides\View\Helper;

use Simulaides\Constante\TableConstante;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Zend\Form\View\Helper\AbstractHelper;

/**
 * Classe Table
 * Permet de générer un tableau simple
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class Table extends AbstractHelper
{
    /**
     * Permet de générer un tableau
     *
     * @param array  $tabEntete    Liste des celllules de l'entête
     * @param array  $listLignes   Liste des informations des lignes
     * @param bool   $isDragable   Indique si le tableau est draggable (tri par drag and drop)
     * @param bool   $isTabAllPage Indique si le tableau doit être en pleine page
     * @param string $classeEntete
     * @param bool   $noScroll     A vrai si le tableau ne doit pas comporter de scroll
     *
     * @return string
     */
    public function __invoke($tabEntete, $listLignes, $isDragable = false, $isTabAllPage = false, $classeEntete = '', $noScroll = false)
    {
        $render = $this->renderTableauHtml($tabEntete, $listLignes, $isDragable, $isTabAllPage, $classeEntete, $noScroll);

        return $render;
    }

    /**
     * Permet de générer un tableau html
     *
     * @param array  $tabEntete    Liste des celllules de l'entête
     * @param array  $listLignes   Liste des informations des lignes
     * @param bool   $isDragable   Indique si le tableau est draggable (tri par drag and drop)
     * @param bool   $isTabAllPage Indique si le tableau doit être en pleine page
     * @param string $classeEntete
     * @param bool   $noScroll     A vrai si le tableau ne doit pas comporter de scroll
     *
     * @return string
     */
    private function renderTableauHtml($tabEntete, $listLignes, $isDragable, $isTabAllPage, $classeEntete, $noScroll)
    {
        $classTable = 'table table-condensed table-striped table-bordered simul-table';
        if (!$noScroll && count($listLignes) > 7) {
            if ($isTabAllPage) {
                $divClass = 'fix-tableau-all-page';
            } else {
                $divClass = 'fix-tableau';
            }
        }
        $render  = '<table class="' . $classTable . '">';
        $classTd = '';
        $styleTd = '';
        if ($isDragable) {
            //Ajout du cursor pour le drag & drop
            $classTd .= 'sorter';
            $styleTd .= 'style="cursor:move"';
        }
        //Construction de l'entête du tableau
        $renderEntete = '<tr class="simul-table-entete '.$classeEntete.'">';
        /** @var TableCellule $cellule */
        foreach ($tabEntete as $cellule) {
            //Construction des celulles de l'entête
            switch ($cellule->getTypeCellule()) {
                case TableConstante::TYPE_CASE_COCHER:
                    $checked = '';
                    if ($cellule->getIsChecked()) {
                        $checked = 'checked';
                    }
                    $renderEntete .= '<th><input title="'.$cellule->getTitle().'" id="'.$cellule->getIntitule().'" type="checkbox" ' . $checked . '></th>';
                    break;
                default:
                    $renderEntete .= '<th>' . $cellule->getIntitule() . '</th>';
                    break;
            }
        }
        $renderEntete .= '</tr>';

        $renderLignes = '';
        //Construction des lignes du tableau
        /** @var  TableLigne $eltLigne */
        if(count($listLignes) == 0) {
            return '<table class="' . $classTable . '"><tr width="100%"><td><div align="center">Aucun résultat</div></td></tr></table>';
        } else {
            foreach ($listLignes as $eltLigne) {
                $renderLignes .= '<tr id="' . $eltLigne->getId() . '" width="100%">';
                /** @var TableCellule $cellule */
                foreach ($eltLigne->getTabCellule() as $cellule) {
                    $classTdLine = empty($classTd) ? '' : ($classTd . ' ');
                    //Construction des celulles de la ligne
                    $renderLignes .= '<td title="' . $cellule->getTitle() . '" ';
                    switch ($cellule->getTypeCellule()) {
                        case TableConstante::TYPE_TEXTE:
                            $classTdLine  .= $this->getClassAlignement($cellule->getAlign());
                            $renderLignes .= 'class="' . $classTdLine . '" ' . $styleTd . '>';
                            $renderLignes .= $cellule->getIntitule();
                            break;
                        case TableConstante::TYPE_LIEN:
                            $classTdLine  .= $this->getClassAlignement($cellule->getAlign());
                            $renderLignes .= 'class="' . $classTdLine . '" ' . $styleTd . '>';
                            $renderLignes .= '<a href="' . $cellule->getLien() . '">' . $cellule->getIntitule(
                                ) . '</a>';
                            break;
                        case TableConstante::TYPE_CASE_COCHER:
                            $classTdLine  .= $this->getClassAlignement($cellule->getAlign());
                            $renderLignes .= 'class="' . $classTdLine . '" ' . $styleTd . '>';
                            $checked      = '';
                            if ($cellule->getIsChecked()) {
                                $checked = 'checked';
                            }
                            $renderLignes .= '<input name="' . $eltLigne->getId(
                                ) . '" type="checkbox" ' . $checked . '>';
                            break;
                        case TableConstante::TYPE_BOUTON_MODIF:
                            $classTdLine  .= $this->getClassAlignement(TableConstante::ALIGN_CENTER);
                            $renderLignes .= 'class="' . $classTdLine . '" ' . $styleTd . '>';
                            $renderLignes .= '<a href="' . $cellule->getLien() . '" title="Modifier">';
                            $renderLignes .= '<span class="fa fa-edit"></span></a>';
                            break;
                        case TableConstante::TYPE_BOUTON_SUPP:
                            $classTdLine  .= $this->getClassAlignement(TableConstante::ALIGN_CENTER);
                            $renderLignes .= 'class="' . $classTdLine . '" ' . $styleTd . '>';
                            $renderLignes .= '<a href="' . $cellule->getLien() . '" title="Supprimer">';
                            $renderLignes .= '<span class="fa fa-remove"></span></a>';
                            break;
                        case TableConstante::TYPE_BOUTON_VISUALISER:
                            $classTdLine  .= $this->getClassAlignement(TableConstante::ALIGN_CENTER);
                            $renderLignes .= 'class="' . $classTdLine . '" ' . $styleTd . '>';
                            $renderLignes .= '<a href="' . $cellule->getLien() . '" title="Visualiser">';
                            $renderLignes .= '<span class="fa fa-eye"></span></a>';
                            break;
                    }
                    $renderLignes .= '</td>';
                }
                $renderLignes .= '</tr>';
            }
        }

        $render .= $renderEntete . $renderLignes;

        if ($isDragable) {
            //Attention : ne pas ajouter le <tbody> au début, car auto-généré
            $render .= '</tbody>';
        }

        $render .= '</table>';

        if (!empty($divClass)) {
            //Gestion de la div permettant d'afficher un scroll sur le tableau
            $render = '<div class="table-responsive-container"><div class="table-responsive-scroller ' . $divClass . '">' . $render . '</div></div>';
        }else{
            $render = '<div class="table-responsive-container"><div class="table-responsive-scroller">' . $render . '</div></div>';
        }
        return $render;
    }

    /**
     * Permet de retourner la class associée à l'alignement demandé
     *
     * @param string $align alignement souhaité
     *
     * @return string
     */
    private function getClassAlignement($align)
    {
        $classAlign = '';
        switch ($align) {
            case TableConstante::ALIGN_RIGHT:
                $classAlign = 'align-right';
                break;
            case TableConstante::ALIGN_LEFT:
                $classAlign = 'align-left';
                break;
            case TableConstante::ALIGN_CENTER:
                $classAlign = 'align-center';
                break;
        }
        return $classAlign;
    }
}
