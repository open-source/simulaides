<?php
namespace Simulaides\View\Helper;

use DoctrineModule\Form\Element\ObjectSelect;
use Simulaides\Form\Element\Factory\ValeurListeObjectSelectFactory;
use Zend\Form\Element;
use Zend\Form\ElementInterface;

/**
 * Classe FormElement
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class FormElement extends \Zend\Form\View\Helper\FormElement
{
    /**
     * Form element errors helper instance
     *
     * @var FormElementErrors
     */
    protected $elementErrorsHelper;

    /**
     * @param ElementInterface $element
     *
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $renderer = $this->getView();
        if (!method_exists($renderer, 'plugin')) {
            // Bail early if renderer is not pluggable
            return '';
        }

        $render = parent::render($element);

        if (!$element instanceof ObjectSelect && !$element instanceof Element\Select) {
            $render .= $this->getElementErrorsHelper()->render($element);
        }
        return $render;
    }

    /**
     * Retrieve the FormElementErrors helper
     *
     * @return FormElementErrors
     */
    protected function getElementErrorsHelper()
    {
        if ($this->elementErrorsHelper) {
            return $this->elementErrorsHelper;
        }

        if (method_exists($this->view, 'plugin')) {
            $this->elementErrorsHelper = $this->view->plugin('form_element_errors');
        }

        if (!$this->elementErrorsHelper instanceof FormElementErrors) {
            $this->elementErrorsHelper = new FormElementErrors();
        }

        return $this->elementErrorsHelper;
    }
}
