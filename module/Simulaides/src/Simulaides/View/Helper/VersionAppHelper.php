<?php
namespace Simulaides\View\Helper;

use Doctrine\ORM\EntityManager;
use Simulaides\Constante\SessionConstante;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\VersionService;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * Classe VersionAppHelper
 * Aide de vue pour le paramétrage des couleurs
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class VersionAppHelper extends AbstractHelper
{

    /**
     * @var $version
     */
    protected $version;

    /**
     * Retourne le code couleur
     *
     * @param string $typeColor Type de la couleur demandée
     *
     * @return string
     */
    public function __invoke()
    {
        return $this->version ;
    }

    /**
     * @param null|string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }


}
