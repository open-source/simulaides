<?php
namespace Simulaides\View\Helper;

use Zend\Form\View\Helper\AbstractHelper;

/**
 * Classe BandeauSimulation
 * Aide de vue pour faire le bandeau avec le trinagle en haut des simulations
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View
 */
class BandeauSimulation extends AbstractHelper
{

    /**
     * Permet de faire le bandeau de titre des pages de simulation
     *
     * @param String $titre Titre à afficher dans le bandeau
     *
     * @return string
     */
    public function __invoke($titre)
    {
        return $this->renderBandeau($titre);
    }

    /**
     * Retourne le bandeau avec le titre et le triangle au dessous
     *
     * @param String $titre Titre à afficher dans le bandeau
     *
     * @return string
     */
    private function renderBandeau($titre)
    {
        $render = '<div class="simul-titre-N2 col-md-12">';
        $render .= '    <div class="col-xs-1 simul-icone-titre-2">&nbsp;</div>';
        $render .= '    <h1 class="col-md-11 simul-bandeau-titre-n2">' . $titre . '</h1>';
        $render .= '</div>';
        $render .= '<div class="col-md-12">';
        $render .= '    <div class="simul-container-triangle">';
        $render .= '        <div class="simul-triangle simul-triangle-couleur">&nbsp;</div>';
        $render .= '    </div>';
        $render .= '</div>';

        return $render;
    }
}
