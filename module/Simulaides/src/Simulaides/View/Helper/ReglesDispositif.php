<?php

namespace Simulaides\View\Helper;

use Exception;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\DispositifRegle;
use Simulaides\Service\DispositifService;
use Simulaides\Service\RegleService;
use Zend\View\Helper\AbstractHelper;

/**
 * Classe ReglesDispositif
 * l'aide de vue du bandeau de l'application
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\View\Helper
 */
class ReglesDispositif extends AbstractHelper
{
    /** @var  DispositifService */
    private $dispositifService;

    /**
     * Permet de faire l'affichage des règles du dispositif suivant Eligibilité, Montant d'aide, Plafond d'aide
     *
     *
     * @param $dispositif
     * @param $dispositifService
     * @param $regleService
     *
     * @return string
     * @throws Exception
     */
    public function __invoke($dispositif, $dispositifService, $regleService)
    {
        return $this->renderReglesDispositif($dispositif, $dispositifService, $regleService);
    }

    /**
     * Permet de créer l'entête
     *
     * @param Dispositif        $dispositif
     * @param DispositifService $dispositifService
     * @param RegleService      $regleService
     *
     * @return string
     * @throws Exception
     */
    private function renderReglesDispositif($dispositif, $dispositifService, $regleService)
    {
        //Trie de la liste des règles des dispositifs sur le type de règle
        $tabDispositifRegle = $dispositif->getListDispositifRegles();

        /** @var DispositifRegle $regle */
        $render = '';
        foreach ($tabDispositifRegle->getIterator() as $regle) {
            //Cellule du type
            $libelle = $dispositifService->getLibelleTypeRegleDispositif($regle->getType());

            //Cellule de la formule
            $formule   = '';
            $condition = $regle->getConditionRegle();
            if (!empty($condition)) {
                $conditionLitterale = $regleService->getChaineLitterale($condition, false);
                $formule            = "Si " . htmlspecialchars($conditionLitterale, ENT_DISALLOWED) . "<br> Alors ";
            }

            $formule .= htmlspecialchars($regleService->getChaineLitterale($regle->getExpression(), false), ENT_DISALLOWED);

            $render .= '<tr>';
            $render .= '<td><label><b>' . $libelle . '</b></label></td>';
            $render .= '<td>' . str_replace(
                    ['&amp;#8805;', '&amp;#8800;', '&amp;#8804'],
                    ['>=', '!=', '<='],
                    $formule
                ) . '</td>';
            $render .= '</tr>';

        }

        return $render;
    }
}
