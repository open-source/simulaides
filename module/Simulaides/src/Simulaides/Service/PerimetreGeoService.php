<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Repository\PerimetreGeoRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe PerimetreGeoService
 * Services sur les péimètres géographique des dispositifs
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class PerimetreGeoService
{
    /** Nom du service */
    const SERVICE_NAME = 'PerimetreGeoService';

    /**
     * Nom de la classe des périmètres géographique
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\PerimetreGeo';

    /**
     * Repository sur les périmètres géographique
     *
     * @var PerimetreGeoRepository
     */
    private $perimetreGeoRepository;

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager          = $entityManager;
        $this->perimetreGeoRepository = $entityManager->getRepository($this->className);
        $this->serviceLocator         = $serviceLocator;
    }

    /**
     * Retourne la liste des périmètres géographique d'un dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return array
     * @throws \Exception
     */
    public function getPerimetreGeoPourDispositif($idDispositif)
    {
        try {
            return $this->perimetreGeoRepository->getPerimetreGeoPourDispositif($idDispositif);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de supprimer tous les éléments du périmètre d'un dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @throws \Exception
     */
    public function deletePerimetreGeoPourdispositif($idDispositif)
    {
        try {
            //Suppression du périmètre du dispositif
            $this->perimetreGeoRepository->deletePerimetreGeoPourdispositif($idDispositif);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @param $perimetreGeo
     *
     * @throws \Exception
     */
    public function savePerimetreGeo($perimetreGeo)
    {
        try {
            //Sauvegarde du périmètre géo
            $this->perimetreGeoRepository->savePerimetreGeo($perimetreGeo);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
