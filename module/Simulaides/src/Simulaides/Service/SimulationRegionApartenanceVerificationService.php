<?php
namespace Simulaides\Service;
use Simulaides\Domain\Repository\RegionRepository;

/**
 * Classe SimulationRegionApartenanceVerificationService
 *
 * Projet : Alyoskills

 *
 * @author
 * @package   Simulaides\Service
 */
class SimulationRegionApartenanceVerificationService
{
    const SERVICE_NAME = 'SimulationRegionApartenanceVerificationService';

    /** @var  RegionRepository $regionRepository */
    private $regionRepository;

    /**
     * SimulationRegionApartenanceVerificationService constructor.
     *
     * @param RegionRepository $regionRepository
     */
    public function __construct(RegionRepository $regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    /**
     * @return RegionRepository
     */
    public function getRegionRepository()
    {
        return $this->regionRepository;
    }

    /**
     * @param RegionRepository $regionRepository
     */
    public function setRegionRepository($regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    /**
     * @param string $codeCommune
     *
     * @param string $codeRegion
     *
     * @return bool
     */
    public function isSimulationCommuneInRegionCode($codeCommune, $codeRegion)
    {
        return ($codeRegion === $this->getRegionRepository()->getRegionByCodeCommune($codeCommune)->getCode());
    }
}
