<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Constante\RegleVariableConstante;
use Simulaides\Domain\Entity\TrancheRevenu;
use Simulaides\Domain\Repository\TrancheRevenuRepository;
use Simulaides\Domain\Validator\FicheTrancheRevenuValidator;
use Simulaides\Form\TrancheRevenu\FicheTrancheRevenuForm;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Form\FormElementManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe TrancheRevenuService
 * Service permettant de gérer les tranches de revenu
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class TrancheRevenuService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'TrancheRevenuService';

    /**
     * Nom de la classe
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\TrancheRevenu';

    /** @var  TrancheRevenuRepository */
    private $trancheRevenuRepository;

    /**
     * @var FormElementManager
     */
    protected $formElementManager;

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;


    /**
     * @return \Simulaides\Domain\Repository\TrancheRevenuRepository
     */
    private function getTrancheRevenuRepository()
    {
        return $this->trancheRevenuRepository;
    }

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param FormElementManager      $formElementManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormElementManager $formElementManager,
        ServiceLocatorInterface $serviceLocator
    ) {
        $this->entityManager           = $entityManager;
        $this->trancheRevenuRepository = $entityManager->getRepository($this->className);
        $this->formElementManager      = $formElementManager;
        $this->serviceLocator          = $serviceLocator;
    }

    /**
     * Suppression des tranches appartenant au groupe indiqué
     *
     * @param string $codeGroupe Code du groupe
     *
     * @throws \Exception
     */
    public function deleteTrancheRevenuParGroupe($codeGroupe)
    {
        try {
            //Suppression des tranches du groupe
            $this->getTrancheRevenuRepository()->deleteTrancheRevenuParGroupe($codeGroupe);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Suppression d'une tranche de revenu
     *
     * @param TrancheRevenu $trancheRevenu Tranche à supprimer
     *
     * @throws \Exception
     */
    public function deleteTrancheRevenu($trancheRevenu)
    {
        try {
            //Suppression de la tranches de revenu
            $this->getTrancheRevenuRepository()->deleteTrancheRevenu($trancheRevenu);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner la liste des tranches de revenu pur un groupe
     *
     * @param string $codeGroupe Code du groupe de tranche
     *
     * @return array
     * @throws \Exception
     */
    public function getTrancheRevenusParGroupe($codeGroupe)
    {
        try {
            //Récupère la liste des tranches de revenu par groupe
            $tabTrancheRevenu = $this->getTrancheRevenuRepository()->getTrancheRevenusParGroupe($codeGroupe);
            return $tabTrancheRevenu;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le formulaire de la fiche d'une tranche de revenu
     *
     * @param boolean $isModification Indique que le formuaire est en modification
     *
     * @return FicheTrancheRevenuForm
     */
    public function getTrancheRevenuForm($isModification = null)
    {
        /** @var FicheTrancheRevenuForm $trancheRevenuForm */
        $trancheRevenuForm = $this->formElementManager->get('FicheTrancheRevenuForm');
        if ($isModification) {
            $trancheRevenuForm = $this->setTrancheRevenuFormInModification($trancheRevenuForm);
        }
        $hydrator = new DoctrineHydrator($this->getEntityManager(), $this->className);
        $trancheRevenuForm->setHydrator($hydrator);
        $trancheRevenuForm->setInputFilter(new FicheTrancheRevenuValidator());

        return $trancheRevenuForm;
    }

    /**
     * Permet de modifier le formulaire en modification
     *
     * @param FicheTrancheRevenuForm $trancheRevenuForm
     *
     * @return \Simulaides\Form\TrancheRevenu\FicheTrancheRevenuForm
     */
    public function setTrancheRevenuFormInModification($trancheRevenuForm)
    {
        $trancheRevenuForm->get('codeTranche')->setAttribute('readonly', 'readonly');
        return $trancheRevenuForm;
    }

    /**
     * Permet de sauvegarder la tranche de revenu
     *
     * @param TrancheRevenu $trancheRevenu Tranche de revenu à mémoriser
     *
     * @throws \Exception
     */
    public function saveTrancheRevenu($trancheRevenu)
    {
        try {
            $this->getTrancheRevenuRepository()->saveTrancheRevenu($trancheRevenu);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la tanche de revenu
     *
     * @param $codeTranche
     *
     * @return TrancheRevenu
     * @throws \Exception
     */
    public function getTrancheRevenu($codeTranche)
    {
        try {
            return $this->getTrancheRevenuRepository()->getTrancheRevenu($codeTranche);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Récupère le code de tranche pour un groupe de tranche donné
     *
     * @param int    $nbPersonnes       Nb de personnes total dans le foyer
     * @param string $codeGroupeTranche Codede la tranche
     * @param float  $revenusFoyer      Revenu du foyer
     *
     * @return array|string
     * @throws \Exception
     */
    public function getCodeTranchePourNbPersonne($nbPersonnes, $codeGroupeTranche, $revenusFoyer)
    {
        $codeTranche = RegleVariableConstante::VALEUR_TRANCHE_HORS_TRANCHE;
        try {
            $result = $this->getTrancheRevenuRepository()->getCodeTranchePourNbPersonne(
                $nbPersonnes,
                $codeGroupeTranche,
                $revenusFoyer
            );
            if (!empty($result)) {
                $codeTranche = $result[0]['code_tranche'];
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
        return $codeTranche;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
