<?php
namespace Simulaides\Service;

use Simulaides\Domain\Entity\DispositifRegle;
use Simulaides\Domain\Entity\Oblige;
use Simulaides\Domain\SessionObject\Simulation\AideTravauxSimul;
use Simulaides\Domain\SessionObject\Simulation\CoutsSimul;
use Simulaides\Domain\SessionObject\Simulation\DispositifSimul;
use Simulaides\Domain\SessionObject\Simulation\ProjetSimul;
use Simulaides\Domain\SessionObject\Simulation\RegleSimul;
use Simulaides\Logger\LoggerSimulation;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe DispositifSimulService
 * Service sur les dispositifs dans la simulation
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class DispositifSimulService
{
    /** Nom du service */
    const SERVICE_NAME = 'DispositifSimulService';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  DispositifService */
    private $dispositifService;

    /** @var  Logger */
    private $loggerService;

    /**
     * constructeur
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Retourne un tableau des dispositifs candidats à la simulation en fonction du contexte et du projet
     *
     * @param string $modeExecution valeur TEST ou PRODUCTION
     * @param ProjetSimul $projet
     * @param array $tabIdDispositifSelectionnes
     * @param bool $offresCdpSeulement
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getDispositifsCandidats($modeExecution, $projet, $tabIdDispositifSelectionnes, $offresCdpSeulement = false)
    {
        try {
            // Récupération dans la base des dispositifs candidats
            $dispositifsCandidats = $this->chargerDispositifsCandidats(
                $modeExecution,
                $projet,
                $tabIdDispositifSelectionnes,
                $offresCdpSeulement
            );

            // Récupération dans la base des règles des dispositifs candidats
            LoggerSimulation::detail(
                "\n\n Récupération dans la base des règles des dispositifs candidats"
            );
            foreach ($dispositifsCandidats as $dispositif) {
                LoggerSimulation::detail(
                    "\n============================================================================================"
                );    LoggerSimulation::detail(
                    "\n\n Chargement des Regles du dispostif : ".$dispositif->getIntitule()
                );
                $this->chargerReglesDispositif($dispositif);
                LoggerSimulation::detail(
                    "\n\n Chargement des Aides travaux du dispositif"
                );
                $this->chargerAidesTravauxDispositif($projet, $dispositif);
                LoggerSimulation::detail(
                    "\n\n Calcul couts des travaux dispostif"
                );
                $this->calculerCoutsTravauxDispositif($dispositif);
            }
            LoggerSimulation::detail(
                "\n============================================================================================"
            );
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
        return $dispositifsCandidats;
    }

    /**
     * @param DispositifSimul $dispositif
     * @throws \Exception
     */
    public function supprimerTravauxExclus($dispositif)
    {
        $typeDispositif = $dispositif->getType();
        $typesExclusDispositif = $dispositif->getTypesExclus();

        try {
            foreach($dispositif->getAidesTravaux() as $aidesTravaux){
                $dispositifCompatible = true;
                $travaux = $aidesTravaux->getTravaux();

                //Si le type du dispositif figure dans la liste des types exclus par une aide déjà apportée au travaux
                if($travaux->getEstExclu($typeDispositif)){
                    $dispositifCompatible = false;
                }

                //Si un des types d'aide déjà apportées au travaux est incompatible avce le dispositif
                foreach($typesExclusDispositif as $typeExcluDispositif){
                    if($travaux->getEstInclu($typeExcluDispositif)){
                        $dispositifCompatible = false;
                    }
                }

                if(!$dispositifCompatible){
                    $dispositif->removeAideTravaux($aidesTravaux->getCode());
                }
                /*
                var_dump($typeDispositif);
                var_dump($typesExclusTravaux);

                var_dump($typesInclusTravaux);
                var_dump($typesExclusDispositif);

                var_dump($dispositifCompatible);
                */
            }

            //##TODO supprimer l'effet doublon avec l'appel de calculerCoutsTravauxDispositif dans la fonction getDispositifsCandidats
            $this->calculerCoutsTravauxDispositif($dispositif);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }


    /**
     * Charge depuis la bdd les dispositifs répondant au critères de la simulation, à partir de la bdd
     *
     * @param string $modeExecution valeur TEST ou PRODUCTION
     * @param ProjetSimul $projet
     * @param array $tabIdDispositifSelectionnes
     * @param $offresCdpSeulement
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function chargerDispositifsCandidats($modeExecution, $projet, $tabIdDispositifSelectionnes, $offresCdpSeulement = false)
    {
        try {
            $tabResult = $this->getDispositifService()->getDispositifCandidat(
                $projet->getCodeCommune(),
                $projet->getId(),
                $modeExecution,
                $tabIdDispositifSelectionnes,
                $offresCdpSeulement
            );
            // Chargement du tableau des dispositifs candidats
            $dispositifsCandidat = array();
            LoggerSimulation::detail("\n\n Chargement Chargement du tableau des dispositifs candidats " );
            foreach ($tabResult as $row) {

                LoggerSimulation::detail(
                    "\n\n Dispositif : " . $row['id'].'-'.$row['intitule']
                );
                $idDispositif = $row['id'];
                $candidat     = new DispositifSimul($idDispositif, $this->serviceLocator);

                $candidat->setId($row['id']);
                $candidat->setNumOrdre($row['numero_ordre']);
                $candidat->setEtat($row['code_etat']);
                $candidat->setType($row['code_type_dispositif']);
                $candidat->setIntitule($row['intitule']);
                $candidat->setDescriptif($row['descriptif']);
                $candidat->setFinanceur($row['financeur']);
                $candidat->setExclutCEE($row['exclut_cee']);
                $candidat->setExclutAnah($row['exclut_anah']);
                $candidat->setExclutEPCI($row['exclut_epci']);
                $candidat->setExclutRegion($row['exclut_region']);
                $candidat->setExclutCommunal($row['exclut_communal']);
                $candidat->setExclutNational($row['exclut_national']);
                $candidat->setExclutDepartement($row['exclut_departement']);
                $candidat->setExclutCoupDePouce($row['exclut_coup_de_pouce']);
                $candidat->setEvaluerRegle($row['evaluer_regle']);
                $candidat->setDebutValidite($row['debut_validite']);
                $candidat->setSiteWeb($row['site_web']);
                $candidat->setIdDispositifRequis($row['id_dispositif_requis']);

                if ($offresCdpSeulement) {
                    $candidat->setDebutValidite($row['debut_validite']);


                    $oblige = new Oblige();
                    $oblige->setNom($row['obligeNom']);
                    $oblige->setMail($row['obligeMail']);
                    $oblige->setUrl($row['obligeUrl']);
                    $oblige->setTelephone($row['obligeTelephone']);
                    $oblige->setSiren($row['obligeSiren']);
                    $candidat->setOblige($oblige);
                }

                $dispositifsCandidat[$idDispositif] = $candidat;
            }
            LoggerSimulation::detail(
                "\n============================================================================================"
            );
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
        return $dispositifsCandidat;
    }

    /**
     * Charge les règles d'un dispositif
     *
     * @param DispositifSimul $dispositif
     *
     * @throws \Exception
     */
    public function chargerReglesDispositif($dispositif)
    {
        try {
            // Recherches des règles du dispositifs
            /** @var DispositifService $dispositifService */
            $dispositifService = $this->getDispositifService();
            $result            = $dispositifService->getDispositifRegleParIdDispositif($dispositif->getId());

            /** @var DispositifRegle $dispositifRegle */
            foreach ($result as $dispositifRegle) {
                $regle = new RegleSimul($dispositifRegle->getType(), $dispositifRegle->getConditionRegle(
                ), $dispositifRegle->getExpression());
                $dispositif->addRegle($regle);
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Charge les Aides travaux du dispositif concernant des travaux appartiennent au projet
     *
     * @param ProjetSimul     $projet
     * @param DispositifSimul $dispositif
     *
     * @throws \Exception
     */
    public function chargerAidesTravauxDispositif($projet, $dispositif)
    {
        try {
            $tabCodesTravaux = $projet->getCodesTravaux();
            $strCodesTravaux = implode("','", $tabCodesTravaux);
            $strCodesTravaux = "'" . $strCodesTravaux . "'";

            /** @var DispositifTravauxService $dispositifTravauxService */
            $dispositifTravauxService = $this->serviceLocator->get(DispositifTravauxService::SERVICE_NAME);
            $result                   = $dispositifTravauxService->getRegleTravauxPourDispositif(
                $strCodesTravaux,
                $dispositif->getId()
            );

            if (!empty($result)) {
                foreach ($result as $row) {
                    $codeTravaux = $row['code_travaux'];
                    $aideTravaux = $dispositif->getAideTravauxByCode($codeTravaux);

                    // Si l'Aide travaux n'a pas encore été ajouté à la liste des Aides travaux du dispositifs
                    if ($aideTravaux === null) {
                        $aideTravaux = new AideTravauxSimul($codeTravaux);

                        $travaux = $projet->getTravauxByCode(
                            $codeTravaux
                        ); // On couple l'aide travaux à son travaux (du projet)
                        $aideTravaux->setTravaux($travaux);

                        $aideTravaux->setEligibiliteSpecifique($row['eligibilite_specifique']);
                        $dispositif->addAideTravaux($aideTravaux);
                    }
                    // Il y a une règle sur l'Aide travaux.
                    //On passe la ligne de résultat pour récupérer les règles sur l'Aide travaux
                    if ($row['type'] !== null) {
                        $regle = new RegleSimul($row['type'], $row['condition_regle'], $row['expression']);
                        $aideTravaux->addRegle($regle);
                        // Il existe au moins une règle sur un travaux du dispositif alors on le déclare NON forfaitaire
                        $dispositif->setForfaitaire(false);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Calclul les sommes des coûts des travaux pris en charge par le dispositif et appartenant au projet
     *
     * @param DispositifSimul $dispositif
     *
     * @throws \Exception
     */
    public function calculerCoutsTravauxDispositif($dispositif)
    {
        try {
            $coutHtTo  = 0.0;
            $coutTtcTo = 0.0;
            $coutHtMo  = 0.0;
            $coutTtcMo = 0.0;
            $coutHtFo  = 0.0;
            $coutTtcFo = 0.0;

            // Parcours sur les Aides travaux du dispositif
            /** @var AideTravauxSimul $aideTravaux */
            foreach ($dispositif->getAidesTravaux() as $aideTravaux) {

                // récupération de l'objet travaux du projet (ayant le meme code)
                $travaux = $aideTravaux->getTravaux();

                if ($travaux !== null) {

                    $coutHtTo  = $coutHtTo + $travaux->getCoutHtTo();
                    $coutTtcTo = $coutTtcTo + $travaux->getCoutTtcTo();
                    $coutHtMo  = $coutHtMo + $travaux->getCoutHtMo();
                    $coutTtcMo = $coutTtcMo + $travaux->getCoutTtcMo();
                    $coutHtFo  = $coutHtFo + $travaux->getCoutHtFo();
                    $coutTtcFo = $coutTtcFo + $travaux->getCoutTtcFo();
                }
            }
            $couts = new CoutsSimul();
            $couts->setCoutHtTo($coutHtTo);
            $couts->setCoutTtcTo($coutTtcTo);
            $couts->setCoutHtMo($coutHtMo);
            $couts->setCoutTtcMo($coutTtcMo);
            $couts->setCoutHtFo($coutHtFo);
            $couts->setCoutTtcFo($coutTtcFo);

            $dispositif->setCoutsTravaux($couts);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service sur les dispositifs
     * @return array|object|DispositifService
     */
    private function getDispositifService()
    {
        if (empty($this->dispositifService)) {
            $this->dispositifService = $this->serviceLocator->get(DispositifService::SERVICE_NAME);
        }
        return $this->dispositifService;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
