<?php
namespace Simulaides\Service;

use Simulaides\Domain\SessionObject\Simulation\Simulation;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe SimulationService
 * Service de simulation
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class SimulationService
{
    /** Nom du service */
    const SERVICE_NAME = 'SimulationService';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur du service
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Permet de lancerla simulation et de récupérer cette simulation
     *
     * @param int $idProjet Identifiant du projet à simuler
     * @param string $modeExecution Mode d'execution
     * @param array $tabIdDispositifSelectionnes Liste des identifiants de dispositif
     *
     * @param bool $offresCdpSeulement
     * @return Simulation
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSimulation($idProjet, $modeExecution, $tabIdDispositifSelectionnes, $offresCdpSeulement = false)
    {
        try {
            $simulation = new Simulation($modeExecution, $this->serviceLocator);
            $simulation->init($idProjet, $tabIdDispositifSelectionnes, $offresCdpSeulement);

            $evaluateur = EvaluateurService::getEvaluateur();
            $evaluateur->init($simulation);
            $simulation->evaluerSimulation($offresCdpSeulement);

            return $simulation;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
