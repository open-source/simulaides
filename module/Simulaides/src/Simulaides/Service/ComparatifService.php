<?php

namespace Simulaides\Service;

use ArrayObject;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Simulaides\Domain\Entity\Comparatif;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\Repository\ComparatifRepository;
use Simulaides\Domain\Repository\ProjetRepository;
use Simulaides\Form\Comparatif\AccesComparatifForm;
use Simulaides\Tools\Utils;
use Zend\Log\Logger;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\RendererInterface;

/**
 * Classe ComparatifService
 *
 * Projet : ADEME Simul'Aides
 *
 * @author
 */
class ComparatifService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'ComparatifService';

    /**
     * @var ComparatifRepository
     */
    protected $comparatifRepository;

    /**
     * @var Logger
     */
    protected $loggerService;

    /**
     * @var ProjetRepository
     */
    protected $projetRepository;

    /**
     * @var ProjetService
     */
    protected $projetService;

    /**
     * @var RendererInterface
     */
    protected $viewRenderer;

    /**
     * @var AccesComparatifForm
     */
    protected $accesComparatifForm;

    /**
     * @param EntityManagerInterface $entityManager
     * @param Logger                 $loggerService
     * @param ProjetService          $projetService
     * @param RendererInterface      $viewRenderer
     * @param AccesComparatifForm    $accesComparatifForm
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Logger $loggerService,
        ProjetService $projetService,
        RendererInterface $viewRenderer,
        AccesComparatifForm $accesComparatifForm
    ) {
        $this->setEntityManager($entityManager);
        $this->comparatifRepository = $entityManager->getRepository('Simulaides\Domain\Entity\Comparatif');
        $this->projetRepository     = $entityManager->getRepository('Simulaides\Domain\Entity\Projet');
        $this->projetService        = $projetService;
        $this->loggerService        = $loggerService;
        $this->viewRenderer         = $viewRenderer;
        $this->accesComparatifForm  = $accesComparatifForm;
    }

    /**
     * Permet de faire la modification ou l'ajout du comparatif
     *
     * @param int $idComparatif  Identifiant du comparatif
     * @param int $idSimulation1 Identifiant de la simulation 1 du comparatif
     * @param int $idSimulation2 OPTIONNEL - Identifiant de la simulation 2 du comparatif
     * @param int $idSimulation3 OPTIONNEL - Identifiant de la simulation 3 du comparatif
     *
     * @return Comparatif
     * @throws OptimisticLockException
     */
    public function saveComparatif($idComparatif, $idSimulation1, $idSimulation2 = null, $idSimulation3 = null)
    {
        if (empty($idComparatif)) {
            $comparatif = new Comparatif();
            //Affectation du mot de passe
            $comparatif->setMotPasse(Utils::genereMotDePasse());
        } else {
            $comparatif = $this->comparatifRepository->getComparatifById($idComparatif);
        }
        $comparatif->setSimulation1($this->projetRepository->getProjetParId($idSimulation1));
        if (null !== $idSimulation2) {
            $comparatif->setSimulation2($this->projetRepository->getProjetParId($idSimulation2));
        }
        if (null !== $idSimulation3) {
            $comparatif->setSimulation3($this->projetRepository->getProjetParId($idSimulation3));
        }

        //Sauvegarde du projet en base de données
        try {
            $this->comparatifRepository->save($comparatif);
        } catch (OptimisticLockException $e) {
            $this->loggerService->err($e->getMessage());
            throw $e;
        }

        return $comparatif;
    }

    /**
     * Permet de retourner le comparatif pour l'id et le mot de passe
     *
     * @param int    $id         Identifiant du comparatif
     * @param string $motDePasse Mot de passe crypté
     *
     * @return Comparatif
     * @throws NonUniqueResultException
     */
    public function getComparatifByIdEtMdp($id, $motDePasse)
    {
        try {
            return $this->comparatifRepository->getComparatifByIdEtMdp($id, $motDePasse);
        } catch (NonUniqueResultException $e) {
            $this->loggerService->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne les informations résumées d'une simulation
     *
     * @param Comparatif $comparatif
     *
     * @return ViewModel
     */
    public function renderSimulationView(Comparatif $comparatif)
    {
        $model = new ViewModel(
            [
                AccesComparatifForm::CHAMP_NUM_COMPARATIF => $comparatif->getId(),
                AccesComparatifForm::CHAMP_CODE_ACCES     => $comparatif->getMotPasse(),
            ]
        );

        $model->setVariable(
            'resumeSimulations',
            [
                $this->addResumeToView('idSimulation1', $model, $comparatif->getSimulation1()),
                $this->addResumeToView('idSimulation2', $model, $comparatif->getSimulation2()),
                $this->addResumeToView('idSimulation3', $model, $comparatif->getSimulation3())
            ]
        );

        return $model;
    }

    /**
     * Ajoute un résumé à la vue de simulation
     *
     * @param string    $variableName
     * @param ViewModel $model
     * @param Projet    $simulation
     *
     * @return string|null
     */
    private function addResumeToView($variableName, ViewModel $model, Projet $simulation = null)
    {
        if ($simulation !== null) {
            $resume = $this->viewRenderer->render($this->getResumeSimulation($simulation));
            $model->setVariable($variableName, $simulation->getId());
        } else {
            $resume = null;
            $model->setVariable($variableName, null);
        }

        return $resume;
    }

    /**
     * Retourne les informations résumées d'une simulation
     *
     * @param Projet $simulation
     *
     * @return ViewModel
     */
    public function getResumeSimulation($simulation)
    {
        if (null === $simulation) {
            return null;
        }

        //on lance le calcul

        $simulationHistorique = $this->projetService->getSimulationHistorique($simulation);
        $this->projetService->lanceSimulation($simulation->getId(),$simulationHistorique);
        $montantAide          = $simulationHistorique->getMontant();
        $tauxAide             = $simulationHistorique->getTaux();
        $coutsTravaux         = $this->projetService->getCoutsTravaux($montantAide, $tauxAide);
        $montantResteACharge  = $this->projetService->getMontantResteACharge($tauxAide, $montantAide);

        $model = new ViewModel(
            [
                'id'                  => $simulation->getId(),
                'numero'              => $simulation->getNumero(),
                'motPasse'            => $simulation->getMotPasse(),
                'montantTravaux'      => Utils::formatNumberToString($coutsTravaux, 2),
                'montantAide'         => Utils::formatNumberToString($montantAide, 2),
                'montantResteACharge' => Utils::formatNumberToString($montantResteACharge, 2),
                'tauxAide'            => Utils::formatNumberToString($tauxAide,0)
            ]
        );
        $model->setTemplate('simulaides/comparatif/resumer.phtml');

        return $model;
    }

    /**
     * @return AccesComparatifForm
     */
    public function getAndBindFormAccesComparatif()
    {
        $dataAcces = [
            AccesComparatifForm::CHAMP_NUM_COMPARATIF => '',
            AccesComparatifForm::CHAMP_CODE_ACCES     => ''
        ];
        $this->accesComparatifForm->bind(new ArrayObject($dataAcces));

        return $this->accesComparatifForm;
    }

    /**
     * Supprime le lien entre une simulation et son comparatif, supprime le comparatif si nécessaire
     *
     * @param string $idComparatif
     * @param string $idSimulation
     *
     * @return Comparatif
     * @throws OptimisticLockException
     */
    public function deleteSimulationComparatif($idComparatif, $idSimulation)
    {
        $comparatif = $this->comparatifRepository->getComparatifById($idComparatif);

        if ($comparatif->getSimulation2() === null) {
            // Il n'y a qu'une simulation dans le comparatif
            $this->comparatifRepository->delete($comparatif);

            return new Comparatif();
        } else {
            if ($comparatif->getSimulation1()->getId() === (int)$idSimulation) {
                $comparatif->setSimulation1($comparatif->getSimulation2());
                $comparatif->setSimulation2($comparatif->getSimulation3());
            } elseif ($comparatif->getSimulation2()->getId() === (int)$idSimulation) {
                $comparatif->setSimulation2($comparatif->getSimulation3());
            }
            $comparatif->setSimulation3(null);
            $this->comparatifRepository->save($comparatif);

            return $comparatif;
        }
    }
}
