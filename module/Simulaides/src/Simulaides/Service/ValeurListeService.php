<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 20/02/15
 * Time: 09:30
 */

namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\Liste;
use Simulaides\Domain\Entity\ValeurListe;
use Simulaides\Domain\Repository\ValeurListeRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ValeurListeService
 * @package Simulaides\Service
 */
class ValeurListeService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'ValeurListeService';

    /**
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\ValeurListe';

    /** @var  ValeurListeRepository */
    private $valeurListeRepository;

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager         = $entityManager;
        $this->valeurListeRepository = $entityManager->getRepository($this->className);
        $this->serviceLocator        = $serviceLocator;
    }

    /**
     * @return ValeurListeRepository
     */
    private function getValeurListeRepository()
    {
        return $this->valeurListeRepository;
    }

    /**
     * Permet de retourner le texte administrable selon le code
     *
     * @param String $codeTexte Code du texte administrable
     *
     * @return mixed|string
     * @throws \Exception
     */
    public function getTextAdministrable($codeTexte)
    {
        $textAdmin = '';
        try {
            $valeurListe = $this->valeurListeRepository->getTextAdministrable($codeTexte);
            if (!empty($valeurListe)) {
                $textAdmin = $valeurListe->getDescriptif();
                //cas des url non mise en target blanck
                $textAdmin = str_replace('target="_blank"','" target',$textAdmin);
                $textAdmin = str_replace('target','target="_blank"',$textAdmin);
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
        return $textAdmin;
    }

    /**
     * Retourne la valeur selon le code de la liste et le code
     *
     * @param string $codeListe Code de la liste
     * @param string $code      Code
     *
     * @return null|\Simulaides\Domain\Entity\ValeurListe
     * @throws \Exception
     */
    public function getValeurPourCodeListeEtCode($codeListe, $code)
    {
        try {
            return $this->getValeurListeRepository()->getValeurPourCodeListeEtCode($codeListe, $code);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de sauvegarder la valeur d'une valeur liste
     *
     * @param Liste  $liste     Liste
     * @param string $code      Code de la valeur
     * @param string $newValeur Valeur à affecter
     *
     * @throws \Exception
     */
    public function saveValeurListePourCodeListeEtCode($liste, $code, $newValeur)
    {
        try {
            if (!empty($liste)) {
                $valeurListe = $this->getValeurPourCodeListeEtCode($liste->getCode(), $code);
                if (empty($valeurListe)) {
                    $valeurListe = new ValeurListe();
                    $valeurListe->setCode($code);
                    $valeurListe->setCodeListe($liste->getCode());
                    $valeurListe->setListe($liste);
                }
                $valeurListe->setLibelle($newValeur);
                $this->getValeurListeRepository()->saveValeurListe($valeurListe);
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Mémorisation d'une valeurListe
     *
     * @param ValeurListe $valeurListe ValeurListe à mémoriser
     *
     * @throws \Exception
     */
    public function saveValeurListe($valeurListe)
    {
        try {
            $this->getValeurListeRepository()->saveValeurListe($valeurListe);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des valeurs d'un code liste
     *
     * @param string $codeListe Code de la liste
     *
     * @return array
     * @throws \Exception
     */
    public function getListeValuesByCodeListe($codeListe)
    {
        try {
            return $this->getValeurListeRepository()->getListeValuesByCodeListe($codeListe);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner la valeur du coefficient en base
     *
     * @return mixed|null
     */
    public function getCoefficientKwhCumac()
    {
        $valCoef = null;
        //Recherche de la valeur du coefficient
        $valeurListeCoeff = $this->getValeurPourCodeListeEtCode(
            ValeurListeConstante::LISTE_NUM_ADMINISTRABLE,
            ValeurListeConstante::CONVERSION_KWH_EUROS
        );
        if (!empty($valeurListeCoeff)) {
            $libCoeff = $valeurListeCoeff->getLibelle();
            if (!empty($libCoeff)) {
                //Transformation car il s'agit de texte, mais c'est un float
                $valCoef = str_replace(",", ".", $libCoeff);
            }

        }
        return $valCoef;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
    

    /**
     * Retourne le code de l'énergie parente en fonction du code d'énergie principale
     * @param string $codeEnergie
     * @return string
     */
    public function getCodeEnergieParent($codeEnergie)
    {
        switch ($codeEnergie) {
            case ValeurListeConstante::ENERG_RESEAU:
            case ValeurListeConstante::ENERG_GAZ_VILLE:
            case ValeurListeConstante::ENERG_FIOUL:
            case ValeurListeConstante::ENERG_GAZ_BOUTEILLE:
            case ValeurListeConstante::ENERG_GAZ_CITERNE:
            case ValeurListeConstante::ENERG_CHARBON:
            case ValeurListeConstante::ENERG_BOIS_BUCHES:
            case ValeurListeConstante::ENERG_BOIS_GRANULES:
                return ValeurListeConstante::ENERG_PARENT_COMBUSTIBLE;
                break;
            default:
                return ValeurListeConstante::ENERG_PARENT_ELECTRIQUE;

        }
    }
}
