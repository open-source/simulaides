<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\TrancheConstante;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\Repository\PartFiscaleRepository;
use Simulaides\Domain\SessionObject\Simulation\ParametreSimul;
use Simulaides\Domain\SessionObject\Simulation\ProduitSimul;
use Simulaides\Domain\SessionObject\Simulation\ProjetSimul;
use Simulaides\Domain\SessionObject\Simulation\TravauxSimul;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Classe ProjetSimulService
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class ProjetSimulService
{
    /** Nom du service */
    const SERVICE_NAME = 'ProjetSimulService';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * Constructeur
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator  = $serviceLocator;
        $this->entityManager = $entityManager;
    }

    /**
     * Retourne un objet ProjetSimul contenant toutes les données du projet (Situation, travaux, produit, paramètres)
     *
     * @param int $idProjet
     *
     * @return ProjetSimul $projetSimul
     * @throws \Exception
     */
    public function getProjetSimul($idProjet)
    {
        try {
            $projetSimul = new ProjetSimul($idProjet, $this->serviceLocator);

            /** @var ProjetService $projetService */
            $projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
            $projet        = $projetService->getProjetParId($idProjet);

            //Chargement du projet de simulation avec les données du projet en base
            $this->chargeProjetSimulFromProjet($projetSimul, $projet);

            // Chargement des travaux du projet et leurs données
            $this->chargerTravauxDuProjet($projetSimul);

            // Calcul les coûts des travaux du projet
            $projetSimul->calculerCoutsTravaux();

            return $projetSimul;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Chargement du projet de simulation avec les données du projet en base
     *
     * @param ProjetSimul $projetSimul
     * @param Projet $projet
     */
    private function chargeProjetSimulFromProjet($projetSimul, $projet)
    {
        $projetSimul->setNumero($projet->getNumero());
        $projetSimul->setMotPasse($projet->getMotPasse());
        $projetSimul->setCodeCommune($projet->getCodeCommune());
        $zoneClimatique = '';
        $commune        = $projet->getCommune();
        if (!empty($commune)) {
            $departement = $commune->getDepartement();
            if (!empty($departement)) {
                $zoneClimatique = $departement->getZoneClimatique();
            }
        }
        $projetSimul->setZoneClimatique($zoneClimatique);
        $projetSimul->setCodeStatut($projet->getCodeStatut());
        $projetSimul->setNbPartsFiscales($projet->getNbPartsFiscales());
        $projetSimul->setCodeTypeLogement($projet->getCodeTypeLogement());
        $projetSimul->setCodeEnergie($projet->getCodeEnergie());
        $projetSimul->setModeChauffage($projet->getCodeModeChauffage());
        /* @var $valeurListeService ValeurListeService */
        $valeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
        $projetSimul->setCodeEnergieParent($valeurListeService->getCodeEnergieParent($projet->getCodeEnergie()));
        $projetSimul->setRevenus($projet->getRevenus());
        $projetSimul->setNbAdultes($projet->getNbAdultes());
        $projetSimul->setNbPersonnesCharge($projet->getNbPersonnesCharge());
        $projetSimul->setNbEnfantsAlterne($projet->getNbEnfantAlterne());
        $projetSimul->setAnneeConstruction($projet->getAnneeConstruction());
        $projetSimul->setSurfaceHabitable($projet->getSurfaceHabitable());
        $projetSimul->setBbc($projet->getBbc());
        $projetSimul->setBeneficePtz($projet->getBeneficePtz());
        $projetSimul->setPrimoAccession($projet->getPrimoAccession());
        $projetSimul->setSalarieSecteurPrive($projet->getSalarieSecteurPrive());
        //$projetSimul->setMontantBeneficeCi($projet->getMontantBeneficeCi());
        $projetSimul->setMtAideConseiller($projet->getMtAideConseiller());
    }

    /**
     * Récupère et Ajoute les travaux  du projet
     *
     * @param ProjetSimul $projetSimul
     */
    private function chargerTravauxDuProjet($projetSimul)
    {
        /** @var ProduitService $produitSaisieService */
        $produitSaisieService = $this->serviceLocator->get(ProduitService::SERVICE_NAME);
        $result               = $produitSaisieService->getSaisieProduitPourTravauxInfo($projetSimul->getId());

        if (!empty($result)) {
            foreach ($result as $row) {
                $codeTrav = $row['code_trav'];
                $intitule = $row['intitule'];
                /** @var TravauxSimul $travaux */
                $travaux = $projetSimul->getTravauxByCode($codeTrav);

                // Si le travaux n'a pas encore été ajouté à la liste des travaux du projet
                if ($travaux === null) {
                    $travaux = new TravauxSimul($codeTrav, $intitule);
                    $projetSimul->addTravaux($travaux);
                }

                // On passe la surface habitable en argument car elle est équivalente
                //à un paramètre technique qui a été saisi au niveau projet ("Ma situation")
                // elle peut correspondre au nombre d'unité d'un produit du travaux
                $this->chargerProduitDuTravaux($travaux, $row, $projetSimul->getSurfaceHabitable(),
                                               $projetSimul->getCodeCommune());
            }
        }
    }

    /**
     * Récupère et Ajoute un produit à un  travaux à partir d'une ligne
     * de résultats de la requête sur les données saisies pour les travaux
     *
     * @param TravauxSimul $travaux
     * @param              $row
     * @param              $surfaceHabitable
     * param               $codeCommune
     *
     * @throws \Exception
     */
    public function chargerProduitDuTravaux($travaux, $row, $surfaceHabitable,$codeCommune)
    {
        try {
            $codeProduit = $row['code_produit'];
            $produit     = $travaux->getProduitByCode($codeProduit);

            // Si le produit n'a pas encore été ajouté à la liste des produits du travaux
            if ($produit === null) {
                /** @var ProduitService $pdtService */
                $pdtService = $this->serviceLocator->get(ProduitService::SERVICE_NAME);
                $produit    = new ProduitSimul($codeProduit, $pdtService);
                $travaux->addProduit($produit);
            }
            $this->chargerDataProduits($produit, $row, $surfaceHabitable,$codeCommune);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Récupère et affecte les données à un produit à partir
     * d'une ligne de résultats de la requête sur les données saisies pour les travaux
     *
     * @param ProduitSimul $produit
     * @param array        $row
     * @param              $surfaceHabitable
     * param               $codeCommune
     *
     */
    private function chargerDataProduits($produit, $row, $surfaceHabitable,$codeCommune)
    {
        $type     = $row['type'];
        $regleNBU = $row['regle_nbu']; // Donnée qui permet d'acceder à la quantité du produit (nombre d'unités)


        /** @var LocalisationService $localisationService */
        $localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);

        $produit->setOutre_mer($localisationService->estOutreMer($codeCommune));

        // tant que l'on a pas reçu une ligne données ligne ($row)
        //qui permette de caluler le nombre d'unité du produit on refait le traitement
        $nbUnites = $produit->getNombreUnites();
        if (empty($nbUnites)) {

            // si la regle NBU est un numérique c'est qu'elle porte elle même le nombre d'unités
            if (is_numeric($regleNBU)) {
                $produit->setNombreUnites($regleNBU);

                // si la regle NBU contient "SURF_HABITABLE", alors il s'agit de la valeur
                //saisie dans la partie "Ma situation" (parametre de la méthode)
            } else {
                if ($regleNBU == "SURF_HABITABLE") {
                    $produit->setNombreUnites($surfaceHabitable);
                }
            }
        }
        // Cas d'une ligne de données de type Coût
        if ($type == "MONTANT") {
            $typeCout = $row['type_montant'];
            $cout     = $row['cout'];

            if ($cout !== null) {
                switch ($typeCout) {
                    case SessionConstante::COUT_TOTAL:
                        $produit->setCoutTo((float)$cout);
                        break;
                    case SessionConstante::COUT_MO:
                        $produit->setCoutMo((float)$cout);
                        break;
                    case SessionConstante::COUT_FO:
                        $produit->setCoutFo((float)$cout);
                        break;
                }
            }
        }
        // Cas d'une ligne de données de type Paramètre technique
        if ($type == "PARAM_TECH") {

            $codeParam = $row['code_parametre_tech'];
            $parametre = $produit->getParametreByCode($codeParam);

            // Si le parametre n'a pas encore été ajouté à la liste des paramètres du produit
            if ($parametre === null) {
                $parametre = new ParametreSimul($codeParam);
                $produit->addParametre($parametre);
            }
            $parametre->setTypeData($row['type_data']);
            $parametre->setValeur($row['valeur']);

            // tant que l'on a pas reçu une ligne données ligne ($row)
            //qui permette de caluler le nombre d'unité du produit
            // on regarde si le paramètre technique est porteur de cette information
            if (empty($nbUnites) && $regleNBU == $codeParam) {
                $produit->setNombreUnites($row['valeur']);
            }
        }
    }

    /**
     * Récupère et affecte le code de tranche à un projet pour un groupe de tranche donné
     *
     * @param ProjetSimul $projetSimul
     * @param string $groupeTranche
     *
     * @throws \Exception
     */
    public function chargerCodeTranchePourGroupe($projetSimul, $groupeTranche)
    {
        try {
            $codeTranche = $this->getCodeTranche($projetSimul, $groupeTranche);
            $projetSimul->setCodeTranchePourGroupe($groupeTranche, $codeTranche);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    public function getCodeTranche($projet, $groupeTranche) {
        $nbPersonnes = $projet->getNbAdultes() + $projet->getNbPersonnesCharge()
        + $projet->getNbEnfantAlterne();

        /** @var TrancheRevenuService $trancheRevenuService */
        $trancheRevenuService = $this->serviceLocator->get(TrancheRevenuService::SERVICE_NAME);
        $codeTranche          = $trancheRevenuService->getCodeTranchePourNbPersonne(
            $nbPersonnes,
            $groupeTranche,
            $projet->getRevenus()
        );
        return $codeTranche;
    }

    /**
     * Retourne les coordonnées PRIS 
     *
     * @return void
     */
    public function getCoordonneesPris($codeCommune, $codeTranche) {
        $res = [];
        $communeRepo = $this->entityManager->getRepository('Simulaides\Domain\Entity\Commune');
        $codeCommune = $communeRepo->getCommuneParCode($codeCommune)->getCodeInsee();

        $rsm = new ResultSetMappingBuilder($this->entityManager);
        $rsm->addScalarResult('nom', 'nom');
        $rsm->addScalarResult('adresse_1', 'adresse_1');
        $rsm->addScalarResult('adresse_2', 'adresse_2');
        $rsm->addScalarResult('adresse_3', 'adresse_3');
        $rsm->addScalarResult('code_postal', 'code_postal');
        $rsm->addScalarResult('ville', 'ville');
        $rsm->addScalarResult('num_tel_local', 'num_tel_local');
        $rsm->addScalarResult('email', 'email');

        $query = $this->entityManager->createNativeQuery('select tb_pts_contact.nom, tb_pts_contact.adresse_1, tb_pts_contact.adresse_2, tb_pts_contact.adresse_3, tb_pts_contact.code_postal, tb_pts_contact.ville, tb_qualificatif.num_tel_local, tb_qualificatif.email
        from tb_commune
        left join tb_chalandise
        on tb_chalandise.tb_commune_id = tb_commune.id
        left join tb_qualificatif
        on tb_qualificatif.id = tb_chalandise.tb_qualificatif_id     
        left join tb_pts_contact
        on tb_pts_contact.id = tb_qualificatif.tb_pts_contact_id
        where tb_commune.code_insee = :codeCommune and tb_type_qualificatif_id = :typequalif ', $rsm);

        $query->setParameter('codeCommune', $codeCommune);
        if($codeTranche == TrancheConstante::MODESTE
           || $codeTranche == TrancheConstante::TRES_MODESTE
           || $codeTranche == TrancheConstante::MODESTE_IDF
           || $codeTranche == TrancheConstante::TRESMODESTE_IDF) {
            $query->setParameter('typequalif', 2);
        } else {
            $query->setParameter('typequalif', 1);
        }
        //FIXME 
        $result = $query->getResult();
        
        if(count($result) > 0) {
            $res = $result[0];
        }
        return $res;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
