<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 17/02/15
 * Time: 16:03
 */

namespace Simulaides\Service;

use Simulaides\Domain\SessionObject\Localisation;
use Zend\Session\Container;
use Zend\Session\SessionManager;

/**
 * Class LocalisationSessionContainer
 * @package Simulaides\Service
 */
class LocalisationSessionContainer extends Container
{
    /** Nom du service */
    const SERVICE_NAME = 'LocalisationSessionContainer';

    /**
     * @param SessionManager $sessionManager
     */
    public function __construct(SessionManager $sessionManager)
    {
        parent::__construct('localisation', $sessionManager);
        if (!$this->offsetExists('localisation')) {
            $this->localisation = new Localisation();
        }
    }

    /**
     * @param Localisation $localisation
     */
    public function saveLocalisation(Localisation $localisation)
    {
        $this->localisation = $localisation;
    }

    /**
     * @return Localisation
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }
}
