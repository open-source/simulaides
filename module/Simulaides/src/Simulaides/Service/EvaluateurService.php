<?php
namespace Simulaides\Service;

use Simulaides\Constante\RegleVariableConstante;
use Simulaides\Domain\SessionObject\Simulation\Simulation;
use Simulaides\Logger\LoggerSimulation;

/**
 * Classe EvaluateurService
 * Classe d'évaluation des formules
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class EvaluateurService
{
    /** @var null */
    private static $instance = null;

    /** @var  simulation */
    private $simulation;

    /**
     * Méthode qui crée l'unique instance de la classe si elle n'existe pas encore puis la retourne.
     *
     * @return EvaluateurService
     */
    public static function getEvaluateur()
    {

        if (is_null(self::$instance)) {
            self::$instance = new EvaluateurService();
        }
        return self::$instance;
    }

    /**
     * Méthode qui initialise l'évaluateur à partir du tableau des dispositifs et du prjet de la simulation
     *
     * @param Simulation $simulation
     */
    public function init($simulation)
    {
        $this->simulation = $simulation;
    }

    /**
     * Méthode d'évaluation d'une formule
     *
     * @param int    $idDispositifEnEval
     * @param string $codeTravauxEnEval
     * @param string $formule
     *
     * @return boolean|float $resultat
     */
    public function evaluerFormule($idDispositifEnEval, $codeTravauxEnEval, $formule)
    {
        $projet = $this->simulation->getProjet();

        $valeur      = '';
        $lexemes     = explode(" ", $formule);
        $formuleEval = "";
        $formuleEvalTravRequis = "";
        $travRequisEval = false;
        $travauxProjet = [];
        foreach ($lexemes as $lexeme) {

            // Si le lexème est une variable on évalue cette variable
            if (substr($lexeme, 0, 1) == "$") {

                $domaine  = substr($lexeme, 1, 1);
                $variable = $this->extraireVariable($lexeme);
                $index    = $this->extraireIndex($lexeme);           

                switch ($domaine) {
                    case RegleVariableConstante::VARIABLE_PREFIX_FOYER:
                        $valeurTravRequis = $valeur = $projet->evaluerVariableFoyer($variable, $index);
                        break;

                    case RegleVariableConstante::VARIABLE_PREFIX_LOGEMENT:
                        $valeurTravRequis =$valeur = $projet->evaluerVariableLogement($variable);
                        break;

                    case RegleVariableConstante::VARIABLE_PREFIX_DISPOSITIF:
                        $dispositif = $this->simulation->getDispositifById($idDispositifEnEval);
                        if($variable === RegleVariableConstante::VARIABLE_LIT_NB_TOTAL_TRAVAUX) {
                            $valeurTravRequis =$valeur     = count($projet->getTravaux());
                            break;
                        } else if(strpos($variable, "trav_requis") !== false){
                            //des travaux requis sont demandé
                            $travRequisEval = true;
                            if(count($travauxProjet) == 0){
                                //on récupere la liste des travaux
                                foreach($projet->getTravaux() as $travaux){
                                    $travauxProjet[] = $travaux->getCode();
                                }
                            }
                            if(in_array($index,$travauxProjet)){
                                $valeur = true;
                            }else{
                                $valeur = false;
                            }
                            $valeurTravRequis = 'in_array(\''.$index.'\',['.implode(',',$travauxProjet).'])';

                        }else {
                            $valeurTravRequis = $valeur = $dispositif->evaluerVariableDispositif($variable);
                            break;
                        }

                    case RegleVariableConstante::VARIABLE_PREFIX_TRAVAUX:
                        // variables concernant un travaux évaluées directement depuis le travaux en cours d'évaluation
                        if ((strpos($variable, "cout_") !== false)
                            || (strpos($variable, RegleVariableConstante::VARIABLE_LIT_PARAM_TECH) !== false)
                        ) {

                            $travaux = $projet->getTravauxByCode($codeTravauxEnEval);
                            $valeurTravRequis = $valeur  = $travaux->evaluerVariableTravaux($variable, $index);

                            // variables concernant une aide travaux (évaluées au niveau du dispositif en cours)
                        } else {
                            if (strpos($variable, "montant_") !== false) {

                                $dispositif = $this->simulation->getDispositifById($idDispositifEnEval);
                                $valeurTravRequis = $valeur = $dispositif->evaluerVariableAideTravaux($variable,$index);

                                // variables concernant une aide travaux (évaluées au niveau de la simulation)
                            } else {
                                if (strpos($variable, "cumul_") !== false) {

                                    $valeurTravRequis = $valeur = $this->simulation->evaluerVariableSimulation(
                                        $variable,
                                        $index,
                                        $idDispositifEnEval
                                    );

                                    // variables concernant une donnée spécifique.
                                } else {
                                    if (strpos($variable, "_fenetres") !== false) {
                                        $valeurTravRequis = $valeur = $projet->evaluerVariableSpecifique($variable);
                                    }
                                }
                            }
                        }
                        break;
                }
                $valeur = $this->formaterValeurVariable($valeur);
                $valeurTravRequis = $this->formaterValeurVariable($valeurTravRequis);
//                LoggerSimulation::detail("\n\t\t\t\t\t" . $lexeme . ' = ' . $valeur);
                $formuleEval = $formuleEval . $valeur . " ";
                $formuleEvalTravRequis = $formuleEvalTravRequis . $valeurTravRequis . " ";

            } else {
                // si le lexème est une parenthèse ou un opérateur
                $formuleEval = $formuleEval . $lexeme . " ";
                $formuleEvalTravRequis = $formuleEvalTravRequis . $lexeme . " ";
            }
        }   
        if($travRequisEval){
            LoggerSimulation::detail("\n\t\t\t\t\t\t\t= " . $formuleEvalTravRequis);
        }else{
            LoggerSimulation::detail("\n\t\t\t\t\t\t\t= " . $formuleEval);
        }
        $formuleEval = 'return $resultat = ' . $formuleEval . ";";
        $resultat = @eval($formuleEval);
        LoggerSimulation::detail("\n\t\t\t\t  [Resultat = " . $resultat . "]");


        return $resultat;
    }

    /**
     * Méthode qui récupère le nom  d'une variable
     *
     * @param string $lexeme
     *
     * @return string
     */
    public static function extraireVariable($lexeme)
    {

        // La variable n'est pas indexée
        if (strpos($lexeme, "[") === false) {
            $variable = substr($lexeme, 3);
        } else {
            $ouvre    = strpos($lexeme, "[");
            $variable = substr($lexeme, 3, $ouvre - 3);
        }
        return $variable;
    }

    /**
     * Méthode qui récupère l'index des variables indexées (donnée entre [])
     *
     * @param string $lexeme
     *
     * @return string
     */
    public static function extraireIndex($lexeme)
    {

        // La variable n'est pas indexée
        if (strpos($lexeme, "[") === false) {
            $index = null;
        } else {
            $ouvre = strpos($lexeme, "[");
            $ferme = strpos($lexeme, "]");
            $index = substr($lexeme, $ouvre + 1, $ferme - $ouvre - 1);
        }
        return $index;
    }

    /**
     * Méthode qui transforme un booléen en chaine "true", "false" et encadre une chaine avec le caractère "
     *
     * @param boolean $valeur
     *
     * @return string    $chaine
     */
    private function formaterValeurVariable($valeur)
    {

        switch (gettype($valeur)) {

            case "boolean":
                if ($valeur === true) {
                    return "true";
                } else {
                    return "false";
                }
                break;

            case "string":
                return "\"" . $valeur . "\"";
                break;

            default:
                return $valeur;
                break;
        }
    }

    /**
     * @param Simulation $simulation
     */
    public function setSimulation($simulation)
    {
        $this->simulation = $simulation;
    }

    /**
     * @return Simulation
     */
    public function getSimulation()
    {
        return $this->simulation;
    }
}
