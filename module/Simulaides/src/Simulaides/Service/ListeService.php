<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Entity\Liste;
use Simulaides\Domain\Repository\ListeRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ListeService
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class ListeService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'ListeService';

    /**
     * Nom de la classe de l'entité
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\Liste';

    /** @var  ListeRepository */
    private $listeRepository;

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager   = $entityManager;
        $this->listeRepository = $entityManager->getRepository($this->className);
        $this->serviceLocator  = $serviceLocator;
    }

    /**
     * @return ListeRepository
     */
    public function getListeRepository()
    {
        return $this->listeRepository;
    }

    /**
     * Retourne une liste selon son code
     *
     * @param string $code Code de la liste
     *
     * @return null|Liste
     * @throws \Exception
     */
    public function getListeParCode($code)
    {
        try {
            return $this->getListeRepository()->getListeParCode($code);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
