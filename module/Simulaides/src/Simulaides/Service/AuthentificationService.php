<?php

namespace Simulaides\Service;

use Simulaides\Constante\SessionConstante;
use Simulaides\Domain\Entity\Conseiller;
use Simulaides\Domain\Repository\ConseillerRepository;
use Simulaides\Exception\AuthException;
use Simulaides\Form\Authentification\AuthentificationForm;
use Zend\Form\FormElementManager;
use Zend\Log\Logger;

/**
 * Classe AuthentificationService
 * Services pour la gestion de l'authentification du conseiller
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class AuthentificationService extends AbstractEntityService
{

    /** Nom du service */
    const SERVICE_NAME = 'AuthentificationService';

    /**
     * @var FormElementManager
     */
    protected $formElementManager;

    /**
     * @var PrisService
     */
    protected $prisService;
    /**
     * @var SessionContainerService
     */
    protected $session;
    /**
     * @var ConseillerRepository
     */
    protected $conseillerRepository;
    /**
     * @var Logger
     */
    protected $loggerService;

    /**
     * Constructeur du service
     *
     * @param FormElementManager      $formElementManager
     * @param PrisService             $prisService
     * @param SessionContainerService $session
     * @param ConseillerRepository    $conseillerRepository
     * @param Logger                  $loggerService
     */
    public function __construct(
        FormElementManager $formElementManager,
        PrisService $prisService,
        SessionContainerService $session,
        ConseillerRepository $conseillerRepository,
        Logger $loggerService
    ) {
        $this->formElementManager   = $formElementManager;
        $this->prisService          = $prisService;
        $this->session              = $session;
        $this->conseillerRepository = $conseillerRepository;
        $this->loggerService        = $loggerService;
    }

    /**
     * Retourne le formulaire d'authentification
     *
     * @return AuthentificationForm
     */
    public function getAuthentificationForm()
    {
        /** @var AuthentificationForm $form */
        $form = $this->formElementManager->get('AuthentificationForm');

        return $form;
    }

    /**
     * Permet de vérifier la saisie de l'authentification. Retourne le conseiller
     *
     * @param string $login    Login saisi par l'utilisateur
     * @param string $motPasse Mot de passe saisi par l'utilisateur
     *
     * @return \Simulaides\Domain\SessionObject\ConseillerPris
     * @throws AuthException
     *
     */
    public function chekLogin($login, $motPasse)
    {
        /** @var PrisService $userService */
        $uid = $this->prisService->getUserIdentification($login, $motPasse);

        if (!empty($uid)) {
            //Récupération des informations du conseiller
            $conseiller = $this->prisService->getConseiller($uid);

            return $conseiller;
        } else {
            // Echec de l'authentification
            throw new AuthException();
        }
    }

    /**
     * Met à jour la session
     *
     * @param $conseiller
     */
    public function updateSession($conseiller)
    {
        //Le conseiller est dans la base, on met à jour la session
        $this->session->addOffset(SessionConstante::ADMIN_CONNECTEE, true);
        //On mémorise les informations du conseiller
        $this->session->addOffset(SessionConstante::CONSEILLER, $conseiller);
    }

    /**
     * Indique si le conseiller connecté a accepté les CGU
     *
     * @return bool
     */
    public function isCGUAccepte()
    {
        $uid = $this->session->getConseiller()->getIdentifiant();
        /**@var $conseiller Conseiller */
        $conseiller = $this->conseillerRepository->find($uid);
        if ($conseiller) {
            return $conseiller->getCguAccepte();
        }
        return false;
    }

    /**
     * @throws \Exception
     */
    public function setCGUAcctepte()
    {
        $uid = $this->session->getConseiller()->getIdentifiant();
        /**@var $conseiller Conseiller */
        $conseiller = $this->conseillerRepository->find($uid);
        if (!$conseiller) {
            $conseiller = new Conseiller();
            $conseiller->setId($uid);
        }
        $conseiller->setCguAccepte(true);

        try {
            //Sauvegarde du dispositif
            $this->conseillerRepository->save($conseiller);
        } catch (\Exception $e) {
            $this->loggerService->err($e->getMessage());
            throw $e;
        }
    }
}
