<?php

namespace Simulaides\Service;

use ArrayObject;
use Doctrine\ORM\Query\AST\NewObjectExpression;
use Exception;
use Simulaides\Constante\DispositifConstante;
use Simulaides\Constante\GroupeTrancheConstante;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\TableConstante;
use Simulaides\Constante\TrancheConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\Repository\ProjetChoixDispositifRepository;
use Simulaides\Domain\SessionObject\Simulation\AideTravauxResultat;
use Simulaides\Domain\SessionObject\Simulation\DispositifResultat;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Form\Projet\AutreCommentaireForm;
use Simulaides\Form\Projet\AutreDispositifForm;
use Simulaides\Form\Projet\ResultatPDFForm;
use Simulaides\Service\Factory\OffresCeeServiceFactory;
use Simulaides\Tools\Utils;
use Zend\Form\Element\Submit;
use Zend\View\Helper\Url;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\RendererInterface;

/**
 * Classe ResultatViewService
 *
 * Projet : ADEME Simul'Aides
 *
 * @author
 */
class ResultatViewService
{
    /** Nom du service */
    const SERVICE_NAME = 'ResultatViewService';

    /** @var  String */
    public $msgFormPdf;

    /** @var  ResultatPDFForm */
    public $formPdf;

    /**
     * @var SessionContainerService
     */
    protected $sessionContainerService;

    /**
     * @var ProjetService
     */
    protected $projetService;

    /**
     * @var RendererInterface
     */
    protected $viewRenderer;

    /**
     * @var LocalisationService
     */
    protected $localisationService;

    /**
     * @var ValeurListeService
     */
    protected $valeurListeService;

    /**
     * @var ProjetSimulService
     */
    protected $projetSimulService;

    /**
     * @var Url
     */
    protected $urlHelper;

    /**
     * ResultatViewService constructor.
     *
     * @param ProjetService           $projetService
     * @param SessionContainerService $sessionContainerService
     * @param RendererInterface       $viewRenderer
     * @param LocalisationService     $localisationService
     * @param ValeurListeService      $valeurListeService
     * @param ProjetSimulService      $projetSimulService
     * @param Url                     $urlHelper
     */
    public function __construct(
        ProjetService $projetService,
        SessionContainerService $sessionContainerService,
        RendererInterface $viewRenderer,
        LocalisationService $localisationService,
        ValeurListeService $valeurListeService,
        ProjetSimulService $projetSimulService,
        Url $urlHelper
    ) {
        $this->projetService           = $projetService;
        $this->sessionContainerService = $sessionContainerService;
        $this->viewRenderer            = $viewRenderer;
        $this->localisationService     = $localisationService;
        $this->valeurListeService      = $valeurListeService;
        $this->projetSimulService      = $projetSimulService;
        $this->urlHelper               = $urlHelper;
    }

    /**
     * Permet de retourner le model
     *
     * @param Projet  $projet
     * @param string  $parentRoute
     * @param boolean $isComparatif
     * @param OffresCeeService $offreCeeService
     * @return ViewModel
     * @throws Exception
     */
    public function getViewModelResultat($projet, $parentRoute, $offreCeeService,$isComparatif = false)
    {
        //Création de la vue
       $model         = new ViewModel();
        $estConseiller = $this->sessionContainerService->estConseillerConnecte();

        $model->setTemplate('simulaides/projet/resultat-corps.phtml');

        //Ajout des variables
        $model->setVariable('afficheFormAutreDispositif', $this->getValueAfficheFormAutreDispositif($estConseiller, $projet));
        $model->setVariable('afficheFormAutreCommentaire', $this->getValueAfficheFormAutreCommentaire($estConseiller, $projet));
        $model->setVariable('rappelPrecaution', $this->projetService->getTexteRappelPrecaution());

        $codeCommune = $projet->getCodeCommune();
        $idf = $this->localisationService->estDansLaRegion($codeCommune,MainConstante::$LISTE_IDF);
        if($idf){
            $codeTranche = $this->projetSimulService ->getCodeTranche($projet, GroupeTrancheConstante::MPR_IDF);
        }else{
            $codeTranche = $this->projetSimulService ->getCodeTranche($projet, GroupeTrancheConstante::MPR);
        }

        $model->setVariable('codeTranche', $codeTranche);
        $texteMPRChapo = '';
        $logement =  $projet->getLogement()->getCode();
        if($codeTranche == TrancheConstante::TRES_MOD_MPR || $codeTranche == TrancheConstante::TRES_MOD_MPR_I){
            $texteMPRTranche =  $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_RESULTAT_MAPRIMRENOV_TRESMODESTE
            );
            $texteMPRChapo = $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_CHAPO_MAPRIMRENOV_TRES_MODESTE
            );
        }elseif($codeTranche == TrancheConstante::MODESTE_MPR || $codeTranche == TrancheConstante::MODESTE_MPR_IDF){
            $texteMPRTranche =  $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_RESULTAT_MAPRIMRENOV_MODESTE
            );
            $texteMPRChapo = $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_CHAPO_MAPRIMRENOV_MODESTE
            );
        }elseif($codeTranche == TrancheConstante::INTER_MPR || $codeTranche == TrancheConstante::INTER_MPR_IDF){
            $texteMPRTranche =  $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_RESULTAT_MAPRIMRENOV_INTREMEDIAIRE
            );
            $texteMPRChapo = $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_CHAPO_MAPRIMRENOV_INTERMEDIAIRE
            );

        }else{
            $texteMPRTranche =  $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_RESULTAT_MAPRIMRENOV_SUPERIEUR
            );
            $texteMPRChapo = $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_CHAPO_MAPRIMRENOV_SUPERIEUR
            );

        }
        $outre_mer = $this->getLocalisationService()->estOutreMer($projet->getCodeCommune());
        if($outre_mer){
            $texteMPRChapo = '';
        }

        if(count($projet->getTravaux()->toArray()) == 0
           || ($projet->getCodeStatut()!= 'PROP_RES_1' && $projet->getCodeStatut()!= 'PROP_BAIL')){
            $texteMPRChapo  = '';
        }
        $model->setVariable('texteMPRTranche', $texteMPRTranche);
        $model->setVariable('statutDemandeur', $projet->getCodeStatut());

        //Ajout des enfants
        $model->setVariable('resultatMontantAide', $this->viewRenderer->render($this->getViewModelResultatMontantAide($projet, $parentRoute, $estConseiller, $texteMPRChapo)));
        if (!$estConseiller) {
            $model->setVariable('resultatCoordonneesPris', $this->viewRenderer->render($this->getViewModelCoordonneesPris($projet)));
        }

        /* Todo Faire une seule recupération des dispostitifs (fonction  $this->getResultatListeDispotitif
            ($pourConseiller)) et les passer dans les fonctions
            getViewModelResultatDispositif et getViewModelResultatDispositifCEECoupDePouce pour faire l'affichage
        */

        $pourConseiller = $this->sessionContainerService->estConseillerConnecte();
        $listeToutDispositif = $this->getResultatListeDispotitif($pourConseiller);
        $listeDispositif = $listeToutDispositif['AUTRES'];
        $listeDispositifCee = $listeToutDispositif['CDP_PRINCIPAL'];

        $listeDispositifCdp = $listeToutDispositif['CDP'];


        $model->setVariable('resultatDispositif', $this->viewRenderer->render($this->getViewModelResultatDispositif($listeDispositif)));

        $view=array();
        $listeSelectedCdp =array();
        // On boucle sur les dispo Cee
        foreach ($listeDispositifCee as $dispositifCee){
            $viewDispositifCee=array();
            // on boucle sur les travaux du cee
            if($dispositifCee['travaux']){
                foreach ($dispositifCee['travaux'] as $travaux){
                    $listeCdpCeeTravaux=array();
                    //On vérifie si il y'a une offre cdp de sélectionner
                    $selectedOffre=$offreCeeService->getOffresByIdProjetAndCodeTravaux($projet->getId(),$travaux->getCodeTravaux());
                    if (!empty($selectedOffre)){
                        $listeSelectedCdp[$travaux->getCodeTravaux()]=$selectedOffre;
                    }
                    //On met les dispositif cdp du travaux $travaux dans un array
                    foreach ($listeDispositifCdp as $dipositifCdp){
                        foreach ($dipositifCdp['travaux'] as $travauxCdp){
                            if ($travauxCdp->getCodeTravaux()===$travaux->getCodeTravaux()){
                                $listeCdpCeeTravaux[]=$dipositifCdp;

                            }
                        }
                    }
                    //  si ce travaux a pas d'offres CEE
                    if(!empty($listeCdpCeeTravaux)){
                        $viewDispositifCee[$travaux->getCodeTravaux()]=  $this->viewRenderer->render($this->getViewModelResultatDispositifCDPPopUP($listeCdpCeeTravaux,$dispositifCee['id'],$travaux->getCodeTravaux(),$selectedOffre));
                    }
                }
            }
            $idDispositifCee=$dispositifCee['id'];
            $view[$idDispositifCee]=$viewDispositifCee;
        }
        $model->setVariable('resultatDispositifCEECoupdepouce', $this->viewRenderer->render($this->getViewModelResultatDispositifCEECoupDePouce($listeDispositifCee,$listeDispositifCdp,$listeSelectedCdp,$view)));
        $view= new JsonModel($view);
        $model->setVariable('resultatDispositifPopup',$view );
        $model->setVariable('formAutreDispositif', $this->viewRenderer->render($this->getViewModelResultatAutreDispositif($projet, $parentRoute)));
        $model->setVariable('formAutreCommentaire', $this->viewRenderer->render($this->getViewModelResultatAutreCommentaire($projet, $parentRoute)));

        if ($isComparatif) {
            $model->setTerminal(true);
            $model->setVariable('isComparatif', true);
        }

        return $model;
    }

    /**
     * Retourne la vue pour la liste des travaux
     *
     * @param Projet $projet
     *
     * @return ViewModel
     */
    private function getViewModelResultatTravaux($projet)
    {
        $model = new ViewModel();
        //template
        $model->setTemplate('simulaides/projet/resultat-travaux.phtml');
        //ajout des variables
        $liste = $projet->getTravaux();
        $result0 = [];
        $result1 = [];
        $result2 = [];
        $result3 = [];
        $result4 = [];
        foreach ($liste as $travail){
            switch ($travail->getCategorie()) {
                case ValeurListeConstante::CAT_TRAVAUX_ETUDE:
                    $result0[$travail->getNumOrdre()] = $travail;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_ISOLATION:
                    $result1[$travail->getNumOrdre()] = $travail;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_EQUIPEMENT:
                    $result2[$travail->getNumOrdre()] = $travail;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_ENERGIES_RENOUVELABLES:
                    $result3[$travail->getNumOrdre()] = $travail;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_ELECTROMENAGER:
                    $result4[$travail->getNumOrdre()] = $travail;
                    break;
            }
        }
        ksort($result0);
        ksort($result1);
        ksort($result2);
        ksort($result3);
        ksort($result4);
        $result[0] = $result0;
        $result[1] = $result1;
        $result[2] = $result2;
        $result[3] = $result3;
        $result[4] = $result4;

        $travaux = [];
        for($i=0;$i<5;$i++){
            if(is_array($result[$i]) && count($result[$i])){
                $travaux = array_merge($travaux,$result[$i]);
            }
        }


        $model->setVariable('listTravaux', $travaux);
        $model->setVariable('bbc', ($projet->getBbc() ? 'Oui' : 'Non / Je ne sais pas'));
        $isOutreMer = $this->localisationService->estOutreMer($projet->getCodeCommune());
        $model->setVariable('isOutreMer', $isOutreMer);

        return $model;
    }

    /**
     * Retourne la vue pour les infos d'accès à la simulation
     *
     * @param Projet $projet
     *
     * @return ViewModel
     */
    private function getViewModelResultatMontantAide($projet, $parentRoute, $estConseiller, $texteMPRChapo)
    {
        $model = new ViewModel();
        $model->setTemplate('simulaides/projet/resultat-montant.phtml');
        $model->setVariable('formPDF', $this->viewRenderer->render($this->getViewModelResultatPDF($parentRoute)));
        $model->setVariable('travaux', $this->viewRenderer->render($this->getViewModelResultatTravaux($projet)));

        $model->setVariable('texteMPRChapo', $texteMPRChapo);
        $model->setVariable('textAide', $this->valeurListeService->getTextAdministrable(ValeurListeConstante::TEXTE_AIDE_RESULTATS_SIMULATION));
        $model->setVariable('MontantAide', $this->projetService->getResultatMontantAide($estConseiller));
        $model->setVariable('numSimulation', $projet->getNumero());
        $model->setVariable('codeAcces', $projet->getMotPasse());
        $model->setVariable('estConseiller', $estConseiller);
        $model->setVariable('tabCaracteristique', $this->projetService->getResultatCaracteristiques($projet));
        $model->setVariable('prisEmail', $projet->getPrisEmail());
        $model->setVariable('prisNom', $projet->getPrisNom());
        $model->setVariable('prisTel', $projet->getPrisTel());



        return $model;
    }

    /**
     * retourne la vue pour les coordonnées pris
     *
     * @param $projet
     *
     * @return ViewModel
     */
    private function getViewModelCoordonneesPris($projet)
    {
        $model          = new ViewModel();
        $pourConseiller = $this->sessionContainerService->estConseillerConnecte();
        if (!$pourConseiller) {
            $model->setTemplate('simulaides/projet/particulier/resultat-coordonnees-pris.phtml');
        }

        $codeCommune = $projet->getCodeCommune();
        $codeTranche = $this->getCodeTranche($codeCommune, $projet);
        $model->setVariable('coordPris', $this->projetSimulService->getCoordonneesPris($codeCommune, $codeTranche));

        return $model;
    }

    /**
     * @param $codeCommune
     * @param $projet
     *
     * @return mixed
     */
    public function getCodeTranche($codeCommune, $projet)
    {
        $idf = $this->localisationService->estDansLaRegion($codeCommune, MainConstante::$LISTE_IDF);
        if ($idf) {
            $codeTranche = $this->projetSimulService->getCodeTranche($projet, GroupeTrancheConstante::ANAH_IDF);
        } else {
            $codeTranche = $this->projetSimulService->getCodeTranche($projet, GroupeTrancheConstante::ANAH);
        }

        return $codeTranche;
    }

    /**
     * retourne la vue pour le tableau des dispositifs
     *
     * @return ViewModel
     */
    private function getViewModelResultatDispositif($listeDispositif)
    {
        $model          = new ViewModel();
        $pourConseiller = $this->sessionContainerService->estConseillerConnecte();
        if ($pourConseiller) {
            $model->setTemplate('simulaides/projet/conseiller/resultat-dispositif-conseiller.phtml');
        } else {
            $model->setTemplate('simulaides/projet/particulier/resultat-dispositif-particulier.phtml');
        }
        $model->setVariable('listDispositif', $listeDispositif);
        $model->setVariable('textNonEvaluer', "Ce dispositif n'est pas évaluable par SIMUL'AIDES, il vous est fourni à titre d'information. Pour plus de renseignement, merci de vous rapprocher d'un conseiller FAIRE.");

        return $model;
    }

    /**
     * retourne la vue pour le tableau des dispositifs Cdp de la popup
     *
     * @return ViewModel
     */
    private function getViewModelResultatDispositifCDPPopUP($listeDispositif,$idDipositifCee,$codeTravaux,$selectedOffre)
    {

        $pourConseiller = $this->sessionContainerService->estConseillerConnecte();
        $model          = new ViewModel();

        $model->setTemplate('simulaides/projet/particulier/resultat-dispositif-popup-cdp-particulier');

        $model->setVariable('listDispositif', $listeDispositif);
        $model->setVariable('idDispositifCee', $idDipositifCee);
        $model->setVariable('selectedOffre', $selectedOffre);
        $model->setVariable('isConseiller', $pourConseiller);

        $model->setVariable('codeTravaux', $codeTravaux);

        $model->setVariable('textNonEvaluer', "Ce dispositif n'est pas évaluable par simulaides, il vous est fourni à titre d'information. Pour plus de renseignement, merci de vous rapprocher d'un conseiller FAIRE.");

        return $model;
    }
    /**
     * retourne la vue pour le tableau des dispositifs
     *
     * @return ViewModel
     */
    private function getViewModelResultatDispositifCEECoupDePouce($listeDispositif,$listeDispositifCdp,$listeSelectedCdp,$view)
    {
        $model          = new ViewModel();
        $listeDispositifPrincipale = [];
        //montant total du dispositifs principale
        foreach ($listeDispositif as $dispositifCDPPrincipal){
            $montantAideAffiche = 0;
            if($dispositifCDPPrincipal['travaux']){
                foreach ($dispositifCDPPrincipal['travaux'] as $travaux){
                    $travauxCode = $travaux->getCodeTravaux();
                    if(!isset($listeSelectedCdp[$travauxCode])){
                        $montantAideAffiche += $travaux->getMontantAideTo();
                    }else{
                        foreach ($listeDispositifCdp as $dispositifCdp){
                            if($dispositifCdp['id'] == $listeSelectedCdp[$travauxCode]){
                                foreach ($dispositifCdp['travaux'] as $travail){
                                    if($travail->getCodeTravaux() == $travauxCode){
                                        $montantAideAffiche += $travail->getMontantAideTo();
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            $dispositifCDPPrincipal['montantAideAffiche'] = $montantAideAffiche;
            $listeDispositifPrincipale[] = $dispositifCDPPrincipal;
        }


        $pourConseiller = $this->sessionContainerService->estConseillerConnecte();
        if ($pourConseiller) {
            $model->setTemplate('simulaides/projet/conseiller/resultat-dispositif-cee-conseiller.phtml');
        } else {
            $model->setTemplate('simulaides/projet/particulier/resultat-dispositif-cee-particulier.phtml');
        }
        $model->setVariable('listDispositif', $listeDispositifPrincipale);
        $model->setVariable('listeDispositifCdp', $listeDispositifCdp);
        $model->setVariable('listeSelectedCdp', $listeSelectedCdp);
        $model->setVariable('view', $view);
        $model->setVariable('textNonEvaluer', "Ce dispositif n'est pas évaluable par simulaides, il vous est fourni à titre d'information. Pour plus de renseignement, merci de vous rapprocher d'un conseiller FAIRE.");

        return $model;
    }



    /**
     * Retourne les dispositifs et leurs travaux avec les montants
     *
     * @param boolean $pourConseiller Indique que la liste est pour les conseillers
     *
     * @return array
     */
    private function getResultatListeDispotitif($pourConseiller)
    {
        $simulation    = $this->sessionContainerService->getSimulationResultat();
        $tabDispositif = [];
        $estProduction = true;
        if (!empty($simulation)) {
            $tabDispositif = $simulation->getDispositifs();
            $estProduction = ($simulation->getModeExecution() == SimulationConstante::MODE_PRODUCTION);
        }
        $i = 0;
        /** @var DispositifResultat $dispositif */
        $tabEvalOk = [];
        $tabEvalKo = [];
        $tabEvalCeeOk = [];
        $tabEvalCeeKo = [];
        $tabEvalCdpOk =[];
        $tabEvalCdpKo =[];
        $listeDispositif['AUTRES']  = [];
        $listeDispositif['CDP_PRINCIPAL']  = [];
        $listeDispositif['CDP']  = [];

        //TODO CEE-COUP de pouce - separation en retour
        foreach ($tabDispositif as $dispositif) {
            if ($dispositif->getEligibilite() &&
                ($dispositif->getMontant() > 0 || $dispositif->getEvaluerRegle() === false)) {
                $infoDispositif = [];
                $montantArrondi = round($dispositif->getMontant()/10)*10;
                $montant = ($pourConseiller)?Utils::formatNumberToString($dispositif->getMontant(), 2):
                    Utils::formatNumberToString($montantArrondi, 0);
                $infoDispositif['nomDispositif']        = $dispositif->getIntitule();
                $infoDispositif['financeur']            = $dispositif->getFinanceur();
                $infoDispositif['montantDispositif']    = $montant;
                $infoDispositif['descriptifDispositif'] = $dispositif->getDescriptif();
                $infoDispositif['image']                = $dispositif->getImage();
                $infoDispositif['credit']               = $dispositif->getCredit();
                $infoDispositif['DebutValidite']        = $dispositif->getDebutValidite();
                $infoDispositif['tabEntete']            = $this->getResultatEnteteTravaux($pourConseiller);
                $infoDispositif['tabLignes']            = $this->getResultatLignesTravaux(
                    $pourConseiller,
                    $dispositif,
                    $i
                );
                $infoDispositif['siteWeb'] = $dispositif->getSiteWeb();
                $infoDispositif['travaux']       = $dispositif->getAidesTravaux();
                $infoDispositif['id']       = $dispositif->getId();
                $infoDispositif['idPanelTravaux']       = 'idPnl_' . $i;
                $infoDispositif['idLienTravaux']        = 'idLien_' . $i;
                $nonEvaluer                             = !$dispositif->getEvaluerRegle();
                $infoDispositif['nonEvaluer']           = $nonEvaluer;

                if ($nonEvaluer) {
                   if(in_array($infoDispositif['id'] ,explode(',',DispositifConstante::IDS_DISPOSITIF_COUPDEPOUCE))){
                       $infoDispositif['tabLignes']            = $this->getResultatLignesTravauxCdp(
                           $pourConseiller,
                           $dispositif,
                           $i
                       );
                       $tabEvalCeeOk[] = $infoDispositif;

                   } elseif($dispositif->getType() == ValeurListeConstante::TYPE_DISPOSITIF_COUP_DE_POUCE){
                       $infoDispositif['tabLignes']            = $this->getResultatLignesTravauxCdp(
                           $pourConseiller,
                           $dispositif,
                           $i
                       );
                       $tabEvalCdpOk[] = $infoDispositif;
                   }else{
                       $tabEvalKo[] = $infoDispositif;
                   }
                } else {
                    if(in_array($infoDispositif['id'] ,explode(',',DispositifConstante::IDS_DISPOSITIF_COUPDEPOUCE))){
                        $infoDispositif['tabLignes']            = $this->getResultatLignesTravauxCdp(
                            $pourConseiller,
                            $dispositif,
                            $i
                        );
                        $tabEvalCeeKo[] = $infoDispositif;

                    }elseif($dispositif->getType() == ValeurListeConstante::TYPE_DISPOSITIF_COUP_DE_POUCE){
                        $infoDispositif['tabLignes']            = $this->getResultatLignesTravauxCdp(
                            $pourConseiller,
                            $dispositif,
                            $i
                        );
                        $tabEvalCdpKo[] = $infoDispositif;
                    }else
                        {
                        $tabEvalOk[] = $infoDispositif;
                    }
                }

                $i++;
            }
        }

        $listeDispositif['AUTRES']  = array_merge($tabEvalOk, $tabEvalKo);
        $listeDispositif['CDP_PRINCIPAL']  = array_merge($tabEvalCeeOk, $tabEvalCeeKo);
        $listeDispositif['CDP']  = array_merge($tabEvalCdpOk, $tabEvalCdpKo);
        return $listeDispositif;
    }

    /**
     * Retourne l'entête du tableau des travaux d'un dispositif
     *
     * @param boolean $pourConseiller Indique que la liste est pour les conseillers
     *
     * @return array
     */
    private function getResultatEnteteTravaux($pourConseiller)
    {
        $tabEntete = [];
        //Colonne Travaux
        $tabEntete[] = new TableCellule('Travaux');
        //Colonne Critères éligibilité
        $tabEntete[] = new TableCellule("Critères d'éligibilité spécifiques");
        if ($pourConseiller) {
            //Colonne Montant de l'aide
            $tabEntete[] = new TableCellule("Montant de l'aide");
            //Colonne Cout
            $tabEntete[] = new TableCellule('Coût HT');
        }

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau de résultat des travaux du dispositif
     *
     * @param boolean            $pourConseiller   Indique si le tableau est pour l'affichage conseiller
     * @param DispositifResultat $dispositif
     * @param int                $numeroDispositif Numéro d'ordre du dispositif dans l'affichage de la page
     *
     * @return array
     */
    private function getResultatLignesTravaux($pourConseiller, $dispositif, $numeroDispositif)
    {
        $tabLignes       = [];
        $tabLignesResult = [];
        if (!empty($dispositif)) {
            $tabLignes = $dispositif->getAidesTravaux();
        }
        $i = 0;
        /** @var AideTravauxResultat $aideTravaux */
        if($tabLignes){
            foreach ($tabLignes as $aideTravaux) {
                if ($aideTravaux->getMontantAideTo() > 0) {
                    $ligne = new TableLigne();
                    $ligne->setId($numeroDispositif . '_' . $i);

                    //Cellule du travaux
                    if ($pourConseiller) {
                        //CAs du conseiller : lien vers la fiche travaux
                        $link    = $this->urlHelper->__invoke('admin/simulation/travaux', ['action' => 'saisies']);
                        $cellule = new TableCellule($aideTravaux->getIntituleTravaux(), TableConstante::TYPE_LIEN, $link);
                    } else {
                        //Cas du particulier : libellé du travaux
                        $cellule = new TableCellule($aideTravaux->getIntituleTravaux(), TableConstante::TYPE_TEXTE);
                    }
                    $ligne->addCellule($cellule);
                    //Cellule du critères éligibilité
                    $cellule = new TableCellule($aideTravaux->getEligibiliteSpecifique(), TableConstante::TYPE_TEXTE);
                    $ligne->addCellule($cellule);
                    if ($pourConseiller) {
                        //Cellule du montant
                        $cellule = new TableCellule(Utils::formatNumberToString($aideTravaux->getMontantAideTo(), 2) . ' €', TableConstante::TYPE_TEXTE);
                        $ligne->addCellule($cellule);
                        $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                        //Cellule du cout
                        $cellule = new TableCellule(
                            Utils::formatNumberToString($aideTravaux->getCoutTravauxHtTo(), 2) . ' €', TableConstante::TYPE_TEXTE
                        );
                        $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                        $ligne->addCellule($cellule);
                    }
                    //ajout de la ligne à la liste des lignes
                    $tabLignesResult[] = $ligne;
                    $i++;
                }
            }
        }

        return $tabLignesResult;
    }
    /**
     * Retourne les lignes du tableau de résultat des travaux du dispositif
     *
     * @param boolean            $pourConseiller   Indique si le tableau est pour l'affichage conseiller
     * @param DispositifResultat $dispositif
     * @param int                $numeroDispositif Numéro d'ordre du dispositif dans l'affichage de la page
     *
     * @return array
     */
    private function getResultatLignesTravauxCdp($pourConseiller, $dispositif, $numeroDispositif)
    {
        $tabLignes       = [];
        $tabLignesResult = [];
        if (!empty($dispositif)) {
            $tabLignes = $dispositif->getAidesTravaux();
        }
        $i = 0;
        /** @var AideTravauxResultat $aideTravaux */
        if($tabLignes && count($tabLignes)){
            foreach ($tabLignes as $aideTravaux) {
                if ($aideTravaux->getMontantAideTo() > 0) {
                    $ligne = new TableLigne();
                    $ligne->setId($numeroDispositif . '_' . $i);

                    //Cellule du travaux
                    if ($pourConseiller) {
                        //CAs du conseiller : lien vers la fiche travaux
                        $link    = $this->urlHelper->__invoke('admin/simulation/travaux', ['action' => 'saisies']);
                        $cellule = new TableCellule($aideTravaux->getIntituleTravaux(), TableConstante::TYPE_LIEN, $link);
                    } else {
                        //Cas du particulier : libellé du travaux
                        $cellule = new TableCellule($aideTravaux->getIntituleTravaux(), TableConstante::TYPE_TEXTE);
                    }
                    $ligne->addCellule($cellule);
                    //Cellule du critères éligibilité
                    $cellule = new TableCellule($aideTravaux->getEligibiliteSpecifique(), TableConstante::TYPE_TEXTE);
                    $ligne->addCellule($cellule);
                    if ($pourConseiller) {
                        //Cellule du montant
                        $cellule = new TableCellule(Utils::formatNumberToString($aideTravaux->getMontantAideTo(), 2) . ' €', TableConstante::TYPE_TEXTE);
                        $ligne->addCellule($cellule);
                        $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                        //Cellule du cout
                        $cellule = new TableCellule(
                            Utils::formatNumberToString($aideTravaux->getCoutTravauxHtTo(), 2) . ' €', TableConstante::TYPE_TEXTE
                        );
                        $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                        $ligne->addCellule($cellule);
                    }
                    //ajout de la ligne à la liste des lignes
                    $tabLignesResult[$aideTravaux->getCodeTravaux()] = $ligne;
                    $i++;
                }
            }
        }


        return $tabLignesResult;
    }
    /**
     * Retourne la vue pour le formulaire Autre dispositif
     *
     * @param Projet              $projet
     * @param                     $parentRoute
     * @param AutreDispositifForm $form
     * @param string              $msgOK
     *
     * @return ViewModel
     */
    public function getViewModelResultatAutreDispositif(Projet $projet, $parentRoute, $form = null, $msgOK = null)
    {
        $estParticulier = !$this->sessionContainerService->estConseillerConnecte();
        $model          = new ViewModel();
        $model->setTemplate('simulaides/projet/resultat-autre-dispositif.phtml');
        if (empty($form)) {
            $form = $this->getAutreDispositifFormAndBind($projet);
        }
        $this->setDisabledFormAutreDispositif($form, $estParticulier);
        $model->setVariable('form', $form);
        $model->setVariable('disabled', $estParticulier);
        $model->setVariable('msgOK', $msgOK);
        $model->setVariable('urlAction', $this->urlHelper->__invoke($parentRoute . '/simulation/projet', ['action' => 'autreDispositif']));

        return $model;
    }

    /**
     * Permet de récupérer le formulaire et faire le bind
     *
     * @param Projet $projet
     *
     * @return AutreDispositifForm
     */
    public function getAutreDispositifFormAndBind($projet)
    {
        $form = $this->projetService->getAutreDispositifForm();
        $form->bind($projet);

        return $form;
    }

    /**
     * Permet de mettre en inactif les champs du formulaire
     *
     * @param AutreDispositifForm $form
     * @param                     $estParticulier
     */
    private function setDisabledFormAutreDispositif($form, $estParticulier)
    {
        if ($estParticulier) {
            //Particulier : les champs sont inaccessibles
            $form->get('intituleAideConseiller')->setAttribute('readonly', true);
            $form->get('mtAideConseiller')->setAttribute('readonly', true);
            $form->get('descriptifAideConseiller')->setAttribute('readonly', true);
        }
    }

    /**
     * Retourne la vue pour le formulaire Autre dispositif
     *
     * @param Projet                    $projet
     * @param                           $parentRoute
     * @param null|AutreCommentaireForm $form
     * @param string                    $msgOK
     *
     * @return ViewModel
     */
    public function getViewModelResultatAutreCommentaire(Projet $projet, $parentRoute, $form = null, $msgOK = null)
    {
        $estParticulier = !$this->sessionContainerService->estConseillerConnecte();
        $model          = new ViewModel();
        $model->setTemplate('simulaides/projet/resultat-autre-commentaire.phtml');
        if (empty($form)) {
            $form = $this->getAutreCommentaireFormAndBind($projet);
        }
        if ($estParticulier) {
            //Particulier : les champs sont inaccessibles
            $form->get('autreCommentaireConseiller')->setAttribute('readonly', true);
        }
        $model->setVariable('form', $form);
        $model->setVariable('disabled', $estParticulier);
        $model->setVariable('msgOK', $msgOK);
        $model->setVariable(
            'urlAction',
            $this->urlHelper->__invoke($parentRoute . '/simulation/projet', ['action' => 'autreCommentaire'])
        );

        return $model;
    }

    /**
     * Permet de récupérer le formulaire et faire le bind
     *
     * @param Projet $projet
     *
     * @return AutreCommentaireForm
     */
    public function getAutreCommentaireFormAndBind($projet)
    {
        $form = $this->projetService->getAutreCommentaireForm();
        $form->bind($projet);

        return $form;
    }

    /**
     * Permet de retourner le formulaire de PDF
     *
     * @param ResultatPDFForm $form
     * @param null            $msgOk
     *
     * @return ViewModel
     */
    public function getViewModelResultatPDF($parentRoute, $form = null, $msgOk = null)
    {
        $model = new ViewModel();
        $model->setTemplate('simulaides/projet/resultat-pdf.phtml');

        if (empty($form) && empty($this->formPdf)) {
            $form = $this->getPDFFormAndBind($parentRoute);
        } elseif (!empty($this->formPdf)) {
            $form = $this->formPdf;
        }
        if (empty($msgOk) && !empty($this->msgFormPdf)) {
            $msgOk = $this->msgFormPdf;
        }
        $model->setVariable('form', $form);
        $model->setVariable('msgOK', $msgOk);

        return $model;
    }

    /**
     * Retourne le formulaire avec le bind par défaut
     *
     * @return ResultatPDFForm
     */
    public function getPDFFormAndBind($parentRoute)
    {
        $data['email']  = '';
        $data['urlPDF'] = $this->urlHelper->__invoke(
            $parentRoute . '/simulation/projet',
            ['action' => 'generePdf'],
            ['query' => ['PHPSESSID' => $this->sessionContainerService->getManager()->getId()]]
        );
        $form           = $this->projetService->getPDFForm();
        if ($this->sessionContainerService->estJsActif()) {
            /** @var Submit $btnPdfTelecharge */
            // $btnPdfTelecharge = $form->get('telecharger');
            //$btnPdfTelecharge->setAttribute('onclick', 'generePDF(); return false;');
            /** @var Submit $btnPdfEmail */
            $btnPdfEmail = $form->get('envoyer');
            $btnPdfEmail->setAttribute('onclick', 'submitFormGenerePdf(); return false;');
            $btnPdfEmail->setAttribute('id', 'btn-email');
            $btnPdfEmail->setAttribute('disabled', 'true');
        }
        $form->bind(new ArrayObject($data));
        $form->setAttribute('action', $this->urlHelper->__invoke($parentRoute . '/simulation/projet', ['action' => 'envoiMailPDF']));
        $form->setAttribute('name', 'formPDF');
        $form->prepare();

        return $form;
    }

    /**
     * Permet de savoir si le formulaire Autre Dispositif doit être affiché
     *
     * @param        $estConseiller
     * @param Projet $projet
     *
     * @return bool
     */
    private function getValueAfficheFormAutreDispositif($estConseiller, $projet)
    {
        if ($estConseiller) {
            //Pour le conseiller, le formulaire est toujours visible
            $afficheForm = true;
        } else {
            //Pour le particulier, le formulaire est visible si des données existent
            $aideConseiller = $projet->getIntituleAideConseiller();
            $afficheForm    = (!empty($aideConseiller));
        }

        return $afficheForm;
    }

    /**
     * Permet de savoir si le formulaire Autre Commentaire doit être affiché
     *
     * @param        $estConseiller
     * @param Projet $projet
     *
     * @return bool
     */
    private function getValueAfficheFormAutreCommentaire($estConseiller, $projet)
    {
        if ($estConseiller) {
            //Pour le conseiller, le formulaire est toujours visible
            $afficheForm = true;
        } else {
            //Pour le particulier, le formulaire est visible si des données existent
            $commentaireConseiller = $projet->getAutreCommentaireConseiller();
            $afficheForm           = (!empty($commentaireConseiller));
        }

        return $afficheForm;
    }

    /**
     * Permet de récupérer le service sur la localisation
     *
     * @return array|object|LocalisationService
     */
    private function getLocalisationService()
    {
        if (empty($this->localisationService)) {
            $this->localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
        }
        return $this->localisationService;
    }

}
