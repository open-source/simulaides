<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Entity\Groupe;
use Simulaides\Domain\Repository\GroupeRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe GroupeService
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class GroupeService
{
    /** Nom du service */
    const SERVICE_NAME = 'GroupeService';

    /**
     * Nom de la classe des groupes
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\Groupe';

    /** @var  GroupeRepository */
    protected $groupeRepository;

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager    = $entityManager;
        $this->groupeRepository = $entityManager->getRepository($this->className);
        $this->serviceLocator   = $serviceLocator;
    }

    /**
     * @return \Simulaides\Domain\Repository\GroupeRepository
     */
    public function getGroupeRepository()
    {
        return $this->groupeRepository;
    }

    /**
     * Retourne le groupe pour le dispositif dans la région indiquée
     *
     * @param string $codeRegion   Code de la région
     * @param int    $idDispositif Identifiant du dispositif
     *
     * @return mixed
     * @throws \Exception
     */
    public function getGroupeParCodeRegionEtId($codeRegion, $idDispositif)
    {
        try {
            //Récupération du groupe du dispositif dans la région indiquée
            return $this->getGroupeRepository()->getGroupeParCodeRegionEtIdDispositif($codeRegion, $idDispositif);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Supprimer les groupes pour le dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return mixed
     * @throws \Exception
     */
    public function deleteGroupeParIdDispositif($idDispositif)
    {
        try {
            //Récupération des groupes du dispositif
            $tabGroupe = $this->getGroupeRepository()->getGroupeParIdDispositif($idDispositif);
            /** @var Groupe $groupe */
            foreach ($tabGroupe as $groupe) {
                $codeRegion = $groupe->getCodeRegion();
                $ordreSupp  = $groupe->getNumeroOrdre();
                //Suppression du groupe
                $this->groupeRepository->deleteGroupe($groupe);

                //recalcul des ordres dans la région du groupe supprimé
                $this->getGroupeRepository()->modifyOrdreApresSuppression($codeRegion, $ordreSupp);
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de sauvegarder le groupe
     *
     * @param $groupe
     *
     * @throws \Exception
     */
    public function saveGroupe($groupe)
    {
        try {
            //Récupération du groupe du dispositif dans la région indiquée
            $this->getGroupeRepository()->saveGroupe($groupe);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de modifier les ordres des dispositifs du groupe
     * après un déplacement
     *
     * @param $codeRegion
     * @param $idDispositifChanged
     * @param $gap
     * @param $ancienOrdre
     * @param $nouvelOrdre
     *
     * @throws \Exception
     */
    public function changeOrdreAutreGroupe($codeRegion, $idDispositifChanged, $gap, $ancienOrdre, $nouvelOrdre)
    {
        try {
            if ($gap > 0) {
                //le dispositif a été placé après plusieurs autres, on réduit le numéro d'ordre des autres
                $this->getGroupeRepository()->modifyOrdreApresDescente(
                    $codeRegion,
                    $idDispositifChanged,
                    $ancienOrdre,
                    $nouvelOrdre
                );
            } else {
                //le dispositif a été placé avant plusieurs autres, on augmente le numéro d'ordre des autres
                $this->getGroupeRepository()->modifyOrdreApresRemontee(
                    $codeRegion,
                    $idDispositifChanged,
                    $nouvelOrdre,
                    $ancienOrdre
                );
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le max numéro ordre du groupe
     *
     * @param $codeRegion
     *
     * @return mixed
     * @throws \Exception
     */
    public function getMaxOrdrePourCodeRegion($codeRegion)
    {
        $maxOrdreReturn = 0;
        try {
            //Récupération du groupe du dispositif dans la région indiquée
            $maxOrdre = $this->getGroupeRepository()->getMaxOrdrePourCodeRegion($codeRegion);
            if (!empty($maxOrdre)) {
                $maxOrdreReturn = $maxOrdre['maxOrdre'];
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
        return $maxOrdreReturn;
    }

    /**
     * Retourne les groupes du dispositif
     *
     * @param $idDispositif
     * @param $tabRegion
     *
     * @return array
     * @throws \Exception
     */
    public function getGroupePourDispositifNotInTabRegion($idDispositif, $tabRegion)
    {
        try {
            //Retourne les groupes du dispositif
            return $this->getGroupeRepository()->getGroupePourDispositifNotInTabRegion($idDispositif, $tabRegion);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de supprimer un groupe
     *
     * @param Groupe $groupe Groupe à supprimer
     *
     * @throws \Exception
     */
    public function deleteGroupe($groupe)
    {
        try {
            //suppression du groupe
            $this->getGroupeRepository()->deleteGroupe($groupe);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Modifie les numéros d'ordre après une suppression dans un groupe
     *
     * @param string $codeRegion Code de la région du groupe
     * @param int    $ordreSupp  Numéro d'ordre qui a été supprimé dans la région
     *
     * @throws \Exception
     */
    public function modifyOrdreApresSuppression($codeRegion, $ordreSupp)
    {
        try {
            //Modification du groupe
            $this->getGroupeRepository()->modifyOrdreApresSuppression($codeRegion, $ordreSupp);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le groupe en fonction  du dispositif
     *
     * @param $idDispositif
     *
     * @return mixed
     * @throws \Exception
     */
    public function getGroupeParIdDispositif($idDispositif)
    {
        try {
            //sélection des groupes du dispositif
            return $this->getGroupeRepository()->getGroupeParIdDispositif($idDispositif);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
