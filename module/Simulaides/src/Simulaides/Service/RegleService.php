<?php

namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Simulaides\Constante\RegleOperateurConstante;
use Simulaides\Constante\RegleVariableConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\ParametreTech;
use Simulaides\Domain\Entity\RegleCategorie;
use Simulaides\Domain\Entity\RegleVariable;
use Simulaides\Domain\Entity\TrancheGroupe;
use Simulaides\Domain\Entity\TrancheRevenu;
use Simulaides\Domain\Entity\Travaux;
use Simulaides\Domain\Entity\ValeurListe;
use Simulaides\Domain\Repository\RegleCategorieRepository;
use Simulaides\Domain\Repository\RegleVariableRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe RegleService
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class RegleService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'RegleService';

    /**
     * Nom de la classe des catégories
     *
     * @var string
     */
    private $classNameCategorie = 'Simulaides\Domain\Entity\RegleCategorie';

    /**
     * Nom de la classe des variables
     *
     * @var string
     */
    private $classNameVariable = 'Simulaides\Domain\Entity\RegleVariable';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  RegleCategorieRepository */
    private $catgorierepository;

    /** @var  RegleVariableRepository */
    private $variableRepository;

    /** @var  ParametreTechService */
    private $paramTechService;

    /** @var  DispositifTravauxService */
    private $dispositifTravauxService;

    /** @var  TravauxService */
    private $travauxService;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager      = $entityManager;
        $this->catgorierepository = $entityManager->getRepository($this->classNameCategorie);
        $this->variableRepository = $entityManager->getRepository($this->classNameVariable);
        $this->serviceLocator     = $serviceLocator;
    }

    /**
     * Permet de retourner la liste des catégories
     * pour les dispositifs
     *
     * @return array
     * @throws Exception
     */
    public function getListeCategoriePourDispositif()
    {
        try {
            return $this->catgorierepository->getListeCategoriePourDispositif();
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner la liste des catégories
     * pour les travaux
     *
     * @return array
     * @throws Exception
     */
    public function getListeCategoriePourTravaux()
    {
        try {
            return $this->catgorierepository->getListeCategoriePourTravaux();
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de récupérer la liste des variables d'une catégorie
     * avec la liste des valeurs de param pour les variables le nécessitant
     *
     * @param RegleCategorie $categorie
     * @param int            $idDispositifTravaux Renseigné dans le cas des travaux
     *
     * @return array
     * @throws Exception
     */
    public function getListVariablePourCategorie($categorie, $idDispositifTravaux = null)
    {
        $tabVariableCateg = [];
        $gereParamTech    = false;
        try {
            $tabVar = $this->variableRepository->getListVariablePourCategorie($categorie);
            /** @var RegleVariable $regleVariable */
            foreach ($tabVar as $regleVariable) {
                $tabParam  = [];
                $ajouteVar = true;
                $codeVar   = $regleVariable->getCodeVar();
                if ($regleVariable->getAvecListeParam()) {
                    if ($codeVar == RegleVariableConstante::VARIABLE_TRANCHE) {
                        //Tranche de groupe : récupère la liste des groupes de tranche
                        $tabParam = $this->getTabParamPourVariableGroupe();
                    } elseif (in_array($codeVar, RegleVariableConstante::$tabVarTravauxMontantCumul)) {
                        //Montant des aides pour le travaux : recupère la liste des travaux
                        $tabParam = $this->getTabParamPourVariableMontantCumulAide($codeVar);
                    }elseif ($codeVar ==  RegleVariableConstante::VARIABLE_TRAVAUX_REQUIS){
                        //liste des travaux pour travaux requis
                        $tabParam = $this->getTabParamPourVariableMontantCumulAide($codeVar);
                    }
                    $ajouteVar = (!empty($tabParam));
                } elseif ($codeVar == RegleVariableConstante::VARIABLE_PARAM_TECH) {
                    //Cette variable induit la sélection de X variables => traitement ci-dessous
                    $gereParamTech = true;
                    $ajouteVar     = false;
                }
                if ($ajouteVar) {
                    //Si la variable est à ajouter à la liste
                    $tabVariableCateg[] = [
                        'codeVar'    => $regleVariable->getCodeVar(),
                        'libelle'    => $regleVariable->getLibelle(),
                        'avecParam'  => $regleVariable->getAvecListeParam(),
                        'tabParam'   => $tabParam,
                        'avecValeur' => ($regleVariable->getAvecListeValeur() ? 1 : 0),
                        'namePnl'    => $this->getCodePanelPourVariable($regleVariable->getCodeVar())
                    ];
                }
            }

            if ($gereParamTech) {
                //Paramètre technique : récupère la liste des param techniques du travaux
                //chacun de ses paramètres est une variable a ajouter
                $tabVariableCateg = $this->getTabVariablePourParamTech($tabVariableCateg, $idDispositifTravaux);
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

        return $tabVariableCateg;
    }

    /**
     * Permet d'ajouter les variables de paramètres technique
     *
     * @param $tabVariableCateg
     * @param $idDispositifTravaux
     *
     * @return array
     */
    private function getTabVariablePourParamTech($tabVariableCateg, $idDispositifTravaux)
    {
        //Liste des paramètres techniques lié au travaux
        $tabParamTech      = [];
        $estChgeFenetre    = false;
        $dispositifTravaux = $this->getDispositifTravauxService()->getDispositifTravauxParId(
            $idDispositifTravaux
        );
        if (!empty($dispositifTravaux)) {
            $codeTravaux    = $dispositifTravaux->getCodeTravaux();
            $tabParamTech   = $this->getParamTechService()->getListParamTechUtilisableReglePourCodeTravaux(
                $codeTravaux
            );
            $estChgeFenetre = ($codeTravaux == RegleVariableConstante::VARIABLE_CODE_TRAVAUX_CHANGE_FENETRE);
        }
        if ($estChgeFenetre) {
            //Ajout des deux variables sur le nb de fenêtre
            $tabVar = $this->variableRepository->getListVariablePourCodeCategorie(
                RegleVariableConstante::CATEGORIE_TRAVAUX_FENETRE
            );
            /** @var RegleVariable $regleVariable */
            foreach ($tabVar as $regleVariable) {
                $tabVariableCateg[] = [
                    'codeVar'    => $regleVariable->getCodeVar(),
                    'libelle'    => $regleVariable->getLibelle(),
                    'avecParam'  => $regleVariable->getAvecListeParam(),
                    'tabParam'   => null,
                    'avecValeur' => ($regleVariable->getAvecListeValeur() ? 1 : 0),
                    'namePnl'    => $this->getCodePanelPourVariable($regleVariable->getCodeVar())
                ];
            }
        }
        //Ajout des paramètres techniques utilisable dans les règles
        /** @var ParametreTech $paramTech */
        foreach ($tabParamTech as $paramTech) {
            $tabVariableCateg[] = [
                'codeVar'    => RegleVariableConstante::VARIABLE_PARAM_TECH . '[' . $paramTech->getCode() . ']',
                'libelle'    => $paramTech->getLibelle(),
                'avecParam'  => 0,
                'tabParam'   => null,
                'avecValeur' => ($paramTech->getTypeData()=='LISTE')? 1:0,
                'namePnl'    => ($paramTech->getTypeData()=='LISTE')? $this->getCodePanelPourVariable
                (RegleVariableConstante::VARIABLE_PARAM_TECH . '[' . $paramTech->getCode() . ']'):'',
            ];

        }

        return $tabVariableCateg;
    }

    /**
     * Retourne la liste des param pour l'affichage des variables
     * sur le groupe de revenu (bouton dropdown)
     *
     * @return array
     */
    private function getTabParamPourVariableGroupe()
    {
        $tabParam = [];
        //Liste des groupes de tranche de revenu
        $tabTrancheGroup = $this->getTrancheGroupeService()->getTrancheGroupes();

        /** @var TrancheGroupe $trancheGroup */
        foreach ($tabTrancheGroup as $trancheGroup) {
            $codeVar    = RegleVariableConstante::VARIABLE_TRANCHE;
            $codeVar    .= '[' . $trancheGroup->getCodeGroupe() . ']';
            $tabParam[] = [
                'code'    => $trancheGroup->getCodeGroupe(),
                'value'   => $trancheGroup->getIntitule(),
                'namePnl' => $this->getCodePanelPourVariable($codeVar)
            ];
        }

        return $tabParam;
    }

    /**
     * Retourne la liste des param pour l'affichage des variables
     * sur les montants de l'aide et le cumul de l'aide (bouton dropdown)
     *
     * @param $codeVariable
     *
     * @return array
     */
    private function getTabParamPourVariableMontantCumulAide($codeVariable)
    {
        $tabParam = [];
        //Liste des travaux
        $tabTravaux = $this->getTravauxService()->getListTravauxTrieParCategorie();
        //on tri selons les categories suivante : Etude -
        $result[0] = [];
        $result[1] = [];
        $result[2] = [];
        $result[3] = [];
        $result[4] = [];
        foreach ($tabTravaux as $travaux) {
            switch ($travaux->getCategorie()) {
                case ValeurListeConstante::CAT_TRAVAUX_ETUDE:
                    $result[0][] = $travaux;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_ISOLATION:
                    $result[1][] = $travaux;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_EQUIPEMENT:
                    $result[2][] = $travaux;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_ENERGIES_RENOUVELABLES:
                    $result[3][] = $travaux;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_ELECTROMENAGER:
                    $result[4][] = $travaux;
                    break;
            }
        }

        $tabTravaux = [];
        for($i=0;$i<5;$i++){
            $tabTravaux = array_merge($tabTravaux,$result[$i]);
        }

            /** @var Travaux $travaux */
        foreach ($tabTravaux as $travaux) {
            $codeVar    = $codeVariable;
            $codeVar    .= '[' . $travaux->getCode() . ']';
            $tabParam[] = [
                'code'    => $travaux->getCode(),
                'value'   => $travaux->getIntitule(),
                'namePnl' => $this->getCodePanelPourVariable($codeVar)
            ];
        }

        return $tabParam;
    }

    /**
     * Permet de retourner la liste des valeurs pour les variables
     * des règles du dispositif ou du travaux
     *
     * @return array
     * @throws Exception
     */
    public function getListValeurPourVariableEditeur()
    {
        try {
            $tabValeurVariable = [];

            //Tranche de revenu => liste des tanches de revenu du groupe
            $tabTrancheGroup = $this->getTrancheGroupeService()->getTrancheGroupes();
            /** @var TrancheGroupe $trancheGroup */
            foreach ($tabTrancheGroup as $trancheGroup) {
                $tabVal        = [];
                $codeVar       = RegleVariableConstante::VARIABLE_TRANCHE . '[' . $trancheGroup->getCodeGroupe() . ']';
                $tabListValues = $this->getTrancheRevenuService()->getTrancheRevenusParGroupe(
                    $trancheGroup->getCodeGroupe()
                );
                //Ajout de la valeur "Hors tranche"
                $tabVal[] = ['code' => RegleVariableConstante::VALEUR_TRANCHE_HORS_TRANCHE, 'libelle' => 'Hors tranche'];
                /** @var TrancheRevenu $revenu */
                foreach ($tabListValues as $revenu) {
                    $tabVal[] = ['code' => $revenu->getCodeTranche(), 'libelle' => $revenu->getIntitule()];
                }
                $tabValeurVariable[$codeVar]['namePnl']    = $this->getCodePanelPourVariable($codeVar);
                $tabValeurVariable[$codeVar]['listValeur'] = $tabVal;
            }

            //Statut => liste des statuts des demandeurs
            $codeVar                     = RegleVariableConstante::VARIABLE_STATUT;
            $tabValeurVariable[$codeVar] = $this->getOneListValeurPourVariableEditeur(
                $codeVar,
                ValeurListeConstante::LISTE_STATUT
            );

            //Type de logement => liste des types de logement
            $codeVar                     = RegleVariableConstante::VARIABLE_TYPE_LOGEMENT;
            $tabValeurVariable[$codeVar] = $this->getOneListValeurPourVariableEditeur(
                $codeVar,
                ValeurListeConstante::LISTE_TYPE_LOGEMENT
            );

            // Energie de chauffage
            $codeVar                     = RegleVariableConstante::VARIABLE_ENERGIE_CHAUFFAGE;
            $tabValeurVariable[$codeVar] = $this->getOneListValeurPourVariableEditeur(
                $codeVar,
                ValeurListeConstante::LISTE_ENERGIE_CHAUFFAGE
            );
            // Energie de chauffage détaillée
            $codeVar                     = RegleVariableConstante::VARIABLE_ENERGIE_CHAUFFAGE_DETAIL;
            $tabValeurVariable[$codeVar] = $this->getOneListValeurPourVariableEditeur(
                $codeVar,
                ValeurListeConstante::LISTE_ENERGIE_CHAUFFAGE_DETAIL
            );

            // Energie de chauffage détaillée
            $codeVar                     = RegleVariableConstante::VARIABLE_MODE_CHAUFFAGE;
            $tabValeurVariable[$codeVar] = $this->getOneListValeurPourVariableEditeur(
                $codeVar,
                ValeurListeConstante::L_MODE_CHAUFFAGE
            );

            //Statut => liste des statuts des demandeurs
            $codeVar                     = RegleVariableConstante::VARIABLE_TYPE_COMBUSTIBLE;
            $tabValeurVariable[$codeVar] = $this->getOneListValeurPourVariableEditeur(
                $codeVar,
                ValeurListeConstante::LISTE_TYPE_COMBUSTIBLE
            );

            //Statut => liste des statuts des demandeurs
            $codeVar                     = RegleVariableConstante::VARIABLE_TYPE_COMBUSTIBLE_BIOMASSE;
            $tabValeurVariable[$codeVar] = $this->getOneListValeurPourVariableEditeur(
                $codeVar,
                ValeurListeConstante::LISTE_TYPE_COMBUSTIBLE
            );

            return $tabValeurVariable;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne une liste de valeur pour l'éditeur en fonction du code de la variable et du code de la liste
     *
     * @param string $codeVar   Code de la variable
     * @param string $codeListe Code de la liste
     *
     * @return array
     */
    private function getOneListValeurPourVariableEditeur($codeVar, $codeListe)
    {
        $tabListValues = $this->getListeValeurService()->getListeValuesByCodeListe($codeListe);
        $tabVal        = [];
        /** @var ValeurListe $valeurListe */
        foreach ($tabListValues as $valeurListe) {
            $tabVal[] = ['code' => $valeurListe->getCode(), 'libelle' => $valeurListe->getLibelle()];
        }

        return ['namePnl' => $this->getCodePanelPourVariable($codeVar), 'listValeur' => $tabVal];
    }

    /**
     * Permet de retourner la variable selon son code
     *
     * @param $codeVariable
     *
     * @return null|object
     * @throws Exception
     */
    public function getVariablePourCodeVariable($codeVariable)
    {
        try {
            return $this->variableRepository->getVariablePourCodeVariable($codeVariable);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la règle en fonction de la chaine littérale
     *
     * @param string $chaine Chaine littérale
     *
     * @return string
     * @throws InvalidArgumentException|Exception
     */
    public function getRegleFromChaineLitterale($chaine)
    {
        $codeParLibelle = [];

        $listVariableParCategorie = $this->getListVariableParCategorie($this->getListeCategoriePourDispositif());
        foreach ($listVariableParCategorie as $categorie) {
            foreach ($categorie as $variable) {
                if (!empty($variable['tabParam'])) {
                    foreach ($variable['tabParam'] as $param) {
                        $codeParLibelle[$variable['libelle'] . ' ' . $param['value']] = $variable['codeVar'] . '[' . $param['code'] . ']';
                    }
                } else {
                    $codeParLibelle[$variable['libelle']] = $variable['codeVar'];
                }
            }
        }

        $listOperateurCondition = $this->getListOperateurCondition();
        foreach ($listOperateurCondition as $operateurCondition) {
            $codeParLibelle[$operateurCondition['value']] = $operateurCondition['code'];
        }

        $listOperateurExpression = $this->getListOperateurExpression();
        foreach ($listOperateurExpression as $operateurExpression) {
            $codeParLibelle[$operateurExpression['value']] = $operateurExpression['code'];
        }

        $listValeurPourVariableEditeur = $this->getListValeurPourVariableEditeur(); // ????
        foreach ($listValeurPourVariableEditeur as $variable) {
            foreach ($variable['listValeur'] as $valeur) {
                $codeParLibelle[$valeur['libelle']] = $valeur['code'];
            }
        }

        return $this->parseRegleLitterale(trim($chaine), $codeParLibelle);
    }

    /**
     * Permet de retourner une chaine littérale à partir des codes
     *  si $pourJs = true :Les mots sont séparés par des caractères "¤" de façon
     *                     à pouvoir splitter en tableau dans le js
     *
     * @param string  $chaine Chaine de variable/code à traduire
     * @param boolean $pourJs Indique que la chaine va être utilisée dans le js
     *
     * @return string
     * @throws Exception
     */
    public function getChaineLitterale($chaine, $pourJs = true)
    {
        try {
            $tabChaineLitteral = [];
            //Transformation de la chaine en tableau
            $tabChaine   = explode(' ', $chaine);
            $previousVar = '';
            $paramTechWithList = false;
            //Parcours de tous les éléments de la chaine
            foreach ($tabChaine as $elt) {
                if (RegleOperateurConstante::isOperateur($elt)) {
                    //Le code est un opérateur
                    $tabChaineLitteral[] = RegleOperateurConstante::getLabelOperateur($elt);
                } elseif (substr($elt, 0, 1) == '$') {
                    //Le code est une variable
                    $codeVar       = $elt;
                    $posCrochetDeb = strpos($elt, '[');
                    $posCrochetFin = strpos($elt, ']');
                    if ($posCrochetDeb > 0 && $posCrochetFin > 0) {
                        //La variable possède un attribut (format => $nameVar[attr])
                        //Récupération de la variable
                        $codeVar = substr($elt, 0, $posCrochetDeb);
                        //Récupération de l'attribut
                        $size            = ($posCrochetFin - $posCrochetDeb - 1);
                        $attribut        = substr($elt, $posCrochetDeb + 1, $size);
                        $libelleVariable = $this->getLibelleVariableSelonAttribut($codeVar, $attribut);
                        //cas particulier si $attribut
                        if($attribut == "TYPE_COMBUSTIBLE" || $attribut == "TYPE_COMBUSTIBLE_BIOMASSE"){
                            $paramTechWithList = $attribut;
                        }
                    } else {
                        //La variable ne possède pas d'attribut, recherche du libellé
                        /** @var RegleVariable $variable */
                        $variable        = $this->getVariablePourCodeVariable($codeVar);
                        $libelleVariable = $variable->getLibelle();
                    }
                    $tabChaineLitteral[] = $libelleVariable;
                    $previousVar         = $codeVar;
                } else {
                    //L'élément est une valeur
                    $tabChaineLitteral[] = $this->getValeurLitterale($elt, $previousVar,$paramTechWithList);
                    $paramTechWithList = false;
                }
            }
            if ($pourJs) {
                //Pour l'utilisation dans le js, on ajoute un caractère permettant le split en tableau
                $chaineLitterale = implode('§', $tabChaineLitteral);
            } else {
                //Pour l'affichage direct, on ajoute un espace entre les mots
                $chaineLitterale = implode(' ', $tabChaineLitteral);
            }

            return $chaineLitterale;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le libellé de la variable avec l'attribut
     *
     * @param $codeVar
     * @param $attribut
     *
     * @return string
     */
    private function getLibelleVariableSelonAttribut($codeVar, $attribut)
    {
        $libelleAttribut = '';
        $libelleVariable = '';

        if ($codeVar == RegleVariableConstante::VARIABLE_TRANCHE) {
            //L'attribut est un code de groupe de tranche de revenu
            $groupeTranche = $this->getTrancheGroupeService()->getTrancheGroupe($attribut);
            if (!empty($groupeTranche)) {
                $libelleAttribut = ' ' . $groupeTranche->getIntitule();
            }
            //Récupération de la variable pour avoir son libellé
            /** @var RegleVariable $variable */
            $variable        = $this->getVariablePourCodeVariable($codeVar);
            $libelleVariable = $variable->getLibelle() . $libelleAttribut;
        } elseif ($codeVar == RegleVariableConstante::VARIABLE_PARAM_TECH) {
            //L'attribut est un code de paramètre technique
            $paramTech = $this->getParamTechService()->getParametreTechParCode($attribut);
            if (!empty($paramTech)) {
                $libelleVariable = $paramTech->getLibelle();
            }
        } elseif ($codeVar == RegleVariableConstante::VARIABLE_TRAVAUX_MONTANT_MO
                  || $codeVar == RegleVariableConstante::VARIABLE_TRAVAUX_MONTANT_FO
                  || $codeVar == RegleVariableConstante::VARIABLE_TRAVAUX_MONTANT_TOTAL
                  || $codeVar == RegleVariableConstante::VARIABLE_TRAVAUX_CUMUL_FO
                  || $codeVar == RegleVariableConstante::VARIABLE_TRAVAUX_CUMUL_MO
                  || $codeVar == RegleVariableConstante::VARIABLE_TRAVAUX_CUMUL_TOTAL
                  || $codeVar == RegleVariableConstante::VARIABLE_TRAVAUX_REQUIS
        ) {
            //L'attribut est un montant des aides du travaux
            $variable = $this->getVariablePourCodeVariable($codeVar);
            $travaux  = $this->getTravauxService()->getTravauxByCode($attribut);
            if (!empty($travaux)) {
                $libelleAttribut = $travaux->getIntitule();
            }
            $libelleVariable = $variable->getLibelle() . ' ' . $libelleAttribut;
        }

        return $libelleVariable;
    }

    /**
     * Permet de retourner la valeur littérale de la valeur
     *
     * @param string $elt         Valeur de la formule
     * @param string $previousVar Variable utilisée juste avant
     *
     * @return string
     */
    private function getValeurLitterale($elt, $previousVar,$paramTechWithList)
    {
        $valeur = str_replace(".", ",", $elt);
        if ($previousVar == RegleVariableConstante::VARIABLE_TRANCHE) {
            //La variable qui précède est un groupe de tranche, on suppose que la valeur est une des tranches
            $trancheRevenu = $this->getTrancheRevenuService()->getTrancheRevenu(str_replace('"', '', $elt));
            if (!empty($trancheRevenu)) {
                $valeur = '"' . $trancheRevenu->getIntitule() . '"';
            }elseif(str_replace('"', '',$valeur) == "HORS_TRANCHE"){
                $valeur = '"Hors tranche"';
            }
        } elseif ($previousVar == RegleVariableConstante::VARIABLE_STATUT) {
            //La variable qui précède est un statut , on suppose que la valeur est un des statuts
            $valeurListe = $this->getListeValeurService()->getValeurPourCodeListeEtCode(
                ValeurListeConstante::LISTE_STATUT,
                str_replace('"', '', $elt)
            );
            if (!empty($valeurListe)) {
                $valeur = '"' . $valeurListe->getLibelle() . '"';
            }
        } elseif ($previousVar == RegleVariableConstante::VARIABLE_ENERGIE_CHAUFFAGE_DETAIL) {
            //La variable qui précède est un type de logement , on suppose que la valeur est un des types
            $valeurListe = $this->getListeValeurService()->getValeurPourCodeListeEtCode(
                ValeurListeConstante::LISTE_ENERGIE_CHAUFFAGE_DETAIL,
                str_replace('"', '', $elt)
            );
            if (!empty($valeurListe)) {
                $valeur = '"' . $valeurListe->getLibelle() . '"';
            }
        } elseif ($previousVar == RegleVariableConstante::VARIABLE_TYPE_LOGEMENT) {
            //La variable qui précède est un type de logement , on suppose que la valeur est un des types
            $valeurListe = $this->getListeValeurService()->getValeurPourCodeListeEtCode(
                ValeurListeConstante::LISTE_TYPE_LOGEMENT,
                str_replace('"', '', $elt)
            );
            if (!empty($valeurListe)) {
                $valeur = '"' . $valeurListe->getLibelle() . '"';
            }
        } elseif ($previousVar == RegleVariableConstante::VARIABLE_MODE_CHAUFFAGE) {
            //La variable qui précède est un type de logement , on suppose que la valeur est un des types
            $valeurListe = $this->getListeValeurService()->getValeurPourCodeListeEtCode(
                ValeurListeConstante::L_MODE_CHAUFFAGE,
                str_replace('"', '', $elt)
            );
            if (!empty($valeurListe)) {
                $valeur = '"' . $valeurListe->getLibelle() . '"';
            }
        }elseif($paramTechWithList && $previousVar == RegleVariableConstante::VARIABLE_PARAM_TECH){
            $paramTech = $this->getParamTechService()->getParametreTechParCode($paramTechWithList);
            $valeurListe = $this->getListeValeurService()->getValeurPourCodeListeEtCode(
                $paramTech->getCodeListe(),
                str_replace('"', '', $elt)
            );
            if (!empty($valeurListe)) {
                $valeur = '"' . $valeurListe->getLibelle() . '"';
            }
        }

        return $valeur;
    }

    /**
     * Retourne l'id du panel en supprimant les caractères
     * qui empêche l'utilisation de l'identification jQuery
     *
     * @param $codeVar
     *
     * @return mixed
     */
    private function getCodePanelPourVariable($codeVar)
    {
        $codeVar = str_replace('$', '', $codeVar);
        $codeVar = str_replace('[', '', $codeVar);
        $codeVar = str_replace(']', '', $codeVar);
        $codeVar = str_replace('.', '-', $codeVar);

        return $codeVar;
    }

    /**
     * Retourne le service des tranches de groupe
     *
     * @return array|TrancheGroupeService
     */
    private function getTrancheGroupeService()
    {
        return $this->serviceLocator->get(TrancheGroupeService::SERVICE_NAME);
    }

    /**
     * Retourne le service des tranches de revenu
     *
     * @return array|TrancheRevenuService
     */
    private function getTrancheRevenuService()
    {
        return $this->serviceLocator->get(TrancheRevenuService::SERVICE_NAME);
    }

    /**
     * Retourne le service des listes de valeur
     *
     * @return array|ValeurListeService
     */
    private function getListeValeurService()
    {
        return $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
    }

    /**
     * Retourne le service des listes de valeur
     *
     * @return array|ParametreTechService
     */
    private function getParamTechService()
    {
        if (empty($this->paramTechService)) {
            $this->paramTechService = $this->serviceLocator->get(ParametreTechService::SERVICE_NAME);
        }

        return $this->paramTechService;
    }

    /**
     * Retourne le service des travaux dans le dispositif
     *
     * @return array|DispositifTravauxService
     */
    private function getDispositifTravauxService()
    {
        if (empty($this->dispositifTravauxService)) {
            $this->dispositifTravauxService = $this->serviceLocator->get(DispositifTravauxService::SERVICE_NAME);
        }

        return $this->dispositifTravauxService;
    }

    /**
     * Retourne le service des travaux
     *
     * @return array|TravauxService
     */
    private function getTravauxService()
    {
        if (empty($this->travauxService)) {
            $this->travauxService = $this->serviceLocator->get(TravauxService::SERVICE_NAME);
        }

        return $this->travauxService;
    }

    /**
     * Retourne la liste des variables par catégorie
     *
     * @param array $listCategorie Liste des objets RegleCategorie
     *
     * @return array
     * @throws Exception
     */
    public function getListVariableParCategorie($listCategorie)
    {
        try {
            $tabVariable = [];

            /** @var $categorie RegleCategorie */
            foreach ($listCategorie as $categorie) {
                $tabVariable[$categorie->getCode()] = $this->getListVariablePourCategorie($categorie);
            }

            return $tabVariable;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des variables par catégorie pour les travaux
     * La liste des catégorie est mise à jour le cas échéant
     *   -> si aucun param à mettre dans la catégorie, alors elle est supprimée de la liste
     *
     * @param int   $idDispositifTravaux Identifiant du travaux dans le dispositif
     * @param array $listCategorie       Liste des objets RegleCategorie
     *
     * @return array
     * @throws Exception
     */
    public function getListVariableParCategoriePourTravaux($idDispositifTravaux, &$listCategorie)
    {
        try {
            $tabVariable         = [];
            $listCategorieFinale = [];

            /** @var $categorie RegleCategorie */
            foreach ($listCategorie as $categorie) {
                //Sélection des paramètres de la catégorie
                $listVariable = $this->getListVariablePourCategorie(
                    $categorie,
                    $idDispositifTravaux
                );
                if (!empty($listVariable)) {
                    //Il existe au moins un paramètre à ajouter à la catégorie
                    $tabVariable[$categorie->getCode()] = $listVariable;
                    $listCategorieFinale[]              = $categorie;
                }
            }

            //Réaffectation de la liste finalement nécessaire des catégories
            $listCategorie = $listCategorieFinale;

            return $tabVariable;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des opérateurs de la condition
     *
     * @return array
     */
    public function getListOperateurCondition()
    {
        $tabOperateur = [];

        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_PARANTHESE_OUVRE;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_PARANTHESE_FERME;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_PLUS;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_MOINS;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_MULTIPLIE;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_DIVISE;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_EGAL;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_DIFFERENT;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_INF_EGAL;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_INF;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_SUP_EGAL;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_SUP;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_ET;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_OU;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_NON;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_VRAI;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_FAUX;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];

        return $tabOperateur;
    }

    /**
     * Retourne la liste des opérateurs de l'expression
     *
     * @return array
     */
    public function getListOperateurExpression()
    {
        $tabOperateur = [];

        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_PARANTHESE_OUVRE;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_PARANTHESE_FERME;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_PLUS;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_MOINS;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_MULTIPLIE;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];
        $codeOperateur  = RegleOperateurConstante::DISPOSITIF_OP_DIVISE;
        $tabOperateur[] = [
            'code'  => $codeOperateur,
            'value' => RegleOperateurConstante::getLabelOperateur($codeOperateur)
        ];

        return $tabOperateur;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }

        return $this->loggerService;
    }

    /**
     * Parse une chaine de libellés correspondant à une règle et la transforme en règle
     *
     * @param string $chaine         Chaine à transformer
     * @param array  $codeParLibelle Tableau des libellés => codes
     *
     * @return string
     * @throws InvalidArgumentException
     */
    private function parseRegleLitterale($chaine, array $codeParLibelle)
    {
        $regle = '';
        while ($chaine !== '') {
            $isMatched = false;
            foreach ($codeParLibelle as $libelle => $code) {
                if (0 === strpos($chaine, $libelle)) {
                    $chaine    = trim(substr($chaine, strlen($libelle)));
                    $regle     .= ' ' . $code;
                    $isMatched = true;
                } else {
                    $quotedLibelle = '"' . $libelle . '"';
                    if (0 === strpos($chaine, $quotedLibelle)) {
                        $chaine    = trim(substr($chaine, strlen($quotedLibelle)));
                        $regle     .= ' "' . $code . '"';
                        $isMatched = true;
                    }
                }
            }
            $chaine = trim(preg_replace_callback(
                '/^\d+/',
                static function ($match) use (&$regle, &$isMatched) {
                    $regle     .= ' ' . $match[0];
                    $isMatched = true;

                    return '';
                },
                $chaine
            ));

            if (!$isMatched && $chaine !== '') {
                throw new InvalidArgumentException('Veuillez vérifier les expressions suivantes car elles ne sont pas conformes : ' . $chaine);
            }
        }

        return $regle;
    }
}
