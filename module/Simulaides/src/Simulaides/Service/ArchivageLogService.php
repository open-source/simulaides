<?php
namespace Simulaides\Service;

use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ArchivageLogService
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class ArchivageLogService
{
    /** Nom du service */
    const SERVICE_NAME = 'ArchivageLogService';

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Permet de faire l'archivage
     */
    public function archiveLog()
    {
        try {
            //Récupération du fichier de log
            $tabConfig          = $this->serviceLocator->get('ApplicationConfig');
            $tabLog             = $tabConfig['log'];
            $fileNameFichierLog = $tabLog['default_logfile'];

            if (file_exists($fileNameFichierLog)) {
                //Vérification de la taille du fichier de log
                $tailleFichierLog = filesize($fileNameFichierLog);

                if ($tailleFichierLog > 5000) {
                    //Archivage du fichier
                    $zipFile            = new \ZipArchive();
                    $dateJour           = new \DateTime('now');
                    $fileNameFichierZip = $tabLog['dir_archive'] . date_format($dateJour, 'y_m_d') . '.zip';
                    $this->verifieRepertoireArchive($tabLog['dir_archive']);
                    if ($zipFile->open($fileNameFichierZip, \ZipArchive::CREATE | \ZipArchive::OVERWRITE) === true) {
                        //Ajout du fichie de log dans l'archive
                        $zipFile->addFile($fileNameFichierLog, 'application.log');
                        $zipFile->close();
                        //Suppression du fichier de log
                        unlink($fileNameFichierLog);
                    } else {
                        $this->getLoggerService()->err("Impossible de créer l'archive du fichier de log");
                    }
                }
            }
        } catch (\Exception $e) {
            $this->getLoggerService()->err($e->getMessage());
        }
    }

    /**
     * Vérifie que le répertoire d'archivage est présent
     * Sinon, création du répertoire
     *
     * @param string $nomRepArchive Nom du répertoire
     */
    private function verifieRepertoireArchive($nomRepArchive)
    {
        if (!is_dir($nomRepArchive)) {
            mkdir($nomRepArchive);
        }
    }

    /**
     * Retourne le service de logger
     *
     * @return array|object|Logger
     */
    private function getLoggerService()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
