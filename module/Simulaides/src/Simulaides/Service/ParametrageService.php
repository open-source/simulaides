<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Entity\Produit;
use Simulaides\Domain\Validator\ReferentielPrixParamValidator;
use Simulaides\Domain\Validator\RefPrixAideCEEValidator;
use Simulaides\Form\Contenu\ContenuParamForm;
use Simulaides\Form\ReferentielProduit\ReferentielPrixParamForm;
use Simulaides\Form\ReferentielProduit\ReferentielProduitForm;
use Simulaides\Form\ReferentielPrix\ReferentielPrixProduitOuForm;
use Simulaides\Form\ReferentielProduit\RefPrixAideCEEForm;
use Simulaides\Form\Statistique\StatistiqueForm;
use Zend\Form\FormElementManager;
use Zend\Stdlib\Hydrator\ArraySerializable;

/**
 * Classe ParametrageService
 * Service de gestion du paramétrage
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class ParametrageService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'ParametreService';

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface $entityManager
     * @param FormElementManager     $formElementManager
     */
    public function __construct(EntityManagerInterface $entityManager, FormElementManager $formElementManager)
    {
        $this->entityManager      = $entityManager;
        $this->formElementManager = $formElementManager;
    }

    /**
 * Retourne le formulaire de paramétrage du référentiel des prix
 *
 * @return ReferentielPrixParamForm
 */
    public function getReferentielPrixForm()
    {
        /** @var ReferentielPrixParamForm $refPrixForm */
        $refPrixForm = $this->formElementManager->get('ReferentielPrixParamForm');
        $refPrixForm->setHydrator(new ArraySerializable());

        return $refPrixForm;
    }

    /**
     * Retourne le formulaire de paramétrage du coef d'aide CEE
     *
     * @return ReferentielPrixParamForm
     */
    public function getReferentielPrixAideCEEForm()
    {
        /** @var RefPrixAideCEEForm $refPrixForm */
        $refPrixForm = $this->formElementManager->get('RefPrixAideCEEForm');
        $refPrixForm->setHydrator(new ArraySerializable());
        $refPrixForm->setInputFilter(new RefPrixAideCEEValidator());

        return $refPrixForm;
    }

    /**
     * Retourne le formulaire du référentiel des prix pour un produit
     *
     * @return ReferentielProduitForm
     */
    public function getReferentielProduitForm()
    {
        /** @var ReferentielProduitForm $refPrixForm */
        $refPrixForm = $this->formElementManager->get('ReferentielProduitForm');
        $refPrixForm->setObject(new Produit());
        $refPrixForm->setHydrator(
            new DoctrineObject($this->entityManager, MainConstante::ENTITY_PATH . 'Produit')
        );

        return $refPrixForm;
    }

    /**
     * Retourne le formulaire du référentiel des prix pour un produit
     *
     * @return ReferentielPrixProduitOuForm
     */
    public function getReferentielPrixProduitOuForm()
    {
        /** @var ReferentielPrixProduitOuForm $refPrixForm */
        $refPrixForm = $this->formElementManager->get('ReferentielPrixProduitOuForm');
        $refPrixForm->setObject(new Produit());
        $refPrixForm->setHydrator(
            new DoctrineObject($this->entityManager, MainConstante::ENTITY_PATH . 'Produit')
        );

        return $refPrixForm;
    }

    /**
     * Retourne le formulaire de paramétrage des contenus
     *
     * @return ContenuParamForm
     */
    public function getContenuForm()
    {
        /** @var ContenuParamForm $contenuForm */
        $contenuForm = $this->formElementManager->get('ContenuParamForm');
        $contenuForm->setHydrator(new ArraySerializable());

        return $contenuForm;
    }

    /**
     * Retourne le formulaire des statistiques
     *
     * @return StatistiqueForm
     */
    public function getStatistiqueForm()
    {
        /** @var StatistiqueForm $statForm */
        $statForm = $this->formElementManager->get('StatistiqueForm');
        $statForm->setHydrator(new ArraySerializable());

        return $statForm;
    }
}
