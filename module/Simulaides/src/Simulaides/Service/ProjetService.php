<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 13:42
 */

namespace Simulaides\Service;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Exception;
use Simulaides\Constante\DispositifConstante;
use Simulaides\Constante\GroupeTrancheConstante;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\ParametreTechConstante;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\TrancheConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\Commune;
use Simulaides\Domain\Entity\Departement;
use Simulaides\Domain\Entity\ParametreTech;
use Simulaides\Domain\Entity\Produit;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\Entity\ProjetChoixDispositif;
use Simulaides\Domain\Entity\Region;
use Simulaides\Domain\Entity\SaisieProduit;
use Simulaides\Domain\Entity\SaisieProduitMontant;
use Simulaides\Domain\Entity\SaisieProduitParametreTech;
use Simulaides\Domain\Entity\SimulationHistorique;
use Simulaides\Domain\Entity\Travaux;
use Simulaides\Domain\Entity\ValeurListe;
use Simulaides\Domain\Repository\ProjetChoixDispositifRepository;
use Simulaides\Domain\Repository\ProjetRepository;
use Simulaides\Domain\Repository\SimulationHistoriqueRepository;
use Simulaides\Domain\SessionObject\Localisation;
use Simulaides\Domain\SessionObject\ProjetSession;
use Simulaides\Domain\SessionObject\Simulation\AideTravauxResultat;
use Simulaides\Domain\SessionObject\Simulation\DispositifResultat;
use Simulaides\Domain\SessionObject\Simulation\DispositifSimul;
use Simulaides\Domain\SessionObject\Simulation\SimulationResultat;
use Simulaides\Domain\SessionObject\Travaux\ProduitSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitCoutSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitParametreTechSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitSession;
use Simulaides\Domain\SessionObject\Travaux\TravauxSession;
use Simulaides\Form\Accueil\ConditionGeneraleForm;
use Simulaides\Form\Caracteristique\CaracteristiqueForm;
use Simulaides\Form\Caracteristique\LocalisationFieldset;
use Simulaides\Form\Caracteristique\ProjetFieldset;
use Simulaides\Form\Projet\AccessProjetForm;
use Simulaides\Form\Projet\AutreCommentaireForm;
use Simulaides\Form\Projet\AutreDispositifForm;
use Simulaides\Form\Projet\ResultatPDFForm;
use Simulaides\Form\Simulation\AccesSimulationForm;
use Simulaides\Logger\LoggerSimulation;
use Simulaides\Logs\Factory\LoggerFactory;
use Simulaides\Tools\Utils;
use Zend\Form\FormElementManager;
use Zend\Log\Logger;
use Zend\Math\Rand;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayObject;
use Zend\Stdlib\Hydrator\ArraySerializable;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Parameters;
use Doctrine\ORM\Query\AST\NullComparisonExpression;


/**
 * Class ProjetService
 * @package Simulaides\Service
 */
class ProjetService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'ProjetService';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * @var ProjetRepository
     */
    protected $projetRepository;

    /**
     * @var SimulationHistoriqueRepository
     */
    protected $simulationHistoRepository;

    /**
     * @var FormElementManager
     */
    protected $formElementManager;
    /**
     * @var ProjetSessionContainer
     */
    protected $projetSession;

    /** @var LocalisationSessionContainer */
    protected $localisationSession;

    /** @var  TravauxService */
    private $travauxService;

    /** @var  DispositifTravauxService */
    private $dispositifTravauxService;

    /** @var  ProduitService */
    private $produitService;

    /** @var  LocalisationService */
    private $localisationService;

    /** @var  ValeurListeService */
    private $valeurListeService;

    /** @var  SessionContainerService */
    private $sessionService;

    /** @var PartenaireService */
    private $partenaireService;

    /**
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\Projet';

    /**
     * entité SimulationHistorique
     * @var string
     */
    private $classNameHisto = 'Simulaides\Domain\Entity\SimulationHistorique';

    /**
     * @param EntityManagerInterface $entityManager
     * @param FormElementManager $formElementManager
     * @param ProjetSessionContainer $projetSessionContainer
     * @param                         LocalisationSessionContainer
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormElementManager $formElementManager,
        ProjetSessionContainer $projetSessionContainer,
        LocalisationSessionContainer $localisationSessionContainer,
        ServiceLocatorInterface $serviceLocator
    ) {
        $this->setEntityManager($entityManager);
        $this->projetRepository          = $entityManager->getRepository($this->className);
        $this->simulationHistoRepository = $entityManager->getRepository($this->classNameHisto);
        $this->formElementManager        = $formElementManager;
        $this->projetSession             = $projetSessionContainer;
        $this->localisationSession       = $localisationSessionContainer;
        $this->serviceLocator            = $serviceLocator;
    }

    /**
     * @return ObjectRepository|SimulationHistoriqueRepository
     */
    public function getSimulationHistoRepository(){
        return $this->simulationHistoRepository;
    }

    /**
     * @return ProjetChoixDispositifRepository
     * @throws \Assetic\Exception\Exception
     */
    private function getProjetChoixDispositifRepository()
    {
        try {
            return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\ProjetChoixDispositif');
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * Permet de récupérer le formulaire de caractéristique du particulier
     *
     * @return CaracteristiqueForm
     */
    public function getCaracteristiqueForm()
    {
        /** @var CaracteristiqueForm $caracteristiqueForm */
        $caracteristiqueForm = $this->formElementManager->get('CaracteristiqueForm');
        $caracteristiqueForm->setHydrator(new ArraySerializable());

        //Hydratation du Fieldset sur le projet
        /** @var ProjetFieldset $projetFdt */
        $projetFdt = $caracteristiqueForm->get('projet');
        $projetFdt->setObject(new ProjetSession());
        $classObject = new ClassMethods(false);
        $projetFdt->setHydrator($classObject);

        //Hydratation du Fieldset sur la localisation
        /** @var LocalisationFieldset $localisationFdt */
        $localisationFdt = $caracteristiqueForm->get('localisation');
        $localisationFdt->setHydrator(new ArraySerializable());

        return $caracteristiqueForm;
    }

    /**
     * Permet de faire le bind sur le formulaire des caractéristiques
     *
     * @param CaracteristiqueForm $caracteristiqueForm
     * @param null|ProjetSession $projet
     * @param null|Localisation $localisation
     *
     * @return mixed
     */
    public function bindCaracteristiqueForm($caracteristiqueForm, $projet = null, $localisation = null)
    {
        //Si le projet n'existe pas, création d'un objet Projet
        if ($projet === null) {
            $projet = new ProjetSession();
        }
        //Si les données de localisation ne sont pas renseignée, création d'un objet Localisation
        if ($localisation == null) {
            $localisation = new Localisation();
        }

        // Attention : permet de formatter la part fiscale (fait de la même façon que pour les couts de travaux)
        if(is_numeric($projet->getNbPartsFiscales())) {
            $projet->setNbPartsFiscales(Utils::formatNumberToString($projet->getNbPartsFiscales(), 1));
        }
        $data['projet']       = $projet;
        $data['localisation'] = [
            'codeRegion'      => $localisation->getCodeRegion(),
            'codeDepartement' => $localisation->getCodeDepartement(),
            'codeCommune'     => $localisation->getCodeInsee()
        ];

        //Bind du formulaire avec les données des filedset
        $caracteristiqueForm->bind(new ArrayObject($data));

        return $caracteristiqueForm;
    }

    /**
     * @return ProjetSessionContainer
     */
    public function getProjetSession()
    {
        return $this->serviceLocator->get(ProjetSessionContainer::SERVICE_NAME);
    }

    /**
     * Mémorisation en session des informations de localisation
     *
     * @param Parameters $data
     */
    public function saveLocalisationSession(Parameters $data)
    {
        $localisation = $this->getLocalisationSession()->getLocalisation();
        //$hasChanged   = $localisation->getCodeRegion() !== $data['localisation']['codeRegion'];
        if (isset($data['localisation']['codeRegion'])) {
            $localisation->setCodeRegion($data['localisation']['codeRegion']);
        }

        if (isset($data['localisation']['codePostal'])) {
            $localisation->setCodePostal($data['localisation']['codePostal']);
            $data['codePostal'] = $data['localisation']['codePostal'];
        }else {
            $localisation->setCodePostal(null);
            $data['codePostal'] = '';
        }

        if (isset($data['localisation']['codeCommune'])) {
            $localisation->setCodeInsee($data['localisation']['codeCommune']);
            $data['codeCommune'] = $data['localisation']['codeCommune'];
        } else {
            $localisation->setCodeInsee(null);
            $data['codeCommune'] = '';
        }
    }

    /**
     * @return LocalisationSessionContainer
     */
    public function getLocalisationSession()
    {
        return $this->localisationSession;
    }

    /**
     * @return AccessProjetForm
     */
    public function getAccessProjetForm()
    {
        /** @var AccessProjetForm $projetForm */
        $projetForm = $this->formElementManager->get('AccessProjetForm');
        return $projetForm;
    }

    /**
     * Permet de créer le projet en base
     * le projet en base est initialisé/modifié avec les données du projet session
     *
     * @param ProjetSession $projetSession
     *
     * @return Projet
     * @throws Exception
     */
    private function getNouveauProjetBdd($projetSession)
    {
        $idProjet = $projetSession->getId();

        if (!empty($idProjet)) {
            $projet = $this->getProjetParId($idProjet);
        } else {
            $projet = new Projet();

            //On associe la simulation avec le partenaire par lequel elle a été réalisée
            $session = $this->getSessionContainerService();
            $idPartenaire = $session->offsetGet(SessionConstante::PARTENAIRE);
            if($idPartenaire) {
                $partenaire = $this->getPartenaireService()->getPartenaireById($idPartenaire);
                if($partenaire) $projet->setPartenaire($partenaire);
            }
        }

        //Initialisation ou modification du projet avec les données de session
        $projet->setDateEval($projetSession->getDateEval());
        $projet->setAnneeConstruction($projetSession->getAnneeConstruction());
        $projet->setBbc($projetSession->getBbc());
        $projet->setBeneficePtz($projetSession->getBeneficePtz());
        $projet->setPrimoAccession($projetSession->getPrimoAccession());
        $projet->setSalarieSecteurPrive($projetSession->getSalarieSecteurPrive());
        //$projet->setMontantBeneficeCi($projetSession->getMontantBeneficeCi());
        $projet->setCodeCommune($projetSession->getCodeCommune());
        $projet->setCodePostal($projetSession->getCodePostal());
        $projet->setCodeEnergie($projetSession->getCodeEnergie());
        $projet->setCodeStatut($projetSession->getCodeStatut());
        $projet->setCodeTypeLogement($projetSession->getCodeTypeLogement());
        $projet->setNumero($projetSession->getNumero());
        $projet->setMotPasse($projetSession->getMotPasse());
        $projet->setNbAdultes($projetSession->getNbAdultes());
        $projet->setNbEnfantAlterne($projetSession->getNbEnfantAlterne());
        $projet->setNbPersonnesCharge($projetSession->getNbPersonnesCharge());
        $projet->setNbPartsFiscales($projetSession->getNbPartsFiscales());
        $projet->setRevenus($projetSession->getRevenus());
        $projet->setSurfaceHabitable($projetSession->getSurfaceHabitable());
        $projet->setPrisNom($projetSession->getPrisNom());
        $projet->setPrisTel($projetSession->getPrisTel());
        $projet->setPrisEmail($projetSession->getPrisEmail());
        $projet->setIntituleAideConseiller($projetSession->getIntituleAideConseiller());
        $projet->setMtAideConseiller($projetSession->getMtAideConseiller());
        $projet->setDescriptifAideConseiller($projetSession->getDescriptifAideConseiller());
        $projet->setAutreCommentaireConseiller($projetSession->getAutreCommentaireConseiller());
        $projet->setcodeModeChauffage($projetSession->getCodeModeChauff());
        return $projet;
    }

    /**
     * Mémorisation du projet en base depuis les éléments de la session
     *
     * @param ProjetSession $projetSession Projet en cours de saisie
     * @param array         $travaux       Liste des travaux sélectionnés
     *
     * @return mixed|SimulationHistorique
     * @throws Exception
     */
    public function saveProjetEnBase(ProjetSession $projetSession, $travaux)
    {
        try {
            //Création ou récupération du projet en base
            //et mise à jour des informations de caractéristiques
            /** @var Projet $projet */
            $projet = $this->getNouveauProjetBdd($projetSession);

            //Mémorisation de la liste des travaux de la session dans le projet en base
            $this->memoriseTravaux($projet, $travaux);

            //Mémorisation de la liste des produits de la session dans le projet
            $this->memoriseProduit($projet, $travaux);

            //Mémorisation de la saisie des montants et des param tech sur les produits sélectionnés dans la session
            $this->memoriseSaisieProduit($projet, $travaux);

            $this->memoriseChoixDispositifs($projet, $travaux);

            // ------------
            // ATTENTION : important en cas d'ajout de nouveaux attribut de type ValeurListe
            //Set les objets des codes sélectionnés (Statut, Type logement, énergie, Commune)
            $this->setObjetValeurListe($projet);

            //Génère le mot de passe si nécessaire
            $this->genereMotDePasse($projet);

            //Sauvegarde du projet en base de données
            $simulHisto = $this->memoriseProjet($projet);

            //Mise à jour du projet en session avec les infos qui ont été calculées lors de la sauvegarde en bdd
            $projetSession->setId($projet->getId());
            $projetSession->setNumero($projet->getNumero());
            $projetSession->setMotPasse($projet->getMotPasse());

            return $simulHisto;//###

        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de setter les objets par rapport aux codes sélectionnés
     *
     * @param Projet $projet
     *
     * @throws Exception
     */
    private function setObjetValeurListe($projet)
    {
        try {
            /** @var ValeurListe $statut */
            $statut = $this->getValeurListeService()->getValeurPourCodeListeEtCode(
                ValeurListeConstante::LISTE_STATUT,
                $projet->getCodeStatut()
            );
            $projet->setStatut($statut);

            /** @var ValeurListe $typeLogement */
            $typeLogement = $this->getValeurListeService()->getValeurPourCodeListeEtCode(
                ValeurListeConstante::LISTE_TYPE_LOGEMENT,
                $projet->getCodeTypeLogement()
            );
            $projet->setLogement($typeLogement);

            /** @var ValeurListe $energie */
            $energie = $this->getValeurListeService()->getValeurPourCodeListeEtCode(
                ValeurListeConstante::LISTE_ENERGIE_CHAUFFAGE_DETAIL,
                $projet->getCodeEnergie()
            );
            $projet->setEnergie($energie);
            // Si on est dans le cas d'un appartement
            if ($projet->getCodeTypeLogement() === 'A') {
                /** @var ValeurListe $modeChauffage */
                $modeChauffage = $this->getValeurListeService()->getValeurPourCodeListeEtCode(
                    ValeurListeConstante::L_MODE_CHAUFFAGE,
                    $projet->getCodeModeChauffage()
                );
                if($modeChauffage){
                    $projet->setModechauffage($modeChauffage);
                }
            }

            $commune = $this->getLocalisationService()->getCommuneParCode($projet->getCodeCommune());
            $projet->setCommune($commune);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Mémorisation de la liste des produits saisis
     *
     * @param Projet $projet  Projet à mémoriser
     * @param array  $travaux Liste des travaux qui ont été saisis
     *
     * @throws Exception
     */
    private function memoriseProduit($projet, $travaux)
    {
        try {
            $projet->removeAllProduit();
            //Parcours de la liste des produits sélectionnés
            foreach ($travaux['produits'] as $codeProduit) {
                /** @var Produit $prod */
                $prod = $this->getProduitService()->getProduitParCodeProduit($codeProduit);
                //Ajout du produit à la collection
                $projet->addProduit($prod);
                unset($prod);
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Mémorisation de la liste des travaux sélectionnés dans le projet
     *
     * @param Projet $projet  Projet en base à modifier
     * @param array  $travaux Tabelau des travaux sélectionnés en session
     *
     * @throws Exception
     */
    private function memoriseTravaux(Projet $projet, $travaux)
    {
        try {
            $projet->removeAllTravaux();
            //Parcours de la liste des travaux sélectionnés
            foreach ($travaux['travaux'] as $codeTravaux) {
                /** @var Travaux $trav */
                $trav = $this->getTravauxService()->getTravauxByCode($codeTravaux);
                //Ajout du travaux au projet
                $projet->addTravaux($trav);
                unset($trav);
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner la saisie en session pour l'id indiqué
     *
     * @param $id
     * @param $travaux
     *
     * @return SaisieProduitSession|null
     */
    private function getSaisieTravauxSessionParId($id, $travaux)
    {
        /** @var SaisieProduitSession $saisie */
        foreach ($travaux['saisies'] as $saisie) {
            if ($saisie->getId() == $id) {
                return $saisie;
            }
        }
        return null;
    }

    /**
     * Mémorisation de la saisie des montants et des param tech sur les produits sélectionnés
     *
     * @param Projet $projet  Projet à mémoriser
     * @param array  $travaux Liste des travaux qui ont été saisis
     *
     * @throws Exception
     */
    private function memoriseSaisieProduit($projet, $travaux)
    {
        try {
            //Parcours des saisies du projet en base
            /** @var SaisieProduit $saisieProduit */
            foreach ($projet->getSaisiesProduit() as $saisieProduit) {
                $saisieSession = $this->getSaisieTravauxSessionParId($saisieProduit->getId(), $travaux);
                if (empty($saisieSession)) {
                    //La saisie en base n'existe plus en session => suppression
                    $projet->removeSaisiesProduit($saisieProduit);
                } else {
                    //La saisie existe encore modification
                    if ($saisieProduit instanceof SaisieProduitMontant) {
                        /* @var SaisieProduitCoutSession $saisieSession */
                        $saisieProduit->setCout($saisieSession->getCout());
                    } else {
                        /* @var SaisieProduitParametreTechSession $saisieSession */
                        /* @var SaisieProduitParametreTech $saisieProduit */
                        $saisieProduit->setValeur($saisieSession->getValeur());
                    }
                }
            }

            //Ajout des nouvelles saisies
            /** @var SaisieProduitSession $saisie */
            foreach ($travaux['saisies'] as $saisie) {
                $idSaisie = $saisie->getId();
                if (empty($idSaisie)) {
                    if ($saisie instanceof SaisieProduitCoutSession) {
                        //Saisie d'un montant
                        $saisieProduit = new SaisieProduitMontant();
                        $saisieProduit->setCodeProduit($saisie->getCodeProduit());
                        $saisieProduit->setCout($saisie->getCout());
                        $saisieProduit->setTypeMontant($saisie->getTypeCout());
                    } else {
                        //Saisie d'un paramètre
                        /** @var SaisieProduitParametreTechSession $saisie */
                        $saisieProduit = new SaisieProduitParametreTech();
                        $saisieProduit->setCodeProduit($saisie->getCodeProduit());
                        $saisieProduit->setValeur($saisie->getValeur());
                        $saisieProduit->setCodeParametreTech($saisie->getCodeParametre());
                        /** @var ParametreTech $paramTech */
                        $paramTech = $this->getEntityManager()
                            ->getRepository('Simulaides\Domain\Entity\ParametreTech')
                            ->findOneBy(['code' => $saisie->getCodeParametre()]);
                        $saisieProduit->setParametre($paramTech);
                    }
                    $saisieProduit->setId($saisie->getId());
                    //Affectation du projet du produit
                    $saisieProduit->setIdProjet($projet->getId());
                    $saisieProduit->setProjet($projet);
                    $prod = $this->getProduitService()->getProduitParCodeProduit($saisieProduit->getCodeProduit());
                    $saisieProduit->setProduit($prod);
                    //Ajout à la collection de saisie des produits
                    $projet->addSaisiesProduit($saisieProduit);
                }
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Génère le mot de passe sur le projet
     *
     * @param Projet $projet
     */
    private function genereMotDePasse($projet)
    {
        //Vérification que le mot de passe n'est pas déjà généré
        //Cas des simulations existantes re-simulée
        $motPasseInit = $projet->getMotPasse();

        if (empty($motPasseInit)) {
            //Affectation du mot de passe
            $projet->setMotPasse(Utils::genereMotDePasse());
        }
    }

    /**
     * Permet de retourne le nombre de projet réalisé par région
     *
     * @param \DateTime $dateDebut Date de début de la période
     * @param \DateTime $dateFin   Date de fin de la période
     *
     * @return array
     * @throws Exception
     */
    public function getNbSimulationParRegion($dateDebut, $dateFin = null)
    {
        try {
            return $this->simulationHistoRepository->getNbSimulationParRegion($dateDebut, $dateFin);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourne le nombre de projet réalisé par partenaire
     *
     * @param \DateTime $dateDebut Date de début de la période
     * @param \DateTime $dateFin   Date de fin de la période
     *
     * @return array
     * @throws Exception
     */
    public function getNbSimulationParPartenaire($dateDebut, $dateFin = null)
    {
        try {
            return $this->simulationHistoRepository->getNbSimulationParPartenaire($dateDebut, $dateFin);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourne le nombre de projet réalisé par
     *
     * @param \DateTime $dateDebut Date de début de la période
     * @param \DateTime $dateFin   Date de fin de la période
     *
     * @return array
     * @throws Exception
     */
    public function getNbSimulationNational($dateDebut, $dateFin = null, $region=null)
    {
        try {
            return $this->simulationHistoRepository->getNbSimulationNational($dateDebut, $dateFin, $region);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourne le nombre de projet réalisé par EPCI
     *
     * @param \DateTime $dateDebut Date de début de la période
     * @param \DateTime $dateFin   Date de fin de la période
     * @param Region
     *
     * @return array
     * @throws Exception
     */
    public function getNbSimulationParEPCI($dateDebut, $dateFin = null, $departement=null)
    {
        try {
            return $this->simulationHistoRepository->getNbSimulationParEPCI($dateDebut, $dateFin, $departement);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Sauvegarde du projet
     *
     * @param Projet $projet
     *
     * @throws Exception
     */
    private function memoriseProjet($projet)
    {
        try {
            $choixDispositifs = $projet->getChoixDispositifs();
            $projet->setChoixDispositifs(null);

            //Sauvegarde du projet en base de données
            $this->projetRepository->saveProjet($projet);

            //Récupération de la session
            $session = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);
            //Gestion du numéro du projet si nécessaire
            $numProjet = $projet->getNumero();

            if (empty($numProjet)) {
                $numero = $projet->getId() + 1000;
                $projet->setNumero($numero);

                //Sauvegarde du projet en base de données
                //Une nouvelle fois car le numéro dépend de l'Id du projet
                $this->projetRepository->saveProjet($projet);
            }

            $newProjet = $this->projetRepository->find($projet->getid());
            /*foreach ($choixDispositifs as $choix) {
                $choix->setProjet($newProjet);
                $choix->setIdProjet($newProjet->getId());
                $this->getProjetChoixDispositifRepository()->saveProjetChoixDispositif($choix);
            }*/

            //Récupération de la ligne d'historique d'exécution
            $simulHisto = $this->getSimulationHistorique($projet);
            if (empty($simulHisto)) {
                //Si l'historique n'existe pas, création => uniquement dans le cas d'une simulation particulier
                $simulHisto = new SimulationHistorique();
                $simulHisto->setIdProjet($projet->getId());
                //Initialisation de la variable simulAdmin à false dans le cas d'un utilisateur n'ayant pas le rôle ADEME
                $simulAdmin = false;
                //Vérification que l'utilisateur courant est un conseiller et que son rôle est ADEME
                if($session->getConseiller() && $session->getConseiller()->aRoleADEME()) {
                    //Définition de la variable simulAdmin à true car l'utilisateur à un rôle ADEME
                    $simulAdmin = true;
                }
                $simulHisto->setSimulAdmin($simulAdmin);
            }
            if (!empty($simulHisto)) {
                //Modification de la date avec la date du jour
                $simulHisto->setDateExecution(new \DateTime());
                //Modification de la commune
                $simulHisto->setCommune($projet->getCommune());

                $simulHisto->setPartenaire($projet->getPartenaire());

                $this->simulationHistoRepository->saveSimulationHistorique($simulHisto);

                return $simulHisto;//####
            }

        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @param Projet $projet
     */
    public function memoriseChoixDispositifs($projet, $travaux)
    {
        try {
           if(isset($travaux['offres-cee'])){
               $projet->setChoixDispositifs($travaux['offres-cee']);
           }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @param Projet $projet
     */
    public function memoriseChoixDispositifsEnBase($projet, $travaux)
    {
        try {
            foreach ($projet->getChoixDispositifs() as $choix) {
                $this->getProjetChoixDispositifRepository()->saveProjetChoixDispositif($choix);
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner le formulaire d'accès à une simulation
     *
     * @return AccesSimulationForm
     */
    public function getAccessSimulationForm()
    {
        /** @var AccesSimulationForm $simulationAccesForm */
        $simulationAccesForm = $this->formElementManager->get('AccesSimulationForm');
        $simulationAccesForm->setHydrator(new ArraySerializable());
        return $simulationAccesForm;
    }

    /**
     * Permet de retourner le formulaire d'un autre dispositif
     *
     * @return AutreDispositifForm
     */
    public function getAutreDispositifForm()
    {
        /** @var AutreDispositifForm $autreDispositifForm */
        $autreDispositifForm = $this->formElementManager->get('AutreDispositifForm');
        $autreDispositifForm->setObject(new Projet());
        $autreDispositifForm->setHydrator(
            new DoctrineObject($this->entityManager, MainConstante::ENTITY_PATH . 'Projet')
        );

        return $autreDispositifForm;
    }

    /**
     * Permet de retourner le formulaire d'un autre dispositif
     *
     * @return AutreCommentaireForm
     */
    public function getAutreCommentaireForm()
    {
        /** @var AutreCommentaireForm $autreCommentaireForm */
        $autreCommentaireForm = $this->formElementManager->get('AutreCommentaireForm');
        $autreCommentaireForm->setObject(new ProjetSession());
        $autreCommentaireForm->setHydrator(
            new DoctrineObject($this->entityManager, MainConstante::ENTITY_PATH . 'Projet')
        );

        return $autreCommentaireForm;
    }

    /**
     * Retourne le formulaire pour la génération de PDF
     *
     * @return ResultatPDFForm
     */
    public function getPDFForm()
    {
        /** @var ResultatPDFForm $pdfForm */
        $pdfForm = $this->formElementManager->get('ResultatPDFForm');
        $pdfForm->setHydrator(new ArraySerializable());
        return $pdfForm;
    }

    /**
     * Permet de retourner le projet de simulation pour l'Id et le mot de passe
     *
     * @param int    $numero    Identifiant
     * @param string $codeAcces Mot de passe crypté
     *
     * @return Projet
     * @throws Exception
     */
    public function getProjetParNumeroEtCodeAcces($numero, $codeAcces)
    {
        try {
            return $this->projetRepository->getProjetParNumeroEtCodeAcces($numero, $codeAcces);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de créer le projet en session
     * si un projet doctrine est indiqué, le projet de session
     * est initialisé avec les données du projet doctrine
     *
     * @param null|Projet $projet
     *
     * @return ProjetSession
     */
    private function getNouveauProjetSession($projet = null)
    {
        $projetSession = new ProjetSession();

        if (!empty($projet)) {
            $projetSession->setId($projet->getId());
            $projetSession->setDateEval($projet->getDateEval());
            $projetSession->setAnneeConstruction($projet->getAnneeConstruction());
            $projetSession->setBbc($projet->getBbc());
            $projetSession->setBeneficePtz($projet->getBeneficePtz());
            $projetSession->setPrimoAccession($projet->getPrimoAccession());
            $projetSession->setSalarieSecteurPrive($projet->getSalarieSecteurPrive());
            $projetSession->setMontantBeneficeCi($projet->getMontantBeneficeCi());
            $projetSession->setCodeCommune($projet->getCodeCommune());
            $projetSession->setCodeEnergie($projet->getCodeEnergie());
            $projetSession->setCodeStatut($projet->getCodeStatut());
            $projetSession->setCodeTypeLogement($projet->getCodeTypeLogement());
            $projetSession->setNumero($projet->getNumero());
            $projetSession->setMotPasse($projet->getMotPasse());
            $projetSession->setNbAdultes($projet->getNbAdultes());
            $projetSession->setNbEnfantAlterne($projet->getNbEnfantAlterne());
            $projetSession->setNbPersonnesCharge($projet->getNbPersonnesCharge());
            $projetSession->setNbPartsFiscales($projet->getNbPartsFiscales());
            $projetSession->setRevenus(intval($projet->getRevenus()));
            $projetSession->setSurfaceHabitable($projet->getSurfaceHabitable());
            $projetSession->setPrisNom($projet->getPrisNom());
            $projetSession->setPrisTel($projet->getPrisTel());
            $projetSession->setPrisEmail($projet->getPrisEmail());
            $projetSession->setIntituleAideConseiller($projet->getIntituleAideConseiller());
            $projetSession->setMtAideConseiller($projet->getMtAideConseiller());
            $projetSession->setDescriptifAideConseiller($projet->getDescriptifAideConseiller());
            $projetSession->setAutreCommentaireConseiller($projet->getAutreCommentaireConseiller());
            $projetSession->setCodeModeChauff($projet->getCodeModeChauffage());
        }
        return $projetSession;
    }

    /**
     * Permet de charger un projet existant dans la session
     *
     * @param Projet      $projet             Projet à charger
     * @param string|null $idSimulationSource Identifiant de la simulation source en cas de duplication
     *
     * @return ProjetSession
     * @throws Exception
     */
    public function chargeProjetDansSession($projet, $idSimulationSource = null)
    {
        try {
            if (!empty($projet)) {
                //Création du projet de session
                $projetSession = $this->getNouveauProjetSession($projet);
                //Sauvegarde du projet en session
                $this->getProjetSession()->saveProjet($projetSession);

                //Chargement des données de localisation
                /** @var Commune $commune */
                $commune = $this->getLocalisationService()->getCommuneParCode($projet->getCodeCommune());
                $this->getLocalisationService()->getLocalisationSession()->getLocalisation()->setCodeDepartement(
                    $commune->getCodeDepartement()
                );
                /** @var Departement $departement */
                $departement = $commune->getDepartement();
                $this->getLocalisationService()->getLocalisationSession()->getLocalisation()->setCodeRegion(
                    $departement->getCodeRegion()
                );

                $this->getLocalisationService()->getLocalisationSession()->getLocalisation()->setCodeInsee(
                    $projet->getCodeCommune()
                );

                $this->getLocalisationService()->getLocalisationSession()->getLocalisation()->setCodePostal(
                    $projet->getCodePostal()
                );

                //Suppression de tous les travaux qui existeraient éventuellement dans la session
                $this->getTravauxService()->deleteAllTravauxSession();

                //Sélection des travaux sélectionnés dans le projet
                /** @var Travaux $travaux */
                foreach ($projet->getTravaux() as $travaux) {
                    $travauxSession = new TravauxSession();
                    $travauxSession->setCodeTravaux($travaux->getCode());
                    $travauxSession->setLibelle($travaux->getIntitule());
                    foreach ($projet->getChoixDispositifs() as $choixDispositif) {
                        if ($choixDispositif->getCodeTravaux() == $travaux->getCode()) {
                            $travauxSession->setChoixOffre($choixDispositif->getIdDispositif());
                        }
                    }

                    //Sélection des informations produits du travaux qui ont été sélectionnés
                    $idProjet    = $idSimulationSource !== null ? $idSimulationSource : $projet->getId();
                    $tabProduits = $this->getProduitService()->getProduitInfoParCodeTravauxEtIdProjet($travaux->getCode(), $idProjet);
                    foreach ($tabProduits as $infoProduit) {
                        $codeProduit = $infoProduit['code'];

                        //Création du produit dans la session
                        $produitSession = new ProduitSession();
                        $produitSession->setCodeProduit($codeProduit);
                        $produitSession->setLibelle($infoProduit['intitule']);
                        $produitSession->setInfosSaisieCout($infoProduit['infosSaisieCouts']);
                        $produitSession->setInfosSaisieCoutOu($infoProduit['infosSaisieCoutsOu']);

                        //Chargement des informations de saisie sur le produit
                        $this->chargeSaisieProduitSession($projet->getId(), $codeProduit, $produitSession, $idSimulationSource);

                        //Ajout du produit au travaux
                        $travauxSession->addProduit($produitSession);
                    }

                    //Ajout du travaux dans la session
                    $this->getTravauxService()->saveTravauxProduit($travauxSession);
                }

                return $projetSession;
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Ajout des infos de saisie (montant et param tech) sur le produit
     *
     * @param int            $idProjet           Identifiant du projet
     * @param string         $codeProduit        Code du produit
     * @param ProduitSession $produitSession     Produit en session
     * @param string|null    $idSimulationSource Identifiant de la simulation source en cas de duplication
     *
     * @throws Exception
     */
    private function chargeSaisieProduitSession($idProjet, $codeProduit, $produitSession, $idSimulationSource = null)
    {
        //Sélection de la liste des saisies du produit
        $tabSaisieProduit = $this->getProduitService()->getSaisieProduitParIdProjetEtCodeProduit(
            $idSimulationSource !== null ? $idSimulationSource : $idProjet,
            $codeProduit
        );

        $regleNbU = '';
        $produit  = $this->getProduitService()->getProduitParCodeProduit($codeProduit);
        if (!empty($produit)) {
            $regleNbU = $produit->getRegleNBU();
        }
        /** @var ProjetSession $projetSession */
        $projetSession = $this->getProjetSession()->getProjet();

        //Initialisation des montants saisis
        $mt_ht_total = 0;
        $mt_ht_mo    = 0;
        $mt_ht_fo    = 0;
        $nbUnite     = null;
        foreach ($tabSaisieProduit as $saisieProduit) {
            /** @var SaisieProduit $saisieProduit */
            /* @var SaisieProduitSession $saisieProduitSession */
            if ($saisieProduit instanceof SaisieProduitMontant) {
                //Saisie de type Cout
                $saisieProduitSession = new SaisieProduitCoutSession();
                $saisieProduitSession->setTypeCout($saisieProduit->getTypeMontant());
                $saisieProduitSession->setCout($saisieProduit->getCout());
                if ($saisieProduit->getTypeMontant() == SessionConstante::COUT_TOTAL) {
                    $mt_ht_total = $saisieProduit->getCout();
                } elseif ($saisieProduit->getTypeMontant() == SessionConstante::COUT_MO) {
                    $mt_ht_mo = $saisieProduit->getCout();
                } elseif ($saisieProduit->getTypeMontant() == SessionConstante::COUT_FO) {
                    $mt_ht_fo = $saisieProduit->getCout();
                }

                if (is_null($nbUnite)) {
                    $nbUnite = $this->getProduitService()->getNombreUnitePourProduit(
                        $projetSession->getSurfaceHabitable(),
                        $regleNbU
                    );
                }
            } elseif ($saisieProduit instanceof SaisieProduitParametreTech) {
                //Saisie de type Paramètre technique
                $saisieProduitSession = new SaisieProduitParametreTechSession();
                /** @var ParametreTech $paramTech */
                $paramTech = $saisieProduit->getParametre();
                $saisieProduitSession->setCodeParametre($paramTech->getCode());
                $saisieProduitSession->setParamCodeListe($paramTech->getCodeListe());
                $saisieProduitSession->setParamLibelle($paramTech->getLibelle());
                $saisieProduitSession->setParamType($paramTech->getTypeData());
                $saisieProduitSession->setValeur($saisieProduit->getValeur());

                if(($paramTech->getTypeData() === ParametreTechConstante::TYPE_DATA_BOOLEEN) && !$saisieProduit->getValeur()){
                    $nbUnite = 0;
                }
                /*if(($saisieProduit->getParamType() === ParametreTechConstante::TYPE_DATA_BOOLEEN_RADIO)){
                    $nbUnite = 1;
                }*/
                if (is_null($nbUnite)) {
                    $nbUnite = $this->getProduitService()->getNombreUnitePourProduit(
                        $projetSession->getSurfaceHabitable(),
                        $regleNbU,
                        $paramTech->getCode(),
                        $saisieProduit->getValeur()
                    );
                }
            }

            if ($idSimulationSource === null) {
                $saisieProduitSession->setId($saisieProduit->getId());
            }
            $saisieProduitSession->setCodeProduit($codeProduit);
            $saisieProduitSession->setIdProjet($idProjet);
            //Ajout de la saisie du produit au produit
            $produitSession->addSaisiesProduit($saisieProduitSession);
        }

        if(is_null($nbUnite)) {
            $nbUnite = 0;
        }
        /** @var LocalisationService $localisationService */
        $localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
        /** @var ProjetSessionContainer $projet */
        $projet  = $this->serviceLocator->get(ProjetSessionContainer::SERVICE_NAME);
        $outre_mer = $localisationService->estOutreMer($projet->getProjet()->getCodeCommune());

        //Si on est en mode connecté, il faut calculer les couts
        if ($this->getSessionContainerService()->estConseillerConnecte()) {
            //Calcul du cout du produit
            $coutCalcule = $this->getProduitService()->getCouts(
                $codeProduit,
                $mt_ht_total,
                $mt_ht_mo,
                $mt_ht_fo,
                $nbUnite,
                $outre_mer
            );

            //Affectation des valeurs des couts calculés
            foreach ($produitSession->getSaisiesProduit() as $saisieProduit) {
                if ($saisieProduit instanceof SaisieProduitCoutSession) {
                    /** @var SaisieProduitCoutSession $saisieProduit */
                    if ($saisieProduit->getTypeCout() == SessionConstante::COUT_TOTAL) {
                        $saisieProduit->setCoutCalcule(round($coutCalcule->getCoutHtTo(), 2));
                        $saisieProduit->setCoutUser($coutCalcule->getPrixUserTo());
                    } elseif ($saisieProduit->getTypeCout() == SessionConstante::COUT_MO) {
                        $saisieProduit->setCoutCalcule(round($coutCalcule->getCoutHtMo(), 2));
                        $saisieProduit->setCoutUser($coutCalcule->getPrixUserMo());
                    } elseif ($saisieProduit->getTypeCout() == SessionConstante::COUT_FO) {
                        $saisieProduit->setCoutCalcule(round($coutCalcule->getCoutHtFo(), 2));
                        $saisieProduit->setCoutUser($coutCalcule->getPrixUserFo());
                    }
                }
            }
        }
    }

    /**
     * Permet de récupérer le service sur les travaux
     *
     * @return array|object|TravauxService
     */
    private function getTravauxService()
    {
        if (empty($this->travauxService)) {
            $this->travauxService = $this->serviceLocator->get(TravauxService::SERVICE_NAME);
        }
        return $this->travauxService;
    }

    /**
     * Permet de récupérer le service sur les dispositif_travaux
     *
     * @return array|object|DispositifTravauxService
     */
    private function getDispositifTravauxService()
    {
        if (empty($this->dispositifTravauxService)) {
            $this->dispositifTravauxService = $this->serviceLocator->get(DispositifTravauxService::SERVICE_NAME);
        }
        return $this->dispositifTravauxService;
    }

    /**
     * Permet de récupérer le service sur les produits
     *
     * @return array|object|ProduitService
     */
    private function getProduitService()
    {
        if (empty($this->produitService)) {
            $this->produitService = $this->serviceLocator->get(ProduitService::SERVICE_NAME);
        }
        return $this->produitService;
    }

    /**
     * Permet de récupérer le service sur les produits
     *
     * @return array|object|PartenaireService
     */
    private function getPartenaireService()
    {
        if (empty($this->partenaireService)) {
            $this->partenaireService = $this->serviceLocator->get(PartenaireService::SERVICE_NAME);
        }
        return $this->partenaireService;
    }

    /**
     * Permet de récupérer le service sur la localisation
     *
     * @return array|object|LocalisationService
     */
    private function getLocalisationService()
    {
        if (empty($this->localisationService)) {
            $this->localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
        }
        return $this->localisationService;
    }

    /**
     * Permet de récupérer le service sur les listes de valeurs
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
        }
        return $this->valeurListeService;
    }

    /**
     * Retourne le projet pour l'ID
     *
     * @param int $idProjet Identifiant du projet
     *
     * @return null|Projet
     * @throws Exception
     */
    public function getProjetParId($idProjet)
    {
        try {
            return $this->projetRepository->getProjetParId($idProjet);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Sauvegarde du projet en base
     *
     * @param $projet
     *
     * @throws Exception
     */
    public function sauveProjetEnBdd($projet)
    {
        try {
            $this->projetRepository->saveProjet($projet);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le tableau des libellés / valeur des caractéristiques
     *
     * @param Projet $projet
     *
     * @return array
     * @throws Exception
     */
    public function getResultatCaracteristiques($projet)
    {
        try {
            $tabCaracteristique = [];

            $tabCaracteristique[] = array(
                ['label' => 'Commune ', 'value' =>'&nbsp;'.$projet->getCommune()->getNom()],
                ['label' => "Nombre d'adultes ", 'value' => '&nbsp;'.$projet->getNbAdultes()]
            );

            $tabCaracteristique[] = array(
                ['label' => "Type de logement ", 'value' => '&nbsp;'.$projet->getLogement()->getLibelle()],
                ['label' => "Nombre de personnes à charge ", 'value' => '&nbsp;'.$projet->getNbPersonnesCharge()]
            );
            $tabCaracteristique[] = array(
                ['label' => "Année de construction ", 'value' => '&nbsp;'.$projet->getAnneeConstruction()],
                ['label' => "Nombre d'enfants en garde alternée ", 'value' =>'&nbsp;'.$projet->getNbEnfantAlterne()]
            );
            $tabCaracteristique[] = array(
                ['label' => "Surface habitable en m² ", 'value' => '&nbsp;'.$projet->getSurfaceHabitable()],
                ['label' => "Nombre de parts fiscales ", 'value' => '&nbsp;'.$projet->getNbPartsFiscales()]
            );
            $tabCaracteristique[] = array(
                ['label' => "Statut ", 'value' => '&nbsp;'.$projet->getStatut()->getLibelle()],
                [
                    'label' => "Revenu fiscal de référence en € (dernier avis d'imposition) ",
                    'value' => '&nbsp;'.intval($projet->getRevenus())
                ]
            );
            $tabCaracteristique[] = array(
                ['label' => "Energie de chauffage ", 'value' => '&nbsp;'.$projet->getEnergie()->getLibelle()],
                [
                    'label' => "Prêt à taux zéro au cours des 5 dernières années ",
                    'value' => '&nbsp;'.($projet->getBeneficePtz() ? "Oui" : "Non")
                ]
            );
            // Si on est dans le cas d'un appartement
            if ($projet->getCodeTypeLogement() === 'A') {
                $tabCaracteristique[] = array(
                    ['label' => "Mode de chauffage ", 'value' => '&nbsp;' . ($projet->getModechauffage()->getLibelle())],
                );
            }
            $tabCaracteristique[] = array(
                ['label' => "Primo-accession ", 'value' => '&nbsp;'.($projet->getPrimoAccession() ? "Oui" : "Non")],
                ['label' => "Salarié d’une entreprise du secteur privé ", 'value' => '&nbsp;'.($projet->getSalarieSecteurPrive() ? "Oui" : "Non")],

            );
            return $tabCaracteristique;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le tableau des libellés / valeur des caractéristiques pour le pdf
     *
     * @param Projet $projet
     *
     * @return array
     * @throws Exception
     */
    public function getResultatCaracteristiquesPDF($projet)
    {
        try {
            $tabCaracteristique = [];

            $tabCaracteristique[] = array(
                ['label' => 'Commune :', 'value' => $projet->getCommune()->getNom()],
                ['label' => "Nombre d'adultes :", 'value' => $projet->getNbAdultes()]
            );

            $tabCaracteristique[] = array(
                ['label' => "Type de logement :", 'value' => $projet->getLogement()->getLibelle()],
                ['label' => "Nombre de personnes à charge :", 'value' => $projet->getNbPersonnesCharge()]
            );
            $tabCaracteristique[] = array(
                ['label' => "Année de construction :", 'value' => $projet->getAnneeConstruction()],
                ['label' => "Nombre d'enfants en garde alternée :", 'value' => $projet->getNbEnfantAlterne()]
            );
            $tabCaracteristique[] = array(
                ['label' => "Surface habitable en m² :", 'value' => $projet->getSurfaceHabitable()],
                ['label' => "Nombre de parts fiscales :", 'value' => $projet->getNbPartsFiscales()]
            );
            $tabCaracteristique[] = array(
                ['label' => "Statut :", 'value' => $projet->getStatut()->getLibelle()]
            );
            $tabCaracteristique[] = array(
                ['label' => null, 'value' => null],
                [
                    'label' => "Revenu fiscal de référence en € (dernier avis d'imposition) :",
                    'value' => intval($projet->getRevenus())
                ]
            );
            $tabCaracteristique[] = array(
                ['label' => null, 'value' => null],
                ['label' => "Energie de chauffage :", 'value' => $projet->getEnergie()->getLibelle()],
            );
            $tabCaracteristique[] = array(
                ['label' => null, 'value' => null],
                [
                    'label' => "Prêt à taux zéro au cours des 5 dernières années :",
                    'value' => ($projet->getBeneficePtz() ? "Oui" : "Non")
                ]
            );
            $tabCaracteristique[] = array(
                ['label' => "Primo-accédant :", 'value' => ($projet->getPrimoAccession() ? "Oui" : "Non")],
                ['label' => "Salarié d’une entreprise du secteur privé :", 'value' => ($projet->getSalarieSecteurPrive() ? "Oui" : "Non")],

            );
            return $tabCaracteristique;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner le texte d'aide avec le montant
     *
     * @param boolean $avecMontant Indique que le montant doit être affiché
     *
     * @return mixed|string
     * @throws Exception
     */
        public function getResultatAide($avecMontant)
    {
        try {
           $aides = $this->getResultatMontantAide($avecMontant);

            if ($avecMontant) {
                $aideProjet = "D'après les informations que vous avez saisies et sous réserve d'obtention des aides,
                            votre projet pourrait bénéficier de <h4 class='simul-colorN1' id='resultat-taux-aide'><b>[TAUX_AIDE]%</b> d'aide financière";

                $aideProjet .= ', soit <b>[MT_AIDE]</b> €</h4>
                 <h4 class=\'simul-colorN1\' id=\'resultat-taux-aide\'>Soit un reste à charge de <b>[MT_RESTE_A_CHARGE]</b> € (TTC).</h4>';
                $aideProjet .= '.</h4>';
            }else{
                $aideProjet = "<span style='font-size: 16px'>D'après les informations que vous avez saisies et sous réserve d'obtention des aides, votre projet pourrait bénéficier d’environ :</span>
                <h4 class='simul-colorN1' id='resultat-taux-aide'><b>[MT_AIDE]€</b> d'aide financière.<br><b>Attention</b>, ceci est une estimation</h4>";

            }

            //remplacement des variables
            $aideProjet = str_replace('[TAUX_AIDE]', $aides['tauxAide'], $aideProjet);
            $aideProjet = str_replace('[MT_AIDE]', $aides['MontantAide'], $aideProjet);
            $aideProjet = str_replace('[MT_RESTE_A_CHARGE]', $aides['MontantACharge'], $aideProjet);

            return $aideProjet;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner un tableau avec le montant des aides
     *
     * @param boolean $avecMontant Indique que le montant doit être affiché
     *
     * @return mixed|array
     * @throws Exception
     */
    public function getResultatMontantAide($avecMontant)
    {
        try {
            //Récupération des valeurs dans le résultat de la simulation
            $montantTotal = 0;
            $coutTravaux  = 1;
            $session      = $this->getSessionContainerService();
            $simulation   = $session->getSimulationResultat();
            if (!empty($simulation)) {
                $coutTravaux  = $simulation->getCoutTotalTravaux();
                $montantTotal = $this->getMontantTotal($simulation);
            }

            //Calcul du taux
            $tauxAide            = $this->getTauxAide($coutTravaux, $montantTotal);
            if($tauxAide > 100){
                $tauxAide = 100;
            }
            if($tauxAide != '0.0'){
                $montantResteACharge = $this->getMontantResteACharge($tauxAide, $montantTotal);
            }else{
                $montantResteACharge =   $coutTravaux;
            }
            if($montantResteACharge < 0){
                $montantResteACharge = 0;
            }

            $montantTotalArrondi = round($montantTotal/10) * 10;
            if(!$avecMontant){
                $montantDaide = number_format($montantTotalArrondi,0, ',' ,' ');
            }else{
                $montantDaide = number_format($montantTotal,2, ',' ,' ');
            }
            $aide = [
                'tauxAide' => number_format($tauxAide, 0),
                'MontantAide' => $montantDaide,
                'MontantACharge' => number_format($montantResteACharge,2, ',' ,' '),
            ];

            return $aide;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le texte d'aide au résultat de simulation
     *
     * @return null|ValeurListe
     * @throws Exception
     */
    private function getTexteAideComprehensionpourcentage()
    {
        /** @var ValeurListeService $valeurService */
        $valeurService = $this->getValeurListeService();

        return $valeurService->getTextAdministrable(ValeurListeConstante::TEXTE_AIDE_COMPREHENSION_POURCENTAGE);
    }

    /**
     * Permet de retourner le message pour l'annuaire PRIS
     *
     * @return mixed|string
     * @throws Exception
     */
    public function getResultatMessagePRIS()
    {
        try {
            $existDispositifElligible = false;
            $session                  = $this->getSessionContainerService();
            $simulation               = $session->getSimulationResultat();
            if (!empty($simulation)) {
                $existDispositifElligible = $simulation->getExistDispositifElligible();
            }

            $valeurListeService = $this->getValeurListeService();

            //Si il existe au moins un dispositif elligible pour la simulation
            if ($existDispositifElligible) {
                $msgPRIS = $valeurListeService->getTextAdministrable(
                    ValeurListeConstante::DISPOSITIFS_ELIGIBLES_DETECTES
                );
            } else {
                $msgPRIS = $valeurListeService->getTextAdministrable(
                    ValeurListeConstante::AUCUN_DISPOSITIF_ELIGIBLE
                );
            }

            return $msgPRIS;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de récupérer la liste des dispositifs éligibles
     * dans la simulation réalisée
     *
     * @return array
     * @throws Exception
     */
    public function getListeDispositifEligible()
    {
        try {
            $dispositifEvalOk = [];
            $dispositifEvalKo = [];
            $session          = $this->getSessionContainerService();
            $simulation       = $session->getSimulationResultat();
            if (!empty($simulation)) {
                if ($simulation->getExistDispositifElligible()) {
                    /** @var DispositifResultat $dispositif */
                    foreach ($simulation->getDispositifs() as $dispositif) {
                        if ($dispositif->getEligibilite() && $dispositif->getMontant() > 0) {
                            if ($dispositif->getEvaluerRegle()) {
                                $dispositifEvalOk[] = $dispositif;
                            } else {
                                $dispositifEvalKo[] = $dispositif;
                            }
                        }
                    }
                }
            }

            return array_merge($dispositifEvalOk, $dispositifEvalKo);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le texte de rappel des précautions
     *
     * @return mixed|string
     * @throws Exception
     */
    public function getTexteRappelPrecaution()
    {
        try {
            /** @var ValeurListeService $valeurService */
            $valeurService = $this->getValeurListeService();
            $textAide      = $valeurService->getTextAdministrable(
                ValeurListeConstante::RAPPEL_PRECAUTIONS
            );
            if (!empty($textAide)) {
                $dateJour = new \DateTime('now');
                $textAide = str_replace('##DATE_COURANTE##', Utils::formatDateToString($dateJour), $textAide);
            }

            return $textAide;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de session
     *
     * @return array|object|SessionContainerService
     */
    private function getSessionContainerService()
    {
        if (empty($this->sessionService)) {
            $this->sessionService = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);
        }
        return $this->sessionService;
    }

    /**
     * Initialise un nouveau projet en session
     * @param array $data data du formulaire de l'accueil
     * @throws Exception
     */
    public function initialiseNouveauProjet($data = null)
    {
        try {
            //Initialisation d'un projet vierge
            $projetSession = new ProjetSession();
            //Initialisation des valeurs par défaut
            //$projetSession->setMontantBeneficeCi(0);
            $projetSession->setBeneficePtz(false);
            $projetSession->setPrimoAccession(false);
            $projetSession->setSalarieSecteurPrive(false);
            $projetSession->setNbPersonnesCharge(0);
            $projetSession->setNbEnfantAlterne(0);
            if(isset($data['codeTypeLogement'])){
                $projetSession->setCodeTypeLogement($data['codeTypeLogement']);
            }else{
                $projetSession->setCodeTypeLogement('M');
            }
            /** @var ProjetSessionContainer $containerProjet */
            $containerProjet = $this->serviceLocator->get(ProjetSessionContainer::SERVICE_NAME);
            $containerProjet->saveProjet($projetSession);

            //Initialisation des travaux sélectionnés
            $this->getSessionContainerService()->deleteAllTravaux();
            //Suppression du travaux ouvert
            $this->getSessionContainerService()->setTravauxOpened('');
            //suppression des dispositifs
            $this->getSessionContainerService()->setTabIdDispositifTest(null);

            //Initialisation de la localisation
            $localisation = new Localisation();
            //Si la région du particulier est renseigné, on affecte à la localisation
            $codeRegion = $this->getSessionContainerService()->offsetGet(SessionConstante::REGION_PARTICULIER);
            if (!empty($codeRegion)) {
                $localisation->setCodeRegion($codeRegion);
            }

            /** @var LocalisationSessionContainer $localisationContainer */
            $localisationContainer = $this->serviceLocator->get(LocalisationSessionContainer::SERVICE_NAME);
            $localisationContainer->saveLocalisation($localisation);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }

    /**
     * Permet de supprimer plusieurs projet
     *
     * @param array $tabIdProjet Liste des identifiants des projets à supprimer
     *
     * @throws Exception
     */
    public function supprimeListeProjet($tabIdProjet)
    {
        try {
            //Gestion d'une transaction car suppression en plusieurs fois
            $this->getEntityManager()->beginTransaction();
            //1 - Suppression dans Saisie_Produit : c'est une entité distincte
            $produitservice = $this->getProduitService();
            $produitservice->deleteSaisieProduitPourListeProjet($tabIdProjet);

            //2 - Suppression dans comparatif : c'est une entité distincte
            $produitservice->deleteComparatifPourListeProjet($tabIdProjet);

            //3 - Suppression dans projet_choix_dispositif : c'est une entité distincte
            $produitservice->deleteProjetChoixDispositifPourListeProjet($tabIdProjet);

            //4 - Suppression des projet (en cascade Travaux_projet et Projet_produit)
            $this->projetRepository->supprimeListeProjet($tabIdProjet);

            $this->getEntityManager()->commit();
        } catch (Exception $e) {
            $this->getEntityManager()->rollback();
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de sélectionner tous les projets dont la date d'évaluation
     * est antérieure à la date indiquée
     *
     * @param \DateTime $date Date à prendre en compte
     *
     * @return array
     * @throws Exception
     */
    public function getIdProjetAvantDate($date)
    {
        try {
            $tabIdProjet = $this->projetRepository->getIdProjetAvantDate($date);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

        return $tabIdProjet;
    }

    /**
     * Permet de retourner le formulaire d'un autre dispositif
     *
     * @return ConditionGeneraleForm
     */
    public function getCguForm()
    {
        /** @var ConditionGeneraleForm $cguForm */
        $cguForm = $this->formElementManager->get('ConditionGeneraleForm');
        $cguForm->setHydrator(new ArraySerializable());

        return $cguForm;
    }

    /**
     * Lancement de la simulation
     *
     * @param int $idProjet Identifiant du projet
     * @param SimulationHistorique $simulHisto
     * @param bool $offresCdpSeulement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function lanceSimulation($idProjet, $simulHisto = null, $offresCdpSeulement = false)//###
    {
        /** @var SimulationService $simulationService */
        $simulationService   = $this->serviceLocator->get(SimulationService::SERVICE_NAME);
        $tabIdDispositifTest = $this->getSessionContainerService()->getTabIdDispositifTest();
        if ((!is_null($tabIdDispositifTest))
            && ($idx = array_search('PHPSESSID', $tabIdDispositifTest)) !== false) {
            unset($tabIdDispositifTest[$idx]);
        }

        if ((!is_null($tabIdDispositifTest))
            && ($idx = array_search('codeTypeLogement', $tabIdDispositifTest)) !== false
            ) {
            unset($tabIdDispositifTest[$idx]);
        }
        $simulation = $simulationService->getSimulation(
            $idProjet,
            $this->sessionService->getModeExecution(),
            $tabIdDispositifTest,
            $offresCdpSeulement
        );
        //Mémorisation de la simulation résultante
        $simulationResultat = new SimulationResultat();
        $simulationResultat->setDispositifs($this->transfertDispositifSession($simulation->getDispositifs(),$idProjet));
        $simulationResultat->setModeExecution($simulation->getModeExecution());
        $simulationResultat->setExistDispositifElligible($simulation->getExistDispositifElligible());
        $simulationResultat->setMontantTotal($simulation->getMontantTotal());
        $simulationResultat->setCoutTotalTravaux($simulation->getProjet()->getCoutTotalTravaux());
        $simulationResultat->setMtConseillerDispositif($simulation->getProjet()->getMtAideConseiller());
        $this->getSessionContainerService()->setSimulationResultat($simulationResultat);

        if ($simulHisto) {
            $simulHisto->setMontant($simulation->getMontantTotal());
            $simulHisto->setTaux(($simulation->getMontantTotal() / $simulation->getProjet()->getCoutTotalTravaux()) * 100);
            $this->getSimulationHistoRepository()->saveSimulationHistorique($simulHisto);
        }

        //Ajout du fichier de log dans le projet
        $this->majProjetAvecFichierLog();
    }

    /**
     * Récupération des données utilies au résultat
     * pour ne pas charger la session
     *
     * @param $tabDispositif
     *
     * @return array
     */
    private function transfertDispositifSession($tabDispositif,$idProjet)
    {
        $tabDispositifResultat = [];
        /** @var DispositifSimul $dispositif */
        foreach ($tabDispositif as $dispositif) {
            $dispositifResultat = new DispositifResultat();
            $dispositifResultat->setId($dispositif->getId());
            $dispositifResultat->setIntitule($dispositif->getIntitule());
            $dispositifResultat->setFinanceur($dispositif->getFinanceur());
            $dispositifResultat->setDescriptif($dispositif->getDescriptif());
            $dispositifResultat->setMontant($dispositif->getMontant());
            $dispositifResultat->setEvaluerRegle($dispositif->getEvaluerRegle());
            $dispositifResultat->setEligibilite($dispositif->getEligibilite());
            if($dispositif->getDebutValidite()){
                $dateDebutValidite = new \DateTime($dispositif->getDebutValidite());
                $dispositifResultat->setDebutValidite($dateDebutValidite->format('d/m/Y'));
            }
            $dispositifResultat->setOblige($dispositif->getOblige());
            $dispositifResultat->setSiteWeb($dispositif->getSiteWeb());
            $dispositifResultat->setType($dispositif->getType());
            $image = '';
            if($image){
                $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_DEFAULT);
            }else if(in_array($dispositif->getId(),explode(',',DispositifConstante::IDS_DISPOSITIF_MAPRIMRENOV))){
                //cas spécifique MAPrimRenov
                $projet = $this->getProjetParId($idProjet);
                $projetSimulService = $this->serviceLocator->get(ProjetSimulService::SERVICE_NAME);
                /** @var LocalisationService $localisationService */
                $localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
                $idf = $localisationService->estDansLaRegion($projet->getCodeCommune(),MainConstante::$LISTE_IDF);
                $codeTranche = '';
                if($idf){
                    $codeTranche = $projetSimulService ->getCodeTranche($projet, GroupeTrancheConstante::MPR_IDF);
                }else{
                    $codeTranche = $projetSimulService->getCodeTranche($projet, GroupeTrancheConstante::MPR);
                }
                switch ($codeTranche) {
                    case TrancheConstante::TRES_MOD_MPR :
                    case TrancheConstante::TRES_MOD_MPR_I:
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_MAPRIMRENOV_TRES_MODESTE);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_DEFAULT);
                        break;
                    case TrancheConstante::MODESTE_MPR :
                    case TrancheConstante::MODESTE_MPR_IDF:
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_MAPRIMRENOV_MODESTE);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_DEFAULT);
                        break;
                    case TrancheConstante::INTER_MPR :
                    case TrancheConstante::INTER_MPR_IDF:
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_MAPRIMRENOV_INTER);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_DEFAULT);
                        break;
                    default :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_MAPRIMRENOV_SUP);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_DEFAULT);
                        break;
                }

            }else if(in_array($dispositif->getId(),explode(',',DispositifConstante::IDS_DISPOSITIF_COUPDEPOUCE))){
                //cas spécifique Coup de pouce
                $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_COUP_DE_POUCE);
                $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_COUP_DE_POUCE);
            }else{
                switch ($dispositif->getType()) {
                    case SimulationConstante::TYPE_AIDE_ANAH :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_ANAH);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_ANAH);
                        break;
                    case SimulationConstante::TYPE_AIDE_CEE :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_CEE);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_CEE);
                        break;
                    case SimulationConstante::TYPE_AIDE_COMMUNAL :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_COMMUNAL);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_COMMUNAL);
                        break;
                    case SimulationConstante::TYPE_AIDE_DEPARTEMENT :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_DEPARTEMENT);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_DEPARTEMENT);
                        break;
                    case SimulationConstante::TYPE_AIDE_EPCI :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_EPCI);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_EPCI);
                        break;
                    case SimulationConstante::TYPE_AIDE_NATIONAL :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_NATIONAL);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_NATIONAL);
                        break;
                    case SimulationConstante::TYPE_AIDE_REGION :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_REGION);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_REGION);
                        break;
                    case SimulationConstante::TYPE_AIDE_PUBLIC :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_PUBLIC);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_PUBLIC);
                        break;
                    case SimulationConstante::TYPE_AIDE_COUP_DE_POUCE :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_COUP_DE_POUCE);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_COUP_DE_POUCE);
                        break;

                    default :
                        $dispositifResultat->setImage(DispositifConstante::IMAGE_DISPOSITIF_DEFAULT);
                        $dispositifResultat->setCredit(DispositifConstante::CREDIT_DISPOSITIF_DEFAULT);
                        break;
                }
            }
            $tabTravaux = [];
            foreach ($dispositif->getAidesTravaux() as $aideTravaux) {
                $aideTravauxResult = new AideTravauxResultat();
                $travaux           = $aideTravaux->getTravaux();
                $aideTravauxResult->setCodeTravaux($travaux->getCode());
                $aideTravauxResult->setIntituleTravaux($travaux->getIntitule());
                $aideTravauxResult->setEligibiliteSpecifique($aideTravaux->getEligibiliteSpecifique());
                $aideTravauxResult->setMontantAideTo($aideTravaux->getMontantAideTo());
                $aideTravauxResult->setCoutTravauxHtTo($travaux->getCoutHtTo());
                $travail = $this->getTravauxService()->getTravauxByCode($travaux->getCode());
                $aideTravauxResult->setCategorie($travail->getCategorie());
                $aideTravauxResult->setNumOrdre($travail->getNumOrdre());
                $tabTravaux[] = $aideTravauxResult;
            }
            //tri les travaux
            $result0 = [];
            $result1 = [];
            $result2 = [];
            $result3 = [];
            $result4 = [];
            foreach ($tabTravaux as $travaux) {
                switch ($travaux->getCategorie()) {
                    case ValeurListeConstante::CAT_TRAVAUX_ETUDE:
                        $result0[$travaux->getNumOrdre()] = $travaux;
                        break;
                    case ValeurListeConstante::CAT_TRAVAUX_ISOLATION:
                        $result1[$travaux->getNumOrdre()] = $travaux;
                        break;
                    case ValeurListeConstante::CAT_TRAVAUX_EQUIPEMENT:
                        $result2[$travaux->getNumOrdre()] = $travaux;
                        break;
                    case ValeurListeConstante::CAT_TRAVAUX_ENERGIES_RENOUVELABLES:
                        $result3[$travaux->getNumOrdre()] = $travaux;
                        break;
                    case ValeurListeConstante::CAT_TRAVAUX_ELECTROMENAGER:
                        $result4[$travaux->getNumOrdre()] = $travaux;
                        break;
                }
            }

            ksort($result0);
            ksort($result1);
            ksort($result2);
            ksort($result3);
            ksort($result4);
            $result[0] = $result0;
            $result[1] = $result1;
            $result[2] = $result2;
            $result[4] = $result4;

            $tabTravaux = [];
            for($i=0;$i<5;$i++){
                if(is_array($result[$i]) && count($result[$i])){
                    $tabTravaux = array_merge($tabTravaux,$result[$i]);
                }
            }


            $dispositifResultat->setAidesTravaux($tabTravaux);
            $tabDispositifResultat[] = $dispositifResultat;
        }

        return $tabDispositifResultat;
    }

    /**
     * Permet de mettre le fichier de log dans le projet en base
     */
    private function majProjetAvecFichierLog()
    {
        //Récupération du projet en base
        /** @var Projet $projet */
        if($this->projetSession->getProjet()) {
            $projet = $this->getProjetParId($this->projetSession->getProjet()->getId());

            if (!empty($projet)) {
                //Mémorisation du fichier dans le projet
                $projet->setFichierLog(file_get_contents(LoggerSimulation::getLogFile()));
                $this->save($projet);
                //Suppression du fichier de log
                LoggerSimulation::deleteLogFile();
            }
        }
    }

    /**
     * Calcule de montant total des aides
     *
     * @param SimulationResultat $simulation
     *
     * @return int
     */
    private function getMontantTotal($simulation)
    {
        if (!empty($simulation)) {
            $montantTotal   = $simulation->getMontantTotal();
            $aideConseiller = $simulation->getMtConseillerDispositif();
            if (!empty($aideConseiller)) {
                //Si le conseiller a ajouter une aide supplémentaire avec un montant, il est ajouté au total
                $montantTotal += $aideConseiller;
            }
        } else {
            $montantTotal = 0;
        }

        return $montantTotal;
    }

    /**
     * Calcule le taux d'aides
     *
     * @param $coutTravaux
     * @param $montantTotal
     *
     * @return string
     */
    private function getTauxAide($coutTravaux, $montantTotal)
    {
        $division = 0;
        if ($coutTravaux > 0) {
            $division = ($montantTotal / $coutTravaux);
        }

        return Utils::formatNumberToString($division * 100, 0);
    }

    /**
     * Calcule le reste à charge
     *
     * @param $tauxAide
     * @param $montantAide
     *
     * @return float|int
     */
    public function getMontantResteACharge($tauxAide, $montantAide)
    {
        $montantResteACharge = 0;
        if ($tauxAide) {
            $montantResteACharge = $this->getCoutsTravaux($montantAide, $tauxAide) - $montantAide;
        }

        return $montantResteACharge;
    }

    /**
     * Calcul des coûts des travaux
     *
     * @param $montantAide
     * @param $tauxAide
     *
     * @return float|int
     */
    public function getCoutsTravaux($montantAide, $tauxAide)
    {
        if ($tauxAide > 0) {
            return $montantAide * 100 / $tauxAide;
        }

        return 0;
    }

    /**
     * @param Projet $projet
     *
     * @return SimulationHistorique
     */
    public function getSimulationHistorique(Projet $projet)
    {
        return $this->simulationHistoRepository->getSimulationHistoriqueParIdProjet($projet->getId());
    }

    /**
     * Duplique la simulation
     *
     * @param string $idSimulationSource
     *
     * @return ProjetSession|null
     * @throws Exception
     */
    public function copySimulation($idSimulationSource)
    {
        $simulationSource = $this->projetRepository->getProjetParId($idSimulationSource);
        if ($simulationSource !== null) {
            $simulationClone = clone $simulationSource;
            $simulationClone->setDateEval(new \DateTime());

            $this->getSessionContainerService()->setModeExecution(SimulationConstante::MODE_PRODUCTION);
            $projetSession = $this->chargeProjetDansSession($simulationClone, $idSimulationSource);
            $projetSession->setCodePostal($simulationClone->getCodePostal());
            /** @var TravauxService $travauxService */
            $travauxService = $this->serviceLocator->get(TravauxService::SERVICE_NAME);
            $travaux        = $travauxService->getSelectedProduits($simulationClone); //Récupération de la liste des travaux/produits/saisies produits sélectionnés

            $simulHisto = $this->saveProjetEnBase($projetSession, $travaux);
            $this->getProjetSession()->saveProjet($projetSession); //Mémorisation du projet ainsi modifié en session
            $this->lanceSimulation($projetSession->getId(), $simulHisto);
            $travauxService->memoriseCoutProduitPourProjet($projetSession->getId()); // Mémorisation du calcul des coûts

            return $projetSession;
        }

        return null;
    }

    function getTrancheRevenuMPR($projet){
        $projetSimulService = $this->serviceLocator->get(ProjetSimulService::SERVICE_NAME);
        $codeCommune = $projet->getCodeCommune();
        $localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
        $idf = $localisationService->estDansLaRegion($codeCommune,MainConstante::$LISTE_IDF);
        if($idf){
            $codeTranche = $projetSimulService->getCodeTranche($projet, GroupeTrancheConstante::MPR_IDF);
        }else{
            $codeTranche = $projetSimulService ->getCodeTranche($projet, GroupeTrancheConstante::MPR);
        }
        return $codeTranche;
    }

}
