<?php
namespace Simulaides\Service;

use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Mime;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe SendMailService
 * Service de gestion de l'envoi de mail
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class SendMailService
{
    /** Nom du service */
    const SERVICE_NAME = 'SendMailService';

    /**
     * Constante pour définir le champ BCC
     *
     * @var string
     */
    const BCC = 'BCC';

    /**
     * Constante pour définir le champ TO
     *
     * @var string
     */
    const TO = 'TO';

    /**
     * Constante pour définir le champ CC
     *
     * @var string
     */
    const CC = 'CC';

    /**
     * Encodage par défaut des mails envoyés
     * @var string
     */
    const ENCODAGE = 'UTF-8';
    //const ENCODAGE = 'base64';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /**
     * Constructeur du service
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Fonction d'envoi de mail avec gestion des erreurs
     *
     * @param string | array $mails
     *                            Adresse(s) mail(s) de destination
     * @param string         $body
     *                            Corps du message
     * @param string         $subject
     *                            Sujet du message
     * @param string         $fromMail
     *                            Adresse mail de l'expediteur
     * @param string         $fromName
     *                            Nom de l'expediteur [OPTIONNEL]
     * @param                Zend /Mime/Part | array de Zend/Mime/Part $attachements
     *                            Pièce(s) jointe(s) [OPTIONNEL]
     *
     * @return boolean
     *     Vrai si les mails ont bien été envoyés,
     *     Faux si une exception a eu lieu (l'exception est alors loggée)
     */
    public function sendMail($mails, $body, $subject, $fromMail, $fromName = null, $attachments = null)
    {
        if (empty($mails)) {
            return true;
        }
        $mailsBcc = '';
        $mailsCc  = '';
        switch (true) {
            // Si des mails BCC sont paramétrés
            case (isset($mails['bcc'])):
                $mailsBcc = array_unique($mails['bcc']);
                unset($mails['bcc']);
                break;
            // Si des mails CC sont paramétrés
            case (isset($mails['cc'])):
                $mailsCc = array_unique($mails['cc']);
                unset($mails['cc']);
                break;
            // Si des mails TO sont paramétrés
            case (isset($mails['to'])):
                $mails = array_unique($mails['to']);
                unset($mails['to']);
                break;
        }
        // Dans tous les autres cas la variable mails contiendra les mails normaux
        $mails = array_unique($mails);
        try {
            $message = new Message();
            $message->getHeaders()->addHeaderLine('Content-Type', 'text/html; charset="utf-8"');
            # contenu du message
            $bodyMessage     = new MimeMessage();
            $tabMessageParts = array();
            // contenu html du message
            $html              = new MimePart(utf8_decode($body));
//            $html              = new MimePart($body);
            $html->encoding = self::ENCODAGE;
            $html->type        = Mime::TYPE_HTML;
            $tabMessageParts[] = $html;
            // pièces jointes
            if (!empty($attachments)) {
                if (!is_array($attachments)) {
                    $attachments->disposition = Mime::DISPOSITION_ATTACHMENT;
                    $attachments->encoding    = Mime::ENCODING_BASE64;
                    $tabMessageParts[]        = $attachments;
                } else {
                    foreach ($attachments as $attachment) {
                        $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
                        $attachment->encoding    = Mime::ENCODING_BASE64;
                        $tabMessageParts[]       = $attachment;
                    }
                }
            }

            // expéditeur
            if ($fromMail) {
                $message->setFrom($fromMail, $fromName);
            }
            // ajout du contenu au message
            $bodyMessage->setParts($tabMessageParts);
            $message->setBody($bodyMessage);
            // destinataire(s)
            if (!empty($mails)) {
                $message->addTo($mails, 'destinataire');
            }
            // Bc(s)
            if (!empty($mailsBcc)) {
                $message->addBcc($mailsBcc, 'bcc');
            }
            // Cc(s)
            if (!empty($mailsCc)) {
                $message->addCc($mailsCc, 'cc');
            }
            // sujet
            $message->setSubject($subject);
            // envoi du message
            $transport = $this->getServiceLocator()->get('SmtpTransport');
            // encodage du message
            $message->setEncoding(self::ENCODAGE);
            $transport->send($message);
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }

    /**
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
