<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\DispositifHistorique;
use Simulaides\Domain\Repository\DispositifHistoriqueRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe DispositifHistoriqueService
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class DispositifHistoriqueService
{
    /** Nom du service */
    const SERVICE_NAME = 'DispositifHistoriqueService';

    /**
     * Nom de la classe des historiques des dispositifs
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\DispositifHistorique';

    /**
     * Repository sur historique de dispositif
     * @var DispositifHistoriqueRepository
     */
    private $dispositifHistoriqueRepository;

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager                  = $entityManager;
        $this->dispositifHistoriqueRepository = $entityManager->getRepository($this->className);
        $this->serviceLocator                 = $serviceLocator;
    }

    /**
     * Permet d'ajouter une note d'historique au dispositif
     *
     * @param Dispositif $dispositif Dispositif sur lequel ajouter la note
     * @param string     $note       Note à ajouter
     *
     * @throws \Exception
     */
    public function ajouteNoteDispositifHistorique($dispositif, $note)
    {
        try {
            //Création d'une note d'historique pour le dispositif
            $dispositifHistorique = new DispositifHistorique();
            $dispositifHistorique->setIdDispositif($dispositif->getId());
            $dispositifHistorique->setDispositif($dispositif);
            $dispositifHistorique->setNote($note);
            $dispositifHistorique->setDateNote(new \DateTime());

            //sauvegarde de la ligne d'historique
            $this->dispositifHistoriqueRepository->saveDispositifHistorique($dispositifHistorique);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Sauvegarde de l'historique du dispositif
     *
     * @param DispositifHistorique $dispositifHistorique
     *
     * @throws \Exception
     */
    public function saveDispositifHistorique($dispositifHistorique)
    {
        try {
            $this->dispositifHistoriqueRepository->saveDispositifHistorique($dispositifHistorique);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des historiques d'un dispositif
     *
     * @param Dispositif $dispositif Dispositif
     *
     * @return array
     * @throws \Exception
     */
    public function getListHistoriquePourDispositif($dispositif)
    {
        try {
            return $this->dispositifHistoriqueRepository->getListHistoriquePourDispositif($dispositif);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
