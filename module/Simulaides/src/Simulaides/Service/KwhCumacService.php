<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Repository\KwhCumacRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe KwhCumacService
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class KwhCumacService
{
    /** Nom du service */
    const SERVICE_NAME = 'KwhCumacService';

    /** @var  KwhCumacRepository */
    private $kwhCumacRepository;

    /**
     * Nom de la classe des Kwh Cumac
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\KwhCumac';

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager      = $entityManager;
        $this->kwhCumacRepository = $entityManager->getRepository($this->className);
        $this->serviceLocator     = $serviceLocator;
    }

    /**
     * Fournit le nombre de kwh_cumac des données : code du travaux, caracteristique,
     * type du logement,énergie, mode de chauffage et la zone climatique
     *
     * @param string $codeTravaux       Code du travaux
     * @param string $caracteristique   Caractéristique
     * @param string $codeTypeLogement  Code du type de logement
     * @param string $codeEnergie       Code du type d'énergie
     * @param string $codeModeChauffage Code du type de chauffage
     * @param string $zone              Zone
     *
     * @return int
     * @throws \Exception
     */
    public function getKwhCumacpourCodeTravaux(
        $codeTravaux,
        $caracteristique,
        $codeTypeLogement,
        $codeEnergie,
        $codeModeChauffage,
        $zone
    ) {
        $nb_kwh = 0;
        try {
            $result = $this->kwhCumacRepository->getKwhCumacpourCodeTravaux(
                $codeTravaux,
                $caracteristique,
                $codeTypeLogement,
                $codeEnergie,
                $codeModeChauffage,
                $zone
            );
            if (!empty($result)) {
                $nb_kwh = $result['nbKwh'];
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

        return $nb_kwh;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
