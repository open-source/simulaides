<?php

namespace Simulaides\Service;

use ArrayObject;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Simulaides\Constante\AccesSimulationFormConstante;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Form\Simulation\AccesSimulationForm;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\RequestInterface;

/**
 * Classe AccessSimulationFormService
 * Service pour la gestion du formulaire d'accès aux simulations
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class AccessSimulationFormService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'AccessSimulationFormService';

    /**
     * Service sur le projet
     * @var ProjetService
     */
    private $projetService;

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /**
     * Constructeur
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ServiceLocatorInterface $serviceLocator
    ) {
        $this->setEntityManager($entityManager);
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Retourne le formulaire d'accès à la simulation avec le bind
     *
     * @return AccesSimulationForm
     */
    public function getAndBindFormAccessSimulation()
    {
        $formAccesSimulation = $this->getProjetService()->getAccessSimulationForm();

        $dataAcces = [
            AccesSimulationFormConstante::CHAMP_NUM_SIMULATION => '',
            AccesSimulationFormConstante::CHAMP_CODE_ACCES     => ''
        ];
        $formAccesSimulation->bind(new ArrayObject($dataAcces));

        return $formAccesSimulation;
    }

    /**
     * Permet de vérifier que le projet existe
     * et charge en session le projet
     *
     * @param AccesSimulationForm $formAccesSimulation
     * @param int                 $numSimulation Numéro de la simulation à recharger
     * @param string              $codeAcces     Code d'accès
     * @param array               $dataPost      Données postées par le formulaire
     *
     * @return array
     * @throws Exception
     */
    public function processSimulationApresVerifProjet($formAccesSimulation, $numSimulation, $codeAcces, $dataPost)
    {
        $dataReturn = ['msgError' => null, 'simulationOK' => false];

        $formAccesSimulation->setData($dataPost);
        //Vérification que le formulaire soit valide
        if ($formAccesSimulation->isValid()) {
            $projetSimulation = $this->getProjetService()->getProjetParNumeroEtCodeAcces($numSimulation, $codeAcces);
            if (null === $projetSimulation) {
                //Le projet n'existe pas
                $dataReturn['msgError'] = "Aucune simulation ne correspond au numéro
                                            et au code d'accès que vous avez saisis";
            } else {
                $this->getSessionContainer()->setModeExecution(SimulationConstante::MODE_PRODUCTION);
                if (!$this->getSessionContainer()->estConseillerConnecte()
                    && $this->getSessionContainer()->offsetExists(SessionConstante::REGION_PARTICULIER)
                    && !$this->getSimulationRegionApartenanceVerificationService()->isSimulationCommuneInRegionCode(
                        $projetSimulation->getCodeCommune(),
                        $this->getSessionContainer()->offsetGet(SessionConstante::REGION_PARTICULIER)
                    )
                ) {
                    $dataReturn['msgError'] = "La région de votre simulation n'est pas disponible sur ce site.";
                } else {
                    //Chargement du projet dans la session
                    $this->getProjetService()->chargeProjetDansSession($projetSimulation);
                    $dataReturn['simulationOK'] = true;
                }
            }
        }

        $dataReturn['formAccesSimulation'] = $formAccesSimulation;

        return $dataReturn;
    }

    /**
     * Permet de faire le traitement de la simulation
     *
     * @param RequestInterface $request
     *
     * @return array
     * @throws Exception
     */
    public function processSimulation(RequestInterface $request)
    {
        $formAccesSimulation = $this->getAndBindFormAccessSimulation();
        $dataReturn          = [
            'msgError'            => null,
            'simulationOK'        => false,
            'formAccesSimulation' => $formAccesSimulation
        ];

        if ($request->isPost()) {
            $dataPost = $request->getPost();
            //Récupération des données du formulaire
            $numSimulation = $dataPost[AccesSimulationFormConstante::CHAMP_NUM_SIMULATION];
            $codeAcces     = $dataPost[AccesSimulationFormConstante::CHAMP_CODE_ACCES];

            //Vérifie le projet, valide le formulaire et charge le projet en session
            $dataReturn = $this->processSimulationApresVerifProjet(
                $formAccesSimulation,
                $numSimulation,
                $codeAcces,
                $dataPost
            );
        }

        return $dataReturn;
    }

    /**
     * Retourne le service sur les projets
     *
     * @return array|object|ProjetService
     */
    private function getProjetService()
    {
        if (empty($this->projetService)) {
            $this->projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
        }

        return $this->projetService;
    }

    /** @return SessionContainerService */
    private function getSessionContainer()
    {
        /* @var SessionContainerService $sessionContainerService */
        $sessionContainerService = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);

        return $sessionContainerService;
    }

    /** @return SimulationRegionApartenanceVerificationService */
    private function getSimulationRegionApartenanceVerificationService()
    {
        /* @var SimulationRegionApartenanceVerificationService $simulationRegionApartenanceVerificationService */
        $simulationRegionApartenanceVerificationService = $this->serviceLocator->get(SimulationRegionApartenanceVerificationService::SERVICE_NAME);

        return $simulationRegionApartenanceVerificationService;
    }
}
