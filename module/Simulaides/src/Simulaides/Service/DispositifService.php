<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Exception;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\DispositifHistorique;
use Simulaides\Domain\Entity\DispositifRegle;
use Simulaides\Domain\Entity\DispositifTravaux;
use Simulaides\Domain\Entity\DispTravauxRegle;
use Simulaides\Domain\Entity\Groupe;
use Simulaides\Domain\Entity\PerimetreGeo;
use Simulaides\Domain\Entity\Region;
use Simulaides\Domain\Entity\ValeurListe;
use Simulaides\Domain\Repository\DispositifRegleRepository;
use Simulaides\Domain\Repository\DispositifRepository;
use Simulaides\Form\Dispositif\DispositifFieldset;
use Simulaides\Form\Dispositif\DispositifGestionForm;
use Simulaides\Form\Dispositif\DispositifHistoriqueForm;
use Simulaides\Form\Dispositif\DispositifTravauxFieldset;
use Simulaides\Form\Dispositif\DispositifTravauxForm;
use Simulaides\Form\Dispositif\EditeurRegleForm;
use Simulaides\Form\Dispositif\FiltreDispositifForm;
use Simulaides\Form\Dispositif\PerimetreGeoFieldset;
use Simulaides\Form\Dispositif\RegionAdminForm;
use Simulaides\Form\ValeurListe\ValeurListeFieldset;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Form\Element\Select;
use Zend\Form\FormElementManager;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\ArraySerializable;
use Zend\View\Model\ViewModel;

/**
 * Classe DispositifService
 * Service sur les dispositifs
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class DispositifService
{

    /** Nom du service */
    const SERVICE_NAME = 'DispositifService';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * @var PerimetreGeoService
     */
    private $perimetreGeoService;

    /**
     * Service sur les groupes
     *
     * @var GroupeService
     */
    private $groupeService;

    /**
     * Service sur la localisation
     *
     * @var LocalisationService
     */
    private $localisationService;

    /**
     * Service sur les travaux des dispositifs
     *
     * @var DispositifTravauxService
     */
    private $dispositifTravauxService;

    /**
     * Service sur les règles
     *
     * @var RegleService
     */
    private $regleService;

    /**
     * Nom de la classe des dispositifs
     * @var string
     */
    private $classNameDispositif = 'Simulaides\Domain\Entity\Dispositif';

    /**
     * Nom de la classe des dispositifs
     * @var string
     */
    private $classNameDispositifRegle = 'Simulaides\Domain\Entity\DispositifRegle';

    /**
     * @var FormElementManager
     */
    protected $formElementManager;

    /**
     * @var DispositifRepository
     */
    protected $dispositifRepository;

    /**
     * @var DispositifRegleRepository
     */
    protected $dispositifRegleRepository;

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param FormElementManager      $formElementManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormElementManager $formElementManager,
        ServiceLocatorInterface $serviceLocator
    ) {
        $this->entityManager             = $entityManager;
        $this->serviceLocator            = $serviceLocator;
        $this->dispositifRepository      = $entityManager->getRepository($this->classNameDispositif);
        $this->dispositifRegleRepository = $entityManager->getRepository($this->classNameDispositifRegle);
        $this->formElementManager        = $formElementManager;
    }

    /**
     * Retourne la liste des dispositifs appartenant à la région
     * et triée sur le numéro d'ordre dans la région
     *
     * @param string  $codeRegion Code de la région
     * @param boolean $validAdate
     *
     * @return array
     * @throws Exception
     */
    public function getDispositifParCodeRegion($codeRegion,$validAdate = false)
    {
        try {
            $tabReturn = [];
            if (!empty($codeRegion)) {
                //Récupération des dispositifs de la région indiquée
                $tabReturn = $this->getDispositifRepository()->getDispositifParCodeRegion($codeRegion,$validAdate);
            }

            return $tabReturn;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des dispositifs appartenant à la région
     * et triée sur le numéro d'ordre dans la région
     *
     * @param string $codeRegion Code de la région
     *
     * @param        $intitule
     * @param        $financeur
     * @param        $typeDispositif
     * @param        $etatDispositif
     * @param        $validite
     * @param        $codeTravaux
     * @param        $codeDepartement
     * @param        $codeCommune
     *
     * @return array
     * @throws Exception
     */
    public function searchDispositif($codeRegion, $intitule, $financeur, $typeDispositif, $etatDispositif, $validite,
        $codeTravaux,$codeDepartement,$codeCommune)
    {
        try {
            $tabReturn = [];
            if (!empty($codeRegion)) {
                //Récupération des dispositifs de la région indiquée
                $tabReturn = $this->getDispositifRepository()->searchDispositif(
                    $codeRegion,
                    $intitule,
                    $financeur,
                    $typeDispositif,
                    $etatDispositif,
                    $validite,
                    $codeTravaux,
                    $codeDepartement,
                    $codeCommune
                );
            }

            return $tabReturn;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @return DispositifRepository
     */
    public function getDispositifRepository()
    {
        return $this->dispositifRepository;
    }

    /**
     * Retourne le formulaire de la liste des dispositifs
     * Choix de la région
     *
     * @return RegionAdminForm
     */
    public function getRegionAdminForm()
    {
        /** @var RegionAdminForm $regionAdminForm */
        $regionAdminForm = $this->formElementManager->get('RegionAdminForm');
        $regionAdminForm->setHydrator(new ArraySerializable());

        return $regionAdminForm;
    }

    /**
     * Retourne le formulaire de la liste des dispositifs
     *
     * @return FiltreDispositifForm
     */
    public function getFiltreDispositifForm()
    {
        /** @var FiltreDispositifForm $form */
        $form = $this->formElementManager->get('FiltreDispositifForm');
        $form->setHydrator(new ArraySerializable());

        $session = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);
        $codeRegion = ($session->getFiltresDispositifRegion())? $session->getFiltresDispositifRegion() : null;
        $codeDepartement = ($session->getFiltresDispositifCodeDepartement())?
            $session->getFiltresDispositifCodeDepartement() : null;

        $form->add(
            [
                'name'       => 'codeDepartement',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Département",
                    'empty_option' => 'Sélectionner un département',
                    'target_class' => 'Simulaides\Domain\Entity\Departement',
                    'property'     => 'nom',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getDepartementsFiltres',
                        'params' => [
                            'codeRegion' => $codeRegion
                        ]
                    ],
                ],
                'attributes' => [
                    'id'    => 'codeDepartement',
                    'class' => 'chosen-select change-selectDep form-control'
                ]
            ]
        );

        $form->add(
            [
                'name'       => 'codeCommune',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Commune",
                    'empty_option' => 'Sélectionner une commune',
                    'target_class' => 'Simulaides\Domain\Entity\Commune',
                    'property'     => 'nom',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getCommunesFiltres',
                        'params' => [
                            'codeDepartement' => $codeDepartement
                        ]
                    ],
                ],
                'attributes' => [
                    'id'    => 'codeCommune',
                    'class' => 'chosen-select change-selectCom form-control'
                ]
            ]
        );

        return $form;
    }

    /**
     * Retourne le formulaire de l'onglet "Gestion dispositif"
     *
     * @return DispositifGestionForm
     */
    public function getDispositifGestionForm()
    {
        set_time_limit(1800);//30 minutes
        /** @var DispositifGestionForm $gestionForm */
        $gestionForm = $this->formElementManager->get('DispositifGestionForm');
        //Définition de l'hydrator sur un tableau
        $gestionForm->setHydrator(new ArraySerializable());
        $gestionForm->setUseAsBaseFieldset(true);
        //Ajout de la fonction permettant de gérer la modification
        $gestionForm->setChangeFonctionJs();
        //définition de l'hydrator sur le fieldset Dispositif
        /** @var DispositifFieldset $dispositifFieldset */
        $dispositifFieldset = $gestionForm->getFieldsets()['dispositif'];
        $dispositifFieldset->setObject(new Dispositif());
        $dispositifFieldset->setHydrator(
            new DoctrineObject($this->entityManager, MainConstante::ENTITY_PATH . 'Dispositif')
        );

        return $gestionForm;
    }

    /**
     * Permet de mettre tout le formulaire en disabled
     *
     * @param DispositifGestionForm $form
     * @param bool                  $isConsult            Indique si le formulaire doit être en consultation
     * @param                       $typeDispositif
     * @param bool                  $disabledLocalisation Indique si on rend inaccessible les éléments
     *                                                    de localisation (epci et commune)
     *
     * @return mixed
     */
    public function setDispositifGestionFormDisabled($form, $isConsult, $typeDispositif, $disabledLocalisation)
    {
        /** @var DispositifFieldset $fdtDispositif */
        $fdtDispositif = $form->get('dispositif');
        /** @var PerimetreGeoFieldset $fdtPerimetreGeo */
        $fdtPerimetreGeo = $form->get('perimetregeo');

        //L'état et le type dispositif doivent toujours être inaccessibles
        /** @var ValeurListeFieldset $fdtEtat */
        $fdtEtat = $fdtDispositif->get('etat');
        $fdtEtat->get('libelle')->setAttribute('readonly', true);
        /** @var ValeurListeFieldset $fdtType */
        $fdtType = $fdtDispositif->get('typeDispositif');
        $fdtType->setAttribute('readonly', $isConsult);

        //Si on est en consultation, tous champs sont inaccessibles
        //Rem: Les champs datePicker ne sont pas mis à disabled ici car cela ne désactive pas le bouton
        $fdtDispositif->get('intitule')->setAttribute('readonly', $isConsult);
        $fdtDispositif->get('intituleConseiller')->setAttribute('readonly', $isConsult);
        $fdtDispositif->get('financeur')->setAttribute('readonly', $isConsult);
        $fdtDispositif->get('descriptif')->setAttribute('readonly', $isConsult);

        $fdtDispositif->get('evaluerRegle')->setAttribute('disabled', $isConsult);
        //Si on est en modification, mais que le type est CEE => radio CEE inaccessible
        $fdtDispositif->get('exclutCEE')->setAttribute('disabled', $isConsult || $typeDispositif == SimulationConstante::TYPE_AIDE_CEE);
        //var_dump($typeDispositif);
        $fdtDispositif->get('exclutAnah')->setAttribute('disabled', $isConsult || $typeDispositif == SimulationConstante::TYPE_AIDE_ANAH);
        $fdtDispositif->get('exclutEPCI')->setAttribute('disabled', $isConsult || $typeDispositif == SimulationConstante::TYPE_AIDE_EPCI);
        $fdtDispositif->get('exclutRegion')->setAttribute('disabled', $isConsult || $typeDispositif == SimulationConstante::TYPE_AIDE_REGION);
        $fdtDispositif->get('exclutCommunal')->setAttribute('disabled', $isConsult || $typeDispositif == SimulationConstante::TYPE_AIDE_COMMUNAL);
        $fdtDispositif->get('exclutNational')->setAttribute('disabled', $isConsult || $typeDispositif == SimulationConstante::TYPE_AIDE_NATIONAL);
        $fdtDispositif->get('exclutDepartement')->setAttribute('disabled', $isConsult || $typeDispositif == SimulationConstante::TYPE_AIDE_DEPARTEMENT);
        $fdtDispositif->get('exclutCoupDePouce')->setAttribute('disabled', $isConsult || $typeDispositif == SimulationConstante::TYPE_AIDE_COUP_DE_POUCE);

        $fdtPerimetreGeo->get('codeRegion')->setAttribute('disabled', $isConsult);
        $fdtPerimetreGeo->get('codeDepartement')->setAttribute('disabled', $isConsult);
        //Si on est en modification, mais que périmètre est National/Dep/Region => Epci et Commune inaccessibles
        $fdtPerimetreGeo->get('codeEpci')->setAttribute('disabled', $isConsult || $disabledLocalisation);
        $fdtPerimetreGeo->get('codeCommune')->setAttribute('disabled', $isConsult || $disabledLocalisation);

        return $form;
    }

    /**
     * Retourne le dispositif correspondant à l'id
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return null|Dispositif
     * @throws Exception
     */
    public function getDispositifParId($idDispositif)
    {
        try {
            //Récupération des dispositifs de la région indiquée
            return $this->getDispositifRepository()->getDispositifParId($idDispositif);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le dispositif correspondant à l'id
     *
     * @param array $ids
     *
     * @return array
     * @throws Exception
     */
    public function getListDispositifByIds(array $ids)
    {
        try {
            //Récupération des dispositifs de la région indiquée
            return $this->getDispositifRepository()->getListDispositifByIds($ids);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de faire la modification ou l'ajout du dispositif
     *
     * @param Dispositif $dispositif
     *
     * @throws Exception
     */
    public function saveDispositif($dispositif)
    {
        try {
            //Sauvegarde du dispositif
            $this->getDispositifRepository()->saveDispositif($dispositif);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de sauvegarder les éléments du formulaire "Gestion dispositif"
     *
     * @param Dispositif $dispositif      Dispositif à mémoriser
     * @param array      $tabPerimetreGeo Tableau des périmètres géo
     * @param array      $tabRegionGroupe Tableau des région auxquelles ajouter le dispositif
     *
     * @throws Exception
     */
    public function saveFicheGestionDispositif($dispositif, $tabPerimetreGeo, $tabRegionGroupe)
    {
        try {
            //Début de transaction
            $this->entityManager->beginTransaction();

            //Sauvegarde du dispositif
            $this->saveDispositif($dispositif);

            //Sauvegarde du périmètre géo
            $this->savePerimetreGeoPourDispositif(
                $dispositif,
                $tabPerimetreGeo,
                $dispositif->getTypePerimetre()
            );

            //Sauvegarde dans les groupes de région
            $this->saveGroupeRegionPourDispositif($dispositif->getId(), $tabRegionGroupe);

            //Validation de la transaction
            $this->entityManager->commit();
        } catch (Exception $e) {
            //Annulation de la transation
            $this->entityManager->rollback();
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de sauvegarder les informations du périmètre géo d'un dispositif
     *
     * @param Dispositif $dispositif      Dispositif
     * @param array      $tabPerimetreGeo Tableau des périmètres
     * @param string     $typePerimetre   Type de périmètre
     *
     * @throws Exception
     */
    private function savePerimetreGeoPourDispositif($dispositif, $tabPerimetreGeo, $typePerimetre)
    {
        try {
            //Suppression du périmètre du dispositif
            $this->getPerimetreGeoService()->deletePerimetreGeoPourdispositif($dispositif->getId());

            $tabRegion      = isset($tabPerimetreGeo['codeRegion'])?        $tabPerimetreGeo['codeRegion']:[];
            $tabDepartement = isset($tabPerimetreGeo['codeDepartement'])?   $tabPerimetreGeo['codeDepartement']:[];
            $tabCommune     = isset($tabPerimetreGeo['codeCommune'])?       $tabPerimetreGeo['codeCommune']:[];

            //On ne garde le périmètre intercommunal que s'il ne s'agit pas d'un périmètre communal
            $tabCodeEpci    = (empty($tabCommune)&&isset($tabPerimetreGeo['codeEpci']))?          $tabPerimetreGeo['codeEpci']:[];


            foreach ($tabRegion as $eltPerimetreGeo) {
                $perimetreGeo = new PerimetreGeo();
                $perimetreGeo->setIdDispositif($dispositif->getId());
                $perimetreGeo->setDispositif($dispositif);

                $perimetreGeo->setCodeRegion($eltPerimetreGeo);
                $region = $this->getLocalisationService()->getRegionParCode($eltPerimetreGeo);
                $perimetreGeo->setRegion($region);

                //Sauvegarde du périmètre géo
                $this->getPerimetreGeoService()->savePerimetreGeo($perimetreGeo);
            }

            foreach ($tabDepartement as $eltPerimetreGeo) {
                $perimetreGeo = new PerimetreGeo();
                $perimetreGeo->setIdDispositif($dispositif->getId());
                $perimetreGeo->setDispositif($dispositif);

                $perimetreGeo->setCodeDepartement($eltPerimetreGeo);
                $departement = $this->getLocalisationService()->getDepartementParCode($eltPerimetreGeo);
                $perimetreGeo->setDepartement($departement);

                //Sauvegarde du périmètre géo
                $this->getPerimetreGeoService()->savePerimetreGeo($perimetreGeo);
            }

            foreach ($tabCodeEpci as $eltPerimetreGeo) {
                $perimetreGeo = new PerimetreGeo();
                $perimetreGeo->setIdDispositif($dispositif->getId());
                $perimetreGeo->setDispositif($dispositif);

                $perimetreGeo->setCodeEpci($eltPerimetreGeo);
                $epci = $this->getLocalisationService()->getEpciPourCode($eltPerimetreGeo);
                $perimetreGeo->setEpci($epci);

                //Sauvegarde du périmètre géo
                $this->getPerimetreGeoService()->savePerimetreGeo($perimetreGeo);
            }

            foreach ($tabCommune as $eltPerimetreGeo) {
                $perimetreGeo = new PerimetreGeo();
                $perimetreGeo->setIdDispositif($dispositif->getId());
                $perimetreGeo->setDispositif($dispositif);

                $perimetreGeo->setCodeCommune($eltPerimetreGeo);
                $commune = $this->getLocalisationService()->getCommuneParCode($eltPerimetreGeo);
                $perimetreGeo->setCommune($commune);

                //Sauvegarde du périmètre géo
                $this->getPerimetreGeoService()->savePerimetreGeo($perimetreGeo);
            }

            //Ajout du périmètre du dispositif
            /*#foreach ($tabPerimetreGeo as $eltPerimetreGeo) {
                $perimetreGeo = new PerimetreGeo();
                $perimetreGeo->setIdDispositif($dispositif->getId());
                $perimetreGeo->setDispositif($dispositif);

                //Renseignement de la bonne valeur en fonction du type de périmètre
                if ($typePerimetre == ValeurListeConstante::TYPE_PERIM_GEO_REGIONAL) {
                    $perimetreGeo->setCodeRegion($eltPerimetreGeo);
                    $region = $this->getLocalisationService()->getRegionParCode($eltPerimetreGeo);
                    $perimetreGeo->setRegion($region);
                } elseif ($typePerimetre == ValeurListeConstante::TYPE_PERIM_GEO_DEPARTEMENTAL) {
                    $perimetreGeo->setCodeDepartement($eltPerimetreGeo);
                    $departement = $this->getLocalisationService()->getDepartementParCode($eltPerimetreGeo);
                    $perimetreGeo->setDepartement($departement);
                } elseif ($typePerimetre == ValeurListeConstante::TYPE_PERIM_GEO_EPCI) {
                    $perimetreGeo->setCodeEpci($eltPerimetreGeo);
                    $epci = $this->getLocalisationService()->getEpciPourCode($eltPerimetreGeo);
                    $perimetreGeo->setEpci($epci);
                } elseif ($typePerimetre == ValeurListeConstante::TYPE_PERIM_GEO_COMMUNAL) {
                    $perimetreGeo->setCodeCommune($eltPerimetreGeo);
                    $commune = $this->getLocalisationService()->getCommuneParCode($eltPerimetreGeo);
                    $perimetreGeo->setCommune($commune);
                }

                //Sauvegarde du périmètre géo
                $this->getPerimetreGeoService()->savePerimetreGeo($perimetreGeo);
            }*/
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Perme d'ajouter/modifier les associations des régions aux dispositifs
     *
     * @param $idDispositif
     * @param $tabRegion
     *
     * @throws Exception
     */
    private function saveGroupeRegionPourDispositif($idDispositif, $tabRegion)
    {
        try {
            /** @var Dispositif $dispositif */
            $dispositif = $this->getDispositifParId($idDispositif);
            //Parcours ds régions à associer au dispositif
            /** @var Region $region */
            foreach ($tabRegion as $region) {
                $groupe = $this->getGroupeService()->getGroupeParCodeRegionEtId(
                    $region->getCode(),
                    $idDispositif
                );
                if (empty($groupe)) {
                    //Le groupe n'existe pas : le dispositif n'est pas encore associé à la région
                    //Ajout de l'association
                    $newOrdre = 1;
                    $maxOrdre = $this->getGroupeService()->getMaxOrdrePourCodeRegion($region->getCode());
                    if (!empty($maxOrdre)) {
                        $newOrdre = $maxOrdre + 1;
                    }
                    $groupe = new Groupe();
                    $groupe->setIdDispositif($idDispositif);
                    $groupe->setDispositif($dispositif);
                    $groupe->setCodeRegion($region->getCode());
                    $groupe->setRegion($region);
                    $groupe->setNumeroOrdre($newOrdre);
                    $this->getGroupeService()->saveGroupe($groupe);
                }
            }

            //Sélection des régions associées au dispositif et qui ne sont pas contenues dans la liste des régions
            //en entrée (=> association de région à supprimer)
            $groupeASupprimer = $this->getGroupeService()->getGroupePourDispositifNotInTabRegion(
                $idDispositif,
                $tabRegion
            );

            /** @var Groupe $groupe */
            foreach ($groupeASupprimer as $groupe) {
                //Récupération du numéro d'ordre supp
                $noOrdreSupp = $groupe->getNumeroOrdre();
                //Récupération du code région
                $codeRegion = $groupe->getCodeRegion();

                //Suppression du groupe
                $this->getGroupeService()->deleteGroupe($groupe);

                //Réorganisation des ordres pour la région
                $this->getGroupeService()->modifyOrdreApresSuppression($codeRegion, $noOrdreSupp);
            }

        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service sur les périmètres géographiques
     *
     * @return array|object|PerimetreGeoService
     */
    private function getPerimetreGeoService()
    {
        if (empty($this->perimetreGeoService)) {
            $this->perimetreGeoService = $this->serviceLocator->get(PerimetreGeoService::SERVICE_NAME);
        }
        return $this->perimetreGeoService;
    }

    /**
     * Retourne le service sur les groupes
     *
     * @return array|object|GroupeService
     */
    private function getGroupeService()
    {
        if (empty($this->groupeService)) {
            $this->groupeService = $this->serviceLocator->get(GroupeService::SERVICE_NAME);
        }
        return $this->groupeService;
    }

    /**
     * Retourne le service sur la localisation
     *
     * @return array|object|LocalisationService
     */
    private function getLocalisationService()
    {
        if (empty($this->localisationService)) {
            $this->localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
        }
        return $this->localisationService;
    }

    /**
     * Retourne le service sur les travaux des dispositifs
     *
     * @return array|object|DispositifTravauxService
     */
    private function getDispositifTravauxService()
    {
        if (empty($this->dispositifTravauxService)) {
            $this->dispositifTravauxService = $this->serviceLocator->get(DispositifTravauxService::SERVICE_NAME);
        }
        return $this->dispositifTravauxService;
    }

    /**
     * Permet de supprimer le dispositif
     *
     * @param Dispositif $dispositif Dispositif à supprimer
     *
     * @throws Exception
     */
    public function deleteDispositif($dispositif)
    {
        $connexion = $this->entityManager->getConnection();
        try {
            $connexion->beginTransaction();
            //Suppression du dispositif dans les groupes de région
            $this->getGroupeService()->deleteGroupeParIdDispositif($dispositif->getId());

            //Suppression de la liste des travaux pris en charge par le dispositif
            //(cascade : supprime les règles du travaux dans le dispositif)
            $this->getDispositifTravauxService()->deleteDispositifTravauxParIdDispositif($dispositif->getId());

            //suppression du dispositif (cascade : supprime les règles et le périm géo)
            $this->getDispositifRepository()->deleteDispositif($dispositif);


            $connexion->commit();
        } catch (Exception $e) {
            $connexion->rollBack();
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de modifier l'état du dispositif
     *
     * @param Dispositif $dispositif Dispositif à modifier
     * @param string     $codeEtat   Code de l'état à modifier
     *
     * @throws Exception
     */
    public function changeEtatDispositif($dispositif, $codeEtat)
    {
        try {
            /** @var ValeurListeService $ValeurListeService */
            $ValeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
            /** @var ValeurListe $nouvelEtat */
            $nouvelEtat = $ValeurListeService->getValeurPourCodeListeEtCode(
                ValeurListeConstante::L_ETAT_DISPOSITIF,
                $codeEtat
            );
            /** @var ValeurListe $etatInit */
            $etatInit = $dispositif->getEtat();

            //Modification de l'état du dispositif
            $dispositif->setCodeEtat($codeEtat);
            $dispositif->setEtat($nouvelEtat);

            //Sauvegarde du dispositif
            $this->saveDispositif($dispositif);

            //Ajout d'une note dans l'historique
            //Récupération du conseiller
            /** @var SessionContainerService $session */
            $session    = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);
            $conseiller = $session->getConseiller();
            //Création de la note
            $structureConseiller = $conseiller->getStructure();
            $note                = $structureConseiller . " / Changement d'état : ";
            $note                .= $etatInit->getLibelle() . " -> " . $nouvelEtat->getLibelle();

            /** @var DispositifHistoriqueService $HistoriqueService */
            $HistoriqueService = $this->serviceLocator->get(DispositifHistoriqueService::SERVICE_NAME);
            $HistoriqueService->ajouteNoteDispositifHistorique($dispositif, $note);

        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }



    /**
     * Permet de modifier l'état du dispositif
     *
     * @param int $idDispositif Dispositif à modifier
     * @param string     $codeEtat   Code de l'état à modifier
     *
     * @throws Exception
     */
    public function changeEtatDispositifBatch($idDispositif, $codeEtat)
    {
        try {

            $dispositif = $this->getDispositifParId($idDispositif);

            /** @var ValeurListeService $ValeurListeService */
            $ValeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
            /** @var ValeurListe $nouvelEtat */
            $nouvelEtat = $ValeurListeService->getValeurPourCodeListeEtCode(
                ValeurListeConstante::L_ETAT_DISPOSITIF,
                $codeEtat
            );
            /** @var ValeurListe $etatInit */
            $etatInit = $dispositif->getEtat();

            //Modification de l'état du dispositif
            $dispositif->setCodeEtat($codeEtat);
            $dispositif->setEtat($nouvelEtat);

            //Sauvegarde du dispositif
            $this->saveDispositif($dispositif);

            $note                = "Batch automatique / Changement d'état : ";
            $note                .= $etatInit->getLibelle() . " -> " . $nouvelEtat->getLibelle();

            /** @var DispositifHistoriqueService $HistoriqueService */
            $HistoriqueService = $this->serviceLocator->get(DispositifHistoriqueService::SERVICE_NAME);
            $HistoriqueService->ajouteNoteDispositifHistorique($dispositif, $note);

        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de sélectionner tous les dispositif actif dont la date de fin de validité est dépassé
     * @param \DateTime $date Date à prendre en compte
     * @param string $etat etat pris en compte
     *
     * @return array
     * @throws Exception
     */
    public function getIdDispositifByDateDepassee($date,$etat=null)
    {
        try {
            $tabId = $this->dispositifRepository->getIdDispositifByDateDepassee($date,$etat);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

        return $tabId;
    }


    /**
     * Permet de sélectionner tous les dispositif actif dont la date de fin de validité est dépassé
     *
     * @param $codeRegion
     * @param $ordreMax
     *
     * @return array
     * @throws Exception
     */
    public function getListeDispositifRequis($codeRegion = null, $ordreMax = null)
    {
        $tab = [];
        try {
            $tabListe = $this->dispositifRepository->getListeDispositifRequis($codeRegion,$ordreMax);
            foreach ($tabListe as $element){
                $tab[$element->getId()] = $element->getIntitule();
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

        return $tab;
    }



    /**
     * Permet de réaliser la duplication d'un dispositif
     *
     * @param $dispositifInit
     * @param $dispositifNew
     *
     * @throws Exception
     */
    public function dupliqueDispositif($dispositifInit, &$dispositifNew)
    {
        try {
            $this->entityManager->beginTransaction();

            //Initialisation du nouveau dispositif
            $dispositifNew = $this->initNewDispositif($dispositifNew);

            //Duplication des données du dispositif
            $dispositifNew = $this->dupliqueDonneesDispositif($dispositifInit, $dispositifNew);

            //Sauvegarde du nouveau dispositif
            $this->saveDispositif($dispositifNew);

            //Duplication des travaux pris en charge par le dispositif
            $this->dupliqueTravauxDispositif($dispositifInit, $dispositifNew);

            //Duplication des groupes de région
            $this->dupliqueGroupeDispositif($dispositifInit, $dispositifNew);

            $this->entityManager->commit();
        } catch (Exception $e) {
            $this->entityManager->rollback();
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet d'initialiser le nouveau dispositif
     *
     * @param Dispositif $newDispositif
     *
     * @return mixed
     * @throws Exception
     */
    private function initNewDispositif($newDispositif)
    {
        //Le nouveau dispositif est créé à l'état DESACTIVE
        $newDispositif->setCodeEtat(ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE);
        /** @var ValeurListeService $ValeurListeService */
        $ValeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
        /** @var ValeurListe $nouvelEtat */
        $nouvelEtat = $ValeurListeService->getValeurPourCodeListeEtCode(
            ValeurListeConstante::L_ETAT_DISPOSITIF,
            ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE
        );
        $newDispositif->setEtat($nouvelEtat);
        $newDispositif->setIntitule('Nouveau dispositif');

        return $newDispositif;
    }

    /**
     * Permet de dupliquer les données du dispositif
     *
     * @param Dispositif $dispositifInit Dispositif initial à partir duquel on duplique
     * @param Dispositif $dispositifNew  Nouveau dispositif à remplir
     *
     * @return mixed
     * @throws Exception
     */
    private function dupliqueDonneesDispositif($dispositifInit, $dispositifNew)
    {
        try {
            //Données du dispositif
            $dispositifNew->setCodeTypeDispositif($dispositifInit->getCodeTypeDispositif());
            $dispositifNew->setTypeDispositif($dispositifInit->getTypeDispositif());
            $dispositifNew->setIntituleConseiller($dispositifInit->getIntituleConseiller());
            $dispositifNew->setDescriptif($dispositifInit->getDescriptif());
            $dispositifNew->setFinanceur($dispositifInit->getFinanceur());
            $dispositifNew->setExclutCEE($dispositifInit->getExclutCEE());
            $dispositifNew->setExclutAnah($dispositifInit->getExclutAnah());
            $dispositifNew->setExclutEPCI($dispositifInit->getExclutEPCI());
            $dispositifNew->setExclutRegion($dispositifInit->getExclutRegion());
            $dispositifNew->setExclutCommunal($dispositifInit->getExclutCommunal());
            $dispositifNew->setExclutNational($dispositifInit->getExclutNational());
            $dispositifNew->setExclutDepartement($dispositifInit->getExclutDepartement());
            $dispositifNew->setExclutCoupDePouce($dispositifInit->getExclutCoupDePouce());
            $dispositifNew->setEvaluerRegle($dispositifInit->getEvaluerRegle());
            $dispositifNew->setTypePerimetre($dispositifInit->getTypePerimetre());

            //Liste des règles du dispositif
            $dispositifNew = $this->dupliqueRegleDispositif($dispositifInit, $dispositifNew);

            //Le périmètre géographique
            $dispositifNew = $this->dupliquePerimetreGeoDispositif($dispositifInit, $dispositifNew);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
        return $dispositifNew;
    }

    /**
     * Duplique les périmètre géo d'un dispositif à un autre
     *
     * @param Dispositif $dispositifInit Dispositif initial à partir duquel on duplique
     * @param Dispositif $dispositifNew  Nouveau dispositif à remplir
     *
     * @return mixed
     * @throws Exception
     */
    private function dupliquePerimetreGeoDispositif($dispositifInit, $dispositifNew)
    {
        $listPerimGeo = $dispositifInit->getListDispositifPerimetreGeo();
        /** @var PerimetreGeo $perimGeo */
        foreach ($listPerimGeo as $perimGeo) {
            $newPerimGeo = new PerimetreGeo();
            $newPerimGeo->setDispositif($dispositifNew);
            //Région
            $codeRegion = $perimGeo->getCodeRegion();
            if (!empty($codeRegion)) {
                $region = $this->getLocalisationService()->getRegionParCode($codeRegion);
                $newPerimGeo->setCodeRegion($perimGeo->getCodeRegion());
                $newPerimGeo->setRegion($region);
            }
            //Département
            $codeDept = $perimGeo->getCodeDepartement();
            if (!empty($codeDept)) {
                $dept = $this->getLocalisationService()->getDepartementParCode($codeDept);
                $newPerimGeo->setCodeDepartement($codeDept);
                $newPerimGeo->setDepartement($dept);
            }
            //Commune
            $codeCommune = $perimGeo->getCodeCommune();
            if (!empty($codeCommune)) {
                $commune = $this->getLocalisationService()->getCommuneParCode($codeCommune);
                $newPerimGeo->setCodeCommune($codeCommune);
                $newPerimGeo->setCommune($commune);
            }
            //Epci
            $codeEpci = $perimGeo->getCodeEpci();
            if (!empty($codeEpci)) {
                $epci = $this->getLocalisationService()->getEpciPourCode($codeEpci);
                $newPerimGeo->setCodeEpci($perimGeo->getCodeEpci());
                $newPerimGeo->setEpci($epci);
            }
            $dispositifNew->addDispositifPerimetreGeo($newPerimGeo);
        }

        return $dispositifNew;
    }

    /**
     * Duplique les règles d'un dispositif à un autre
     *
     * @param Dispositif $dispositifInit Dispositif initial à partir duquel on duplique
     * @param Dispositif $dispositifNew  Nouveau dispositif à remplir
     *
     * @return mixed
     */
    private function dupliqueRegleDispositif($dispositifInit, $dispositifNew)
    {
        $listRegle = $dispositifInit->getListDispositifRegles();
        /** @var DispositifRegle $regle */
        foreach ($listRegle as $regle) {
            $newRegle = new DispositifRegle();
            $newRegle->setDispositif($dispositifNew);
            $newRegle->setType($regle->getType());
            $newRegle->setConditionRegle($regle->getConditionRegle());
            $newRegle->setExpression($regle->getExpression());
            $dispositifNew->addDispositifRegle($newRegle);
        }

        return $dispositifNew;
    }

    /**
     * Duplique les travaux d'un dispositif à un autre
     *
     * @param Dispositif $dispositifInit Dispositif initial à partir duquel on duplique
     * @param Dispositif $dispositifNew  Nouveau dispositif à remplir
     *
     * @return mixed
     * @throws Exception
     */
    private function dupliqueTravauxDispositif($dispositifInit, $dispositifNew)
    {
        //Sélection de la liste des travaux pris en charge par le dispositif
        $listDispTravaux = $this->getDispositifTravauxService()->getDispositifTravauxParIdDispositif(
            $dispositifInit->getId()
        );
        /** @var DispositifTravaux $dispTravaux */
        foreach ($listDispTravaux as $dispTravaux) {
            $newTravauxDipositif = new DispositifTravaux();
            $newTravauxDipositif->setDispositif($dispositifNew);
            $newTravauxDipositif->setCodeTravaux($dispTravaux->getCodeTravaux());
            $newTravauxDipositif->setTravaux($dispTravaux->getTravaux());
            $newTravauxDipositif->setEligibiliteSpecifique($dispTravaux->getEligibiliteSpecifique());
            //Liste des règles du travaux dans le dispositif
            /** @var DispTravauxRegle $regle */
            foreach ($dispTravaux->getListeTravauxRegles() as $regle) {
                $newRegle = new DispTravauxRegle();
                $newRegle->setDispTravaux($newTravauxDipositif);
                $newRegle->setType($regle->getType());
                $newRegle->setConditionRegle($regle->getConditionRegle());
                $newRegle->setExpression($regle->getExpression());
                $newTravauxDipositif->addDispositifTravauxRegle($newRegle);
            }
            //Sauvegarde du travaux
            $this->getDispositifTravauxService()->saveDispositifTravaux($newTravauxDipositif);
        }
    }

    /**
     * Duplique les groupes de région d'un dispositif à un autre
     *
     * @param Dispositif $dispositifInit Dispositif initial à partir duquel on duplique
     * @param Dispositif $dispositifNew  Nouveau dispositif à remplir
     *
     * @return mixed
     * @throws Exception
     */
    private function dupliqueGroupeDispositif($dispositifInit, $dispositifNew)
    {
        //Sélection des groupes du diposisitif
        $listGroup = $this->getGroupeService()->getGroupeParIdDispositif($dispositifInit->getId());

        /** @var Groupe $group */
        foreach ($listGroup as $group) {
            /** @var Region $region */
            $region   = $this->getLocalisationService()->getRegionParCode($group->getCodeRegion());
            $newOrdre = $this->getGroupeService()->getMaxOrdrePourCodeRegion($group->getCodeRegion());

            $newGroup = new Groupe();
            $newGroup->setDispositif($dispositifNew);
            $newGroup->setRegion($region);
            $newGroup->setNumeroOrdre($newOrdre + 1);

            //Sauvegarde du groupe
            $this->groupeService->saveGroupe($newGroup);
        }
    }

    /**
     * Retourne le libellé du type de la règle du dispositif
     *
     * @param string $codeType Code du type de la règle
     *
     * @return string
     */
    public function getLibelleTypeRegleDispositif($codeType)
    {
        $libelle = '';

        switch ($codeType) {
            case ValeurListeConstante::TYPE_REGLE_DISPOSITIF_ELIGIBILITE:
                $libelle = 'Eligibilité';
                break;
            case ValeurListeConstante::TYPE_REGLE_DISPOSITIF_MONTANT:
                $libelle = "Montant de l'aide";
                break;
            case ValeurListeConstante::TYPE_REGLE_DISPOSITIF_PLAFOND:
                $libelle = "Plafond de l'aide";
                break;
        }

        return $libelle;
    }

    /**
     * Retourne la liste des types de règles
     *
     * @return mixed
     */
    public function getListTypeRegleDispositifPourSelect()
    {
        $tabCodeType  = [
            ValeurListeConstante::TYPE_REGLE_DISPOSITIF_ELIGIBILITE,
            ValeurListeConstante::TYPE_REGLE_DISPOSITIF_MONTANT,
            ValeurListeConstante::TYPE_REGLE_DISPOSITIF_PLAFOND
        ];
        $tabTypeRegle = [];
        foreach ($tabCodeType as $codeType) {
            $tabTypeRegle[$codeType] = $this->getLibelleTypeRegleDispositif($codeType);
        }

        return $tabTypeRegle;
    }

    /**
     * Retourne la liste des types de règles
     *
     * @return mixed
     */
    public function getListTypeRegleTravauxPourSelect()
    {
        $tabCodeType    = [
            ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_TOTAL,
            ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_MO,
            ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_FOURNITURE,
            ValeurListeConstante::TYPE_REGLE_TRAVAUX_PLAFOND
        ];
        $tabTypeRegle   = [];
        $serviceTravaux = $this->getDispositifTravauxService();
        foreach ($tabCodeType as $codeType) {
            $tabTypeRegle[$codeType] = $serviceTravaux->getLibelleTypeRegleTravaux($codeType);
        }

        return $tabTypeRegle;
    }

    /**
     * Retourne la règle du dispositif par son Id
     *
     * @param $idRegle
     *
     * @return null|DispositifRegle
     * @throws Exception
     */
    public function getDispositifRegleParId($idRegle)
    {
        try {
            return $this->dispositifRegleRepository->getDispositifRegleParId($idRegle);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de supprimer une règle de dispositif
     *
     * @param DispositifRegle $regle Regle de dispositif à supprimer
     *
     * @throws Exception
     */
    public function deleteDispositifRegle($regle)
    {
        try {
            $this->dispositifRegleRepository->deleteDispositifRegle($regle);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le formulaire de l'éditeur de règle
     *
     * @return EditeurRegleForm
     */
    public function getEditeurRegleFormPourDispositif()
    {
        /** @var EditeurRegleForm $regleForm */
        $regleForm = $this->formElementManager->get('EditeurRegleForm');
        //Ajout de la fonction permettant de gérer la modification
        $regleForm->setChangeFonctionJs();

        /** @var Select $selectTypeObject */
        $selectTypeObject = $regleForm->get('type');
        //Affectation de la liste des valeurs au select des types de règle
        $selectTypeObject->setValueOptions($this->getListTypeRegleDispositifPourSelect());

        return $regleForm;
    }

    /**
     * Retourne le formulaire de l'éditeur de règle
     *
     * @return EditeurRegleForm
     */
    public function getEditeurRegleFormPourTravaux()
    {
        /** @var EditeurRegleForm $regleForm */
        $regleForm = $this->formElementManager->get('EditeurRegleForm');
        //Ajout de la fonction permettant de gérer la modification
        $regleForm->setChangeFonctionJs();

        /** @var Select $selectTypeObject */
        $selectTypeObject = $regleForm->get('type');
        //Affectation de la liste des valeurs au select des types de règle
        $selectTypeObject->setValueOptions($this->getListTypeRegleTravauxPourSelect());

        return $regleForm;
    }

    /**
     * Permet de sauvegarder une règle de dispositif
     *
     * @param $regle
     *
     * @throws Exception
     */
    public function saveDispositifRegle($regle)
    {
        try {
            $this->dispositifRegleRepository->saveDispositifRegle($regle);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des catégorie des dispositif
     * pour l'éditeur
     * @return array
     */
    public function getListCategeorieDispositif()
    {
        $tabCategorie                                                      = [];
        $tabCategorie[ValeurListeConstante::DISPOSITIF_CATEG_FOYER]        = 'Foyer';
        $tabCategorie[ValeurListeConstante::DISPOSITIF_CATEG_LOGEMENT]     = 'Logement';
        $tabCategorie[ValeurListeConstante::DISPOSITIF_CATEG_COUT_TRAVAUX] = 'Coûts des travaux';
        $tabCategorie[ValeurListeConstante::DISPOSITIF_CATEG_MONTANT_AIDE] = 'Montants des aides du dispositif';

        return $tabCategorie;
    }

    /**
     * Retourne le formulaire de l'onglet "Travaux"
     *
     * @return DispositifTravauxForm
     */
    public function getDispositifTravauxForm()
    {
        /** @var DispositifTravauxForm $travauxForm */
        $travauxForm = $this->formElementManager->get('DispositifTravauxForm');
        //Ajout de la fonction permettant de gérer la modification
        $travauxForm->setChangeFonctionJs();
        //Définition de l'hydrator sur un tableau
        $travauxForm->setHydrator(new ArraySerializable());
        $travauxForm->setUseAsBaseFieldset(true);
        //Définition de l'hydrator sur DispositifTravaux
        /** @var DispositifTravauxFieldset $fdtDispositifTravaux */
        $fdtDispositifTravaux = $travauxForm->getFieldsets()['dispositifTravaux'];
        $fdtDispositifTravaux->setObject(new DispositifTravaux());
        $fdtDispositifTravaux->setHydrator(
            new DoctrineObject($this->entityManager, MainConstante::ENTITY_PATH . 'DispositifTravaux')
        );

        return $travauxForm;
    }

    /**
     * Retourne la vue de l'éditeur de règle
     * utilisé dans pour le dispositif
     *
     * @param EditeurRegleForm $form
     * @param                  $msgOk
     * @param                  $urlAction
     * @param                  $formName
     *
     * @return ViewModel
     * @throws Exception
     */
    public function getEditeurRegleViewModel($form, $msgOk, $msgKo, $urlAction, $formName)
    {
        //Liste des catégories pour un dispositif
        $listCategorie = $this->getRegleService()->getListeCategoriePourDispositif();

        $model      = new ViewModel(
            [
                'form'            => $form,
                'formName'        => $formName,
                'msgOK'           => $msgOk,
                'msgKo'           => $msgKo,
                'url'             => $urlAction,
                'listCategorie'   => $listCategorie,
                'listVarCateg'    => $this->getRegleService()->getListVariableParCategorie($listCategorie),
                'listValVariable' => $this->getRegleService()->getListValeurPourVariableEditeur(),
                'listOpCond'      => $this->getRegleService()->getListOperateurCondition(),
                'listOpExpr'      => $this->getRegleService()->getListOperateurExpression(),
                'aRoleAdeme'      => $this->aRoleAdeme()
            ]
        );
        $model->setTemplate('simulaides/editeur-regle.phtml');

        return $model;
    }

    /**
     * Retourne le service sur les règles desdispositifs
     *
     * @return array|object|RegleService
     */
    private function getRegleService()
    {
        if (empty($this->regleService)) {
            $this->regleService = $this->serviceLocator->get(RegleService::SERVICE_NAME);
        }
        return $this->regleService;
    }

    /**
     * Retourne le formulaire de l'historique
     *
     * @return DispositifHistoriqueForm
     */
    public function getDispositifHistoriqueForm()
    {
        /** @var DispositifHistoriqueForm $historiqueForm */
        $historiqueForm = $this->formElementManager->get('DispositifHistoriqueForm');
        //Ajout de la fonction permettant de gérer la modification
        $historiqueForm->setChangeFonctionJs();
        $historiqueForm->setObject(new DispositifHistorique());
        $historiqueForm->setHydrator(
            new DoctrineObject($this->entityManager, MainConstante::ENTITY_PATH . 'DispositifHistorique')
        );

        return $historiqueForm;
    }

    /**
     * Retourne les règles d'un dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return array|null
     * @throws Exception
     */
    public function getDispositifRegleParIdDispositif($idDispositif)
    {
        try {
            return $this->dispositifRegleRepository->getDispositifRegleParIdDispositif($idDispositif);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de récupérer les dispositifs candidats
     *
     * @param $codeCommune
     * @param $idProjet
     * @param $modeExecution
     * @param $tabIdDispositif
     * @param $offresCdpSeulement
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getDispositifCandidat($codeCommune, $idProjet, $modeExecution, $tabIdDispositif, $offresCdpSeulement= false)
    {
        try {
            $tabResult = $this->dispositifRepository->getDispositifCandidat(
                $codeCommune,
                $idProjet,
                $modeExecution,
                $tabIdDispositif,
                $offresCdpSeulement
            );
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

        return $tabResult;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }

    /**
     * @return bool
     */
    private function aRoleAdeme()
    {
        /** @var SessionContainerService $session */
        $session    = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);
        $conseiller = $session->getConseiller();
        if (null !== $conseiller) {
            $aRoleAdeme = $conseiller->aRoleADEME();
        } else {
            $aRoleAdeme = false;
        }

        return $aRoleAdeme;
    }
}
