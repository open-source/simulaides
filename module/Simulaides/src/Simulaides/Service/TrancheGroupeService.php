<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Entity\TrancheGroupe;
use Simulaides\Domain\Repository\TrancheGroupeRepository;
use Simulaides\Domain\Validator\FicheGroupeTrancheValidator;
use Simulaides\Form\TrancheRevenu\FicheGroupeTrancheForm;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Form\FormElementManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe TrancheGroupeService
 * Service permettant de gérer les groupes de tranche de revenu
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class TrancheGroupeService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'TrancheGroupeService';

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Nom de la classe
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\TrancheGroupe';

    /** @var  TrancheGroupeRepository */
    private $trancheGroupeRepository;

    /**
     * @var FormElementManager
     */
    protected $formElementManager;

    /**
     * @return \Simulaides\Domain\Repository\TrancheGroupeRepository
     */
    private function getTrancheGroupeRepository()
    {
        return $this->trancheGroupeRepository;
    }

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param FormElementManager      $formElementManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormElementManager $formElementManager,
        ServiceLocatorInterface $serviceLocator
    ) {
        $this->entityManager           = $entityManager;
        $this->trancheGroupeRepository = $entityManager->getRepository($this->className);
        $this->formElementManager      = $formElementManager;
        $this->serviceLocator          = $serviceLocator;
    }

    /**
     * Retourne la liste des groupes de tranche
     *
     * @return array
     * @throws \Exception
     */
    public function getTrancheGroupes()
    {
        try {
            //sélection de tous les groupes de tranche
            $tabTrancheGroupe = $this->getTrancheGroupeRepository()->getTrancheGroupes();
            return $tabTrancheGroupe;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Suppression du groupe de tranche
     *
     * @param string $codeGroupe Code du groupe à supprimer
     *
     * @throws \Exception
     */
    public function deleteTrancheGroupe($codeGroupe)
    {
        try {
            $this->getTrancheGroupeRepository()->deleteTrancheGroupe($codeGroupe);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne l'entité TrancheGroupe du groupe indiqué
     *
     * @param string $codeGroupe Code du groupe
     *
     * @return TrancheGroupe
     * @throws \Exception
     */
    public function getTrancheGroupe($codeGroupe)
    {
        try {
            return $this->getTrancheGroupeRepository()->getTrancheGroupe($codeGroupe);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le formulaire de la fiche d'un groupe de tranche
     *
     * @param boolean $isModification Indique que le formuaire est en modification
     *
     * @return FicheGroupeTrancheForm
     */
    public function getTrancheGroupeForm($isModification = null)
    {
        /** @var FicheGroupeTrancheForm $trancheGroupeForm */
        $trancheGroupeForm = $this->formElementManager->get('FicheGroupeTrancheForm');
        if ($isModification) {
            $trancheGroupeForm = $this->setTrancheGroupeFormInModification($trancheGroupeForm);
        }
        $hydrator = new DoctrineHydrator($this->getEntityManager(), $this->className);
        $trancheGroupeForm->setHydrator($hydrator);
        $trancheGroupeForm->setInputFilter(new FicheGroupeTrancheValidator());

        return $trancheGroupeForm;
    }

    /**
     * Permet de modifier le formulaire en modification
     *
     * @param FicheGroupeTrancheForm $trancheGroupeForm
     *
     * @return \Simulaides\Form\TrancheRevenu\FicheGroupeTrancheForm
     */
    public function setTrancheGroupeFormInModification($trancheGroupeForm)
    {
        $trancheGroupeForm->get('codeGroupe')->setAttribute('readonly', 'readonly');
        return $trancheGroupeForm;
    }

    /**
     * Permet de sauvegarder la tranche de groupe (ajout ou modif)
     *
     * @param TrancheGroupe $trancheGroupe Tranche de groupe à sauvegarder
     *
     * @throws \Exception
     */
    public function saveTrancheGroupe($trancheGroupe)
    {
        try {
            $this->getTrancheGroupeRepository()->saveTrancheGroupe($trancheGroupe);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
