<?php

namespace Simulaides\Service;

use Doctrine\ORM\EntityManager;
use Simulaides\Domain\SessionObject\ConseillerPris;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe PrisService
 * Service de gestion de l'accès à la base PRIS
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class PrisService
{
    /** Nom du service */
    const SERVICE_NAME = 'PrisService';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  EntityManager */
    private $entityManager;

    /** @var  Logger */
    private $loggerService;
    
    /**
     * Constructeur du service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param EntityManager           $entityManager
     */
    public function __construct(ServiceLocatorInterface $serviceLocator, EntityManager $entityManager)
    {
        $this->entityManager  = $entityManager;
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Permet de vérifier si l'utilisateur est identifié dans la base PRIS
     * On retourne l'identifiant de l'utilisateur
     *
     * @param string $login    Login (nom ou email PRIS) de l'utilisateur
     * @param string $motPasse Mot de passe non crypté
     *
     * @return null
     * @throws \Exception
     */
    public function getUserIdentification($login, $motPasse)
    {
        try {
            $idUser = null;

            $sql = 'SELECT users.uid, pass ';
            $sql .= 'FROM users ';
            $sql .= 'WHERE (name = :login OR mail = :login)';
            $sql .= 'AND status = :active ';

            $connexion = $this->entityManager->getConnection();
            $statement = $connexion->prepare($sql);

            $statement->bindValue('login', $login);
            $statement->bindValue('active', true);

            $statement->execute();
            $existLigne = $statement->fetch();

            //Si il existe un enregistrement, c'est que le user existe
            if ($existLigne) {
                //Vérification du mot de passe
                if ($this->verifieMotPassePris($existLigne['pass'], $motPasse)) {
                    $idUser = $existLigne['uid'];
                }
            }

            return $idUser;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner un objet ConseillerPris
     * à partir de l'identifiant
     *
     * @param integer $identifiant Identifiant PRIS du conseiller
     *
     * @return ConseillerPris
     * @throws \Exception
     */
    public function getConseiller($identifiant)
    {
        try {
            //Création d'un conseiller
            $conseiller = new ConseillerPris();
            $conseiller->setIdentifiant($identifiant);

            $sql = 'SELECT users.mail, tnom.nom_value AS nom, tprenom.prenom_value AS prenom, ';
            $sql .= 'ttel.telephone_value AS tel, GROUP_CONCAT(tregion.region_value SEPARATOR ",") AS tabIdRegion,';
            $sql .= 'trole.role, tptsContact.nom AS structure ';
            $sql .= 'FROM users ';
            $sql .= 'LEFT JOIN field_data_nom tnom ON users.uid = tnom.entity_id ';
            $sql .= 'LEFT JOIN field_data_prenom tprenom ON users.uid = tprenom.entity_id ';
            $sql .= 'LEFT JOIN field_data_telephone ttel ON users.uid = ttel.entity_id ';
            $sql .= 'LEFT JOIN field_data_region tregion ON users.uid = tregion.entity_id ';
            $sql .= 'LEFT JOIN (';
            $sql .= 'SELECT uid, GROUP_CONCAT(name SEPARATOR ",") AS role ';
            $sql .= 'FROM users_roles ';
            $sql .= 'INNER JOIN role ON users_roles.rid = role.rid ';
            $sql .= 'GROUP BY uid ';
            $sql .= ') AS trole ON users.uid = trole.uid ';
            $sql .= 'LEFT JOIN field_data_tb_pts_contact_id tptsContactId ON users.uid = tptsContactId.entity_id ';
            $sql .= 'LEFT JOIN tb_pts_contact_pris tptsContact ON tptsContact.id = tptsContactId.tb_pts_contact_id_value ';
            $sql .= 'WHERE users.uid = :identifiant ';

            $connexion = $this->entityManager->getConnection();
            $statement = $connexion->prepare($sql);

            $statement->bindValue('identifiant', $identifiant);

            $statement->execute();
            $existLigne = $statement->fetch();

            if ($existLigne) {
                if (isset($existLigne['nom'])) {
                    $conseiller->setNom($existLigne['nom']);
                }
                if (isset($existLigne['prenom'])) {
                    $conseiller->setPrenom($existLigne['prenom']);
                }
                if (isset($existLigne['mail'])) {
                    $conseiller->setMail($existLigne['mail']);
                }
                if (isset($existLigne['tel'])) {
                    $conseiller->setTel($existLigne['tel']);
                }
                if (isset($existLigne['tabIdRegion'])) {
                    $tabIdRegion = explode(",", $existLigne['tabIdRegion']);
                    $codeRegion  = null;
                    if (count($tabIdRegion) == 1) {
                        $codeRegion = $tabIdRegion[0];
                    }
                    $conseiller->setCodeRegion($codeRegion);
                }
                if (isset($existLigne['role'])) {
                    $tabRole = explode(",", $existLigne['role']);
                    $conseiller->setListRole($tabRole);
                }
                if (isset($existLigne['structure'])) {
                    $conseiller->setStructure($existLigne['structure']);
                }
            }

            return $conseiller;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de vérifier le mot de passe PRIS
     *
     * @param string $motPassePris Mot de base de la base Pris
     * @param string $motPasse     Mot de passe non crypté
     *
     * @return bool
     */
    private function verifieMotPassePris($motPassePris, $motPasse)
    {
        $motPasseCrypte = '';
        $stored_hash    = $motPassePris;
        $type           = substr($stored_hash, 0, 3);
        if ($type == '$S$') {
            //encodage du mot de passe saisi
            $motPasseCrypte = $this->passwordCrypt('sha512', $motPasse, $stored_hash);
        }

        //Ok si le mot de passe saisie encodé est identique au mot de passe en bdd
        return ($motPassePris == $motPasseCrypte);
    }

    /**
     * Cryptage du mot de passe
     * Copié depuis le code Drupal user_check_password()
     * https://api.drupal.org/api/drupal/includes!password.inc/function/user_check_password/7
     *
     * @param $algo
     * @param $password
     * @param $setting
     *
     * @return bool|string
     */
    private function passwordCrypt($algo, $password, $setting)
    {
        // Prevent DoS attacks by refusing to hash large passwords.
        if (strlen($password) > 512) {
            return false;
        }
        // The first 12 characters of an existing hash are its setting string.
        $setting = substr($setting, 0, 12);

        if ($setting[0] != '$' || $setting[2] != '$') {
            return false;
        }
        $count_log2 = $this->passwordGetCountLog2($setting);
        // Hashes may be imported from elsewhere, so we allow != DRUPAL_HASH_COUNT
        if ($count_log2 < 7 || $count_log2 > 30) {
            return false;
        }
        $salt = substr($setting, 4, 8);
        // Hashes must have an 8 character salt.
        if (strlen($salt) != 8) {
            return false;
        }

        // Convert the base 2 logarithm into an integer.
        $count = 1 << $count_log2;

        // We rely on the hash() function being available in PHP 5.2+.
        $hash = hash($algo, $salt . $password, true);
        do {
            $hash = hash($algo, $hash . $password, true);
        } while (--$count);

        $len    = strlen($hash);
        $output = $setting . $this->passwordBase64Encode($hash, $len);
        // _password_base64_encode() of a 16 byte MD5 will always be 22 characters.
        // _password_base64_encode() of a 64 byte sha512 will always be 86 characters.
        $expected = 12 + ceil((8 * $len) / 6);
        return (strlen($output) == $expected) ? substr($output, 0, 55) : false;
    }

    /**
     * @param $setting
     *
     * @return int
     */
    private function passwordGetCountLog2($setting)
    {
        $itoa64 = $this->passwordItoa64();
        return strpos($itoa64, $setting[3]);
    }

    /**
     * @return string
     */
    private function passwordItoa64()
    {
        return './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    }

    /**
     * @param $input
     * @param $count
     *
     * @return string
     */
    private function passwordBase64Encode($input, $count)
    {
        $output = '';
        $i      = 0;
        $itoa64 = $this->passwordItoa64();
        do {
            $value = ord($input[$i++]);
            $output .= $itoa64[$value & 0x3f];
            if ($i < $count) {
                $value |= ord($input[$i]) << 8;
            }
            $output .= $itoa64[($value >> 6) & 0x3f];
            if ($i++ >= $count) {
                break;
            }
            if ($i < $count) {
                $value |= ord($input[$i]) << 16;
            }
            $output .= $itoa64[($value >> 12) & 0x3f];
            if ($i++ >= $count) {
                break;
            }
            $output .= $itoa64[($value >> 18) & 0x3f];
        } while ($i < $count);

        return $output;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
