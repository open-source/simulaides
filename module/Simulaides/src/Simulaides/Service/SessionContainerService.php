<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 20/02/15
 * Time: 16:09
 */

namespace Simulaides\Service;

use Simulaides\Constante\SessionConstante;
use Simulaides\Domain\SessionObject\ConseillerPris;
use Simulaides\Domain\SessionObject\Simulation\SimulationResultat;
use Simulaides\Domain\SessionObject\Travaux\TravauxSession;
use Zend\Session\Container;
use Zend\Session\SessionManager;

/**
 * Class SessionContainerService
 * @package Simulaides\Service
 */
class SessionContainerService extends Container
{
    /** Nom du service */
    const SERVICE_NAME = 'SessionContainer';

    /**
     * @param SessionManager $sessionManager
     */
    public function __construct(SessionManager $sessionManager)
    {
        parent::__construct(SessionConstante::CONTAINER_NAME, $sessionManager);
    }

    /**
     * @param $key
     * @param $value
     */
    public function addOffset($key, $value)
    {
        $this->offsetSet($key, $value);
    }

    /**
     * @return bool
     */
    public function checkCGU()
    {
        if (!$this->offsetExists(SessionConstante::CGU) || ($this->offsetGet(SessionConstante::CGU) !== true)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function estJsActif()
    {
        if (!$this->offsetExists(SessionConstante::JS_ACTIVE) || ($this->offsetGet(
                    SessionConstante::JS_ACTIVE
                ) !== true)
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param TravauxSession $travauxSession
     */
    public function addTravaux(TravauxSession $travauxSession)
    {
        if ($this->offsetExists(SessionConstante::TRAVAUX)) {
            $travauxArray                                    = $this->offsetGet(SessionConstante::TRAVAUX);
            $travauxArray[$travauxSession->getCodeTravaux()] = $travauxSession;
            $this->offsetSet(SessionConstante::TRAVAUX, $travauxArray);
        } else {
            $travauxArray = [$travauxSession->getCodeTravaux() => $travauxSession];
            $this->addOffset(SessionConstante::TRAVAUX, $travauxArray);
        }
    }

    /**
     * @param string $codeTravaux
     *
     * @return null|TravauxSession
     */
    public function getTravaux($codeTravaux)
    {
        if ($this->offsetExists(SessionConstante::TRAVAUX)) {
            $travauxArray = $this->offsetGet(SessionConstante::TRAVAUX);
            if (isset($travauxArray[$codeTravaux])) {
                return $travauxArray[$codeTravaux];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Retourne la liste des travaux sélectionnés dans le projet
     * @return array|mixed
     */
    public function getListTravaux()
    {
        $listTravaux = [];
        if ($this->offsetExists(SessionConstante::TRAVAUX)) {
            $listTravaux = $this->offsetGet(SessionConstante::TRAVAUX);
        }

        return $listTravaux;
    }

    /**
     * @param $codeTravaux
     */
    public function deleteTravaux($codeTravaux)
    {
        $travauxArray = $this->offsetGet(SessionConstante::TRAVAUX);
        if (isset($travauxArray[$codeTravaux])) {
            unset ($travauxArray[$codeTravaux]);
            $this->offsetSet(SessionConstante::TRAVAUX, $travauxArray);
        }
    }

    /**
     * Permet de supprimer tous les travaux de la session
     */
    public function deleteAllTravaux()
    {
        //Si il existe la clé dans la session, on la supprime
        if ($this->offsetExists(SessionConstante::TRAVAUX)) {
            $this->offsetSet(SessionConstante::TRAVAUX, []);
        }
        //Si il existe la clé dans la session, on la supprime
        if ($this->offsetExists(SessionConstante::CODES_TRAVAUX)) {
            $this->offsetSet(SessionConstante::CODES_TRAVAUX, []);
        }

    }

    /**
     * Retourne la liste des travaux sélectionnés
     *
     * @return array|null
     */
    public function getSelectedTravaux()
    {
        if ($this->offsetExists(SessionConstante::TRAVAUX)) {
            $travauxArray = $this->offsetGet(SessionConstante::TRAVAUX);

            return array_keys($travauxArray);
        }

        return [];
    }

    /**
     * Permet d'indiquer si un conseiller est connecté
     *
     * @return bool|mixed
     */
    public function estConseillerConnecte()
    {
        $estConnecte = false;

        if ($this->offsetExists(SessionConstante::ADMIN_CONNECTEE)) {
            $estConnecte = $this->offsetGet(SessionConstante::ADMIN_CONNECTEE);
        }

        return $estConnecte;
    }

    /**
     * Permet de déconnecter un conseiller
     */
    public function deconnecteConseiller()
    {
        if ($this->offsetExists(SessionConstante::ADMIN_CONNECTEE)) {
            $this->offsetSet(SessionConstante::ADMIN_CONNECTEE, false);
        }
        $this->effaceConseiller();
    }

    /**
     * Retourne le prénom et le nom du conseiller connecté
     *
     * @return string
     */
    public function getIdentiteConseiller()
    {
        $nomPrenom = '';

        if ($this->estConseillerConnecte()) {
            $conseiller = $this->getConseiller();
            $nomPrenom  = $conseiller->getPrenom();
            if (!empty($nomPrenom)) {
                $nomPrenom .= ' ';
            }
            $nomPrenom .= $conseiller->getNom();
        }

        return $nomPrenom;
    }

    /**
     * Permet de setter la couleur de fond de titre Niveau 1
     *
     * @param string $color Code couleur
     */
    public function setCouleurFondTitreN1($color)
    {
        $this->addOffset(SessionConstante::NAME_FOND_TITRE_N1, $color);
    }

    /**
     * Retourne la couleur de fond des titres de Niveau 1
     * @return mixed|string
     */
    public function getCouleurFondTitreN1()
    {
        if ($this->estConseillerConnecte()) {
            //Site conseiller => couleur fixe
            $color = SessionConstante::FOND_TITRE_N1_PRIS;
        } else {
            //Site du particulier => couleur paramétrable
            $color = SessionConstante::FOND_TITRE_N1_DEFAUT;
            if ($this->offsetExists(SessionConstante::NAME_FOND_TITRE_N1)) {
                $color = $this->offsetGet(SessionConstante::NAME_FOND_TITRE_N1);
            }
        }

        return $color;
    }

    /**
     * Permet de setter la couleur de fond de titre Niveau 2
     *
     * @param string $color Code couleur
     */
    public function setCouleurFondTitreN2($color)
    {
        $this->addOffset(SessionConstante::NAME_FOND_TITRE_N2, $color);
    }

    /**
     * Retourne la couleur de fond des titres de Niveau 2
     * @return mixed|string
     */
    public function getCouleurFondTitreN2()
    {
        if ($this->estConseillerConnecte()) {
            //Site conseiller => couleur fixe
            $color = SessionConstante::FOND_TITRE_N2_PRIS;
        } else {
            //Site du particulier => couleur paramétrable
            $color = SessionConstante::FOND_TITRE_N2_DEFAUT;
            if ($this->offsetExists(SessionConstante::NAME_FOND_TITRE_N2)) {
                $color = $this->offsetGet(SessionConstante::NAME_FOND_TITRE_N2);
            }
        }

        return $color;
    }

    /**
     * Permet de setter la couleur de fond de titre Niveau 3
     *
     * @param string $color Code couleur
     */
    public function setCouleurFondTitreN3($color)
    {
        $this->addOffset(SessionConstante::NAME_FOND_TITRE_N3, $color);
    }

    /**
     * Retourne la couleur de fond des titres de Niveau 3
     * @return mixed|string
     */
    public function getCouleurFondTitreN3()
    {
        if ($this->estConseillerConnecte()) {
            //Site conseiller => couleur fixe
            $color = SessionConstante::FOND_TITRE_N3_PRIS;
        } else {
            //Site du particulier => couleur paramétrable
            $color = SessionConstante::FOND_TITRE_N3_DEFAUT;
            if ($this->offsetExists(SessionConstante::NAME_FOND_TITRE_N3)) {
                $color = $this->offsetGet(SessionConstante::NAME_FOND_TITRE_N3);
            }
        }

        return $color;
    }

    /**
     * Permet de setter la couleur de fond des boutons
     *
     * @param string $color Code couleur
     */
    public function setCouleurFondBouton($color)
    {
        $this->addOffset(SessionConstante::NAME_FOND_BOUTON, $color);
    }

    /**
     * Permet de setter la version
     *
     * @param string $color Code couleur
     */
    public function setVersion($version)
    {
        $this->addOffset(SessionConstante::VERSION, $version);
    }

    /**
     * Retourne la couleur de fond des boutons
     * @return mixed|string
     */
    public function getCouleurFondBouton()
    {
        if ($this->estConseillerConnecte()) {
            //Site conseiller => couleur fixe
            $color = SessionConstante::FOND_BOUTON_PRIS;
        } else {
            //Site du particulier => couleur paramétrable
            $color = SessionConstante::FOND_BOUTON_DEFAUT;

            if ($this->offsetExists(SessionConstante::NAME_FOND_BOUTON)) {
                $color = $this->offsetGet(SessionConstante::NAME_FOND_BOUTON);
            }
        }

        return $color;
    }

    /**
     * Retourne Si l'accès à PRIS a été demandé
     *
     * @return bool|mixed
     */
    public function accesPris()
    {
        $acces = false;
        if ($this->offsetExists(SessionConstante::ACCESS_PRIS)) {
            $acces = $this->offsetGet(SessionConstante::ACCESS_PRIS);
        }
        return $acces;
    }

    /**
     * Permet de mémoriser la catégorie de travaux ouverte
     * dans l'accordéon
     *
     * @param $codeTravaux
     */
    public function setTravauxOpened($codeTravaux)
    {
        if (!$this->offsetExists(SessionConstante::TRAVAUX_OPEN)) {
            $this->addOffset(SessionConstante::TRAVAUX_OPEN, $codeTravaux);
        } else {
            $this->offsetSet(SessionConstante::TRAVAUX_OPEN, $codeTravaux);
        }
    }

    /**
     * Permet de retourner les informations du conseiller
     *
     * @return ConseillerPris|null
     */
    public function getConseiller()
    {
        $conseiller = null;
        if ($this->offsetExists(SessionConstante::CONSEILLER)) {
            $conseiller = $this->offsetGet(SessionConstante::CONSEILLER);
        }

        return $conseiller;
    }

    /**
     * Permet de supprimer le conseiller en session
     */
    public function effaceConseiller()
    {
        if ($this->offsetExists(SessionConstante::CONSEILLER)) {
            $this->offsetSet(SessionConstante::CONSEILLER, null);
        }
    }

    /**
     * @param array $tabIdDispositifTest
     */
    public function setTabIdDispositifTest($tabIdDispositifTest)
    {
        $this->offsetSet('tabIdDispositifTest', $tabIdDispositifTest);
    }

    /**
     * @return array
     */
    public function getTabIdDispositifTest()
    {
        return $this->offsetGet('tabIdDispositifTest');
    }

    /**
     * @param string $modeExecution
     */
    public function setModeExecution($modeExecution)
    {
        $this->offsetSet('modeExecution', $modeExecution);
    }

    /**
     * @return string
     */
    public function getModeExecution()
    {
        return $this->offsetGet('modeExecution');
    }

    /**
     * @param SimulationResultat $simulationResultat
     */
    public function setSimulationResultat($simulationResultat)
    {
        $this->addOffset('simulationResultat', $simulationResultat);
    }

    /**
     * @return SimulationResultat
     */
    public function getSimulationResultat()
    {
        return $this->offsetGet('simulationResultat');
    }

    /**
     * Efface les couleur de la session
     * @return void
     */
    public function clearColor()
    {
        $this->offsetUnset(SessionConstante::NAME_FOND_TITRE_N1);
        $this->offsetUnset(SessionConstante::FOND_TITRE_N1_DEFAUT);
        $this->offsetUnset(SessionConstante::FOND_TITRE_N1_PRIS);

        $this->offsetUnset(SessionConstante::NAME_FOND_TITRE_N2);
        $this->offsetUnset(SessionConstante::FOND_TITRE_N2_DEFAUT);
        $this->offsetUnset(SessionConstante::FOND_TITRE_N2_PRIS);

        $this->offsetUnset(SessionConstante::NAME_FOND_TITRE_N3);
        $this->offsetUnset(SessionConstante::FOND_TITRE_N3_DEFAUT);
        $this->offsetUnset(SessionConstante::FOND_TITRE_N3_PRIS);

        $this->offsetUnset(SessionConstante::NAME_FOND_BOUTON);
        $this->offsetUnset(SessionConstante::FOND_BOUTON_DEFAUT);
        $this->offsetUnset(SessionConstante::FOND_BOUTON_PRIS);

    }

    /**
     * Permet de retourner la version de l'application
     *
     * @return ConseillerPris|null
     */
    public function getVersion()
    {
        return  $this->offsetGet(SessionConstante::VERSION);
    }

    /**
     * @param $region
     */
    public function setFiltresDispositifRegion($region)
    {
        $this->addOffset('filtresDispositifRegion', $region);
    }

    /**
     * @param $intitule
     */
    public function setFiltresDispositifIntitule($intitule)
    {
        $this->addOffset('filtresDispositifIntitule', $intitule);
    }

    /**
     * @param $financeur
     */
    public function setFiltresDispositifFinanceur($financeur)
    {
        $this->addOffset('filtresDispositifFinanceur', $financeur);
    }

    /**
     * @param $type
     */
    public function setFiltresDispositifType($type)
    {
        $this->addOffset('filtresDispositifType', $type);
    }

    /**
     * @param $etat
     */
    public function setFiltresDispositifEtat($etat)
    {
        $this->addOffset('filtresDispositifEtat', $etat);
    }

    /**
     * @param $validite
     */
    public function setFiltresDispositifValidite($validite)
    {
        $this->addOffset('filtresDispositifValidite', $validite);
    }

    /**
     * @param $codeTravaux
     */
    public function setFiltresDispositifCodeTravaux($codeTravaux)
    {
        $this->addOffset('filtresDispositifCodeTravaux', $codeTravaux);
    }

    /**
     * @param $codeTravaux
     */
    public function setFiltresDispositifCodeDepartement($codeDepartement)
    {
        $this->addOffset('filtresDispositifCodeDepartement', $codeDepartement);
    }

    /**
     * @param $codeTravaux
     */
    public function setFiltresDispositifCodeCommune($codeCommune)
    {
        $this->addOffset('filtresDispositifCodeCommune', $codeCommune);
    }

    /**
     * @return mixed|null
     */
    public function getFiltresDispositifRegion()
    {
        return $this->offsetGet('filtresDispositifRegion');
    }

    /**
     * @return mixed|null
     */
    public function getFiltresDispositifIntitule()
    {
        return $this->offsetGet('filtresDispositifIntitule');
    }

    /**
     * @return mixed|null
     */
    public function getFiltresDispositifFinanceur()
    {
        return $this->offsetGet('filtresDispositifFinanceur');
    }

    /**
     * @return mixed|null
     */
    public function getFiltresDispositifType()
    {
        return $this->offsetGet('filtresDispositifType');
    }

    /**
     * @return mixed|null
     */
    public function getFiltresDispositifEtat()
    {
        return $this->offsetGet('filtresDispositifEtat');
    }

    /**
     * @return mixed|null
     */
    public function getFiltresDispositifValidite()
    {
        return $this->offsetGet('filtresDispositifValidite');
    }

    /**
     * @return mixed|null
     */
    public function getFiltresDispositifCodeTravaux()
    {
        return $this->offsetGet('filtresDispositifCodeTravaux');
    }

    /**
     * @return mixed|null
     */
    public function getFiltresDispositifCodeDepartement()
    {
        return $this->offsetGet('filtresDispositifCodeDepartement');
    }

    /**
     * @return mixed|null
     */
    public function getFiltresDispositifCodeCommune()
    {
        return $this->offsetGet('filtresDispositifCodeCommune');
    }
}
