<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 15:13
 */

namespace Simulaides\Service;

use Simulaides\Domain\SessionObject\ProjetSession;
use Zend\Session\Container;
use Zend\Session\SessionManager;

/**
 * Class ProjetSessionContainer
 * Cette classe permet de gérer la sauvegarde du projet dans la session
 *
 * @package Simulaides\Service
 */
class ProjetSessionContainer extends Container
{
    /** Nom du service */
    const SERVICE_NAME = 'ProjetSessionContainer';

    /**
     * @param SessionManager $sessionManager
     */
    public function __construct(SessionManager $sessionManager)
    {
        parent::__construct('projet', $sessionManager);
    }

    /**
     * @param ProjetSession $projet
     */
    public function saveProjet(ProjetSession $projet)
    {
        $this->projet = $projet;
    }

    /**
     * @return ProjetSession
     */
    public function getProjet()
    {
        return $this->projet;
    }
}
