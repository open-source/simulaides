<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Repository\CoeffSurfaceRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe CoeffSurfaceService
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class CoeffSurfaceService
{
    /** Nom du service */
    const SERVICE_NAME = 'CoeffSurfaceService';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  CoeffSurfaceRepository */
    private $coeffSurfaceRepository;

    /** @var  LoggerFactory */
    private $loggerService;

    /**
     * Nom de la classe des coeff surface
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\CoeffSurface';

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager          = $entityManager;
        $this->coeffSurfaceRepository = $entityManager->getRepository($this->className);
        $this->serviceLocator         = $serviceLocator;
    }

    /**
     * Permet de retourner le coefficient de surface
     * pour un type de logement et un code surface
     *
     * @param string $codeTypeLogement Code du type de logement
     * @param string $codeSurface      Code de la surface
     * @param        $codeTravaux
     *
     * @return int
     * @throws \Exception
     */
    public function getCoefficientSurfacePourTypeLogement($codeTypeLogement, $codeSurface, $codeTravaux)
    {
        $coefficient = 0;
        try {
            $result = $this->coeffSurfaceRepository->getCoefficientSurfacePourTypeLogement(
                $codeTypeLogement,
                $codeSurface,
                $codeTravaux
            );
            if (!empty($result)) {
                $coefficient = $result['coefficient'];
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

        return $coefficient;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
