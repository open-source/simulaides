<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 20/02/15
 * Time: 09:30
 */

namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Entity\Version;
use Simulaides\Domain\Repository\VersionRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class VersionService
 * @package Simulaides\Service
 */
class VersionService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'VersionService';

    /**
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\Version';

    /** @var  VersionRepository */
    private $versionRepository;

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;


    /**
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager         = $entityManager;
        $this->versionRepository = $entityManager->getRepository($this->className);
        $this->serviceLocator        = $serviceLocator;
    }

    /**
     * Retourne la tanche de revenu
     *
     * @param $codeTranche
     *
     * @return Version
     * @throws \Exception
     */
    public function getVersion()
    {
        try {
            $version = $this->getVersionRepository()->getVersionApp();
            if(isset($version)){
                $version = $version[0];
                return $version->getVersion();
            }else{
                return '';
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @return VersionRepository
     */
    private function getVersionRepository()
    {
        return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\Version');
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }

}
