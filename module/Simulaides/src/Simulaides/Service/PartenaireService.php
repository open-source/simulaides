<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Simulaides\Domain\Entity\Partenaire;
use Simulaides\Domain\Entity\PrixAdmin;
use Simulaides\Domain\Entity\Produit;
use Simulaides\Domain\Repository\PartenaireRepository;
use Simulaides\Domain\Repository\PrixAdminRepository;
use Simulaides\Domain\Repository\ProduitRepository;
use Simulaides\Domain\Repository\SaisieProduitRepository;
use Simulaides\Domain\SessionObject\Simulation\CoutsProduit;
use Simulaides\Form\Partenaire\PartenaireForm;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Form\FormElementManager;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\HelperPluginManager;

/**
 * Classe PartenaireService
 * @package   Simulaides\Service
 */
class PartenaireService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'PartenaireService';

    /** @var  PartenaireRepository */
    private $partenaireRepository;

    /**
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\Partenaire';

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var FormElementManager $formElementManager */
    private $formElementManager;

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator, FormElementManager $formElementManager)
    {
        $this->partenaireRepository    = $entityManager->getRepository($this->className);
        $this->entityManager           = $entityManager;
        $this->serviceLocator          = $serviceLocator;
        $this->formElementManager      = $formElementManager;
    }


    /**
     * @return Partenaire[]
     */
    public function getAllPartenaire(){
        return $this->partenaireRepository->findAll();
    }

    /**
     * @param Partenaire $partenaire
     * @return PartenaireForm
     */
    public function getPartenaireForm($partenaire = null){
        /** @var PartenaireForm $partenaireForm */
        $partenaireForm = $this->formElementManager->get('PartenaireForm');

        $urlHelper = $this->serviceLocator->get('viewHelperManager')->get('url');

        //Action du formulaire
        if($partenaire){
            $partenaireForm->setAttribute('action', $urlHelper('admin/partenaire-modif', ['idPartenaire' => $partenaire->getId()]));
        }else{
            $partenaireForm->setAttribute('action', $urlHelper('admin/partenaire-ajout', []));
        }

        //Hydrator
        $hydrator = new DoctrineObject($this->getEntityManager());
        $partenaireForm->setHydrator($hydrator);

        return $partenaireForm;
    }

    public function getPartenaireById($idPartenaire){
        return $this->partenaireRepository->find($idPartenaire);
    }

    /**
     * @param string $cleApi - clé api du partenaire
     * @return $tabListe : liste de noms de domaine des partenaires ayant la clé la clé API
     */
    public function getListeUrlByCleApi($cleApi){
        $tabListe = array();
        $partenaires = $this->partenaireRepository->findAll();
        foreach($partenaires as $partenaire) {
           if( $partenaire->getCleApi() == $cleApi){
               $tabListe[$partenaire->getUrl()] = $partenaire->getCleApi();
           }
        }
        return $tabListe;
    }

    /**
     * @param string $url - Nom de domaine du partenaire
     * @return Partenaire
     */
    public function getPartenaireByUrl($url){
        return $this->partenaireRepository->findOneBy(['url'=>$url]);
    }
}
