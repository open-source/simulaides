<?php


namespace Simulaides\Service;


use Assetic\Exception\Exception;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\Entity\ProjetChoixDispositif;
use Simulaides\Domain\Repository\DispositifRepository;
use Simulaides\Domain\Repository\ProjetChoixDispositifRepository;
use Simulaides\Domain\Repository\TravauxRepository;
use Simulaides\Domain\SessionObject\ProjetSession;
use Simulaides\Form\OffresCee\OffresCeeForm;
use Zend\Form\FormElementManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\ArraySerializable;

class OffresCeeService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'OffresCeeService';

    /**
     * @var FormElementManager
     */
    protected $formElementManager;

    /**
     * @var ProjetSessionContainer
     */
    protected $projetSession;

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  SessionContainerService */
    private $sessionContainer;

    /**
     * @var ObjectRepository
     */
    private $dispostifRepository;

    /**
     * @var ObjectRepository
     */
    private $projetChoixDispositif;

    /**
     * @var array|object
     */
    private $projetService;

    /**
     * @param EntityManagerInterface $entityManager
     * @param FormElementManager $formElementManager
     * @param ProjetSessionContainer $projetSessionContainer
     * @param SessionContainerService $sessionContainer
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormElementManager $formElementManager,
        ProjetSessionContainer $projetSessionContainer,
        SessionContainerService $sessionContainer,
        ServiceLocatorInterface $serviceLocator
    )
    {
        $this->dispostifRepository = $entityManager->getRepository('Simulaides\Domain\Entity\Dispositif');
        $this->projetChoixDispositif = $entityManager->getRepository('Simulaides\Domain\Entity\ProjetChoixDispositif');
        $this->sessionContainer = $sessionContainer;
        $this->formElementManager = $formElementManager;
        $this->projetSession = $projetSessionContainer;
        $this->entityManager = $entityManager;
        $this->serviceLocator = $serviceLocator;
    }

    public function getOffresCeeForm($listeTravauxAvecOffre)
    {
        /** @var OffresCeeForm $offresCeeForm */
        $offresCeeForm = $this->formElementManager->get('OffresCeeForm', $listeTravauxAvecOffre);
        $offresCeeForm->setHydrator(new ArraySerializable());

        return $offresCeeForm;

    }

    public function isFormValid($data)
    {
        return count($data) > 1;
    }

    public function insertOffresSelectionnees($data)
    {
        try {
            /** @var ProjetSession $projetSession */
            $projetSession = $this->getProjetService()->getProjetSession()->getProjet();

            /** @var Projet $projet */
            $projet = $this->getProjetService()->getProjetParId($projetSession->getId());

            $this->getProjetChoixDispositifRepository()->deleteByIdProjet($projet->getId());

            foreach ($data as $key => $value) {
                if ($key != 'PHPSESSID') {
                    /** @var Dispositif $dispositif */
                    $dispositif = $this->getDispositifRepository()->findOneBy(['id' => $value]);

                    $travaux = $this->getTravauxRepository()->findOneBy(['code' => $key]);

                    $choixDispositif = new ProjetChoixDispositif();
                    $choixDispositif->setDispositif($dispositif);
                    $choixDispositif->setTravaux($travaux);
                    $choixDispositif->setProjet($projet);

                    $this->getProjetChoixDispositifRepository()->saveProjetChoixDispositif($choixDispositif);
                }
            }
        } catch (Exception $e) {
        }
    }
    public function insertCoupDePouceFromPopup($idProjet,$projet,$dispositifCdp,$codeTravaux)
    {
        try {
            $dispositif = $this->getDispositifRepository()->findOneBy(['id' => $dispositifCdp]);

            $travaux = $this->getTravauxRepository()->findOneBy(['code' => $codeTravaux]);

            $choixDispositif = new ProjetChoixDispositif();
            $choixDispositif->setDispositif($dispositif);
            $choixDispositif->setTravaux($travaux);
            $this->getProjetChoixDispositifRepository()->deleteByIdProjetAndTravaux($idProjet,$codeTravaux);


                    /** @var Dispositif $dispositif */

                    $choixDispositif = new ProjetChoixDispositif();
                    $choixDispositif->setDispositif($dispositif);
                    $choixDispositif->setTravaux($travaux);
                    $choixDispositif->setProjet($projet);

                    $this->getProjetChoixDispositifRepository()->saveProjetChoixDispositif($choixDispositif);

        } catch (Exception $e) {
        }
    }

    /**
     * @return DispositifRepository
     * @throws Exception
     */
    private function getDispositifRepository()
    {
        try {
            return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\Dispositif');
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * @return TravauxRepository
     * @throws Exception
     */
    private function getTravauxRepository()
    {
        try {
            return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\Travaux');
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * @return ProjetChoixDispositifRepository
     * @throws Exception
     */
    private function getProjetChoixDispositifRepository()
    {
        try {
            return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\ProjetChoixDispositif');
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * @return EntityManagerInterface
     * @throws Exception
     */
    public function getEntityManager()
    {
        try {
            return $this->entityManager;
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * Permet de récupérer le service sur les projets
     *
     * @return array|object|ProjetService
     */
    private function getProjetService()
    {
        if (empty($this->projetService)) {
            $this->projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
        }
        return $this->projetService;
    }

    /**
     * @return array
     */
    public function getOffresSelectionnees()
    {
        if($this->getProjetService()->getProjetSession()->getProjet()){
            return $this->getProjetChoixDispositifRepository()
                        ->getByIdProjet($this->getProjetService()->getProjetSession()->getProjet()->getId());
        }
        return [];
    }

    /**
     * @return array
     */
    public function purgeOffresSelectionnees()
    {
        if($this->getProjetService()->getProjetSession()->getProjet()){
            return $this->getProjetChoixDispositifRepository()
                        ->purgeOffresSelectionnees($this->getProjetService()->getProjetSession()->getProjet()->getId());
        }
    }


    /**
     * Permet de récupérer le service sur les projets
     *
     * @return array
     */
    public function getOffresByIdProjetAndIdDispositif($idProjet,$idDispositif)
    {

       return $this->getProjetChoixDispositifRepository()-> getOffresByIdProjetAndIdDispositif($idProjet,$idDispositif);

    }
    /**
     * Permet de récupérer le service sur les projets
     *
     * @return array
     */
    public function getOffresByIdProjetAndCodeTravaux($idProjet,$codeTravau)
    {

        return $this->getProjetChoixDispositifRepository()-> getOffresByIdProjetAndCodeTravaux($idProjet,$codeTravau);

    }

}
