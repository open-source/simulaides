<?php
namespace Simulaides\Service;

use Simulaides\Constante\RegleVariableConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\TravauxConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\SessionObject\Simulation\AideTravauxSimul;
use Simulaides\Domain\SessionObject\Simulation\ProjetSimul;
use Simulaides\Logger\LoggerSimulation;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe CalculateurCeeService
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class CalculateurCeeService
{
    /** Nom du service */
    const SERVICE_NAME = 'CalculateurCeeService';

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  KwhCumacService */
    private $kwhCumacService;

    /** @var  ValeurListeService */
    private $valeurListeService;

    /** @var  Logger */
    private $loggerService;

    /** @var array */
    private $typesFenetres = array(
        1 => RegleVariableConstante::VAR_PARAM_TECH_NB_FEN_ALU,
        2 => RegleVariableConstante::VAR_PARAM_TECH_NB_FEN_BOIS,
        3 => RegleVariableConstante::VAR_PARAM_TECH_NB_FEN_PVC,
        4 => RegleVariableConstante::VAR_PARAM_TECH_NB_POR_FEN_ALU,
        5 => RegleVariableConstante::VAR_PARAM_TECH_NB_POR_FEN_BOIS,
        6 => RegleVariableConstante::VAR_PARAM_TECH_NB_POR_FEN_PVC
    );

    /**
     * Constructeur
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Méthode qui calcul l'aide CEE d'un travaux
     *
     * @param    ProjetSimul      $projet
     * @param    AideTravauxSimul $aideTravaux
     *
     * @throws \Exception
     */
    public function calculMontantCEETravaux($projet, $aideTravaux)
    {
        try {
            $travaux          = $aideTravaux->getTravaux();
            $codeTravaux      = $travaux->getCode();
            $codeTypeLogement = $projet->getCodeTypeLogement();
            $codeSurface      = $projet->getCodeSurface();
            $codeEnergie      = $projet->getCodeEnergieParent();
            $zone             = $projet->getZoneClimatique();

            //Service pour le Kwh Cumac
            $kwhCumacService = $this->getKwhCumacService();

            //Récupération du coefficient
            $coeffKwhEuros = $this->getValeurListeService()->getCoefficientKwhCumac();
            if (empty($coeffKwhEuros)) {
                $coeffKwhEuros = 0;
            }

            $nbKwh = 0;
            $nbu   = 0;
            switch ($codeTravaux) {
                case TravauxConstante::TRAVAUX_T01_CHGE_FENETRE:
                    foreach ($this->typesFenetres as $typeFenetre) {
                        if ($travaux->getValeurParamTech($typeFenetre) !== null) {
                            $nbu = $nbu + (int)$travaux->getValeurParamTech($typeFenetre);
                        }
                    }
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        $codeEnergie,
                        null,
                        $zone
                    );
                    $nbKwh = $nbKwh * $nbu;
                    break;

                case TravauxConstante::TRAVAUX_T02_ISOL_MUR_INT:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        $codeEnergie,
                        null,
                        $zone
                    );
                    $nbu   = $travaux->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURFACE_ISOLANT_MURS_INT
                    );
                    $nbKwh = $nbKwh * $nbu;
                    break;

                case TravauxConstante::TRAVAUX_T03_ISOL_MUR_EXT:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        $codeEnergie,
                        null,
                        $zone
                    );
                    $nbu   = $travaux->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURFACE_ISOLANT_MURS_EXT
                    );
                    $nbKwh = $nbKwh * $nbu;
                    break;

                case TravauxConstante::TRAVAUX_T04_ISOL_COMBLE:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        null,
                        null,
                        $zone
                    );
                    $nbu   = $travaux->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURFACE_ISOLANT_COMBLES
                    );
                    $nbKwh = $nbKwh * $nbu;
                    break;

                case TravauxConstante::TRAVAUX_T05_ISOL_TOIT_INT:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        null,
                        null,
                        $zone
                    );
                    $nbu   = $travaux->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURFACE_ISOLANT_TOIT_INT
                    );
                    $nbKwh = $nbKwh * $nbu;
                    break;

                case TravauxConstante::TRAVAUX_T06_ISOL_TOIT_EXT:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        null,
                        null,
                        $zone
                    );
                    $nbu   = $travaux->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURFACE_ISOLANT_TOIT_EXT
                    );
                    $nbKwh = $nbKwh * $nbu;
                    break;

                case TravauxConstante::TRAVAUX_T07_ISOL_TOIT_TERRASSE:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        $codeEnergie,
                        null,
                        $zone
                    );
                    $nbu   = $travaux->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURFACE_ISOLANT_TOIT_TERRASSE
                    );
                    $nbKwh = $nbKwh * $nbu;
                    break;

                case TravauxConstante::TRAVAUX_T08_ISOL_PLANCHER_BAS:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        null,
                        null,
                        $zone
                    );
                    $nbu   = $travaux->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURF_ISOLANT_PLANCHER_BAS
                    );
                    $nbKwh = $nbKwh * $nbu;
                    break;

                case TravauxConstante::TRAVAUX_T09_FERMET_ISOLANTE:
                    $nbKwh        = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        $codeEnergie,
                        null,
                        $zone
                    );
                    $nbPersiennes = $travaux->getValeurParamTech(RegleVariableConstante::VAR_PARAM_TECH_NB_PERSIENNES);
                    $nbVolets     = $travaux->getValeurParamTech(RegleVariableConstante::VAR_PARAM_TECH_NB_VOLETS);
                    if (!empty($nbPersiennes)) {
                        $nbu = $nbu + $nbPersiennes;
                    }
                    if (!empty($nbVolets)) {
                        $nbu = $nbu + $nbVolets;
                    }
                    $nbKwh = $nbKwh * $nbu;
                    break;

                case TravauxConstante::TRAVAUX_T11_CHAUDIERE_GAZ_COND:
                case TravauxConstante::TRAVAUX_T12_CHAUDIERE_BASSE_TEMP:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        $codeTypeLogement,
                        null,
                        null,
                        $zone
                    );
                    if ($codeTypeLogement == ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON) {
                        /** @var CoeffSurfaceService $coeffSurfaceService */
                        $coeffSurfaceService = $this->serviceLocator->get(CoeffSurfaceService::SERVICE_NAME);
                        $coeffSurface        = $coeffSurfaceService->getCoefficientSurfacePourTypeLogement(
                            $codeTypeLogement,
                            $codeSurface,
                            $codeTravaux
                        );

                        $nbKwh = $nbKwh * $coeffSurface;
                    }
                    break;

                case TravauxConstante::TRAVAUX_T13_EQUIP_REGULATION:
                    $nbKwhSonde         = 0;
                    $nbKwhProgrammateur = 0;
                    /** @var CoeffSurfaceService $coeffSurfaceService */
                    $coeffSurfaceService = $this->serviceLocator->get(CoeffSurfaceService::SERVICE_NAME);

                    if ($travaux->getValeurParamTech(RegleVariableConstante::VAR_PARAM_TECH_SONDE) == true) {
                        $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                            $codeTravaux,
                            SimulationConstante::CEE_CARAC_SONDE,
                            $codeTypeLogement,
                            $codeEnergie,
                            null,
                            $zone
                        );

                        $coeffSurface = $coeffSurfaceService->getCoefficientSurfacePourTypeLogement(
                            $codeTypeLogement,
                            $codeSurface,
                            $codeTravaux
                        );
                        $nbKwhSonde   = $nbKwh * $coeffSurface;
                    }
                    if ($travaux->getValeurParamTech(RegleVariableConstante::VAR_PARAM_TECH_PROGRAMMATEUR) == true) {

                        if ($codeTypeLogement == ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON) {
                            $nbKwh              = $kwhCumacService->getKwhCumacpourCodeTravaux(
                                $codeTravaux,
                                SimulationConstante::CEE_CARAC_PROGRAMMATEUR,
                                $codeTypeLogement,
                                $codeEnergie,
                                null,
                                $zone
                            );
                            $coeffSurface       = $coeffSurfaceService->getCoefficientSurfacePourTypeLogement(
                                $codeTypeLogement,
                                $codeSurface,
                                $codeTravaux
                            );
                            $nbKwhProgrammateur = $nbKwh * $coeffSurface;
                        } elseif ($codeTypeLogement == ValeurListeConstante::TYPE_LOGEMENT_CODE_APPART) {
                            $modeChauffage      = $travaux->getValeurParamTech(
                                RegleVariableConstante::VAR_PARAM_TECH_MODE_CHAUFFAGE_PROG
                            );
                            $nbKwhProgrammateur = $kwhCumacService->getKwhCumacpourCodeTravaux(
                                $codeTravaux,
                                SimulationConstante::CEE_CARAC_PROGRAMMATEUR,
                                $codeTypeLogement,
                                $codeEnergie,
                                $modeChauffage,
                                $zone
                            );
                        }
                    }
                    $nbKwh = $nbKwhSonde + $nbKwhProgrammateur;
                    break;

                case TravauxConstante::TRAVAUX_T14_VMC_SIMPLE:
                case TravauxConstante::TRAVAUX_T15_VMC_DOUBLE:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        null,
                        null,
                        $zone
                    );
                    /** @var CoeffSurfaceService $coeffSurfaceService */
                    $coeffSurfaceService = $this->serviceLocator->get(CoeffSurfaceService::SERVICE_NAME);
                    $coeffSurface        = $coeffSurfaceService->getCoefficientSurfacePourTypeLogement(
                        $codeTypeLogement,
                        $codeSurface,
                        $codeTravaux
                    );
                    $nbKwh               = $nbKwh * $coeffSurface;
                    // facteur_correctif_installation
                    if($codeTravaux == TravauxConstante::TRAVAUX_T14_VMC_SIMPLE){
                        $nbKwh = $nbKwh * 0.9 ;
                    }
                    break;

                case TravauxConstante::TRAVAUX_T18_RAD_DOUCE:
                    if ($codeTypeLogement == ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON) {
                        $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                            $codeTravaux,
                            null,
                            $codeTypeLogement,
                            null,
                            null,
                            $zone
                        );
                    }
                    if ($codeTypeLogement == ValeurListeConstante::TYPE_LOGEMENT_CODE_APPART) {
                        $modeChauffage = $travaux->getValeurParamTech(
                            RegleVariableConstante::VAR_PARAM_TECH_MODE_CHAUFFAGE_RAD
                        );
                        $nbKwh         = $kwhCumacService->getKwhCumacpourCodeTravaux(
                            $codeTravaux,
                            null,
                            $codeTypeLogement,
                            null,
                            $modeChauffage,
                            $zone
                        );
                    }
                    $nbRadiateurs = $travaux->getValeurParamTech(RegleVariableConstante::VAR_PARAM_TECH_NB_RADIATEURS);
                    $nbKwh        = $nbKwh * $nbRadiateurs;
                    break;

                case TravauxConstante::TRAVAUX_T19_PLANCHER_CHAUFF:
                    if ($codeTypeLogement == ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON) {
                        $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                            $codeTravaux,
                            null,
                            $codeTypeLogement,
                            null,
                            null,
                            $zone
                        );
                    }
                    if ($codeTypeLogement == ValeurListeConstante::TYPE_LOGEMENT_CODE_APPART) {
                        $modeChauffage = $travaux->getValeurParamTech(
                            RegleVariableConstante::VAR_PARAM_TECH_MODE_CHAUFFAGE_PLANCHER
                        );
                        $nbKwh         = $kwhCumacService->getKwhCumacpourCodeTravaux(
                            $codeTravaux,
                            null,
                            $codeTypeLogement,
                            null,
                            $modeChauffage,
                            $zone
                        );
                    }
                    $surfacePlancher = $travaux->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURF_PLANCHER
                    );
                    $nbKwh           = $nbKwh * $surfacePlancher;
                    break;

                case TravauxConstante::TRAVAUX_T20_POELE:
                    $nbKwh      = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        null,
                        null,
                        $zone
                    );
                    /*$paramPoele = $travaux->getValeurParamTech(RegleVariableConstante::VAR_PARAM_TECH_POELE);
                    if ($paramPoele !== null && $paramPoele === true) {
                        ++$nbu;
                    }
                    $nbKwh *= $nbu;*/
                    break;

                case TravauxConstante::TRAVAUX_T21_CHAUDIERE_BIOMASSE:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        $codeTypeLogement,
                        null,
                        null,
                        $zone
                    );
                    break;

                case TravauxConstante::TRAVAUX_T22_PAC_AIR_AIR:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        $codeTypeLogement,
                        null,
                        null,
                        $zone
                    );
                    /** @var CoeffSurfaceService $coeffSurfaceService */
                    $coeffSurfaceService = $this->serviceLocator->get(CoeffSurfaceService::SERVICE_NAME);
                    $coeffSurface        = $coeffSurfaceService->getCoefficientSurfacePourTypeLogement(
                        $codeTypeLogement,
                        $codeSurface,
                        $codeTravaux
                    );
                    $nbKwh               = $nbKwh * $coeffSurface;
                    break;

                case TravauxConstante::TRAVAUX_T23_PAC_AIR_EAU:
                case TravauxConstante::TRAVAUX_T24_PAC_EAU_EAU:
                case TravauxConstante::TRAVAUX_T43_RAC_RES_CHA:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        $codeTypeLogement,
                        null,
                        null,
                        $zone
                    );
                    /** @var CoeffSurfaceService $coeffSurfaceService */
                    $coeffSurfaceService = $this->serviceLocator->get(CoeffSurfaceService::SERVICE_NAME);
                    $coeffSurface        = $coeffSurfaceService->getCoefficientSurfacePourTypeLogement(
                        $codeTypeLogement,
                        $codeSurface,
                        $codeTravaux
                    );
                    $nbKwh               = $nbKwh * $coeffSurface;
                    break;

                case TravauxConstante::TRAVAUX_T25_SYST_SOLAIRE:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        null,
                        null,
                        null,
                        $zone
                    );
                    break;

                case TravauxConstante::TRAVAUX_T26_CE_THERMODYN:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux(
                        $codeTravaux,
                        null,
                        $codeTypeLogement,
                        null,
                        null,
                        null
                    );
                    break;

                case TravauxConstante::TRAVAUX_T27_CE_SOLAIRE:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux($codeTravaux, null, null, null, null, $zone);
                    break;
                case TravauxConstante::TRAVAUX_T45_INSERT:
                    $nbKwh       = $kwhCumacService->getKwhCumacpourCodeTravaux($codeTravaux, null, null, null, null, $zone);
                    $paramInsert = $travaux->getValeurParamTech(RegleVariableConstante::VAR_PARAM_TECH_INSERT);
                    if ($paramInsert !== null && $paramInsert === true) {
                        ++$nbu;
                    }
                    $nbKwh *= $nbu;
                    break;
                case TravauxConstante::TRAVAUX_T46_CHEMINEE:
                    $nbKwh = $kwhCumacService->getKwhCumacpourCodeTravaux($codeTravaux, null, null, null, null, $zone);
                    $param = $travaux->getValeurParamTech(RegleVariableConstante::VAR_PARAM_TECH_FOYER_FERME);
                    if ($param !== null && $param === true) {
                        ++$nbu;
                    }
                    $nbKwh *= $nbu;
                    break;

            }
            $montantTo = $nbKwh * $coeffKwhEuros;
            $CoutTo    = $travaux->getCoutHtTo();
            $CoutMo    = $travaux->getCoutHtMo();
            //le montant de l'aide ne doit dépassé le monant du coût des travaux
            if($montantTo > $travaux->getCoutTtcTo()){
                $montantTo = $travaux->getCoutTtcTo();
            }
            //@todo a voir comment corriger ceci.
            if($CoutTo == 0){
                $CoutTo = 1;
            }
            $montantMo = $montantTo * ($CoutMo / $CoutTo);
            $montantFo = $montantTo - $montantMo;

            $aideTravaux->setMontantAideTo($montantTo);
            $aideTravaux->setMontantAideMo($montantMo);
            $aideTravaux->setMontantAideFo($montantFo);

            if ($aideTravaux->getMontantAideTo() > 0) {
                LoggerSimulation::detail(
                    "\n\t\t ==>[Montant de l'aide CEE = " . $aideTravaux->getMontantAideTo() . "]"
                );
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner le service sur le kwh Cumac
     * @return array|object|KwhCumacService
     */
    private function getKwhCumacService()
    {
        if (empty($this->kwhCumacService)) {
            $this->kwhCumacService = $this->serviceLocator->get(KwhCumacService::SERVICE_NAME);
        }
        return $this->kwhCumacService;
    }

    /**
     * Retourne le service des valeurs liste
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
        }
        return $this->valeurListeService;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
