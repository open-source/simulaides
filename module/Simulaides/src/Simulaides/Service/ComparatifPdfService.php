<?php

namespace Simulaides\Service;

use Exception;
use Simulaides\Domain\Entity\Comparatif;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Tools\ResultatPDF;
use Simulaides\Tools\Utils;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\RendererInterface;

/**
 * Classe ComparatifPdfService
 *
 * Projet : ADEME Simul'Aides
 *
 * @author
 */
class ComparatifPdfService
{
    /** Nom du service */
    const SERVICE_NAME = 'ComparatifPdfService';

    /**
     * Nom du paramètre de vue de ligne simulation : montant des travaux
     */
    const MONTANT_TRAVAUX = 'montantTravaux';

    /**
     * Nom du paramètre de vue de ligne simulation : montant des aides
     */
    const MONTANT_AIDE = 'montantAide';

    /**
     * Nom du paramètre de vue de ligne simulation : montant reste à charge
     */
    const MONTANT_RESTE_A_CHARGE = 'montantResteACharge';

    /**
     * Nom du paramètre de vue de ligne simulation : taux des aides
     */
    const TAUX_AIDE = 'tauxAide';

    /**
     * @var ProjetService
     */
    protected $projetService;

    /**
     * @var GenerePdfService
     */
    protected $pdfService;

    /**
     * @var RendererInterface
     */
    protected $viewRenderer;

    /**
     * @param ProjetService     $projetService
     * @param GenerePdfService  $pdfService
     * @param RendererInterface $viewRender
     */
    public function __construct(ProjetService $projetService, GenerePdfService $pdfService, RendererInterface $viewRender)
    {
        $this->projetService = $projetService;
        $this->pdfService    = $pdfService;
        $this->viewRenderer  = $viewRender;
    }

    /**
     * Génère le fichier PDF du comparatif
     *
     * @param Comparatif $comparatif
     *
     * @return string
     * @throws Exception
     */
    public function generePdf(Comparatif $comparatif)
    {
        $fileName = DIR_APPLI . '/data/tmp/' . $this->getNomFichierPDF($comparatif->getId());
        $pdf      = $this->pdfService->initPdf();

        $this->addComparatifPart($pdf, $comparatif);

        $this->addSimulationPart($pdf, $comparatif->getSimulation1(), '1');
        if ($comparatif->getSimulation2() !== null) {
            $pdf->AddPage();
            $this->addSimulationPart($pdf, $comparatif->getSimulation2(), '2');
            if ($comparatif->getSimulation3() !== null) {
                $pdf->AddPage();
                $this->addSimulationPart($pdf, $comparatif->getSimulation3(), '3');
            }
        }

        $pdf->Output($fileName, 'F');

        return $fileName;
    }

    /**
     * Retourne le nom du fichier PDF
     *
     * @param int $idComparatif Id du comparatif
     *
     * @return string
     */
    private function getNomFichierPDF($idComparatif)
    {
        $date = new \DateTime();

        return 'Comparatif_' . $idComparatif . '_' . Utils::formatDateToString($date, 'dmy_hms') . '.pdf';
    }

    /**
     * Ajoute au PDF la partie simulation correspondant au projet passé en paramètre
     *
     * @param ResultatPDF $pdf
     * @param Comparatif  $comparatif
     *
     * @return ResultatPDF
     */
    private function addComparatifPart(ResultatPDF $pdf, Comparatif $comparatif)
    {
        $pdf->AddPage();

        $lignesSimulation = [$this->getLigneSimulation($comparatif->getSimulation1())];
        if ($comparatif->getSimulation2() !== null) {
            $lignesSimulation[] = $this->getLigneSimulation($comparatif->getSimulation2());
            if ($comparatif->getSimulation3() !== null) {
                $lignesSimulation[] = $this->getLigneSimulation($comparatif->getSimulation3());
            }
        }

        $model = new ViewModel(
            [
                'idComparatif'       => $comparatif->getId(),
                'motPasseComparatif' => $comparatif->getMotPasse(),
                'simulations'        => $lignesSimulation
            ]
        );
        $model->setTemplate('simulaides/comparatif/recapitulatif-pdf.phtml');

        $pdf->SetXY(13, 20);
        $pdf->writeHTML($this->viewRenderer->render($model));

        return $pdf;
    }

    /**
     * @param Projet $simulation
     *
     * @return array
     */
    private function getLigneSimulation(Projet $simulation)
    {
        $simulationHistorique = $this->projetService->getSimulationHistorique($simulation);
        $montantAide          = $simulationHistorique->getMontant();
        $tauxAide             = $simulationHistorique->getTaux();
        $coutsTravaux         = $this->projetService->getCoutsTravaux($montantAide, $tauxAide);
        $montantResteACharge  = $this->projetService->getMontantResteACharge($tauxAide, $montantAide);

        return [
            self::MONTANT_TRAVAUX        => Utils::formatNumberToString($coutsTravaux, 2),
            self::MONTANT_AIDE           => Utils::formatNumberToString($montantAide, 2),
            self::MONTANT_RESTE_A_CHARGE => Utils::formatNumberToString($montantResteACharge, 2),
            self::TAUX_AIDE              => $tauxAide
        ];
    }

    /**
     * Ajoute au PDF la partie simulation correspondant au projet passé en paramètre
     *
     * @param ResultatPDF $pdf
     * @param Projet      $projet
     * @param string      $simulationNumber
     *
     * @throws Exception
     */
    private function addSimulationPart(ResultatPDF $pdf, Projet $projet, $simulationNumber)
    {

        $this->projetService->chargeProjetDansSession($projet);
        $this->projetService->lanceSimulation($projet->getId());
        $this->pdfService->setProjet($projet);
        $this->pdfService->genereCorpsResultatPDF($pdf, $simulationNumber);
    }
}
