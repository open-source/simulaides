<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Entity\ParametreTech;
use Simulaides\Domain\Repository\ParametreTechRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Form\FormElementManager;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ParametreTechService
 * service de gestion des paramètres technique
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service\Factory
 */
class ParametreTechService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'ParametreTechService';

    /**
     * Nom de la classe
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\ParametreTech';

    /** @var  ParametreTechRepository */
    private $parametreTechRepository;

    /**
     * @var FormElementManager
     */
    protected $formElementManager;

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * @return \Simulaides\Domain\Repository\ParametreTechRepository
     */
    public function getParametreTechRepository()
    {
        return $this->parametreTechRepository;
    }

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param FormElementManager      $formElementManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormElementManager $formElementManager,
        ServiceLocatorInterface $serviceLocator
    ) {
        $this->entityManager           = $entityManager;
        $this->parametreTechRepository = $entityManager->getRepository($this->className);
        $this->formElementManager      = $formElementManager;
        $this->serviceLocator          = $serviceLocator;
    }

    /**
     * Permet de retourne le paramètre technique selon le code
     *
     * @param $codeParamTech
     *
     * @return null|ParametreTech
     * @throws \Exception
     */
    public function getParametreTechParCode($codeParamTech)
    {
        try {
            return $this->parametreTechRepository->getParametreTechParCode($codeParamTech);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de récupérer les param tech utilisable dans les règles
     * et qui sont liés aux produits du travaux
     *
     * @param string $codeTravaux Code du travaux
     *
     * @return array
     * @throws \Exception
     */
    public function getListParamTechUtilisableReglePourCodeTravaux($codeTravaux)
    {
        try {
            return $this->parametreTechRepository->getListParamTechUtilisableReglePourCodeTravaux($codeTravaux);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }
}
