<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 14:39
 */

namespace Simulaides\Service\Factory;

use Doctrine\ORM\EntityManager;
use Simulaides\Service\LocalisationSessionContainer;
use Simulaides\Service\ProjetService;
use Simulaides\Service\ProjetSessionContainer;
use Zend\Form\FormElementManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ProjetServiceFactory
 * @package Simulaides\Service\Factory
 */
class ProjetServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var FormElementManager */
        $formElementManager = $serviceLocator->get('FormElementManager');
        /** @var ProjetSessionContainer */
        $projetSession = $serviceLocator->get(ProjetSessionContainer::SERVICE_NAME);

        /** @var LocalisationSessionContainer */
        $localisationSession = $serviceLocator->get(LocalisationSessionContainer::SERVICE_NAME);

        $projetService = new ProjetService(
            $entityManager,
            $formElementManager,
            $projetSession,
            $localisationSession,
            $serviceLocator
        );

        return $projetService;
    }
}
