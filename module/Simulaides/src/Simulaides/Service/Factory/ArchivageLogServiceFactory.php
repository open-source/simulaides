<?php
namespace Simulaides\Service\Factory;

use Simulaides\Service\ArchivageLogService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ArchivageLogServiceFactory
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service\Factory
 */
class ArchivageLogServiceFactory implements FactoryInterface
{
    /**
     * Création du service d'authentification
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed|ArchivageLogService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new ArchivageLogService($serviceLocator);
    }
}
