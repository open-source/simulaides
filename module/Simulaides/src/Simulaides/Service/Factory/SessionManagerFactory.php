<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 20/02/15
 * Time: 14:52
 */

namespace Simulaides\Service\Factory;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;

/**
 * Class SessionManagerFactory
 * @package Simulaides\Service\Factory
 */
class SessionManagerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');

        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config['session']);

        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        return $sessionManager;
    }
}
