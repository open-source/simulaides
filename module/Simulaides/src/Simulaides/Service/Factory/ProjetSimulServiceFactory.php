<?php
namespace Simulaides\Service\Factory;

use Doctrine\ORM\EntityManager;
use Simulaides\Domain\Repository\PartFiscaleRepository;
use Simulaides\Service\ProjetSimulService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ProjetSimulServiceFactory
 * Factory de création du service de simulation sur le projet
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service\Factory
 */
class ProjetSimulServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        return new ProjetSimulService($entityManager, $serviceLocator);
    }
}
