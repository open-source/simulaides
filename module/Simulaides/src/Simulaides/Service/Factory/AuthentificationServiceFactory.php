<?php
namespace Simulaides\Service\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Repository\ConseillerRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Simulaides\Service\AuthentificationService;
use Simulaides\Service\PrisService;
use Simulaides\Service\SessionContainerService;
use Zend\Form\FormElementManager;
use Zend\Log\Logger;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe AuthentificationServiceFactory
 * Factory permettant de créer le service sur l'authentification
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service\Factory
 */
class AuthentificationServiceFactory implements FactoryInterface
{

    /**
     * Création du service d'authentification
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed|AuthentificationService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var FormElementManager $formElementManager */
        $formElementManager = $serviceLocator->get('FormElementManager');
        /** @var PrisService $prisService */
        $prisService = $serviceLocator->get(PrisService::SERVICE_NAME);
        /**@var SessionContainerService $session */
        $session = $serviceLocator->get(SessionContainerService::SERVICE_NAME);
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var ConseillerRepository $conseillerRepository */
        $conseillerRepository = $entityManager->getRepository(MainConstante::ENTITY_PATH . 'Conseiller');
        /**@var Logger $loggerService */
        $loggerService = $serviceLocator->get(LoggerFactory::SERVICE_NAME);

        $authService = new AuthentificationService(
            $formElementManager,
            $prisService,
            $session,
            $conseillerRepository,
            $loggerService
        );

        return $authService;
    }
}
