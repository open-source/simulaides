<?php
namespace Simulaides\Service\Factory;

use Doctrine\ORM\EntityManager;
use Simulaides\Service\AccessSimulationFormService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe AccessSimulationFormServiceFactory
 * Factory du service pour la gestion du formulaire d'accès aux simulations
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service\Factory
 */
class AccessSimulationFormServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        return new AccessSimulationFormService($entityManager, $serviceLocator);
    }
}
