<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 15:18
 */

namespace Simulaides\Service\Factory;


use Simulaides\Service\ProjetSessionContainer;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\SessionManager;

/**
 * Class ProjetSessionContainerFactory
 * @package Simulaides\Service\Factory
 */
class ProjetSessionContainerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var SessionManager $sessionManager */
        $sessionManager = $serviceLocator->get('SessionManager');

        return new ProjetSessionContainer($sessionManager);
    }
}
