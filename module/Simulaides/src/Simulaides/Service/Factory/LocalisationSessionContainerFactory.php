<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 17/02/15
 * Time: 16:35
 */

namespace Simulaides\Service\Factory;


use Simulaides\Service\LocalisationSessionContainer;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\SessionManager;

/**
 * Class LocalisationSessionContainerFactory
 * @package Simulaides\Service\Factory
 */
class LocalisationSessionContainerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var SessionManager $sessionManager */
        $sessionManager = $serviceLocator->get('SessionManager');

        return new LocalisationSessionContainer($sessionManager);
    }
}
