<?php
namespace Simulaides\Service\Factory;

use Simulaides\Service\GenerePdfService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe GenerePdfServiceFactory
 * Factory pour le service de génération des PDF
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service\Factory
 */
class GenerePdfServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new GenerePdfService($serviceLocator);
    }
}
