<?php
namespace Simulaides\Service\Factory;
use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Repository\RegionRepository;
use Simulaides\Service\SimulationRegionApartenanceVerificationService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe SimulationRegionApartenanceVerificationServiceFactory
 *
 * Projet : Alyoskills
 
 *
 * @author
 * @package   Simulaides\Service\Factory
 */
class SimulationRegionApartenanceVerificationServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        /** @var RegionRepository $regionRepository */
        $regionRepository = $entityManager->getRepository(MainConstante::ENTITY_PATH.'Region');

        return new SimulationRegionApartenanceVerificationService($regionRepository);
    }
}