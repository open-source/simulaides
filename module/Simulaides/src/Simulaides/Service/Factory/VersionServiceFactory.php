<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 20/02/15
 * Time: 09:42
 */

namespace Simulaides\Service\Factory;


use Doctrine\ORM\EntityManager;
use Simulaides\Service\VersionService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class VersionServiceFactory
 * @package Simulaides\Service\Factory
 */
class VersionServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return VersionService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        return new VersionService($entityManager, $serviceLocator);
    }
}
