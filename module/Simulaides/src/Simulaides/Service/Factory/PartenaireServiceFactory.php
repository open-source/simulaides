<?php
namespace Simulaides\Service\Factory;

use Doctrine\ORM\EntityManager;
use Simulaides\Service\ListeService;
use Simulaides\Service\PartenaireService;
use Zend\Form\FormElementManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe PartenaireServiceFactory
 *
 * @package   Simulaides\Service\Factory
 */
class PartenaireServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var FormElementManager $formElementManager */
        $formElementManager = $serviceLocator->get('FormElementManager');

        return new PartenaireService($entityManager, $serviceLocator, $formElementManager);
    }
}
