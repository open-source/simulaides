<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 10:26
 */

namespace Simulaides\Service\Factory;

use Doctrine\ORM\EntityManager;
use Simulaides\Service\ProjetSessionContainer;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\TravauxService;
use Zend\Form\FormElementManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class TravauxServiceFactory
 * @package Simulaides\Service\Factory
 */
class TravauxServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var FormElementManager $formElementManager */
        $formElementManager = $serviceLocator->get('FormElementManager');
        /** @var ProjetSessionContainer $projetSession */
        $projetSession = $serviceLocator->get('ProjetSessionContainer');
        /** @var SessionContainerService $sessionContainer */
        $sessionContainer = $serviceLocator->get('SessionContainer');

        return new TravauxService(
            $entityManager,
            $formElementManager,
            $projetSession,
            $sessionContainer,
            $serviceLocator
        );
    }
}
