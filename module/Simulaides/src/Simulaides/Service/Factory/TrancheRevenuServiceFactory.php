<?php
namespace Simulaides\Service\Factory;

use Doctrine\ORM\EntityManager;
use Simulaides\Service\TrancheRevenuService;
use Zend\Form\FormElementManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe TrancheRevenuServiceFactory
 * Factory pemettant de créer le service sur les tranche de revenu
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service\Factory
 */
class TrancheRevenuServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var FormElementManager */
        $formElementManager = $serviceLocator->get('FormElementManager');

        return new TrancheRevenuService($entityManager, $formElementManager, $serviceLocator);
    }
}
