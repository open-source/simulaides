<?php
namespace Simulaides\Service\Factory;

use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe SmtpTransportFactory
 * Factory pour l'envoi de mail smtp
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service\Factory
 */
class SmtpTransportFactory implements FactoryInterface
{
    /**
     * @param  ServiceLocatorInterface $serviceLocator
     *
     * @return Smtp
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config        = $serviceLocator->get('Configuration');
        $options       = new SmtpOptions($config['mailer']['smtp_options']);
        $smtpTransport = new Smtp($options);

        return $smtpTransport;
    }
}
