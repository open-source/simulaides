<?php

namespace Simulaides\Service\Factory;

use Simulaides\Service\ComparatifPdfService;
use Simulaides\Service\GenerePdfService;
use Simulaides\Service\ProjetService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Renderer\RendererInterface;

/**
 * Classe ComparatifPdfServiceFactory
 *
 * Projet : ADEME Simul'Aides
 *
 * @author
 */
class ComparatifPdfServiceFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var ProjetService $projetService */
        $projetService = $serviceLocator->get(ProjetService::SERVICE_NAME);
        /** @var GenerePdfService $pdfService */
        $pdfService = $serviceLocator->get(GenerePdfService::SERVICE_NAME);
        /** @var RendererInterface $viewRender */
        $viewRender = $serviceLocator->get('ViewRenderer');

        return new ComparatifPdfService($projetService, $pdfService, $viewRender);
    }
}
