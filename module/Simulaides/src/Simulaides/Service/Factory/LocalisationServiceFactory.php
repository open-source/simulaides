<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 19/02/15
 * Time: 15:50
 */

namespace Simulaides\Service\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Service\LocalisationService;
use Simulaides\Service\LocalisationSessionContainer;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class LocalisationServiceFactory
 * @package Simulaides\Service\Factory
 */
class LocalisationServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var LocalisationSessionContainer $localisationSession */
        $localisationSession = $serviceLocator->get(LocalisationSessionContainer::SERVICE_NAME);

        return new LocalisationService($entityManager, $localisationSession, $serviceLocator);
    }
}
