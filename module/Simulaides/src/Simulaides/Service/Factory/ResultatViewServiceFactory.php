<?php

namespace Simulaides\Service\Factory;

use Simulaides\Service\LocalisationService;
use Simulaides\Service\ProjetService;
use Simulaides\Service\ProjetSimulService;
use Simulaides\Service\ResultatViewService;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\ValeurListeService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\Url;
use Zend\View\Renderer\RendererInterface;

/**
 * Classe ResultatViewServiceFactory
 *
 * Projet : ADEME Simul'Aides
 *
 * @author
 */
class ResultatViewServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /* @var ProjetService $projetService */
        $projetService = $serviceLocator->get(ProjetService::SERVICE_NAME);
        /**@var SessionContainerService $sessionContainerService */
        $sessionContainerService = $serviceLocator->get(SessionContainerService::SERVICE_NAME);
        /* @var $viewRenderer RendererInterface */
        $viewRenderer = $serviceLocator->get('ViewRenderer');
        /* @var LocalisationService $localisationService */
        $localisationService = $serviceLocator->get(LocalisationService::SERVICE_NAME);
        /* @var ValeurListeService $valeurListeService */
        $valeurListeService = $serviceLocator->get(ValeurListeService::SERVICE_NAME);
        /* @var ProjetSimulService $projetSimulService */
        $projetSimulService = $serviceLocator->get(ProjetSimulService::SERVICE_NAME);
        /* @var Url $urlHelper */
        $urlHelper = $serviceLocator->get('viewhelpermanager')->get('url');

        return new ResultatViewService(
            $projetService,
            $sessionContainerService,
            $viewRenderer,
            $localisationService,
            $valeurListeService,
            $projetSimulService,
            $urlHelper
        );
    }

}
