<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 20/02/15
 * Time: 09:42
 */

namespace Simulaides\Service\Factory;


use Doctrine\ORM\EntityManager;
use Simulaides\Service\ValeurListeService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ValeurListeServiceFactory
 * @package Simulaides\Service\Factory
 */
class ValeurListeServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        return new ValeurListeService($entityManager, $serviceLocator);
    }
}
