<?php

namespace Simulaides\Service\Factory;

use Doctrine\ORM\EntityManager;
use Simulaides\Form\Comparatif\AccesComparatifForm;
use Simulaides\Logs\Factory\LoggerFactory;
use Simulaides\Service\ComparatifService;
use Simulaides\Service\ProjetService;
use Zend\Form\FormElementManager;
use Zend\Log\Logger;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Renderer\RendererInterface;

/**
 * Class ComparatifServiceFactory
 * @package Simulaides\Service\Factory
 */
class ComparatifServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return ComparatifService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var Logger $loggerService */
        $loggerService = $serviceLocator->get(LoggerFactory::SERVICE_NAME);
        /** @var ProjetService $projetService */
        $projetService = $serviceLocator->get(ProjetService::SERVICE_NAME);
        /* @var $viewRenderer RendererInterface */
        $viewRenderer = $serviceLocator->get('ViewRenderer');
        /** @var FormElementManager */
        $formElementManager = $serviceLocator->get('FormElementManager');
        /** @var AccesComparatifForm $accesComparatifForm */
        $accesComparatifForm = $formElementManager->get('AccesComparatifForm');

        return new ComparatifService($entityManager, $loggerService, $projetService, $viewRenderer, $accesComparatifForm);
    }
}
