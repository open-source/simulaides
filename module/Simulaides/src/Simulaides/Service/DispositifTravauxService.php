<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\DispositifTravaux;
use Simulaides\Domain\Repository\DispositifTravauxRepository;
use Simulaides\Domain\Repository\DispTravauxRegleRepository;
use Simulaides\Domain\Repository\TravauxRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe DispositifTravauxService
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class DispositifTravauxService
{
    /** Nom du service */
    const SERVICE_NAME = 'DispositifTravauxService';

    /**
     * Nom de la classe des travaux des dispositifs
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\DispositifTravaux';

    /**
     * Nom de la classe des travaux des dispositifs
     * @var string
     */
    private $classNameRegle = 'Simulaides\Domain\Entity\DispTravauxRegle';

    /** @var  DispositifTravauxRepository */
    private $dispositifTravauxRepository;

    /** @var  travauxRepository */
    private $travauxRepository;

    /** @var  DispTravauxRegleRepository */
    private $dispTravauxRegleRepository;

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->entityManager               = $entityManager;
        $this->dispositifTravauxRepository = $entityManager->getRepository($this->className);
        $this->dispTravauxRegleRepository  = $entityManager->getRepository($this->classNameRegle);
        $this->serviceLocator              = $serviceLocator;
    }

    /**
     * Supprime la liste des travaux dispositif pour un id dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return array
     * @throws \Exception
     */
    public function deleteDispositifTravauxParIdDispositif($idDispositif)
    {
        try {
            //Sélection des id des dispositif_travaux à supprimer
            $tabIdDispTravaux = $this->dispositifTravauxRepository
                ->getIdDispositifTravauxParIdDispositif($idDispositif);
            //Suppression de toutes les règles des travaux
            $this->dispTravauxRegleRepository->deleteDispTravauxRegleParDispositifTravaux($tabIdDispTravaux);
            //Suppression de toutes les associations des travaux aux dispositifs
            $this->dispositifTravauxRepository->deleteDispositifTravauxParIdDispositif($idDispositif);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourne la liste des travaux prix en charge par un dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return array
     * @throws \Exception
     */
    public function getDispositifTravauxParIdDispositif($idDispositif)
    {
        try {
            return $this->dispositifTravauxRepository->getDispositifTravauxParIdDispositif($idDispositif);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de sauvegarder un travaux d'un dispositif
     *
     * @param DispositifTravaux $dispositifTravaux
     *
     * @throws \Exception
     */
    public function saveDispositifTravaux($dispositifTravaux)
    {
        try {
            $this->dispositifTravauxRepository->saveDispositifTravaux($dispositifTravaux);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des codes des travaux pris en charge par le dispositif
     *
     * @param int $idDispositif Idenifiant du dispositif
     *
     * @return array
     * @throws \Exception
     */
    public function getCodeTravauxPourDispositif($idDispositif)
    {
        $tabCode = [];
        try {
            $tabCodeTravaux = $this->dispositifTravauxRepository->getCodeTravauxPourDispositif($idDispositif);
            foreach ($tabCodeTravaux as $travaux) {
                $tabCode[] = $travaux['codeTravaux'];
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

        return $tabCode;
    }

    /**
     * Retourne le dispositif travaux pour le code travaux et le dispositif
     *
     * @param $idDispositif
     * @param $codeTravaux
     *
     * @return null|DispositifTravaux
     * @throws \Exception
     */
    public function getDispositifTravauxPourIdDispositifEtCodeTravaux($idDispositif, $codeTravaux)
    {
        try {
            $dispositifTravaux = $this->dispositifTravauxRepository->getDispositifTravauxPourIdDispositifEtCodeTravaux(
                $idDispositif,
                $codeTravaux
            );
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

        return $dispositifTravaux;
    }

    /**
     * Supprime le travaux dans le dispositif et ses règles
     *
     * @param DispositifTravaux $dispositifTravaux
     *
     * @throws \Exception
     */
    public function deleteDispositifTravaux($dispositifTravaux)
    {
        try {
            $this->dispositifTravauxRepository->deleteDispositifTravaux($dispositifTravaux);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le libellé du type de la règle du travaux
     *
     * @param string $codeType Code du type de la règle
     *
     * @return string
     */
    public function getLibelleTypeRegleTravaux($codeType)
    {
        $libelle = '';

        switch ($codeType) {
            case ValeurListeConstante::TYPE_REGLE_TRAVAUX_PLAFOND:
                $libelle = "Plafond de l'aide";
                break;
            case ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_TOTAL:
                $libelle = 'Montant aide total';
                break;
            case ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_FOURNITURE:
                $libelle = 'Montant aide fourniture';
                break;
            case ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_MO:
                $libelle = "Montant aide main d'oeuvre";
                break;
        }

        return $libelle;
    }

    /**
     * Permet de retourner le travaux dans le dispositif par son Id
     *
     * @param int $idDispositifTravaux Identifiant du travaux dans le dispositif
     *
     *
     * @return null|DispositifTravaux
     * @throws \Exception
     */
    public function getDispositifTravauxParId($idDispositifTravaux)
    {
        try {
            return $this->dispositifTravauxRepository->getDispositifTravauxParId($idDispositifTravaux);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la règle du travaux dans le dispositif par sont Id
     *
     * @param $idDispTravauxRegle
     *
     * @return null|\Simulaides\Domain\Entity\DispTravauxRegle
     * @throws \Exception
     */
    public function getDispTravauxRegleParId($idDispTravauxRegle)
    {
        try {
            return $this->dispTravauxRegleRepository->getDispTravauxRegleParId($idDispTravauxRegle);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de récupérer les travaux du dispositif
     *
     * @param string $lstCodeTravaux Liste des codes travaux séparés par des virgules
     * @param int    $idDispositif   Identifiant du dispositif
     *
     * @return array
     * @throws \Exception
     */
    public function getRegleTravauxPourDispositif($lstCodeTravaux, $idDispositif)
    {
        try {
            $tabResult = $this->dispositifTravauxRepository->getRegleTravauxPourDispositif(
                $lstCodeTravaux,
                $idDispositif
            );
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
        return $tabResult;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }

    /**
     * @param $simulation
     * @return bool
     */
    public function hasEligibleCeeCdp($simulation)
    {
        return $simulation->getMontantTotal() > 0;
    }

    public function getOffresEligibles(array $travauxListe) {
        $offres = array();
        foreach ($travauxListe as $travaux) {
            foreach ($this->dispositifTravauxRepository->getDispositifTravauxParCodeTravaux($travaux) as $dispositifTravaux) {
                if (($dispositifTravaux->getDispositif()->getCodeTypeDispositif() == ValeurListeConstante::TYPE_DISPOSITIF_COUP_DE_POUCE
                        || $dispositifTravaux->getDispositif()->getCodeTypeDispositif() == ValeurListeConstante::TYPE_DISPOSITIF_CEE)
                    && $dispositifTravaux->getEligibiliteSpecifique() != null) {
                    $offres[] = $dispositifTravaux;
                }
            }
        }
        return $offres;
    }
}
