<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 19/02/15
 * Time: 15:17
 */

namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\SessionConstante;
use Simulaides\Domain\Entity\Commune;
use Simulaides\Domain\Entity\Departement;
use Simulaides\Domain\Entity\Epci;
use Simulaides\Domain\Repository\CommuneRepository;
use Simulaides\Domain\Repository\DepartementRepository;
use Simulaides\Domain\Repository\EpciRepository;
use Simulaides\Domain\Repository\RegionRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class LocalisationService
 * @package Simulaides\Service
 */
class LocalisationService
{

    /** Nom du service */
    const SERVICE_NAME = 'LocalisationService';

    /**
     * @var LocalisationSessionContainer
     */
    private $localisationSession;

    /** @var  EntityManagerInterface */
    private $entityManager;

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * @param EntityManagerInterface       $entityManager
     * @param LocalisationSessionContainer $localisationSessionContainer
     * @param ServiceLocatorInterface      $serviceLocator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LocalisationSessionContainer $localisationSessionContainer,
        ServiceLocatorInterface $serviceLocator
    ) {
        $this->entityManager       = $entityManager;
        $this->localisationSession = $localisationSessionContainer;
        $this->serviceLocator      = $serviceLocator;
    }


    /**
     * @return mixed|\Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @return CommuneRepository
     */
    public function getCommuneRepository()
    {
        return $this->entityManager->getRepository('Simulaides\Domain\Entity\Commune');
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return LocalisationSessionContainer
     */
    public function getLocalisationSession()
    {
        return $this->localisationSession;
    }

    /**
     * @return RegionRepository
     */
    public function getRegionRepository()
    {
        return $this->entityManager->getRepository('Simulaides\Domain\Entity\Region');
    }

    /**
     * @return EpciRepository
     */
    private function getEpciRepository()
    {
        return $this->entityManager->getRepository('Simulaides\Domain\Entity\Epci');
    }


    /**
     * Retourne la liste des values_options de l'objet select
     * En fonction du type et de la valeur de filtre des données
     *
     * @param string $type  Type de la valeur du filtre (code region, code département)
     * @param string $value Valeur du filtre
     *
     * @return array
     */
    public function getOptions($type, $value)
    {
        $result = ['status' => true];
        $session = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);

        try {
            switch ($type) {
                case 'codePostal':
                    if ($session->offsetExists(SessionConstante::REGION_PARTICULIER)) {
                        $codeRegion = $session->offsetGet(SessionConstante::REGION_PARTICULIER);
                    }else{
                        $codeRegion = null;
                    }

                    $codePostal = empty($value['codePostal'])? null:  $value['codePostal'];
                    /**
                     * @var Commune[] $communes
                     */
                    if(!empty($codePostal)) {
                        //Code postal envoyé : on recherche la liste des communes ayant ce code postal
                        $communes = $this->getCommunePourCodePostal($codePostal, $codeRegion);
                    }elseif(!empty($codeRegion)){
                        $communes = $this->getCommunePourListeRegion($codeRegion);
                    }else{
                        $communes = $this->getAllCommunes();
                    }
                    //Liste des valeurs qui vont remplir le select
                    $result['options'] = $this->getOptionsListCommune($communes);
                    $outre_mer = false;
                    if(count($communes)){
                        $outre_mer = $this->estOutreMer($communes[0]);
                    }
                    $result['outre_mer'] = $outre_mer;
                    //Si aucune commune correspondante au codepostal et à la région n'a été trouvé on regarde
                    //s'il s'agit d'un code postal valide pour une autre région
                    if(!empty($codeRegion) && (count($communes) === 0)){
                        $communesHorsRegion = $this->getCommuneRepository()->getCommunesPourCodePostal($value['codePostal']);
                        if(count($communesHorsRegion) !== 0){
                            $result['codePostalValide'] = true;
                            $result['currentRegion'] = $this->getRegionParCode($codeRegion)->getNom();
                        }else{
                            $result['codePostalValide'] = false;
                        }
                    }
                    //Nom de l'objet select cible : liste déroulante des communes
                    $result['target'] = 'commune';
                    break;
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            //Une erreur est survenue, mise à false du booléen de statut
            $result['status'] = false;
        }

        return $result;
    }

    /**
     * Retourne si oui ou non la commune désignée fait partie d'une région d'Outremer
     *
     * @param  integer $codeCommune Code INSEE de la commune
     *
     * @return boolean
     */
    public function estOutreMer($codeCommune)
    {
        $commune = $this->getCommuneParCode($codeCommune);
        if (!$commune) {
            return false;
        }

        return in_array($commune->getCodeDepartement(), MainConstante::$LISTE_DOM);
    }

    /**
     * Retourne si oui ou non la commune désignée fait partie de Mayotte
     *
     * @param integer $codeCommune Code INSEE de la commune
     *
     * @return boolean
     * @throws Exception
     */
    public function estMayotte($codeCommune)
    {
        $commune = $this->getCommuneParCode($codeCommune);

        return $commune !== null && $commune->getCodeDepartement() === MainConstante::DEPT_MAYOTTE;
    }

    /**
     * Retourne si oui ou non la commune désignée fait partie de La Réunion
     *
     * @param  integer $codeCommune Code INSEE de la commune
     *
     * @return boolean
     */
    public function estLaReunion($codeCommune)
    {
        $commune = $this->getCommuneParCode($codeCommune);
        if(!$commune){
            return false;
        }
        return $commune !== null && $commune->getCodeDepartement() === MainConstante::DEPT_REUNION;
    }

    /**
     * Retourne si oui ou non la commune désignée fait partie de La Martinique
     *
     * @param  integer $codeCommune Code INSEE de la commune
     *
     * @return boolean
     */
    public function estLaMartinique($codeCommune)
    {
        $commune = $this->getCommuneParCode($codeCommune);
        if(!$commune){
            return false;
        }
        return $commune !== null && $commune->getCodeDepartement() === MainConstante::DEPT_MARTINIQUE;
    }

    /**
     * Retourne si oui ou non la commune désignée fait partie de La Guadeloupe
     *
     * @param  integer $codeCommune Code INSEE de la commune
     *
     * @return boolean
     */
    public function estLaGuadeloupe($codeCommune)
    {
        $commune = $this->getCommuneParCode($codeCommune);
        if(!$commune){
            return false;
        }
        return $commune !== null && $commune->getCodeDepartement() === MainConstante::DEPT_GUADELOUPE;
    }


    /**
     * Retourne la liste des values_options de l'objet select
     * En fonction du type et des valeurs de filtre des données
     *
     * @param string $type   Type de la valeur du filtre (code region, code département)
     * @param array  $values Liste des valeurs du filtre
     * @param string $target Cible utilisé pour le cas du département
     *                       On recharge soit les intercommunalité, soit les communes
     *
     * @return array
     */
    public function getOptionsForMultiSelect($type, $values, $target)
    {
        $result = ['status' => true];
        try {
            switch ($type) {
                case 'region':
                    //Code région envoyé : on recherche la liste des départements de ces régions
                    $departements = $this->getDepartementRepository()->getDepartementsPourListeRegion($values);
                    //Liste des valeurs qui vont remplir le select
                    $result['options'] = $this->getOptionsListDepartement($departements);
                    //Nom de l'objet select cible : liste déroulante des départements
                    $result['target'] = 'departement';
                    break;
                case 'departement':
                    if ($target == 'commune') {
                        //Code département envoyé : on recherche la liste des communes de ce département
                        $result['options'] = [];
                        if (!empty($values)) {
                            $communes = $this->getCommuneRepository()->getCommunesPourListeDepartement($values);
                            //Liste des valeurs qui vont remplir le select
                            $result['options'] = $this->getOptionsListCommune($communes);
                        }
                    } elseif ($target == 'epci') {
                        //Code département envoyé : on recherche la liste des epci ayant au moins une commune
                        //de ces départements
                        $result['options'] = [];
                        if (!empty($values)) {
                            $epci = $this->getEpciPourListeDepartement($values);
                            //Liste des valeurs qui vont remplir le select
                            $result['options'] = $this->getOptionsListEpci($epci);
                        }
                    }
                    //Nom de l'objet select cible : liste déroulante des communes
                    $result['target'] = $target;
                    break;
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            //Une erreur est survenue, mise à false du booléen de statut
            $result['status'] = false;
        }

        return $result;
    }

    /**
     * Permet de retourner au format des objectSelect la liste des départements
     *
     * @param $tabDepartements
     *
     * @return array
     */
    private function getOptionsListDepartement($tabDepartements)
    {
        $options = [];
        try {
            $options = [['code' => '', 'nom' => 'Choisissez votre département']];
            /** @var Departement $departement */
            foreach ($tabDepartements as $departement) {
                $options[] = ['code' => $departement->getCode(), 'nom' => $departement->getNom()];
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
        }
        return $options;
    }

    /**
     * Permet de retourner au format des objectSelect la liste des communes
     *
     * @param $tabCommunes
     *
     * @return array
     */
    private function getOptionsListCommune($tabCommunes)
    {
        $options = [];
        try {
            $options = [['code' => '', 'nom' => 'Choisissez votre commune']];
            /** @var Commune $commune */
            foreach ($tabCommunes as $commune) {
                $options[] = ['code' => $commune->getCodeInsee(), 'nom' => $commune->getNom()];
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
        }
        return $options;
    }

    /**
     * Permet de retourner au format des objectSelect la liste des epci
     *
     * @param $tabEpci
     *
     * @return array
     */
    private function getOptionsListEpci($tabEpci)
    {
        $options = [];
        try {
            $options = [['code' => '', 'nom' => 'Choisissez votre intercommunalité']];
            /** @var Epci $epci */
            foreach ($tabEpci as $epci) {
                $options[] = ['code' => $epci->getId(), 'nom' => $epci->getNom()];
            }
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
        }
        return $options;
    }

    /**
     * @param $type
     * @param $value
     */
    public function setLocalisationSession($type, $value)
    {
        $localisation = $this->getLocalisationSession()->getLocalisation();
        switch ($type) {
            case SessionConstante::LOCALISATION_CODE_POSTAL:
                $localisation->setCodePostal($value['codePostal']);
                $localisation->setCodeInsee(null);
                break;
            case SessionConstante::LOCALISATION_REGION:
                $localisation->setCodeRegion($value);
                $localisation->setCodeDepartement(null);
                $localisation->setCodePostal(null);
                $localisation->setCodeInsee(null);
                break;
            case SessionConstante::LOCALISATION_DEPARTEMENT:
                $localisation->setCodeDepartement($value);
                $localisation->setCodePostal(null);
                $localisation->setCodeInsee(null);
                break;
            case SessionConstante::LOCALISATION_COMMUNE:
                $localisation->setCodeInsee($value);
                break;
        }
    }

    /**
     * @return DepartementRepository
     */
    public function getDepartementRepository()
    {
        return $this->entityManager->getRepository('Simulaides\Domain\Entity\Departement');
    }

    /**
     * Retourne la région selon le code indiqué
     *
     * @param string $codeRegion Code de la région
     *
     * @return null|\Simulaides\Domain\Entity\Region
     * @throws Exception
     */
    public function getRegionParCode($codeRegion)
    {
        try {
            return $this->getRegionRepository()->getRegionParCode($codeRegion);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner les départements des communes indiquées
     *
     * @param array|null $tabCommune Liste des codes INSEE des communes
     *
     * @return array
     * @throws Exception
     */
    public function getDepartementsPourListeCommune($tabCommune)
    {
        try {
            return $this->getDepartementRepository()->getDepartementsPourListeCommune($tabCommune);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner le départements de la commune indiquée
     *
     * @param array|null $codeCommune code INSEE de la commune
     *
     * @return
     * @throws Exception
     */
    public function getDepartementPourCommune($codeCommune)
    {
        $dpt = $this->getDepartementsPourListeCommune($codeCommune);
        return empty($dpt)? null:$dpt[0];
    }

    /**
     * Permet de retourner les départements des communes des Epci
     *
     * @param array $tabEpci Liste des codes EPCI
     *
     * @return array
     * @throws Exception
     */
    public function getDepartementsPourListeEpci($tabEpci)
    {
        try {
            return $this->getDepartementRepository()->getDepartementsPourListeEpci($tabEpci);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des EPCI ayant au moins une commune
     * qui sont dans les départements indiqués
     *
     * @param $tabDepartements
     *
     * @return array
     * @throws Exception
     */
    public function getEpciPourListeDepartement($tabDepartements)
    {
        try {
            return $this->getEpciRepository()->getEpciPourListeDepartement($tabDepartements);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des communes
     *
     * @return array
     * @throws Exception
     */
    public function getAllCommunes()
    {
        try {
            return $this->getCommuneRepository()->findBy([],['nom' => 'ASC']);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des communes qui sont dans les départements indiqués
     *
     * @param $tabDepartements
     *
     * @return array
     * @throws Exception
     */
    public function getCommunePourListeDepartement($tabDepartements)
    {
        try {
            return $this->getCommuneRepository()->getCommunesPourListeDepartement($tabDepartements);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des communes trié sur le nom pour les régions indiquées
     *
     * @param $tabCodeRegion
     * @return array
     * @throws Exception
     */
    public function getCommunePourListeRegion($tabRegions)
    {
        try {
            return $this->getCommuneRepository()->getCommunesPourListeRegion($tabRegions);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des communes qui ont le code postal indiqué dans le département indiqué
     *
     * @param $codePostal
     * @param $codeDepartement
     *
     * @return array
     * @throws Exception
     */
    public function getCommunePourCodePostal($codePostal, $codeDepartement = null)
    {
        try {
            return $this->getCommuneRepository()->getCommunesPourCodePostal($codePostal, $codeDepartement);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner la liste des optionsValue d'une liste d'EPCI
     *
     * @param array $tabEpci Liste des Epci
     *
     * @return array
     */
    public function getListOptionsValuePourEpci($tabEpci)
    {
        $options[''] = 'Choisissez votre intercommunalité';
        /** @var Epci $epci */
        foreach ($tabEpci as $epci) {
            $options[$epci->getId()] = $epci->getNom();
        }

        return $options;
    }

    /**
     * Permet de retourner la liste des optionsValue d'une liste de commune
     *
     * @param array $tabCommune Liste des Commune
     *
     * @return array
     */
    public function getListOptionsValuePourCommune($tabCommune)
    {
        $options[''] = 'Choisissez votre commune';
        /** @var Commune $commune */
        foreach ($tabCommune as $commune) {
            $options[$commune->getCodeInsee()] = $commune->getNom();
        }

        return $options;
    }

    /**
     * Permet de retourner la liste des optionsValue d'une liste de département
     *
     * @param array $tabDepartement Liste des Départements
     *
     * @return array
     */
    public function getListOptionsValuePourDepartement($tabDepartement)
    {
        $options[''] = 'Choisissez votre département';
        /** @var Departement $departement */
        foreach ($tabDepartement as $departement) {
            $options[$departement->getCode()] = $departement->getNom();
        }

        return $options;
    }

    /**
     * Retourne la liste complète des régions
     *
     * @return array
     * @throws Exception
     */
    public function getListeRegions()
    {
        try {
            return $this->getRegionRepository()->getListeRegions();
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Récupère la liste des régions pour les codes indiqués en entrée
     *
     * @param $tabCodeRegion
     *
     * @return array
     * @throws Exception
     */
    public function getRegionsParListeCodeRegion($tabCodeRegion)
    {
        try {
            return $this->getRegionRepository()->getRegionsParListeCodeRegion($tabCodeRegion);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des régions des départements indiqués
     *
     * @param array $tabCodeDept Liste des codes départements
     *
     * @return array
     * @throws Exception
     */
    public function getRegionsParListeCodeDepartement($tabCodeDept)
    {
        try {
            return $this->getRegionRepository()->getRegionsParListeCodeDepartement($tabCodeDept);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des régions contenant les communes des EPCI indiquées
     *
     * @param array $tabCodeEpci Liste des codes EPCI
     *
     * @return array
     * @throws Exception
     */
    public function getRegionsParListeCodeEpci($tabCodeEpci)
    {
        try {
            return $this->getRegionRepository()->getRegionsParListeCodeEpci($tabCodeEpci);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des régions contenant les communes indiquées
     *
     * @param array $tabCodeComune Liste des codes Communes
     *
     * @return array
     * @throws Exception
     */
    public function getRegionsParListeCodeCommune($tabCodeComune)
    {
        try {
            return $this->getRegionRepository()->getRegionsParListeCodeCommune($tabCodeComune);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le département à partir du code
     *
     * @param string $codeDept Code du département
     *
     * @return null|Departement
     * @throws Exception
     */
    public function getDepartementParCode($codeDept)
    {
        try {
            return $this->getDepartementRepository()->getDepartementParCode($codeDept);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne l'Epci à partir de son code
     *
     * @param string $codeEpci Code Epci
     *
     * @return null|Epci
     * @throws Exception
     */
    public function getEpciPourCode($codeEpci)
    {
        try {
            return $this->getEpciRepository()->getEpciPourCode($codeEpci);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la commune à partir de son code
     *
     * @param string $codeCommune Code de la commune
     *
     * @return null|Commune
     * @throws Exception
     */
    public function getCommuneParCode($codeCommune)
    {
        try {
            return $this->getCommuneRepository()->getCommuneParCode($codeCommune);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des départements trié sur le nom pour les régions indiquées
     *
     * @param array $tabCodeRegion Liste des codes région
     *
     * @return array
     * @throws Exception
     */
    public function getDepartementsPourListeRegion($tabCodeRegion)
    {
        try {
            return $this->getDepartementRepository()->getDepartementsPourListeRegion($tabCodeRegion);
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }

    /**
     * Retourne si oui ou non la commune désignée fait partie d'une region
     *
     * @param  integer $codeCommune Code INSEE de la commune
     * @param  array $listeDept  liste des communes de la region
     *
     * @return boolean
     */
    public function estDansLaRegion($codeCommune,$listeDept)
    {
        $commune = $this->getCommuneParCode($codeCommune);
        if(!$commune){
            return false;
        }
        return in_array($commune->getCodeDepartement(),$listeDept);
    }
}
