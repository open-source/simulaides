<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 10:25
 */

namespace Simulaides\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Internal\Hydration\ArrayHydrator;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\ParametreTechConstante;
use Simulaides\Constante\RegleVariableConstante;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\TravauxConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\ParametreTech;
use Simulaides\Domain\Entity\PrixAdmin;
use Simulaides\Domain\Entity\Produit;
use Simulaides\Domain\Entity\Travaux;
use Simulaides\Domain\Entity\ValeurListe;
use Simulaides\Domain\Repository\ProduitRepository;
use Simulaides\Domain\Repository\TravauxRepository;
use Simulaides\Domain\SessionObject\ProjetSession;
use Simulaides\Domain\SessionObject\Simulation\CoutsProduit;
use Simulaides\Domain\SessionObject\Travaux\ProduitSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitCoutSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitParametreTechSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitSession;
use Simulaides\Domain\SessionObject\Travaux\TravauxSession;
use Simulaides\Form\Dispositif\EditeurRegleForm;
use Simulaides\Form\Travaux\BbcForm;
use Simulaides\Form\Travaux\Hydrator\TravauxProduitHydrator;
use Simulaides\Form\Travaux\MultiTravauxForm;
use Simulaides\Form\Travaux\SaisieProduitFieldset;
use Simulaides\Form\Travaux\TravauxForm;
use Simulaides\Logs\Factory\LoggerFactory;
use Simulaides\Tools\Utils;
use Zend\Form\FormElementManager;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\ArraySerializable;
use Zend\Stdlib\Parameters;
use Zend\View\Model\ViewModel;

/**
 * Class TravauxService
 *
 * @package Simulaides\Service
 */
class TravauxService extends AbstractEntityService
{

    /** Nom du service */
    const SERVICE_NAME = 'TravauxService';

    /**
     * @var TravauxRepository
     */
    protected $travauxRepository;

    /** @var  ProduitRepository */
    protected $produitRepository;

    /**
     * @var FormElementManager
     */
    protected $formElementManager;

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /**
     * @var ProjetSessionContainer
     */
    protected $projetSession;

    /** @var  SessionContainerService */
    private $sessionContainer;

    /** @var  ProduitService */
    private $produitService;

    /** @var  ValeurListeService */
    private $valeurListeService;

    /** @var  LocalisationService */
    private $localisationService;

    /**
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\Travaux';

    /** @var  Logger */
    private $loggerService;

    /**
     * Service sur les règles
     *
     * @var RegleService
     */
    private $regleService;

    /**
     * @param EntityManagerInterface  $entityManager
     * @param FormElementManager      $formElementManager
     * @param ProjetSessionContainer  $projetSessionContainer
     * @param SessionContainerService $sessionContainer
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormElementManager $formElementManager,
        ProjetSessionContainer $projetSessionContainer,
        SessionContainerService $sessionContainer,
        ServiceLocatorInterface $serviceLocator
    ) {
        $this->travauxRepository  = $entityManager->getRepository($this->className);
        $this->produitRepository  = $entityManager->getRepository('Simulaides\Domain\Entity\Produit');
        $this->sessionContainer   = $sessionContainer;
        $this->formElementManager = $formElementManager;
        $this->projetSession      = $projetSessionContainer;
        $this->entityManager      = $entityManager;
        $this->serviceLocator     = $serviceLocator;
    }

    /**
     * @return TravauxRepository
     */
    public function getRepository()
    {
        return $this->travauxRepository;
    }

    /**
     * @param string  $type
     * @param array $condition
     *
     * @return mixed
     * @throws \Exception
     */
    public function getTravauxByTypeLogementEtRegion($type, $condition)
    {
        //récuperation pour outre-mer
        $outre_mer = (isset($condition['outre_mer']))? $condition['outre_mer'] : 0;
        $isMayotte = (isset($condition['isMayotte']))? $condition['isMayotte'] : 0;
        $isReunion = (isset($condition['isReunion']))? $condition['isReunion'] : 0;
        $isMartinique = (isset($condition['isMartinique']))? $condition['isMartinique'] : 0;
        $isGuadeloupe = (isset($condition['isGuadeloupe']))? $condition['isGuadeloupe'] : 0;

        try {
            //Récupération du type de travaux en fonction du logement
            if ($type === ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON) {
                $type = Travaux::CHAMP_MAISON;
            } else {
                $type = Travaux::CHAMP_APPARTEMENT;
            }
            //Liste ds travaux du type de logement
            $tabTravaux = $this->travauxRepository->getTravauxByLogementTypeEtRegion($type, $outre_mer);

            //Initialisation du tableau pour avoir les catégories dans l'ordre
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_ETUDE]                  = [];
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_ISOLATION]              = [];
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_EQUIPEMENT]             = [];
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_ENERGIES_RENOUVELABLES] = [];
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_ELECTROMENAGER] = [];

            /** @var Travaux $travaux */
            foreach ($tabTravaux as $travaux) {
                //gestion des cas particuliers Outre Mer
                if($outre_mer){
                    if($isReunion && in_array($travaux->getCode(),['T33'])){
                        // Si on est à la réunion, on n'affiche pas le travaux T33
                        continue;
                    }

                    if(!$isReunion && !$isGuadeloupe  && in_array($travaux->getCode(),['T53','T54','T55'])){
                        // Si on n'est pas à la réunion et pas à la guadeloupe, on n'affiche pas les travaux T53, T54, T55
                        continue;
                    }

                    if (in_array($travaux->getCode(),['T29','T42'])) {
                        // Si on est en outre-mer, on n'affiche pas le travaux T29,T42
                        continue;
                    }

                    if (!$isMayotte && in_array($travaux->getCode(),['T44','T52'])) {
                        // Si on n'est pas à Mayotte, on n'affiche pas le travaux T44
                        continue;
                    }
                }

                //fin gestion des cas particuliers
                if (!isset($triTravaux[$travaux->getCategorie()])) {
                    //Au cas ou la catégorie du travaux n'existerait pas dans l'initialisation
                    $triTravaux[$travaux->getCategorie()] = [];
                }
                //Ajout du travaux dans la catégorie
                $triTravaux[$travaux->getCategorie()][] = [
                    'code'     => $travaux->getCode(),
                    'intitule' => $travaux->getIntitule(),
                    'cat'      => $travaux->getValeurListe()->getLibelle()
                ];
            }

            return $triTravaux;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }


    /**
     * Retourne le formulaire de saisie d'un produit
     *
     * @param ProjetSession $projet
     * @param               $codeTravaux
     * @param               $label
     * @param               $type
     *
     * @param               $isEtude
     *
     * @return TravauxForm
     * @throws \Exception
     */
    public function getTravauxForm(ProjetSession $projet, $codeTravaux, $label, $type, $isEtude)
    {
        try {
            if ($type === ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON) {
                $type = 'maison';
            } else {
                $type = 'appartement';
            }
            //Récupère la liste des produits pris en charge par le travaux
            $produits = $this->produitRepository->getProduitByCodeTravauxEtTypeLogement($codeTravaux, $type);

            /** @var Produit $prod */
            $produitsSession = [];
            foreach ($produits as $prod) {
                //Création du produit
                $produitSession = new ProduitSession();
                $produitSession->setCodeProduit($prod->getCode());
                $produitSession->setLibelle($prod->getIntitule());
                $produitSession->setInfosSaisieCout($prod->getInfosSaisieCouts());
                $produitSession->setInfosSaisieCoutOu($prod->getInfosSaisieCoutsOu());
                $params = $prod->getParamsTech();
                //ajout des 3 couts au produit
                $this->buildSaisieProduitMontant($produitSession, SessionConstante::COUT_TOTAL);
                $this->buildSaisieProduitMontant($produitSession, SessionConstante::COUT_MO);
                $this->buildSaisieProduitMontant($produitSession, SessionConstante::COUT_FO);
                //ajout des param tech du produit
                $params->map(
                    function (ParametreTech $param) use ($produitSession) {
                        $this->buildSaisieParamTech($produitSession, $param);
                    }
                );
                $produitsSession[] = $produitSession;
            }

            $isTravauxSelected = ($this->getSessionContainer()->getTravaux($codeTravaux) !== null);
            /** @var TravauxForm $travauxForm */



            $travauxForm = $this->formElementManager->get(
                'TravauxForm',
                [
                    'label'             => $label,
                    'projet'            => $projet,
                    'codeTravaux'       => $codeTravaux,
                    'produits'          => $produitsSession,
                    'isTravauxSelected' => $isTravauxSelected,
                    'avecCoutCalcule'   => $this->getSessionContainer()->estConseillerConnecte(),
                    'isEtude'           => $isEtude,
                    'isOutreMer'           => $isOutreMer
                ]
            );
            $hydrator    = new TravauxProduitHydrator();
            $travauxForm->setHydrator($hydrator);
            $travauxSession = $this->getSessionContainer()->getTravaux($codeTravaux);
            //si le travaux n'existe pas en session, création d'un nouveau
            if ($travauxSession === null) {
                $travauxSession = new TravauxSession();
            }
            $travauxForm->bind($travauxSession);

            if ($isTravauxSelected) {
                $this->checkSaisieHidable($travauxForm, $travauxSession);
            }

            return $travauxForm;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @return TravauxRepository
     */
    private function getTravauxRepository()
    {
        return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\Travaux');
    }

    /**
     * @return MultiTravauxForm[]
     * @throws \Exception
     */
    public function getMultiTravauxForm(ProjetSession $projet, $listeTravaux, $type, $isNew=true, $isOutreMer=false)
    {
        try {
            if ($type === ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON) {
                $type = 'maison';
            } else {
                $type = 'appartement';
            }

            $categories = [];

            foreach($listeTravaux as $travaux){
                $codeTravaux = $travaux->getCode();
                $isEtude     = $travaux->getCategorie() == ValeurListeConstante::CAT_TRAVAUX_ETUDE;
                $label       = $travaux->getIntitule();

                //Récupère la liste des produits pris en charge par le travaux
                $produits = $this->produitRepository->getProduitByCodeTravauxEtTypeLogement($codeTravaux, $type);

                /** @var Produit $prod */
                $produitsSession = [];
                foreach ($produits as $prod) {
                    //Création du produit
                    $produitSession = new ProduitSession();
                    $produitSession->setCodeProduit($prod->getCode());
                    $produitSession->setLibelle($prod->getIntitule());
                    $produitSession->setInfosSaisieCout($prod->getInfosSaisieCouts());
                    $produitSession->setInfosSaisieCoutOu($prod->getInfosSaisieCoutsOu());
                    $params = $prod->getParamsTech();
                    //ajout des 3 couts au produit
                    $this->buildSaisieProduitMontant($produitSession, SessionConstante::COUT_TOTAL);
                    $this->buildSaisieProduitMontant($produitSession, SessionConstante::COUT_MO);
                    $this->buildSaisieProduitMontant($produitSession, SessionConstante::COUT_FO);
                    //ajout des param tech du produit
                    $params->map(
                        function (ParametreTech $param) use ($produitSession) {
                            $this->buildSaisieParamTech($produitSession, $param);
                        }
                    );
                    $produitsSession[] = $produitSession;
                }

                //$isTravauxSelected = ($this->getSessionContainer()->getTravaux($codeTravaux) !== null);
                $localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
                $outre_mer = $localisationService->estOutreMer($projet->getCodeCommune());

                /** @var TravauxForm $travauxForm */
                $travauxForm = $this->formElementManager->get(
                    'TravauxForm',
                    [
                        'label'             => $label,
                        'projet'            => $projet,
                        'codeTravaux'       => $codeTravaux,
                        'produits'          => $produitsSession,
                        //'isTravauxSelected' => $isTravauxSelected,
                        'avecCoutCalcule'   => $this->getSessionContainer()->estConseillerConnecte(),
                        'isEtude'           => $isEtude,
                        'isNew'             => $isNew,
                        'isOutreMer'        => $outre_mer
                    ]
                );
                $travauxForm->setName($codeTravaux);
                $hydrator    = new TravauxProduitHydrator();
                $travauxForm->setHydrator($hydrator);
                $travauxSession = $this->getSessionContainer()->getTravaux($codeTravaux);
                //si le travaux n'existe pas en session, création d'un nouveau
                if ($travauxSession === null) {
                    $travauxSession = new TravauxSession();
                }
                $travauxForm->bind($travauxSession);

                $this->checkSaisieHidable($travauxForm, $travauxSession);

                if(!key_exists($travaux->getCategorie(), $categories)) {
                    $categories[$travaux->getCategorie()] = $this->formElementManager->get(
                        'MultiTravauxForm',
                        [
                            'label'                 => $travaux->getCategorie()
                        ]
                    );
                }
                $categories[$travaux->getCategorie()]->add($travauxForm);
            }

            return $categories;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }

    }

    /**
     * @param ProduitSession $produit
     * @param                $type
     */
    private function buildSaisieProduitMontant(ProduitSession $produit, $type)
    {
        $saisieCout = new SaisieProduitCoutSession();
        $saisieCout->setTypeCout($type);
        $saisieCout->setCodeProduit($produit->getCodeProduit());
        $produit->addSaisiesProduit($saisieCout);
    }


    /**
     * Permet d'ajouter un paramètre technique à un produit
     *
     * @param ProduitSession $produit
     * @param ParametreTech  $param
     */
    private function buildSaisieParamTech(ProduitSession $produit, ParametreTech $param)
    {
        $saisieParam = new SaisieProduitParametreTechSession();
        $saisieParam->setCodeParametre($param->getCode());
        $saisieParam->setCodeProduit($produit->getCodeProduit());
        $saisieParam->setParamType($param->getTypeData());
        $saisieParam->setParamLibelle($param->getLibelle());
        $saisieParam->setParamCodeListe($param->getCodeListe());
        $produit->addSaisiesProduit($saisieParam);
    }

    /**
     * @return \Simulaides\Service\SessionContainerService
     */
    public function getSessionContainer()
    {
        return $this->sessionContainer;
    }

    /**
     * @param \Simulaides\Service\SessionContainerService $sessionContainer
     */
    public function setSessionContainer($sessionContainer)
    {
        $this->sessionContainer = $sessionContainer;
    }

    /**
     * Permet de vérifier le formulaire et d'ajouter le travaux si tout va bien
     *
     * @param TravauxForm $travauxForm
     * @param Parameters  $data
     *
     * @return bool
     * @throws \Exception
     */
    public function processForm(array &$CategorieMultitravauxForms, Parameters $data)
    {
        $session = $this->getSessionContainer();

        if($session->offsetExists(SessionConstante::TRAVAUX)) {
            foreach ($session->offsetGet(SessionConstante::TRAVAUX) as $travaux) {
                //Récupération du code du travaux à supprimer
                $code = $travaux->getCodeTravaux();
                //Suppresion des prix calculés pour les produits et le projet
                $this->deletePrixAdminPourTravaux($code);
                //Suppression du travaux dans la session
                $session->deleteTravaux($code);
            }
        }

        try {
            $formAreValid = true;
            $formIsValid = false;
            foreach($CategorieMultitravauxForms as $travauxForms) {
                $travauxForms->setData($data);
                $TravauxForms = $travauxForms->getFieldsets();


                /** @var TravauxForm[] $TravauxForms */
                foreach ($TravauxForms as $index => $travauxForm) {
                    $formIsValid = false;
                    //Nombre de paramètre dans le formulaire
                    $saisies = $travauxForm->getFieldsets();
                    $nbParametreInit = end($saisies)->getNbParamTech();

                    $this->getSessionContainer()->offsetSet(SessionConstante::NB_PARAMETER, $nbParametreInit);

                    $travauxForm->setData($data[$travauxForm->getName()]);
                    if ($travauxForm->isValid()) {
                        $nbParameter = $this->getSessionContainer()->offsetGet(SessionConstante::NB_PARAMETER);
                        if ($nbParametreInit > 0 && $nbParameter == $nbParametreInit) {
                            $travauxForms->get($travauxForm->getName())->setErrorMessages(["Vous devez compléter les caractéristiques."]);
                            $formAreValid = false;
                            continue;
                        }

                        //Le isValid change le nb de paramètre en session si tout est OK
                        //Il faut donc que le nbInit soit différent de la session
                        //Ce test n'est fait que si il  existe au moins un param dans le formulaire initial

                        /** @var TravauxSession $travauxSession */
                        $travauxSession = $travauxForm->getObject();

                        if ($travauxSession->getCodeTravaux() === TravauxConstante::TRAVAUX_T01_CHGE_FENETRE) {
                            // Dans le cas du travaux changement de fenêtres, il peut être nécessaire de calculer la surface des fenêtres
                            $this->calculSurfaceFenetre($travauxSession);
                        }

                        //Calcul des couts du travaux
                        $this->calculCoutDuTravaux($travauxSession);

                        //Ajout du travaux dans la liste des travaux sélectionnés
                        $this->saveTravauxProduit($travauxSession);

                        $formIsValid = true;

                    }

                    $formAreValid = $formAreValid && $formIsValid;
                }
            }
            return $formAreValid;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @param TravauxSession $travauxSession
     */
    private function calculSurfaceFenetre($travauxSession)
    {
        /** @var ProduitSession $produitSession */
        foreach ($travauxSession->getProduits() as $produitSession) {
            $saisiesProduit = $produitSession->getSaisiesProduit();
            /* @var $surface SaisieProduitParametreTechSession */
            $nbValeur = $saisiesProduit[3]->getValeur();
            $surface  = $saisiesProduit[4];

            $surfaceValeur = $surface->getValeur();
            if (!empty($nbValeur) && empty($surfaceValeur)) {
                $surfaceParam = $surface->getCodeParametre();
                if (strpos($surfaceParam, 'POR_FEN') > 1) {
                    $surface->setValeur((int)$nbValeur * 2.58);
                } else {
                    $surface->setValeur((int)$nbValeur * 1.62);
                }
            }
        }
    }

    /**
     * Fonction de calcul des couts du travaux
     *
     * @param TravauxSession $travauxSession
     *
     * @throws \Exception
     */
    private function calculCoutDuTravaux($travauxSession)
    {
        try {
            /** @var ProjetSession $projetSession */
            $projetSession = $this->projetSession->getProjet();
            //Calcul des couts du travaux
            $mt_ht_total = 0;
            $mt_ht_mo    = 0;
            $mt_ht_fo    = 0;

            /** @var LocalisationService $localisationService */
            $localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
            /** @var ProjetSessionContainer $projet */
            $projet  = $this->serviceLocator->get(ProjetSessionContainer::SERVICE_NAME);
            $outre_mer = $localisationService->estOutreMer($projet->getProjet()->getCodeCommune());

            /** @var ProduitSession $produitSession */
            foreach ($travauxSession->getProduits() as $produitSession) {
                $nbUnite = null;

                $produit = $this->getProduitService()->getProduitParCodeProduit($produitSession->getCodeProduit());
                if (!empty($produit)) {
                    $regleNbU = $produit->getRegleNBU();
                    if (is_null($nbUnite)) {
                        $nbUnite = $this->getProduitService()->getNombreUnitePourProduit(
                            $projetSession->getSurfaceHabitable(),
                            $regleNbU
                        );
                    }
                }
                /** @var SaisieProduitSession $saisieProduit */
                foreach ($produitSession->getSaisiesProduit() as $saisieProduit) {
                    if ($saisieProduit instanceof SaisieProduitCoutSession) {
                        //Saisie de type Cout
                        if ($saisieProduit->getTypeCout() == SessionConstante::COUT_TOTAL) {
                            $mt_ht_total = $saisieProduit->getCout();
                        } elseif ($saisieProduit->getTypeCout() == SessionConstante::COUT_MO) {
                            $mt_ht_mo = $saisieProduit->getCout();
                        } elseif ($saisieProduit->getTypeCout() == SessionConstante::COUT_FO) {
                            $mt_ht_fo = $saisieProduit->getCout();
                        }
                    } elseif ($saisieProduit instanceof SaisieProduitParametreTechSession) {
                        if(($saisieProduit->getParamType() === ParametreTechConstante::TYPE_DATA_BOOLEEN) && !$saisieProduit->getValeur()){
                            $nbUnite = 0;
                        }
                        if(($saisieProduit->getParamType() === ParametreTechConstante::TYPE_DATA_BOOLEEN_RADIO)){
                            $nbUnite = 0;
                        }
                        if (is_null($nbUnite)) {
                            $nbUnite = $this->getProduitService()->getNombreUnitePourProduit(
                                $projetSession->getSurfaceHabitable(),
                                $regleNbU,
                                $saisieProduit->getCodeParametre(),
                                $saisieProduit->getValeur()
                            );
                        }
                    }
                }

                //Calcul du cout du produit
                $coutCalcule = $this->getProduitService()->getCouts(
                    $produitSession->getCodeProduit(),
                    $mt_ht_total,
                    $mt_ht_mo,
                    $mt_ht_fo,
                    $nbUnite,
                    $outre_mer
                );

                //Gestion de sauvegarde des prix calculés dans Prix_Admin
                $idProjet = $projetSession->getId();
                if (!empty($idProjet)) {
                    $this->sauvePrixAdmin($produitSession->getCodeProduit(), $idProjet, $coutCalcule);
                }

                //Affectation des valeurs des couts calculés
                /** @var SaisieProduitSession $saisieProduit */
                foreach ($produitSession->getSaisiesProduit() as $saisieProduit) {
                    if ($saisieProduit instanceof SaisieProduitCoutSession) {
                        /** @var SaisieProduitCoutSession $saisieProduit */
                        if ($saisieProduit->getTypeCout() == SessionConstante::COUT_TOTAL) {
                            $saisieProduit->setCoutCalcule(round($coutCalcule->getCoutHtTo(), 2));
                            $saisieProduit->setCoutUser($coutCalcule->getPrixUserTo());
                        } elseif ($saisieProduit->getTypeCout() == SessionConstante::COUT_MO) {
                            $saisieProduit->setCoutCalcule(round($coutCalcule->getCoutHtMo(), 2));
                            $saisieProduit->setCoutUser($coutCalcule->getPrixUserMo());
                        } elseif ($saisieProduit->getTypeCout() == SessionConstante::COUT_FO) {
                            $saisieProduit->setCoutCalcule(round($coutCalcule->getCoutHtFo(), 2));
                            $saisieProduit->setCoutUser($coutCalcule->getPrixUserFo());
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de sauvegarder les prix calculés
     *
     * @param string       $codeProduit Code du produit
     * @param int          $idProjet    Identifiant du projet
     * @param CoutsProduit $coutCalcule Couts et prix calculés
     */
    private function sauvePrixAdmin($codeProduit, $idProjet, $coutCalcule)
    {
        $estConseillerConnecte = $this->getSessionContainer()->estConseillerConnecte();
        if (!$estConseillerConnecte) {
            //si le particulier est en cours de simulation, on mémorise les prix calculés
            //Récupération du prix admin éventuellement existant
            $prixAdmin = $this->getProduitService()->getPrixAdminParCodeProduitCodeProjet($codeProduit, $idProjet);

            $prixUserMo = $coutCalcule->getPrixUserMo();
            $prixUserFo = $coutCalcule->getPrixUserFo();
            $prixUserTo = $coutCalcule->getPrixUserTo();

            if ((empty($prixUserFo) || $prixUserFo == 0)
                && (empty($prixUserMo)
                    || $prixUserMo == 0)
                && (empty($prixUserTo) || $prixUserTo == 0)
            ) {
                //Tous les prix sont à 0:
                //  si le prix admin existe, il faut le supprimer
                //  si il  n'existe pas, il ne faut pas l'ajouter
                if (!empty($prixAdmin)) {
                    $this->getProduitService()->deletePrixAdmin($prixAdmin);
                }
            } else {
                //Au moins un des prix est renseigné, on ajoute/modifie le prixAdmin
                if (empty($prixAdmin)) {
                    //Création d'un prix admin
                    $prixAdmin = new PrixAdmin();
                    $prixAdmin->setIdProjet($idProjet);
                    $prixAdmin->setCodeProduit($codeProduit);
                    $produit = $this->getProduitService()->getProduitParCodeProduit($codeProduit);
                    $prixAdmin->setProduit($produit);
                }
                //Mise à jour des informations de prix et de date

                if ($prixUserMo !== 0 && $prixUserMo !== 0.00) {
                    $prixAdmin->setPrixMO($prixUserMo);
                } else {
                    $prixAdmin->setPrixMO(null);
                }
                if ($prixUserTo !== 0 && $prixUserTo !== 0.00) {
                    $prixAdmin->setPrixTotal($prixUserTo);
                } else {
                    $prixAdmin->setPrixTotal(null);

                }
                if ($prixUserFo !== 0 && $prixUserFo !== 0.00) {
                    $prixAdmin->setPrixFourniture($prixUserFo);
                } else {
                    $prixAdmin->setPrixFourniture(null);
                }
                $prixAdmin->setDateSaisie(new \DateTime('now'));
                //Sauvegarde en base
                $this->getProduitService()->savePrixAdmin($prixAdmin);
            }
        }
    }

    /**
     * Permet de supprimer dans prix admin les prix sur le projet et le produit
     * du travaux supprimé
     *
     * @param string $codeTravaux Code du travaux
     *
     * @throws \Exception
     */
    public function deletePrixAdminPourTravaux($codeTravaux)
    {
        try {
            $estConseillerConnecte = $this->getSessionContainer()->estConseillerConnecte();
            if (!$estConseillerConnecte) {
                /** @var TravauxSession $travauxSession */
                $travauxSession = $this->sessionContainer->getTravaux($codeTravaux);
                /** @var ProjetSession $projetSession */
                $projetSession = $this->projetSession->getProjet();

                /** @var ProduitSession $produitSession */
                foreach ($travauxSession->getProduits() as $produitSession) {
                    $prixAdmin = $this->getProduitService()->getPrixAdminParCodeProduitCodeProjet(
                        $produitSession->getCodeProduit(),
                        $projetSession->getId()
                    );
                    if (!empty($prixAdmin)) {
                        $this->getProduitService()->deletePrixAdmin($prixAdmin);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @param TravauxSession $travauxSession
     */
    public function saveTravauxProduit(TravauxSession $travauxSession)
    {
        $travaux = $this->sessionContainer->getListTravaux();
        if (empty($travaux[$travauxSession->getCodeTravaux()])) {
            $this->sessionContainer->addTravaux($travauxSession);
        }
    }

    /**
     * Permet de supprimer tous les travaux de la session
     */
    public function deleteAllTravauxSession()
    {
        $this->sessionContainer->deleteAllTravaux();
    }

    /**
     * @return array
     */
    public function getTravauxEntitiesFromSelectedTravaux()
    {
        $selectedTravaux = $this->getSessionContainer()->getSelectedTravaux();

        return $this->travauxRepository->getTravauxByCodes($selectedTravaux);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getProduitEntitiesFromSelectedTravaux()
    {
        try {
            $selectedTravaux = $this->getSessionContainer()->offsetGet(SessionConstante::TRAVAUX);
            $prods           = [];
            /** @var TravauxSession $travaux */
            foreach ($selectedTravaux as $travaux) {
                $produits = $travaux->getProduits();
                /** @var ProduitSession $p */
                foreach ($produits as $p) {
                    $prods[] = $p->getCodeProduit();
                }
            }

            return $this->produitRepository->getProduitbyCodes($prods);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste des produits sélectionnés
     * avec la liste des saisies sur ces produits
     *
     * @param $simulationClone
     * @return array
     * @throws \Exception
     */
    public function getSelectedProduits($simulationClone = null)
    {
        try {
            //Récupération des travaux choisis
            $travaux = $this->getSessionContainer()->offsetGet(SessionConstante::TRAVAUX);
            $offresCee = $this->getSessionContainer()->offsetGet(SessionConstante::OFFRES);
            //Initialisation des éléments des travaux
            $travauxArray = [
                'travaux'  => $this->getSessionContainer()->getSelectedTravaux(),
                'produits' => [], //Liste des produits sélectionnés
                'saisies'  => [],//Liste des saisies sur les produits sélectionnés
                'offres-cee' => $simulationClone ? $simulationClone->getChoixDispositifs() : null
            ];
            //Parcours des travaux sélectionnés
            /** @var TravauxSession $trav */
            foreach ($travaux as $trav) {
                //Récupération des produits du travaux
                $produits = $trav->getProduits();
                //Parcours des produits du travaux
                /** @var ProduitSession $prod */
                foreach ($produits as $prod) {
                    //ajout du produit (code) aux éléments des travaux
                    $travauxArray['produits'][] = $prod->getCodeProduit();
                    //ajout des valeurs saisies aux éléments des travaux
                    $saisies = $prod->getSaisiesProduit();
                    foreach ($saisies as $saisie) {
                        //$saisie->setCodeProduit($prod->getCodeProduit());
                        $travauxArray['saisies'][] = $saisie;
                    }
                }
            }

            return $travauxArray;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @param $code
     *
     * @return Travaux
     */
    public function getTravauxByCode($code)
    {
        return $this->travauxRepository->getTravauxByCode($code);
    }

    /**
     * Retourne la liste des travaux pour la fiche dispositif
     *
     * @return mixed
     * @throws \Exception
     */
    public function getListTravauxPourFicheDispositif()
    {
        try {
            //Initialisation du tableau pour avoir les catégories dans l'ordre
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_ETUDE]                  = [];
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_ISOLATION]              = [];
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_EQUIPEMENT]             = [];
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_ENERGIES_RENOUVELABLES] = [];
            $triTravaux[ValeurListeConstante::CAT_TRAVAUX_ELECTROMENAGER] = [];

            //Liste des travaux du type de logement
            $tabTravaux = $this->travauxRepository->getListTravauxTrieParCategorie();

            /** @var Travaux $travaux */
            foreach ($tabTravaux as $travaux) {
                if (!isset($triTravaux[$travaux->getCategorie()])) {
                    //Au cas ou la catégorie du travaux n'existerait pas dans l'initialisation
                    $triTravaux[$travaux->getCategorie()] = [];
                }
                //Ajout du travaux dans la catégorie
                $triTravaux[$travaux->getCategorie()][] = [
                    'code'      => $travaux->getCode(),
                    'intitule'  => $travaux->getIntitule(),
                    'categorie' => $travaux->getValeurListe()->getLibelle()
                ];
            }

            return $triTravaux;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la vue de l'éditeur de règle
     * utilisé dans pour pour le travaux
     *
     * @param int              $idDispositifTravaux
     * @param EditeurRegleForm $form
     * @param                  $msgOk
     * @param                  $urlAction
     * @param                  $formName
     *
     * @return ViewModel
     * @throws \Exception
     */
    public function getEditeurRegleViewModel($idDispositifTravaux, $form, $msgOk, $msgKo, $urlAction, $formName)
    {
        try {
            //Liste des catégories pour un travaux
            $listCategorie = $this->getRegleService()->getListeCategoriePourTravaux();
            //Liste des variables pour les catégories
            $listVarCateg = $this->getRegleService()->getListVariableParCategoriePourTravaux(
                $idDispositifTravaux,
                $listCategorie
            );

            $model = new ViewModel(
                [
                    'form'            => $form,
                    'formName'        => $formName,
                    'msgOK'           => $msgOk,
                    'msgKo'           => $msgKo,
                    'url'             => $urlAction,
                    'listCategorie'   => $listCategorie,
                    'listVarCateg'    => $listVarCateg,
                    'listValVariable' => $this->getRegleService()->getListValeurPourVariableEditeur(),
                    'listOpCond'      => $this->getRegleService()->getListOperateurCondition(),
                    'listOpExpr'      => $this->getRegleService()->getListOperateurExpression(),
                    'aRoleAdeme'      => $this->aRoleAdeme()
                ]
            );
            $model->setTemplate('simulaides/editeur-regle.phtml');

            return $model;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @return bool
     */
    private function aRoleAdeme()
    {
        /** @var SessionContainerService $session */
        $session    = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);
        $conseiller = $session->getConseiller();
        if (null !== $conseiller) {
            $aRoleAdeme = $conseiller->aRoleADEME();
        } else {
            $aRoleAdeme = false;
        }

        return $aRoleAdeme;
    }

    /**
     * Retourne le service sur les règles desdispositifs
     *
     * @return array|object|RegleService
     */
    private function getRegleService()
    {
        if (empty($this->regleService)) {
            $this->regleService = $this->serviceLocator->get(RegleService::SERVICE_NAME);
        }
        return $this->regleService;
    }

    /**
     * Retourne la liste des travaux trié par catégorie
     *
     * @return array
     * @throws \Exception
     */
    public function getListTravauxTrieParCategorie()
    {
        try {
            return $this->travauxRepository->getListTravauxTrieParCategorie();
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste de tous les produits d'un travaux
     *
     * @param string $codeTravaux Code du travaux
     *
     * @return array
     * @throws \Exception
     */
    public function getProduitsParCodeTravaux($codeTravaux)
    {
        try {
            return $this->produitRepository->getProduitsParCodeTravaux($codeTravaux);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne la liste de tous les produits trié sur les travaux
     *
     * @return array
     * @throws \Exception
     */
    public function getTousProduitsTrieParCodeTravaux()
    {
        try {
            return $this->produitRepository->getTousProduitsTrieParCodeTravaux();
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne le service sur les produits
     *
     * @return array|object|ProduitService
     */
    private function getProduitService()
    {
        if (empty($this->produitService)) {
            $this->produitService = $this->serviceLocator->get(ProduitService::SERVICE_NAME);
        }
        return $this->produitService;
    }

    /**
     * Retourne le service sur les produits
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
        }
        return $this->valeurListeService;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }

    /**
     * Permet de mémoriser le calcul des couts pour le projet
     * A appeler lors de la sauvegarde d'un nouveau projet
     * car les couts n'ont pu être mémorisé lors de la saisie
     *
     * @param int $idProjet Identifiant du nouveau projet
     */
    public function memoriseCoutProduitPourProjet($idProjet)
    {
        //Parcours des travaux du projet
        $tabTravauxSession = $this->getSessionContainer()->getListTravaux();
        /** @var TravauxSession $travauxSession */
        foreach ($tabTravauxSession as $travauxSession) {
            //Parcours des produits du travaux
            /** @var ProduitSession $produitSession */
            foreach ($travauxSession->getProduits() as $produitSession) {
                //Initialisation d'un objet de couts de produit
                $coutCalcule = new CoutsProduit();
                /** @var SaisieProduitCoutSession $saisieProduit */
                foreach ($produitSession->getSaisiesProduit() as $saisieProduit) {

                    if ($saisieProduit instanceof SaisieProduitCoutSession) {
                        if ($saisieProduit->getTypeCout() == SessionConstante::COUT_TOTAL) {
                            $coutCalcule->setPrixUserTo($saisieProduit->getCoutUser());
                        } elseif ($saisieProduit->getTypeCout() == SessionConstante::COUT_MO) {
                            $coutCalcule->setPrixUserMo($saisieProduit->getCoutUser());
                        } elseif ($saisieProduit->getTypeCout() == SessionConstante::COUT_FO) {
                            $coutCalcule->setPrixUserFo($saisieProduit->getCoutUser());
                        }
                    }
                }
                //Mémorisation des couts calculés pour le produit
                $this->sauvePrixAdmin($produitSession->getCodeProduit(), $idProjet, $coutCalcule);
            }
        }
    }

    /**
     * Permet de retourner les travaux dans le cas de l'appel par service web
     *
     * @return array
     */
    public function getTravauxPourServiceWeb()
    {
        $data['travaux'] = [];

        $tabTravaux = $this->travauxRepository->getListTravauxTrieParCode();
        /** @var Travaux $travaux */
        foreach ($tabTravaux as $travaux) {
            $tabInfosTravaux   = [
                'code_travaux'       => $travaux->getCode(),
                'intitule_travaux'   => $travaux->getIntitule(),
                'code_categorie'     => $travaux->getCategorie(),
                'intitule_categorie' => $travaux->getValeurListe()->getLibelle(),
                'appartement'        => Utils::getBooleanLitteral($travaux->getAppartement()),
                'maison'             => Utils::getBooleanLitteral($travaux->getMaison()),
                'outre_mer'          => Utils::getBooleanLitteral($travaux->getOutreMer()),
                'produits'           => $this->getDataProduitsTravaux($travaux->getProduits())
            ];
            $data['travaux'][] = $tabInfosTravaux;
        }
        return $data;
    }

    /**
     * Permet de retourner la liste dse produits d'un travaux pour le service web
     *
     * @param ArrayCollection $tabProduits Liste de Produit
     *
     * @return array
     */
    private function getDataProduitsTravaux($tabProduits)
    {
        $data = [];
        /** @var Produit $produit */
        foreach ($tabProduits as $produit) {
            $tabInfosProduit = [
                'code_produit'       => $produit->getCode(),
                'intitule_produit'   => $produit->getIntitule(),
                'infos_saisie_couts' => $produit->getInfosSaisieCouts(),
                'regle_quantite'     => $produit->getRegleNBU(),
                'param_technique'    => $this->getDataParamTechProduitsTravaux($produit->getParamsTech())
            ];
            $data[]          = $tabInfosProduit;
        }
        return $data;
    }

    /**
     * Permet de retourner la liste des param tech d'un produit pour le service web
     *
     * @param ArrayCollection $tabParamTech Liste de ParamTech
     *
     * @return array
     */
    private function getDataParamTechProduitsTravaux($tabParamTech)
    {
        $data = [];
        /** @var ParametreTech $paramTech */
        foreach ($tabParamTech as $paramTech) {
            $tabInfosParamTech = [
                'code_param'     => $paramTech->getCode(),
                'intitule_param' => $paramTech->getLibelle(),
                'type_champ'     => $paramTech->getTypeData()
            ];
            //Si le paramètre est de type LISTE, récupération des valeurs possibles
            if ($paramTech->getTypeData() == ParametreTechConstante::TYPE_DATA_LISTE) {
                $tabInfosParamTech['valeurs_liste'] = $this->getDataValeurListeParamTechProduitsTravaux(
                    $paramTech->getCodeListe()
                );
            }
            $data[] = $tabInfosParamTech;
        }
        return $data;
    }

    /**
     * Permet de retourne la liste des valeurs possibles d'un paramètre technique
     * pour le produit d'un travaux du sevice web
     *
     * @param string $codeListe Code de la liste des valeurs à retourner
     *
     * @return array
     */
    private function getDataValeurListeParamTechProduitsTravaux($codeListe)
    {
        $data = [];

        $tabValeur = $this->getValeurListeService()->getListeValuesByCodeListe($codeListe);
        /** @var ValeurListe $valeurListe */
        foreach ($tabValeur as $valeurListe) {
            $data[] = [
                'code'   => $valeurListe->getCode(),
                'valeur' => $valeurListe->getLibelle()
            ];
        }
        return $data;
    }

    /**
     *
     *
     * @param TravauxForm    $travauxForm
     * @param TravauxSession $travauxSession
     *
     * @return void
     */
    private function checkSaisieHidable(TravauxForm $travauxForm, TravauxSession $travauxSession)
    {
        $produitFieldsets = $travauxForm->getFieldsets();
        $produits         = $travauxSession->getProduits();

        /** @var ProduitSession $produit */
        foreach ($produits as $produit) {
            $nbEmptyCost      = 0;
            $nbEmptyParamTech = 0;
            $saisieProduits   = $produit->getSaisiesProduit();
            //Recupère le nombre de paramètre technique par produit
            $nbParamTech = count($saisieProduits) - 3;

            foreach ($saisieProduits as $saisieProduit) {
                if ($saisieProduit instanceof SaisieProduitCoutSession) {
                    if (intval($saisieProduit->getCout()) !== 0) {
                        $nbEmptyCost++;
                    }
                } elseif ($saisieProduit instanceof SaisieProduitParametreTechSession) {
                    if (intval($saisieProduit->getValeur()) !== 0) {
                        $nbEmptyParamTech++;
                    }
                }
            }

            /** @var SaisieProduitFieldset $produitFieldset */
            $produitFieldset    = $produitFieldsets[$produit->getCodeProduit()];
            $CheckedElt         = $produitFieldset->get('devisUser');
            //$radioElt           = $produitFieldset->get('devisUser');
            if ($nbEmptyCost === 3 &&  ($nbParamTech === 0 || $nbParamTech !== $nbEmptyParamTech)) {
                //$radioElt->setAttribute('value', '1');
                $CheckedElt->setChecked(true);
            } else {
                //$radioElt->setAttribute('value', '0');
                $CheckedElt->setChecked(false);
            }
        }
    }

    /**
     * @param $urlAction
     *
     * @return BbcForm
     */
    public function getBbcForm($urlAction)
    {
        /** @var BbcForm $bbcForm */
        $bbcForm = $this->formElementManager->get('BbcForm');

        $bbcForm->setHydrator(new ArraySerializable());

        $bbcForm->setAttribute('method', 'post');

        $bbcForm->setAttribute('action', $urlAction);

        $bbcForm->prepare();

        return $bbcForm;
    }
}
