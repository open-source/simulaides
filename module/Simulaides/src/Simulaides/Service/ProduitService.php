<?php
namespace Simulaides\Service;

use Doctrine\ORM\EntityManagerInterface;
use Simulaides\Domain\Entity\PrixAdmin;
use Simulaides\Domain\Entity\Produit;
use Simulaides\Domain\Repository\PrixAdminRepository;
use Simulaides\Domain\Repository\ProduitRepository;
use Simulaides\Domain\Repository\SaisieProduitRepository;
use Simulaides\Domain\SessionObject\Simulation\CoutsProduit;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ProduitService
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class ProduitService extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'ProduitService';

    /** @var  ProduitRepository */
    private $produitRepository;

    /** @var SaisieProduitRepository */
    private $saisieProduitRepository;

    /** @var  PrixAdminRepository */
    private $prixAdminRepository;

    /**
     * @var string
     */
    private $className = 'Simulaides\Domain\Entity\Produit';

    /**
     * @var string
     */
    private $classNameSaisieProduit = 'Simulaides\Domain\Entity\SaisieProduit';

    /**
     * @var string
     */
    private $classNameProjetChoixDispositif = 'Simulaides\Domain\Entity\ProjetChoixDispositif';



    /**
     * @var string
     */
    private $classNamePrixAdmin = 'Simulaides\Domain\Entity\PrixAdmin';

    /**
     * @var string
     */
    private $classNameComparatif = 'Simulaides\Domain\Entity\Comparatif';

    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  Logger */
    private $loggerService;

    /**
     * Constructeur du service
     *
     * @param EntityManagerInterface  $entityManager
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(EntityManagerInterface $entityManager, ServiceLocatorInterface $serviceLocator)
    {
        $this->produitRepository       = $entityManager->getRepository($this->className);
        $this->saisieProduitRepository = $entityManager->getRepository($this->classNameSaisieProduit);
        $this->comparatifRepository     = $entityManager->getRepository($this->classNameComparatif);
        $this->projetChoixDispositifRepository     = $entityManager->getRepository($this->classNameProjetChoixDispositif);
        $this->prixAdminRepository     = $entityManager->getRepository($this->classNamePrixAdmin);
        $this->entityManager           = $entityManager;
        $this->serviceLocator          = $serviceLocator;
    }

    /**
     * Retourne le produit à partir de son code
     *
     * @param string $codeProduit Code du produit
     *
     * @return null|\Simulaides\Domain\Entity\Produit
     * @throws \Exception
     */
    public function getProduitParCodeProduit($codeProduit)
    {
        try {
            return $this->produitRepository->getProduitParCodeProduit($codeProduit);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de faire la modification ou l'ajout du produit
     *
     * @param Produit $produit
     *
     * @throws \Exception
     */
    public function saveProduit($produit)
    {
        try {
            $this->produitRepository->saveProduit($produit);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de récupérer les produits d'un travaux qui ont été
     * sélectionnés dans le projet
     *
     * @param string $codeTravaux Code du travaux
     * @param int    $idProjet    Identifiant du projet
     *
     * @return array
     * @throws \Exception
     */
    public function getProduitInfoParCodeTravauxEtIdProjet($codeTravaux, $idProjet)
    {
        try {
            return $this->produitRepository->getProduitInfoParCodeTravauxEtIdProjet($codeTravaux, $idProjet);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner la liste des saisies produit par le code produit
     * et le projet
     *
     * @param int    $idProjet    Identifiant du projet
     * @param string $codeProduit Code du produit
     *
     * @return array
     * @throws \Exception
     */
    public function getSaisieProduitParIdProjetEtCodeProduit($idProjet, $codeProduit)
    {
        try {
            return $this->saisieProduitRepository->getSaisieProduitParIdProjetEtCodeProduit($idProjet, $codeProduit);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de charger les informations découlant de la saisie des produits
     * sur le projet indiqué
     *
     * @param int $idProjet Identifiant du projet
     *
     * @return array
     * @throws \Exception
     */
    public function getSaisieProduitPourTravauxInfo($idProjet)
    {
        try {
            return $this->saisieProduitRepository->getSaisieProduitPourTravauxInfo($idProjet);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Effectue les calcul de couts d'un produit et retourne un objet
     * CoutsProduit contenant les prix (total, main oeuvre, fourniture / HT, TTC)
     *
     * @param string $codeProduit
     * @param float  $CoutSaisiTo Cout total saisi par l'utilisateur (null si non renseigné)
     * @param float  $CoutSaisiMo Cout main d'oeuvre saisi par l'utilisateur (null si non renseigné)
     * @param float  $CoutSaisiFo Cout fourniture saisi par l'utilisateur (null si non renseigné)
     * @param        $nombreUnites
     * @param        $outre_mer
     *
     * @return CoutsProduit
     * @throws \Exception
     */
    public function getCouts($codeProduit, $CoutSaisiTo, $CoutSaisiMo, $CoutSaisiFo, $nombreUnites,$outre_mer=false)
    {
        try {
            $prixMo = 0.0;
            $prixTo = 0.0;
            $cote   = 0.0;
            $tva    = 0.0;

            //Récupération des caractéristiques du produit
            $produit = $this->produitRepository->getProduitParCodeProduit($codeProduit);

            if(!empty($produit) && $outre_mer == 0) {
                $prixTo = $produit->getPrixUTotal();
                $prixMo = $produit->getPrixUMo();
                $cote   = $produit->getCote();
                $tva    = $produit->getTva();
            } else if(!empty($produit)){
                $prixTo = $produit->getPrixUTotalOu();
                $prixMo = $produit->getPrixUMoOu();
                $cote   = $produit->getCote();
                $tva    = $produit->getTvaOu();
            }

            // Variables des coût calculés (ce sont des coût HT comme les coûts saisis et les prix du référentiel
            $coutTo = 0.0;
            $coutMo = 0.0;
            $coutFo = 0.0;

            // Les prixUser sont les prix unitaires qui seront obtenus à partir
            // des couts saisis par l'utilisateur (administration du référentiel des prix)
            $prixUserTo = null;
            $prixUserMo = null;
            $prixUserFo = null;

            // Début du calcul
            //----------------
            // Si la quantiné de produit = 0, on renvoie des coûts = 0.0 (initialisés dans le constructeur)
            if ($nombreUnites === 0 || $nombreUnites === null) {
                return new CoutsProduit();
            }

            if($prixTo > 0){
                $ratioMo = $prixMo / $prixTo;
            }else{
                $ratioMo = 0;
            }

            // Si le coût total a été saisi
            if ($CoutSaisiTo > 0) {

                $coutTo     = $CoutSaisiTo;
                $prixUserTo = ($coutTo / $nombreUnites) * $cote;

                // Si le coût de la main d'oeuvre a été saisi
                if ($CoutSaisiMo > 0) {
                    $coutMo     = $CoutSaisiMo;
                    $coutFo     = $coutTo - $coutMo;
                    $prixUserMo = ($coutMo / $nombreUnites) * $cote;
                    $prixUserFo = $prixUserTo - $prixUserMo;

                } else {
                    // Si le coût de la main d'oeuvre n'a pas été saisi

                    // Si le coût des fournitures a été saisi
                    if ($CoutSaisiFo > 0) {
                        $coutFo     = $CoutSaisiFo;
                        $coutMo     = $coutTo - $coutFo;
                        $prixUserFo = ($coutFo / $nombreUnites) * $cote;
                        $prixUserMo = $prixUserTo - $prixUserFo;

                    } else {
                        $coutMo = $coutTo * $ratioMo;
                        $coutFo = $coutTo - $coutMo;
                    }
                }

            } else {
                // Si le coût total n'a pas été saisi

                // Si le coût de la main d'oeuvre a été saisi
                if ($CoutSaisiMo > 0) {
                    $coutMo     = $CoutSaisiMo;
                    $prixUserMo = ($coutMo / $nombreUnites) * $cote;

                    // Si le coût des fournitures a été saisi
                    if ($CoutSaisiFo > 0) {
                        $coutFo     = $CoutSaisiFo;
                        $coutTo     = $coutMo + $coutFo;
                        $prixUserFo = ($coutFo / $nombreUnites) * $cote;
                        $prixUserTo = $prixUserMo + $prixUserFo;

                    } else {
                        $coutTo = $coutMo / $ratioMo;
                        $coutFo = $coutTo - $coutMo;
                    }

                } else {
                    // Si le coût des fournitures a été saisi
                    if ($CoutSaisiFo > 0) {
                        $coutFo     = $CoutSaisiFo;
                        $prixUserFo = ($coutFo / $nombreUnites) * $cote;
                        $coutTo     = $coutFo / (1 - $ratioMo);
                        $coutMo     = $coutTo - $coutFo;

                    } else {
                        // Aucun coût n'a été saisi
                        $coutTo = ($nombreUnites / $cote) * $prixTo;
                        $coutMo = $coutTo * $ratioMo;
                        $coutFo = $coutTo - $coutMo;
                    }
                }
            }

            $coutsProduit = new CoutsProduit();
            $coutsProduit->setCoutHtTo($coutTo);
            $coutsProduit->setCoutHtMo($coutMo);
            $coutsProduit->setCoutHtFo($coutFo);
            $coutsProduit->setCoutTtcTo($coutTo * (1 + $tva / 100));
            $coutsProduit->setCoutTtcMo($coutMo * (1 + $tva / 100));
            $coutsProduit->setCoutTtcFo($coutFo * (1 + $tva / 100));
            $coutsProduit->setPrixUserTo($prixUserTo);
            $coutsProduit->setPrixUserMo($prixUserMo);
            $coutsProduit->setPrixUserFo($prixUserFo);

            return $coutsProduit;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de retourner le nombre d'unité d'un produit
     *
     * @param      $surfaceHabitable
     * @param      $regleNBU
     * @param null $codeParam
     * @param null $valeurParam
     *
     * @return int|null|string
     * @throws \Exception
     */
    public function getNombreUnitePourProduit($surfaceHabitable, $regleNBU, $codeParam = null, $valeurParam = null)
    {
        try {
            $nombreUnite = null;
            // si la regle NBU est un numérique c'est qu'elle porte elle même le nombre d'unités
            if (is_numeric($regleNBU)) {
                $nombreUnite = $regleNBU;

                // si la regle NBU contient "SURF_HABITABLE", alors il s'agit de la valeur
                //saisie dans la partie "Ma situation" (parametre de la méthode)
            } else {
                if ($regleNBU == "SURF_HABITABLE") {
                    $nombreUnite = $surfaceHabitable;
                } else {
                    if (trim($regleNBU) == trim($codeParam)) {
                        $nombreUnite = $valeurParam;
                    }
                }
            }

            return $nombreUnite;
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de modifier ou d'ajouter un prix admin
     *
     * @param PrixAdmin $prixAdmin
     *
     * @throws \Exception
     */
    public function savePrixAdmin($prixAdmin)
    {
        try {
            $this->prixAdminRepository->savePrixAdmin($prixAdmin);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Suppresion d'un prix admin
     *
     * @param PrixAdmin $prixAdmin
     *
     * @throws \Exception
     */
    public function deletePrixAdmin($prixAdmin)
    {
        try {
            $this->prixAdminRepository->deletePrixAdmin($prixAdmin);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de récupérer un prix admin selon le cod eproduit et l'id projet
     *
     * @param string $codeProduit Code du produit
     * @param int    $idProjet    Identifiant du projet
     *
     * @return null|PrixAdmin
     * @throws \Exception
     */
    public function getPrixAdminParCodeProduitCodeProjet($codeProduit, $idProjet)
    {
        $prixAdmin = null;
        try {
            $prixAdmin = $this->prixAdminRepository->getPrixAdminParCodeProduitCodeProjet($codeProduit, $idProjet);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
        return $prixAdmin;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }
        return $this->loggerService;
    }

    /**
     * Permet de supprimer toutes les saisies d'un ou plusieurs projets
     *
     * @param array $tabIdProjet Liste des identifiant des projets
     *
     * @throws \Exception
     */
    public function deleteSaisieProduitPourListeProjet($tabIdProjet)
    {
        try {
            $this->saisieProduitRepository->deleteSaisieProduitPourProjet($tabIdProjet);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de supprimer tous les comparatifs d'un ou plusieurs projets
     *
     * @param array $tabIdProjet Liste des identifiant des projets
     *
     * @throws \Exception
     */
    public function deleteComparatifPourListeProjet($tabIdProjet)
    {
        try {
            $this->comparatifRepository->deleteComparatifPourProjet($tabIdProjet);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de supprimer tous les choix dispositifs d'un ou plusieurs projets
     *
     * @param array $tabIdProjet Liste des identifiant des projets
     *
     * @throws \Exception
     */
    public function deleteProjetChoixDispositifPourListeProjet($tabIdProjet)
    {
        try {
            $this->projetChoixDispositifRepository->deleteComparatifPourProjet($tabIdProjet);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }


    /**
     * Permet de retourner la liste des prix admin
     *
     * @throws \Exception
     */
    public function getAllPrixAdminPourExport()
    {
        try {
            return $this->prixAdminRepository->getAllPrixAdminPourExport();
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Retourne les saisie produit pour les ravaux d'un projet
     *
     * @param int    $idProjet    Identifiant du projet
     * @param string $codeTravaux Code du travaux
     *
     * @return array
     * @throws \Exception
     */
    public function getSaisieProduitParIdProjetEtCodeTravaux($idProjet, $codeTravaux)
    {
        try {
            return $this->saisieProduitRepository->getSaisieProduitParIdProjetEtCodeTravaux($idProjet, $codeTravaux);
        } catch (\Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }
}
