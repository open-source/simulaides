<?php
namespace Simulaides\Service;

use DateTime;
use Exception;
use Simulaides\Constante\GroupeTrancheConstante;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\TrancheConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\Entity\Region;
use Simulaides\Domain\Entity\Travaux;
use Simulaides\Domain\SessionObject\Simulation\AideTravauxResultat;
use Simulaides\Domain\SessionObject\Simulation\DispositifResultat;
use Simulaides\Logs\Factory\LoggerFactory;
use Simulaides\Tools\ResultatPDF;
use Simulaides\Tools\Utils;
use Zend\Log\Logger;
use Zend\Mime\Part;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;

/**
 * Classe GenerePdfService
 * Service permettant de générer les pdf
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Service
 */
class GenerePdfService
{

    /** Constante pour la largeur des tableaux */
    const WIDTH_TAB = 180;

    /** Nom du service */
    const SERVICE_NAME = 'GenerePdfService';

    /** Police de caractère */
    const FONT = 'dejavusans';

    /** Style de la police */
    const FONT_STYLE = 'B';

    /** Style de la police */
    const FONT_SIZE = 9;

    /**
     * @var ServiceLocatorInterface
     */
    private $serviceLocator;

    /**
     * @var SessionContainerService
     */
    private $sessionContainerService;

    /**
     * @var ProjetService
     */
    private $projetService;

    /** @var  Logger */
    private $loggerService;

    /**
     * @var SendMailService
     */
    private $sendMailService;

    /**
     * Projet à générer
     *
     * @var Projet
     */
    private $projet;

    /**
     * @var ValeurListeService
     */
    private $valeurListeService;

    /**
     * @var float $lineHeight
     */
    private $lineHeight;

    /** @var  float $maxLineHeight */
    private $maxLineHeight;

    /**
     * Constructeur
     *
     * @param $serviceLocator
     */
    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->lineHeight     = 0;
        $this->maxLineHeight  = 0;
    }

    /**
     * @return float
     */
    public function getMaxLineHeight()
    {
        return $this->maxLineHeight;
    }

    /**
     * @param float $maxLineHeight
     */
    public function setMaxLineHeight($maxLineHeight)
    {
        $this->maxLineHeight = $maxLineHeight;
    }

    /**
     * @return float
     */
    public function getLineHeight()
    {
        return $this->lineHeight;
    }

    /**
     * @param float $lineHeight
     */
    public function setLineHeight($lineHeight)
    {
        $this->lineHeight = $lineHeight;
    }

    /**
     * Permet de générer le PDF
     *
     * @param Projet $projet
     *
     * @return string
     * @throws Exception
     */
    public function genereResultatPdf($projet)
    {
        try {
            $this->projet = $projet;

            //Génération du nom de fichier
            $fileName = $this->getNomFichierPDF($projet->getNumero());
            $fileName = DIR_APPLI . '/data/tmp/' . $fileName;
            $pdf      = $this->genereCorpsResultatPDF();
            $pdf->Output($fileName, 'F');

            return $fileName;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de générer le PDF
     *
     * @param array  $idDispositifs
     * @param Region $region
     *
     * @return string
     * @throws Exception
     */
    public function genereResultatDispositifPdf($idDispositifs, $region)
    {
        try {
            $fileName = $this->getNomFichierPDFDispositif($region->getNom());
            $fileName = DIR_APPLI . '/data/tmp/' . $fileName;
            $pdf      = $this->genereCorpsResultatPDFDispositif($idDispositifs);
            $pdf->Output($fileName, 'F');

            return $fileName;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de générer le PDF et faire l'envoi par mail
     *
     * @param string $email Adresse mail
     * @param Projet $projet
     *
     * @return bool
     * @throws Exception
     */
    public function genereMailRestulatPdf($email, $projet)
    {
        try {
            //Affectation du projet
            $this->projet = $projet;
            //Génération du nom de fichier
            $fileName = $this->getNomFichierPDF($projet->getNumero());
            //Génération du corps du PDF
            $pdf = $this->genereCorpsResultatPDF();
            //Création du fichier PDF (non utilisation de l'option 'E' comme expliqué dans la doc TcPDF,
            //car pb d'encodage lors de l'envoi du mail
            $pdf->Output(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName, 'F');
            //Ouverture du fichier pour le passer en pièce jointe
            $openFile = fopen(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName, 'r');
            //ajout du fichier PDF en pièce jointe
            $attachement           = new Part($openFile);
            $attachement->filename = $fileName;
            $attachement->type     = 'application/pdf';

            //Corps du mail avec le texte paramétré
            $corpsMail = $this->getValeurListeService()->getTextAdministrable(
                ValeurListeConstante::CORPS_MAIL_PDF
            );
            //sujet du mail
            $sujetMail = "[Ne pas répondre à ce message] Votre simulation d'aides financières";

            //Envoi du mail
            $sendMail = $this->getSendMailService();
            $sendMail->sendMail([$email], $corpsMail, $sujetMail, 'simulaides@ademe.fr', "SIMUL'AIDES", $attachement);

            return true;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }



    /**
     * Permet de faire l'envoi par mail sans pdf
     *
     * @param string $email Adresse mail
     * @param Projet $projet
     *
     * @return bool
     * @throws Exception
     */
    public function genereMailSansPdf($email, $projet)
    {
        try {
            //Affectation du projet
            $this->projet = $projet;
            //Corps du mail avec le texte paramétré
            $corpsMail = $this->getValeurListeService()->getTextAdministrable(
                ValeurListeConstante::CORPS_MAIL_PDF
            );

            $corpsMail = str_replace('NUMERO_SIMU',$projet->getNumero(),$corpsMail);
            $corpsMail = str_replace('MOT_DE_PASSE_SIMU',$projet->getMotPasse(),$corpsMail);
            //sujet du mail
            $sujetMail = "[Ne pas répondre à ce message] Votre simulation d'aides financières";

            //Envoi du mail
            $sendMail = $this->getSendMailService();
            $sendMail->sendMail([$email], $corpsMail, $sujetMail, 'simulaides@ademe.fr', "SIMUL'AIDES");

            return true;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de générer le PDF et faire l'envoi par mail
     *
     * @param string $email Adresse mail
     * @param Projet $projet
     *
     * @return bool
     * @throws Exception
     */
    public function genereMailWithSmallPdf($email, $projet)
    {
        try {
            //Affectation du projet
            $this->projet = $projet;
            //Génération du nom de fichier
            $fileName = $this->getNomFichierPDF($projet->getNumero());
            //Génération du corps du PDF
            $pdf = $this->genereCorpsResultatSmallPDF();
            //Création du fichier PDF (non utilisation de l'option 'E' comme expliqué dans la doc TcPDF,
            //car pb d'encodage lors de l'envoi du mail
            $filePath = sys_get_temp_dir() .  DIRECTORY_SEPARATOR . $fileName;
            $pdf->Output($filePath, 'F');
            //Ouverture du fichier pour le passer en pièce jointe
            $openFile = fopen($filePath, 'r');
            //ajout du fichier PDF en pièce jointe
            $attachement           = new Part($openFile);
            $attachement->filename = $fileName;
            $attachement->type     = 'application/pdf';

            //Corps du mail avec le texte paramétré
            $corpsMail = $this->getValeurListeService()->getTextAdministrable(
                ValeurListeConstante::CORPS_MAIL_PDF
            );
            $corpsMail = str_replace('NUMERO_SIMU',$projet->getNumero(),$corpsMail);
            $corpsMail = str_replace('MOT_DE_PASSE_SIMU',$projet->getMotPasse(),$corpsMail);

            //sujet du mail
            $sujetMail = "[Ne pas répondre à ce message] Votre simulation d'aides financières";

            //Envoi du mail
            $sendMail = $this->getSendMailService();
            $sendMail->sendMail([$email], $corpsMail, $sujetMail, 'simulaides@ademe.fr', "SIMUL'AIDES", $attachement);
            unlink($filePath);

            return true;
        } catch (Exception $e) {
            $this->getLogger()->err($e->getMessage());
            throw $e;
        }
    }

    /**
     * @param null $pdf
     * @param null $simulationNumber
     *
     * @return ResultatPDF
     * @throws Exception
     */
    public function genereCorpsResultatPDF($pdf = null, $simulationNumber = null)
    {
        if ($pdf === null) {
            //Création du document
            $pdf = $this->initPdf();
        }

        $pdf->AddPage();

        $produits      = $this->projet->getProduits();
        $travauxParCat = [];

        $libelleCat = [];

        foreach ($produits as $produit) {
            if (!isset($travauxParCat[$produit->getTravaux()->getValeurListe()->getLibelle()])) {
                $travauxParCat[$produit->getTravaux()->getValeurListe()->getLibelle()] = [];
                $libelleCat[$produit->getTravaux()->getCategorie()]                    = $produit->getTravaux()->getValeurListe()->getLibelle();
            }
            if (!in_array($produit->getTravaux()->getIntitule(), $travauxParCat[$produit->getTravaux()->getValeurListe()->getLibelle()])) {
                $travauxParCat[$produit->getTravaux()->getValeurListe()->getLibelle()][] = $produit->getTravaux()->getIntitule();
            }
        }

        $codeCommune = $this->projet->getCodeCommune();
        $localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
        $projetSimulService = $this->serviceLocator->get(ProjetSimulService::SERVICE_NAME);
        $idf = $localisationService->estDansLaRegion($codeCommune,MainConstante::$LISTE_IDF);
        $codeTranche = '';
        if($idf){
            $codeTranche = $projetSimulService ->getCodeTranche($this->projet, GroupeTrancheConstante::MPR_IDF);
        }else{
            $codeTranche = $projetSimulService->getCodeTranche($this->projet, GroupeTrancheConstante::MPR);
        }
        $texteMPRChapo = '';
        if($codeTranche == TrancheConstante::TRES_MOD_MPR || $codeTranche == TrancheConstante::TRES_MOD_MPR_I){
            $texteMPRTranche =  $this->getValeurListeService()->getTextAdministrable(
                ValeurListeConstante::TEXTE_RESULTAT_MAPRIMRENOV_TRESMODESTE
            );
        }elseif($codeTranche == TrancheConstante::MODESTE_MPR || $codeTranche == TrancheConstante::MODESTE_MPR_IDF){
            $texteMPRTranche =  $this->getValeurListeService()->getTextAdministrable(
                ValeurListeConstante::TEXTE_RESULTAT_MAPRIMRENOV_MODESTE
            );
        }elseif($codeTranche == TrancheConstante::INTER_MPR || $codeTranche == TrancheConstante::INTER_MPR_IDF){
            $texteMPRTranche =  $this->getValeurListeService()->getTextAdministrable(
                ValeurListeConstante::TEXTE_RESULTAT_MAPRIMRENOV_INTREMEDIAIRE
            );
            $texteMPRChapo = $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_CHAPO_MAPRIMRENOV_INTERMEDIAIRE
            );
        }else{
            $texteMPRTranche =  $this->getValeurListeService()->getTextAdministrable(
                ValeurListeConstante::TEXTE_RESULTAT_MAPRIMRENOV_SUPERIEUR
            );
            $texteMPRChapo = $this->valeurListeService->getTextAdministrable(
                ValeurListeConstante::TEXTE_CHAPO_MAPRIMRENOV_SUPERIEUR
            );
        }

        $model = new ViewModel(
            [
                'projet'           => $this->projet,
                'simulationNumber' => $simulationNumber,
                'travauxParCat'    => $travauxParCat,
                'libelleCat'       => $libelleCat,
                'codeTranche' => $codeTranche,
                'texteMPRTranche' => $texteMPRTranche,
                'statutDemandeur' => $this->projet->getCodeStatut()

            ]
        );
        $model->setTemplate('simulaides/projet/recapitulatif-pdf.phtml');

        $viewRender = $this->serviceLocator->get('ViewRenderer');
        $html       = $viewRender->render($model);//179

        $pdf->Image(
            __DIR__ . '/../../../../../public/images/familly.png',
            '129',
            '84',
            40,
            21,
            '',
            '',
            'T',
            false,
            300,
            '',
            false,
            false,
            1,
            false,
            false,
            false
        );

        $pdf->SetXY(13, 20);
        $pdf->writeHTML($html);
        $currentY = $pdf->getY();
        $pdf->Image(__DIR__ . '/../../../../../public/images/home2.png', '130', $pdf->getY() - 26.5, 40, 21, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
        $pdf->Image(__DIR__ . '/../../../../../public/images/home2.png', '130', '50', 40, 21, '', '', 'T', false, 300, '', false, false, 1, false, false, false);

        $pdf->SetXY(13, $currentY);
        //$pdf->AddPage();

        $msgPRIS = $this->getProjetService()->getResultatMessagePRIS();

        $isAdmin = $this->getSessionContainerService()->estConseillerConnecte();

        $resultatAide = $this->getProjetService()->getResultatAide($isAdmin);

        if(count($this->projet->getTravaux()->toArray()) == 0
           || $this->projet->getLogement()->getCode() != 'M'
           || ($this->projet->getCodeStatut()!= 'PROP_RES_1' && $this->projet->getCodeStatut()!= 'PROP_BAIL') ){
            $texteMPRChapo = '';
        }

        $model = new ViewModel(
            [
                'projet'       => $this->projet,
                'isComparatif' => $simulationNumber !== null,
                'msgPRIS'      => $msgPRIS,
                'resultatAide' => $resultatAide,
                'texteMPRChapo' => $texteMPRChapo
            ]
        );
        $model->setTemplate('simulaides/projet/recapitulatif-resultat-pdf.phtml');

        $html = $viewRender->render($model);

        $pdf->writeHTML($html);
        //$pdf->writeHTML('Pour prendre rendez-vous, contactez le 0808 800 700, prix d\'un appel local ou bien connectez-vous sur le site <a href="renovation-info-service.gouv.fr">renovation-info-service.gouv.fr</a>, rubrique j\'agis.');
        //$pdf->writeHTML('<br>Le réseau Rénovation Info Service rassemble les Espaces INFO>ENERGIE coordonnés par l\'ADEME et le réseau de l\'ANAH.');

        $pdf->AddPage();

        //Dispositifs
        $this->getDispositifs($pdf);

        //Autre dispositif
        $this->getAutreDispositif($pdf);

        //Point Info Rénovation
        $this->getPointRenovation($pdf);

        //Commentaire conseiller
        $this->getCommentaireConseiller($pdf);

        //Rappel des précautions
        $this->getTexteRappelPrecaution($pdf);

        return $pdf;
    }


    /**
     * @param null $pdf
     * @param null $simulationNumber
     *
     * @return ResultatPDF
     * @throws Exception
     */
    public function genereCorpsResultatSmallPDF($pdf = null, $simulationNumber = null)
    {
        if ($pdf === null) {
            //Création du document
            $pdf = $this->initPdf();
        }

        $pdf->AddPage();

        $corpsMail = $this->getValeurListeService()->getTextAdministrable(
            ValeurListeConstante::CORPS_MAIL_PDF
        );

        $corpsMail = str_replace('NUMERO_SIMU',$this->projet->getNumero(),$corpsMail);
        $corpsMail = str_replace('MOT_DE_PASSE_SIMU',$this->projet->getMotPasse(),$corpsMail);

        $model = new ViewModel(
            [
                'corpsMail'           => $corpsMail,

            ]
        );
        $model->setTemplate('simulaides/projet/recapitulatif-small-pdf.phtml');

        $viewRender = $this->serviceLocator->get('ViewRenderer');
        $html       = $viewRender->render($model);//179
        $pdf->writeHTML($html);

        return $pdf;
    }

    /**
     * @param array $idDispositifs
     *
     * @return ResultatPDF
     * @throws Exception
     */
    private function genereCorpsResultatPDFDispositif($idDispositifs)
    {
        set_time_limit(0);
        //Création du document
        $pdf = $this->initPdf();

        /* @var $dispositifService DispositifService */
        $dispositifService = $this->serviceLocator->get(DispositifService::SERVICE_NAME);
        /* @var $regleService RegleService */
        $regleService  = $this->serviceLocator->get(RegleService::SERVICE_NAME);
        $numDispositif = 0;
        foreach ($idDispositifs as $idDispositif) {
            if(!is_int($idDispositif)){continue;}
            $numDispositif++;
            $leDispositif = $dispositifService->getDispositifParId($idDispositif);
            $model        = new ViewModel(
                [
                    'dispositif'        => $leDispositif,
                    'dispositifService' => $dispositifService,
                    'regleService'      => $regleService,
                    'numDispositif'     => $numDispositif
                ]
            );
            $model->setTemplate('simulaides/dispositif-gestion/dispositif-pdf.phtml');

            $viewRender = $this->serviceLocator->get('ViewRenderer');
            $html       = $viewRender->render($model);
            $pdf->AddPage();
            $pdf->writeHTML($html);

            //pour les travaux au verso
            $dispositifTravauxService = $this->serviceLocator->get(DispositifTravauxService::SERVICE_NAME);
            $model                    = new ViewModel(
                [
                    'dispositif'               => $leDispositif,
                    'dispositifService'        => $dispositifService,
                    'regleService'             => $regleService,
                    'dispositifTravauxService' => $dispositifTravauxService
                ]
            );
            $model->setTemplate('simulaides/dispositif-gestion/dispositif-travaux-pdf.phtml');

            $viewRender = $this->serviceLocator->get('ViewRenderer');
            $html       = $viewRender->render($model);

            $pdf->AddPage();
            $pdf->writeHTML($html);

        }

        return $pdf;
    }

    /**
     * @param Projet $projet
     */
    public function setProjet($projet)
    {
        $this->projet = $projet;
    }

    /**
     * Permet de générer le bandeau de titre
     *
     * @param ResultatPDF $pdf
     */
    private function getTitle($pdf)
    {
        $texte = 'Résultat de votre simulation';

        $this->setColorFondJaune($pdf);
        $pdf->SetDrawColor(127, 127, 127); //Couleur de bordure

        $pdf->SetLineWidth(0.4); //Taille de la bordure

        //Ajout d'une cellule avec le texte centré avec une bordure au bas
        $pdf->Cell(self::WIDTH_TAB, 7, $texte, self::FONT_STYLE, 0, 'C', true);

        $pdf->Ln();
    }

    /**
     * Permet de générer le bloc sur les caractéristiques
     *
     * @param ResultatPDF $pdf
     *
     * @throws Exception
     */
    private function getCaracteristique($pdf)
    {
        $tabCaracteristique = $this->getProjetService()->getResultatCaracteristiquesPDF($this->projet);

        $pdf->SetFont(self::FONT, '', self::FONT_SIZE);
        $this->setColorFondGris($pdf);

        $pdf->setCellPaddings('', 2, '', '');

        //Création d'un tableau avec 4 cellules
        $iCol     = 0;
        $widthCol = self::WIDTH_TAB / 4;
        foreach ($tabCaracteristique as $rowCaracteristique) {
            if (count($rowCaracteristique) == 1) {
                $widthColCurrent = 0;
            } else {
                $widthColCurrent = $widthCol;
            }
            foreach ($rowCaracteristique as $caracteristique) {
                //1ere celule : libellé
                $this->setColorLabel($pdf);
                $pdf->Cell($widthCol, 0, $caracteristique['label'], '', 0, 'R', true);
                //2ème celule : valeur
                $this->setColorValue($pdf);
                $pdf->Cell($widthColCurrent, 0, $caracteristique['value'], '', 0, 'L', true);
                $iCol++;
            }
            $pdf->Ln();
        }
        //Réinitialisation des padding
        $pdf->setCellPaddings('', 1, '', '');
        $pdf->Ln();
    }

    /**
     * @param ResultatPDF $pdf
     */
    private function getDataProjet($pdf)
    {
        $pdf->SetFillColor(255, 255, 255); //Couleur du fond
        $this->setColorBordureGrisClair($pdf);

        $wPartLeft  = 4 * (self::WIDTH_TAB / 6);
        $wPartRight = 2 * (self::WIDTH_TAB / 6);

        //Votre projet
        $pdf->SetFont(self::FONT, self::FONT_STYLE, 10);
        $this->setColorTextNoir($pdf);
        $pdf->Cell($wPartLeft, 0, "Votre projet :", 'LT');
        //Numéro simulation
        $pdf->Cell($wPartRight / 2, 0, "Numéro de simulation :", 'T', 0, 'R');
        $this->setColorValue($pdf);
        $pdf->Cell($wPartRight / 2, 0, $this->projet->getNumero(), 'TR', 0, 'L');
        $pdf->Ln();
        //Liste des travaux
        $i = 0;
        /** @var Travaux $travaux */
        foreach ($this->projet->getTravaux() as $travaux) {
            $pdf->SetFont(self::FONT, '', 10);
            $this->setColorValue($pdf);
            //ajout d'une cellule vice pour faire le décallage
            $pdf->Cell(10, 0, '', 'L', 0, 'C', true);
            //Ajout d'une cellule avec le libellé du travaux
            $pdf->Cell($wPartLeft - 10, 0, '- ' . $travaux->getIntitule(), '', 0, 'L', true);
            //Si on est sur le premier, impression du code d'accès
            if ($i == 0) {
                //Code d'accès
                $pdf->SetFont(self::FONT, self::FONT_STYLE);
                $this->setColorTextNoir($pdf);
                $pdf->Cell($wPartRight / 2, 0, "Code d'accès :", '', 0, 'R');
                $this->setColorValue($pdf);
                $pdf->Cell($wPartRight / 2, 0, $this->projet->getMotPasse(), 'R', 0, 'L');
            } else {
                //Ajout d'une ligne blanche pour la bordure de droite
                $pdf->Cell($wPartRight, 0, '', 'R', 0, 'L');
            }
            $i++;
            $pdf->Ln();
        }
        //Projet BBC
        $pdf->SetFont(self::FONT, '', self::FONT_SIZE);
        $text  = "Projet de rénovation énergétique globale, permettant d'atteindre le niveau BBC-rénovation:";
        $value = ($this->projet->getBbc() ? "Oui" : "Non");
        $this->setColorLabel($pdf);
        $pdf->Cell(self::WIDTH_TAB - 30, 0, $text, 'LB', 0, 'L', true);
        $this->setColorValue($pdf);
        $pdf->Cell(30, 0, $value, 'RB', 0, 'L', true);
        $pdf->Ln();
    }

    /**
     * Permet de générer la partie aide
     *
     * @param ResultatPDF $pdf
     *
     * @throws Exception
     */
    private function getInfosAides($pdf)
    {
        $isAdmin = $this->getSessionContainerService()->estConseillerConnecte();

        $aideText = $this->getProjetService()->getResultatAide($isAdmin);

        $pdf->SetFillColor(131, 49, 150); //Couleur du fond
        $pdf->SetFont(self::FONT, '', 10);
        $this->setColorTextBlanc($pdf);

        //Ajout d'une cellule avec le texte centré avec une bordure au bas
        $pdf->setCellPaddings(2, 2, '', '');
        $pdf->writeHTMLCell(
            self::WIDTH_TAB,
            15,
            '',
            '',
            $this->getTextePourHtmlCell($aideText),
            '',
            '0',
            true,
            true,
            'L'
        );
        $pdf->Ln();
        $pdf->setCellPaddings(1, 1, '', '');
    }

    /**
     * Permet de générer le message PRIS
     *
     * @param ResultatPDF $pdf
     *
     * @throws Exception
     */
    private function getMessagePRIS($pdf)
    {
        $msg = $this->getProjetService()->getResultatMessagePRIS();

        $pdf->SetFillColor(255, 255, 255); //Couleur du fond
        $pdf->SetFont(self::FONT, '', self::FONT_SIZE);
        $this->setColorTextNoir($pdf);

        //Ajout d'une cellule avec le texte centré avec une bordure au bas
        $pdf->setCellPaddings('', 2, '', 3);
        $pdf->writeHTMLCell(self::WIDTH_TAB, 0, '', '', $this->getTextePourHtmlCell($msg), '', '0', true);
        $pdf->Ln();
        $pdf->setCellPaddings('', 1, '', 1);
    }

    /**
     * Retourne les données des dispositifs eligibles
     *
     * @param ResultatPDF $pdf
     *
     * @throws Exception
     */
    private function getDispositifs($pdf)
    {
        $listDispositif = $this->getProjetService()->getListeDispositifEligible();

        $isAdmin = $this->getSessionContainerService()->estConseillerConnecte();

        $i = 1;
        /** @var DispositifResultat $dispositif */
        foreach ($listDispositif as $dispositif) {
            //entête du dispositif
            $this->getEnteteDispositif(
                $pdf,
                $i++,
                $dispositif->getIntitule(),
                $dispositif->getFinanceur(),
                $dispositif->getDescriptif(),
                Utils::formatNumberToString($dispositif->getMontant(), 2),
                $isAdmin
            );

            $this->ajouteLigneBlanche($pdf);

            //Les travaux
            $this->getEnteteTravaux($pdf, $isAdmin);
            $this->getTravauxDispositif($pdf, $dispositif->getAidesTravaux(), $isAdmin);

            $this->ajouteLigneBlanche($pdf);
        }
    }

    /**
     * Retourne l'entête d'un dispositif
     *
     * @param ResultatPDF $pdf
     * @param int         $num
     * @param string      $intitule
     * @param string      $financeur
     * @param string      $descriptif
     * @param float       $montant
     * @param bool        $isAdmin
     */
    private function getEnteteDispositif($pdf, $num, $intitule, $financeur, $descriptif, $montant, $isAdmin = false)
    {
        $wNum = 8;

        $this->addPageBreak($pdf, 100);
        //$pdf->SetFillColor(238,118,26); //Couleur du fond
        $pdf->SetFillColor(52, 124, 142); //Couleur du fond
        $pdf->SetFont(self::FONT, '', self::FONT_SIZE);

        //Définition des tailles des zones
        if ($isAdmin) {
            $wAdmin      = (self::WIDTH_TAB / 4);
            $wDispositif = $wFinanceur = (self::WIDTH_TAB - ($wAdmin + $wNum)) / 2;
        } else {
            $wDispositif = (2 * (self::WIDTH_TAB / 3)) - $wNum;
            $wFinanceur  = self::WIDTH_TAB / 3;
        }

        //Intitulé dispositif
        $this->setColorTextBlanc($pdf);
        $pdf->setCellPaddings(1, 0.5, '', '');
        $pdf->writeHTMLCell($wNum, 15, '', '', "<b>" . $num . "</b>", 0, 0, true, true, 'R');
        $textHtml = $this->getTextePourHtmlCell('- Dispositif : <b>' . $intitule . '</b>');
        $pdf->writeHTMLCell($wDispositif, 15, '', '', $textHtml, 0, 0, true, true, 'L');
        //Financeur
        $textHtml = $this->getTextePourHtmlCell('Financeur : <b>' . $financeur . '</b>');
        $pdf->writeHTMLCell($wFinanceur, 15, '', '', $textHtml, 0, intval(!$isAdmin), true, true, 'L');

        if ($isAdmin) {
            $textHtml = $this->getTextePourHtmlCell('Montant: <b>' . $montant . '€</b>');
            $pdf->writeHTMLCell($wAdmin, 15, '', '', $textHtml, 0, 1, true, true, 'R');
        }

        $this->setColorFondGris($pdf);
        $pdf->SetFont(self::FONT, '', self::FONT_SIZE);
        $this->setColorTextNoir($pdf);

        //Descriptif
        $pdf->writeHTMLCell(self::WIDTH_TAB, 0, '', '', $descriptif, 0, 0, true, true, 'J');

        $pdf->Ln();
        //Réinitialisation des valeurs
        $pdf->setCellPaddings(1, 1, '', '');
    }

    /**
     * Permet de générer le tableau de la liste des travaux
     *
     * @param ResultatPDF $pdf
     * @param array       $listTravaux
     * @param boolean     $isAdmin
     */
    private function getTravauxDispositif($pdf, $listTravaux, $isAdmin)
    {
        $pdf->SetFillColor(255, 255, 255); //Couleur du fond
        $this->setColorTextNoir($pdf);
        $this->setColorBordureGrisClair($pdf);
        if ($isAdmin) {
            $wCriteria = 2 * (self::WIDTH_TAB / 4) - 10;
            $wTravaux  = (self::WIDTH_TAB - ($wCriteria + 10)) / 2;
            $wMontant  = 2 * ($wTravaux / 3) - 1;
            $wCout     = $wMontant - 5 + 2;
        } else {
            $wTravaux  = self::WIDTH_TAB / 3;
            $wCriteria = 2 * (self::WIDTH_TAB / 3);
        }
        /** @var AideTravauxResultat $aideTravaux */
        foreach ($listTravaux as $aideTravaux) {
            $eligilibite = $aideTravaux->getEligibiliteSpecifique();
            if (Utils::formatNumberToString($aideTravaux->getMontantAideTo(), 2) === '0,00') {
                continue;
            }
            if (empty($eligilibite)) {
                $hCriteria = 15;
            } else {
                $nbCriteriaLine = 0;
                $lines          = explode('<br />', nl2br($eligilibite));
                foreach ($lines as $line) {
                    $nbCriteriaLine += ceil((strlen($line) * 2) / $wCriteria);
                }
                $hCriteria = ($nbCriteriaLine * 5) + 2;
            }
            $pdf->SetFont(self::FONT, self::FONT_STYLE, self::FONT_SIZE);
            //Intitulé travaux
            $pdf->writeHTMLCell(
                $wTravaux,
                $hCriteria,
                '',
                $pdf->GetY(),
                $this->getTextePourHtmlCell($aideTravaux->getIntituleTravaux()),
                'T',
                0,
                true,
                true,
                'L',
                true
            );
            //Critères
            $pdf->SetFont(self::FONT, '', self::FONT_SIZE);

            $pdf->writeHTMLCell(
                $wCriteria,
                $hCriteria,
                '',
                '',
                $this->getTextePourHtmlCell(nl2br($aideTravaux->getEligibiliteSpecifique())),
                'T',
                !$isAdmin,
                true,
                true,
                'L'
            );
            if ($isAdmin) {
                $pdf->writeHTMLCell(
                    $wMontant,
                    $hCriteria,
                    '',
                    '',
                    $this->getTextePourHtmlCell(
                        Utils::formatNumberToString($aideTravaux->getMontantAideTo(), 2) . ' €'
                    ),
                    'T',
                    0,
                    true,
                    true,
                    'R'
                );
                $pdf->writeHTMLCell(
                    $wCout,
                    $hCriteria,
                    '',
                    '',
                    $this->getTextePourHtmlCell(
                        Utils::formatNumberToString($aideTravaux->getCoutTravauxHtTo(), 2) . ' €'
                    ),
                    'T',
                    1,
                    true,
                    true,
                    'R'
                );

            }
        }
    }

    /**
     * Permet de générer l'entête du tableau des travaux
     *
     * @param ResultatPDF $pdf
     * @param Boolean     $isAdmin
     */
    private function getEnteteTravaux($pdf, $isAdmin)
    {
        $this->addPageBreak($pdf, 30);
        $pdf->SetFillColor(250, 224, 191); //Couleur du fond
        $pdf->SetFont(self::FONT, '', self::FONT_SIZE);
        $this->setColorTextNoir($pdf);
        $pdf->SetDrawColor(251, 251, 251); //Couleur de bordure
        $pdf->SetLineWidth(0.3); //Taille de la bordure

        if ($isAdmin) {
            $wCriteria = 2 * (self::WIDTH_TAB / 4) - 10;
            $wTravaux  = (self::WIDTH_TAB - ($wCriteria + 10)) / 2;
            $wMontant  = 2 * ($wTravaux / 3) - 1;
            $wCout     = $wMontant - 5 + 2;
        } else {
            $wTravaux  = self::WIDTH_TAB / 3;
            $wCriteria = 2 * (self::WIDTH_TAB / 3);
        }
        //Celule Travaux
        $pdf->Cell($wTravaux, 5, 'Travaux', 'R', 0, 'L', true);
        //Celule Travaux
        $pdf->Cell($wCriteria, 5, "Critères d'éligibilité spécifiques", 'L', 0, 'L', true);
        if ($isAdmin) {
            $pdf->Cell($wMontant, 5, "Montant de l'aide", 'L', 0, 'L', true);
            $pdf->Cell($wCout, 5, "Coût HT", 'L', 0, 'L', true);
        }
        $pdf->Ln();
    }

    /**
     * Permet de générer les infos du point rénovation
     *
     * @param ResultatPDF $pdf
     */
    private function getPointRenovation($pdf)
    {
        $prisNom   = $this->projet->getPrisNom();
        $prisEmail = $this->projet->getPrisEmail();
        $prisTel   = $this->projet->getPrisTel();

        $this->addPageBreak($pdf, 30);

        if (!empty($prisNom) || !empty($prisEmail) || !empty($prisTel)) {
            $this->setColorFondOrange($pdf);
            $this->setColorBordureOrange($pdf);

            //Ajout d'une cellule avec le texte
            //$this->setColorTextBlanc($pdf);
            $pdf->SetFont(self::FONT, self::FONT_STYLE);
            $pdf->setCellPaddings(5, 5, '', '');
            $w     = self::WIDTH_TAB / 3;
            $wPlus = self::WIDTH_TAB / 3 * 2;
            $pdf->SetFont(self::FONT, self::FONT_STYLE);
            $pdf->Cell($w, 50, 'Votre conseiller FAIRE', 0, 0, 'L', true,'',0,false,'T','T');

            $pdf->SetFont(self::FONT, '');
            $text = '<br><br><b>' . $prisNom . '</b><br>' . $prisEmail . '<br>' . $prisTel;
            $pdf->writeHTMLCell(
                $w ,
                50,
                $pdf->GetX(),
                $pdf->GetY(),
                $this->getTextePourHtmlCell($text),
                0,
                0,
                true,
                true,
                'C'
            );
            $image = __DIR__ . '/../../../../../public/images/ic_conseiller.png';

            $pdf->Cell($w, 50, '', 0, 0, 'C', true);
            $pdf->Image($image,$pdf->GetX()-40,$pdf->GetY()+15,30, 30);
            $pdf->Ln();
        }
    }

    /**
     * Permet de générer le commentaire conseiller
     * si il a été saisi
     *
     * @param ResultatPDF $pdf
     */
    private function getCommentaireConseiller($pdf)
    {
        $this->addPageBreak($pdf, 9);
        $commentaire = $this->projet->getAutreCommentaireConseiller();
        if (!empty($commentaire)) {
            $this->setColorFondGris($pdf);
            $this->setColorTextNoir($pdf);
            //Titre
            $pdf->SetFont(self::FONT, 'BI');
            $pdf->Cell(self::WIDTH_TAB, 0, 'Commentaire du conseiller :', 'T', 0, 'L', true);
            $pdf->Ln();
            //Intitulé
            $pdf->SetFont(self::FONT, '');
            $pdf->writeHTMLCell(self::WIDTH_TAB, 0, '', '', $commentaire, 0, 0, false, true, 'J');
            $pdf->Ln();
        }
    }

    /**
     * Permet de générer les informations de l'autre dispositif
     * si il a été saisi
     *
     * @param ResultatPDF $pdf
     */
    private function getAutreDispositif($pdf)
    {
        $this->addPageBreak($pdf, 9);
        $intitule = $this->projet->getIntituleAideConseiller();
        if (!empty($intitule)) {
            $this->setColorFondGrisClair($pdf);

            //titre du bloc
            $this->setColorTextNoir($pdf);
            $pdf->SetFont(self::FONT, self::FONT_STYLE);
            $pdf->Cell(self::WIDTH_TAB, 0, 'Autre dispositif mobilisable :', '', 0, 'L', true);
            $pdf->Ln();

            //Intitulé
            $pdf->SetFont(self::FONT, '');
            $this->setColorLabel($pdf);
            $pdf->Cell(self::WIDTH_TAB / 4, 0, 'Intitulé :', '', 0, 'R', true);
            $this->setColorValue($pdf);
            $pdf->Cell(self::WIDTH_TAB / 4, 0, $intitule, '', 0, 'L', true);
            //Montant
            $montant = Utils::formatNumberToString($this->projet->getMtAideConseiller(), 2);
            $this->setColorLabel($pdf);
            $pdf->Cell(self::WIDTH_TAB / 4, 0, 'Montant :', '', 0, 'R', true);
            $this->setColorValue($pdf);
            $pdf->SetFont(self::FONT, self::FONT_STYLE);
            $pdf->Cell(self::WIDTH_TAB / 4, 0, $montant . ' €', '', 0, 'L', true);
            $pdf->SetFont(self::FONT, '');
            $pdf->Ln();
            //Commentaire
            $commentaire = $this->projet->getDescriptifAideConseiller();
            $this->setColorLabel($pdf);
            $pdf->Cell(self::WIDTH_TAB / 4, 0, 'Commentaire :', '', 0, 'R', true);
            $this->setColorValue($pdf);
            $pdf->writeHTMLCell((self::WIDTH_TAB / 4) * 3, 0, '', '', $commentaire, 0, 0, true, true, 'J');
            $pdf->Ln();
            $pdf->Ln();
        }
    }

    /**
     * Permet de générer le texte de rappel des précautions
     *
     * @param ResultatPDF $pdf
     *
     * @throws Exception
     */
    private function getTexteRappelPrecaution($pdf)
    {
        $text = $this->getProjetService()->getTexteRappelPrecaution();
        if (!empty($text)) {
            $pdf->SetFillColor(255, 255, 255); //Couleur du fond
            $pdf->SetFont(self::FONT, '', self::FONT_SIZE);
            $this->setColorTextNoir($pdf);
            //Texte HTML
            $pdf->setCellPaddings('', 3, '', '');
            $pdf->writeHTMLCell(
                self::WIDTH_TAB,
                0,
                $pdf->GetX(),
                $pdf->GetY(),
                $this->getTextePourHtmlCell($text),
                '',
                0,
                true
            );
            $pdf->Ln();

            $pdf->setCellPaddings('', 1, '', '');
        }
    }

    /**
     * Permet de setter la couleur du texte des libellé avant valeur
     *
     * @param ResultatPDF $pdf
     */
    private function setColorLabel($pdf)
    {
        $pdf->SetTextColor(89, 130, 99);
    }

    /**
     * Permet de setter la couleur du texte des valeurs
     *
     * @param ResultatPDF $pdf
     */
    private function setColorValue($pdf)
    {
        $pdf->SetTextColor(70, 11, 83);
    }

    /**
     * Permet d'ajouter une ligne blanche
     *
     * @param ResultatPDF $pdf
     * @param int         $height
     * @param bool        $fill
     */
    private function ajouteLigneBlanche($pdf, $height = 0, $fill = false)
    {
        //Ajout d'une ligne blanche
        $pdf->Cell(self::WIDTH_TAB, $height, '', 0, 0, '', $fill);
        $pdf->Ln();
    }

    /**
     * Permet de setter la couleur du fond en gris
     *
     * @param ResultatPDF $pdf
     */
    private function setColorFondGris($pdf)
    {
        $pdf->SetFillColor(239, 237, 237); //Couleur du fond
    }

    /**
     * Permet de setter la couleur du fond en gris
     *
     * @param ResultatPDF $pdf
     */
    private function setColorFondGrisClair($pdf)
    {
        $pdf->SetFillColor(248, 248, 248); //Couleur du fond
    }

    /**
     * Permet de setter la couleur du bord en gris
     *
     * @param ResultatPDF $pdf
     */
    private function setColorBordureGrisClair($pdf)
    {
        $pdf->SetDrawColor(166, 166, 166); //Couleur de bordure
        $pdf->SetLineWidth(0.3); //Taille de la bordure
    }

    /**
     * Permet de setter la couleur du bord en orange
     *
     * @param ResultatPDF $pdf
     */
    private function setColorBordureOrange($pdf)
    {
        $colorRgbTab = Utils::hex2rgb('#F1C28B');
        $pdf->SetDrawColor($colorRgbTab[0], $colorRgbTab[1], $colorRgbTab[2]); //Couleur de bordure
        $pdf->SetLineWidth(0.3); //Taille de la bordure
    }

    /**
     * Permet de setter la couleur noir au texte
     *
     * @param ResultatPDF $pdf
     */
    private function setColorTextNoir($pdf)
    {
        $pdf->SetTextColor(0, 0, 0);
    }

    /**
     * Permet de setter la couleur blanche au texte
     *
     * @param ResultatPDF $pdf
     */
    private function setColorTextBlanc($pdf)
    {
        $pdf->SetTextColor(255, 255, 255);
    }

    /**
     * Permet de setter la couleur de fond jaune
     *
     * @param ResultatPDF $pdf
     */
    private function setColorFondJaune($pdf)
    {
        $colorFond   = $this->getSessionContainerService()->getCouleurFondTitreN1();
        $colorRgbTab = Utils::hex2rgb($colorFond); //Transformer la couleur hexa en RGB
        $pdf->SetFillColor($colorRgbTab[0], $colorRgbTab[1], $colorRgbTab[2]); //Couleur du fond
        $pdf->SetFillColor(34, 109, 182);
    }

    /**
     * Permet de setter la couleur de fond jaune
     *
     * @param ResultatPDF $pdf
     */
    private function setColorFondOrange($pdf)
    {
        $colorRgbTab = Utils::hex2rgb('#F1C28B'); //Transformer la couleur hexa en RGB
        $pdf->SetFillColor($colorRgbTab[0], $colorRgbTab[1], $colorRgbTab[2]); //Couleur du fond

    }

    /**
     * Permet de retourner le service de session
     *
     * @return array|object|SessionContainerService
     */
    private function getSessionContainerService()
    {
        if (empty($this->sessionContainerService)) {
            $this->sessionContainerService = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);
        }

        return $this->sessionContainerService;
    }

    /**
     * Permet de retourner le service de projet
     *
     * @return array|object|ProjetService
     */
    private function getProjetService()
    {
        if (empty($this->projetService)) {
            $this->projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
        }

        return $this->projetService;
    }

    /**
     * Permet de retourner le service de mail
     *
     * @return array|object|SendMailService
     */
    private function getSendMailService()
    {
        if (empty($this->sendMailService)) {
            $this->sendMailService = $this->serviceLocator->get(SendMailService::SERVICE_NAME);
        }

        return $this->sendMailService;
    }

    /**
     * Permet de retourner le service de liste de valeur
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
        }

        return $this->valeurListeService;
    }

    /**
     * Permet de retourner le nom du fichier PDF
     *
     * @param int $numProjet Numéro du projet
     *
     * @return string
     */
    private function getNomFichierPDF($numProjet)
    {
        $date       = new DateTime();
        $dateFormat = Utils::formatDateToString($date, 'dmy_hms');

        return 'Simulation_' . $numProjet . '_' . $dateFormat . '.pdf';
    }

    /**
     * Permet de retourner le nom du fichier PDF
     *
     * @param string $region Numéro du projet
     *
     * @return string
     */
    private function getNomFichierPDFDispositif($region)
    {
        $date       = new DateTime();
        $dateFormat = Utils::formatDateToString($date, 'dmY').'_'.Utils::formatDateToString($date, 'His');

        return 'Dispositifs_Région_' . str_replace([' ', "'"], ['_', '_'], $region) . '_' . $dateFormat . '.pdf';
    }

    /**
     * Permet de retourner un espace au lieu de vide
     * pour une cellule HTML
     *
     * @param $text
     *
     * @return string
     */
    private function getTextePourHtmlCell($text)
    {
        if (empty($text)) {
            $text = '&nbsp;';
        } else {
            $text = str_replace(['≠', '≤', '≥'], [' &#8800; ', ' &#8804; ', ' &#8805; '], $text);
        }

        return $text;
    }

    /**
     * Retourne le service de log
     *
     * @return array|object|Logger
     */
    private function getLogger()
    {
        if (empty($this->loggerService)) {
            $this->loggerService = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        }

        return $this->loggerService;
    }

    /**
     * Teste si on doit ajouter une nouvelle page
     *
     * @param ResultatPDF $pdf
     * @param int         $height correspond a la hauteur du block à insérer
     *
     * @return void
     */
    private function addPageBreak(ResultatPDF $pdf, $height = 0)
    {
        $this->setLineHeight($pdf->GetY() + $height);
        if ($this->getLineHeight() > $this->getMaxLineHeight()) {
            $pdf->AddPage();
            $this->setLineHeight(0);
        }
    }

    /**
     * Initialise le PDF
     * @return ResultatPDF
     */
    public function initPdf()
    {
        $pdf = new ResultatPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->setMaxLineHeight(($pdf->getPageHeight() - PDF_MARGIN_BOTTOM));
        $this->setLineHeight(PDF_MARGIN_HEADER);
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        $pdf->SetFont(self::FONT);

        return $pdf;
    }
}
