<?php
namespace Simulaides\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

/**
 * Classe LoadRegleVariableData
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Fixture
 */
class LoadRegleVariableData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        echo "load regle variable \n";

        $filePath = __DIR__ . '/../../../data/regle_variables.csv';
        if (!$manager instanceof EntityManager) {
            return;
        }
        $connection = $manager->getConnection();
        $sql        = "LOAD DATA INFILE '{$filePath}' INTO TABLE regle_variable CHARACTER SET 'UTF8'
                        FIELDS TERMINATED BY ','   ENCLOSED BY '\"' lines terminated by '\\n' ignore 1
                        lines (code_var, code_categorie, libelle, avec_liste_valeur,avec_liste_param);";
        echo $sql . "\n";
        $statement = $connection->prepare($sql);
        $statement->execute();
    }

    /**
     * Retourne les fichiers devant être exécutés avant celui-ci
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['Simulaides\Fixture\LoadRegleCategorieData'];
    }
}
