<?php
namespace Simulaides\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

/**
 * Classe LoadRegleCategorieData
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Fixture
 */
class LoadRegleCategorieData implements FixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        echo "load regle catégorie \n";

        $filePath = __DIR__ . '/../../../data/regle_categorie.csv';
        if (!$manager instanceof EntityManager) {
            return;
        }
        $connection = $manager->getConnection();
        $sql        = "LOAD DATA INFILE '{$filePath}' INTO TABLE regle_categorie CHARACTER SET 'UTF8'
                        FIELDS TERMINATED BY ','   ENCLOSED BY '\"' lines terminated by '\\n' ignore 1
                        lines (code, libelle,type,numero_ordre);";
        echo $sql . "\n";
        $statement = $connection->prepare($sql);
        $statement->execute();
    }
}
