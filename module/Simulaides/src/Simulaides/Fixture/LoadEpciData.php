<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 12/02/15
 * Time: 15:06
 */

namespace Simulaides\Fixture;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Zend\Di\ServiceLocator;

/**
 * Classe LoadEpciData
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Fixture
 */
class LoadEpciData implements FixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        echo "load Epci \n";

        $filePath = __DIR__ . '/../../../data/epci.csv';
        if (!$manager instanceof EntityManager) {
            return;
        }
        $connection = $manager->getConnection();
        $sql        = "LOAD DATA INFILE '{$filePath}' INTO TABLE epci CHARACTER SET 'UTF8'  FIELDS TERMINATED BY ','
                    ENCLOSED BY '\"' lines terminated by '\\r\\n' ignore 1 lines (code, nom);";
        echo $sql . "\n";
        $statement = $connection->prepare($sql);
        $statement->execute();
    }
}
