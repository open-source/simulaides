<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 09:22
 */

namespace Simulaides\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

/**
 * Class LoadTravauxData
 * @package Simulaides\Fixture
 */
class LoadTravauxData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $filePath = __DIR__ . '/../../../data/liste_travaux.csv';
        if (file_exists($filePath)) {
            echo "load Travaux \n";

            if (!$manager instanceof EntityManager) {
                return;
            }
            $connection = $manager->getConnection();
            $sql        = "LOAD DATA INFILE '{$filePath}' INTO TABLE travaux CHARACTER SET 'UTF8'
                    FIELDS TERMINATED BY ','   ENCLOSED BY '\"' lines terminated by '\\r\\n' ignore 1
                    lines (code, intitule, categorie, maison, appartement);";
            echo $sql . "\n";
            $statement = $connection->prepare($sql);
            $statement->execute();
        } else {
            echo "file not found";
        }
    }

    /**
     * Retourne les fichiers devant être exécutés avant celui-ci
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['Simulaides\Fixture\LoadValeurListeData']; // fixture classes fixture is dependent on
    }
}
