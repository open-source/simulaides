<?php
namespace Simulaides\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

/**
 * Classe LoadCeeData
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Fixture
 */
class LoadCeeData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        echo "load données CEE \n";

        $filePath = __DIR__ . '/../../../data/s3_import_donnees_CEE.sql';
        if (!$manager instanceof EntityManager) {
            return;
        }
        $connection = $manager->getConnection();
        $data       = file_get_contents($filePath);
        $connection->executeQuery($data);
        echo "load données CEE ok \n";
    }

    /**
     * Retourne les fichiers devant être exécutés avant celui-ci
     * @return array
     */
    public function getDependencies()
    {
        return [
            'Simulaides\Fixture\LoadTravauxData',
            'Simulaides\Fixture\LoadValeurListeData'
        ]; // fixture classes fixture is dependent on
    }
}
