<?php
namespace Simulaides\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

/**
 * Classe LoadDispositifData
 * Permet d'importer les dispositifs
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Fixture
 */
class LoadDispositifData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        echo "load dispositif liste \n";

        $filePath = __DIR__ . '/../../../data/s4_import_dispositifs_HNO.sql';
        if (!$manager instanceof EntityManager) {
            return;
        }
        $connection = $manager->getConnection();
        $data       = file_get_contents($filePath);
        $connection->executeQuery($data);
        echo "load dispositif liste ok \n";
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['Simulaides\Fixture\LoadValeurListeData', 'Simulaides\Fixture\LoadCommuneData',
            'Simulaides\Fixture\LoadEpciData', 'Simulaides\Fixture\LoadTravauxData'];
    }
}
