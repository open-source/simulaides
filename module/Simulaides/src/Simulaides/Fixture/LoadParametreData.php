<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 16:28
 */

namespace Simulaides\Fixture;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

/**
 * Class LoadParametreData
 * @package Simulaides\Fixture
 */
class LoadParametreData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['Simulaides\Fixture\LoadProduitsData', 'Simulaides\Fixture\LoadValeurListeData'];
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $filePath = __DIR__ . '/../../../data/liste_parametre_produit.csv';
        if (file_exists($filePath)) {
            echo "load Parametre Produits \n";

            if (!$manager instanceof EntityManager) {
                return;
            }
            $connection = $manager->getConnection();
            $sql        = "LOAD DATA INFILE '{$filePath}' INTO TABLE parametre_tech CHARACTER SET 'UTF8'  FIELDS
            TERMINATED BY ','   ENCLOSED BY '\"' lines terminated by '\\r\\n' ignore 1 lines (code_produit, code,
            libelle, type_data, code_liste, utilisable_regle, maison, appartement);";
            echo $sql . "\n";
            $statement = $connection->prepare($sql);
            $statement->execute();
        } else {
            echo "file not found $filePath \n";
        }
    }
}
