<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 12/02/15
 * Time: 18:46
 */

namespace Simulaides\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Zend\Di\ServiceLocator;

/**
 * Classe LoadEpciCommuneData
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Fixture
 */
class LoadEpciCommuneData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        echo "load Epci Commune \n";

        $filePath = __DIR__ . '/../../../data/epci_commune.csv';
        if (!$manager instanceof EntityManager) {
            return;
        }
        $connection = $manager->getConnection();
        $sql        = "LOAD DATA INFILE '{$filePath}' INTO TABLE epci_commune CHARACTER SET 'UTF8'  FIELDS
           TERMINATED BY ','   ENCLOSED BY '\"' lines terminated by '\\r\\n' ignore 1 lines (code_commune, code_epci);";
        echo $sql . "\n";
        $statement = $connection->prepare($sql);
        $statement->execute();
    }

    /**
     * Retourne les fichiers devant être exécutés avant celui-ci
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['Simulaides\Fixture\LoadEpciData', 'Simulaides\Fixture\LoadCommuneData'];
    }
}
