<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 15:17
 */

namespace Simulaides\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

/**
 * Class LoadProduitsData
 * @package Simulaides\Fixture
 */
class LoadProduitsData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['Simulaides\Fixture\LoadTravauxData'];
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $filePath = __DIR__ . '/../../../data/liste_produits.csv';
        if (file_exists($filePath)) {
            echo "load Produits \n";

            if (!$manager instanceof EntityManager) {
                return;
            }
            $connection = $manager->getConnection();
            $sql        = "LOAD DATA INFILE '{$filePath}' INTO TABLE produit CHARACTER SET 'UTF8'
                            FIELDS TERMINATED BY ','   ENCLOSED BY '\"' lines terminated by '\\r\\n' ignore 1
                            lines (code_travaux, code, intitule, regle_nbu, intitule_long, cote, unite, prix_u_total,
                            prix_u_mo, prix_u_fourniture, tva, infos_saisie_couts);";
            echo $sql . "\n";
            $statement = $connection->prepare($sql);
            $statement->execute();
        } else {
            echo "file not found";
        }
    }
}
