<?php
namespace Simulaides\Logger;

/**
 * Classe LoggerSimulation
 * Gestion des log de la simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\LoggerSimulation
 */
class LoggerSimulation
{
    /**
     * Fichier du détail de la simulation
     *
     * @var
     */
    public static $ficDetail;

    /**
     * Nom et chemin du fichier
     * @var string
     */
    private static $fileName;

    /**
     * Initialisation du fichier
     *
     * @param int $numProjet
     */
    public static function init($numProjet)
    {
        self::$fileName  = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $numProjet . ".log";
        self::$ficDetail = fopen(self::$fileName, "w");
    }

    /**
     * Ajoute du détail dans le fichier de simulation
     *
     * @param $texte
     */
    public static function detail($texte)
    {
        fwrite(self::$ficDetail, $texte);
    }

    /**
     * Retourne le fichier de log
     *
     * @return mixed
     */
    public static function getLogFile()
    {
        return self::$fileName;
    }

    /**
     * Supprime le fichier de log
     */
    public static function deleteLogFile()
    {
        if (file_exists(self::$fileName)) {
            unlink(self::$fileName);
        }
    }
}
