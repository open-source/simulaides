<?php
namespace Simulaides\EventListener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Http\Header\Referer;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Uri\Http;

/**
 * Classe ResponseEventListener
 *
 * Projet : Instm Ent 2013-2015

 *
 * @copyright Copyright © Institut Mines Télécom 2013-2015, All Rights Reserved
 * @author
 * @package   Simulaides\EventListener
 */
class ResponseEventListener implements ListenerAggregateInterface
{

    /**
     * @var ListenerAggregateInterface
     */
    private $listener;

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listener = $events->attach(MvcEvent::EVENT_FINISH, [$this, 'AddResponseHeader'], -9000);
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        $events->detach($this->listener);
    }

    /**
     * @param MvcEvent $event
     */
    public function addResponseHeader(MvcEvent $event)
    {
        /** @var Request $request */
        $request = $event->getRequest();
        if ($request instanceof Request) {
            $uri = $request->getUri();
            /** @var Referer $referer */
            $referer = $request->getHeader('referer');
            if ($referer !== false) {
                /** @var Http $refererUri */
                $refererUri = $referer->uri();
                $config = $event->getApplication()->getServiceManager()->get('ApplicationConfig');
                $authorizeParent = $config['iframe-parent-autorisee'];
                $frontControllerList = ['front-home', 'front', 'front-session'];
                $controller = $event->getController();
                if (in_array($controller, $frontControllerList)) {
                    /** @var Response $response */
                    $response = $event->getResponse();
                    $headers = $response->getHeaders();
                    if ($uri->getHost() !== $refererUri->getHost()) {
                        $refererUri = $refererUri->getScheme() . '://' . $refererUri->getHost();
                        if (array_key_exists($refererUri, $authorizeParent)) {
                            $headers->addHeaderLine('X-Frame-Options', $refererUri);
                        } else {
                            $headers->addHeaderLine('X-Frame-Options', 'DENY');
                        }
                    } else {
                        $refererUri = $refererUri->getScheme() . '://' . $refererUri->getHost();
                        $headers->addHeaderLine('X-Frame-Options', $refererUri);
                    }
                    $response->setHeaders($headers);
                    $event->setResponse($response);
                }
            }
        }
    }
}