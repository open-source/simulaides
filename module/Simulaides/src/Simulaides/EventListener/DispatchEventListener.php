<?php
namespace Simulaides\EventListener;

use ServicesWeb\Controller\IWsController;
use Simulaides\Controller\Back\IBackController;
use Simulaides\Controller\Front\IFrontController;
use Simulaides\Controller\Front\AccueilController;
use Simulaides\Service\PartenaireService;
use Simulaides\Tools\Utils;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;

/**
 * Classe DispatchEventListener
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\EventListener
 */
class DispatchEventListener implements ListenerAggregateInterface
{
    /**
     * Nom du listener pour la définition du layout
     */
    const LISTENER_LAYOUT = 'layout-listener';

    /**
     * Nom du listener pour la vérification de l'iFrame
     */
    const LISTENER_IFRAME = 'iframe-listener';

    /**
     * Nom du listener pour la vérification de la maintenance
     */
    const LISTENER_MAINTENANCE = 'iframe-maintenance';

    /**
     * @var array
     */
    private $listeners = [];

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[self::LISTENER_LAYOUT] = $events->attach(
            MvcEvent::EVENT_DISPATCH,
            [$this, 'definirLayout']
        );
        $this->listeners[self::LISTENER_IFRAME] = $events->attach(
            MvcEvent::EVENT_DISPATCH,
            [$this, 'verifieIFrame']
        );
        $this->listeners[self::LISTENER_MAINTENANCE] = $events->attach(
            MvcEvent::EVENT_DISPATCH,
            [$this, 'verifieMaintenance']
        );
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     * @param null                  $nomListener
     */
    public function detach(EventManagerInterface $events, $nomListener = null)
    {
        if (!empty($nomListener)) {
            $events->detach($this->listeners[$nomListener]);
            unset($this->listeners[$nomListener]);
        }
    }

    /**
     * Permet de définir le layout à appliquer
     *
     * @param MvcEvent $e
     */
    public function definirLayout(MvcEvent $e)
    {

        $parentRoute = $this->getParentRoute($e);
        $controller  = $e->getTarget();
        $e->getViewModel();

        if ($controller instanceof IBackController) {
            $template = 'layout/back';
        } elseif ($controller instanceof IFrontController) {
            if ($parentRoute == 'admin') {
                //Cas des écrans de simulations dédiés au Front mais utilisé dans le back pour les conseillers
                $template = 'layout/back';
            } else {
                $template = 'layout';
                if($controller instanceof AccueilController){
                    $template = 'layout/accueil';
                }
            }
        } elseif ($controller instanceof IWsController) {
            $template = 'layout/ws';
        }

        if (isset($template)) {
            $controller->layout()->setTemplate($template);
        }
    }

    /**
     * Permet de vérifier l'appel iFrame
     *
     * @param MvcEvent $e
     */
    public function verifieIFrame(MvcEvent $e)
    {
        $parentRoute = $this->getParentRoute($e);
        $controller  = $e->getTarget();
        $serverName  = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';
        if ($controller instanceof IFrontController) {
            if ($parentRoute == 'front-home' || $parentRoute == 'front-session') {
                if (isset($_SERVER['HTTP_REFERER'])) {
                    $conteneurParent = $_SERVER['HTTP_REFERER'];
                    $serveurParent   = Utils::getServerNameConteneur($conteneurParent);
                    if ($serverName != $serveurParent) {
                        $cle = $e->getRouteMatch()->getParam('cle');
                        if (!$this->verifieListeBlanche($conteneurParent, $controller, $cle)) {
                            //L'appel est fait dans une iFrame,
                            //mais le parent n'est pas dans la liste des sites autorisés
                            $controller->layout()->setTemplate('error/403');
                        }
                    }
                } else {
                    //Le parent n'est pas renseigné, l'appel n'est pas fait dans une iFrame => rejet 403
                    $controller->layout()->setTemplate('error/403');
                }
            }
        }
    }

    /**
     * Permet de vérifier si le parent indiqué est autorisé à appeler l'iFrame
     *
     * @param                  $conteneurParent
     * @param IFrontController $controller
     * @param string           $cle
     *
     * @return bool
     */
    private function verifieListeBlanche($conteneurParent, $controller, $cle)
    {
        $conteneurParent = Utils::getConteneurParentFromPageAppelante($conteneurParent);

        /** @var PartenaireService $partenaireService */
        $partenaireService = $controller->getServiceLocator()->get(PartenaireService::SERVICE_NAME);
        $partenaire = $partenaireService->getPartenaireByUrl($conteneurParent);

        return ($partenaire && ($partenaire->getCle()==$cle));
    }

    /**
     * Retourne la route parente
     *
     * @param MvcEvent $e
     *
     * @return mixed
     */
    private function getParentRoute(MvcEvent $e)
    {
        $routeName   = $e->getRouteMatch()->getMatchedRouteName();
        $tabPart     = explode('/', $routeName);
        $parentRoute = $tabPart[0];

        return $parentRoute;
    }

    /**
     * Permet de vérifier l'état de maintenance
     *
     * @param MvcEvent $e
     */
    public function verifieMaintenance(MvcEvent $e)
    {
        $controller  = $e->getTarget();
        $tabConfig       = $controller->getServiceLocator()->get('ApplicationConfig');
        $etatMaintenance = isset($tabConfig['under-maintenance'])? $tabConfig['under-maintenance']: false;

        if($etatMaintenance === true){
            $controller->layout()->setTemplate('error/503');
        }
    }
}
