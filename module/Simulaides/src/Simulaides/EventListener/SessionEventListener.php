<?php
namespace Simulaides\EventListener;

use Simulaides\Service\ProjetSessionContainer;
use Simulaides\Service\SessionContainerService;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;

/**
 * Classe SessionEventListener
 * Gère un listener pour la vérification de la session
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\EventListener
 */
class SessionEventListener implements ListenerAggregateInterface
{
    /**
     * @var array
     */
    private $listeners = [];

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'verifieSession'));
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        // TODO: Implement detach() method.
    }

    /**
     * Permet de faire les vérifications de session
     *
     * @param MvcEvent $e
     *
     * @return Response
     */
    public function verifieSession(MvcEvent $e)
    {
        $routeName = $e->getRouteMatch()->getMatchedRouteName();
        $tabPart   = explode('/', $routeName);
        $params    = $e->getRouteMatch()->getParams();
        $isCheckCguAction = in_array('check-cgu', $params);

        $parentRoute = $tabPart[0];
        //On ne fait les vérifications que si la route parente est 'admin' ou 'front'
        //Sinon, c'est qu'il s'agit des routes de déconnexion ou de home
        $verifie = ($parentRoute == 'admin' || $parentRoute == 'front');

        $routeRedirect = '';
        if ($verifie && !$isCheckCguAction) {
            if ($parentRoute == 'admin' && isset($tabPart[1])) {
                //Vérification que l'utilisateur est connecté
                /** @var SessionContainerService $sessionService */
                $sessionService = $e->getApplication()->getServiceManager()->get(SessionContainerService::SERVICE_NAME);
                if (!$sessionService->estConseillerConnecte()) {
                    //Le conseiller n'est pas connecté => retour à la page de connexion
                    $routeRedirect = '/admin';
                }
            }
            if (isset($tabPart[1]) && $tabPart[1] == 'simulation' && !(isset($params['action']) && ($params['action'] == 'disable-js' || $params['action'] == 'enable-js'))) {
                /** @var ProjetSessionContainer $projetSessionService */
                $projetSessionService = $e->getApplication()->getServiceManager()->get(
                    ProjetSessionContainer::SERVICE_NAME
                );
                //Vérification qu'un projet est en session
                $projetSession = $projetSessionService->getProjet();
                if (empty($projetSession)) {
                    //Le projet n'est plus en session => retour à la page d'accueil
                    $routeRedirect = $this->getRoutePerteSession($parentRoute);
                }

            }
            if (!empty($routeRedirect)) {
                /** @var Response $response */
                $response = $e->getResponse();
                /** @var Request $request */
                $request = $e->getRequest();
                if ($request->isXmlHttpRequest()) {
                    $response->setStatusCode(Response::STATUS_CODE_405);
                    $response->setContent($routeRedirect);
                } else {
                    $response->getHeaders()->addHeaderLine('Location', $routeRedirect);
                    $response->setStatusCode(Response::STATUS_CODE_302);
                }
                return $response;
            }
        }
    }

    /**
     * Permet de retourner les informations de la route de reconnexion
     * en cas de perte de session
     *
     * @param string $parentRoute Route parente
     *
     * @return string
     */
    private function getRoutePerteSession($parentRoute)
    {
        if ($parentRoute == 'front') {
            $routeSession = '/front-session/1';
        } else {
            $routeSession = '/admin-session';
        }

        return $routeSession;
    }
}
