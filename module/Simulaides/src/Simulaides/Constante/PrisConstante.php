<?php
namespace Simulaides\Constante;

/**
 * Classe PrisConstante
 * Constantes des données venant de PRIS
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class PrisConstante
{

    /*
     * Liste des roles des Adémiens
     */

    /** Role ADEME */
    const ROLE_ADEME = 'ADEME';

    /** Role DR ADEME */
    const ROLE_ADEME_DR = 'DR ADEME';

    /**
     * Role des Conseillers pris
     */
    const ROLE_PRIS = 'PRIS';

    /* Role Obligé */
    const ROLE_OBLIGE = 'Obligé';

}
