<?php
namespace Simulaides\Constante;

/**
 * Classe TravauxConstante
 * Classe de constante sur les travaux
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class TravauxConstante
{
    /** Changement de fenêtre */
    const TRAVAUX_T01_CHGE_FENETRE = 'T01';
    /** Isolation des murs par l'intérieur */
    const TRAVAUX_T02_ISOL_MUR_INT = 'T02';
    /** Isolation des murs par l'extérieur */
    const TRAVAUX_T03_ISOL_MUR_EXT = 'T03';
    /** Isolation des combles */
    const TRAVAUX_T04_ISOL_COMBLE = 'T04';
    /** Isolation des toitures par l'intérieur */
    const TRAVAUX_T05_ISOL_TOIT_INT = 'T05';
    /** Isolation des toitures par l'extérieur */
    const TRAVAUX_T06_ISOL_TOIT_EXT = 'T06';
    /** Isolation des toitures terrase */
    const TRAVAUX_T07_ISOL_TOIT_TERRASSE = 'T07';
    /** Isolation du plancher bas */
    const TRAVAUX_T08_ISOL_PLANCHER_BAS = 'T08';
    /** Fermeture isolante */
    const TRAVAUX_T09_FERMET_ISOLANTE = 'T09';
    /** Chaudière gaz à condensation */
    const TRAVAUX_T11_CHAUDIERE_GAZ_COND = 'T11';
    /** Chaudière basse température */
    const TRAVAUX_T12_CHAUDIERE_BASSE_TEMP = 'T12';
    /** Equipement de régulation */
    const TRAVAUX_T13_EQUIP_REGULATION = 'T13';
    /** VMC Simple */
    const TRAVAUX_T14_VMC_SIMPLE = 'T14';
    /** VMC Double */
    const TRAVAUX_T15_VMC_DOUBLE = 'T15';
    /** Radiateur chaleur douce */
    const TRAVAUX_T18_RAD_DOUCE = 'T18';
    /** Plancher chauffant */
    const TRAVAUX_T19_PLANCHER_CHAUFF = 'T19';
    /** Poele */
    const TRAVAUX_T20_POELE = 'T20';
    /** Insert */
    const TRAVAUX_T45_INSERT = 'T45';
    /** Cheminée foyer fermé */
    const TRAVAUX_T46_CHEMINEE = 'T46';
    /** Chaudière biomasse */
    const TRAVAUX_T21_CHAUDIERE_BIOMASSE = 'T21';
    /** Pompe à chaleur Air/Air */
    const TRAVAUX_T22_PAC_AIR_AIR = 'T22';
    /** Pompe à chaleur Air/Eau */
    const TRAVAUX_T23_PAC_AIR_EAU = 'T23';
    /** Pompe à chaleur Eau/Eau */
    const TRAVAUX_T24_PAC_EAU_EAU = 'T24';
    /** Système solaire combiné */
    const TRAVAUX_T25_SYST_SOLAIRE = 'T25';
    /** chauffe Eau thermodynamique */
    const TRAVAUX_T26_CE_THERMODYN = 'T26';
    /** chauffe Eau Solaire */
    const TRAVAUX_T27_CE_SOLAIRE = 'T27';
    /** chauffe Eau Solaire */
    const TRAVAUX_T43_RAC_RES_CHA = 'T43';
    /** Panneaux solaires photovoltaïques */
    const TRAVAUX_T28_PAN_SOL_PHOTO = 'T28';


    /** class font awesome */
    const TRAVAUX_FA_ETUDES = 'fa fa-file-text';

    /** class font awesome */
    const TRAVAUX_FA_ISOLATION = 'fa fa-home';

    /** class font awesome */
    const TRAVAUX_FA_EQUIPEMENT = 'fa fa-fire';

    /** class font awesome */
    const TRAVAUX_FA_RENOUVELABLE = 'fa fa-sun-o';

}
