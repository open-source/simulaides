<?php
namespace Simulaides\Constante;

/**
 * Classe MainConstante
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class MainConstante
{
    /** chemin des entités */
    const ENTITY_PATH = 'Simulaides\\Domain\\Entity\\';

    /** Format des dates */
    const FORMAT_DATE_DEFAUT = 'd/m/Y';

    /** Format du séparateur de décimal */
    const DECIMAL_SEPARATOR = ',';

    /** Format du séparateur de millier */
    const THOUSAND_SEPARATOR = '';

    /** Nom de la zone de saisie début de période */
    const NAME_DATE_DEBUT_PERIODE = 'debutPeriode';

    /** Nom de la zone de saisie fin de période */
    const NAME_DATE_FIN_PERIODE = 'finPeriode';

    /** Etape de la simulation */

    /** Etape 1 : Mes caractéristiques */
    const SIMUL_ETAPE_CARACTERISTIQUE = '1';

    /** Etape 2 : Mon projet */
    const SIMUL_ETAPE_PROJET = '2';

    /** Etape : Mes travaux */
    const SIMUL_ETAPE_MES_TRAVAUX = '3';

    /** Etape : Mes Offres */
    const SIMUL_ETAPE_MES_OFFRES= '3-1';

    /** Etape 3 : Résultat */
    const SIMUL_ETAPE_RESULTAT = '4';

    /**
     * Caractère BOM
     */
    const CARACTERE_BOM = "\xEF\xBB\xBF";

    /** Format de date logger */
    const FORMAT_COMPLETE_DATE_MYSQL = 'Y-m-d H:i:s';

    /**
     * Département Mayotte
     */
    const DEPT_MAYOTTE = '976';

    /**
     * Département Reunion
     */
    const DEPT_REUNION = '974';

    /**
     * Département Martinique
     */
    const DEPT_MARTINIQUE = '972';

    /**
     * Département Guadeloupe
     */
    const DEPT_GUADELOUPE= '971';

    public static $LISTE_DOM =[
        971,972,973,974,976
    ];

    public static $LISTE_IDF =[
        75,77,78,91,92,93,94,95
    ];

    public static $LISTE_REUNION =[
        974
    ];
}
