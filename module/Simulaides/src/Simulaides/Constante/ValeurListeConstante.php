<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 16/02/15
 * Time: 15:48
 */

namespace Simulaides\Constante;

/**
 * Class ValeurListeConstante
 * @package Simulaides\Constante
 */
class ValeurListeConstante
{

    /**
     *  Code de la liste type de logement
     */
    const LISTE_TYPE_LOGEMENT = "L_TYPE_LOGEMENT";
    /** Logement de type maison */
    const TYPE_LOGEMENT_CODE_MAISON = "M";
    /** Logement de type appartement */
    const TYPE_LOGEMENT_CODE_APPART = "A";

    /**
     *  Code de la liste type de situation maritale
     */
    const LISTE_SITUATION = "L_SITUATION";
    /** Célibataire ou divorcé avec concubinage */
    const CELIB_CONCUBINAGE = 'CELIB_CONCUBINAGE';
    /** Célibataire ou divorcé ayant élevé au moins un enfant */
    const CELIB_ENFANT = 'CELIB_CONCUBINAGE';
    /** Célibataire ou divorcé sans concubinage */
    const CELIB_SEUL = 'CELIB_CONCUBINAGE';
    /** Couple marié ou pacsé */
    const COUPLE = 'CELIB_CONCUBINAGE';
    /** Veuf ou veuve */
    const VEUF = 'CELIB_CONCUBINAGE';

    /**
     *  Code de la liste énergie de chauffage
     */
    const LISTE_ENERGIE_CHAUFFAGE = "L_ENERGIE_CHAUFFAGE";
    /**
     *  Code de la liste énergie de chauffage principale
     */
    const LISTE_ENERGIE_CHAUFFAGE_DETAIL = "L_ENERGIE_CHAUFFAGE_DETAIL";

    /**
     *  Code de la liste des types de combustible
     */
    const LISTE_TYPE_COMBUSTIBLE = "L_TYPE_COMBUSTIBLE ";

    /**
     *  Code de la liste statut du demandeur
     */
    const LISTE_STATUT = "L_STATUT_DEMANDEUR";

    /**
     * Texte de la page d'accueil
     */
    const TEXTE_ACCEUIL = 'TEXTE_ACCUEIL';

    /**
     * Deuxième Texte de la page d'accueil
     */
    const TEXTE_ACCEUIL_2 = 'TEXTE_ACCUEIL_2';

    /**
     * Couleur du deuxième Texte de la page d'accueil
     */
    const COULEUR_TEXTE_ACCEUIL_2 = 'COULEUR_TEXTE_ACCUEIL_2';

    /**
     * Troisième Texte de la page d'accueil
     */
    const TEXTE_ACCEUIL_3 = 'TEXTE_ACCUEIL_3';

    /**
     * Quatrième Texte de la page d'accueil
     */
    const TEXTE_ACCUEIL_PREREQUIS = 'TEXTE_ACCUEIL_PREREQUIS';

    /**
     * Texte du chapô de la page d'accueil
     */
    const TEXTE_ACCUEIL_CHAPO_HOME = 'TEXTE_ACCUEIL_CHAPO_HOME';

    /**
     * Texte d'aide pour la simulation
     */
    const TEXTE_AIDE_MA_SITUATION = 'TEXTE_AIDE_MA_SITUATION';

    /**
     * Texte d'aide au choix des travaux
     */
    const TEXTE_AIDE_SELECTION_TRAVAUX = 'TEXTE_AIDE_SELECTION_TRAVAUX';
    /**
     * Texte d'aide à la saisie du travaux
     */
    const TEXTE_AIDE_SAISIE_TRAVAUX = 'TEXTE_AIDE_SAISIE_TRAVAUX';
    /**
     * Texte d'aide à la saisie de l'étude
     */
    const TEXTE_AIDE_SAISIE_TRAVAUX_ETUDE = 'TEXTE_AIDE_SAISIE_TRAVAUX_ETUDE';

    /**
     * Texte d'aide à la saisie des travaux de type changement de fenêtre
     */
    const TEXTE_AIDE_SAISIE_TRAVAUX_FENETRE = 'TEXTE_AIDE_SAISIE_TRAVAUX_FENETRE';

    /**
     * Texte d'aide aux résultats de la simulation
     */
    const TEXTE_AIDE_RESULTATS_SIMULATION = 'TEXTE_AIDE_RESULTATS_SIMULATION';
    /**
     * Texte d'aide à la compréhension du pourcentage d'aide
     */
    const TEXTE_AIDE_COMPREHENSION_POURCENTAGE = 'TEXTE_AIDE_COMPREHENSION_POURCENTAGE';

    /**
     * Texte PRIS en cas d'élligibilité des dispositifs
     */
    const DISPOSITIFS_ELIGIBLES_DETECTES = 'DISPOSITIFS_ELIGIBLES_DETECTES';

    /**
     * Texte PRIS en cas d'aucun dispositifi elligible
     */
    const AUCUN_DISPOSITIF_ELIGIBLE = 'AUCUN_DISPOSITIF_ELIGIBLE';

    /**
     *  Texte document utiles
     */
    const DOC_UTILE = 'DOCUMENT_UTILES';

    /**
     * Texte document eligible trouvé
     */
    const DISPOSITIF_ELIGIBLE = 'DISPOSITIFS_ELIGIBLES_DETECTES';

    /**
     * Texte de rappel des précautions
     */
    const RAPPEL_PRECAUTIONS = 'RAPPEL_PRECAUTIONS';

    /**
     * Texte administrable
     */
    const TEXT_ADMINISTRABLE = 'L_TEXTE_ADMINISTRABLE';

    /**
     * Texte du corps du mail
     */
    const CORPS_MAIL_PDF = 'CORPS_MAIL_PDF';

    /**
     * Code de la liste numérique administrable
     */
    const LISTE_NUM_ADMINISTRABLE = 'L_NUMERIQUE_ADMINISTRABLE';

    /** Valeur du coeff de conversion en kwk => euro*/
    const CONVERSION_KWH_EUROS = 'CONVERSION_KWH_EUROS';

    /** CATEGORIE DE TRAVAUX */
    /** Etude */
    const CAT_TRAVAUX_ETUDE = 'ETUDE';
    /** Isolation */
    const CAT_TRAVAUX_ISOLATION = 'ISOLATION';
    /** Equipement */
    const CAT_TRAVAUX_EQUIPEMENT = 'EQUIPEMENT';
    /** Energie renouvelable */
    const CAT_TRAVAUX_ENERGIES_RENOUVELABLES = 'ENERGIES_RENOUVELABLES';
    /** ELECTROMENAGER */
    const CAT_TRAVAUX_ELECTROMENAGER = 'ELECTROMENAGER';


    /** ETAT DU DISPOSITIF */
    const L_ETAT_DISPOSITIF = 'L_ETAT_DISPOSITIF';
    /** Désactivé */
    const ETAT_DISPOSITIF_DESACTIVE = 'DESACTIVE';
    /** A valider */
    const ETAT_DISPOSITIF_AVALIDER = 'A_VALIDER';
    /** Activé */
    const ETAT_DISPOSITIF_ACTIVE = 'ACTIVE';

    /** TYPE DE DISPOSITIF */
    const L_TYPE_DISPOSITIF = 'L_TYPE_DISPOSITIF';
    /** Public */
    const TYPE_DISPOSITIF_PUBLIC = 'PUBLIC';
    /** CEE */
    const TYPE_DISPOSITIF_CEE = 'CEE';
    /** National */
    const TYPE_DISPOSITIF_NATIONAL = 'NATIONAL';
    /** Coup de pouce */
    const TYPE_DISPOSITIF_COUP_DE_POUCE = 'COUP_DE_POUCE';

    /** TYPE DE PERIMETRE DU DISPOSITIF */
    /** National */
    const TYPE_PERIM_GEO_NATIONAL = 'N';
    /** Régional */
    const TYPE_PERIM_GEO_REGIONAL = 'R';
    /** Départemental */
    const TYPE_PERIM_GEO_DEPARTEMENTAL = 'D';
    /** Intercommunalité */
    const TYPE_PERIM_GEO_EPCI = 'E';
    /** Communal */
    const TYPE_PERIM_GEO_COMMUNAL = 'C';

    /** TYPE DE REGLE DE DISPOSITIF */
    /** Eligibilité */
    const TYPE_REGLE_DISPOSITIF_ELIGIBILITE = 'E';
    /** Montant */
    const TYPE_REGLE_DISPOSITIF_MONTANT = 'M';
    /** Plafond */
    const TYPE_REGLE_DISPOSITIF_PLAFOND = 'P';

    /** CATEGORIE DE DISPOSITIF */
    /** Foyer */
    const DISPOSITIF_CATEG_FOYER = 'foyer';
    /** Logement */
    const DISPOSITIF_CATEG_LOGEMENT = 'logement';
    /** Cout des travaux */
    const DISPOSITIF_CATEG_COUT_TRAVAUX = 'cout_travaux';
    /** Montant des aides */
    const DISPOSITIF_CATEG_MONTANT_AIDE = 'montant_aide';

    /** TYPE DE REGLE DE TRAVAUX */
    /** Montant total */
    const TYPE_REGLE_TRAVAUX_MT_TOTAL = 'MT';
    /** Montant main d'oeuvre */
    const TYPE_REGLE_TRAVAUX_MT_MO = 'MO';
    /** Montant Fourniture */
    const TYPE_REGLE_TRAVAUX_MT_FOURNITURE = 'MF';
    /** Plafond */
    const TYPE_REGLE_TRAVAUX_PLAFOND = 'P';


    /**
     * Energie parente : électrique
     */
    const ENERG_PARENT_ELECTRIQUE = 'ELECTRIQUE';
    /**
     * Energie parente : combustible
     */
    const ENERG_PARENT_COMBUSTIBLE = 'COMBUSTIBLE';

    /**
     * Energie principale : fioul
     */
    const ENERG_FIOUL = 'FIOUL';
    /**
     * Energie principale : réseau de chaleur
     */
    const ENERG_RESEAU = 'RESEAU';
    /**
     * Energie principale : charbon
     */
    const ENERG_CHARBON = 'CHARBON';
    /**
     * Energie principale : solaire
     */
    const ENERG_SOLAIRE = 'SOLAIRE';
    /**
     * Energie principale : gaz de ville
     */
    const ENERG_GAZ_VILLE = 'GAZ_VILLE';
    /**
     * Energie principale : géothermie
     */
    const ENERG_GEOTHERMIE = 'GEOTHERMIE';
    /**
     * Energie principale : bûches
     */
    const ENERG_BOIS_BUCHES = 'BOIS_BUCHES';
    /**
     * Energie principale : électricité
     */
    const ENERG_ELECTRICITE = 'ELECTRICITE';
    /**
     * Energie principale : gaz en citerne
     */
    const ENERG_GAZ_CITERNE = 'GAZ_CITERNE';
    /**
     * Energie principale : granulé
     */
    const ENERG_BOIS_GRANULES = 'BOIS_GRANULES';
    /**
     * Energie principale : Gaz en bouteille
     */
    const ENERG_GAZ_BOUTEILLE = 'GAZ_BOUTEILLE';

    /**
     * Texte administrable pour MaPrimeRenov - très modeste
     */
    const TEXTE_POPUP_MAPRIMRENOV_TRESMODESTE = 'TEXTE_POPUP_MAPRIMRENOV_TRESMODESTE';

    /**
     * Texte administrable pour MaPrimeRenov - modeste
     */
    const TEXTE_POPUP_MAPRIMRENOV_MODESTE = 'TEXTE_POPUP_MAPRIMRENOV_MODESTE';

    /**
     * Texte administrable pour MaPrimeRenov - intermédiare
     */
    const TEXTE_POPUP_MAPRIMRENOV_INTREMEDIAIRE = 'TEXTE_POPUP_MAPRIMRENOV_INTREMEDIAIRE';

    /**
     * Texte administrable pour MaPrimeRenov - supérieur
     */
    const TEXTE_POPUP_MAPRIMRENOV_SUPERIEUR = 'TEXTE_POPUP_MAPRIMRENOV_SUPERIEUR';

    /**
     * Texte administrable pour MaPrimeRenov - très modeste
     */
    const TEXTE_RESULTAT_MAPRIMRENOV_TRESMODESTE = 'TEXTE_RESULTAT_MAPRIMRENOV_TRESMODESTE';

    /**
     * Texte administrable pour MaPrimeRenov - modeste
     */
    const TEXTE_RESULTAT_MAPRIMRENOV_MODESTE = 'TEXTE_RESULTAT_MAPRIMRENOV_MODESTE';

    /**
     * Texte administrable pour MaPrimeRenov - intermédiare
     */
    const TEXTE_RESULTAT_MAPRIMRENOV_INTREMEDIAIRE = 'TEXTE_RESULTAT_MAPRIMRENOV_INTREMEDIAIRE';

    /**
     * Texte administrable pour MaPrimeRenov - supérieur
     */
    const TEXTE_RESULTAT_MAPRIMRENOV_SUPERIEUR = 'TEXTE_RESULTAT_MAPRIMRENOV_SUPERIEUR';

    /**
     * Texte administrable pour Chapo MaPrimeRenov - Intermediaire
     */
    const TEXTE_CHAPO_MAPRIMRENOV_INTERMEDIAIRE= 'TEXTE_CHAPO_MAPRIMRENOV_INTERMEDIAIRE';

    /**
     * Texte administrable pour Chapo MaPrimeRenov - supérieur
     */
    const TEXTE_CHAPO_MAPRIMRENOV_SUPERIEUR = 'TEXTE_CHAPO_MAPRIMRENOV_SUPERIEUR';

    /**
     * Texte administrable pour Chapo MaPrimeRenov - très Modeste
     */
    const TEXTE_CHAPO_MAPRIMRENOV_TRES_MODESTE = 'TEXTE_CHAPO_MAPRIMRENOV_TRES_MODESTE';

    /**
     * Texte administrable pour Chapo MaPrimeRenov - Modeste
     */
    const TEXTE_CHAPO_MAPRIMRENOV_MODESTE = 'TEXTE_CHAPO_MAPRIMRENOV_MODESTE';

    /**
     *  Code de la liste mode de chauffage
     */
    const L_MODE_CHAUFFAGE = 'L_MODE_CHAUFFAGE';

    /**
     *  Type du mode de chauffage INDIVIDUEL
     */
    const MODE_CHAUFFAGE_INDIVIDUEL = 'INDIVIDUEL';
    /**
     *  Type du mode de chauffage COLLECTIF
     */
    const MODE_CHAUFFAGE_COLLECTIF = 'COLLECTIF';
    /**
     * Texte d'aide pour la simulation
     */
    const TEXTE_AIDE_OFFRES_CEE = 'TEXTE_AIDE_OFFRES_CEE';


}
