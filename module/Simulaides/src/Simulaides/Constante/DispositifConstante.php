<?php
namespace Simulaides\Constante;

/**
 * Classe PrisConstante
 * Constantes des données venant de PRIS
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class DispositifConstante
{
    //images et credit defaut pour dispositif
    const IMAGE_DISPOSITIF_DEFAULT = 'default.jpg';
    const CREDIT_DISPOSITIF_DEFAULT = '';
    const IMAGE_DISPOSITIF_ANAH = 'dispositif_anah.jpg';
    const CREDIT_DISPOSITIF_ANAH = '© ANAH';
    const IMAGE_DISPOSITIF_CEE = 'dispositif_cee.jpg';
    const CREDIT_DISPOSITIF_CEE = '';
    const IMAGE_DISPOSITIF_COMMUNAL = 'dispositif_communal.jpg';
    const CREDIT_DISPOSITIF_COMMUNAL = '';
    const IMAGE_DISPOSITIF_DEPARTEMENT = 'dispositif_departement.jpg';
    const CREDIT_DISPOSITIF_DEPARTEMENT = '';
    const IMAGE_DISPOSITIF_EPCI = 'dispositif_epci.jpg';
    const CREDIT_DISPOSITIF_EPCI = '';
    const IMAGE_DISPOSITIF_NATIONAL = 'dispositif_national.jpg';
    const CREDIT_DISPOSITIF_NATIONAL = '';
    const IMAGE_DISPOSITIF_PUBLIC = 'dispositif_public.jpg';
    const CREDIT_DISPOSITIF_PUBLIC = '';
    const IMAGE_DISPOSITIF_REGION = 'dispositif_region.jpg';
    const CREDIT_DISPOSITIF_REGION = '';
    const IMAGE_DISPOSITIF_MAPRIMRENOV_SUP = 'dispositif_maprimrenov_sup.png';
    const IMAGE_DISPOSITIF_MAPRIMRENOV_INTER = 'dispositif_maprimrenov_inter.png';
    const IMAGE_DISPOSITIF_MAPRIMRENOV_MODESTE = 'dispositif_maprimrenov_modeste.png';
    const IMAGE_DISPOSITIF_MAPRIMRENOV_TRES_MODESTE = 'dispositif_maprimrenov_tres_modeste.png';
    const IMAGE_DISPOSITIF_COUP_DE_POUCE = 'coup-de-pouce.jpg';
    const CREDIT_DISPOSITIF_COUP_DE_POUCE = '';


    //IDS dispostif spécifique
    const IDS_DISPOSITIF_MAPRIMRENOV = '1764, 1765, 1766';
    const IDS_DISPOSITIF_COUPDEPOUCE = '815, 820, 1554, 1556';
}
