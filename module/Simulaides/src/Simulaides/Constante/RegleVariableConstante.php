<?php
namespace Simulaides\Constante;

/**
 * Classe RegleVariableConstante
 * constantes pour les règles dispositif/travaux
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class RegleVariableConstante
{
    /** Variable sur les tranches */
    const VARIABLE_TRANCHE = '$F.tranche';

    /** Valeur "Hors TRanche" pour la variable tranche */
    const VALEUR_TRANCHE_HORS_TRANCHE = 'HORS_TRANCHE';

    /** Variable sur le statut */
    const  VARIABLE_STATUT = '$F.statut';

    /** Variable sur le statut */
    const  VARIABLE_TYPE_COMBUSTIBLE = '$T.tech[TYPE_COMBUSTIBLE]';

    /** Variable sur le statut */
    const  VARIABLE_TYPE_COMBUSTIBLE_BIOMASSE = '$T.tech[TYPE_COMBUSTIBLE_BIOMASSE]';

    /** Nombre de parts fiscales */
    const  VARIABLE_NB_PARTS_FISC = '$F.nb_parts_fisc';

    /** Type de logement */
    const  VARIABLE_TYPE_LOGEMENT = '$L.type';

    /** Energie de chauffage */
    const  VARIABLE_ENERGIE_CHAUFFAGE = '$L.energie_chauffage';

    /** Energie de chauffage détaillée*/
    const  VARIABLE_ENERGIE_CHAUFFAGE_DETAIL = '$L.energie_chauffage_detail';

    /** Paramètre technique */
    const VARIABLE_PARAM_TECH = '$T.tech';

    /** Montant total des travaux */
    const VARIABLE_TRAVAUX_MONTANT_TOTAL = '$T.montant_to';

    /** Montant main d'oeuvre des travaux */
    const VARIABLE_TRAVAUX_MONTANT_MO = '$T.montant_mo';

    /** Montant fourniture des travaux */
    const VARIABLE_TRAVAUX_MONTANT_FO = '$T.montant_fo';

    /** Cumul total des travaux */
    const VARIABLE_TRAVAUX_CUMUL_TOTAL = '$T.cumul_to';

    /** Cumul main d'oeuvre des travaux */
    const VARIABLE_TRAVAUX_CUMUL_MO = '$T.cumul_mo';

    /** Cumul fourniture des travaux */
    const VARIABLE_TRAVAUX_CUMUL_FO = '$T.cumul_fo';

    /** Catégorie de classement des variables du travaux Changement de fenetre */
    const CATEGORIE_TRAVAUX_FENETRE = 'X';

    /** Paramètre nombre de fenêtre */
    const VARIABLE_NB_FENETRE = '$T.nb_fenetres';

    /** Paramètre nombre de porte fenêtre */
    const VARIABLE_NB_PORTE_FENETRE = '$T.nb_portes_fenetres';

    /** Paramètre surface de fenêtre */
    const VARIABLE_SURF_FENETRE = '$T.surf_fenetres';

    /** Paramètre surface de porte fenêtre */
    const VARIABLE_SURF_PORTE_FENETRE = '$T.surf_portes_fenetres';

    /** Code du travaux de type Changement de fenêtre */
    const VARIABLE_CODE_TRAVAUX_CHANGE_FENETRE = 'T01';

    /** Code du travaux de type Panneaux solaires photovoltaïques */
    const VARIABLE_CODE_TRAVAUX_PAN_SOL_PHOTO = 'T28';

    /** Préfixe des variables */
    /** Foyer */
    const VARIABLE_PREFIX_FOYER = 'F';
    /** Logement */
    const VARIABLE_PREFIX_LOGEMENT = 'L';
    /** Travaux */
    const VARIABLE_PREFIX_TRAVAUX = 'T';
    /** Dispositif */
    const VARIABLE_PREFIX_DISPOSITIF = 'D';

    /** Partie literral des variables, sans le préfixe $X */
    /** Cout HT Total */
    const VARIABLE_LIT_COUT_HT_TOTAL = 'cout_ht_to';
    /** Cout TTC Total */
    const VARIABLE_LIT_COUT_TTC_TOTAL = 'cout_ttc_to';
    /** Cout HT Main Oeuvre */
    const VARIABLE_LIT_COUT_HT_MO = 'cout_ht_mo';
    /** Cout TTC Main Oeuvre */
    const VARIABLE_LIT_COUT_TTC_MO = 'cout_ttc_mo';
    /** Cout HT Fourniture */
    const VARIABLE_LIT_COUT_HT_FO = 'cout_ht_fo';
    /**  Cout TTC Fourniture */
    const VARIABLE_LIT_COUT_TTC_FO = 'cout_ttc_fo';
    /**  Nombre de travaux différents */
    const VARIABLE_LIT_NB_TRAVAUX_ELIGIBLES = 'nb_travaux_eligibles';
    /**  Nombre de travaux différents */
    const VARIABLE_LIT_NB_TOTAL_TRAVAUX = 'nb_total_travaux';
    /** Paramètre technique */
    const VARIABLE_LIT_PARAM_TECH = 'tech';
    /** Nombre de fenêtre */
    const VARIABLE_LIT_NB_FENETRE = 'nb_fenetres';
    /** Nombre de porte fenêtre */
    const VARIABLE_LIT_NB_PORTE_FENETRE = 'nb_portes_fenetres';
    /** Surface de fenêtre */
    const VARIABLE_LIT_SURF_FENETRE = 'surf_fenetres';
    /** Surface de porte fenêtre */
    const VARIABLE_LIT_SURF_PORTE_FENETRE = 'surf_portes_fenetres';
    /** Type de logement */
    const VARIABLE_LIT_TYPE_LOGEMENT = 'type';
    /** Age du logement */
    const VARIABLE_LIT_AGE_LOGEMENT = 'age';
    /** Annee du logement */
    const VARIABLE_LIT_ANNEE_LOGEMENT = 'annee';
    /** Surface du logement */
    const VARIABLE_LIT_SURFACE_LOGEMENT = 'surface';
    /** Energie de chauffage */
    const VARIABLE_LIT__ENERGIE_CHAUFF = 'energie_chauffage';
    /** Energie de chauffage détaillée */
    const VARIABLE_LIT__ENERGIE_CHAUFF_DETAIL = 'energie_chauffage_detail';
    /** Logement bbc ?*/
    const VARIABLE_LIT_BBC_LOGEMENT = 'bbc';
    /** Statut du foyer */
    const VARIABLE_LIT_STATUT_FOYER = 'statut';
    /** Nombre de parts fiscales */
    const VARIABLE_LIT_NB_PARTS_FISC = 'nb_parts_fisc';
    /** code mode chauffage */
    const VARIABLE_LIT_MODE_CHAUFFAGE = 'mode_chauffage';
    /** Nb d'adulte dans le foyer */
    const VARIABLE_LIT_NB_ADULTE_FOYER = 'nb_adultes';
    /** Nb de pers à charge dans le foyer */
    const VARIABLE_LIT_NB_PERS_CHARGE_FOYER = 'nb_pers_charge';
    /** Nb enfant garde alterné dans le foyer */
    const VARIABLE_LIT_NB_ENF_GARDE_ALT_FOYER = 'nb_enf_gardealt';
    /** Revenu du foyer */
    const VARIABLE_LIT_REVENU_FOYER = 'revenu';
    /** Prêt à taux 0 du foyer */
    const VARIABLE_LIT_PTZ_FOYER = 'ptz';
    /** Primo-accédant du foyer */
    const VARIABLE_LIT_PRIMO_ACCESSION_FOYER = 'primo_accedant';
    /** salarie_secteur_prive du foyer */
    const VARIABLE_LIT_SALARIE_SECTEUR_PRIVE_FOYER = 'salarie_secteur_prive';
    /** Montant crédit d'impot */
    const VARIABLE_LIT_MT_CREDIT_IMPOT_FOYER = 'm_credit_impot';
    /** Tranche de revenu du foyer */
    const VARIABLE_LIT_TRANCHE_FOYER = 'tranche';

    /** VARIABLE DES PARAMETRES TECHNIQUES */

    /** Nombre de fenêtre Alu */
    const VAR_PARAM_TECH_NB_FEN_ALU = 'NB_FEN_ALU';
    /** Nombre de fenêtre Bois */
    const VAR_PARAM_TECH_NB_FEN_BOIS = 'NB_FEN_BOIS';
    /** Nombre de fenêtre PVC */
    const VAR_PARAM_TECH_NB_FEN_PVC = 'NB_FEN_PVC';
    /** Nombre de porte fenêtre Alu */
    const VAR_PARAM_TECH_NB_POR_FEN_ALU = 'NB_POR_FEN_ALU';
    /** Nombre de porte fenêtre Bois */
    const VAR_PARAM_TECH_NB_POR_FEN_BOIS = 'NB_POR_FEN_BOIS';
    /** Nombre de porte fenêtre PVC */
    const VAR_PARAM_TECH_NB_POR_FEN_PVC = 'NB_POR_FEN_PVC';
    /** Surface de fenêtre Alu */
    const VAR_PARAM_TECH_SURF_FEN_ALU = 'SURF_FEN_ALU';
    /** Surface de fenêtre Bois */
    const VAR_PARAM_TECH_SURF_FEN_BOIS = 'SURF_FEN_BOIS';
    /** Surface de fenêtre PVC */
    const VAR_PARAM_TECH_SURF_FEN_PVC = 'SURF_FEN_PVC';
    /** Surface de porte fenêtre Alu */
    const VAR_PARAM_TECH_SURF_POR_FEN_ALU = 'SURF_POR_FEN_ALU';
    /** Surface de porte fenêtre Bois */
    const VAR_PARAM_TECH_SURF_POR_FEN_BOIS = 'SURF_POR_FEN_BOIS';
    /** Surface de porte fenêtre PVC */
    const VAR_PARAM_TECH_SURF_POR_FEN_PVC = 'SURF_POR_FEN_PVC';

    /** Surface isolant mur extérieur */
    const VAR_PARAM_TECH_SURFACE_ISOLANT_MURS_EXT = 'SURFACE_ISOLANT_MURS_EXT';
    /** Surface isolant mur intérieur */
    const VAR_PARAM_TECH_SURFACE_ISOLANT_MURS_INT = 'SURFACE_ISOLANT_MURS_INT';
    /** Surface isolant comble = */
    const VAR_PARAM_TECH_SURFACE_ISOLANT_COMBLES = 'SURFACE_ISOLANT_COMBLES';
    /** Surface isolant toit intérieur */
    const VAR_PARAM_TECH_SURFACE_ISOLANT_TOIT_INT = 'SURFACE_ISOLANT_TOIT_INT';
    /** Surface isolant toit extérieur */
    const VAR_PARAM_TECH_SURFACE_ISOLANT_TOIT_EXT = 'SURFACE_ISOLANT_TOIT_EXT';
    /** Surface isolant toit terrasse */
    const VAR_PARAM_TECH_SURFACE_ISOLANT_TOIT_TERRASSE = 'SURFACE_ISOLANT_TOIT_TERRASSE';
    /** Surface plancher */
    const VAR_PARAM_TECH_SURF_PLANCHER = 'SURF_PLANCHER';
    /** Surface isolant plancher bas */
    const VAR_PARAM_TECH_SURF_ISOLANT_PLANCHER_BAS = 'SURFACE_ISOLANT_PLANCHER_BAS';
    /** Nb de persiennes */
    const VAR_PARAM_TECH_NB_PERSIENNES = 'NB_PERSIENNES';
    /** Nb de volets */
    const VAR_PARAM_TECH_NB_VOLETS = 'NB_VOLETS';

    /** Nb de volets */
    const VAR_NB_RAD_ELEC_PERF = 'NB_RAD_ELEC_PERF';

    /** Poele */
    const VAR_PARAM_TECH_POELE = 'POELE';
    /** Insert */
    const VAR_PARAM_TECH_INSERT = 'INSERT';
    /** Foyer fermé */
    const VAR_PARAM_TECH_FOYER_FERME = 'FOYER_FERME';

    /** Sonde */
    const VAR_PARAM_TECH_SONDE = 'SONDE';

    /** Programmateur */
    const VAR_PARAM_TECH_PROGRAMMATEUR = 'PROGRAMMATEUR';

    /** Programmateur */
    const VAR_PARAM_TECH_MODE_CHAUFFAGE_PROG = 'MODE_CHAUFFAGE_PROGRAMMATEUR';

    /** Mode chauffage radiateur */
    const VAR_PARAM_TECH_MODE_CHAUFFAGE_RAD = 'MODE_CHAUFFAGE_RAD';

    /** Nb de radiateurs */
    const VAR_PARAM_TECH_NB_RADIATEURS = 'NB_RADIATEURS';

    /** Mode chauffage au plancher */
    const VAR_PARAM_TECH_MODE_CHAUFFAGE_PLANCHER = 'MODE_CHAUFFAGE_PLANCHER';

    /** Panneaux solaires photovoltaïques*/
    const VAR_PARAM_TECH_PUISSANCE_INSTALLEE = 'PUISSANCE_INSTALLEE';
    /** Panneaux solaires photovoltaïques*/
    const VAR_PARAM_TECH_PRESENCE_SOLUTIONS_STOCKAGE = 'PRESENCE_SOLUTIONS_STOCKAGE';

    /**
     * Liste des variables pour le montant des travaux
     * @var array
     */
    public static $tabVarTravauxMontantCumul = [
        self::VARIABLE_TRAVAUX_MONTANT_TOTAL,
        self::VARIABLE_TRAVAUX_MONTANT_MO,
        self::VARIABLE_TRAVAUX_MONTANT_FO,
        self::VARIABLE_TRAVAUX_CUMUL_TOTAL,
        self::VARIABLE_TRAVAUX_CUMUL_MO,
        self::VARIABLE_TRAVAUX_CUMUL_FO
    ];

    /** Type de chauffage */
    const  VARIABLE_MODE_CHAUFFAGE = '$L.mode_chauffage';
    /** TRAVAUX REQUIS */
    const  VARIABLE_TRAVAUX_REQUIS = '$D.trav_requis';
}
