<?php
namespace Simulaides\Constante;

/**
 * Classe ListeOuiNonConstante
 * Constante pour les listes Oui/non
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class ListeOuiNonConstante
{
    /** Valeur Oui */
    const VALUE_OUI = '1';

    /** Valeur Non */
    const VALUE_NON = '0';

    /**
     * Retourne le libellé de la constante indiquée
     *
     * @param string $codeConst Code de la constante
     *
     * @return string Valeur de la constante
     */
    public static function getValeur($codeConst)
    {
        $libelle = '';

        switch ($codeConst) {
            case self::VALUE_OUI:
                $libelle = 'Oui';
                break;
            case self::VALUE_NON:
                $libelle = 'Non';
                break;
        }

        return $libelle;
    }
}
