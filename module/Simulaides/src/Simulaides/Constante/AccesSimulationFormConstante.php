<?php
namespace Simulaides\Constante;

/**
 * Classe AccesSimulationFormConstante
 * Constantes permettant de gérer le formulaire d'accès à une simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class AccesSimulationFormConstante
{
    /**
     * Nom du champ "Numéro de simulation"
     */
    const CHAMP_NUM_SIMULATION = 'numSimulation';

    /**
     * Nom du champ "Code d'accès"
     */
    const CHAMP_CODE_ACCES = 'codeAcces';

    /**
     * Nom du champ de sécurité
     */
    const CHAMP_CSRF = 'csrf';
}
