<?php
namespace Simulaides\Constante;

/**
 * Classe ParametreTechConstante
 * Fichier de constante des paramètres technique
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class ParametreTechConstante
{
    /** Type de paramètre : Entier */
    const TYPE_DATA_ENTIER = 'ENTIER';

    /** Type de paramètre : Liste */
    const TYPE_DATA_LISTE = 'LISTE';

    /** Type de paramètre : Boolean */
    const TYPE_DATA_BOOLEEN = 'BOOLEEN';

    /** Type de paramètre : Boolean */
    const TYPE_DATA_BOOLEEN_RADIO = 'BOOLEEN_RADIO';
}
