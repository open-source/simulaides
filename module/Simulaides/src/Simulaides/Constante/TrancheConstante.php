<?php
namespace Simulaides\Constante;

/**
 * Classe SimulationConstante
 * Classe des constantes de la simulation
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class TrancheConstante
{
    /** Tranches */
    const MODESTE = 'MODESTE';
    const TRES_MODESTE = 'TRES_MODESTE';
    const MODESTE_IDF = 'MODESTE_IDF';
    const TRESMODESTE_IDF = 'TRESMODESTE_IDF';
    const INTER_MPR = 'INTER_MPR';
    const INTER_MPR_IDF = 'INTER_MPR_IDF';
    const MODESTE_MPR = 'MODESTE_MPR';
    const MODESTE_MPR_IDF = 'MODESTE_MPR_IDF';
    const TRES_MOD_MPR = 'TRES_MOD_MPR';
    const TRES_MOD_MPR_I = 'TRES_MOD_MPR_I';
}
