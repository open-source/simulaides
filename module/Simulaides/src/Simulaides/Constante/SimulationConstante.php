<?php
namespace Simulaides\Constante;

/**
 * Classe SimulationConstante
 * Classe des constantes de la simulation
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class SimulationConstante
{
    /** Mode d'exécution d'une simulation en test */
    const MODE_TEST = 'MODE_TEST';

    /** Mode d'exécution d'une simulation en test */
    const MODE_PRODUCTION = 'MODE_PRODUCTION';

    /** Aide de type Public */
    const TYPE_AIDE_PUBLIC = 'PUBLIC';

    /** Aide de type CEE */
    const TYPE_AIDE_CEE = 'CEE';

    /** Aide de type Anah */
    const TYPE_AIDE_ANAH = 'ANAH';

    /** Aide de type EPCI */
    const TYPE_AIDE_EPCI = 'EPCI';

    /** Aide de type Region */
    const TYPE_AIDE_REGION = 'REGION';

    /** Aide de type Communal */
    const TYPE_AIDE_COMMUNAL = 'COMMUNAL';

    /** Aide de type National */
    const TYPE_AIDE_NATIONAL = 'NATIONAL';

    /** Aide de type Departemental */
    const TYPE_AIDE_DEPARTEMENT = 'DEPARTEMENT';

    /** Aide de type Coup de pouce */
    const TYPE_AIDE_COUP_DE_POUCE = 'COUP_DE_POUCE';

    /** CAractéristique CE */
    const CEE_CARAC_SONDE = 'SONDE';

    /** CAractéristique CE */
    const CEE_CARAC_PROGRAMMATEUR = 'PROGRAMMATEUR';
}
