<?php
namespace Simulaides\Constante;

/**
 * Classe SimulationConstante
 * Classe des constantes de la simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class GroupeTrancheConstante
{
    /** Groupe tranche ANAH */
    const ANAH = 'ANAH';

    /** Groupe tranche ANAH */
    const ANAH_IDF = 'ANAH_IDF';

    /** Groupe tranche MaPrimRenov */
    const MPR = 'MPR';

    /** Groupe tranche MaPrimRenov IDF */
    const MPR_IDF = 'MPR_IDF';
}
