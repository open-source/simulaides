<?php
namespace Simulaides\Constante;

/**
 * Classe SessionConstante
 * Cette classe permet de gérer les noms des éléments stockés en session
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class SessionConstante
{
    /** Nom du container */
    const CONTAINER_NAME = 'simulaides';

    /** Permet de savoir si la mention "lecture des CGU" a été réalisé */
    const CGU = 'cgu';

    /** Permet de connaitre la liste des travaux qui sont dans l'accordéon ouvert */
    const TRAVAUX_OPEN = 'travauxOpen';

    /** Liste des travaux choisis */
    const TRAVAUX = 'travaux';

    /** Liste des travaux choisis */
    const CODES_TRAVAUX = 'codesTravaux';

    /** Indique s'il s'agit d'un projet permettant d'atteindre le niveau BBC-rénovation */
    const BBC = 'bbc';

    const SIMULATION = 'simulation';

    const OFFRES = 'offres';

    /** Nombre de paramètre tech affichés pour un produit */
    const NB_PARAMETER = 'nbParameter';

    /** Indique si le javascript est actif */
    const JS_ACTIVE = 'jsActive';

    /** TYPES DE COUT */

    /** Cout total */
    const COUT_TOTAL = 'COUT_TOTAL';

    /** Cout de la main d'oeuvre */
    const COUT_MO = 'COUT_MO';

    /** Cout de la fourniture */
    const COUT_FO = 'COUT_FOURNITURE';


    /** Informations de l'utilisateur */

    /** Indique que l'on a demandé l'accès au site PRIS */
    const ACCESS_PRIS = 'accesPris';

    /** Indique si l'administrateur est connecté */
    const ADMIN_CONNECTEE = 'adminConnecte';

    /** Objet ConseillerPris */
    const CONSEILLER = 'conseiller';

    /** LOCALISATION */
    /** Région */
    const LOCALISATION_REGION = 'region';

    /** Département */
    const LOCALISATION_DEPARTEMENT = 'departement';

    /** Code Postal */
    const LOCALISATION_CODE_POSTAL = 'codePostal';

    /** Commune */
    const LOCALISATION_COMMUNE = 'commune';

    /** Code région du particulier */
    const REGION_PARTICULIER = 'regionParticulier';

    /** Code région du particulier */
    const PARTENAIRE = 'partenaire';

    /** Constantes pour les codes couleurs de l'application */

    /** Couleur de fond de titre de niveau 1 */
    const NAME_FOND_TITRE_N1 = 'nameFondTitreN1';

    /** Couleur de fond de titre de niveau 1 par défaut */
    const FOND_TITRE_N1_DEFAUT = '#226db6';

    /** Couleur de fond de titre de niveau 1 partie PRIS */
    const FOND_TITRE_N1_PRIS = '#FCC012';

    /** Couleur de fond de titre de niveau 2 */
    const NAME_FOND_TITRE_N2 = 'nameFondTitreN2';

    /** Couleur de fond de titre de niveau 2 par défaut */
    const FOND_TITRE_N2_DEFAUT = '#b871c5';

    /** Couleur de fond de titre de niveau 2 partie PRIS */
    const FOND_TITRE_N2_PRIS = '#42C1CC';

    /** Couleur de fond de titre de niveau 3 */
    const NAME_FOND_TITRE_N3 = 'nameFondTitreN3';

    /** Couleur de fond de titre de niveau 3 par défaut */
    const FOND_TITRE_N3_DEFAUT = '#b871c5';

    /** Couleur de fond de titre de niveau 3 partie PRIS */
    const FOND_TITRE_N3_PRIS = '#42C1CC';

    /** Couleur de fond de bouton */
    const NAME_FOND_BOUTON = 'nameFondBtn';

    /** Couleur de fond de bouton par défaut */
    const FOND_BOUTON_DEFAUT = '#b871c5';

    /** Couleur de fond de bouton partie PRIS*/
    const FOND_BOUTON_PRIS = '#42C1CC';

    /** Constantes sur le cookie de gestion du type de module */

    /** Clé du cookie pour connaitre l'origine (particulier ou conseiller) */
    const COOKIE_KEY_ORIGINE = 'origine';

    /** Origine particulier */
    const COOKIE_ORIGINE_PARTICULIER = 'particulier';

    /** Origine conseiller */
    const COOKIE_ORIGINE_CONSEILLER = 'conseiller';

    /** VERSION */
    const VERSION = 'En cours';

}
