<?php

namespace Simulaides\Constante;

/**
 * Classe RegleOperateurConstante
 * Constantes pour les opérateurs des règles
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class RegleOperateurConstante
{
    /** OPERATEURS DE FORMULE */
    /** "(" */
    const DISPOSITIF_OP_PARANTHESE_OUVRE = '(';

    /** ")" */
    const DISPOSITIF_OP_PARANTHESE_FERME = ')';

    /** "+" */
    const DISPOSITIF_OP_PLUS = '+';

    /** "-" */
    const DISPOSITIF_OP_MOINS = '-';

    /** "*" */
    const DISPOSITIF_OP_MULTIPLIE = '*';

    /** "/" */
    const DISPOSITIF_OP_DIVISE = '/';

    /** "=" */
    const DISPOSITIF_OP_EGAL = '===';

    /** "!=" */
    const DISPOSITIF_OP_DIFFERENT = '!=';

    /** "<=" */
    const DISPOSITIF_OP_INF_EGAL = '<=';

    /** "<" */
    const DISPOSITIF_OP_INF = '<';

    /** ">=" */
    const DISPOSITIF_OP_SUP_EGAL = '>=';

    /** ">" */
    const DISPOSITIF_OP_SUP = '>';

    /** ET */
    const DISPOSITIF_OP_ET = '&&';

    /** OU */
    const DISPOSITIF_OP_OU = '||';

    /** Non */
    const DISPOSITIF_OP_NON = '!';

    /** Vrai */
    const DISPOSITIF_OP_VRAI = 'true';

    /** Faux */
    const DISPOSITIF_OP_FAUX = 'false';

    /**
     * Liste des opérateurs sous forme de tableau
     * @var array
     */
    private static $tabOperateur = [
        self::DISPOSITIF_OP_PARANTHESE_OUVRE,
        self::DISPOSITIF_OP_PARANTHESE_FERME,
        self::DISPOSITIF_OP_PLUS,
        self::DISPOSITIF_OP_MOINS,
        self::DISPOSITIF_OP_MULTIPLIE,
        self::DISPOSITIF_OP_DIVISE,
        self::DISPOSITIF_OP_EGAL,
        self::DISPOSITIF_OP_DIFFERENT,
        self::DISPOSITIF_OP_INF_EGAL,
        self::DISPOSITIF_OP_INF,
        self::DISPOSITIF_OP_SUP_EGAL,
        self::DISPOSITIF_OP_SUP,
        self::DISPOSITIF_OP_ET,
        self::DISPOSITIF_OP_OU,
        self::DISPOSITIF_OP_NON,
        self::DISPOSITIF_OP_VRAI,
        self::DISPOSITIF_OP_FAUX
    ];

    /**
     * Permet de retourner le libellé de l'opérateur
     *
     * @param $codeOperateur
     *
     * @return mixed
     */
    public static function getLabelOperateur($codeOperateur)
    {
        $labelOperateur = $codeOperateur;

        switch ($codeOperateur) {
            case self::DISPOSITIF_OP_PARANTHESE_OUVRE:
                $labelOperateur = '(';
                break;
            case self::DISPOSITIF_OP_PARANTHESE_FERME:
                $labelOperateur = ')';
                break;
            case self::DISPOSITIF_OP_PLUS:
                $labelOperateur = '+';
                break;
            case self::DISPOSITIF_OP_MOINS:
                $labelOperateur = '-';
                break;
            case self::DISPOSITIF_OP_MULTIPLIE:
                $labelOperateur = '*';
                break;
            case self::DISPOSITIF_OP_DIVISE:
                $labelOperateur = '/';
                break;
            case self::DISPOSITIF_OP_EGAL:
                $labelOperateur = '=';
                break;
            case self::DISPOSITIF_OP_DIFFERENT:
                $labelOperateur = '≠';
                break;
            case self::DISPOSITIF_OP_INF_EGAL:
                $labelOperateur = '≤';
                break;
            case self::DISPOSITIF_OP_INF:
                $labelOperateur = '<';
                break;
            case self::DISPOSITIF_OP_SUP_EGAL:
                $labelOperateur = '≥';
                break;
            case self::DISPOSITIF_OP_SUP:
                $labelOperateur = '>';
                break;
            case self::DISPOSITIF_OP_ET:
                $labelOperateur = 'ET';
                break;
            case self::DISPOSITIF_OP_OU:
                $labelOperateur = 'OU';
                break;
            case self::DISPOSITIF_OP_NON:
                $labelOperateur = 'Non';
                break;
            case self::DISPOSITIF_OP_VRAI:
                $labelOperateur = 'Vrai';
                break;
            case self::DISPOSITIF_OP_FAUX:
                $labelOperateur = 'Faux';
                break;
        }

        return $labelOperateur;
    }

    /**
     * Indique si le code est un opérateur
     *
     * @param $code
     *
     * @return bool
     */
    public static function isOperateur($code)
    {
        return in_array($code, self::$tabOperateur);
    }
}
