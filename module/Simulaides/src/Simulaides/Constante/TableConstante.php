<?php
namespace Simulaides\Constante;

/**
 * Classe TableConstante
 * Constantes utilisées dans la gestion des tableaux
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Constante
 */
class TableConstante
{
    /** Celulle de type text */
    const TYPE_TEXTE = 'text';

    /** Celulle de type lien */
    const TYPE_LIEN = 'lien';

    /** Cellule de type case à cocher */
    const TYPE_CASE_COCHER = 'checkbox';

    /** Celulle de type bouton supprimer */
    const TYPE_BOUTON_SUPP = 'bouton-supp';

    /** Celulle de type bouton modifier */
    const TYPE_BOUTON_MODIF = 'bouton-modif';

    /** Celulle de type bouton visualiser */
    const TYPE_BOUTON_VISUALISER = 'bouton-visualiser';

    /** Alignement des cellules */
    /** Centrer */
    const ALIGN_CENTER = 'center';
    /** Droite */
    const ALIGN_RIGHT = 'right';
    /** Gauche */
    const ALIGN_LEFT = 'left';
}
