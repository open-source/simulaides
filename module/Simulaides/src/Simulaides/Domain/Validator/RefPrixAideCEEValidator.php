<?php
namespace Simulaides\Domain\Validator;

use Zend\I18n\Validator\Float;
use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;

/**
 * Classe RefPrixAideCEEValidator
 * Validateur de l'écran de paramétrage du référentiel des prix
 * Formulaire Aide CEE
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Validator
 */
class RefPrixAideCEEValidator extends InputFilter
{
    /**
     * Constructeur du validateur
     */
    public function __construct()
    {
        //Coefficient
        $this->add(
            [
                'name'        => 'coefficient',
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Ce nombre doit être renseigné'
                            ]
                        ]
                    ],
                    [
                        'name'    => 'Float',
                        'options' => [
                            'locale'   => 'en_US',
                            'messages' => [
                                Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                            ]
                        ]
                    ],
                ],
            ]
        );
    }
}
