<?php
namespace Simulaides\Domain\Validator;

use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

/**
 * Classe FicheGroupeTrancheValidator
 * Validateur pour la fiche d'un groupe de tranche de revenu
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Validator
 */
class FicheGroupeTrancheValidator extends InputFilter
{
    /**
     * Constructeur du validateur
     */
    public function __construct()
    {
        //Validateur sur le code du groupe
        $this->add(
            [
                'name'        => 'codeGroupe',
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Le code du groupe doit être renseigné"
                            ]
                        ]
                    ],
                    [
                        'name' => 'Simulaides\Domain\Validator\CodeValidator',
                        'break_chain_on_failure' => true
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'max'      => 15,
                            'messages' => [
                                StringLength::TOO_LONG => 'La valeur saisie ne doit pas dépasser %max%'
                            ]
                        ]
                    ],
                ]
            ]
        );

        //Validateur sur l'intitulé du groupe
        $this->add(
            [
                'name'        => 'intitule',
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "L'intitulé du groupe doit être renseigné"
                            ]
                        ]
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'max'      => 150,
                            'messages' => [
                                StringLength::TOO_LONG => 'La valeur saisie ne doit pas dépasser %max%'
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Zend\Filter\StripTags',
                    ]
                ]
            ]
        );
    }
}
