<?php
namespace Simulaides\Domain\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;
use Zend\Validator\ValidatorPluginManager;
use Zend\Validator\ValidatorPluginManagerAwareInterface;

/**
 * Classe CodeValidator
 * Validateur pour les codes (majuscules et _)
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Validator
 */
class CodeValidator extends AbstractValidator implements ValidatorPluginManagerAwareInterface
{
    /** Valeur non majuscule */
    const NOT_UPPER = 'notUpper';

    /** Valeur non alpha ou _ */
    const NOT_ALPHA = 'notAlpha';

    /** @var array Messages d'erreur */
    protected $messageTemplates = array(
        self::NOT_UPPER => "La valeur n'est pas en majuscule",
        self::NOT_ALPHA => "La valeur n'est pas alphanumérique pouvant contenir un _"
    );

    /**
     * @var
     */
    private $pluginManager;

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     *
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        $this->setValue($value);

        //Remplacement des caractères '_'
        $value = str_replace("_", "", $value);

        if (!ctype_alnum($value)) {
            //La valeur n'est pas en lettre
            $this->error(self::NOT_ALPHA);
            return false;
        }

        //Replacement des chiffres
        $value = preg_replace('`[0-9]`sm', "", $value);

        if (!ctype_upper($value)) {
            //La valeur n'est pas en majuscule
            $this->error(self::NOT_UPPER);
            return false;
        }

        return true;
    }

    /**
     * Set validator plugin manager
     *
     * @param ValidatorPluginManager $pluginManager
     */
    public function setValidatorPluginManager(ValidatorPluginManager $pluginManager)
    {
        $this->pluginManager = $pluginManager;
    }

    /**
     * Get validator plugin manager
     *
     * @return ValidatorPluginManager
     */
    public function getValidatorPluginManager()
    {
        return $this->pluginManager;
    }
}
