<?php
namespace Simulaides\Domain\Validator;

use Simulaides\Constante\MainConstante;
use Zend\Validator\AbstractValidator;

/**
 * Classe PeriodeDateValidator
 * Validateur sur les périodes
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif\Validator
 */
class PeriodeDateValidator extends AbstractValidator
{
    /** Date de fin inférieure ou égale à date début  */
    const DATE_FIN_INF = "dateFinInf";

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::DATE_FIN_INF => "La date de fin doit être supérieure à la date de début.",
    ];

    /**
     * @param mixed $value
     * @param array $context
     *
     * @return bool
     */
    public function isValid($value, $context = [])
    {
        $dateDebut = $this->getDateValue($context, MainConstante::NAME_DATE_DEBUT_PERIODE);
        $dateFin   = $this->getDateValue($context, MainConstante::NAME_DATE_FIN_PERIODE);

        //Vérification que la date de début < date de fin
        if (isset($dateDebut) && isset($dateFin)) {
            if (($dateFin == $dateDebut) || ($dateFin < $dateDebut)) {
                $this->error(self::DATE_FIN_INF);
                return false;
            }
        }

        return true;
    }

    /**
     * Retourne la valeur saisie
     *
     * @param $context
     * @param $key
     *
     * @return null
     */
    private function getDateValue($context, $key)
    {
        $date = null;
        if (isset($context[$key]) && !empty($context[$key])) {
            $date = \DateTime::createFromFormat(MainConstante::FORMAT_DATE_DEFAUT, $context[$key]);
        }
        return $date;
    }
}
