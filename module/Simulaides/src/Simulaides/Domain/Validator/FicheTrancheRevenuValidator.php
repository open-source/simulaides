<?php
namespace Simulaides\Domain\Validator;

use Zend\I18n\Validator\Int;
use Zend\InputFilter\InputFilter;
use Zend\Validator\GreaterThan;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

/**
 * Classe FicheTrancheRevenuValidator
 * Validateur de la fiche des tranches de revenu
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Validator
 */
class FicheTrancheRevenuValidator extends InputFilter
{
    /**
     * Constructeur du validateur
     */
    public function __construct()
    {
        //Validateur sur le code de la tranche
        $this->add(
            [
                'name'        => 'codeTranche',
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Le code de la tranche doit être renseigné'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Simulaides\Domain\Validator\CodeValidator',
                        'break_chain_on_failure' => true,
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'max'      => 15,
                            'messages' => [
                                StringLength::TOO_LONG => 'La valeur saisie ne doit pas dépasser %max%'
                            ]
                        ]
                    ],
                ]
            ]
        );

        //Validateur sur l'intitulé du groupe
        $this->add(
            [
                'name'        => 'intitule',
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "L'intitulé de la tranche doit être renseigné"
                            ]
                        ]
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'max'      => 150,
                            'messages' => [
                                StringLength::TOO_LONG => 'La valeur saisie ne doit pas dépasser %max%'
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Zend\Filter\StripTags',
                    ]
                ]
            ]
        );

        //Validateur sur les champs montant par personne
        for ($i = 1; $i <= 6; $i++) {
            $this->add($this->getValidatorNPersonne($i));
        }

        //Champ Personne supplémentaires
        $this->add(
            [
                'name'        => 'mtPersonneSupp',
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Le montant doit être renseigné'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Int',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Int::NOT_INT => "La saisie n'est pas numérique"
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'GreaterThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => 0,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Ce nombre doit etre positif.'
                            ]
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * Permet de retourner le validateur appliqué
     * aux champs N personne
     *
     * @param $numPersonne
     *
     * @return array
     */
    private function getValidatorNPersonne($numPersonne)
    {
        $name = 'mt' . $numPersonne . 'personne';
        return [
            'name'        => $name,
            'allow_empty' => false,
            'required'    => true,
            'validators'  => [
                [
                    'name'                   => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options'                => [
                        'messages' => [
                            NotEmpty::IS_EMPTY => 'Le montant doit être renseigné'
                        ]
                    ]
                ],
                [
                    'name'                   => 'Int',
                    'break_chain_on_failure' => true,
                    'options'                => [
                        'messages' => [
                            Int::NOT_INT => "La saisie n'est pas numérique"
                        ]
                    ]
                ],
                [
                    'name'                   => 'GreaterThan',
                    'break_chain_on_failure' => true,
                    'options'                => [
                        'min'      => 0,
                        'messages' => [
                            GreaterThan::NOT_GREATER => 'Ce nombre doit etre positif.'
                        ]
                    ]
                ]
            ]
        ];
    }
}
