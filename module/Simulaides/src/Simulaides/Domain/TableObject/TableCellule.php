<?php
namespace Simulaides\Domain\TableObject;

use Simulaides\Constante\TableConstante;

/**
 * Classe TableCellule
 * Cette table permet de gérer les lignes d'un tableau
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\TableObject
 */
class TableCellule
{
    /**
     * Texte infobulle de la cellule
     * @var string
     */
    private $title;

    /**
     * Type de la cellule (texte, lien, bouton)
     * @var string
     */
    private $typeCellule;

    /**
     * Intitulé à afficher dans la cellule
     * @var string
     */
    private $intitule;

    /**
     * Lien à appliqué dans le cas d'un lien/bouton
     * @var string
     */
    private $lien;

    /**
     * Alignement de la celulle (par défaut à gauche)
     * @var string
     */
    private $align = TableConstante::ALIGN_LEFT;

    /**
     * Indique que la case à cocher est sélectionnée
     * @var bool
     */
    private $isChecked = false;

    /**
     * Constructeur
     *
     * @param null $intitule    Intitulé de la cellule à afficher
     * @param null $typeCellule Type de la cellule
     * @param null $lien        Lien à implémenter sur le libellé
     */
    public function __construct($intitule = null, $typeCellule = null, $lien = null)
    {
        $this->setIntitule($intitule);
        $this->setTypeCellule($typeCellule);
        $this->setLien($lien);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $typeCellule
     */
    public function setTypeCellule($typeCellule)
    {
        $this->typeCellule = $typeCellule;
    }

    /**
     * @return string
     */
    public function getTypeCellule()
    {
        return $this->typeCellule;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param \Zend\Mvc\Controller\Plugin\Url $lien
     */
    public function setLien($lien)
    {
        $this->lien = $lien;
    }

    /**
     * @return string
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * @param string $align
     */
    public function setAlign($align)
    {
        $this->align = $align;
    }

    /**
     * @return string
     */
    public function getAlign()
    {
        return $this->align;
    }

    /**
     * @param boolean $isChecked
     */
    public function setIsChecked($isChecked)
    {
        $this->isChecked = $isChecked;
    }

    /**
     * @return boolean
     */
    public function getIsChecked()
    {
        return $this->isChecked;
    }
}
