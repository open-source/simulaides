<?php
namespace Simulaides\Domain\TableObject;

/**
 * Classe TableLigne
 * Permet de gérer une ligne du tableau
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\TableObject
 */
class TableLigne
{

    /**
     * Identifiant de la ligne
     * @var string
     */
    private $id;

    /**
     * Liste des cellules de la ligne
     * @var array
     */
    private $tabCellule;

    /**
     * constructeur
     */
    public function __construct()
    {
        $this->tabCellule = [];
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param array $tabCellule
     */
    public function setTabCellule($tabCellule)
    {
        $this->tabCellule = $tabCellule;
    }

    /**
     * @return array
     */
    public function getTabCellule()
    {
        return $this->tabCellule;
    }

    /**
     * Ajout d'une cellule dans la liste
     *
     * @param TableCellule $cellule
     */
    public function addCellule($cellule)
    {
        $this->tabCellule[] = $cellule;
    }
}
