<?php
namespace Simulaides\Domain\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Simulaides\Constante\MainConstante;

/**
 * Classe ParametreTechRepository
 * Repository sur les paramètres technique
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class ParametreTechRepository extends EntityRepository
{
    /**
     * Permet de retourner le paramètre technique selon son code
     *
     * @param $codeParamTech
     *
     * @return null|object
     */
    public function getParametreTechParCode($codeParamTech)
    {
        return $this->findOneBy(['code' => $codeParamTech]);
    }

    /**
     * Permet de récupérer les param tech utilisable dans les règles
     * et qui sont liés aux produits du travaux
     *
     * @param string $codeTravaux Code du travaux
     *
     * @return array
     */
    public function getListParamTechUtilisableReglePourCodeTravaux($codeTravaux)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('paramTech');
        $qr->from(MainConstante::ENTITY_PATH . 'ParametreTech', 'paramTech');
        $qr->from(MainConstante::ENTITY_PATH . 'Produit', 'produit');
        $qr->where('produit.code = paramTech.codeProduit');
        $qr->andWhere('paramTech.utilisableRegle = :utilisableRegle');
        $qr->andWhere('produit.codeTravaux = :codeTravaux');

        $qr->setParameter('utilisableRegle', true, Type::BOOLEAN);
        $qr->setParameter('codeTravaux', $codeTravaux);

        return $qr->getQuery()->getResult();
    }
}
