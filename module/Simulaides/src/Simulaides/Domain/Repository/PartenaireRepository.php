<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Classe PartenaireRepository
 * Repository sur les Partenaires
 * @package   Simulaides\Domain\Repository
 */
class PartenaireRepository extends EntityRepository
{
    /**
     * return all partenaires ordered by url
     */
    public function findAll()
    {
        return $this->findBy([],['url'=>'ASC']);
    }
}
