<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Classe RegleCategorieRepository
 * Repository des catégories utilisées pour classer les variables utilisées
 * dans les règles des dispositifs / travaux
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class RegleCategorieRepository extends EntityRepository
{

    /**
     * Permet de retourner la liste des catégories
     * pour les dispositifs
     * @return array
     */
    public function getListeCategoriePourDispositif()
    {
        $qr = $this->createQueryBuilder('categorie');
        $qr->where('categorie.type IN (:tabCateg)');
        $qr->orderBy('categorie.numeroOrdre');

        //Retourne les catégories des dispositifs et de tous (All)
        $qr->setParameter('tabCateg', ["D", "A"]);

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de retourner la liste des catégories
     * pour les travaux
     * @return array
     */
    public function getListeCategoriePourTravaux()
    {
        $qr = $this->createQueryBuilder('categorie');
        $qr->where('categorie.type IN (:tabCateg)');
        $qr->orderBy('categorie.numeroOrdre');

        //Retourne les catégories des travaux et de tous (All)
        $qr->setParameter('tabCateg', ["T", "A"]);

        return $qr->getQuery()->getResult();
    }
}
