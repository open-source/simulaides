<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Entity\SimulationHistorique;

/**
 * Classe SimulationHistoriqueRepository
 * Repository sur l'historique des exécutions des simulations
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class SimulationHistoriqueRepository extends EntityRepository
{
    /**
     * Permet d'ajouter ou de modifier la ligne d'historique de simulation
     *
     * @param SimulationHistorique $simulHisto
     */
    public function saveSimulationHistorique($simulHisto)
    {
        if ($this->getEntityManager()->contains($simulHisto)) {
            //Mise à jour
            $this->getEntityManager()->merge($simulHisto);
        } else {
            //Ajout
            $this->getEntityManager()->persist($simulHisto);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Retourne la ligne de simulation correspondant au projet
     *
     * @param int $idProjet Identifiant du projet
     *
     * @return SimulationHistorique|null
     */
    public function getSimulationHistoriqueParIdProjet($idProjet)
    {
        $qr = $this->createQueryBuilder('simuHisto');
        $qr->where('simuHisto.idProjet = :idProjet');
        $qr->setParameter('idProjet', $idProjet);

        return $qr->getQuery()->getOneOrNullResult();
    }

    /**
     * Permet de retourne le nombre de simulation réalisées par région
     * pour la période indiquée par un utilisateur n'ayant pas un rôle ADEME
     *
     * @param \DateTime $dateDebut Date de début de la période
     * @param \DateTime $dateFin   Date de fin de la période
     *
     * @return array
     */
    public function getNbSimulationParRegion($dateDebut, $dateFin = null)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('COUNT(simulHisto.idProjet) AS nbProjet, region.nom');
        $qr->from(MainConstante::ENTITY_PATH . 'SimulationHistorique', 'simulHisto');
        $qr->join('simulHisto.commune', 'commune');
        $qr->join('commune.departement', 'departement');
        $qr->join('departement.region', 'region');

        if (empty($dateFin)) {
            $qr->where('simulHisto.dateExecution >= :dateDebut');
        } else {
            $swhere = $qr->expr()->andX();
            $swhere->add('simulHisto.dateExecution >= :dateDebut');
            $swhere->add('simulHisto.dateExecution <= :dateFin');
            $qr->where($swhere);
        }
        $qr->andWhere('simulHisto.simulAdmin = 0');
        $qr->setParameter('dateDebut', $dateDebut);
        if (!empty($dateFin)) {
            $qr->setParameter('dateFin', $dateFin);
        }

        $qr->groupBy('region.nom');
        $qr->orderBy('region.nom');

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de retourne le nombre de simulation réalisées par région
     * pour la période indiquée par un utilisateur n'ayant pas un rôle ADEME
     *
     * @param \DateTime $dateDebut Date de début de la période
     * @param \DateTime $dateFin   Date de fin de la période
     *
     * @return array
     */
    public function getNbSimulationParPartenaire($dateDebut, $dateFin = null)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('COUNT(simulHisto.idProjet) AS nbProjet, partenaire.id, partenaire.nom, partenaire.url');
        $qr->from(MainConstante::ENTITY_PATH . 'SimulationHistorique', 'simulHisto');
        $qr->join('simulHisto.partenaire', 'partenaire');

        if (empty($dateFin)) {
            $qr->where('simulHisto.dateExecution >= :dateDebut');
        } else {
            $swhere = $qr->expr()->andX();
            $swhere->add('simulHisto.dateExecution >= :dateDebut');
            $swhere->add('simulHisto.dateExecution <= :dateFin');
            $qr->where($swhere);
        }
        $qr->andWhere('simulHisto.simulAdmin = 0');
        $qr->setParameter('dateDebut', $dateDebut);
        if (!empty($dateFin)) {
            $qr->setParameter('dateFin', $dateFin);
        }

        $qr->groupBy('partenaire.id');
        $qr->orderBy('partenaire.nom');

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de retourne le nombre de simulation réalisées par région
     * pour la période indiquée par un utilisateur n'ayant pas un rôle ADEME
     *
     * @param \DateTime $dateDebut Date de début de la période
     * @param \DateTime $dateFin   Date de fin de la période
     *
     * @return array
     */
    public function getNbSimulationParEPCI($dateDebut, $dateFin = null, $departement=null)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('AVG(simulHisto.montant) AS montantMoyen, AVG(simulHisto.taux) AS tauxMoyen, COUNT(simulHisto.idProjet) AS nbProjet, epci.nom');
        $qr->from(MainConstante::ENTITY_PATH . 'SimulationHistorique', 'simulHisto');
        $qr->join('simulHisto.commune', 'commune');
        $qr->join('commune.epcis', 'epci');
        if($departement){
            $qr->join('commune.departement', 'departement');
        }

        if (empty($dateFin)) {
            $qr->where('simulHisto.dateExecution >= :dateDebut');
        } else {
            $swhere = $qr->expr()->andX();
            $swhere->add('simulHisto.dateExecution >= :dateDebut');
            $swhere->add('simulHisto.dateExecution <= :dateFin');
            $qr->where($swhere);
        }
        $qr->andWhere('simulHisto.simulAdmin = 0');
        if($departement){
            $qr->andWhere('departement.code = :codeDepartement');
        }

        $qr->setParameter('dateDebut', $dateDebut);
        if($departement) {
            $qr->setParameter('codeDepartement', $departement);
        }

        if (!empty($dateFin)) {
            $qr->setParameter('dateFin', $dateFin);
        }

        $qr->groupBy('epci.code');
        $qr->orderBy('epci.nom');

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de retourne le nombre de simulation réalisées par région
     * pour la période indiquée par un utilisateur n'ayant pas un rôle ADEME
     *
     * @param \DateTime $dateDebut Date de début de la période
     * @param \DateTime $dateFin   Date de fin de la période
     *
     * @return array
     */
    public function getNbSimulationNational($dateDebut, $dateFin = null)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('AVG(simulHisto.montant) AS montantMoyen, AVG(simulHisto.taux) AS tauxMoyen');
        $qr->from(MainConstante::ENTITY_PATH . 'SimulationHistorique', 'simulHisto');

        if (empty($dateFin)) {
            $qr->where('simulHisto.dateExecution >= :dateDebut');
        } else {
            $swhere = $qr->expr()->andX();
            $swhere->add('simulHisto.dateExecution >= :dateDebut');
            $swhere->add('simulHisto.dateExecution <= :dateFin');
            $qr->where($swhere);
        }

        $qr->andWhere('simulHisto.simulAdmin = 0');
        $qr->setParameter('dateDebut', $dateDebut);

        if (!empty($dateFin)) {
            $qr->setParameter('dateFin', $dateFin);
        }

        return $qr->getQuery()->getResult();
    }
}
