<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\Conseiller;

/**
 * Classe ConseillerRepository
 *
 * Projet : Simul'Aides

 *
 * @author
 * @package   Simulaides\Domain\Repository
 */
class ConseillerRepository extends EntityRepository
{
    /**
     * Permet de faire la modification ou l'ajout de l'entité
     *
     * @param Conseiller $conseiller
     */
    public function save($conseiller)
    {
        if ($this->getEntityManager()->contains($conseiller)) {
            //Mise à jour
            $this->getEntityManager()->merge($conseiller);
        } else {
            //Ajout
            $this->getEntityManager()->persist($conseiller);
        }
        $this->getEntityManager()->flush();
    }
}
