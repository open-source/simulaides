<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 18/02/15
 * Time: 11:56
 */

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\Commune;
use Simulaides\Constante\MainConstante;

/**
 * Class CommuneRepository
 * @package Simulaides\Domain\Repository
 */
class CommuneRepository extends EntityRepository
{
    /**
     * @return CodePostalRepository
     */
    public function getCodePostalRepository()
    {
        return $this->entityManager->getRepository('Simulaides\Domain\Entity\CodePostal');
    }

    /**
     * @param $codeDepartement
     *
     * @return array
     */
    public function getCommunesPourDepartement($codeDepartement)
    {
        return $this->findBy(['codeDepartement' => $codeDepartement], ['nom' => 'ASC']);
    }

    /**
     * Retourne la liste des communes trié sur le nom pour le code postal et le département indiqué
     *
     * @param $codePostal
     *
     * @return array
     */
    public function getCommunesPourCodePostal($codePostal, $codeRegion = null)
    {
        $qr = $this->createQueryBuilder('commune');
        $qr->leftJoin(MainConstante::ENTITY_PATH . 'CodePostal', 'cp', 'WITH', 'cp.codeInsee = commune.codeInsee');
        $qr->leftJoin('commune.departement', 'dpt');


        if(!empty($codePostal)){
            $qr->where('cp.codePostal = :codeP');
            if(!empty($codeRegion)){
                $qr->andWhere('dpt.codeRegion = :codeRegion');
            }
        }else{
            if(!empty($codeRegion)){
                $qr->where('dpt.codeRegion = :codeRegion');
            }
        }


        $qr->orderBy('commune.nom');

        if(!empty($codePostal)){
            $qr->setParameter('codeP', $codePostal);
        }
        if(!empty($codeRegion)){
            $qr->setParameter('codeRegion', $codeRegion);
        }

        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne la liste des communes trié sur le nom pour les départements indiqués
     *
     * @param $tabCodeDepartement
     *
     * @return array
     */
    public function getCommunesPourListeDepartement($tabCodeDepartement=null)
    {
        $qr = $this->createQueryBuilder('commune');
        $qr->where('commune.codeDepartement IN (:tabCodeDepartement)');
        $qr->orderBy('commune.nom');
        $qr->setParameter('tabCodeDepartement', $tabCodeDepartement);

        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne la liste des communes trié sur le nom pour les départements indiqués
     *
     * @param $codeDepartement
     *
     * @return array
     */
    public function getCommunesFiltres($codeDepartement=null)
    {
        if (!empty($codeDepartement)) {
            //Sélection de toutes les communes
            return $this->findBy(['codeDepartement' => $codeDepartement], ['nom' => 'ASC']);
        } else {
            return [];
        }
    }

    /**
     * Retourne la liste des communes trié sur le nom pour les régions indiquées
     *
     * @param $tabCodeRegion
     *
     * @return array
     */
    public function getCommunesPourListeRegion($tabCodeRegion)
    {
        $qr = $this->createQueryBuilder('commune');
        $qr->leftJoin('commune.departement', 'dpt');
        $qr->where('dpt.codeRegion IN (:tabCodeRegion)');
        $qr->orderBy('commune.nom');
        $qr->setParameter('tabCodeRegion', $tabCodeRegion);

        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne la commune à partir de son code
     *
     * @param string $codeCommune Code de la commune
     *
     * @return null|Commune
     */
    public function getCommuneParCode($codeCommune)
    {
        return $this->findOneBy(['codeInsee' => $codeCommune]);
    }
}
