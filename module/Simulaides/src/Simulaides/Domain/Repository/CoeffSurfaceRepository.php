<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Classe CoeffSurfaceRepository
 * Repository sur l'entité CoeffSurface
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class CoeffSurfaceRepository extends EntityRepository
{

    /**
     * Permet de retourner le coefficient de surface
     * pour un type de logement et un code surface
     *
     * @param string $codeTypeLogement Code du type de logement
     * @param string $codeSurface      Code de la surface
     * @param        $codeTravaux
     *
     * @return mixed
     */
    public function getCoefficientSurfacePourTypeLogement($codeTypeLogement, $codeSurface, $codeTravaux)
    {
        $qr = $this->createQueryBuilder('coeffSurf');
        $qr->select('coeffSurf.coefficient');
        $qr->where('coeffSurf.codeTypeLogement = :codeTypeLogement');
        $qr->andWhere('coeffSurf.codeSurface = :codeSurface');

        if (!empty($codeTravaux)) {
            $qr->andWhere('coeffSurf.codeTravaux = :codeTravaux');
            $qr->setParameter('codeTravaux', $codeTravaux);
        } else {
            $qr->andWhere('coeffSurf.codeTravaux is null');
        }

        $qr->setParameter('codeTypeLogement', $codeTypeLogement);
        $qr->setParameter('codeSurface', $codeSurface);

        return $qr->getQuery()->getOneOrNullResult();
    }
}
