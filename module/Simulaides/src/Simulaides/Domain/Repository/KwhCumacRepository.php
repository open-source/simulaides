<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Classe KwhCumacRepository
 * Repository sur l'entité KwhCumac
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class KwhCumacRepository extends EntityRepository
{
    /**
     * Fournit le nombre de kwh_cumac des données : code du travaux, caracteristique,
     * type du logement,énergie, mode de chauffage et la zone climatique
     *
     * @param string $codeTravaux       Code du travaux
     * @param string $caracteristique   Caractéristique
     * @param string $codeTypeLogement  Code du type de logement
     * @param string $codeEnergie       Code du type d'énergie
     * @param string $codeModeChauffage Code du type de chauffage
     * @param string $zone              Zone
     *
     * @return mixed
     */
    public function getKwhCumacpourCodeTravaux(
        $codeTravaux,
        $caracteristique,
        $codeTypeLogement,
        $codeEnergie,
        $codeModeChauffage,
        $zone
    ) {
        $qr = $this->createQueryBuilder('kwhCumac');
        $qr->select('kwhCumac.nbKwh');
        $qr->where('kwhCumac.codeTravaux = :codeTravaux');
        if (!empty($caracteristique)) {
            $qr->andWhere('kwhCumac.caracteristique = :caracteristique');
            $qr->setParameter('caracteristique', $caracteristique);
        }
        if (!empty($codeTypeLogement)) {
            $qr->andWhere('kwhCumac.codeTypeLogement = :codeTypeLogement');
            $qr->setParameter('codeTypeLogement', $codeTypeLogement);
        }
        if (!empty($codeEnergie)) {
            $qr->andWhere('kwhCumac.codeEnergie = :codeEnergie');
            $qr->setParameter('codeEnergie', $codeEnergie);
        }
        if (!empty($codeModeChauffage)) {
            $qr->andWhere('kwhCumac.codeModeChauffage = :codeModeChauffage');
            $qr->setParameter('codeModeChauffage', $codeModeChauffage);
        }
        if (!empty($zone)) {
            $qr->andWhere('kwhCumac.zone = :zone');
            $qr->setParameter('zone', $zone);
        }
        $qr->setParameter('codeTravaux', $codeTravaux);

        return $qr->getQuery()->getOneOrNullResult();
    }
}
