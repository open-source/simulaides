<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 13:49
 */

namespace Simulaides\Domain\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Entity\Projet;

/**
 * Classe ProjetRepository
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class ProjetRepository extends EntityRepository
{
    /**
     * Permet de retourner le projet de simulation pour l'Id et le mot de passe
     *
     * @param int    $numero    Identifiant
     * @param string $codeAcces Mot de passe crypté
     *
     * @return Projet
     */
    public function getProjetParNumeroEtCodeAcces($numero, $codeAcces)
    {
        $qr = $this->createQueryBuilder('projet');
        $qr->where('projet.numero = :numero');
        $qr->andWhere('projet.motPasse = :codeAcces');

        $qr->setParameter('numero', $numero);
        $qr->setParameter('codeAcces', $codeAcces);

        return $qr->getQuery()->getOneOrNullResult();
    }

    /**
     * Permet de faire la modification ou l'ajout du projet
     *
     * @param Projet $projet
     */
    public function saveProjet($projet)
    {
        $idProjet = $projet->getId();

        if (!empty($idProjet)) {
            //Mise à jour
            $this->getEntityManager()->merge($projet);
        } else {
            //Ajout
            $this->getEntityManager()->persist($projet);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Retourne le projet pour l'ID
     *
     * @param int $idProjet Identifiant du projet
     *
     * @return Projet|null
     */
    public function getProjetParId($idProjet)
    {
        /* @var $projet Projet */
        $projet = $this->findOneBy(['id' => $idProjet]);
        return $projet;
    }

    /**
     * Permet de supprimer plusieurs projet
     *
     * @param array $tabIdProjet Liste des identifiants des projets à supprimer
     */
    public function supprimeListeProjet($tabIdProjet)
    {
        $qr = $this->createQueryBuilder('projet');
        $qr->delete(MainConstante::ENTITY_PATH . 'Projet', 'projet');
        $qr->where('projet.id IN (:tabIdProjet)');

        $qr->setParameter('tabIdProjet', $tabIdProjet);

        $qr->getQuery()->execute();
    }

    /**
     * Permet de sélectionner tous les projets dont la date d'évaluation
     * est antérieure à la date indiquée
     *
     * @param \DateTime $date Date à prendre en compte
     *
     * @return array
     */
    public function getIdProjetAvantDate($date)
    {
        $qr = $this->createQueryBuilder('projet');
        $qr->select('projet.id');
        $qr->where('projet.dateEval < :dateEval');

        $qr->setParameter('dateEval', $date, Type::DATETIME);

        return $qr->getQuery()->getResult();
    }
}
