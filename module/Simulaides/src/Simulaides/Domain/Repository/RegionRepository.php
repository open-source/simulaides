<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 17:54
 */

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Entity\Region;

/**
 * Class RegionRepository
 * @package Simulaides\Domain\Repository
 */
class RegionRepository extends EntityRepository
{
    /**
     * Retourne la liste des régions, triée sur le nom
     *
     * @return array
     */
    public function getListeRegions()
    {
        return $this->findBy([], ['nom' => 'ASC']);
    }

    /**
     * Retourne la région selon le code indiqué
     *
     * @param string $codeRegion Code de la région
     *
     * @return null|Region
     */
    public function getRegionParCode($codeRegion)
    {
        return $this->findOneBy(['code' => $codeRegion]);
    }

    /**
     * Récupère la liste des régions pour les codes indiqués en entrée
     *
     * @param $tabCodeRegion
     *
     * @return array
     */
    public function getRegionsParListeCodeRegion($tabCodeRegion)
    {
        $qr = $this->createQueryBuilder('region');
        $qr->where('region.code IN (:tabCodeRegion)');
        $qr->setParameter('tabCodeRegion', $tabCodeRegion);

        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne la liste des régions contenant les départements indiqués
     *
     * @param array $tabCodeDept Liste des codes départements
     *
     * @return array
     */
    public function getRegionsParListeCodeDepartement($tabCodeDept)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('region');

        $qr->from(MainConstante::ENTITY_PATH . 'Region', 'region');
        $qr->from(MainConstante::ENTITY_PATH . 'Departement', 'departement');

        $qr->where('departement.codeRegion = region.code');
        $qr->andWhere('departement.code IN (:tabCodeDept)');

        $qr->setParameter('tabCodeDept', $tabCodeDept);

        return $qr->getQuery()->getResult();

    }

    /**
     * Retourne la liste des régions contenant les communes des EPCI indiquées
     *
     * @param array $tabCodeEpci Liste des codes EPCI
     *
     * @return array
     */
    public function getRegionsParListeCodeEpci($tabCodeEpci)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('region');
        $qr->from(MainConstante::ENTITY_PATH . 'Region', 'region');
        $qr->from(MainConstante::ENTITY_PATH . 'Departement', 'departement');
        $qr->from(MainConstante::ENTITY_PATH . 'Epci', 'epci');
        $qr->join('epci.communes', 'commune');

        $qr->where('departement.codeRegion = region.code');
        $qr->andWhere('commune.codeDepartement = departement.code');
        $qr->andWhere('epci.code IN (:tabCodeEcpi)');

        $qr->setParameter('tabCodeEcpi', $tabCodeEpci);

        return $qr->getQuery()->getResult();

    }

    /**
     * Retourne la liste des régions contenant les communes indiquées
     *
     * @param array $tabCodeComune Liste des codes Communes
     *
     * @return array
     */
    public function getRegionsParListeCodeCommune($tabCodeComune)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('region');
        $qr->from(MainConstante::ENTITY_PATH . 'Region', 'region');
        $qr->from(MainConstante::ENTITY_PATH . 'Departement', 'departement');
        $qr->from(MainConstante::ENTITY_PATH . 'Commune', 'commune');

        $qr->where('departement.codeRegion = region.code');
        $qr->andWhere('commune.codeDepartement = departement.code');
        $qr->andWhere('commune.codeInsee IN (:tabCodeCommune)');

        $qr->setParameter('tabCodeCommune', $tabCodeComune);

        return $qr->getQuery()->getResult();

    }

    /**
     * @param string $codeCommune
     *
     * @return Region
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getRegionByCodeCommune($codeCommune)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('region');
        $qr->from(MainConstante::ENTITY_PATH . 'Region', 'region');
        $qr->from(MainConstante::ENTITY_PATH . 'Departement', 'departement');
        $qr->from(MainConstante::ENTITY_PATH . 'Commune', 'commune');

        $qr->where('departement.codeRegion = region.code');
        $qr->andWhere('commune.codeDepartement = departement.code');
        $qr->andWhere('commune.codeInsee = :codeCommune');

        $qr->setParameter('codeCommune', $codeCommune);

        return $qr->getQuery()->getOneOrNullResult();
    }
}
