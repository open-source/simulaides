<?php
namespace Simulaides\Domain\Repository;

use DateTime;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Logger\LoggerSimulation;

/**
 * Classe DispositifRepository
 * Repository sur les dispositifs
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class DispositifRepository extends EntityRepository
{

    /**
     * Retourne la liste des dispositifs appartenant à la région
     * et triée sur le numéro d'ordre dans la région
     *
     * @param string $codeRegion Code de la région
     * @param boolean $validAdate
     *
     * @return array
     */
    public function getDispositifParCodeRegion($codeRegion,$validAdate=false)
    {
        $qr = $this->_em->createQueryBuilder();

        $qr->select('dispositif');

        $qr->from(MainConstante::ENTITY_PATH . 'Dispositif', 'dispositif');
        $qr->from(MainConstante::ENTITY_PATH . 'Groupe', 'groupe');

        $qr->where('dispositif.id = groupe.idDispositif');
        $qr->andWhere('groupe.codeRegion = :codeRegion');
        if($validAdate){
            $dateJour = new DateTime();
            $qr->andWhere('dispositif.debutValidite <= \''.$dateJour->format('Y-m-d').'\'');
            $qr->andWhere(
                $qr->expr()->orX($qr->expr()->isNull('dispositif.finValidite'),'dispositif.finValidite >= \''
                                                                               .$dateJour->format('Y-m-d').'\'')
            );

        }

        $qr->orderBy('groupe.numeroOrdre');

        $qr->setParameter('codeRegion', $codeRegion);

        return $qr->getQuery()->getResult();
    }

    /**
     * Recherche les dispositifs
     *
     * @param string $codeRegion Code de la région
     *
     * @return array
     */
    public function searchDispositif($codeRegion, $intitule, $financeur, $typeDispositif, $etatDispositif, $validite,
        $codeTravaux,$codeDepartement,$codeCommune)
    {
        $qr = $this->_em->createQueryBuilder();

        $qr->select('dispositif');

        $qr->from(MainConstante::ENTITY_PATH . 'Dispositif', 'dispositif');
        $qr->from(MainConstante::ENTITY_PATH . 'Groupe', 'groupe');

        $qr->where('dispositif.id = groupe.idDispositif');
        $qr->andWhere('groupe.codeRegion = :codeRegion');
        $qr->setParameter('codeRegion', $codeRegion);


        $qr->orderBy('groupe.numeroOrdre');

        if($codeDepartement || $codeCommune){
            $qr->from(MainConstante::ENTITY_PATH . 'PerimetreGeo', 'PerimetreGeo');
            $qr->andWhere('dispositif.id = PerimetreGeo.idDispositif');
            if($codeCommune){
                $qr->andWhere('PerimetreGeo.codeCommune = :codeCommune');
                $qr->setParameter('codeCommune', $codeCommune);

            }else{
                $qr->andWhere('PerimetreGeo.codeDepartement = :codeDepartement');
                $qr->setParameter('codeDepartement', $codeDepartement);
            }

        }


        if(null !== $codeTravaux && '' !== $codeTravaux){
            $qr->from(MainConstante::ENTITY_PATH . 'DispositifTravaux', 'DispoTrav');
            $qr->andWhere('dispositif.id = DispoTrav.idDispositif');
            $qr->andWhere('DispoTrav.codeTravaux = :codeTravaux');
            $qr->setParameter('codeTravaux', $codeTravaux);
        }


        if (null !== $intitule && '' !== $intitule) {
            $qr->andWhere('dispositif.intitule like :intitule');
            $qr->setParameter('intitule', '%' . $intitule . '%');
        }
        if (null !== $financeur && '' !== $financeur) {
            $qr->andWhere('dispositif.financeur like :financeur');
            $qr->setParameter('financeur', '%' . $financeur . '%');
        }
        if (null !== $typeDispositif && '' !== $typeDispositif) {
            $qr->andWhere('dispositif.codeTypeDispositif = :codeType');
            $qr->setParameter('codeType', $typeDispositif);
        }
        if (null !== $etatDispositif && '' !== $etatDispositif) {
            $qr->andWhere('dispositif.codeEtat = :codeEtat');
            $qr->setParameter('codeEtat', $etatDispositif);
        }
        if (null !== $validite && $validite) {
            $qr->andWhere(
                "dispositif.debutValidite <= :now AND (:now <= dispositif.finValidite OR dispositif.finValidite = '0000-00-00' OR dispositif.finValidite IS NULL)"
            );
            $qr->setParameter('now', new \DateTime(), Type::DATETIME);
        }


        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne le dispositif correspondant à l'id
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return null|object
     */
    public function getDispositifParId($idDispositif)
    {
        return $this->findOneBy(['id' => $idDispositif]);
    }

    /**
     * Retourne les dispositifs correspondant aux ids
     *
     * @param array $ids
     *
     * @return array
     */
    public function getListDispositifByIds(array $ids)
    {
        return $this->findBy(['id' => $ids]);
    }

    /**
     * Permet de faire la modification ou l'ajout du dispositif
     *
     * @param Dispositif $dispositif
     *
     * @throws OptimisticLockException
     */
    public function saveDispositif($dispositif)
    {
        if ($this->getEntityManager()->contains($dispositif)) {
            //Mise à jour
            $this->getEntityManager()->merge($dispositif);
        } else {
            //Ajout
            $this->getEntityManager()->persist($dispositif);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Permet de supprimer le dispositif
     *
     * @param Dispositif $dispositif Dispositif à supprimer
     *
     * @throws OptimisticLockException
     */
    public function deleteDispositif($dispositif)
    {
        if ($this->getEntityManager()->contains($dispositif)) {
            $this->getEntityManager()->remove($dispositif);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Permet de récupérer les dispositifs candidats
     *
     * @param $codeCommune
     * @param $idProjet
     * @param $modeExecution
     * @param $tabIdDispositif
     * @param $offresCdpSeulement
     * @return array
     * @throws DBALException
     */
    public function getDispositifCandidat($codeCommune, $idProjet, $modeExecution, $tabIdDispositif, $offresCdpSeulement = false)
    {
        // Recherche des dispositifs pour lesquels :
        // - la commune du particulier appartient au périmètre géographique,
        // - Ils appartiennent au groupe ddéfinit par la région de la commune du particulier
        // - Il existe au moins un travaux du projet pris en charge par le dispositif
        // Si l'on est en mode TEST
        // - ils appartiennet à la liste des dispositifs sélectionnés par l'opérateur
        // Si l'on est en mode Production
        // - Ils sont dans l'état ACTIVE
        // - Ils sont dans leur période de validité cours de
        // Ces dispositifs sont classés par numéro d'ordre

        $req = "SELECT gro.numero_ordre, d.id, d.code_etat, d.code_type_dispositif, d.intitule,
                    d.descriptif, d.site_web, ";
        if ($offresCdpSeulement) {
            $req .= " d.site_web, o.nom AS obligeNom, o.mail AS obligeMail, o.siren AS obligeSiren, o.telephone AS obligeTelephone, o.url AS obligeUrl, ";
        };
        $req = $req . " d.debut_validite, d.financeur, d.exclut_cee, d.exclut_epci, d.exclut_anah, d.exclut_region, d.exclut_communal,d.exclut_coup_de_pouce,
                        d.exclut_national, d.exclut_departement, d.evaluer_regle, d.id_dispositif_requis FROM dispositif d ";
        $req = $req . " INNER JOIN ";
        $req = $req . "(  SELECT din.id FROM dispositif din WHERE din.type_perimetre = :typePerimetreNational ";
        $req = $req . " UNION ";
        $req = $req . " SELECT dir.id FROM dispositif dir ";
        $req = $req . " INNER JOIN perimetre_geo geo ON geo.id_dispositif = dir.id ";
        $req = $req . " INNER JOIN departement dep ON dep.code_region = geo.code_region ";
        $req = $req . " INNER JOIN commune com ON com.code_departement = dep.code
                        AND com.code_insee = :codeCommune ";
        $req = $req . " WHERE dir.type_perimetre = :typePerimetreRegional ";
        $req = $req . " UNION ";
        $req = $req . " SELECT did.id FROM dispositif did ";
        $req = $req . " INNER JOIN perimetre_geo geo ON geo.id_dispositif = did.id ";
        $req = $req . " INNER JOIN departement dep ON dep.code = geo.code_departement ";
        $req = $req . " INNER JOIN commune com ON com.code_departement = dep.code
                        AND com.code_insee = :codeCommune ";
        $req = $req . " WHERE did.type_perimetre = :typePerimetreDepartemental ";
        $req = $req . " UNION ";
        $req = $req . " SELECT die.id FROM dispositif die ";
        $req = $req . " INNER JOIN perimetre_geo geo ON geo.id_dispositif = die.id ";
        $req = $req . " INNER JOIN epci_commune eco ON eco.code_epci = geo.code_epci
                        AND eco.code_commune = :codeCommune ";
        $req = $req . " WHERE die.type_perimetre = :typePerimetreEpci ";
        $req = $req . " UNION ";
        $req = $req . " SELECT dic.id FROM dispositif dic ";
        $req = $req . " INNER JOIN perimetre_geo geo ON geo.id_dispositif = dic.id
                        AND geo.code_commune = :codeCommune ";
        $req = $req . " WHERE dic.type_perimetre = :typePerimetreCommunal ";
        $req = $req . " ) critgeo ON critgeo.id = d.id ";
        if ($offresCdpSeulement) {
            $req .= " INNER JOIN oblige o ON o.id = d.id_oblige ";
        };
        $req = $req . " INNER JOIN groupe gro ON gro.id_dispositif = d.id ";
        $req = $req . " INNER JOIN departement dep ON dep.code_region = gro.code_region ";
        $req = $req . " INNER JOIN commune com ON com.code_departement = dep.code
                        AND com.code_insee = :codeCommune ";
        $req = $req . " WHERE EXISTS ";
        $req = $req . " 	(SELECT dtr.id_dispositif FROM dispositif_travaux dtr ";
        $req = $req . " 	INNER JOIN travaux_projet trp ON trp.code_travaux = dtr.code_travaux
                            AND trp.id_projet = :idProjet ";
        $req = $req . " 	WHERE dtr.id_dispositif = d.id ) ";

        if ($modeExecution == SimulationConstante::MODE_TEST) {
            if(!$tabIdDispositif){
                return [];
            }
            $strIdDispositifSelectionnes = implode(",", $tabIdDispositif);
            $req                         = $req . " AND d.id IN ( " . $strIdDispositifSelectionnes . " ) ";
        }
        if ($modeExecution == SimulationConstante::MODE_PRODUCTION) {
            $req = $req . " AND d.debut_validite <= now() and (d.fin_validite is null or d.fin_validite > now()) ";
            $req = $req . " AND d.code_etat = 'ACTIVE' ";
        }
        if ($offresCdpSeulement) {
            $req .= " AND d.code_type_dispositif IN ('COUP_DE_POUCE', 'CEE') ";
        }
        $req = $req . " ORDER BY gro.numero_ordre, d.intitule ; ";

        $oConnexion = $this->_em->getConnection();
        $oStatement = $oConnexion->prepare($req);
        $oStatement->bindValue('codeCommune', $codeCommune);
        $oStatement->bindValue('idProjet', $idProjet);
        $oStatement->bindValue('typePerimetreNational', ValeurListeConstante::TYPE_PERIM_GEO_NATIONAL);
        $oStatement->bindValue('typePerimetreRegional', ValeurListeConstante::TYPE_PERIM_GEO_REGIONAL);
        $oStatement->bindValue('typePerimetreDepartemental', ValeurListeConstante::TYPE_PERIM_GEO_DEPARTEMENTAL);
        $oStatement->bindValue('typePerimetreCommunal', ValeurListeConstante::TYPE_PERIM_GEO_COMMUNAL);
        $oStatement->bindValue('typePerimetreEpci', ValeurListeConstante::TYPE_PERIM_GEO_EPCI);
        $oStatement->execute();

        $reqComplete = str_replace(':codeCommune', "'$codeCommune'", $req);
        $reqComplete = str_replace(':idProjet', "'$idProjet'", $reqComplete);
        $reqComplete = str_replace(':typePerimetreNational', "'N'", $reqComplete);
        $reqComplete = str_replace(':typePerimetreRegional', "'R'", $reqComplete);
        $reqComplete = str_replace(':typePerimetreDepartemental', "'R'", $reqComplete);
        $reqComplete = str_replace(':typePerimetreCommunal', "'C'", $reqComplete);
        $reqComplete = str_replace(':typePerimetreEpci', "'E'", $reqComplete);
        LoggerSimulation::detail(
            "\n============================================================================================"
        );
        LoggerSimulation::detail("\n\n Requête de selection des dispositifs à traîter \n\n" . $reqComplete);
        LoggerSimulation::detail(
            "\n============================================================================================"
        );
        $tabresult = $oStatement->fetchAll();
        return $tabresult;
    }

    /**
     * Permet de sélectionner tous les dispositif actif dont la date de fin de validité est dépassé
     *
     * @param \DateTime $date Date à prendre en compte
     * @param string $etat etat pris en compte
     *
     * @return array
     */
    public function getIdDispositifByDateDepassee($date,$etat = null)
    {
        $qr = $this->createQueryBuilder('dispositif');
        $qr->select('dispositif.id');
        $qr->where('dispositif.finValidite < :dateEval');

        if($etat != null){
            $qr->andWhere('dispositif.etat = :Etat');
            $qr->setParameter('Etat',$etat);
        }
        $qr->setParameter('dateEval', $date, Type::DATETIME);
        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de sélectionner tous les dispositif actif dont la date de fin de validité est dépassé
     *
     * @param $codeRegion
     * @param $ordreMax
     *
     * @return array
     */
    public function getListeDispositifRequis($codeRegion = null, $ordreMax = null)
    {

        $qr = $this->_em->createQueryBuilder();
        $qr->select('dispositif');

        $qr->from(MainConstante::ENTITY_PATH . 'Dispositif', 'dispositif');
        $qr->from(MainConstante::ENTITY_PATH . 'Groupe', 'groupe');

        $qr->where('dispositif.id = groupe.idDispositif');

        if($codeRegion){
            $qr->andWhere('groupe.codeRegion = :codeRegion');
            $qr->setParameter('codeRegion', $codeRegion);

        }

        if($ordreMax){
            $qr->andWhere('groupe.numeroOrdre < :numeroOrdre');
            $qr->setParameter('numeroOrdre', $ordreMax);

        }

        $qr->orderBy('groupe.numeroOrdre, dispositif.intitule');

        return $qr->getQuery()->getResult();
    }


}
