<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Entity\DispTravauxRegle;

/**
 * Classe DispTravauxRegleRepository
 * Repository sur les règles d'un travaux d'un dispositif
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class DispTravauxRegleRepository extends EntityRepository
{
    /**
     * Retourne la règle du travaux dans le dispositif par sont Id
     *
     * @param $idDispTravauxRegle
     *
     * @return null|DispTravauxRegle
     */
    public function getDispTravauxRegleParId($idDispTravauxRegle)
    {
        return $this->findOneBy(['id' => $idDispTravauxRegle]);
    }

    /**
     * Permet de supprimer les règles de travaux de dispositif indiqué
     *
     * @param array $tabIdDispTravaux Liste des identifiants des dispositif travaux
     */
    public function deleteDispTravauxRegleParDispositifTravaux($tabIdDispTravaux)
    {
        $qr = $this->createQueryBuilder('dispTravauxRegle');
        $qr->delete(MainConstante::ENTITY_PATH . 'DispTravauxRegle', 'dispTravauxRegle');
        $qr->where('dispTravauxRegle.dispTravaux IN (:tabIdDispTravaux)');
        $qr->setParameter('tabIdDispTravaux', $tabIdDispTravaux);

        $qr->getQuery()->execute();
    }

    /**
     * Permet de sauvegarder une règle de dispositif
     *
     * @param $regle
     */
    public function saveDispositifTravauxRegle($regle)
    {
        if ($this->getEntityManager()->contains($regle)) {
            //Mis eà jour
            $this->getEntityManager()->merge($regle);
        } else {
            //Création
            $this->getEntityManager()->persist($regle);
        }
        $this->getEntityManager()->flush();
    }
}
