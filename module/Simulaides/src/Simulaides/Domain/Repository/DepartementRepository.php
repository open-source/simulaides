<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 17/02/15
 * Time: 15:57
 */

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Constante\MainConstante;

/**
 * Class DepartementRepository
 * @package Simulaides\Domain\Repository
 */
class DepartementRepository extends EntityRepository
{
    /**
     * Retourne la liste des départements trié sur le nom d'une région
     *
     * @param $codeRegion
     *
     * @return array
     */
    public function getDepartementsPourRegion($codeRegion)
    {
        return $this->findBy(['codeRegion' => $codeRegion], ['nom' => 'ASC']);
    }



    /**
     * Retourne la liste des départements trié sur le nom pour les régions indiquées
     *
     * @param array $tabCodeRegion Liste des codes région
     *
     * @return array
     */
    public function getDepartementsPourListeRegion($tabCodeRegion)
    {
        $qr = $this->createQueryBuilder('departement');
        if (!empty($tabCodeRegion)) {
            $qr->where('departement.codeRegion IN (:tabCodeRegion)');
            $qr->setParameter('tabCodeRegion', $tabCodeRegion);
        }
        $qr->orderBy('departement.nom');

        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne la liste ds départements triée sur le nom
     *  -> de la france si tabCodeRegion est vide
     *  -> des régions si tabCodeRegion est renseigné
     *
     * @param null|Array $tabCodeRegion Liste des codes région
     *
     * @return array
     */
    public function getDepartements($tabCodeRegion = null)
    {
        if (empty($codeRegion)) {
            //Sélection de tous les départements
            return $this->findBy([], ['nom' => 'ASC']);
        } else {
            return $this->getDepartementsPourListeRegion($tabCodeRegion);
        }
    }

    /**
     * Retourne la liste ds départements triée sur le nom
     *  -> de la france si tabCodeRegion est vide
     *  -> des régions si tabCodeRegion est renseigné
     *
     * @param null|string $codeRegion code de la region
     *
     * @return array|null
     */
    public function getDepartementsFiltres($codeRegion = null)
    {
        if (!empty($codeRegion)) {
            //Sélection de tous les départements
            return $this->findBy(['codeRegion' => $codeRegion], ['nom' => 'ASC']);
        } else {
            return [];
        }
    }

    /**
     * Permet de retourner les départements des communes indiquées
     *
     * @param array|null $tabCommune Liste des codes INSEE des communes
     *
     * @return array
     */
    public function getDepartementsPourListeCommune($tabCommune=null)
    {
       if($tabCommune){
           $qr = $this->_em->createQueryBuilder();
           $qr->select('departement');

           $qr->from(MainConstante::ENTITY_PATH . 'Departement', 'departement');
           $qr->from(MainConstante::ENTITY_PATH . 'Commune', 'commune');

           $qr->where('commune.codeDepartement = departement.code');
           $qr->andWhere('commune.codeInsee IN (:tabCommune)');

           $qr->setParameter('tabCommune', $tabCommune);

           return $qr->getQuery()->getResult();
       }
       return null;

    }

    /**
     * Permet de retourner les départements des communes des Epci
     *
     * @param array $tabEpci Liste des codes EPCI
     *
     * @return array
     */
    public function getDepartementsPourListeEpci($tabEpci)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('departement');

        $qr->from(MainConstante::ENTITY_PATH . 'Departement', 'departement');
        $qr->from(MainConstante::ENTITY_PATH . 'Epci', 'epci');
        $qr->join('epci.communes', 'commune');

        $qr->where('commune.codeDepartement = departement.code');
        $qr->andWhere('epci.code IN (:tabEpci)');

        $qr->setParameter('tabEpci', $tabEpci);

        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne le département à partir du code
     *
     * @param string $codeDept Code du département
     *
     * @return null|object
     */
    public function getDepartementParCode($codeDept)
    {
        return $this->findOneBy(['code' => $codeDept]);
    }
}
