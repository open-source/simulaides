<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 13:49
 */

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Entity\Comparatif;

/**
 * Classe ComparatifRepository
 * Projet : ADEME Simul'Aides
 * @author
 * @package   Simulaides\Domain\Repository
 */
class ComparatifRepository extends EntityRepository
{
    /**
     * Retourne le comparatif pour l'id et le mot de passe
     *
     * @param int    $id         Identifiant
     * @param string $motDePasse Mot de passe crypté
     *
     * @return Comparatif
     * @throws NonUniqueResultException
     */
    public function getComparatifByIdEtMdp($id, $motDePasse)
    {
        $qr = $this->createQueryBuilder('comparatif');
        $qr->where('comparatif.id = :id');
        $qr->andWhere('comparatif.motPasse = :motPasse');

        $qr->setParameter('id', $id);
        $qr->setParameter('motPasse', $motDePasse);

        return $qr->getQuery()->getOneOrNullResult();
    }

    /**
     * Retourne le comparatif pour l'ID
     *
     * @param int $id Identifiant du comparatif
     *
     * @return Comparatif
     */
    public function getComparatifById($id)
    {
        /* @var $comparatif Comparatif */
        $comparatif = $this->find($id);

        return $comparatif;
    }

    /**
     * Modifie ou crée un comparatif
     *
     * @param Comparatif $comparatif
     *
     * @throws OptimisticLockException
     */
    public function save(Comparatif $comparatif)
    {
        $id = $comparatif->getId();

        if (!empty($id)) {
            //Mise à jour
            $this->getEntityManager()->merge($comparatif);
        } else {
            //Ajout
            $this->getEntityManager()->persist($comparatif);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Supprime un comparatif
     *
     * @param Comparatif $comparatif Comparatif à supprimer
     *
     * @throws OptimisticLockException
     */
    public function delete(Comparatif $comparatif)
    {
        if ($this->getEntityManager()->contains($comparatif)) {
            $this->getEntityManager()->remove($comparatif);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Permet de supprimer tous les comparatifs d'un ou plusieurs projets
     *
     * @param array $tabIdProjet Liste des identifiant des projets
     */
    public function deleteComparatifPourProjet($tabIdProjet)
    {
        $qr = $this->createQueryBuilder('Comparatif');
        $qr->delete(MainConstante::ENTITY_PATH . 'Comparatif', 'Comparatif');
        $qr->where('Comparatif.simulation1 IN (:tabIdProjet)');

        $qr->setParameter('tabIdProjet', $tabIdProjet);

        $qr->getQuery()->execute();

        $qr = $this->createQueryBuilder('Comparatif');
        $qr->delete(MainConstante::ENTITY_PATH . 'Comparatif', 'Comparatif');
        $qr->where('Comparatif.simulation2 IN (:tabIdProjet)');

        $qr->setParameter('tabIdProjet', $tabIdProjet);

        $qr->getQuery()->execute();

        $qr = $this->createQueryBuilder('Comparatif');
        $qr->delete(MainConstante::ENTITY_PATH . 'Comparatif', 'Comparatif');
        $qr->where('Comparatif.simulation3 IN (:tabIdProjet)');

        $qr->setParameter('tabIdProjet', $tabIdProjet);

        $qr->getQuery()->execute();
    }


}
