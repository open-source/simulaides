<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\Epci;

/**
 * Classe EpciRepository
 * Repository sur les EPCI
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class EpciRepository extends EntityRepository
{

    /**
     * Retourne la liste des EPCI ayant au moins une commune
     * qui sont dans les départements indiqués
     *
     * @param $tabDepartements
     *
     * @return array
     */
    public function getEpciPourListeDepartement($tabDepartements)
    {
        $qr = $this->createQueryBuilder('epci');
        $qr->join('epci.communes', 'commune');
        $qr->where('commune.codeDepartement IN (:tabDepartement)');
        $qr->setParameter('tabDepartement', $tabDepartements);

        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne l'Epci à partir de son code
     *
     * @param string $codeEpci Code Epci
     *
     * @return null|Epci
     */
    public function getEpciPourCode($codeEpci)
    {
        return $this->findOneBy(['code' => $codeEpci]);
    }
}
