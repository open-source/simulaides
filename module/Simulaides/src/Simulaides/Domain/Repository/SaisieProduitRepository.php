<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Simulaides\Constante\MainConstante;

/**
 * Classe SaisieProduitRepository
 * Repository sur la saisie des prix par le particulier
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class SaisieProduitRepository extends EntityRepository
{
    /**
     * Permet de retourner la liste des saisies produit par le code produit
     * et le projet
     *
     * @param int    $idProjet    Identifiant du projet
     * @param string $codeProduit Code du produit
     *
     * @return array
     */
    public function getSaisieProduitParIdProjetEtCodeProduit($idProjet, $codeProduit)
    {
        $qr = $this->createQueryBuilder('saisieProduit');
        $qr->where('saisieProduit.codeProduit = :codeProduit');
        $qr->andWhere('saisieProduit.idProjet = :idProjet');

        $qr->setParameter('codeProduit', $codeProduit);
        $qr->setParameter('idProjet', $idProjet);

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de charger les informations découlant de la saisie des produits
     * sur le projet indiqué
     *
     * @param int $idProjet Identifiant du projet
     *
     * @return array
     */
    public function getSaisieProduitPourTravauxInfo($idProjet)
    {
        $req = "SELECT tra.code as code_trav, tra.intitule, sp.*, pro.regle_nbu, pat.type_data FROM saisie_produit sp ";
        $req = $req . " INNER JOIN produit pro ON pro.code = sp.code_produit ";
        $req = $req . " LEFT OUTER JOIN parametre_tech pat ON pat.code = sp.code_parametre_tech ";
        $req = $req . " INNER JOIN travaux tra ON tra.code = pro.code_travaux ";
        $req = $req . " INNER JOIN travaux_projet tp ON tp.code_travaux = tra.code
                        AND tp.id_projet = :idProjet ";
        $req = $req . " WHERE sp.id_projet = :idProjet ORDER BY tra.code ; ";

        $oConnexion = $this->_em->getConnection();
        $oStatement = $oConnexion->prepare($req);
        $oStatement->bindValue('idProjet', $idProjet);
        $oStatement->execute();

        return $oStatement->fetchAll();
    }

    /**
     * Permet de supprimer toutes les saisies d'un ou plusieurs projets
     *
     * @param array $tabIdProjet Liste des identifiant des projets
     */
    public function deleteSaisieProduitPourProjet($tabIdProjet)
    {
        $qr = $this->createQueryBuilder('saisieProduit');
        $qr->delete(MainConstante::ENTITY_PATH . 'SaisieProduit', 'saisieProduit');
        $qr->where('saisieProduit.idProjet IN (:tabIdProjet)');

        $qr->setParameter('tabIdProjet', $tabIdProjet);

        $qr->getQuery()->execute();
    }

    /**
     * Retourne les saisie produit pour les ravaux d'un projet
     *
     * @param int    $idProjet    Identifiant du projet
     * @param string $codeTravaux Code du travaux
     *
     * @return array
     */
    public function getSaisieProduitParIdProjetEtCodeTravaux($idProjet, $codeTravaux)
    {
        $qr = $this->createQueryBuilder('saisieProduit');
        $qr->join('saisieProduit.produit', 'produit');
        $qr->join('produit.travaux', 'travaux');
        $qr->where('travaux.code = :codeTravaux');
        $qr->andWhere('saisieProduit.idProjet = :idProjet');

        $qr->setParameter('codeTravaux', $codeTravaux);
        $qr->setParameter('idProjet', $idProjet);

        return $qr->getQuery()->getResult();
    }
}
