<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 24/02/15
 * Time: 11:17
 */

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Entity\Produit;

/**
 * Class ProduitRepository
 * @package Simulaides\Domain\Repository
 */
class ProduitRepository extends EntityRepository
{
    /**
     * Récupération de la liste des produits d'un travaux selon le type de logement
     *
     * @param string $codeTravaux Code du travaux
     * @param string $type        Type de logement (Maison ou Appartement)
     *
     * @return array
     */
    public function getProduitByCodeTravauxEtTypeLogement($codeTravaux, $type)
    {
        $queryBuilder = $this->createQueryBuilder('produit');
        $queryBuilder->leftJoin('produit.paramsTech', 'params', Join::WITH, 'params.' . $type . ' = 1');
        $queryBuilder->andWhere($queryBuilder->expr()->eq('produit.codeTravaux', ':codeTravaux'));
        $queryBuilder->setParameter(':codeTravaux', $codeTravaux);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param $codes
     *
     * @return array
     */
    public function getProduitbyCodes($codes)
    {
        $queryBuilder = $this->createQueryBuilder('produits');
        $queryBuilder->where($queryBuilder->expr()->in('produits.code', $codes));

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param $codeProduit
     * @param $type
     *
     * @return array
     */
    public function getParamByCodeProduit($codeProduit, $type)
    {
        $str      = 'select param, produit from Simulaides\Domain\Entity\ParametreTech param join
        param.produit produit where produit.code = \'' . $codeProduit . '\' and param.' . $type . ' = 1';
        $query    = $this->getEntityManager()->createQuery($str);
        $produits = $query->getResult();

        return $produits;
    }

    /**
     * Retourne la liste de tous les produits d'un travaux
     *
     * @param string $codeTravaux Code du travaux
     *
     * @return array
     */
    public function getProduitsParCodeTravaux($codeTravaux)
    {
        $qr = $this->createQueryBuilder('produit');
        $qr->where('produit.codeTravaux = :codeTravaux');
        $qr->orderBy('produit.intitule');
        $qr->setParameter('codeTravaux', $codeTravaux);

        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne la liste de tous les produits trié sur les travaux
     *
     * @return array
     */
    public function getTousProduitsTrieParCodeTravaux()
    {
        $qr = $this->createQueryBuilder('produit');
        $qr->orderBy('produit.codeTravaux, produit.intitule');
        return $qr->getQuery()->getResult();
    }

    /**
     * Retourne le produit à partir de son code
     *
     * @param string $codeProduit Code du produit
     *
     * @return null|Produit
     */
    public function getProduitParCodeProduit($codeProduit)
    {
        return $this->findOneBy(['code' => $codeProduit]);
    }

    /**
     * Permet de faire la modification ou l'ajout du produit
     *
     * @param Produit $produit
     */
    public function saveProduit($produit)
    {
        if ($this->getEntityManager()->contains($produit)) {
            //Mise à jour
            $this->getEntityManager()->merge($produit);
        } else {
            //Ajout
            $this->getEntityManager()->persist($produit);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Permet de récupérer les produits d'un travaux qui ont été
     * sélectionnés dans le projet
     *
     * @param string $codeTravaux Code du travaux
     * @param int    $idProjet    Identifiant du projet
     *
     * @return array
     */
    public function getProduitInfoParCodeTravauxEtIdProjet($codeTravaux, $idProjet)
    {
        $qr = $this->_em->createQueryBuilder();
        $qr->select('projet.id, produit.code, produit.intitule, produit.infosSaisieCouts, produit.infosSaisieCoutsOu');
        $qr->from(MainConstante::ENTITY_PATH . 'Projet', 'projet');
        $qr->join('projet.produits', 'produit');
        $qr->where('projet.id = :idProjet');
        $qr->andWhere('produit.codeTravaux = :codeTravaux');

        $qr->setParameter('idProjet', $idProjet);
        $qr->setParameter('codeTravaux', $codeTravaux);

        return $qr->getQuery()->getResult();
    }

}
