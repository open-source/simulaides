<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Entity\PerimetreGeo;

/**
 * Classe PerimetreGeoRepository
 * Repository sur les périmètres géographique des dispositifs
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class PerimetreGeoRepository extends EntityRepository
{

    /**
     * Retourne la liste des périmètres géographique d'un dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return array
     */
    public function getPerimetreGeoPourDispositif($idDispositif)
    {
        $qr = $this->createQueryBuilder('perimetreGeo');
        $qr->where('perimetreGeo.idDispositif = :idDispositif');
        $qr->setParameter('idDispositif', $idDispositif);

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de supprimer tous les éléments du périmètre d'un dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     */
    public function deletePerimetreGeoPourdispositif($idDispositif)
    {
        $qr = $this->createQueryBuilder('perimetreGeo');
        $qr->delete(MainConstante::ENTITY_PATH . 'PerimetreGeo', 'perimetreGeo');
        $qr->where('perimetreGeo.idDispositif = :idDispositif');

        $qr->setParameter('idDispositif', $idDispositif);

        $qr->getQuery()->execute();
    }

    /**
     * Permet de faire la modification ou l'ajout du perimetre Geo
     *
     * @param PerimetreGeo $perimetreGeo
     */
    public function savePerimetreGeo($perimetreGeo)
    {
        if ($this->getEntityManager()->contains($perimetreGeo)) {
            //Mise à jour
            $this->getEntityManager()->merge($perimetreGeo);
        } else {
            //Ajout
            $this->getEntityManager()->persist($perimetreGeo);
        }
        $this->getEntityManager()->flush();
    }
}
