<?php

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\RegleCategorie;

/**
 * Classe RegleVariableRepository
 * Repository sur les variables utilisées dans les règles dispositif/travaux
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class RegleVariableRepository extends EntityRepository
{

    /**
     * Permet de récupérer la liste des variables d'une catégorie
     *
     * @param RegleCategorie $categorie
     *
     * @return array
     */
    public function getListVariablePourCategorie($categorie)
    {
        $qr = $this->createQueryBuilder('variable');
        $qr->where('variable.categorie = :categorie');

        $qr->setParameter('categorie', $categorie);

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de récupérer la liste des variables d'une catégorie
     *
     * @param string $codeCategorie
     *
     * @return array
     */
    public function getListVariablePourCodeCategorie($codeCategorie)
    {
        $qr = $this->createQueryBuilder('variable');
        $qr->join('variable.categorie', 'categorie');
        $qr->where('categorie.code = :codeCategorie');

        $qr->setParameter('codeCategorie', $codeCategorie);

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de retourner la variable selon son code
     *
     * @param $codeVariable
     *
     * @return null|object
     */
    public function getVariablePourCodeVariable($codeVariable)
    {
        return $this->findOneBy(['codeVar' => $codeVariable]);
    }
}
