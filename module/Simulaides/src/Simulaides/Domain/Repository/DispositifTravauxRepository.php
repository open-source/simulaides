<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Constante\MainConstante;
use Simulaides\Domain\Entity\DispositifTravaux;

/**
 * Classe DispositifTravauxRepository
 * Repository sur les travaux pris en charge par les dispositifs
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class DispositifTravauxRepository extends EntityRepository
{
    /**
     * Permet de supprimer les travaux d'un dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     */
    public function deleteDispositifTravauxParIdDispositif($idDispositif)
    {
        $qr = $this->createQueryBuilder('dispositifTravaux');
        $qr->delete(MainConstante::ENTITY_PATH . 'DispositifTravaux', 'dispositifTravaux');
        $qr->where('dispositifTravaux.idDispositif = :idDispositif');

        $qr->setParameter('idDispositif', $idDispositif);

        $qr->getQuery()->execute();
    }

    /**
     * Permet de retourne la liste des travaux prix en charge par un dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return array
     */
    public function getDispositifTravauxParIdDispositif($idDispositif)
    {
        $qr = $this->createQueryBuilder('dispositifTravaux');
        $qr->where('dispositifTravaux.idDispositif = :idDispositif');

        $qr->setParameter('idDispositif', $idDispositif);

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de retourne la liste des travaux prix en charge par un dispositif
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return array
     */
    public function getIdDispositifTravauxParIdDispositif($idDispositif)
    {
        $qr = $this->createQueryBuilder('dispositifTravaux');
        $qr->select('dispositifTravaux.id');
        $qr->where('dispositifTravaux.idDispositif = :idDispositif');

        $qr->setParameter('idDispositif', $idDispositif);

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de faire la modification ou l'ajout d'un travaux du dispositif
     *
     * @param DispositifTravaux $dispositifTravaux
     */
    public function saveDispositifTravaux($dispositifTravaux)
    {
        if ($this->getEntityManager()->contains($dispositifTravaux)) {
            //Mise à jour
            $this->getEntityManager()->merge($dispositifTravaux);
        } else {
            //Ajout
            $this->getEntityManager()->persist($dispositifTravaux);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Retourne la liste des codes des travaux pris en charge par le dispositif
     *
     * @param int $idDispositif Idenifiant du dispositif
     *
     * @return array
     */
    public function getCodeTravauxPourDispositif($idDispositif)
    {
        $qr = $this->createQueryBuilder('travaux');
        $qr->select('travaux.codeTravaux');
        $qr->where('travaux.idDispositif = :idDispositif');
        $qr->setParameter('idDispositif', $idDispositif);

        return $qr->getQuery()->getResult();

    }

    /**
     * Retourne le dispositif travaux pour le code travaux et le dispositif
     *
     * @param $idDispositif
     * @param $codeTravaux
     *
     * @return null|DispositifTravaux
     */
    public function getDispositifTravauxPourIdDispositifEtCodeTravaux($idDispositif, $codeTravaux)
    {
        return $this->findOneBy(['idDispositif' => $idDispositif, 'codeTravaux' => $codeTravaux]);
    }

    /**
     * Supprime le travaux dans le dispositif et ses règles
     *
     * @param $dispositifTravaux
     */
    public function deleteDispositifTravaux($dispositifTravaux)
    {
        if ($this->getEntityManager()->contains($dispositifTravaux)) {
            $this->getEntityManager()->remove($dispositifTravaux);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Retourne le ravaux dans le dispositif à partir de son ID
     *
     * @param int $idDispositifTravaux
     *
     * @return null|DispositifTravaux
     */
    public function getDispositifTravauxParId($idDispositifTravaux)
    {
        return $this->findOneBy(['id' => $idDispositifTravaux]);
    }

    /**
     * Permet de récupérer les travaux du dispositif
     *
     * @param string $lstCodeTravaux Liste des codes travaux séparés par des virgules
     * @param int    $idDispositif   Identifiant du dispositif
     *
     * @return array
     */
    public function getRegleTravauxPourDispositif($lstCodeTravaux, $idDispositif)
    {
        $req = "SELECT dit.code_travaux, dit.eligibilite_specifique, dtr.type, dtr.condition_regle,
                dtr.expression FROM dispositif_travaux dit ";
        $req = $req . " LEFT OUTER JOIN disp_travaux_regle dtr ON dtr.id_disp_travaux = dit.id ";
        $req = $req . " WHERE dit.id_dispositif = :idDispositif
                AND dit.code_travaux IN (" . $lstCodeTravaux . ") ORDER BY dit.code_travaux ; ";

        $oConnexion = $this->_em->getConnection();
        $oStatement = $oConnexion->prepare($req);
        $oStatement->bindValue('idDispositif', $idDispositif);
        $oStatement->execute();

        return $oStatement->fetchAll();
    }

    /**
     * @param $codeTravaux
     * @return array
     */
    public function getDispositifTravauxParCodeTravaux($codeTravaux)
    {
        return $this->findBy(['codeTravaux' => $codeTravaux]);
    }
}
