<?php

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\ProjetChoixDispositif;

/**
 * Classe ProjetChoixDispositifRepository
 */
class ProjetChoixDispositifRepository extends EntityRepository
{
    /**
     * Permet de faire la modification ou l'ajout du choix de dispositif
     *
     * @param ProjetChoixDispositif $projetChoixDispositif
     */
    public function saveProjetChoixDispositif($projetChoixDispositif)
    {
        $this->getEntityManager()->persist($projetChoixDispositif);
        $this->getEntityManager()->flush();
    }

    public function deleteByIdProjet($idProjet)
    {
        $choix = $this->findBy(['idProjet' => $idProjet]);
        foreach ($choix as $unChoix) {
            $this->getEntityManager()->remove($unChoix);
        }
        $this->getEntityManager()->flush();
    }
    public function  deleteByIdProjetAndTravaux($idProjet,$codeTravaux)
    {
        $choix = $this->findBy(['codeTravaux'=> $codeTravaux,'idProjet' => $idProjet]);
        foreach ($choix as $unChoix) {
            $this->getEntityManager()->remove($unChoix);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Permet de récupérer le service sur les projets
     *
     * @return array
     */
    public function getOffresByIdProjetAndIdDispositif($idProjet,$idDispositif){

        $req = "SELECT code_travaux FROM projet_choix_dispositif ";
        $req = $req . " WHERE id_projet = ".$idProjet;
        $req = $req . " AND id_dispositif = ".$idDispositif;

        $oConnexion = $this->_em->getConnection();
        $oStatement = $oConnexion->prepare($req);
        $oStatement->execute();
        $retour = $oStatement->fetchAll();
        $arrayResult = [];
        foreach ($retour as $result){
            $arrayResult[] = $result['code_travaux'];
        }
        return $arrayResult;
    }

    /**
     *
     * Retourne l'offre à partir du code de travaux et l'id du projet
     * @param $idProjet int | String id du projet
     * @param $codeTravaux String Code du travaux
     * @return bool|mixed|string
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getOffresByIdProjetAndCodeTravaux($idProjet, $codeTravaux){

        $req = "SELECT id_dispositif FROM projet_choix_dispositif ";
        $req = $req . " WHERE id_projet = ".$idProjet;
        $req = $req . " AND code_travaux = '".$codeTravaux."'";

        $oConnexion = $this->_em->getConnection();
        $oStatement = $oConnexion->prepare($req);
        $oStatement->execute();
        $retour = $oStatement->fetchColumn();

        return $retour;

}

    /**
     * @param $idProjet
     * @return array
     */
    public function getByIdProjet($idProjet)
    {
        return $this->findBy(['idProjet' => $idProjet]);
    }


    /**
     * @param $idProjet
     * @return array
     */
    public function purgeOffresSelectionnees($idProjet)
    {
        $req = "DELETE FROM projet_choix_dispositif ";
        $req = $req . " WHERE id_projet = ".$idProjet;
        $req = $req . " AND id_dispositif in (
        SELECT id FROM dispositif 
        WHERE code_etat <> '". ValeurListeConstante::ETAT_DISPOSITIF_ACTIVE."' 
            OR debut_validite > now()
            OR (fin_validite is not null AND fin_validite < now())
            )";

        $oConnexion = $this->_em->getConnection();
        $oStatement = $oConnexion->prepare($req);
        $oStatement->execute();
    }


    /**
     * Permet de supprimer tous les comparatifs d'un ou plusieurs projets
     *
     * @param array $tabIdProjet Liste des identifiant des projets
     */
    public function deleteSaisieProduitPourProjet($tabIdProjet)
    {
        $qr = $this->createQueryBuilder('Choix');
        $qr->delete(MainConstante::ENTITY_PATH . 'ProjetChoixDispositif', 'Choix');
        $qr->where('Choix.idProjet IN (:tabIdProjet)');

        $qr->setParameter('tabIdProjet', $tabIdProjet);

        $qr->getQuery()->execute();
    }

}
