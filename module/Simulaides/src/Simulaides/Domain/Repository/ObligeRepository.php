<?php

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\Oblige;
use Exception;

/**
 * ObligeRepository
 *
 * @package Simulaides\Domain\Repository
 */
class ObligeRepository extends EntityRepository
{
    /**
     * Permet de faire l'ajout ou la modification d'un obligé
     *
     * @param Oblige $oblige
     *
     * @throws Exception
     */
    public function saveOblige(Oblige $oblige)
    {
        try {
            if ($obligeExist = $this->getOneBySiren($oblige->getSiren())) {
                $oblige->setId($obligeExist->getId());
                $this->getEntityManager()->merge($oblige);
            } else {
                $this->getEntityManager()->persist($oblige);
            }
            $this->getEntityManager()->flush();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Retourne un oblige à partir de son siren
     *
     * @param string $siren Siren de l'oblige
     *
     * @return null|object
     */
    public function getOneBySiren($siren)
    {
        return $this->findOneBy(['siren' => $siren]);
    }
}
