<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Classe ListeRepository
 * Repository sur les Liste
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class ListeRepository extends EntityRepository
{
    /**
     * Retourne une liste par son code
     *
     * @param string $code Code de la liste
     *
     * @return null|object
     */
    public function getListeParCode($code)
    {
        return $this->findOneBy(['code' => $code]);
    }
}
