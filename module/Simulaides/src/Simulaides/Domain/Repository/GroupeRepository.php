<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Simulaides\Domain\Entity\Groupe;

/**
 * Classe GroupeRepository
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class GroupeRepository extends EntityRepository
{
    /**
     * Retourne le groupe en fonctiondu code de la région et du dispositif
     *
     * @param $codeRegion
     * @param $idDispositif
     *
     * @return mixed
     */
    public function getGroupeParCodeRegionEtIdDispositif($codeRegion, $idDispositif)
    {
        $qr = $this->createQueryBuilder('groupe');
        $qr->where('groupe.codeRegion = :codeRegion');
        $qr->andWhere('groupe.idDispositif = :idDispositif');

        $qr->setParameter('codeRegion', $codeRegion);
        $qr->setParameter('idDispositif', $idDispositif);

        return $qr->getQuery()->getOneOrNullResult();
    }

    /**
     * Retourne le groupe en fonction  du dispositif
     *
     * @param $idDispositif
     *
     * @return mixed
     */
    public function getGroupeParIdDispositif($idDispositif)
    {
        $qr = $this->createQueryBuilder('groupe');
        $qr->where('groupe.idDispositif = :idDispositif');

        $qr->setParameter('idDispositif', $idDispositif);

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de faire la modification ou l'ajout d'un groupe
     *
     * @param Groupe $groupe
     */
    public function saveGroupe($groupe)
    {
        if ($this->getEntityManager()->contains($groupe)) {
            //Mise à jour
            $this->getEntityManager()->merge($groupe);
        } else {
            //Ajout
            $this->getEntityManager()->persist($groupe);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Permet de modifier les ordres des dispositifs dans la région
     * Après que l'on ai déplacé vers le haut un dispositif dans la liste
     *
     * @param $codeRegion
     * @param $idDispositifChanged
     * @param $limitInf
     * @param $limitSup
     */
    public function modifyOrdreApresRemontee($codeRegion, $idDispositifChanged, $limitInf, $limitSup)
    {
        $qr = $this->createQueryBuilder('groupe');
        $qr->update('Simulaides\Domain\Entity\Groupe', 'groupe');
        $qr->set('groupe.numeroOrdre', 'groupe.numeroOrdre + 1');
        $qr->where($qr->expr()->between('groupe.numeroOrdre', ':limitInf', ':limitSup'));
        $qr->andWhere('groupe.codeRegion = :codeRegion');
        $qr->andWhere('groupe.idDispositif <> :idDispositif');

        $qr->setParameter('codeRegion', $codeRegion);
        $qr->setParameter('idDispositif', $idDispositifChanged);
        $qr->setParameter('limitSup', $limitSup);
        $qr->setParameter('limitInf', $limitInf);

        $qr->getQuery()->execute();
    }

    /**
     * Permet de modifier les ordres des dispositifs dans la région
     * Après que l'on ai déplacé vers le haut le dispositif dans la liste
     *
     * @param $codeRegion
     * @param $idDispositifChanged
     * @param $limitInf
     * @param $limitSup
     */
    public function modifyOrdreApresDescente($codeRegion, $idDispositifChanged, $limitInf, $limitSup)
    {
        $qr = $this->createQueryBuilder('groupe');
        $qr->update('Simulaides\Domain\Entity\Groupe', 'groupe');
        $qr->set('groupe.numeroOrdre', 'groupe.numeroOrdre - 1');
        $qr->where($qr->expr()->between('groupe.numeroOrdre', ':limitInf', ':limitSup'));
        $qr->andWhere('groupe.codeRegion = :codeRegion');
        $qr->andWhere('groupe.idDispositif <> :idDispositif');

        $qr->setParameter('codeRegion', $codeRegion);
        $qr->setParameter('idDispositif', $idDispositifChanged);
        $qr->setParameter('limitSup', $limitSup);
        $qr->setParameter('limitInf', $limitInf);

        $qr->getQuery()->execute();
    }

    /**
     * Permet de réorganiser les numéros d'ordre d'un groupe d'une région
     * Après suppression d'un dispositif
     *
     * @param string $codeRegion Code de la région du groupe
     * @param int    $ordreSupp  Numéro d'ordre supprimé
     */
    public function modifyOrdreApresSuppression($codeRegion, $ordreSupp)
    {
        $qr = $this->createQueryBuilder('groupe');
        $qr->update('Simulaides\Domain\Entity\Groupe', 'groupe');
        $qr->set('groupe.numeroOrdre', 'groupe.numeroOrdre - 1');
        $qr->where('groupe.numeroOrdre > :ordreSupp');
        $qr->andWhere('groupe.codeRegion = :codeRegion');

        $qr->setParameter('ordreSupp', $ordreSupp);
        $qr->setParameter('codeRegion', $codeRegion);

        $qr->getQuery()->execute();
    }

    /**
     * Retourne le numéro max pour la région
     *
     * @param $codeRegion
     *
     * @return mixed
     */
    public function getMaxOrdrePourCodeRegion($codeRegion)
    {
        $qr = $this->createQueryBuilder('groupe');
        $qr->select('MAX(groupe.numeroOrdre) AS maxOrdre');
        $qr->where('groupe.codeRegion = :codeRegion');
        $qr->setParameter('codeRegion', $codeRegion);

        return $qr->getQuery()->getOneOrNullResult();
    }

    /**
     * Retourne les groupes du dispositif
     *
     * @param $idDispositif
     * @param $tabRegion
     *
     * @return array
     */
    public function getGroupePourDispositifNotInTabRegion($idDispositif, $tabRegion)
    {
        $qr = $this->createQueryBuilder('groupe');
        $qr->where('groupe.region NOT IN (:tabRegion)');
        $qr->andWhere('groupe.idDispositif = :idDispositif');

        $qr->setParameter('tabRegion', $tabRegion);
        $qr->setParameter('idDispositif', $idDispositif);

        return $qr->getQuery()->getResult();
    }

    /**
     * Permet de supprimer un groupe
     *
     * @param Groupe $groupe Groupe à supprimer
     */
    public function deleteGroupe($groupe)
    {
        $this->getEntityManager()->remove($groupe);
        $this->getEntityManager()->flush();
    }
}
