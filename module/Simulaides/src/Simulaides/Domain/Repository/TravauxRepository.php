<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 09:24
 */

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\Travaux;

/**
 * Class TravauxRepository
 * Repository sur les ravaux
 *
 * @package Simulaides\Domain\Repository
 */
class TravauxRepository extends EntityRepository
{
    /**
     * Retourne la liste des travaux selon le type de logement
     *
     * @param string $type Type de logement
     *
     * @return array
     */
    public function getTravauxByLogementTypeEtRegion($type, $outre_mer)
    {
        $condition = [$type => 1];
        if(!$outre_mer){
            $condition["outre_mer"]=0;
        }
        /*if($outre_mer){
            $condition["outre_mer"]=1;
        }else{
            $condition["metropole"]=1;
        }*/
        return $this->findBy($condition, ['categorie' => 'ASC', 'numOrdre' => 'ASC', 'code' => 'ASC']);
    }

    /**
     * Retourne le travaux correspondant au code
     *
     * @param string $code Code du travaux
     *
     * @return Travaux
     */
    public function getTravauxByCode($code)
    {
        return $this->findOneBy(['code' => $code]);
    }

    /**
     * Retourne la liste des Travaux correspondant
     * à la liste des code travaux en entrée
     *
     * @param array $codes Liste des codes travaux
     *
     * @return array
     */
    public function getTravauxByCodes($codes)
    {
        $queryBuilder = $this->createQueryBuilder('travaux');
        $queryBuilder->where($queryBuilder->expr()->in('travaux.code', $codes));

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Retourne la liste des travaux trié sur la catégorie et le code
     *
     * @return array
     */
    public function getListTravauxTrieParCategorie()
    {
        return $this->findBy([], ['categorie' => 'ASC', 'numOrdre' => 'ASC','code' => 'ASC']);
    }

    /**
     * Retourne la liste des travaux trié sur l'intitulé
     *
     * @return array
     */
    public function getListTravauxTrieParIntitule()
    {
        return $this->findBy([], ['intitule' => 'ASC']);
    }

    /**
     * Retourne la liste des travaux trié sur l'intitulé
     *
     * @return array
     */
    public function getListTravauxForFiltre()
    {
        return $this->findBy([], ['intitule' => 'ASC', 'categorie' => 'ASC', 'numOrdre' => 'ASC']);
    }

    /**
     * Retourne la liste des travaux trié sur le code
     *
     * @return array
     */
    public function getListTravauxTrieParCode()
    {
        return $this->findBy([], ['code' => 'ASC']);
    }
}
