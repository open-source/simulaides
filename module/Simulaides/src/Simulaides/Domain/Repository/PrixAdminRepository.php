<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\PrixAdmin;

/**
 * Classe PrixAdminRepository
 * Repository sur la définition des prix des produits du référentiel
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class PrixAdminRepository extends EntityRepository
{
    /**
     * Permet de modifier ou d'ajouter un prix admin
     *
     * @param PrixAdmin $prixAdmin
     */
    public function savePrixAdmin($prixAdmin)
    {
        if ($this->getEntityManager()->contains($prixAdmin)) {
            //Mise à jour
            $this->getEntityManager()->merge($prixAdmin);
        } else {
            //Ajout
            $this->getEntityManager()->persist($prixAdmin);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Suppresion d'un prix admin
     *
     * @param PrixAdmin $prixAdmin
     */
    public function deletePrixAdmin($prixAdmin)
    {
        if ($this->getEntityManager()->contains($prixAdmin)) {
            $this->getEntityManager()->remove($prixAdmin);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Permet de récupérer un prix admin selon le cod eproduit et l'id projet
     *
     * @param string $codeProduit Code du produit
     * @param int    $idProjet    Identifiant du projet
     *
     * @return null|object
     */
    public function getPrixAdminParCodeProduitCodeProjet($codeProduit, $idProjet)
    {
        return $this->findOneBy(['codeProduit' => $codeProduit, 'idProjet' => $idProjet]);
    }

    /**
     * Permet de retourner la liste des prix admin
     *
     * @return mixed
     */
    public function getAllPrixAdminPourExport()
    {
        $qr = $this->createQueryBuilder('prixAdmin');
        $qr->select(
            'prixAdmin.id, prixAdmin.codeProduit, prixAdmin.dateSaisie, prixAdmin.prixTotal,
             prixAdmin.prixMO, prixAdmin.prixFourniture, produit.intitule'
        );
        $qr->join('prixAdmin.produit', 'produit');
        return $qr->getQuery()->execute();
    }
}
