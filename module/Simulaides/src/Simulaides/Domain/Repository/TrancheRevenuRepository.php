<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\TrancheRevenu;

/**
 * Classe TrancheRevenuRepository
 * Repository sur les tanches de revenu
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class TrancheRevenuRepository extends EntityRepository
{

    /**
     * Suppression de toutes les tranches de revenu appartenant à un groupe de tranche
     *
     * @param string $codeGroupe Code du groupe de tranche
     */
    public function deleteTrancheRevenuParGroupe($codeGroupe)
    {
        $qr = $this->createQueryBuilder('tranche');
        $qr->delete('Simulaides\Domain\Entity\TrancheRevenu', 'tranche');
        $qr->where('tranche.codeGroupe = :codeGroupe');
        $qr->setParameter('codeGroupe', $codeGroupe);
        $qr->getQuery()->execute();
    }

    /**
     * Suppression d'une tranche de revenu
     *
     * @param $trancheRevenu
     */
    public function deleteTrancheRevenu($trancheRevenu)
    {
        if ($this->getEntityManager()->contains($trancheRevenu)) {
            $this->getEntityManager()->remove($trancheRevenu);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Retourne la liste des tranches de revenu pour le groupe donné
     *
     * @param string $codeGroupe Code du groupe de tranche
     *
     * @return array
     */
    public function getTrancheRevenusParGroupe($codeGroupe)
    {
        return $this->findBy(['codeGroupe' => $codeGroupe]);
    }

    /**
     * Retourne la tranche de revenu par rapport à son code
     *
     * @param $codeTranche
     *
     * @return TrancheRevenu
     */
    public function getTrancheRevenu($codeTranche)
    {
        return $this->findOneBy(['codeTranche' => $codeTranche]);
    }

    /**
     * Permet de faire la modification ou l'ajout de la tranche de revenu
     *
     * @param $trancheRevenu
     */
    public function saveTrancheRevenu($trancheRevenu)
    {
        if ($this->getEntityManager()->contains($trancheRevenu)) {
            //Mise à jour
            $this->getEntityManager()->merge($trancheRevenu);
        } else {
            //Ajout
            $this->getEntityManager()->persist($trancheRevenu);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Récupère le code de tranche pour un groupe de tranche donné
     *
     * @param int    $nbPersonnes       Nb de personnes total dans le foyer
     * @param string $codeGroupeTranche Codede la tranche
     * @param float  $revenusFoyer      Revenu du foyer
     *
     * @return array
     */
    public function getCodeTranchePourNbPersonne($nbPersonnes, $codeGroupeTranche, $revenusFoyer)
    {
        // On recherche les montants des différentes tranches supérieur aux revenus
        // en fonction du nombre de personnes (les montant sont classés en ordre croissant)
        if ($nbPersonnes < 7) {
            $colonne = "mt_" . $nbPersonnes . "_personne";
            $sousReq = " SELECT code_tranche, " . $colonne . " AS montant FROM tranche_revenu ";
            $sousReq = $sousReq . " WHERE code_groupe = :codeGroupeTranche ";
        } else {
            $sousReq = " SELECT code_tranche, (mt_6_personne + ((" . $nbPersonnes . " -6) *  mt_personne_supp))
                        AS montant FROM tranche_revenu ";
            $sousReq = $sousReq . " WHERE code_groupe = :codeGroupeTranche ";
        }

        $req = "SELECT code_tranche, montant FROM (" . $sousReq . ") tranche ";
        $req = $req . " WHERE montant >= :revenuFoyer ";
        $req = $req . " ORDER BY montant ;";

        $oConnexion = $this->_em->getConnection();
        $oStatement = $oConnexion->prepare($req);
        $oStatement->bindValue('codeGroupeTranche', $codeGroupeTranche);
        $oStatement->bindValue('revenuFoyer', $revenusFoyer);
        $oStatement->execute();

        return $oStatement->fetchAll();
    }
}
