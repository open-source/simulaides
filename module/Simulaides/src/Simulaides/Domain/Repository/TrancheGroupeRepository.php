<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\TrancheGroupe;

/**
 * Classe TrancheGroupeRepository
 * Repository sur les groupes de tranche de revenu
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class TrancheGroupeRepository extends EntityRepository
{

    /**
     * Retourne la liste des groupes de tranche de revenu
     * @return array
     */
    public function getTrancheGroupes()
    {
        $qr = $this->createQueryBuilder('groupe');
        $qr->orderBy('groupe.codeGroupe');

        return $qr->getQuery()->getResult();
    }

    /**
     * Suppression du groupe indiqué
     *
     * @param string $codeGroupe Code du groupe à supprimer
     */
    public function deleteTrancheGroupe($codeGroupe)
    {
        $qr = $this->createQueryBuilder('groupe');
        $qr->delete('Simulaides\Domain\Entity\TrancheGroupe', 'groupe');
        $qr->where('groupe.codeGroupe = :codeGroupe');
        $qr->setParameter('codeGroupe', $codeGroupe);
        $qr->getQuery()->execute();
    }

    /**
     * Retourne l'entité TrancheGroupe du code indiqué
     *
     * @param string $codeGroupe Code du groupe à rechercher
     *
     * @return TrancheGroupe
     */
    public function getTrancheGroupe($codeGroupe)
    {
        return $this->findOneBy(['codeGroupe' => $codeGroupe]);
    }

    /**
     * Permet de faire la modification ou l'ajout de la tranche de groupe
     *
     * @param $trancheGroupe
     */
    public function saveTrancheGroupe($trancheGroupe)
    {
        if ($this->getEntityManager()->contains($trancheGroupe)) {
            //Mise à jour
            $this->getEntityManager()->merge($trancheGroupe);
        } else {
            //Ajout
            $this->getEntityManager()->persist($trancheGroupe);
        }
        $this->getEntityManager()->flush();
    }
}
