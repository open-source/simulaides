<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 13/02/15
 * Time: 17:34
 */

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class VersionRepository
 * @package Simulaides\Domain\Repository
 */
class VersionRepository extends EntityRepository
{
    /**
     * Retournela version
     * @return array
     */
    public function getVersionApp()
    {
        $queryBuilder = $this->createQueryBuilder('version');

        return $queryBuilder->getQuery()->getResult();

    }

}
