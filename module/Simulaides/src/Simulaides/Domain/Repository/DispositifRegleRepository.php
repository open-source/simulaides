<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\DispositifRegle;

/**
 * Classe DispositifRegleRepository
 * Repository sur les règles des dispositifs
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class DispositifRegleRepository extends EntityRepository
{

    /**
     * Retourne la regle selon l'id
     *
     * @param $idRegleDispositif
     *
     * @return null|DispositifRegle
     */
    public function getDispositifRegleParId($idRegleDispositif)
    {
        return $this->findOneBy(['id' => $idRegleDispositif]);
    }

    /**
     * Permet de supprimer une règle de dispositif
     *
     * @param DispositifRegle $regle Regle de dispositif à supprimer
     */
    public function deleteDispositifRegle($regle)
    {
        if ($this->getEntityManager()->contains($regle)) {
            $this->getEntityManager()->remove($regle);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Permet de sauvegarder une règle de dispositif
     *
     * @param $regle
     */
    public function saveDispositifRegle($regle)
    {
        if ($this->getEntityManager()->contains($regle)) {
            //Mis eà jour
            $this->getEntityManager()->merge($regle);
        } else {
            //Création
            $this->getEntityManager()->persist($regle);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Retourne les règles d'un dispositif
     *
     * @param $idDispositif
     *
     * @return null|array
     */
    public function getDispositifRegleParIdDispositif($idDispositif)
    {
        return $this->findBy(['idDispositif' => $idDispositif]);
    }
}
