<?php
namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\DispositifHistorique;

/**
 * Classe DispositifHistoriqueRepository
 * Repository sur l'historique des dispositifs
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Repository
 */
class DispositifHistoriqueRepository extends EntityRepository
{
    /**
     * Permet de faire la modification ou l'ajout de l'historique du dispositif
     *
     * @param DispositifHistorique $dispositifHistorique
     */
    public function saveDispositifHistorique($dispositifHistorique)
    {
        if ($this->getEntityManager()->contains($dispositifHistorique)) {
            //Mise à jour
            $this->getEntityManager()->merge($dispositifHistorique);
        } else {
            //Ajout
            $this->getEntityManager()->persist($dispositifHistorique);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * Retourne la liste des historiques d'un dispositif
     *
     * @param Dispositif $dispositif Dispositif
     *
     * @return array
     */
    public function getListHistoriquePourDispositif($dispositif)
    {
        return $this->findBy(['dispositif' => $dispositif], ['dateNote' => 'ASC']);
    }
}
