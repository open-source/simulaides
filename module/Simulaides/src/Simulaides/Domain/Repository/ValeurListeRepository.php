<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 13/02/15
 * Time: 17:34
 */

namespace Simulaides\Domain\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\ValeurListe;

/**
 * Class ValeurListeRepository
 * @package Simulaides\Domain\Repository
 */
class ValeurListeRepository extends EntityRepository
{
    /**
     * Retourne la liste des valeurs d'un code liste
     *
     * @param string $codeListe Code de la liste
     *
     * @return array
     */
    public function getListeValuesByCodeListe($codeListe)
    {
        return $this->findBy(['codeListe' => $codeListe]);
    }

    /**
     * Retourne la liste des statut du demandeur dans l'ordre de la fonction sql FIELD
     * @return array
     */
    public function getListeStatutDemandeur()
    {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());

        $rsm->addRootEntityFromClassMetadata('Simulaides\Domain\Entity\ValeurListe', 'statut');

        $query = $this->getEntityManager()->createNativeQuery("select * from valeur_liste as statut where statut.code_liste = 'L_STATUT_DEMANDEUR' ORDER BY FIELD(statut.code, 'PROP_RES_1', 'LOC', 'PROP_BAIL', 'PROP_RES_2', 'PROP_RES_1_SCI', 'PROP_BAIL_SCI', 'OCC_GRAT','NU_PROP','USUFRUIT'), statut.code_liste",
            $rsm);

        $res = $query->getResult();

        return $res;

    }

    /**
     * Retourne la valeur du texte adminsitrable
     * défini par le code
     *
     * @param string $codeText Code du texte
     *
     * @return ValeurListe
     */
    public function getTextAdministrable($codeText)
    {
        return $this->findOneBy(['codeListe' => ValeurListeConstante::TEXT_ADMINISTRABLE, 'code' => $codeText]);
    }

    /**
     * Retourne la valeur selon le code de la liste et le code
     *
     * @param string $codeListe Code de la liste
     * @param string $code      Code
     *
     * @return null|ValeurListe
     */
    public function getValeurPourCodeListeEtCode($codeListe, $code)
    {
        return $this->findOneBy(['codeListe' => $codeListe, 'code' => $code]);
    }

    /**
     * Permet de sauvegarder une valeur de liste
     *
     * @param ValeurListe $valeurListe
     */
    public function saveValeurListe($valeurListe)
    {
        if ($this->getEntityManager()->contains($valeurListe)) {
            //Mise à jour
            $this->getEntityManager()->merge($valeurListe);
        } else {
            //Ajout
            $this->getEntityManager()->persist($valeurListe);
        }
        $this->getEntityManager()->flush();
    }

    public function getListeTypeDispositif()
    {
        return $this->findBy(['codeListe' => ValeurListeConstante::L_TYPE_DISPOSITIF], ['libelle' => 'ASC']);
    }

    public function getListeEtatDispositif()
    {
        return $this->findBy(['codeListe' => ValeurListeConstante::L_ETAT_DISPOSITIF], ['libelle' => 'ASC']);
    }

}
