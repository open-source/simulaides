<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 04/03/15
 * Time: 12:23
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity (repositoryClass="Simulaides\Domain\Repository\CoeffSurfaceRepository")
 * @ORM\Table(name="coeff_surface")
 * Class CoeffSurface
 * @package Simulaides\Domain\Entities
 */
class CoeffSurface
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="code_type_logement", type="string", length=50)
     */
    private $codeTypeLogement;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_type_logement", referencedColumnName="code")
     */
    private $typeLogement;

    /**
     * @var
     */
    private $codeTrancheSurface;

    /**
     * @var float
     * @ORM\Column(name="coefficient", type="decimal",precision=10, scale=2,nullable=false)
     */
    private $coefficient;

    /**
     * Code de la surface
     * @var string
     * @ORM\Column(name="code_surface", type="string",length=2,nullable=false)
     */
    private $codeSurface;

    /**
     * Code du travaux
     * @var string
     * @ORM\Column(name="code_travaux", type="string",length=3,nullable=true)
     */
    private $codeTravaux;

    /**
     * @param mixed $codeTrancheSurface
     */
    public function setCodeTrancheSurface($codeTrancheSurface)
    {
        $this->codeTrancheSurface = $codeTrancheSurface;
    }

    /**
     * @return mixed
     */
    public function getCodeTrancheSurface()
    {
        return $this->codeTrancheSurface;
    }

    /**
     * @param mixed $codeTypeLogement
     */
    public function setCodeTypeLogement($codeTypeLogement)
    {
        $this->codeTypeLogement = $codeTypeLogement;
    }

    /**
     * @return mixed
     */
    public function getCodeTypeLogement()
    {
        return $this->codeTypeLogement;
    }

    /**
     * @param mixed $coefficient
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;
    }

    /**
     * @return mixed
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \Simulaides\Domain\Entity\ValeurListe $typeLogement
     */
    public function setTypeLogement($typeLogement)
    {
        $this->typeLogement = $typeLogement;
    }

    /**
     * @return \Simulaides\Domain\Entity\ValeurListe
     */
    public function getTypeLogement()
    {
        return $this->typeLogement;
    }

    /**
     * @param string $codeSurface
     */
    public function setCodeSurface($codeSurface)
    {
        $this->codeSurface = $codeSurface;
    }

    /**
     * @return string
     */
    public function getCodeSurface()
    {
        return $this->codeSurface;
    }

    /**
     * @param mixed $codeTravaux
     */
    public function setCodeTravaux($codeTravaux)
    {
        $this->codeTravaux = $codeTravaux;
    }

    /**
     * @return mixed
     */
    public function getCodeTravaux()
    {
        return $this->codeTravaux;
    }
}
