<?php
namespace Simulaides\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Classe DispositifTravaux
 * Liste des travaux pris en charge par le dispositif
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Entity
 */

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\DispositifTravauxRepository")
 * @ORM\Table(name="dispositif_travaux")
 * Class DispositifTravaux
 * @package Simulaides\Domain\Entities
 */
class DispositifTravaux
{
    /**
     * Identifiant unique
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private $id;

    /**
     * Identifiant du dispositif
     * @var int
     * @ORM\Column(type="integer", name="id_dispositif", length=11)
     */
    private $idDispositif;

    /**
     * @var Dispositif
     * @ORM\ManyToOne(targetEntity="Dispositif")
     * @ORM\JoinColumn(name="id_dispositif", referencedColumnName="id")
     */
    private $dispositif;

    /**
     * Code du travaux pris en charge
     * @var string
     * @ORM\Column(name="code_travaux", type="string", length=3)
     */
    private $codeTravaux;

    /**
     * Travaux pris en charge
     * @var Travaux
     * @ORM\ManyToOne(targetEntity="Travaux")
     * @ORM\JoinColumn(name="code_travaux", referencedColumnName="code")
     */
    private $travaux;

    /**
     * Texte indiquant une elligibilité spécifique
     *
     * @var string
     * @ORM\Column(name="eligibilite_specifique", type="text", nullable=true)
     */
    private $eligibiliteSpecifique;

    /**
     * Liste des règles du travaux
     *
     * @var Collection
     * @ORM\OneToMany(targetEntity="DispTravauxRegle", mappedBy="dispTravaux",
     * cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private $listeTravauxRegles;

    /**
     * constructeur
     */
    public function __construct()
    {
        $this->listeTravauxRegles = new ArrayCollection();
    }

    /**
     * @param mixed $code_travaux
     */
    public function setCodeTravaux($code_travaux)
    {
        $this->codeTravaux = $code_travaux;
    }

    /**
     * @return mixed
     */
    public function getCodeTravaux()
    {
        return $this->codeTravaux;
    }

    /**
     * @param \Simulaides\Domain\Entity\Dispositif $dispositif
     */
    public function setDispositif($dispositif)
    {
        $this->dispositif = $dispositif;
    }

    /**
     * @return \Simulaides\Domain\Entity\Dispositif
     */
    public function getDispositif()
    {
        return $this->dispositif;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $idDispositif
     */
    public function setIdDispositif($idDispositif)
    {
        $this->idDispositif = $idDispositif;
    }

    /**
     * @return int
     */
    public function getIdDispositif()
    {
        return $this->idDispositif;
    }

    /**
     * @param \Simulaides\Domain\Entity\Travaux $travaux
     */
    public function setTravaux($travaux)
    {
        $this->travaux = $travaux;
    }

    /**
     * @return \Simulaides\Domain\Entity\Travaux
     */
    public function getTravaux()
    {
        return $this->travaux;
    }

    /**
     * @param string $eligibiliteSpecifique
     */
    public function setEligibiliteSpecifique($eligibiliteSpecifique)
    {
        $this->eligibiliteSpecifique = $eligibiliteSpecifique;
    }

    /**
     * @return string
     */
    public function getEligibiliteSpecifique()
    {
        return $this->eligibiliteSpecifique;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListeTravauxRegles()
    {
        return $this->listeTravauxRegles;
    }

    /**
     * Permet d'ajouter un ou plusieurs règles des travaux
     *
     * @param $regle
     */
    public function addDispositifTravauxRegle($regle)
    {
        if ($regle instanceof Collection) {
            foreach ($regle as $item) {
                $this->listeTravauxRegles->add($item);
            }
        } else {
            $this->listeTravauxRegles->add($regle);
        }
    }

    /**
     * Perme de supprimer une ou plusieurs règles dans le travaux
     *
     * @param $regle
     */
    public function removeDispositifTravauxRegle($regle)
    {
        if ($regle instanceof Collection) {
            foreach ($regle as $unItem) {
                $this->listeTravauxRegles->removeElement($unItem);
            }
        } else {
            $this->listeTravauxRegles->removeElement($regle);
        }
    }
}
