<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 10/02/15
 * Time: 18:53
 */

namespace Simulaides\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\ProduitRepository")
 * @ORM\Table(name="produit")
 * Class Produit
 * @package Simulaides\Domain\Entities
 */
class Produit
{
    /**
     * @var string
     * @ORM\Column(name="code_travaux", type="string", length=3)
     */
    private $codeTravaux;

    /**
     * @var Travaux
     * @ORM\ManyToOne(targetEntity="Travaux", inversedBy="produits")
     * @ORM\JoinColumn(name="code_travaux", referencedColumnName="code")
     */
    private $travaux;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(type="string", length=150)
     */
    private $intitule;

    /**
     * @var string
     * @ORM\Column(name="regle_nbu", type="string", length=50)
     */
    private $regleNBU;

    /**
     * @var float
     * @ORM\Column(name="prix_u_total", type="decimal", precision=10, scale=2)
     */
    private $prixUTotal;

    /**
     * @var float
     * @ORM\Column(name="prix_u_total_ou", type="decimal", precision=10, scale=2)
     */
    private $prixUTotalOu;

    /**
     * @var float
     * @ORM\Column(name="prix_u_mo", type="decimal", precision=10, scale=2)
     */
    private $prixUMo;

    /**
     * @var float
     * @ORM\Column(name="prix_u_mo_ou", type="decimal", precision=10, scale=2)
     */
    private $prixUMoOu;

    /**
     * @var float
     * @ORM\Column(name="prix_u_fourniture", type="decimal", precision=10, scale=2)
     */
    private $prixUFourniture;

    /**
     * @var float
     * @ORM\Column(name="prix_u_fourniture_ou", type="decimal", precision=10, scale=2)
     */
    private $prixUFournitureOu;

    /**
     * @var float
     * @ORM\Column(type="decimal")
     */
    private $cote;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $unite;

    /**
     * @var  ArrayCollection $paramsTech
     * @ORM\OneToMany(targetEntity="ParametreTech", mappedBy="produit", cascade={"persist", "remove"})
     */
    private $paramsTech;

    /**
     * Taux de TVA
     *
     * @var  float $tva
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $tva;

    /**
     * Taux de TVA pour l'outremer
     *
     * @var  float $tva_ou
     * @ORM\Column(name="tva_ou",type="decimal", precision=10, scale=2)
     */
    private $tvaOu;

    /**
     * @var  string
     * @ORM\Column(name="infos_saisie_couts", type="text", nullable=true)
     */
    private $infosSaisieCouts;

    /**
     * @var  string
     * @ORM\Column(name="infos_saisie_couts_ou", type="text", nullable=true)
     */
    private $infosSaisieCoutsOu;

    /**
     *
     */
    public function __construct()
    {
        $this->paramsTech     = new ArrayCollection();
        $this->saisiesProduit = new ArrayCollection();
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $codeTravaux
     */
    public function setCodeTravaux($codeTravaux)
    {
        $this->codeTravaux = $codeTravaux;
    }

    /**
     * @return string
     */
    public function getCodeTravaux()
    {
        return $this->codeTravaux;
    }

    /**
     * @param float $cote
     */
    public function setCote($cote)
    {
        $this->cote = $cote;
    }

    /**
     * @return float
     */
    public function getCote()
    {
        return $this->cote;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param float $prixUFourniture
     */
    public function setPrixUFourniture($prixUFourniture)
    {
        $this->prixUFourniture = $prixUFourniture;
    }

    /**
     * @return float
     */
    public function getPrixUFourniture()
    {
        return $this->prixUFourniture;
    }

    /**
     * @param float $prixUFournitureOu
     */
    public function setPrixUFournitureOu($prixUFournitureOu)
    {
        $this->prixUFournitureOu = $prixUFournitureOu;
    }

    /**
     * @return float
     */
    public function getPrixUFournitureOu()
    {
        return $this->prixUFournitureOu;
    }

    /**
     * @param float $prixUMo
     */
    public function setPrixUMo($prixUMo)
    {
        $this->prixUMo = $prixUMo;
    }

    /**
     * @return float
     */
    public function getPrixUMo()
    {
        return $this->prixUMo;
    }

    /**
     * @param float $prixUMoOu
     */
    public function setPrixUMoOu($prixUMoOu)
    {
        $this->prixUMoOu = $prixUMoOu;
    }

    /**
     * @return float
     */
    public function getPrixUMoOu()
    {
        return $this->prixUMoOu;
    }

    /**
     * @param float $prixUTotal
     */
    public function setPrixUTotal($prixUTotal)
    {
        $this->prixUTotal = $prixUTotal;
    }

    /**
     * @return float
     */
    public function getPrixUTotal()
    {
        return $this->prixUTotal;
    }

    /**
     * @param float $prixUTotalOu
     */
    public function setPrixUTotalOu($prixUTotalOu)
    {
        $this->prixUTotalOu = $prixUTotalOu;
    }

    /**
     * @return float
     */
    public function getPrixUTotalOu()
    {
        return $this->prixUTotalOu;
    }

    /**
     * @param string $regleNBU
     */
    public function setRegleNBU($regleNBU)
    {
        $this->regleNBU = $regleNBU;
    }

    /**
     * @return string
     */
    public function getRegleNBU()
    {
        return $this->regleNBU;
    }

    /**
     * @param Travaux $travaux
     */
    public function setTravaux(Travaux $travaux)
    {
        $this->travaux = $travaux;
    }

    /**
     * @return Travaux
     */
    public function getTravaux()
    {
        return $this->travaux;
    }

    /**
     * @param string $unite
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;
    }

    /**
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $paramsTech
     */
    public function setParamsTech($paramsTech)
    {
        $this->paramsTech = $paramsTech;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getParamsTech()
    {
        return $this->paramsTech;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $saisiesProduit
     */
    public function setSaisiesProduit($saisiesProduit)
    {
        $this->saisiesProduit = $saisiesProduit;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSaisiesProduit()
    {
        return $this->saisiesProduit;
    }

    /**
     * @param SaisieProduit $saisieProduit
     */
    public function addSaisiesProduit(SaisieProduit $saisieProduit)
    {
        $this->saisiesProduit[] = $saisieProduit;
    }

    /**
     * @param float $tva
     */
    public function setTva($tva)
    {
        $this->tva = $tva;
    }

    /**
     * @return float
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * @param float $tvaOu
     */
    public function setTvaOu($tvaOu)
    {
        $this->tvaOu = $tvaOu;
    }

    /**
     * @return float
     */
    public function getTvaOu()
    {
        return $this->tvaOu;
    }

    /**
     * @param string $infosSaisieCouts
     */
    public function setInfosSaisieCouts($infosSaisieCouts)
    {
        $this->infosSaisieCouts = $infosSaisieCouts;
    }

    /**
     * @return string
     */
    public function getInfosSaisieCouts()
    {
        return $this->infosSaisieCouts;
    }

    /**
     * @param string $infosSaisieCoutsOu
     */
    public function setInfosSaisieCoutsOu($infosSaisieCoutsOu)
    {
        $this->infosSaisieCoutsOu = $infosSaisieCoutsOu;
    }

    /**
     * @return string
     */
    public function getInfosSaisieCoutsOu()
    {
        return $this->infosSaisieCoutsOu;
    }
}
