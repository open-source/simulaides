<?php


namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\ProjetChoixDispositifRepository")
 * @ORM\Table(name="projet_choix_dispositif")
 * Class ProjetChoixDispositif
 * @package Simulaides\Domain\Entities
 */
class ProjetChoixDispositif
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="id_projet", type="integer")
     */
    private $idProjet;

    /**
     * @var Projet
     * @ORM\ManyToOne(targetEntity="Projet", inversedBy="choixDispositifs")
     * @ORM\JoinColumn(name="id_projet", referencedColumnName="id")
     */
    private $projet;

    /**
     * @var int
     * @ORM\Column(name="id_dispositif", type="integer")
     */
    private $idDispositif;

    /**
     * @var Dispositif
     * @ORM\ManyToOne(targetEntity="Dispositif")
     * @ORM\JoinColumn(name="id_dispositif", referencedColumnName="id")
     */
    private $dispositif;

    /**
     * @var string
     * @ORM\Column(name="code_travaux", type="string")
     */
    private $codeTravaux;

    /**
     * @var Travaux
     * @ORM\ManyToOne(targetEntity="Travaux")
     * @ORM\JoinColumn(name="code_travaux", referencedColumnName="code")
     */
    private $travaux;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdProjet()
    {
        return $this->idProjet;
    }

    /**
     * @param int $idProjet
     */
    public function setIdProjet($idProjet)
    {
        $this->idProjet = $idProjet;
    }

    /**
     * @return Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * @param Projet $projet
     */
    public function setProjet($projet)
    {
        $this->projet = $projet;
    }

    /**
     * @return int
     */
    public function getIdDispositif()
    {
        return $this->idDispositif;
    }

    /**
     * @param int $idDispositif
     */
    public function setIdDispositif($idDispositif)
    {
        $this->idDispositif = $idDispositif;
    }

    /**
     * @return Dispositif
     */
    public function getDispositif()
    {
        return $this->dispositif;
    }

    /**
     * @param Dispositif $dispositif
     */
    public function setDispositif($dispositif)
    {
        $this->dispositif = $dispositif;
    }

    /**
     * @return string
     */
    public function getCodeTravaux()
    {
        return $this->codeTravaux;
    }

    /**
     * @param string $codeTravaux
     */
    public function setCodeTravaux($codeTravaux)
    {
        $this->codeTravaux = $codeTravaux;
    }

    /**
     * @return Travaux
     */
    public function getTravaux()
    {
        return $this->travaux;
    }

    /**
     * @param Travaux $travaux
     */
    public function setTravaux($travaux)
    {
        $this->travaux = $travaux;
    }

}
