<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 25/02/15
 * Time: 10:55
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SaisieProduitMontant extends SaisieProduit
{
    /**
     * @var  string
     * @ORM\Column(name="type_montant", type="string", length=15)
     */
    private $typeMontant;

    /**
     * @var  float
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $cout;

    /**
     * @param float $cout
     */
    public function setCout($cout)
    {
        $this->cout = $cout;
    }

    /**
     * @return float
     */
    public function getCout()
    {
        return $this->cout;
    }

    /**
     * @param string $typeMontant
     */
    public function setTypeMontant($typeMontant)
    {
        $this->typeMontant = $typeMontant;
    }

    /**
     * @return string
     */
    public function getTypeMontant()
    {
        return $this->typeMontant;
    }
}
