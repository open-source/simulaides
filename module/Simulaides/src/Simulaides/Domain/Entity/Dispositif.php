<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 09/02/15
 * Time: 14:14
 */

namespace Simulaides\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\DispositifRepository")
 * @ORM\Table(name="dispositif")
 * Class Dispositif
 * @package Simulaides\Domain\Entities
 */
class Dispositif
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=150)
     */
    private $intitule;

    /**
     * Intitulé du dispositif affiché pour le conseiller
     *
     * @var string
     * @ORM\Column(name="intitule_conseiller", type="string", length=150, nullable=true)
     */
    private $intituleConseiller;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptif;

    /**
     * Code de l'état du dispositif
     *
     * @var string
     * @ORM\Column(name="code_etat", type="string", length=10, nullable=false)
     */
    private $codeEtat;


    /**
     * id du dispositif requis
     *
     * @var int
     * @ORM\Column(name="id_dispositif_requis", type="integer", nullable=true)
     */
    private $idDispositifRequis;


    /**
     * Dispositif Requis
     *
     * @var Dispositif
     * @ORM\ManyToOne(targetEntity="Dispositif")
     * @ORM\JoinColumn(name="id_dispositif_requis", referencedColumnName="id",nullable=true)
     */
    private $dispositifRequis;

    /**
     * Etat du dispositif
     *
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_etat", referencedColumnName="code")
     */
    private $etat;

    /**
     * @var \DateTime
     * @ORM\Column(name="debut_validite", type="date", nullable=true)
     */
    private $debutValidite;

    /**
     * @var \DateTime
     * @ORM\Column(name="fin_validite", type="date", nullable=true)
     */
    private $finValidite;

    /** @var string
     * @ORM\Column(type="string", length=50)
     */
    private $financeur;

    /** @var  bool
     * @ORM\Column(name="exclut_cee", type="boolean", length=1)
     */
    private $exclutCEE;

    /** @var  bool
     * @ORM\Column(name="exclut_coup_de_pouce", type="boolean", length=1)
     */
    private $exclutCoupDePouce;

    /**
     * @var  bool
     * @ORM\Column(name="exclut_anah", type="boolean", length=1)
     */
    private $exclutAnah;

    /**
     * @var  bool
     * @ORM\Column(name="exclut_epci", type="boolean", length=1)
     */
    private $exclutEPCI;

    /**
     * @var  bool
     * @ORM\Column(name="exclut_region", type="boolean", length=1)
     */
    private $exclutRegion;

    /**
     * @var  bool
     * @ORM\Column(name="is_offre", type="boolean", length=1)
     */
    private $is_offre;

    /**
     * @return bool
     */
    public function isIsOffre()
    {
        return $this->is_offre;
    }

    /**
     * @param bool $is_offre
     */
    public function setIsOffre($is_offre)
    {
        $this->is_offre = $is_offre;
    }

    /**
     * @var  bool
     * @ORM\Column(name="exclut_communal", type="boolean", length=1)
     */
    private $exclutCommunal;

    /**
     * @var  bool
     * @ORM\Column(name="exclut_national", type="boolean", length=1)
     */
    private $exclutNational;

    /**
     * @var  bool
     * @ORM\Column(name="exclut_departement", type="boolean", length=1)
     */
    private $exclutDepartement;

    /**
     * @var string
     * @ORM\Column(name="code_type_dispositif", type="string", length=50)
     */
    private $codeTypeDispositif;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_type_dispositif", referencedColumnName="code")
     *
     */
    private $typeDispositif;

    /**
     * @var bool
     * @ORM\Column(name="evaluer_regle", type="boolean")
     */
    private $evaluerRegle;

    /**
     * Type du périmètre géographique
     *
     * @var string
     * @ORM\Column(name="type_perimetre", type="string", length=1)
     */
    private $typePerimetre;

    /**
     * Liste des règles du dispositif
     *
     * @var Collection
     * @ORM\OneToMany(targetEntity="DispositifRegle", mappedBy="dispositif", cascade={"remove", "persist"})
     */
    private $listDispositifRegles;

    /**
     * Liste des périmètres géo du dispositif
     *
     * @var Collection
     * @ORM\OneToMany(targetEntity="PerimetreGeo", mappedBy="dispositif", cascade={"remove", "persist"})
     */
    private $listDispositifPerimetreGeo;

    /**
     * Liste des historiques du dispositif
     *
     * @var Collection
     * @ORM\OneToMany(targetEntity="DispositifHistorique", mappedBy="dispositif", cascade={"remove"})
     */
    private $listDispositifHistorique;

    /**
     * Liste des travaux pris en charge par le dispositif
     *
     * @var Collection
     * @ORM\OneToMany(targetEntity="DispositifTravaux", mappedBy="dispositif")
     * @ORM\OrderBy({"codeTravaux" = "ASC"})
     */
    private $listDispositifTravaux;

    /**
     * Liste des travaux pris en charge par le dispositif
     *
     * @var Collection
     * @ORM\OneToMany(targetEntity="DispositifTravaux", mappedBy="dispositif")
     */
    private $listDispositifs;

    /**
     * Obligé
     * @var Oblige
     * @ORM\ManyToOne(targetEntity="Oblige", inversedBy="listDispositifs")
     * @ORM\JoinColumn(name="id_oblige", referencedColumnName="id")
     */
    private $oblige;

    /**
     * Identifiant de l'offre CEE lors dans le fichier d'import
     * @var string
     * @ORM\Column(type="string", name="identifiant_offre_import", length=15)
     */
    private $identifiantOffreImport;

    /**
     * URL du logo de l'offre
     * @var string
     * @ORM\Column(type="string", name="logo", length=255)
     */
    private $logo;

    /**
     * URL du site web de l'offre
     * @var string
     * @ORM\Column(type="string", name="site_web", length=255)
     */
    private $siteWeb;

    /**
     * constructeur
     */
    public function __construct()
    {
        $this->listDispositifRegles = new ArrayCollection();
        $this->listDispositifPerimetreGeo = new ArrayCollection();
        $this->listDispositifHistorique = new ArrayCollection();
        $this->listDispositifTravaux = new ArrayCollection();
        $this->listDispositifs = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDebutValidite()
    {
        return $this->debutValidite;
    }

    /**
     * @param mixed $debutValidite
     */
    public function setDebutValidite($debutValidite)
    {
        $this->debutValidite = $debutValidite;
    }

    /**
     * @return mixed
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @param mixed $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @return mixed
     */
    public function getFinValidite()
    {
        return $this->finValidite;
    }

    /**
     * @param mixed $finValidite
     */
    public function setFinValidite($finValidite)
    {
        $this->finValidite = $finValidite;
    }

    /**
     * @return mixed
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param mixed $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return boolean
     */
    public function getExclutCEE()
    {
        return $this->exclutCEE;
    }

    /**
     * @param boolean $exclutCEE
     */
    public function setExclutCEE($exclutCEE)
    {
        $this->exclutCEE = $exclutCEE;
    }

    /**
     * @return bool
     */
    public function getExclutAnah()
    {
        return $this->exclutAnah;
    }

    /**
     * @param bool $exclutAnah
     */
    public function setExclutAnah($exclutAnah)
    {
        $this->exclutAnah = $exclutAnah;
    }

    /**
     * @return bool
     */
    public function getExclutEPCI()
    {
        return $this->exclutEPCI;
    }

    /**
     * @param bool $exclutEPCI
     */
    public function setExclutEPCI($exclutEPCI)
    {
        $this->exclutEPCI = $exclutEPCI;
    }

    /**
     * @return bool
     */
    public function getExclutRegion()
    {
        return $this->exclutRegion;
    }

    /**
     * @param bool $exclutRegion
     */
    public function setExclutRegion($exclutRegion)
    {
        $this->exclutRegion = $exclutRegion;
    }

    /**
     * @return bool
     */
    public function getExclutCommunal()
    {
        return $this->exclutCommunal;
    }

    /**
     * @param bool $exclutCommunal
     */
    public function setExclutCommunal($exclutCommunal)
    {
        $this->exclutCommunal = $exclutCommunal;
    }

    /**
     * @return bool
     */
    public function getExclutNational()
    {
        return $this->exclutNational;
    }

    /**
     * @param bool $exclutNational
     */
    public function setExclutNational($exclutNational)
    {
        $this->exclutNational = $exclutNational;
    }

    /**
     * @return bool
     */
    public function getExclutDepartement()
    {
        return $this->exclutDepartement;
    }

    /**
     * @param bool $exclutDepartement
     */
    public function setExclutDepartement($exclutDepartement)
    {
        $this->exclutDepartement = $exclutDepartement;
    }

    /**
     * @return string
     */
    public function getFinanceur()
    {
        return $this->financeur;
    }

    /**
     * @param string $financeur
     */
    public function setFinanceur($financeur)
    {
        $this->financeur = $financeur;
    }

    /**
     * @return string
     */
    public function getCodeTypeDispositif()
    {
        return $this->codeTypeDispositif;
    }

    /**
     * @param string $codeTypeDispositif
     */
    public function setCodeTypeDispositif($codeTypeDispositif)
    {
        $this->codeTypeDispositif = $codeTypeDispositif;
    }

    /**
     * @return ValeurListe
     */
    public function getTypeDispositif()
    {
        return $this->typeDispositif;
    }

    /**
     * @param ValeurListe $typeDispositif
     */
    public function setTypeDispositif($typeDispositif)
    {
        $this->typeDispositif = $typeDispositif;
    }

    /**
     * @return boolean
     */
    public function getEvaluerRegle()
    {
        return $this->evaluerRegle;
    }

    /**
     * @param boolean $evaluerRegle
     */
    public function setEvaluerRegle($evaluerRegle)
    {
        $this->evaluerRegle = $evaluerRegle;
    }

    /**
     * @return mixed
     */
    public function getIntituleConseiller()
    {
        return $this->intituleConseiller;
    }

    /**
     * @param mixed $intituleConseiller
     */
    public function setIntituleConseiller($intituleConseiller)
    {
        $this->intituleConseiller = $intituleConseiller;
    }

    /**
     * @return string
     */
    public function getTypePerimetre()
    {
        return $this->typePerimetre;
    }

    /**
     * @param string $typePerimetre
     */
    public function setTypePerimetre($typePerimetre)
    {
        $this->typePerimetre = $typePerimetre;
    }

    /**
     * @return string
     */
    public function getCodeEtat()
    {
        return $this->codeEtat;
    }

    /**
     * @param string $codeEtat
     */
    public function setCodeEtat($codeEtat)
    {
        $this->codeEtat = $codeEtat;
    }

    /**
     * @return null|ValeurListe
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getListDispositifHistorique()
    {
        return $this->listDispositifHistorique;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListDispositifPerimetreGeo()
    {
        return $this->listDispositifPerimetreGeo;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListDispositifRegles()
    {
        return $this->listDispositifRegles;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListDispositifTravaux()
    {
        return $this->listDispositifTravaux;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListDispositifs()
    {
        return $this->listDispositifs;
    }

    /**
     * Permet d'ajouter une ou plusieurs règle(s) au dispositif
     *
     * @param $regle
     */
    public function addDispositifRegle($regle)
    {
        if ($regle instanceof Collection) {
            foreach ($regle as $item) {
                $this->listDispositifRegles->add($item);
            }
        } else {
            $this->listDispositifRegles->add($regle);
        }
    }

    /**
     * Permet d'ajouter un ou plusieurs périmètre(s) géo au dispositif
     *
     * @param $perimetreGeo
     */
    public function addDispositifPerimetreGeo($perimetreGeo)
    {
        if ($perimetreGeo instanceof Collection) {
            foreach ($perimetreGeo as $item) {
                $this->listDispositifPerimetreGeo->add($item);
            }
        } else {
            $this->listDispositifPerimetreGeo->add($perimetreGeo);
        }
    }

    /**
     * @return Oblige
     */
    public function getOblige()
    {
        return $this->oblige;
    }

    /**
     * @param Oblige $oblige
     */
    public function setOblige($oblige)
    {
        $this->oblige = $oblige;
    }

    /**
     * @return string
     */
    public function getIdentifiantOffreImport()
    {
        return $this->identifiantOffreImport;
    }

    /**
     * @param string $identifiantOffreImport
     */
    public function setIdentifiantOffreImport($identifiantOffreImport)
    {
        $this->identifiantOffreImport = $identifiantOffreImport;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * @param string $siteWeb
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;
    }


    /**
     * @return boolean
     */
    public function getExclutCoupDePouce()
    {
        return $this->exclutCoupDePouce;
    }

    /**
     * @param boolean $exclutCoupDePouce
     */
    public function setExclutCoupDePouce($exclutCoupDePouce)
    {
        $this->exclutCoupDePouce = $exclutCoupDePouce;
    }


    /**
     * @return int
     */
    public function getIdDispositifRequis()
    {
        return $this->idDispositifRequis;
    }

    /**
     * @param int $idDispositifRequis
     */
    public function setIdDispositifRequis($idDispositifRequis)
    {
        $this->idDispositifRequis = $idDispositifRequis;
    }

    /**
     * @return null|Dispositif
     */
    public function getDispositifRequis()
    {
        return $this->dispositifRequis;
    }

    /**
     * @param Dispositif $dispositifRequis
     */
    public function setDispositifRequis($dispositifRequis)
    {
        $this->dispositifRequis = $dispositifRequis;
    }


}
