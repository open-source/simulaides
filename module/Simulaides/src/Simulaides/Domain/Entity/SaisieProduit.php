<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 09:45
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity (repositoryClass="Simulaides\Domain\Repository\SaisieProduitRepository")
 * @ORM\Table(name="saisie_produit",indexes={
 *      @ORM\Index(name="idprojet_idx", columns={"id_projet"}),
 *      @ORM\Index(name="codeproduit_idx", columns={"code_produit"}),
 *      @ORM\Index(name="codeprmtech_idx", columns={"code_parametre_tech"}),
 *      @ORM\Index(name="type_idx", columns={"type"})
 * })
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"PARAM_TECH" = "SaisieProduitParametreTech", "MONTANT" = "SaisieProduitMontant"})
 * Class SaisieProduit
 * @package Simulaides\Domain\Entity
 */
abstract class SaisieProduit
{

    public function __clone()
    {
        if ($this->id) {
            $this->setId(null);
        }
    }

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="code_produit", type="string", length=50)
     */
    private $codeProduit;

    /**
     * @var Produit
     * @ORM\ManyToOne(targetEntity="Produit")
     * @ORM\JoinColumn(name="code_produit", referencedColumnName="code")
     */
    private $produit;

    /**
     * @var int
     * @ORM\Column(name="id_projet", type="integer")
     */
    private $idProjet;

    /**
     * @var Projet
     * @ORM\ManyToOne(targetEntity="Projet", inversedBy="saisiesProduit")
     * @ORM\JoinColumn(name="id_projet", referencedColumnName="id")
     */
    private $projet;

    /**
     * @param string $codeProduit
     */
    public function setCodeProduit($codeProduit)
    {
        $this->codeProduit = $codeProduit;
    }

    /**
     * @return string
     */
    public function getCodeProduit()
    {
        return $this->codeProduit;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $idProjet
     */
    public function setIdProjet($idProjet)
    {
        $this->idProjet = $idProjet;
    }

    /**
     * @return int
     */
    public function getIdProjet()
    {
        return $this->idProjet;
    }

    /**
     * @param Produit $produit
     */
    public function setProduit(Produit $produit)
    {
        $this->produit = $produit;
    }

    /**
     * @return Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param Projet $projet
     */
    public function setProjet(Projet $projet)
    {
        $this->projet = $projet;
    }

    /**
     * @return Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }
}
