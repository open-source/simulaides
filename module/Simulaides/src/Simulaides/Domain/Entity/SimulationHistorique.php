<?php
namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe SimulationHistorique
 * Permet de gérer l'historique de l'éxécution des simumlations
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Entity
 */

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\SimulationHistoriqueRepository")
 * @ORM\Table(name="simulation_historique")
 * Class SimulationHistorique
 * @package Simulaides\Domain\Entities
 */
class SimulationHistorique
{
    /**
     * Identifiant invariable
     *
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Identifiant du projet
     *
     * @var int
     * @ORM\Column(name="id_projet", type="integer")
     */
    private $idProjet;

    /**
     * Date de l'éxecution
     *
     * @var \DateTime
     * @ORM\Column(name="date_execution", type="date")
     */
    private $dateExecution;

    /**
     * Code de la commune de l'éxecution
     *
     * @var string
     * @ORM\Column(type="string", length=6, name="code_commune")
     */
    private $codeCommune;

    /**
     * Commune liée au code
     *
     * @var Commune
     * @ORM\ManyToOne(targetEntity="Commune")
     * @ORM\JoinColumn(name="code_commune", referencedColumnName="code_insee")
     */
    private $commune;

    /**
     * Partenaire à travers duquel la simulation a été créée
     * @var Partenaire
     * @ORM\ManyToOne(targetEntity="Partenaire")
     * @ORM\JoinColumn(name="partenaire", referencedColumnName="id")
     */
    private $partenaire;

    /**
     * Identifiant invariable
     *
     * @var int
     * @ORM\Column(name="montant", type="integer")
     */
    private $montant;

    /**
     * Identifiant invariable
     *
     * @var int
     * @ORM\Column(name="taux", type="integer")
     */
    private $taux;

    /**
     * Utilisateur ayant un role ADEME
     *
     * @var int
     * @ORM\Column(name="simul_admin", type="boolean")
     */
    private $simulAdmin;

    /**
     * @param mixed $codeCommune
     */
    public function setCodeCommune($codeCommune)
    {
        $this->codeCommune = $codeCommune;
    }

    /**
     * @return mixed
     */
    public function getCodeCommune()
    {
        return $this->codeCommune;
    }

    /**
     * @param \Simulaides\Domain\Entity\Commune $commune
     */
    public function setCommune($commune)
    {
        $this->commune = $commune;
    }

    /**
     * @return \Simulaides\Domain\Entity\Commune
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * @param \DateTime $dateExecution
     */
    public function setDateExecution($dateExecution)
    {
        $this->dateExecution = $dateExecution;
    }

    /**
     * @return \DateTime
     */
    public function getDateExecution()
    {
        return $this->dateExecution;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $idProjet
     */
    public function setIdProjet($idProjet)
    {
        $this->idProjet = $idProjet;
    }

    /**
     * @return int
     */
    public function getIdProjet()
    {
        return $this->idProjet;
    }

    /**
     * @return mixed
     */
    public function getPartenaire()
    {
        return $this->partenaire;
    }

    /**
     * @param mixed $partenaire
     */
    public function setPartenaire($partenaire)
    {
        $this->partenaire = $partenaire;
    }

    /**
     * @return int
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param int $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }

    /**
     * @return int
     */
    public function getTaux()
    {
        return $this->taux;
    }

    /**
     * @param int $taux
     */
    public function setTaux($taux)
    {
        $this->taux = $taux;
    }

    /**
     * @return boolean
     */
    public function getSimulAdmin()
    {
        return $this->simulAdmin;
    }

    /**
     * @param int $simulAdmin
     */
    public function setSimulAdmin($simulAdmin)
    {
        $this->simulAdmin = $simulAdmin;
    }


}
