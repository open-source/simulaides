<?php

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe Comparatif
 *
 * Projet : simulaides-php
 *
 * @author
 *
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\ComparatifRepository")
 * @ORM\Table(name="comparatif")
 * Class Comparatif
 * @package   Simulaides\Domain\Entities
 */
class Comparatif
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="mot_passe", type="string", length=5)
     */
    private $motPasse;

    /**
     * Simulation 1
     * @var Projet
     * @ORM\ManyToOne(targetEntity="Projet")
     * @ORM\JoinColumn(name="simulation1", referencedColumnName="id")
     */
    private $simulation1;

    /**
     * Simulation 2
     * @var Projet
     * @ORM\ManyToOne(targetEntity="Projet")
     * @ORM\JoinColumn(name="simulation2", referencedColumnName="id")
     */
    private $simulation2;

    /**
     * Simulation 3
     * @var Projet
     * @ORM\ManyToOne(targetEntity="Projet")
     * @ORM\JoinColumn(name="simulation3", referencedColumnName="id")
     */
    private $simulation3;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMotPasse()
    {
        return $this->motPasse;
    }

    /**
     * @param string $motPasse
     */
    public function setMotPasse($motPasse)
    {
        $this->motPasse = $motPasse;
    }

    /**
     * @return Projet
     */
    public function getSimulation1()
    {
        return $this->simulation1;
    }

    /**
     * @param Projet $simulation1
     */
    public function setSimulation1($simulation1)
    {
        $this->simulation1 = $simulation1;
    }

    /**
     * @return Projet|null
     */
    public function getSimulation2()
    {
        return $this->simulation2;
    }

    /**
     * @param Projet|null $simulation2
     */
    public function setSimulation2($simulation2)
    {
        $this->simulation2 = $simulation2;
    }

    /**
     * @return Projet|null
     */
    public function getSimulation3()
    {
        return $this->simulation3;
    }

    /**
     * @param Projet|null $simulation3
     */
    public function setSimulation3($simulation3)
    {
        $this->simulation3 = $simulation3;
    }
}
