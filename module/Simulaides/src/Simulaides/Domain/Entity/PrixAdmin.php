<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 09:19
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\PrixAdminRepository")
 * @ORM\Table(name="prix_admin")
 * Class PrixAdmin
 * @package Simulaides\Domain\Entity
 */
class PrixAdmin
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="code_produit", type="string", length=50)
     */
    private $codeProduit;

    /**
     * @var Produit
     * @ORM\ManyToOne(targetEntity="Produit")
     * @ORM\JoinColumn(name="code_produit", referencedColumnName="code")
     */
    private $produit;

    /**
     * @var int
     * @ORM\Column(name="id_projet", type="integer", length=11)
     */
    private $idProjet;

    /**
     * @var float
     * @ORM\Column(name="prix_total", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prixTotal;

    /**
     * @var float
     * @ORM\Column(name="prix_mo", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prixMO;

    /**
     * @var float
     * @ORM\Column(name="prix_fourniture", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prixFourniture;

    /**
     * Date de la saisie
     * @var \DateTime
     * @ORM\Column(name="date_saisie", type="date", nullable=false)
     */
    private $dateSaisie;

    /**
     * @param string $codeProduit
     */
    public function setCodeProduit($codeProduit)
    {
        $this->codeProduit = $codeProduit;
    }

    /**
     * @return string
     */
    public function getCodeProduit()
    {
        return $this->codeProduit;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param float $prixFourniture
     */
    public function setPrixFourniture($prixFourniture)
    {
        $this->prixFourniture = $prixFourniture;
    }

    /**
     * @return float
     */
    public function getPrixFourniture()
    {
        return $this->prixFourniture;
    }

    /**
     * @param float $prixMO
     */
    public function setPrixMO($prixMO)
    {
        $this->prixMO = $prixMO;
    }

    /**
     * @return float
     */
    public function getPrixMO()
    {
        return $this->prixMO;
    }

    /**
     * @param float $prixTotal
     */
    public function setPrixTotal($prixTotal)
    {
        $this->prixTotal = $prixTotal;
    }

    /**
     * @return float
     */
    public function getPrixTotal()
    {
        return $this->prixTotal;
    }

    /**
     * @param Produit $produit
     */
    public function setProduit(Produit $produit)
    {
        $this->produit = $produit;
    }

    /**
     * @return Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param int $idProjet
     */
    public function setIdProjet($idProjet)
    {
        $this->idProjet = $idProjet;
    }

    /**
     * @return int
     */
    public function getIdProjet()
    {
        return $this->idProjet;
    }

    /**
     * @param \DateTime $dateSaisie
     */
    public function setDateSaisie($dateSaisie)
    {
        $this->dateSaisie = $dateSaisie;
    }

    /**
     * @return \DateTime
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }
}
