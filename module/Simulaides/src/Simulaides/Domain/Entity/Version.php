<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 10/02/15
 * Time: 09:20
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\VersionRepository")
 * @ORM\Table(name="version")
 * Class Version
 * @package Simulaides\Domain\Entities
 */
class Version
{
    /**
     * @ORM\Id
     * @var string
     * @ORM\Column(type="string", length=10)
     */
    private $version;

    /**
     * @param mixed $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

}
