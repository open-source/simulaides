<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 09/02/15
 * Time: 18:27
 */

namespace Simulaides\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Simulaides\Domain\Entity\Exception\InvalidListTypeException;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\ProjetRepository")
 * @ORM\Table(name="projet")
 * Class Projet
 * @package Simulaides\Domain\Entities
 */
class Projet
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Date d'évaluation de la simulation
     *
     * @var \DateTime
     * @ORM\Column(name="date_eval", type="date")
     */
    private $dateEval;
    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $revenus;

    /**
     * @var int
     * @ORM\Column(type="integer", name="nb_adulte")
     */
    private $nbAdultes;

    /**
     * @var string
     * @ORM\Column(type="string", length=5, name="code_postal")
     */
    private $codePostal;

    /**
     * @var string
     * @ORM\Column(type="string", length=6, name="code_commune")
     */
    private $codeCommune;

    /**
     * @var Commune
     * @ORM\ManyToOne(targetEntity="Commune")
     * @ORM\JoinColumn(name="code_commune", referencedColumnName="code_insee")
     */
    private $commune;

    /**
     * @var string
     * @ORM\Column(name="code_statut", type="string", length=50)
     */
    private $codeStatut;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_statut", referencedColumnName="code")
     */
    private $statut;

    /**
     * @var string
     * @ORM\Column(name="code_type_logement", type="string", length=50)
     */
    private $codeTypeLogement;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_type_logement", referencedColumnName="code")
     */
    private $logement;

    /**
     * @var string
     * @ORM\Column(name="nb_parts_fiscales", type="decimal", precision=3, scale=1, nullable=true)
     */
    private $nbPartsFiscales;

    /**
     * @var string
     * @ORM\Column(name="code_energie", type="string", length=50)
     */
    private $codeEnergie;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_energie", referencedColumnName="code")
     */
    private $energie;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_mode_chauff", referencedColumnName="code")
     */
    private $modechauffage;


    /**
     * Nombre de personne à charge
     * @var integer
     * @ORM\Column(name="nb_pers_charge", type="integer", length=2)
     */
    private $nbPersonnesCharge;

    /**
     * @var integer
     * @ORM\Column(name="nb_enfants_alterne", type="integer", length=2)
     */
    private $nbEnfantAlterne;

    /**
     * @var integer
     * @ORM\Column(name="annee_construction", type="integer", length=4)
     */
    private $anneeConstruction;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numero;

    /**
     * @var string
     * @ORM\Column(name="mot_passe", type="string", length=255)
     */
    private $motPasse;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $surfaceHabitable;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Travaux")
     * @ORM\JoinTable(
     *      name="travaux_projet",
     *      joinColumns={@ORM\JoinColumn(name="id_projet", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="code_travaux", referencedColumnName="code")})
     */
    private $travaux;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $bbc;

    /**
     * @var  ArrayCollection
     * @ORM\OneToMany(targetEntity="SaisieProduit", mappedBy="projet",
     *                cascade={"persist", "remove", "merge"},orphanRemoval=true)
     */
    private $saisiesProduit;

    /**
     * @var ArrayCollection $produits
     * @ORM\ManyToMany(targetEntity="Produit")
     * @ORM\JoinTable(name="projet_produit",
     *      joinColumns={@ORM\JoinColumn(name="projet_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="code_produit", referencedColumnName="code")}
     *      )
     */
    private $produits;

    /**
     * Intitulé de l'aide conseiller
     *
     * @var string
     * @ORM\Column(name="intitule_aide_conseiller", type="string", length=250, nullable=true)
     */
    private $intituleAideConseiller;

    /**
     * Descriptif de l'aide conseiller
     *
     * @var string
     * @ORM\Column(name="descriptif_aide_conseiller", type="text", nullable=true)
     */
    private $descriptifAideConseiller;

    /**
     * Montant de l'aide conseiller
     *
     * @var float
     * @ORM\Column(name="mt_aide_conseiller", type="decimal", nullable=true)
     */
    private $mtAideConseiller;

    /**
     * Autre commentaire de l'aide conseiller
     *
     * @var string
     * @ORM\Column(name="autre_commentaire_conseiller", type="text", nullable=true)
     */
    private $autreCommentaireConseiller;

    /**
     * Bénéfice d'un prêt à taux 0
     * @var boolean
     * @ORM\Column(name="benefice_ptz", type="boolean")
     */
    private $beneficePtz;

    /**
     * Montant des travaux ayant bénéficié d'un crédit d'impôt
     *
     * @var int
     * @ORM\Column(name="montant_benefice_ci", type="integer", nullable=true)
     */
    private $montantBeneficeCi;

    /**
     * Primo accédant
     * @var boolean
     * @ORM\Column(name="primo_accession", type="boolean")
     */
    private $primoAccession;

    /**
     * SAlarie du secteur prive
     * @var boolean
     * @ORM\Column(name="salarie_secteur_prive", type="boolean")
     */
    private $salarieSecteurPrive;

    /**
     * Nom du conseiller PRIS ayant réalisé la simulation
     *
     * @var string
     * @ORM\Column(name="pris_nom", type="string", length=100, nullable=true)
     */
    private $prisNom;

    /**
     * Email du conseiller PRIS ayant réalisé la simulation
     *
     * @var string
     * @ORM\Column(name="pris_email", type="string", length=255, nullable=true)
     */
    private $prisEmail;

    /**
     * Téléphone du conseiller PRIS ayant réalisé la simulation
     *
     * @var string
     * @ORM\Column(name="pris_tel", type="string", length=20, nullable=true)
     */
    private $prisTel;

    /**
     * Fichier de log lié à la simulation
     *
     * @var
     * @ORM\Column(name="fichier_log", type="blob", nullable=true)
     */
    private $fichierLog;
    /**
     * Code du mode chauffage
     *
     * @var string
     * @ORM\Column(name="code_mode_chauff", type="string", length=50, nullable=false)
     */
    private $codeModeChauff;


    /**
     * Partenaire à travers duquel la simulation a été créée
     *
     * @ORM\ManyToOne(targetEntity="Partenaire")
     * @ORM\JoinColumn(name="partenaire", referencedColumnName="id")
     */
    private $partenaire;

    /**
     * Liste des choix de dispostifs du projet
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ProjetChoixDispositif", mappedBy="projet",
     *                cascade={"persist", "remove", "merge"},orphanRemoval=true)
     */
    private $choixDispositifs;

    /**
     *
     */
    public function __construct()
    {
        $this->travaux = new ArrayCollection();
        $this->saisiesProduit = new ArrayCollection();
        $this->produits = new ArrayCollection();
        $this->choixDispositifs = new ArrayCollection();
    }

    public function __clone()
    {
        if ($this->id) {
            $this->setId(null);
            $this->setNumero(null);
            $this->setMotPasse(null);

            // Clone des saisies produit
            $saisiesProduitClone = new ArrayCollection();
            /* @var SaisieProduit $saisieProduit */
            foreach ($this->saisiesProduit as $saisieProduit) {
                $saisieProduitClone = clone $saisieProduit;
                $saisieProduitClone->setIdProjet($this->getId());
                $saisiesProduitClone->add($saisieProduitClone);
            }
            $this->saisiesProduit = $saisiesProduitClone;


            $choixDispositifsClone = new ArrayCollection();
            /* @var ProjetChoixDispositif $choixDispositif */
            foreach ($this->choixDispositifs as $choixDispositif) {
                $choixDispositifClone = clone $choixDispositif;
                $choixDispositifClone->setIdProjet($this->getId());
                $choixDispositifsClone->add($choixDispositifClone);
            }
            $this->choixDispositifs = $choixDispositifsClone;
        }
    }

    /**
     * @param mixed $anneeConstruct
     */
    public function setAnneeConstruction($anneeConstruct)
    {
        $this->anneeConstruction = $anneeConstruct;
    }

    /**
     * @return mixed
     */
    public function getAnneeConstruction()
    {
        return $this->anneeConstruction;
    }

    /**
     * @param mixed $codeEnergie
     */
    public function setCodeEnergie($codeEnergie)
    {
        $this->codeEnergie = $codeEnergie;
    }

    /**
     * @return mixed
     */
    public function getCodeEnergie()
    {
        return $this->codeEnergie;
    }

    /**
     * @param mixed $codeStatut
     */
    public function setCodeStatut($codeStatut)
    {
        $this->codeStatut = $codeStatut;
    }

    /**
     * @return mixed
     */
    public function getCodeStatut()
    {
        return $this->codeStatut;
    }

    /**
     * @param mixed $codeTypeLogement
     */
    public function setCodeTypeLogement($codeTypeLogement)
    {
        $this->codeTypeLogement = $codeTypeLogement;
    }

    /**
     * @return mixed
     */
    public function getCodeTypeLogement()
    {
        return $this->codeTypeLogement;
    }

    /**
     * @param mixed $motPasse
     */
    public function setMotPasse($motPasse)
    {
        $this->motPasse = $motPasse;
    }

    /**
     * @return mixed
     */
    public function getMotPasse()
    {
        return $this->motPasse;
    }

    /**
     * @param mixed $nbEnfantAlterne
     */
    public function setNbEnfantAlterne($nbEnfantAlterne)
    {
        $this->nbEnfantAlterne = $nbEnfantAlterne;
    }

    /**
     * @return mixed
     */
    public function getNbEnfantAlterne()
    {
        return $this->nbEnfantAlterne;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $surfaceHabitable
     */
    public function setSurfaceHabitable($surfaceHabitable)
    {
        $this->surfaceHabitable = $surfaceHabitable;
    }

    /**
     * @return mixed
     */
    public function getSurfaceHabitable()
    {
        return $this->surfaceHabitable;
    }

    /**
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * @param string $codePostal
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;
    }

    /**
     * @param mixed $codeCommune
     */
    public function setCodeCommune($codeCommune)
    {
        $this->codeCommune = $codeCommune;
    }

    /**
     * @return mixed
     */
    public function getCodeCommune()
    {
        return $this->codeCommune;
    }

    /**
     * @param Commune $commune
     */
    public function setCommune(Commune $commune)
    {
        $this->commune = $commune;
    }

    /**
     * @return Commune
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $nbAdultes
     */
    public function setNbAdultes($nbAdultes)
    {
        $this->nbAdultes = $nbAdultes;
    }

    /**
     * @return mixed
     */
    public function getNbAdultes()
    {
        return $this->nbAdultes;
    }

    /**
     * @param mixed $revenus
     */
    public function setRevenus($revenus)
    {
        $this->revenus = $revenus;
    }

    /**
     * @return mixed
     */
    public function getRevenus()
    {
        return $this->revenus;
    }

    /**
     * @param ArrayCollection $travaux
     */
    public function setTravaux(ArrayCollection $travaux)
    {
        $this->travaux = $travaux;
    }

    /**
     * @return ArrayCollection
     */
    public function getTravaux()
    {
        return $this->travaux;
    }


    /**
     * @param ValeurListe $energie
     *
     * @throws Exception\InvalidListTypeException
     */
    public function setEnergie(ValeurListe $energie)
    {
        if ($energie->getCodeListe() === '') {
            throw new InvalidListTypeException();
        }
        $this->energie = $energie;
    }

    /**
     * @return ValeurListe
     */
    public function getEnergie()
    {
        return $this->energie;
    }
    /**
     * @return ValeurListe
     */
    public function getModechauffage()

    {
        return $this->modechauffage;
    }

    /**
     * @param ValeurListe $modechauffage
     */
    public function setModechauffage($modechauffage)
    {
        if ($modechauffage->getCodeListe() === '') {
            throw new InvalidListTypeException();
        }
        $this->modechauffage = $modechauffage;
    }
    /**
     * @param ValeurListe $logement
     *
     * @throws Exception\InvalidListTypeException
     */
    public function setLogement(ValeurListe $logement)
    {
        if ($logement->getCodeListe() === '') {
            throw new InvalidListTypeException();
        }
        $this->logement = $logement;
    }

    /**
     * @return ValeurListe
     */
    public function getLogement()
    {
        return $this->logement;
    }

    /**
     * @return string
     */
    public function getNbPartsFiscales()
    {
        return $this->nbPartsFiscales;
    }

    /**
     * @param string $nbPartsFiscales
     */
    public function setNbPartsFiscales($nbPartsFiscales)
    {
        $this->nbPartsFiscales = $nbPartsFiscales;
    }

    /**
     * @param ValeurListe $statut
     *
     * @throws Exception\InvalidListTypeException
     */
    public function setStatut(ValeurListe $statut)
    {
        if ($statut->getCodeListe() === '') {
            throw new InvalidListTypeException();
        }
        $this->statut = $statut;
    }

    /**
     * @return ValeurListe
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param boolean $bbc
     */
    public function setBbc($bbc)
    {
        $this->bbc = $bbc;
    }

    /**
     * @return boolean
     */
    public function getBbc()
    {
        return $this->bbc;
    }

    /**
     * @param ArrayCollection $saisiesProduit
     */
    public function setSaisiesProduit($saisiesProduit)
    {
        $this->saisiesProduit = $saisiesProduit;
    }

    /**
     * @return ArrayCollection
     */
    public function getSaisiesProduit()
    {
        return $this->saisiesProduit;
    }


    /**
     * @param SaisieProduit $saisieProduit
     */
    public function addSaisiesProduit(SaisieProduit $saisieProduit)
    {
        $this->saisiesProduit[] = $saisieProduit;
    }

    /**
     * @param SaisieProduit $saisieProduit
     */
    public function removeSaisiesProduit(SaisieProduit $saisieProduit)
    {
        $this->saisiesProduit->removeElement($saisieProduit);
    }

    /**
     * @param ArrayCollection $produits
     */
    public function setProduits($produits)
    {
        $this->produits = $produits;
    }

    /**
     * @return ArrayCollection
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * @param Produit $produit
     */
    public function addProduit(Produit $produit)
    {
        $this->produits[] = $produit;
    }

    /**
     * @param Produit $produit
     */
    public function removeProduit(Produit $produit)
    {
        $this->produits->removeElement($produit);
    }

    /**
     * Permet de supprimer la liste des produits
     */
    public function removeAllProduit()
    {
        foreach ($this->produits as $produit) {
            $this->produits->removeElement($produit);
        }
    }

    /**
     * @param Travaux $travaux
     */
    public function addTravaux(Travaux $travaux)
    {
        $this->travaux[] = $travaux;
    }

    /**
     * @param Travaux $travaux
     */
    public function removeTravaux(Travaux $travaux)
    {
        $this->travaux->removeElement($travaux);
    }

    /**
     * Permet de supprimer la liste des travaux
     */
    public function removeAllTravaux()
    {
        foreach ($this->travaux as $travaux) {
            $this->travaux->removeElement($travaux);
        }
    }

    /**
     * @param mixed $autreCommentaireConseiller
     */
    public function setAutreCommentaireConseiller($autreCommentaireConseiller)
    {
        $this->autreCommentaireConseiller = $autreCommentaireConseiller;
    }

    /**
     * @return mixed
     */
    public function getAutreCommentaireConseiller()
    {
        return $this->autreCommentaireConseiller;
    }

    /**
     * @param string $descriptifAideConseiller
     */
    public function setDescriptifAideConseiller($descriptifAideConseiller)
    {
        $this->descriptifAideConseiller = $descriptifAideConseiller;
    }

    /**
     * @return string
     */
    public function getDescriptifAideConseiller()
    {
        return $this->descriptifAideConseiller;
    }

    /**
     * @param string $intituleAideConseiller
     */
    public function setIntituleAideConseiller($intituleAideConseiller)
    {
        $this->intituleAideConseiller = $intituleAideConseiller;
    }

    /**
     * @return string
     */
    public function getIntituleAideConseiller()
    {
        return $this->intituleAideConseiller;
    }

    /**
     * @param float $mtAideConseiller
     */
    public function setMtAideConseiller($mtAideConseiller)
    {
        $this->mtAideConseiller = $mtAideConseiller;
    }

    /**
     * @return float
     */
    public function getMtAideConseiller()
    {
        return $this->mtAideConseiller;
    }

    /**
     * @param mixed $dateEval
     */
    public function setDateEval($dateEval)
    {
        $this->dateEval = $dateEval;
    }

    /**
     * @return mixed
     */
    public function getDateEval()
    {
        return $this->dateEval;
    }

    /**
     * @param boolean $beneficePtz
     */
    public function setBeneficePtz($beneficePtz)
    {
        $this->beneficePtz = $beneficePtz;
    }

    /**
     * @return boolean
     */
    public function getBeneficePtz()
    {
        return $this->beneficePtz;
    }

    /**
     * @param int $montantBeneficeCi
     */
    public function setMontantBeneficeCi($montantBeneficeCi)
    {
        $this->montantBeneficeCi = $montantBeneficeCi;
    }

    /**
     * @return int
     */
    public function getMontantBeneficeCi()
    {
        return $this->montantBeneficeCi;
    }

    /**
     * @param boolean $primoAccession
     */
    public function setPrimoAccession($primoAccession)
    {
        $this->primoAccession = $primoAccession;
    }

    /**
     * @return boolean
     */
    public function getPrimoAccession()
    {
        return $this->primoAccession;
    }

    /**
     * @return bool
     */
    public function getSalarieSecteurPrive()
    {
        return $this->salarieSecteurPrive;
    }

    /**
     * @param bool $salarieSecteurPrive
     */
    public function setSalarieSecteurPrive($salarieSecteurPrive)
    {
        $this->salarieSecteurPrive = $salarieSecteurPrive;
    }


    /**
     * @param mixed $prisEmail
     */
    public function setPrisEmail($prisEmail)
    {
        $this->prisEmail = $prisEmail;
    }

    /**
     * @return mixed
     */
    public function getPrisEmail()
    {
        return $this->prisEmail;
    }

    /**
     * @param string $prisNom
     */
    public function setPrisNom($prisNom)
    {
        $this->prisNom = $prisNom;
    }

    /**
     * @return string
     */
    public function getPrisNom()
    {
        return $this->prisNom;
    }

    /**
     * @param mixed $prisTel
     */
    public function setPrisTel($prisTel)
    {
        $this->prisTel = $prisTel;
    }

    /**
     * @return mixed
     */
    public function getPrisTel()
    {
        return $this->prisTel;
    }

    /**
     * @param int $nbPersonnesCharge
     */
    public function setNbPersonnesCharge($nbPersonnesCharge)
    {
        $this->nbPersonnesCharge = $nbPersonnesCharge;
    }

    /**
     * @return int
     */
    public function getNbPersonnesCharge()
    {
        return $this->nbPersonnesCharge;
    }

    /**
     * @param mixed $fichierLog
     */
    public function setFichierLog($fichierLog)
    {
        $this->fichierLog = $fichierLog;
    }

    /**
     * @return mixed
     */
    public function getFichierLog()
    {
        return $this->fichierLog;
    }

    /**
     * @return mixed
     */
    public function getPartenaire()
    {
        return $this->partenaire;
    }

    /**
     * @param mixed $partenaire
     */
    public function setPartenaire($partenaire)
    {
        $this->partenaire = $partenaire;
    }

    /**
     * @return Collection
     */
    public function getChoixDispositifs()
    {
        return $this->choixDispositifs;
    }

    /**
     * @param Collection $choixDispositifs
     */
    public function setChoixDispositifs($choixDispositifs)
    {
        $this->choixDispositifs = $choixDispositifs;
    }

    /**
     * @return string
     */
    public function getCodeModeChauffage()
    {
        return $this->codeModeChauff;
    }

    /**
     * @param string $codeModeChauffage
     */
    public function setCodeModeChauffage($codeModeChauffage)
    {
        $this->codeModeChauff = $codeModeChauffage;
    }

    /**
     * @param ProjetChoixDispositif $choixDispositif
     */
    public function addChoixDispositif(ProjetChoixDispositif $choixDispositif)
    {
        $this->choixDispositifs[] = $choixDispositif;
    }

}
