<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 10/02/15
 * Time: 14:37
 */

namespace Simulaides\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\EpciRepository")
 * @ORM\Table(name="epci")
 * Class Epci
 * @package Simulaides\Domain\Entities
 */
class Epci
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=9)
     */
    private $code;
    /**
     * @var string
     * @ORM\Column(type="string", length=200)
     */
    private $nom;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Commune")
     * @ORM\JoinTable(name="epci_commune",
     *      joinColumns={@ORM\JoinColumn(name="code_epci", referencedColumnName="code")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="code_commune", referencedColumnName="code_insee")}
     *      )
     */
    private $communes;

    /**
     * constructeur
     */
    public function __construct()
    {
        $this->communes = new ArrayCollection();
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->code;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param ArrayCollection $communes
     */
    public function setCommunes(ArrayCollection $communes)
    {
        $this->communes = $communes;
    }

    /**
     * @return ArrayCollection
     */
    public function getCommunes()
    {
        return $this->communes;
    }

    /**
     * Ajout d'une commune
     * @param Commune $commune
     */
    public function addCommunes(Commune $commune)
    {
        $this->communes[] = $commune;
    }
}
