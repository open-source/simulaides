<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 10/02/15
 * Time: 17:31
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity (repositoryClass="Simulaides\Domain\Repository\KwhCumacRepository")
 * @ORM\Table(name="kwh_cumac")
 * Class KwhCumac
 * @package Simulaides\Domain\Entities
 */
class KwhCumac
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="code_travaux", type="string", length=3, nullable=true)
     */
    private $codeTravaux;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $caracteristique;

    /**
     * @var string
     * @ORM\Column(name="code_energie", type="string", length=50, nullable=true)
     */
    private $codeEnergie;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_energie", referencedColumnName="code")
     */
    private $energie;

    /**
     * @var string
     * @ORM\Column(name="code_type_logement", type="string", length=50, nullable=true)
     */
    private $codeTypeLogement;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_type_logement", referencedColumnName="code")
     */
    private $typeLogement;

    /**
     * @var string
     * @ORM\Column(name="code_mode_chauffage", type="string", length=50, nullable=true)
     */
    private $codeModeChauffage;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="code_mode_chauffage", referencedColumnName="code")
     */
    private $modeChauffage;

    /**
     * @var string
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $zone;

    /**
     * @var integer
     * @ORM\Column(name="nb_kwh", type="integer")
     */
    private $nbKwh;

    /**
     * @var Travaux
     * @ORM\ManyToOne(targetEntity="Travaux")
     * @ORM\JoinColumn(name="code_travaux", referencedColumnName="code")
     */
    private $travaux;

    /**
     * @param string $caracteristique
     */
    public function setCaracteristique($caracteristique)
    {
        $this->caracteristique = $caracteristique;
    }

    /**
     * @return string
     */
    public function getCaracteristique()
    {
        return $this->caracteristique;
    }

    /**
     * @param string $codeTravaux
     */
    public function setCodeTravaux($codeTravaux)
    {
        $this->codeTravaux = $codeTravaux;
    }

    /**
     * @return string
     */
    public function getCodeTravaux()
    {
        return $this->codeTravaux;
    }

    /**
     * @param string $codeEnergie
     */
    public function setCodeEnergie($codeEnergie)
    {
        $this->codeEnergie = $codeEnergie;
    }

    /**
     * @return string
     */
    public function getCodeEnergie()
    {
        return $this->codeEnergie;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $codeModeChauffage
     */
    public function setCodeModeChauffage($codeModeChauffage)
    {
        $this->codeModeChauffage = $codeModeChauffage;
    }

    /**
     * @return string
     */
    public function getCodeModeChauffage()
    {
        return $this->codeModeChauffage;
    }

    /**
     * @param int $nbKwh
     */
    public function setNbKwh($nbKwh)
    {
        $this->nbKwh = $nbKwh;
    }

    /**
     * @return int
     */
    public function getNbKwh()
    {
        return $this->nbKwh;
    }

    /**
     * @param Travaux $travaux
     */
    public function setTravaux(Travaux $travaux)
    {
        $this->travaux = $travaux;
    }

    /**
     * @return Travaux
     */
    public function getTravaux()
    {
        return $this->travaux;
    }

    /**
     * @param string $codeTypeLogement
     */
    public function setCodeTypeLogement($codeTypeLogement)
    {
        $this->codeTypeLogement = $codeTypeLogement;
    }

    /**
     * @return string
     */
    public function getCodeTypeLogement()
    {
        return $this->codeTypeLogement;
    }

    /**
     * @param string $zone
     */
    public function setZone($zone)
    {
        $this->zone = $zone;
    }

    /**
     * @return string
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @param ValeurListe $energie
     */
    public function setEnergie(ValeurListe $energie)
    {
        $this->energie = $energie;
    }

    /**
     * @return ValeurListe
     */
    public function getEnergie()
    {
        return $this->energie;
    }

    /**
     * @param ValeurListe $modeChauffage
     */
    public function setModeChauffage(ValeurListe $modeChauffage)
    {
        $this->modeChauffage = $modeChauffage;
    }

    /**
     * @return ValeurListe
     */
    public function getModeChauffage()
    {
        return $this->modeChauffage;
    }

    /**
     * @param ValeurListe $orgaChauffage
     */
    public function setOrgaChauffage(ValeurListe $orgaChauffage)
    {
        $this->orgaChauffage = $orgaChauffage;
    }

    /**
     * @return ValeurListe
     */
    public function getOrgaChauffage()
    {
        return $this->orgaChauffage;
    }

    /**
     * @param ValeurListe $typeLogement
     */
    public function setTypeLogement(ValeurListe $typeLogement)
    {
        $this->typeLogement = $typeLogement;
    }

    /**
     * @return ValeurListe
     */
    public function getTypeLogement()
    {
        return $this->typeLogement;
    }
}
