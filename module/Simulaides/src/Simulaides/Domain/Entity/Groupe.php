<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 04/03/15
 * Time: 12:14
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\GroupeRepository")
 * @ORM\Table(name="groupe")
 * Class Groupe
 * @package Simulaides\Domain\Entities
 */
class Groupe
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private $id;

    /**
     * @var string $codeRegion
     * @ORM\Column(name="code_region", type="string", length=2)
     */
    private $codeRegion;
    /**
     * @var Region $region
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(name="code_region", referencedColumnName="code")
     */
    private $region;
    /**
     * @var int $idDispositif
     * @ORM\Column(name="id_dispositif", type="integer", length=11)
     */
    private $idDispositif;
    /**
     * @var Dispositif $dispositif
     * @ORM\ManyToOne(targetEntity="Dispositif")
     * @ORM\JoinColumn(name="id_dispositif", referencedColumnName="id")
     */
    private $dispositif;
    /**
     * @var int
     * @ORM\Column(name="numero_ordre", type="smallint", length=2)
     */
    private $numeroOrdre;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $codeRegion
     */
    public function setCodeRegion($codeRegion)
    {
        $this->codeRegion = $codeRegion;
    }

    /**
     * @return string
     */
    public function getCodeRegion()
    {
        return $this->codeRegion;
    }

    /**
     * @param Dispositif $dispositif
     */
    public function setDispositif(Dispositif $dispositif)
    {
        $this->dispositif = $dispositif;
    }

    /**
     * @return Dispositif
     */
    public function getDispositif()
    {
        return $this->dispositif;
    }

    /**
     * @param int $idDispositif
     */
    public function setIdDispositif($idDispositif)
    {
        $this->idDispositif = $idDispositif;
    }

    /**
     * @return int
     */
    public function getIdDispositif()
    {
        return $this->idDispositif;
    }

    /**
     * @param int $numeroOrde
     */
    public function setNumeroOrdre($numeroOrde)
    {
        $this->numeroOrdre = $numeroOrde;
    }

    /**
     * @return int
     */
    public function getNumeroOrdre()
    {
        return $this->numeroOrdre;
    }

    /**
     * @param Region $region
     */
    public function setRegion(Region $region)
    {
        $this->region = $region;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }
}
