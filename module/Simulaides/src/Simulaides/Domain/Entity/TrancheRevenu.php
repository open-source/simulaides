<?php
/**
 * Cette classe permet de gérer les tranches de revenu
 *
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 04/03/15
 * Time: 12:05
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\TrancheRevenuRepository")
 * @ORM\Table(name="tranche_revenu")
 * Class TrancheRevenu
 * @package Simulaides\Domain\Entities
 */
class TrancheRevenu
{
    /**
     * Code de la tranche
     *
     * @var string
     * @ORM\Id
     * @ORM\Column(name="code_tranche", type="string", length=15, nullable=false)
     */
    private $codeTranche;

    /**
     * Code du groupe de tranche
     *
     * @var string
     * @ORM\Column(name="code_groupe", type="string", length=15)
     */
    private $codeGroupe;

    /**
     * Groupe de tranche
     *
     * @var TrancheGroupe
     * @ORM\ManyToOne(targetEntity="TrancheGroupe")
     * @ORM\JoinColumn(name="code_groupe", referencedColumnName="code_groupe")
     */
    private $groupe;

    /**
     * @var string
     * @ORM\Column(type="string", length=150)
     */
    private $intitule;

    /**
     * Montant de la tranche pour une personne dans le foyer
     *
     * @var int
     * @ORM\Column(name="mt_1_personne", type="integer")
     */
    private $mt1personne;

    /**
     * Montant de la tranche pour deux personnes dans le foyer
     *
     * @var int
     * @ORM\Column(name="mt_2_personne", type="integer")
     */
    private $mt2personne;

    /**
     * Montant de la tranche pour trois personnes dans le foyer
     *
     * @var int
     * @ORM\Column(name="mt_3_personne", type="integer")
     */
    private $mt3personne;

    /**
     * Montant de la tranche pour quatres personnes dans le foyer
     *
     * @var int
     * @ORM\Column(name="mt_4_personne", type="integer")
     */
    private $mt4personne;

    /**
     * Montant de la tranche pour cinq personnes dans le foyer
     *
     * @var int
     * @ORM\Column(name="mt_5_personne", type="integer")
     */
    private $mt5personne;

    /**
     * Montant de la tranche pour six personnes dans le foyer
     *
     * @var int
     * @ORM\Column(name="mt_6_personne", type="integer")
     */
    private $mt6personne;

    /**
     * Montant de la tranche pour les personnes supplémentaire dans le foyer
     *
     * @var int
     * @ORM\Column(name="mt_personne_supp", type="integer")
     */
    private $mtPersonneSupp;

    /**
     * @param int $mt1personne
     */
    public function setMt1personne($mt1personne)
    {
        $this->mt1personne = $mt1personne;
    }

    /**
     * @return int
     */
    public function getMt1personne()
    {
        return $this->mt1personne;
    }

    /**
     * @param int $mt2personne
     */
    public function setMt2personne($mt2personne)
    {
        $this->mt2personne = $mt2personne;
    }

    /**
     * @return int
     */
    public function getMt2personne()
    {
        return $this->mt2personne;
    }

    /**
     * @param int $mt3personne
     */
    public function setMt3personne($mt3personne)
    {
        $this->mt3personne = $mt3personne;
    }

    /**
     * @return int
     */
    public function getMt3personne()
    {
        return $this->mt3personne;
    }

    /**
     * @param int $mt4personne
     */
    public function setMt4personne($mt4personne)
    {
        $this->mt4personne = $mt4personne;
    }

    /**
     * @return int
     */
    public function getMt4personne()
    {
        return $this->mt4personne;
    }

    /**
     * @param int $mt5personne
     */
    public function setMt5personne($mt5personne)
    {
        $this->mt5personne = $mt5personne;
    }

    /**
     * @return int
     */
    public function getMt5personne()
    {
        return $this->mt5personne;
    }

    /**
     * @param int $mt6personne
     */
    public function setMt6personne($mt6personne)
    {
        $this->mt6personne = $mt6personne;
    }

    /**
     * @return int
     */
    public function getMt6personne()
    {
        return $this->mt6personne;
    }

    /**
     * @param int $mtPersonneSupp
     */
    public function setMtPersonneSupp($mtPersonneSupp)
    {
        $this->mtPersonneSupp = $mtPersonneSupp;
    }

    /**
     * @return int
     */
    public function getMtPersonneSupp()
    {
        return $this->mtPersonneSupp;
    }

    /**
     * @param mixed $codeTranche
     */
    public function setCodeTranche($codeTranche)
    {
        $this->codeTranche = $codeTranche;
    }

    /**
     * @return mixed
     */
    public function getCodeTranche()
    {
        return $this->codeTranche;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param mixed $codeGroupe
     */
    public function setCodeGroupe($codeGroupe)
    {
        $this->codeGroupe = $codeGroupe;
    }

    /**
     * @return mixed
     */
    public function getCodeGroupe()
    {
        return $this->codeGroupe;
    }

    /**
     * @param \Simulaides\Domain\Entity\TrancheGroupe $groupe
     */
    public function setGroupe($groupe)
    {
        $this->groupe = $groupe;
    }

    /**
     * @return \Simulaides\Domain\Entity\TrancheGroupe
     */
    public function getGroupe()
    {
        return $this->groupe;
    }
}
