<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 09/02/15
 * Time: 16:27
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\DispositifRegleRepository")
 * @ORM\Table(name="dispositif_regle")
 * Class DispositifRegle
 * @package Simulaides\Domain\Entities
 */
class DispositifRegle
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Identifiant du dispositif
     * @var int
     * @ORM\Column(type="integer", name="id_dispositif", length=11)
     */
    private $idDispositif;

    /**
     * @var Dispositif
     * @ORM\ManyToOne(targetEntity="Dispositif")
     * @ORM\JoinColumn(name="id_dispositif", referencedColumnName="id")
     */
    private $dispositif;

    /**
     * @var string
     * @ORM\Column(type="string", length=1)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="condition_regle",type="text", nullable=true)
     */
    private $conditionRegle;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $expression;

    /**
     * @param mixed $idDispositif
     */
    public function setIdDispositif($idDispositif)
    {
        $this->idDispositif = $idDispositif;
    }

    /**
     * @return mixed
     */
    public function getIdDispositif()
    {
        return $this->idDispositif;
    }

    /**
     * @param string $conditionRegle
     */
    public function setConditionRegle($conditionRegle)
    {
        $this->conditionRegle = $conditionRegle;
    }

    /**
     * @return string
     */
    public function getConditionRegle()
    {
        return $this->conditionRegle;
    }

    /**
     * @param mixed $expression
     */
    public function setExpression($expression)
    {
        $this->expression = $expression;
    }

    /**
     * @return mixed
     */
    public function getExpression()
    {
        return $this->expression;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Dispositif $dispositif
     */
    public function setDispositif(Dispositif $dispositif)
    {
        $this->dispositif = $dispositif;
    }

    /**
     * @return Dispositif
     */
    public function getDispositif()
    {
        return $this->dispositif;
    }
}
