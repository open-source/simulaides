<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 09/02/15
 * Time: 18:29
 */

namespace Simulaides\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\CommuneRepository")
 * @ORM\Table(name="commune")
 * Class Commune
 * @package Simulaides\Domain\Entities
 */
class Commune
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="code_insee", type="string", length=6)
     */
    private $codeInsee;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     */
    private $nom;

    /**
     * @var string
     * @ORM\Column(name="code_departement", type="string", length=3)
     */
    private $codeDepartement;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Departement")
     * @ORM\JoinColumn(name="code_departement", referencedColumnName="code")
     */
    private $departement;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Epci")
     * @ORM\JoinTable(name="epci_commune",
     *      joinColumns={@ORM\JoinColumn(name="code_commune", referencedColumnName="code_insee")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="code_epci", referencedColumnName="code")}
     *      )
     */
    private $epcis;

    public function __construct(){
        $this->epcis = new ArrayCollection();
    }
    /**
     * @param mixed $codeDepartement
     */
    public function setCodeDepartement($codeDepartement)
    {
        $this->codeDepartement = $codeDepartement;
    }

    /**
     * @return mixed
     */
    public function getCodeDepartement()
    {
        return $this->codeDepartement;
    }

    /**
     * @param mixed $codeInsee
     */
    public function setCodeInsee($codeInsee)
    {
        $this->codeInsee = $codeInsee;
    }

    /**
     * @return mixed
     */
    public function getCodeInsee()
    {
        return $this->codeInsee;
    }

    /**
     * @param Departement $departement
     */
    public function setDepartement(Departement $departement)
    {
        $this->departement = $departement;
    }

    /**
     * @return Departement
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getEpcis()
    {
        return $this->epcis;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $epcis
     */
    public function setEpcis($epcis)
    {
        $this->epcis = $epcis;
    }


}
