<?php
/**
 * Created by PhpStorm.
 * User: meven.cadare
 * Date: 13/02/15
 * Time: 10:01
 */

namespace Simulaides\Domain\Entity\Behavior;

/**
 * Interface TimestampableInterface
 * permet de vérifier si l'entité possède les méthodes implémentées par le trait TimestampableTrait
 * @package Simulaides\Domain\Entity\Behavior
 */
interface TimestampableInterface
{
    /**
     * @param \Datetime $dateCreation
     */
    public function setDateCreation($dateCreation);

    /**
     * @return \Datetime
     */
    public function getDateCreation();

    /**
     * @param \Datetime $dateMaj
     */
    public function setDateMaj($dateMaj);

    /**
     * @return \Datetime
     */
    public function getDateMaj();
}
