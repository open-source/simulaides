<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 17:03
 */

namespace Simulaides\Domain\Entity\Behavior;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class TimestampableTrait
 * @package Simulaides\Domain\Entity\Behavior
 */
trait TimestampableTrait
{
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="date_creation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="date_maj", type="datetime")
     */
    private $dateMaj;

    /**
     * @param \Datetime $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return \Datetime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param \Datetime $dateMaj
     */
    public function setDateMaj($dateMaj)
    {
        $this->dateMaj = $dateMaj;
    }

    /**
     * @return \Datetime
     */
    public function getDateMaj()
    {
        return $this->dateMaj;
    }
}
