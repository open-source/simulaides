<?php
namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe CodePostal
 *
 * Projet : SimulAides
 
 *
 * @author
 * @package   Simulaides\Domain\Entity
 *
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\CodePostalRepository")
 * @ORM\Table(name="code_postal")
 */
class CodePostal
{
    /**
     * Identifiant unique
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private $id;
    
    /**
     * @var string
     * @ORM\Column(name="code_postal", type="string", length=5)
     */
    private $codePostal;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Commune")
     * @ORM\JoinColumn(name="code_insee", referencedColumnName="code_insee")
     */
    private $codeInsee;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CodePostal
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * @param string $codePostal
     * @return CodePostal
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodeInsee()
    {
        return $this->codeInsee;
    }

    /**
     * @param string $codeInsee
     * @return CodePostal
     */
    public function setCodeInsee($codeInsee)
    {
        $this->codeInsee = $codeInsee;
        return $this;
    }
}
