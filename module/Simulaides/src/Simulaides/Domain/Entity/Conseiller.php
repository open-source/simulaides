<?php
namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe Conseiller
 *
 * Projet : Simul'Aides
 
 *
 * @author
 * @package   Simulaides\Domain\Entity
 * 
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\ConseillerRepository")
 * @ORM\Table(name="conseiller")
 */
class Conseiller
{
    /**
     * Identifiant unique provenant de la base PRIS
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer", length=10)
     */
    private $id;

    /**
     * Indique si les CGU on été acceptées par le conseiller
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $cgu_accepte;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Conseiller
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCguAccepte()
    {
        return $this->cgu_accepte;
    }

    /**
     * @param mixed $cgu_accepte
     * @return Conseiller
     */
    public function setCguAccepte($cgu_accepte)
    {
        $this->cgu_accepte = $cgu_accepte;
        return $this;
    }
}
