<?php
namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe DispTravauxRegle
 * Permet de gérer les règles sur certains des travaux pris en
 * charge par le dispositif
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Entity
 */

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\DispTravauxRegleRepository")
 * @ORM\Table(name="disp_travaux_regle")
 * Class DispTravauxRegle
 * @package Simulaides\Domain\Entities
 */
class DispTravauxRegle
{
    /**
     * Identifiant unique
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private $id;

    /**
     * Identifiant du travaux dans le dispositif
     * @var int
     * @ORM\Column(type="integer", name="id_disp_travaux", length=11, nullable=true)
     */
    private $idDispTravaux;

    /**
     * Travaux du dispositif
     * @ORM\ManyToOne(targetEntity="DispositifTravaux")
     * @ORM\JoinColumn(name="id_disp_travaux", referencedColumnName="id")
     * @var
     */
    private $dispTravaux;

    /**
     * @var string
     * @ORM\Column(type="string", length=2)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="condition_regle",type="text", nullable=true)
     */
    private $condition_regle;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $expression;

    /**
     * @param mixed $dispTravaux
     */
    public function setDispTravaux($dispTravaux)
    {
        $this->dispTravaux = $dispTravaux;
    }

    /**
     * @return mixed
     */
    public function getDispTravaux()
    {
        return $this->dispTravaux;
    }

    /**
     * @param string $condition_regle
     */
    public function setConditionRegle($condition_regle)
    {
        $this->condition_regle = $condition_regle;
    }

    /**
     * @return string
     */
    public function getConditionRegle()
    {
        return $this->condition_regle;
    }

    /**
     * @param string $expression
     */
    public function setExpression($expression)
    {
        $this->expression = $expression;
    }

    /**
     * @return string
     */
    public function getExpression()
    {
        return $this->expression;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $idDispTravaux
     */
    public function setIdDispTravaux($idDispTravaux)
    {
        $this->idDispTravaux = $idDispTravaux;
    }

    /**
     * @return int
     */
    public function getIdDispTravaux()
    {
        return $this->idDispTravaux;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
