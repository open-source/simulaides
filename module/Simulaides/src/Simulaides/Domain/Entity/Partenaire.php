<?php
namespace Simulaides\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Classe Partenaire
 *
 * Projet : Simul'Aides
 * @package   Simulaides\Domain\Entity
 * 
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\PartenaireRepository")
 * @ORM\Table(name="partenaire")
 */
class Partenaire
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", length=11)
     */
    private $id;

    /**
     * @ORM\Column(name="url", type="string", length=300, nullable=false)
     */
    private $url;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    private $nom;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="cle", type="string", length=100, nullable=true)
     */
    private $cle;

    /**
     * @var Region
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(name="code_region", referencedColumnName="code", nullable=true)
     */
    private $region;

    /**
     * @var string
     * @ORM\Column(name="cle_api", type="string", length=100, nullable=true)
     */
    private $cleApi;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Projet", mappedBy="partenaire")
     */
    private $projets;


    public function __construct(){
        $this->projets = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Partenaire
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCle()
    {
        return $this->cle;
    }

    /**
     * @param string $cle
     */
    public function setCle($cle)
    {
        $this->cle = $cle;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param Region $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getCleApi()
    {
        return $this->cleApi;
    }

    /**
     * @param string $cleApi
     */
    public function setCleApi($cleApi)
    {
        $this->cleApi = $cleApi;
    }

    /**
     * @return ArrayCollection
     */
    public function getProjets()
    {
        return $this->projets;
    }

    /**
     * @param ArrayCollection $projets
     */
    public function setProjets($projets)
    {
        $this->projets = $projets;
    }
}
