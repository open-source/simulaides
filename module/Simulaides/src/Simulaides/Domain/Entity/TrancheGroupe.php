<?php
namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe TrancheGroupe
 * Cette classe permet de gérer les groupes de tanche de revenu
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Entity
 */

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\TrancheGroupeRepository")
 * @ORM\Table(name="tranche_groupe")
 * Class TrancheGroupe
 * @package Simulaides\Domain\Entities
 */
class TrancheGroupe
{

    /**
     * Code du groupe de tranche
     *
     * @var string
     * @ORM\Id
     * @ORM\Column(name="code_groupe", type="string", length=15, nullable=false)
     */
    private $codeGroupe;

    /**
     * Intitulé du groupe de tranche de revenu
     *
     * @var string
     * @ORM\Column(name="intitule", type="string", length=150, nullable=false)
     */
    private $intitule;

    /**
     * @param string $codeGroupe
     */
    public function setCodeGroupe($codeGroupe)
    {
        $this->codeGroupe = $codeGroupe;
    }

    /**
     * @return string
     */
    public function getCodeGroupe()
    {
        return $this->codeGroupe;
    }

    /**
     * @param mixed $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return mixed
     */
    public function getIntitule()
    {
        return $this->intitule;
    }
}
