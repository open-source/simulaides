<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 09/02/15
 * Time: 18:37
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\RegionRepository")
 * @ORM\Table(name="region")
 * Class Region
 * @package Simulaides\Domain\Entities
 */
class Region
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=2)
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(type="string", length=150)
     */
    private $nom;

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }
}
