<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 25/02/15
 * Time: 10:45
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 */
class SaisieProduitParametreTech extends SaisieProduit
{
    /**
     * @var  string
     * @ORM\Column(name="code_parametre_tech", type="string", length=50)
     */
    private $codeParametreTech;

    /**
     * @var ParametreTech $parametre
     * @ORM\ManyToOne(targetEntity="ParametreTech")
     * @ORM\JoinColumn(name="code_parametre_tech", referencedColumnName="code")
     */
    private $parametre;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     */
    private $valeur;

    /**
     * @param string $codeParametreTech
     */
    public function setCodeParametreTech($codeParametreTech)
    {
        $this->codeParametreTech = $codeParametreTech;
    }

    /**
     * @return string
     */
    public function getCodeParametreTech()
    {
        return $this->codeParametreTech;
    }

    /**
     * @param string $valeur
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    }

    /**
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * @param ParametreTech $parametre
     */
    public function setParametre(ParametreTech $parametre)
    {
        $this->parametre = $parametre;
    }

    /**
     * @return ParametreTech
     */
    public function getParametre()
    {
        return $this->parametre;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return var_export($this, true);
    }
}
