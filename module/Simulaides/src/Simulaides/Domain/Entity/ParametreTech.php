<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 10:27
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\ParametreTechRepository")
 * @ORM\Table(name="parametre_tech")
 * Class ParametreTech
 * @package Simulaides\Domain\Entity
 */
class ParametreTech
{
    /**
     * @var string
     * @ORM\Column(name="code_produit", type="string", length=50)
     */
    private $codeProduit;

    /**
     * @var Produit
     * @ORM\ManyToOne(targetEntity="Produit", inversedBy="paramsTech")
     * @ORM\JoinColumn(name="code_produit", referencedColumnName="code")
     */
    private $produit;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(type="string", length=150)
     */
    private $libelle;

    /**
     * @var string
     * @ORM\Column(name="type_data", type="string", length=20)
     */
    private $typeData;

    /**
     * @var string
     * @ORM\Column(name="code_liste", type="string", length=50, nullable=TRUE)
     */
    private $codeListe;

    /**
     * @var Liste
     * @ORM\ManyToOne(targetEntity="Liste")
     * @ORM\JoinColumn(name="code_liste", referencedColumnName="code")
     */
    private $liste;

    /**
     * Indique si le paramètre est utilisable dans une règle
     *
     * @var bool
     * @ORM\Column(name="utilisable_regle", type="boolean")
     */
    private $utilisableRegle;

    /**
     * Indique que le paramètre doit être saisi pour un logement de type "Maison"
     *
     * @var  bool
     * @ORM\Column(type="boolean")
     */
    private $maison;

    /**
     * Indique que le paramètre doit être saisi pour un logement de type "Appartement"
     *
     * @var  bool
     * @ORM\Column(type="boolean")
     */
    private $appartement;

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $codeListe
     */
    public function setCodeListe($codeListe)
    {
        $this->codeListe = $codeListe;
    }

    /**
     * @return string
     */
    public function getCodeListe()
    {
        return $this->codeListe;
    }

    /**
     * @param string $codeProduit
     */
    public function setCodeProduit($codeProduit)
    {
        $this->codeProduit = $codeProduit;
    }

    /**
     * @return string
     */
    public function getCodeProduit()
    {
        return $this->codeProduit;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param Liste $liste
     */
    public function setListe(Liste $liste)
    {
        $this->liste = $liste;
    }

    /**
     * @return Liste
     */
    public function getListe()
    {
        return $this->liste;
    }

    /**
     * @param Produit $produit
     */
    public function setProduit(Produit $produit)
    {
        $this->produit = $produit;
    }

    /**
     * @return Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param string $typeData
     */
    public function setTypeData($typeData)
    {
        $this->typeData = $typeData;
    }

    /**
     * @return string
     */
    public function getTypeData()
    {
        return $this->typeData;
    }

    /**
     * @param boolean $utilisableRegle
     */
    public function setUtilisableRegle($utilisableRegle)
    {
        $this->utilisableRegle = $utilisableRegle;
    }

    /**
     * @return boolean
     */
    public function getUtilisableRegle()
    {
        return $this->utilisableRegle;
    }

    /**
     * @param boolean $appartement
     */
    public function setAppartement($appartement)
    {
        $this->appartement = $appartement;
    }

    /**
     * @return boolean
     */
    public function getAppartement()
    {
        return $this->appartement;
    }

    /**
     * @param boolean $maison
     */
    public function setMaison($maison)
    {
        $this->maison = $maison;
    }

    /**
     * @return boolean
     */
    public function getMaison()
    {
        return $this->maison;
    }
}
