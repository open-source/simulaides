<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 10/02/15
 * Time: 09:20
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\ValeurListeRepository")
 * @ORM\Table(name="valeur_liste")
 * Class ValeurListe
 * @package Simulaides\Domain\Entities
 */
class ValeurListe
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(name="code_liste", type="string", length=50)
     */
    private $codeListe;

    /**
     * @var Liste
     * @ORM\ManyToOne(targetEntity="Liste")
     * @ORM\JoinColumn(name="code_liste", referencedColumnName="code")
     */
    private $liste;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptif;

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $codeListe
     */
    public function setCodeListe($codeListe)
    {
        $this->codeListe = $codeListe;
    }

    /**
     * @return mixed
     */
    public function getCodeListe()
    {
        return $this->codeListe;
    }

    /**
     * @param mixed $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @return mixed
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param Liste $liste
     */
    public function setListe(Liste $liste)
    {
        $this->liste = $liste;
    }

    /**
     * @return Liste
     */
    public function getListe()
    {
        return $this->liste;
    }
}
