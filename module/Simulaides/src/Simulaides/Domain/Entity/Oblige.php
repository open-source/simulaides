<?php


namespace Simulaides\Domain\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\ObligeRepository")
 * @ORM\Table(name="oblige")
 * Class Oblige
 * @package Simulaides\Domain\Entities
 */
class Oblige
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", length=11)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string", length=150, nullable=false)
     */
    private $nom;

    /**
     * @var string
     * @ORM\Column(name="siren", type="string", length=14, nullable=false, unique=true)
     */
    private $siren;

    /**
     * @var string
     * @ORM\Column(name="mail", type="string", length=255, nullable=false)
     */
    private $mail;

    /**
     * @var string
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * Liste des dispositifs de l'obligé
     *
     * @var Collection
     * @ORM\OneToMany(targetEntity="Dispositif", mappedBy="dispositif", cascade={"remove", "persist"})
     */
    private $listDispositifs;

    /**
     * Oblige constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * @param string $siren
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;
    }


    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return Collection
     */
    public function getListDispositifs()
    {
        return $this->listDispositifs;
    }

    /**
     * @param Collection $listDispositifs
     */
    public function setListDispositifs($listDispositifs)
    {
        $this->listDispositifs = $listDispositifs;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

}
