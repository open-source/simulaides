<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 09/02/15
 * Time: 17:18
 */

namespace Simulaides\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\TravauxRepository")
 * @ORM\Table(name="travaux")
 * Class Travaux
 * @package Simulaides\Domain\Entities
 */
class Travaux
{
    /** Nom du champ maison */
    const CHAMP_MAISON = 'maison';

    /** Nom du champ appartement */
    const CHAMP_APPARTEMENT = 'appartement';

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $categorie;

    /**
     * @var ValeurListe
     * @ORM\ManyToOne(targetEntity="ValeurListe")
     * @ORM\JoinColumn(name="categorie", referencedColumnName="code")
     */
    private $valeurListe;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=3)
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     */
    private $intitule;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $maison;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $appartement;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $outre_mer;

    /**
     * Numéro d'ordre
     * @var int
     * @ORM\Column(name="num_ordre", type="integer", nullable=false)
     */
    private $numOrdre;



    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Dispositif", mappedBy="travaux")
     */
    private $dispositifs;

    /**
     * @var  ArrayCollection $produits
     * @ORM\OneToMany(targetEntity="Produit", mappedBy="travaux", cascade={"persist", "remove"})
     */
    private $produits;

    /**
     *
     */
    public function __construct()
    {
        $this->dispositifs = new ArrayCollection();
    }

    /**
     * @param mixed $dispositifs
     */
    public function setDispositifs($dispositifs)
    {
        $this->dispositifs = $dispositifs;
    }

    /**
     * @return mixed
     */
    public function getDispositifs()
    {
        return $this->dispositifs;
    }

    /**
     * @param boolean $appartement
     */
    public function setAppartement($appartement)
    {
        $this->appartement = $appartement;
    }

    /**
     * @return boolean
     */
    public function getAppartement()
    {
        return $this->appartement;
    }

    /**
     * @param string $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param boolean $maison
     */
    public function setMaison($maison)
    {
        $this->maison = $maison;
    }

    /**
     * @return boolean
     */
    public function getMaison()
    {
        return $this->maison;
    }

    /**
     * @return bool
     */
    public function getOutreMer()
    {
        return $this->outre_mer;
    }

    /**
     * @param bool $outre_mer
     */
    public function setOutreMer($outre_mer)
    {
        $this->outre_mer = $outre_mer;
    }

    /**
     * @param ValeurListe $valeurListe
     */
    public function setValeurListe(ValeurListe $valeurListe)
    {
        $this->valeurListe = $valeurListe;
    }

    /**
     * @return ValeurListe
     */
    public function getValeurListe()
    {
        return $this->valeurListe;
    }

    /**
     * @param Dispositif $dispositif
     */
    public function addDispositif(Dispositif $dispositif)
    {
        $this->dispositifs[] = $dispositif;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $produits
     */
    public function setProduits($produits)
    {
        $this->produits = $produits;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * @param Produit $produit
     */
    public function addProduits(Produit $produit)
    {
        $this->produits[] = $produit;
    }

    /**
     * @param Produit $produit
     */
    public function removeProduits(Produit $produit)
    {
        $this->produits->removeElement($produit);
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return var_export($this, true);
    }


    /**
     * @return int
     */
    public function getNumOrdre()
    {
        return $this->numOrdre;
    }

    /**
     * @param int $numOrdre
     */
    public function setNumOrdre($numOrdre)
    {
        $this->numOrdre = $numOrdre;
    }

}
