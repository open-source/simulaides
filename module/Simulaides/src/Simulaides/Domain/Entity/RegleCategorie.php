<?php
namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe RegleCategorie
 * classe permettant de gérer les catégorie de classement des variables
 * pour les règles des dispositifs et des travaux
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Entity
 */

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\RegleCategorieRepository")
 * @ORM\Table(name="regle_categorie")
 * Class RegleCategorie
 * @package Simulaides\Domain\Entities
 */
class RegleCategorie
{
    /**
     * Code de la catégorie
     * @var string
     * @ORM\Id
     * @ORM\Column(name="code", type="string", length=20, nullable=false)
     */
    private $code;

    /**
     * Libellé de la catégorie
     * @var string
     * @ORM\Column(name="libelle", type="string", length=80, nullable=false)
     */
    private $libelle;

    /**
     * Type de la catégorie (dispositif ou travaux)
     * @var string
     * @ORM\Column(name="type", type="string", length=1, nullable=false)
     */
    private $type;

    /**
     * Numéro d'ordre de la catégorie dans le type
     * @var int
     * @ORM\Column(name="numero_ordre", type="integer", nullable=false)
     */
    private $numeroOrdre;

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $numeroOrdre
     */
    public function setNumeroOrdre($numeroOrdre)
    {
        $this->numeroOrdre = $numeroOrdre;
    }

    /**
     * @return int
     */
    public function getNumeroOrdre()
    {
        return $this->numeroOrdre;
    }
}
