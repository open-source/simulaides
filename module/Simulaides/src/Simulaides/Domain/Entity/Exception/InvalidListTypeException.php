<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 13/02/15
 * Time: 11:34
 */

namespace Simulaides\Domain\Entity\Exception;


/**
 * Class InvalidListTypeException
 * @package Simulaides\Domain\Entity\Exception
 */
class InvalidListTypeException extends \Exception
{

}
