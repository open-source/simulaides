<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 10/02/15
 * Time: 09:09
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\ListeRepository")
 * @ORM\Table(name="liste")
 * Class Liste
 * @package Simulaides\Domain\Entities
 */
class Liste
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     */
    private $libelle;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $descriptif;


    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @return mixed
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}
