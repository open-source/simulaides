<?php
namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classe RegleVariable
 * Permet de gérer les variables utilisées dans les règles
 * des dispositifs et des travaux
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Entity
 */

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\RegleVariableRepository")
 * @ORM\Table(name="regle_variable")
 * Class RegleVariable
 * @package Simulaides\Domain\Entities
 */
class RegleVariable
{
    /**
     * Identifiant unique
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private $id;

    /**
     * Catégorie associée
     * @var RegleCategorie
     * @ORM\ManyToOne(targetEntity="RegleCategorie")
     * @ORM\JoinColumn(name="code_categorie", referencedColumnName="code")
     */
    private $categorie;

    /**
     * Code de la variable utilisé dans les formules
     * @var string
     * @ORM\Column(name="code_var", type="string", length=50, nullable=false)
     */
    private $codeVar;

    /**
     * Libellé de la variable
     * @var string
     * @ORM\Column(name="libelle", type="string", length=150, nullable=false)
     */
    private $libelle;

    /**
     * Indique que cette variable est associée à une liste de param
     * et ne peut être choisie seule
     *
     * @var boolean
     * @ORM\Column(name="avec_liste_param", type="boolean", nullable=false)
     */
    private $avecListeParam;

    /**
     * Indique si cette variable est associée à une liste de valeurs
     * @var boolean
     * @ORM\Column(name="avec_liste_valeur", type="boolean", nullable=false)
     */
    private $avecListeValeur;

    /**
     * @param boolean $avecListeValeur
     */
    public function setAvecListeValeur($avecListeValeur)
    {
        $this->avecListeValeur = $avecListeValeur;
    }

    /**
     * @return boolean
     */
    public function getAvecListeValeur()
    {
        return $this->avecListeValeur;
    }

    /**
     * @param \Simulaides\Domain\Entity\RegleCategorie $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return \Simulaides\Domain\Entity\RegleCategorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param string $codeVar
     */
    public function setCodeVar($codeVar)
    {
        $this->codeVar = $codeVar;
    }

    /**
     * @return string
     */
    public function getCodeVar()
    {
        return $this->codeVar;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $avecListeParam
     */
    public function setAvecListeParam($avecListeParam)
    {
        $this->avecListeParam = $avecListeParam;
    }

    /**
     * @return mixed
     */
    public function getAvecListeParam()
    {
        return $this->avecListeParam;
    }
}
