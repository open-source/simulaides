<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 09/02/15
 * Time: 18:32
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\DepartementRepository")
 * @ORM\Table(name="departement")
 * Class Departement
 * @package Simulaides\Domain\Entity
 */
class Departement
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=3)
     */
    private $code;
    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $nom;
    /**
     * @var string
     * @ORM\Column(type="string", length=2, name="code_region")
     */
    private $codeRegion;
    /**
     * @var Region
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(name="code_region", referencedColumnName="code")
     */
    private $region;
    /**
     * @var string;
     * @ORM\Column(name="zone_climatique", type="string", length=2)
     */
    private $zoneClimatique;

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $codeRegion
     */
    public function setCodeRegion($codeRegion)
    {
        $this->codeRegion = $codeRegion;
    }

    /**
     * @return mixed
     */
    public function getCodeRegion()
    {
        return $this->codeRegion;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param Region $region
     */
    public function setRegion(Region $region)
    {
        $this->region = $region;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $zoneClimatique
     */
    public function setZoneClimatique($zoneClimatique)
    {
        $this->zoneClimatique = $zoneClimatique;
    }

    /**
     * @return string
     */
    public function getZoneClimatique()
    {
        return $this->zoneClimatique;
    }
}
