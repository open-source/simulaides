<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 10/02/15
 * Time: 14:45
 */

namespace Simulaides\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\PerimetreGeoRepository")
 * @ORM\Table(name="perimetre_geo")
 * Class PerimetreGeo
 * @package Simulaides\Domain\Entity
 */
class PerimetreGeo
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="id_dispositif", type="integer", length=11)
     */
    private $idDispositif;

    /**
     * @var Dispositif
     * @ORM\ManyToOne(targetEntity="Dispositif")
     * @ORM\JoinColumn(name="id_dispositif", referencedColumnName="id")
     */
    private $dispositif;

    /**
     * @var string
     * @ORM\Column(name="code_region", type="string", length=2, nullable=true)
     */
    private $codeRegion;

    /**
     * @var Region
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(name="code_region", referencedColumnName="code")
     */
    private $region;

    /**
     * @var string
     * @ORM\Column(name="code_departement", type="string", length=3, nullable=true)
     */
    private $codeDepartement;

    /**
     * @var Departement
     * @ORM\ManyToOne(targetEntity="Departement")
     * @ORM\JoinColumn(name="code_departement", referencedColumnName="code")
     */
    private $departement;

    /**
     * @var string
     * @ORM\Column(name="code_commune", type="string", length=6, nullable=true)
     */
    private $codeCommune;

    /**
     * @var Commune
     * @ORM\ManyToOne(targetEntity="Commune")
     * @ORM\JoinColumn(name="code_commune", referencedColumnName="code_insee")
     */
    private $commune;

    /**
     * @var string
     * @ORM\Column(name="code_epci", type="string", length=9, nullable=true)
     */
    private $codeEpci;

    /**
     * @var Epci
     * @ORM\ManyToOne(targetEntity="Epci")
     * @ORM\JoinCOlumn(name="code_epci", referencedColumnName="code")
     */
    private $epci;

    /**
     * @param string $codeCommune
     */
    public function setCodeCommune($codeCommune)
    {
        $this->codeCommune = $codeCommune;
    }

    /**
     * @return string
     */
    public function getCodeCommune()
    {
        return $this->codeCommune;
    }

    /**
     * @param string $codeDepartement
     */
    public function setCodeDepartement($codeDepartement)
    {
        $this->codeDepartement = $codeDepartement;
    }

    /**
     * @return string
     */
    public function getCodeDepartement()
    {
        return $this->codeDepartement;
    }

    /**
     * @param $idDispositif
     */
    public function setIdDispositif($idDispositif)
    {
        $this->idDispositif = $idDispositif;
    }

    /**
     * @return int
     */
    public function getIdDispositif()
    {
        return $this->idDispositif;
    }

    /**
     * @param string $codeRegion
     */
    public function setCodeRegion($codeRegion)
    {
        $this->codeRegion = $codeRegion;
    }

    /**
     * @return string
     */
    public function getCodeRegion()
    {
        return $this->codeRegion;
    }

    /**
     * @param Commune $commune
     */
    public function setCommune(Commune $commune)
    {
        $this->commune = $commune;
    }

    /**
     * @return Commune
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * @param Departement $departement
     */
    public function setDepartement(Departement $departement)
    {
        $this->departement = $departement;
    }

    /**
     * @return Departement
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * @param Dispositif $dispositif
     */
    public function setDispositif(Dispositif $dispositif)
    {
        $this->dispositif = $dispositif;
    }

    /**
     * @return Dispositif
     */
    public function getDispositif()
    {
        return $this->dispositif;
    }

    /**
     * @param Epci $epci
     */
    public function setEpci(Epci $epci)
    {
        $this->epci = $epci;
    }

    /**
     * @return Epci
     */
    public function getEpci()
    {
        return $this->epci;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $codeEpci
     */
    public function setCodeEpci($codeEpci)
    {
        $this->codeEpci = $codeEpci;
    }

    /**
     * @return string
     */
    public function getCodeEpci()
    {
        return $this->codeEpci;
    }

    /**
     * @param Region $region
     */
    public function setRegion(Region $region)
    {
        $this->region = $region;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }
}
