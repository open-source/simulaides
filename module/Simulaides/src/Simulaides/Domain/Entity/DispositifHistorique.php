<?php
namespace Simulaides\Domain\Entity;

/**
 * Classe DispositifHistorique
 * Permet de gérer les actions historisées sous forme de note
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\Entity
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Simulaides\Domain\Repository\DispositifHistoriqueRepository")
 * @ORM\Table(name="dispositif_historique")
 * Class DispositifHistorique
 * @package Simulaides\Domain\Entities
 */
class DispositifHistorique
{
    /**
     * Identifiant de la note
     *
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private $id;

    /**
     * Identifiant du dispositif
     * @var int
     * @ORM\Column(type="integer", name="id_dispositif", length=11)
     */
    private $idDispositif;

    /**
     * Dispositif
     * @var Dispositif
     * @ORM\ManyToOne(targetEntity="Dispositif")
     * @ORM\JoinColumn(name="id_dispositif", referencedColumnName="id")
     */
    private $dispositif;

    /**
     * Date de la note
     * @var \DateTime
     * @ORM\Column(name="date_note", type="date")
     */
    private $dateNote;

    /**
     * Note
     * @var string
     * @ORM\Column(name="note", type="text")
     */
    private $note;

    /**
     * @param \DateTime $dateNote
     */
    public function setDateNote($dateNote)
    {
        $this->dateNote = $dateNote;
    }

    /**
     * @return \DateTime
     */
    public function getDateNote()
    {
        return $this->dateNote;
    }

    /**
     * @param \Simulaides\Domain\Entity\Dispositif $dispositif
     */
    public function setDispositif($dispositif)
    {
        $this->dispositif = $dispositif;
    }

    /**
     * @return \Simulaides\Domain\Entity\Dispositif
     */
    public function getDispositif()
    {
        return $this->dispositif;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $idDispositif
     */
    public function setIdDispositif($idDispositif)
    {
        $this->idDispositif = $idDispositif;
    }

    /**
     * @return int
     */
    public function getIdDispositif()
    {
        return $this->idDispositif;
    }

    /**
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }
}
