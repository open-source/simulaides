<?php
namespace Simulaides\Domain\Filter;

use Simulaides\Constante\MainConstante;
use Zend\Filter\AbstractFilter;

/**
 * Classe Date
 * Retourne la valeur absolue d'un entier
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Element\Filter
 */
class Date extends AbstractFilter
{

    /**
     * Format de transcription de la date
     * @var string
     */
    protected $format = MainConstante::FORMAT_DATE_DEFAUT;

    /**
     * Met en forme une chaine de type interval en critère DQL
     *
     * @see \Zend\Filter\FilterInterface::filter()
     */
    public function filter($value)
    {
        if (!$value instanceof \Datetime) {
            $value2 = \DateTime::createFromFormat($this->format, $value);

            $errors = \DateTime::getLastErrors();
            if (!empty($errors['warning_count'])) {
                // Strictly speaking, that date is invalid
                return $value;
            }

            if ($value2) {
                $value = $value2;
            }
        }

        return $value;
    }

    /**
     * Modifie le format de la date
     *
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }
}
