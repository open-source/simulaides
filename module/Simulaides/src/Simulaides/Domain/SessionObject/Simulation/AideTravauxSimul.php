<?php
namespace Simulaides\Domain\SessionObject\Simulation;

use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Logger\LoggerSimulation;

/**
 * Classe AideTravauxSimul
 * Classe de stockage des règles d'aides pour un travaux dans le cadre d'un dispositif
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class AideTravauxSimul
{
    /** @var string code */
    private $code;

    /** @var TravauxSimul travaux */
    private $travaux;

    /** @var float montantAideTo */
    private $montantAideTo;

    /** @var float montantAideMo */
    private $montantAideMo;

    /** @var float montantAideFo */
    private $montantAideFo;

    /** @var string eligibiliteSpecifique */
    private $eligibiliteSpecifique;

    /** @var array reglesMontant */
    private $reglesMontant = array();

    /** @var array reglesPlafond */
    private $reglesPlafond = array();

    /**
     * Constructeur
     *
     * @param string $code
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Calcule le montant d'aide d'une Aide travaux
     *
     * @param int $idDispositif
     */
    public function calculerMontantAideTravaux($idDispositif)
    {
        /** @var TravauxSimul $travaux */
        $travaux = $this->getTravaux();
        LoggerSimulation::detail(
            "\n\n\t\t --------- [ " . $travaux->getCode() . " : " . $travaux->getIntitule() . " ] ---------"
        );
        LoggerSimulation::detail("\n\n\t\t - Regles de Montant de l'Aide travaux");

        // Si l'Aide travaux possede des règles de montant
        if (count($this->reglesMontant) > 0) {

            // Parcours les règles montant de l'Aide travaux
            /** @var RegleSimul $regle */
            foreach ($this->reglesMontant as $regle) {

                $montant = $regle->calculerRegle($idDispositif, $this->code);
                if ($montant !== null) { // Si l'évaluation de la règle a fourni un montant

                    if ($montant < 0) { // Si le montant est négatif, on le ramène à zéro
                        $montant = 0.0;
                    }
                    switch ($regle->getType()) {
                        case ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_TOTAL:
                            $this->montantAideTo = $montant;
                            break;
                        case ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_MO:
                            $this->montantAideMo = $montant;
                            break;
                        case ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_FOURNITURE:
                            $this->montantAideFo = $montant;
                            break;
                    }
                }
            }
            // Si l’on a obtenu un montant total (par une règle), il n’y a pas de raison
            //que l’on en ait une sur le montant fournitures ou sur le montant main d’œuvre,
            // Il faut donc répartir la valeur de montant total sur les montants main d’œuvre
            //et fournitures au prorata des coûts de la main d’œuvre et
            // des fournitures du travaux
            if ($this->montantAideTo > 0) {

                $this->montantAideMo = $this->montantAideTo * ($travaux->getCoutHtMo() / $travaux->getCoutHtTo());
                $this->montantAideFo = $this->montantAideTo - $this->montantAideMo;

            } else {
                $this->montantAideTo = $this->montantAideMo + $this->montantAideFo;
            }
            LoggerSimulation::detail(
                "\n\t\t ==> [" . $travaux->getCode(
                ) . ": Montant de l'Aide avant plafonnement = " . $this->montantAideTo . "]"
            );

            if ($this->montantAideTo > 0) {
                $this->plafonnerMontantAideTravaux($idDispositif);
                LoggerSimulation::detail(
                    "\n\n\t\t ==> [" . $travaux->getCode(
                    ) . ":Montant de l'Aide travaux apres plafonnement = " . $this->montantAideTo . "]"
                );
            }
        }
    }

    /**
     * Calcule le Plafond d'aide d'une Aide travaux
     *
     * @param int $idDispositif
     */
    public function plafonnerMontantAideTravaux($idDispositif)
    {
        // initialisation du plafond avant l'évaluation des règles de plafond
        $travaux = $this->getTravaux();
        $plafond = $travaux->getCoutTtcTo();
        LoggerSimulation::detail(
            "\n\n\t\t ==> [" . $travaux->getCode(
            ) . ": Initialisation du Plafond de l'Aide avec le cout TTC du type de travaux = " . $plafond
        );

        // Si l'Aide travaux possede des règles de plafond
        if (count($this->reglesPlafond) > 0) {

            LoggerSimulation::detail("\n\t\t - Regles de Plafond de l'Aide travaux ");

            /** @var RegleSimul $regle */
            foreach ($this->reglesPlafond as $regle) {

                $plafondRegle = $regle->calculerRegle($idDispositif, $this->code);
                if ($plafondRegle !== null) { // Si la règle fournit une valeur de plafond
                    if ($plafondRegle < 0) { // Si le plafond est négatif on le ramène à zéro
                        $plafondRegle = 0;
                    }
                    if ($plafond > $plafondRegle) {
                        // si le plafond fourni par la règle est > au plafond en cours => on abaisse le plafond
                        $plafond = $plafondRegle;
                    }
                }
            }
            LoggerSimulation::detail("\n\t\t ==> [" . $travaux->getCode() . ": Plafond de l'Aide = " . $plafond);

            // Si l'on a obtenu un plafond pour l'Aide travaux
            if ($plafond !== null) {

                if ($this->montantAideTo > $plafond) {
                    // Si le montant de l'Aide travaux est supérieur au plafond
                    // alors on plafonne les montants c'est à dire (montant total = plafond)
                    // on applique le ratio de baisse du montant total au montant main d'oeuvre
                    $this->montantAideMo = $this->montantAideMo * ($plafond / $this->montantAideTo);
                    $this->montantAideTo = $plafond;
                    $this->montantAideFo = $this->montantAideTo - $this->montantAideMo;
                }
            }
        }
    }

    /**
     * Affectation du part du montant de son dispositif forfaitaire à une Aide travaux
     *
     * @param float        $montantDispositif
     * @param TravauxSimul $coutsTravauxDispositif
     */
    public function distribuerMontantDispositif($montantDispositif, $coutsTravauxDispositif)
    {
        $travaux = $this->getTravaux();
        $prorata = $travaux->getCoutHtTo() / $coutsTravauxDispositif->getCoutHtTo();
        $ratioMo = $travaux->getCoutHtMo() / $travaux->getCoutHtTo();

        $this->montantAideTo = $montantDispositif * $prorata;
        $this->montantAideMo = $this->montantAideTo * $ratioMo;
        $this->montantAideFo = $this->montantAideTo - $this->montantAideMo;
    }

    /**
     * Réajuste les Montant d'une Aide Travaux lorsque son dispositif a été plafonné
     *
     * @param float $coeffReajustement
     * @param boolean $isadmin
     */
    public function impacterPlafondDispositif($coeffReajustement,$isadmin=false)
    {
        if(!$isadmin){
            $this->montantAideTo = floor($this->montantAideTo * $coeffReajustement);
            $this->montantAideMo = floor($this->montantAideMo * $coeffReajustement);
            $this->montantAideFo = floor($this->montantAideFo * $coeffReajustement);
        }else{
            $this->montantAideTo = round ($this->montantAideTo * $coeffReajustement,2);
            $this->montantAideMo = round ($this->montantAideMo * $coeffReajustement,2);
            $this->montantAideFo = round ($this->montantAideFo * $coeffReajustement,2);
        }

    }

    /**
     * Ajoute une règle au à l'Aide travaux selon son type
     *
     * @param RegleSimul $regle
     */
    public function addRegle($regle)
    {
        switch ($regle->getType()) {
            case ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_TOTAL:
            case ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_MO:
            case ValeurListeConstante::TYPE_REGLE_TRAVAUX_MT_FOURNITURE:
                $this->reglesMontant[] = $regle;
                break;
            case ValeurListeConstante::TYPE_REGLE_TRAVAUX_PLAFOND:
                $this->reglesPlafond[] = $regle;
                break;
        }
    }

    //-------------  Accesseur -----------------
    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param TravauxSimul $travaux
     */
    public function setTravaux($travaux)
    {
        $this->travaux = $travaux;
    }

    /**
     * @return TravauxSimul
     */
    public function getTravaux()
    {
        return $this->travaux;
    }

    /**
     * @param float $montantAideTo
     */
    public function setMontantAideTo($montantAideTo)
    {
        $this->montantAideTo = $montantAideTo;
    }

    /**
     * @return float
     */
    public function getMontantAideTo()
    {
        return $this->montantAideTo;
    }

    /**
     * @param float $montantAideMo
     */
    public function setMontantAideMo($montantAideMo)
    {
        $this->montantAideMo = $montantAideMo;
    }

    /**
     * @return float
     */
    public function getMontantAideMo()
    {
        return $this->montantAideMo;
    }

    /**
     * @param float $montantAideFo
     */
    public function setMontantAideFo($montantAideFo)
    {
        $this->montantAideFo = $montantAideFo;
    }

    /**
     * @return float
     */
    public function getMontantAideFo()
    {
        return $this->montantAideFo;
    }

    /**
     * @param string $eligibiliteSpecifique
     */
    public function setEligibiliteSpecifique($eligibiliteSpecifique)
    {
        $this->eligibiliteSpecifique = $eligibiliteSpecifique;
    }

    /**
     * @return string
     */
    public function getEligibiliteSpecifique()
    {
        return $this->eligibiliteSpecifique;
    }

    /**
     * @param array $reglesMontant
     */
    public function setReglesMontant($reglesMontant)
    {
        $this->reglesMontant = $reglesMontant;
    }

    /**
     * @return array
     */
    public function getReglesMontant()
    {
        return $this->reglesMontant;
    }

    /**
     * @param array $reglesPlafond
     */
    public function setReglesPlafond($reglesPlafond)
    {
        $this->reglesPlafond = $reglesPlafond;
    }

    /**
     * @return array
     */
    public function getReglesPlafond()
    {
        return $this->reglesPlafond;
    }
}
