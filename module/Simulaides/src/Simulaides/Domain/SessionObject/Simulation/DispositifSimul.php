<?php
namespace Simulaides\Domain\SessionObject\Simulation;

use DateTime;
use Simulaides\Constante\RegleVariableConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Logger\LoggerSimulation;
use Simulaides\Service\CalculateurCeeService;
use Simulaides\Service\SessionContainerService;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe DispositifSimul
 * Objet représentant un dispositif dans la simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class DispositifSimul
{
    /** @var int id */
    private $id;

    /** @var  int numOrdre */
    private $numOrdre;

    /** @var  string etat */
    private $etat;

    /** @var  string codeType */
    private $type;

    /** @var string intitule */
    private $intitule;

    /** @var boolean forfaitaire */
    private $forfaitaire;

    /** @var string descriptif */
    private $descriptif;

    /** @var string financeur */
    private $financeur;

    /** @var Oblige oblige */
    private $oblige;

    /** @var DateTime */
    private $debutValidite;

    /** @var string */
    private $siteWeb;

    /** @var boolean exclutCEE */
    private $exclutCEE;

    /** @var boolean */
    private $exclutDepartement;

    /** @var boolean */
    private $exclutAnah;

    /** @var boolean */
    private $exclutEPCI;

    /** @var boolean */
    private $exclutRegion;

    /** @var boolean */
    private $exclutCommunal;

    /** @var boolean */
    private $exclutNational;

    /** @var boolean */
    private $exclutCoupDePouce;

    /** @var boolean evaluerRegle */
    private $evaluerRegle;

    /** @var array reglesEligibilite */
    private $reglesEligibilite = array();

    /** @var array reglesMontant */
    private $reglesMontant = array();

    /** @var array reglesPlafond */
    private $reglesPlafond = array();

    /** @var array aidesTravaux */
    private $aidesTravaux = array();

    /** @var TravauxSimul coutsTravaux */
    private $coutsTravaux;

    /** @var boolean eligibilite */
    private $eligibilite;

    /** @var float montant */
    private $montant;

    /** @var int $dispositifRequis */
    private $idDispositifRequis;

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /**
     * Constructeur du dispositif à partir d'une ligne résultat
     * de la requête de récupération des dispositifs (SimulationService)
     *
     * @param int                     $id
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct($id, ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->id             = (int)$id;
        $this->montant        = 0.0;

        // On initialise un dispositif comme étant éligible.
        $this->eligibilite = true;

        // On initialise un dispositif comme étant forfaitaire.
        // Au chargement du dispositif dès qu'une règle sur un de ses travaux sera rencontrée,
        //il sera déclaré non forfaitaire.
        $this->forfaitaire = true;
    }

    /**
     * Calcule l'éligibilté du dispositif
     */
    public function calculerEligibilite($modeExecution,$tabDispoEligible)
    {
        LoggerSimulation::detail("\n - ELIGIBILITE du Dispositif : debut {");


        // On evalue les regles d'éligibilité Si l’indicateur d’évaluation des règles du dispositif
        //est positionné à faux OU si l'on est en MODE TEST
        // Dans le cas (indicateur à faux ET MODE PRODUCTION) le dispositif est éligible de fait
//        if (($this->evaluerRegle === true) || ($modeExecution == SimulationConstante::MODE_TEST)) {
        if (($this->etat == ValeurListeConstante::ETAT_DISPOSITIF_ACTIVE) || ($modeExecution == SimulationConstante::MODE_TEST)) {
            /** @var RegleSimul $regle */
            foreach ($this->reglesEligibilite as $regle) {
                $this->eligibilite = $this->eligibilite && $regle->calculerRegle($this->id, null);
            }
        }

        $type = $this->getType();
        if($type != SimulationConstante::TYPE_AIDE_PUBLIC){
            $nbTravauxNonExclus = 0;

            /** @var AideTravauxSimul $aideTravaux */
            foreach ($this->aidesTravaux as $aideTravaux) {

                $travaux = $aideTravaux->getTravaux();
                if ($travaux->getEstExclu($type) === false) { // le travaux n'est pas exclu des aides CEE
                    $nbTravauxNonExclus = $nbTravauxNonExclus + 1;
                }
            }
            if ($nbTravauxNonExclus == 0) {
                $this->eligibilite = false;
                LoggerSimulation::detail("\n\n\t\t - Tous les travaux sont exclus des aides ".$type);
            }
        }

        if($this->getIdDispositifRequis() && !in_array($this->getIdDispositifRequis(),$tabDispoEligible) ){
            $this->eligibilite = false;
        }


        LoggerSimulation::detail("\n\n\t ==> [eligibilite dispositif = " . $this->eligibilite . "]");
        LoggerSimulation::detail("\n } - Fin ELIGIBILITE ");
    }

    /**
     * Calcule le montant du dispositif CEE
     *
     * @param ProjetSimul $projet
     * @param array $travauxCDP
     */
    public function calculerMontantDispositifCEE($projet)
    {
        /** @var CalculateurCeeService $calculateurCEE */
        $calculateurCEE = $this->serviceLocator->get(CalculateurCeeService::SERVICE_NAME);

        /** @var AideTravauxSimul $aideTravaux */
        foreach ($this->aidesTravaux as $aideTravaux) {

            $travaux = $aideTravaux->getTravaux();

            if ($travaux->getEstExclu(SimulationConstante::TYPE_AIDE_CEE) === false)
                { // le travaux n'est pas
                // exclu des aides CEE
                $travaux->setEstInclu(SimulationConstante::TYPE_AIDE_CEE);

                LoggerSimulation::detail(
                    "\n\t * Travaux (" . $travaux->getCode() . " : " . $travaux->getIntitule() . ")"
                );

                $calculateurCEE->calculMontantCEETravaux($projet, $aideTravaux);
                $this->montant = $this->montant + $aideTravaux->getMontantAideTo();
            }else{
                LoggerSimulation::detail(
                    "\n\t * Travaux (" . $travaux->getCode() . " : " . $travaux->getIntitule() . ") est exclu de l'aide CEE"
                );

            }
        }
        LoggerSimulation::detail("\n\t ==>[Montant total des aides CEE du dispositif = " . $this->montant . "]");
        LoggerSimulation::detail("\n} - Fin MONTANT");
    }

    /**
     * Calcule le montant d'un dispositif (PUBLIC)
     */
    public function calculerMontantDispositif()
    {
        // Si le dispositif est forfaitaire
        if ($this->forfaitaire === true) {
            LoggerSimulation::detail("\n\n - MONTANT du Dispositif (forfaitaire): debut { ");
            $this->calculerMontantDispositifForfaitaire();

        } else {
            LoggerSimulation::detail("\n\n - MONTANT du Dispositif (Non forfaitaire): debut {");
            $this->calculerMontantDispositifNonForfaitaire();
        }

        LoggerSimulation::detail("\n\t ==>[Montant total des aides du dispositif = " . $this->montant . "]");
        LoggerSimulation::detail("\n} - Fin MONTANT");
    }

    /**
     * Calcule le montant du dispositif forfaitaire
     */
    public function calculerMontantDispositifForfaitaire()
    {
        // Si le dispositif possède des Regles de montant (Niveau dispositif)
        if (count($this->reglesMontant) > 0) {

            LoggerSimulation::detail("\n\n\t ** Calcul des regles de Montant propres au Dispositif :");
            //On est dans le cas d’un Dispositif qui a des règles de calcul par travaux
            //mais qui a aussi une règle de calcul de son montant au niveau dispositif.
            // Ceci peut se produire dans le cas où le dispositif rajoute une prime au montant obtenus par les travaux
            //(par exemple le dispositif de « Grand couronne »).
            // Sachant que cette évaluation de la règle de montant du dispositif va venir
            //écraser le montant précédemment obtenu,
            // le concepteur de la règle doit l’écrire sous la forme Md = Md + prime,
            //c’est-à-dire que le montant du dispositif est utilisé comme variable

            // Parcours des règles montant du dispositif
            /** @var RegleSimul $regle */
            foreach ($this->reglesMontant as $regle) {

                $montant = $regle->calculerRegle($this->id, null);
                if ($montant !== null) { // Si l'évaluation de la règle a fourni un montant
                    if ($montant < 0) { // Si le montant est négatif, on le ramène à zéro
                        $montant = 0.0;
                    }
                    $this->montant = $montant; // Affectation du montant au dispositif
                }
            }
        }
        LoggerSimulation::detail(
            "\n\n\t ==> [Montant du dispositif obtenu apres evaluation de ses propres regles : " . $this->montant . ""
        );

        // si l'on a obtenu un montant d'aide pour le dispositif
        if ($this->montant > 0) {

            $plafond = $this->calculerPlafondDispositif(
            ); // on calcule si il existe une valeur de plafond pour le dispositif
            if ($plafond !== null) {
                if ($this->montant > $plafond) { // Si le montant du dispositif dépasse le plafond
                    $this->montant = $plafond;
                }
            }
            // Il faut distribuer le montant du dispositif sur ses aides travaux.
            // Pour l'instant elles ont un montant = 0 car elles n'ont pas de règle
            $this->distribuerMontantSurAidesTravaux();
        }
    }

    /**
     * Calcule le montant du dispositif NON forfaitaire
     */
    public function calculerMontantDispositifNonForfaitaire()
    {
        LoggerSimulation::detail("\n\n\t ** Calcul des regles de Montant des aides des travaux du Dispositif :");

        // Calcul du montant du dispositif par le calcul du montant de ses Aides travaux
        /** @var AideTravauxSimul $aideTravaux */
        foreach ($this->aidesTravaux as $aideTravaux) {
            $aideTravaux->calculerMontantAideTravaux($this->id);
            $this->montant = $this->montant + $aideTravaux->getMontantAideTo();
        }
        LoggerSimulation::detail(
            "\n\n\t ==> [Montant des aides du dispositif obtenu par les regles sur ses travaux = " . $this->montant . ""
        );

        // Si le dispositif possède des Regles de montant (Niveau dispositif)
        LoggerSimulation::detail("\n\n\t ** Calcul des regles de Montant propres au Dispositif :");
        if (count($this->reglesMontant) > 0) {

            LoggerSimulation::detail("\n\t -- Montant calcule par les règles du Dispositif : \n\n");
            //On est dans le cas d’un Dispositif qui a des règles de calcul par travaux
            //mais qui a aussi une règle de calcul de son montant au niveau dispositif.
            // Ceci peut se produire dans le cas où le dispositif rajoute une prime au montant obtenus
            //par les travaux (par exemple le dispositif de « Grand couronne »).
            // Sachant que cette évaluation de la règle de montant du dispositif
            //va venir écraser le montant précédemment obtenu,
            // le concepteur de la règle doit l’écrire sous la forme Md = Md + prime,
            //c’est-à-dire que le montant du dispositif est utilisé comme variable

            // Parcours des règles montant du dispositif
            /** @var RegleSimul $regle */
            foreach ($this->reglesMontant as $regle) {

                $montant = $regle->calculerRegle($this->id, null);
                if ($montant !== null) { // Si l'évaluation de la règle a fourni un montant
                    if ($montant < 0) { // Si le montant est négatif, on le ramène à zéro
                        $montant = 0.0;
                    }
                    $this->montant = $montant; // Affectation du montant au dispositif
                }
            }
            LoggerSimulation::detail(
                "\n\n\t ==> [Montant du dispositif obtenu apres
                evaluation de ses propres regles : " . $this->montant . ""
            );
        }
        // si l'on a obtenu un montant d'aide pour le dispositif
        if ($this->montant > 0) {

            $plafond = $this->calculerPlafondDispositif(
            ); // on calcule si il existe une valeur de plafond pour le dispositif
            if ($plafond !== null) {
                if ($this->montant > $plafond) { // Si le montant du dispositif dépasse le plafond

                    // Il faut réajuster les montants des Aides travaux du dispositif
                    $coeffReajustement = $plafond / $this->montant;
                    $this->montant     = $plafond;
                    $this->impacterPlafondSurAidesTravaux($coeffReajustement);
                }
            }
        }
    }

    /**
     * Calcule le Plafond d'aide d'un dispositif
     */
    public function calculerPlafondDispositif()
    {
        LoggerSimulation::detail("\n\n\t\t - PLAFOND du dispositif : debut {");

        // initialisation: le coût des travaux du dispositif est un plafond
        //du dispositif par principe (les aides ne peuvent dépasser le coût)
        $plafond = $this->coutsTravaux->getCoutTtcTo();
        LoggerSimulation::detail(
            "\n\n\t\t\t ==> [Initialisation du plafond du dispositif avec le cout de ses travaux = " . $plafond . "]"
        );

        /** @var RegleSimul $regle */
        foreach ($this->reglesPlafond as $regle) {

            $plafondRegle = $regle->calculerRegle($this->id, null);

            if ($plafondRegle !== null) { // Si la règle fournit une valeur de plafond
                if ($plafondRegle < 0) { // Si le plafond est négatif on le ramène à zéro
                    $plafondRegle = 0;
                }
                // si le plafond fourni par la règle est < au plafond en cours alors on abaisse le plafond
                if ($plafondRegle < $plafond) {
                    $plafond = $plafondRegle;
                }
            }
        }
        LoggerSimulation::detail("\n\n\t\t\t ==> [plafond du dispositif = " . $plafond . "]");
        LoggerSimulation::detail("\n\t\t } - Fin PLAFOND \n\n");
        return $plafond;
    }

    /**
     * Distribue le montant d'un dispositif forfaitaire sur ses Aides travaux
     */
    public function distribuerMontantSurAidesTravaux()
    {
        /** @var AideTravauxSimul $aideTravaux */
        foreach ($this->aidesTravaux as $aideTravaux) {
            $aideTravaux->distribuerMontantDispositif($this->montant, $this->coutsTravaux);
        }
    }

    /**
     * Impacte le Plafond d'un dispositif sur ses aides travaux
     *
     * @param float $coeffReajustement
     */
    public function impacterPlafondSurAidesTravaux($coeffReajustement)
    {
        // Si le dispositif possède des Aides Travaux => Les montants calculés pour ses Aides
        //doivent être réajustés en fonction du plafond

        /** @var SessionContainerService $sessionContainerService */
        $sessionContainerService = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);
        $isAdmin = $sessionContainerService->estConseillerConnecte();
        if (count($this->aidesTravaux) > 0) {

            /** @var AideTravauxSimul $aideTravaux */
            foreach ($this->aidesTravaux as $aideTravaux) {
                $aideTravaux->impacterPlafondDispositif($coeffReajustement,$isAdmin);
            }
        }
    }

    /**
     * Exclut les travaux des aides CEE si le dispositif exclut les CEE
     * et pour Travaux ayant reçu un montant d'aide par le dispositif
     *
     */
    public function gererExclusionCeeTravaux()
    {
        // Si le dispositif exclut les aides CEE
        if ($this->exclutCEE === true) {
            if (count($this->aidesTravaux) > 0) { // Si le dispositif possède des Aides Travaux
                /** @var AideTravauxSimul $aideTravaux */
                foreach ($this->aidesTravaux as $aideTravaux) {

                    if ($aideTravaux->getMontantAideTo() > 0) { // Si l'Aide Travaux à reçu une aide

                        $travaux = $aideTravaux->getTravaux();
                        $travaux->setEstExcluCEE(true); // Alors le travaux ne pourra bénéficier des aides CEE
                    }
                }
            }
        }
    }

    /**
     * Exclut les travaux des aides de types exclus par le dipositif
     * pour Travaux ayant reçu un montant d'aide par le dispositif
     *
     */
    public function gererExclusionsTravaux()
    {
        $typesExclus = $this->getTypesExclus();
        // Si le dispositif exclut certains types
        if ($typesExclus) {
            /** @var AideTravauxSimul $aideTravaux */
            foreach ($this->aidesTravaux as $aideTravaux) {
                if ($aideTravaux->getMontantAideTo() > 0) { // Si l'Aide Travaux a reçu une aide

                    $travaux = $aideTravaux->getTravaux();

                    $travaux->setEstInclu($this->getType()); // Le travaux a reçu des aides du type de ce dispositif
                    foreach ($typesExclus as $type) {
                        $travaux->setEstExclu($type);//Alors le travaux ne pourra bénéficier des aides de type $type
                    }
                }
            }
        }
    }


    public function isTravauxExclu($travaux){

    }

    public function getTypesExclus(){
        $typesExclus = [];

        if($this->exclutCEE){
            $typesExclus[] = SimulationConstante::TYPE_AIDE_CEE;
        }

        if($this->exclutAnah){
            $typesExclus[] = SimulationConstante::TYPE_AIDE_ANAH;
        }

        if($this->exclutEPCI){
            $typesExclus[] = SimulationConstante::TYPE_AIDE_EPCI;
        }

        if($this->exclutRegion){
            $typesExclus[] = SimulationConstante::TYPE_AIDE_REGION;
        }

        if($this->exclutCommunal){
            $typesExclus[] = SimulationConstante::TYPE_AIDE_COMMUNAL;
        }

        if($this->exclutNational){
            $typesExclus[] = SimulationConstante::TYPE_AIDE_NATIONAL;
        }

        if($this->exclutDepartement){
            $typesExclus[] = SimulationConstante::TYPE_AIDE_DEPARTEMENT;
        }

        if($this->exclutCoupDePouce){
            $typesExclus[] = SimulationConstante::TYPE_AIDE_COUP_DE_POUCE;
        }

        return $typesExclus;
    }

    //----- Evaluation des variables niveau Dispositif -------------------------------------------
    //--------------------------------------------------------------------------------------------
    /**
     * Méthode d'évaluation d'une variable DISPOSITIF (remplacement par sa valeur)
     *
     * @param string $variable
     * @param ProjetSimul $projet
     *
     * @return float
     */
    public function evaluerVariableDispositif($variable)
    {
        // evaluation des variables COUT pour obtenir les sommes des coûts des travaux
        //pris en charge par le dispositif et appartenant au projet
        if (strpos($variable, "cout_") !== false) {

            /** @var TravauxSimul $coutTravaux */
            $coutTravaux = $this->coutsTravaux;

            switch ($variable) {

                case RegleVariableConstante::VARIABLE_LIT_COUT_HT_TOTAL:
                    return $coutTravaux->getCoutHtTo();
                    break;
                case RegleVariableConstante::VARIABLE_LIT_COUT_TTC_TOTAL:
                    return $coutTravaux->getCoutTtcTo();
                    break;
                case RegleVariableConstante::VARIABLE_LIT_COUT_HT_MO:
                    return $coutTravaux->getCoutHtMo();
                    break;
                case RegleVariableConstante::VARIABLE_LIT_COUT_TTC_MO:
                    return $coutTravaux->getCoutTtcMo();
                    break;
                case RegleVariableConstante::VARIABLE_LIT_COUT_HT_FO:
                    return $coutTravaux->getCoutHtFo();
                    break;
                case RegleVariableConstante::VARIABLE_LIT_COUT_TTC_FO:
                    return $coutTravaux->getCoutTtcFo();
                    break;
                default:
                    echo("variable [" . $variable . "] inconnue !\n");
                    break;
            }
            // evaluation de la variable montant du dispositif
        } else {
            if($variable == "montant_to") {
                return $this->montant;
            } elseif($variable == RegleVariableConstante::VARIABLE_LIT_NB_TRAVAUX_ELIGIBLES) {
                return count($this->aidesTravaux);
            } else {
                echo("variable [" . $variable . "] inconnue !\n");
            }
        }
    }

    /**
     * Méthode d'évaluation d'une variable AIDE TRAVAUX (remplacement par sa valeur)
     *
     * @param string $variable
     * @param string $index
     *
     * @return float
     */
    public function evaluerVariableAideTravaux($variable, $index)
    {
        if (strpos($variable, "montant_") !== false) {
            $montant         = 0;
            $codeAideTravaux = $index;
            /** @var AideTravauxSimul $aideTravaux */
            $aideTravaux = $this->aidesTravaux[$codeAideTravaux];

            switch ($variable) {

                case "montant_to":
                    $montant = $aideTravaux->getMontantAideTo();
                    break;
                case "montant_mo":
                    $montant = $aideTravaux->getMontantAideMo();
                    break;
                case "montant_fo":
                    $montant = $aideTravaux->getMontantAideFo();
                    break;
                default:
                    echo("variable [" . $variable . "] inconnue !\n");
                    break;
            }
            return $montant;

        } else {
            echo("variable [" . $variable . "] inconnue !\n");
        }
    }

    //----- Méthodes utilitaires sur le dispositif -------------------------------------------
    //----------------------------------------------------------------------------------------

    /**
     * Ajoute une règle au dispositif selon son type
     *
     * @param RegleSimul $regle
     */
    public function addRegle($regle)
    {
        switch ($regle->getType()) {
            case ValeurListeConstante::TYPE_REGLE_DISPOSITIF_ELIGIBILITE:
                $this->reglesEligibilite[] = $regle;
                break;
            case ValeurListeConstante::TYPE_REGLE_DISPOSITIF_MONTANT:
                $this->reglesMontant[] = $regle;
                break;
            case ValeurListeConstante::TYPE_REGLE_DISPOSITIF_PLAFOND:
                $this->reglesPlafond[] = $regle;
                break;
        }
    }

    /**
     * Retourne une Aide travaux du dispositif à partir du code travaux.
     * Retourne null si l'Aide travaux n'est pas dans le dispositif
     *
     * @param string $codeTravaux
     *
     * @return AideTravauxSimul $travaux
     */
    public function getAideTravauxByCode($codeTravaux)
    {

        if (isset($this->aidesTravaux[$codeTravaux]) === true) {
            return $this->aidesTravaux[$codeTravaux];
        }
        return null;
    }

    /**
     * Ajoute une Aide travaux au dispositif
     *
     * @param AideTravauxSimul $aideTravaux
     */
    public function addAideTravaux($aideTravaux)
    {

        $codeTravaux                      = $aideTravaux->getCode();
        $this->aidesTravaux[$codeTravaux] = $aideTravaux;
    }

    /**
     * Supprime une Aide travaux du dispositif
     * @param $codeTravaux
     */
    public function removeAideTravaux($codeTravaux)
    {
        if(isset($this->aidesTravaux[$codeTravaux])){
            unset($this->aidesTravaux[$codeTravaux]);
        }
    }

    //-------------  Accesseur -----------------
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $numOrdre
     */
    public function setNumOrdre($numOrdre)
    {
        $this->numOrdre = (int)$numOrdre;
    }

    /**
     * @return int
     */
    public function getNumOrdre()
    {
        return $this->numOrdre;
    }

    /**
     * @param int $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return int
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param boolean $forfaitaire
     */
    public function setForfaitaire($forfaitaire)
    {
        $this->forfaitaire = $forfaitaire;
    }

    /**
     * @return boolean
     */
    public function getForfaitaire()
    {
        return $this->forfaitaire;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @param string $financeur
     */
    public function setFinanceur($financeur)
    {
        $this->financeur = $financeur;
    }

    /**
     * @return string
     */
    public function getFinanceur()
    {
        return $this->financeur;
    }

    /**
     * @param boolean $exclutCEE
     */
    public function setExclutCEE($exclutCEE)
    {
        $this->exclutCEE = (bool)$exclutCEE;
    }

    /**
     * @return boolean
     */
    public function getExclutCEE()
    {
        return $this->exclutCEE;
    }

    /**
     * @param boolean $evaluerRegle
     */
    public function setEvaluerRegle($evaluerRegle)
    {
        $this->evaluerRegle = (bool)$evaluerRegle;
    }

    /**
     * @return boolean
     */
    public function getEvaluerRegle()
    {
        return $this->evaluerRegle;
    }

    /**
     * @param AideTravauxSimul $aidesTravaux
     */
    public function setAidesTravaux($aidesTravaux)
    {
        $this->aidesTravaux = $aidesTravaux;
    }

    /**
     * @return AideTravauxSimul[]
     */
    public function getAidesTravaux()
    {
        return $this->aidesTravaux;
    }


    /**
     * @param TravauxSimul $coutsTravaux
     */
    public function setCoutsTravaux($coutsTravaux)
    {
        $this->coutsTravaux = $coutsTravaux;
    }

    /**
     * @return TravauxSimul
     */
    public function getCoutsTravaux()
    {
        return $this->coutsTravaux;
    }

    /**
     * @param bool $eligibilite
     */
    public function setEligibilite($eligibilite)
    {
        $this->eligibilite = $eligibilite;
    }

    /**
     * @return bool
     */
    public function getEligibilite()
    {
        return $this->eligibilite;
    }

    /**
     * @param float $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }

    /**
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param array $reglesEligibilite
     */
    public function setReglesEligibilite($reglesEligibilite)
    {
        $this->reglesEligibilite = $reglesEligibilite;
    }

    /**
     * @return array
     */
    public function getReglesEligibilite()
    {
        return $this->reglesEligibilite;
    }

    /**
     * @param array $reglesMontant
     */
    public function setReglesMontant($reglesMontant)
    {
        $this->reglesMontant = $reglesMontant;
    }

    /**
     * @return array
     */
    public function getReglesMontant()
    {
        return $this->reglesMontant;
    }

    /**
     * @param array $reglesPlafond
     */
    public function setReglesPlafond($reglesPlafond)
    {
        $this->reglesPlafond = $reglesPlafond;
    }

    /**
     * @return array
     */
    public function getReglesPlafond()
    {
        return $this->reglesPlafond;
    }

    /**
     * @return bool
     */
    public function isExclutDepartement()
    {
        return $this->exclutDepartement;
    }

    /**
     * @param bool $exclutDepartement
     */
    public function setExclutDepartement($exclutDepartement)
    {
        $this->exclutDepartement = $exclutDepartement;
    }

    /**
     * @return bool
     */
    public function isExclutAnah()
    {
        return $this->exclutAnah;
    }

    /**
     * @param bool $exclutAnah
     */
    public function setExclutAnah($exclutAnah)
    {
        $this->exclutAnah = $exclutAnah;
    }

    /**
     * @return bool
     */
    public function isExclutEPCI()
    {
        return $this->exclutEPCI;
    }

    /**
     * @param bool $exclutEPCI
     */
    public function setExclutEPCI($exclutEPCI)
    {
        $this->exclutEPCI = $exclutEPCI;
    }

    /**
     * @return bool
     */
    public function isExclutRegion()
    {
        return $this->exclutRegion;
    }

    /**
     * @param bool $exclutRegion
     */
    public function setExclutRegion($exclutRegion)
    {
        $this->exclutRegion = $exclutRegion;
    }

    /**
     * @return bool
     */
    public function isExclutCommunal()
    {
        return $this->exclutCommunal;
    }

    /**
     * @param bool $exclutCommunal
     */
    public function setExclutCommunal($exclutCommunal)
    {
        $this->exclutCommunal = $exclutCommunal;
    }

    /**
 * @return bool
 */
    public function isExclutNational()
    {
        return $this->exclutNational;
    }

    /**
     * @param bool $exclutNational
     */
    public function setExclutNational($exclutNational)
    {
        $this->exclutNational = $exclutNational;
    }

    /**
     * @return bool
     */
    public function isExclutCoupDePouce()
    {
        return $this->exclutCoupDePouce;
    }

    /**
     * @param bool $exclutCoupDePouce
     */
    public function setExclutCoupDePouce($exclutCoupDePouce)
    {
        $this->exclutCoupDePouce = $exclutCoupDePouce;
    }

    /**
     * @return mixed
     */
    public function getOblige()
    {
        return $this->oblige;
    }

    /**
     * @param mixed $oblige
     */
    public function setOblige($oblige)
    {
        $this->oblige = $oblige;
    }

    /**
     * @return DateTime
     */
    public function getDebutValidite()
    {
        return $this->debutValidite;
    }

    /**
     * @param DateTime $debutValidite
     */
    public function setDebutValidite($debutValidite)
    {
        $this->debutValidite = $debutValidite;
    }

    /**
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * @param string $siteWeb
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;
    }

    /**
     * @return int
     */
    public function getIdDispositifRequis()
    {
        return $this->idDispositifRequis;
    }

    /**
     * @param string $siteWeb
     */
    public function setIdDispositifRequis($idDispositifRequis)
    {
        $this->idDispositifRequis = $idDispositifRequis;
    }


}
