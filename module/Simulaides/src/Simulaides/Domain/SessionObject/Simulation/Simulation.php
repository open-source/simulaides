<?php
namespace Simulaides\Domain\SessionObject\Simulation;

use Simulaides\Constante\DispositifConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\ProjetChoixDispositif;
use Simulaides\Logger\LoggerSimulation;
use Simulaides\Service\DispositifSimulService;
use Simulaides\Service\OffresCeeService;
use Simulaides\Service\ProjetSimulService;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe Simulation
 * Objet représentant une simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class Simulation
{
    /** @var string */
    private $modeExecution;

    /** @var  ProjetSimul */
    private $projet;

    /** @var array */
    private $dispositifs = array();

    /** @var float */
    private $montantTotal;

    /** @var  boolean */
    private $existDispositifElligible;

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /**
     * Constructeur
     *
     * @param                         $modeExecution
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct($modeExecution, ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->modeExecution  = $modeExecution;
        $this->montantTotal   = 0.0;
    }

    /**
     * Initialisation de la simulation
     *
     * @param int $idProjet Identifiant du projet à simuler
     * @param array $tabIdDispositifSelectionnes Liste des id dispositif à traiter
     * @param bool $offresCdpSeulement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function init($idProjet, $tabIdDispositifSelectionnes, $offresCdpSeulement = false)
    {
        // Chargement des données du projet
        //(données saisies pour : travaux sélectionnés, produits, paramètres technique, calcul des coûts)
        /** @var ProjetSimulService $projetSimulService */
        $projetSimulService = $this->serviceLocator->get(ProjetSimulService::SERVICE_NAME);
        $this->projet       = $projetSimulService->getProjetSimul($idProjet);

        //Inialisation du logger
        LoggerSimulation::init($this->projet->getNumero());

        // Chargement des dispositifs qui sont compatibles avec le projet
        //(critère géographique, période validité, travaux pris en charge,...)
        /** @var DispositifSimulService $dispositifSimulService */
        $dispositifSimulService = $this->serviceLocator->get(DispositifSimulService::SERVICE_NAME);
        $this->dispositifs      = $dispositifSimulService->getDispositifsCandidats(
            $this->modeExecution,
            $this->projet,
            $tabIdDispositifSelectionnes,
            $offresCdpSeulement
        );

        $dispositifsLibelle = array();
        foreach ($this->dispositifs as $dispositif){
            $dispositifsLibelle[] = $dispositif->getIntitule();
        }
        LoggerSimulation::detail(
            "\n============================================================================================"
        );
        LoggerSimulation::detail(
            "\n\n liste des Dispositifs : " . implode(', ',$dispositifsLibelle)
        );
        LoggerSimulation::detail(
            "\n============================================================================================"
        );

    }

    /**
     * Evaluation de la simulation
     */
    public function evaluerSimulation($offresCdpSeulement=false)
    {
        $coutsTravauxProjet = $this->projet->getCoutTotalTravaux();
        /** @var DispositifSimulService $dispositifSimulService */
        $dispositifSimulService = $this->serviceLocator->get(DispositifSimulService::SERVICE_NAME);

        $offresCeeService = $this->serviceLocator->get(OffresCeeService::SERVICE_NAME);
        /** @var ProjetChoixDispositif $projetChoixDispositif */

        /** @var DispositifSimul $dispositif */
        $idProjet = $this->projet->getId();
        $maxAtteint = false;

        // @todo récuperation des choix dispositifs CDP
        // purge choix des dispositifs CDP choisit
        $this->getOffresCeeService()->purgeOffresSelectionnees();
        $projetChoixDispositifs = $this->getOffresCeeService()->getOffresSelectionnees();
        $choixDispositifCDP = [];
        $travauxCDPChoix = [];
        /** @var ProjetChoixDispositif $projetChoixDispositif */

       foreach ($projetChoixDispositifs as $projetChoixDispositif){
           $choixDispositifCDP[$projetChoixDispositif->getIdDispositif()][]
               = $projetChoixDispositif->getCodeTravaux();
           if(!in_array($projetChoixDispositif->getCodeTravaux(),$travauxCDPChoix)){
               $travauxCDPChoix[] = $projetChoixDispositif->getCodeTravaux();
           }
       }

        $tabCPRPrincipaux = explode(',',DispositifConstante::IDS_DISPOSITIF_COUPDEPOUCE);
        $this->travauxCDP = [];
        $tabDispoEligible = [];
        foreach ($this->dispositifs as $dispositif) {
            LoggerSimulation::detail(
                "\n\n Evaluation du Dispositif : " . $dispositif->getIntitule(
                ) . " [numéro d'ordre : " . $dispositif->getNumOrdre() . "]"
            );
            LoggerSimulation::detail(
                "\n============================================================================================"
            );

            // Analyse de l'éligibilité du dispositif
            $dispositif->calculerEligibilite($this->getModeExecution(),$tabDispoEligible);

            if ($dispositif->getEligibilite() === true) {
                if($dispositif->getEvaluerRegle() === true) {

                    $tabDispoEligible[] = $dispositif->getId();

                    if ($dispositif->getType() == ValeurListeConstante::TYPE_DISPOSITIF_CEE) {
                        LoggerSimulation::detail("\nCalcul des aides CEE");
                        LoggerSimulation::detail("\n----------------------");
                        /*if(!$offresCdpSeulement && $countprojetChoixDispositifs > 0){
                            $dispositifSimulService->supprimerTravauxPourCEE($idProjet,$dispositif);
                        }*/
                        $dispositif->calculerMontantDispositifCEE($this->projet);
                    }
                    // Calcul du montant du dispositif
                    if ($dispositif->getType() != ValeurListeConstante::TYPE_DISPOSITIF_CEE) {
                        if(!$maxAtteint || $dispositif->getType() == ValeurListeConstante::TYPE_DISPOSITIF_COUP_DE_POUCE){
                            $dispositif->calculerMontantDispositif();
                            $dispositif->gererExclusionsTravaux();//gererExclusionCeeTravaux();
                        }else{
                            continue;
                        }
                    }
                    // Si l'ajout du montant du dispositif fait que le montant total des aides
                    // devient supérieur au coût total des travaux du projet
                    // Alors il faut plafonner le montant du dispositif et réajuster ses aides travaux
                    // et on ne traite pas les dispositif suivants
                    if(in_array($dispositif->getId(),$tabCPRPrincipaux)){
                        $this->ajouteMontantCDPPrincipaux($dispositif,$travauxCDPChoix);
                       continue;
                    }else if($dispositif->getType() == ValeurListeConstante::TYPE_DISPOSITIF_COUP_DE_POUCE){
                        if(isset($choixDispositifCDP[$dispositif->getId()])){
                            //on ajoute le montant du ou des travaux
                            $this->ajouteMontantCDP($dispositif,$choixDispositifCDP[$dispositif->getId()]);
                        }
                        continue;
                    }else if (($this->montantTotal + $dispositif->getMontant()) > $coutsTravauxProjet) {
                        $plafond           = $coutsTravauxProjet - $this->montantTotal;
                        $coeffReajustement = $plafond / $dispositif->getMontant();
                        $dispositif->setMontant($plafond);
                        $dispositif->impacterPlafondSurAidesTravaux($coeffReajustement);
                        $this->montantTotal = $this->montantTotal + $dispositif->getMontant();
                        $maxAtteint = true;
                        LoggerSimulation::detail("\n------------------------------------------------------------------");
                        LoggerSimulation::detail("\nEcrêtement du dispositif : Réajustement des aides");
                        LoggerSimulation::detail("\n------------------------------------------------------------------");

                        continue;
                    } else {
                        $this->montantTotal = $this->montantTotal + $dispositif->getMontant();
                    }
                }else{
                    LoggerSimulation::detail("\nCalcul des aides Non calculer");
                    LoggerSimulation::detail("\n----------------------");
                }
                $this->existDispositifElligible = true;
            }
        }
        LoggerSimulation::detail(
            "\n\n============================================================================================"
        );
        LoggerSimulation::detail("\n\n ==> [Montant total des aides pour la Simulation = " . $this->montantTotal . "]");
        LoggerSimulation::detail(
            "\n\n============================================================================================"
        );
    }

    /**
     * Evaluation des variables de la simulation
     *
     * @param DispositifSimul $dispositif
     * @param array $tabTravaux
     */
    public function ajouteMontantCDP($dispositif,$tabTravaux){
        foreach ($dispositif->getAidesTravaux() as $aidesTravaux){
            if(in_array($aidesTravaux->getCode(),$tabTravaux)){
                $this->montantTotal = $this->montantTotal + $aidesTravaux->getMontantAideTo();
            }
        }
    }

    /**
     * Evaluation des variables de la simulation
     *
     * @param DispositifSimul $dispositif
     * @param array $tabTravaux
     */
    public function ajouteMontantCDPPrincipaux($dispositif,$tabTravaux){
        foreach ($dispositif->getAidesTravaux() as $aidesTravaux){
            if(!in_array($aidesTravaux->getCode(),$tabTravaux)){
                $this->montantTotal = $this->montantTotal + $aidesTravaux->getMontantAideTo();
            }
        }
    }



    /**
     * Evaluation des variables de la simulation
     *
     * @param string $variable
     * @param string $index
     * @param int    $idDispositifEnEval
     *
     * @return float
     */
    public function evaluerVariableSimulation($variable, $index, $idDispositifEnEval)
    {
        $cumul = 0.0;

        // récupération du numéro d'ordre du dispositif en cours d'évaluation
        /** @var DispositifSimul $dispositif */
        $dispositif     = $this->dispositifs[$idDispositifEnEval];
        $numOrdreEnEval = $dispositif->getNumOrdre();

        if (strpos($variable, "cumul_") !== false) {

            // On récupère le code de l'Aide travaux mis entre crochets dans la variable ex: cumul_to[T01]
            $codeAideTravaux = $index;

            // parcours sur les dispositifs de la simulation
            /** @var DispositifSimul $dispositif */
            foreach ($this->dispositifs as $dispositif) {

                // on ne prend que les dispositifs déjà évalués et le dispositif en cours d'évaluation
                // c'est à dire avec un numéro d'ordre inférieur ou égal au dispositif en cours d'évaluation
                if ($dispositif->getNumOrdre() <= $numOrdreEnEval) {

                    $aideTravaux = $dispositif->getAideTravauxByCode($codeAideTravaux);
                    // Si le dispositif prend bien en charge ce travaux
                    if ($aideTravaux !== null) {
                        switch ($variable) {
                            case "cumul_to":
                                $cumul = $cumul + $aideTravaux->getMontantAideTo();
                                break;
                            case "cumul_mo":
                                $cumul = $cumul + $aideTravaux->getMontantAideMo();
                                break;
                            case "cumul_fo":
                                $cumul = $cumul + $aideTravaux->getMontantAideFo();
                                break;
                            default:
                                echo("variable [" . $variable . "] inconnue !\n");
                                break;
                        }
                    }
                }
            }
            return $cumul;

        } else {
            echo("variable [" . $variable . "] inconnue !\n");
        }
    }

    /**
     * Retourne le dispositif selon son Identifiant
     *
     * @param int $idDispositif Identifiant du dispositif
     *
     * @return DispositifSimul
     */
    public function getDispositifById($idDispositif)
    {
        return $this->dispositifs[$idDispositif];
    }

    //-------------  Accesseur -----------------
    /**
     * @param string $modeExecution
     */
    public function setModeExecution($modeExecution)
    {
        $this->modeExecution = $modeExecution;
    }

    /**
     * @return string
     */
    public function getModeExecution()
    {
        return $this->modeExecution;
    }

    /**
     * @param ProjetSimul $projet
     */
    public function setProjet($projet)
    {
        $this->projet = $projet;
    }

    /**
     * @return ProjetSimul
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * @param array $dispositifs
     */
    public function setDispositifs($dispositifs)
    {
        $this->dispositifs = $dispositifs;
    }

    /**
     * @return array
     */
    public function getDispositifs()
    {
        return $this->dispositifs;
    }

    /**
     * @param float $montantTotal
     */
    public function setMontantTotal($montantTotal)
    {
        $this->montantTotal = $montantTotal;
    }

    /**
     * @return float
     */
    public function getMontantTotal()
    {
        return $this->montantTotal;
    }

    /**
     * @param boolean $existDispositifElligible
     */
    public function setExistDispositifElligible($existDispositifElligible)
    {
        $this->existDispositifElligible = $existDispositifElligible;
    }

    /**
     * @return boolean
     */
    public function getExistDispositifElligible()
    {
        return $this->existDispositifElligible;
    }

    /**
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Retourne le service sur les offres cee
     *
     * @return array|object|OffresCeeService
     */
    private function getOffresCeeService()
    {
        if (empty($this->offresCeeService)) {
            $this->offresCeeService = $this->serviceLocator->get(OffresCeeService::SERVICE_NAME);
        }
        return $this->offresCeeService;
    }
}
