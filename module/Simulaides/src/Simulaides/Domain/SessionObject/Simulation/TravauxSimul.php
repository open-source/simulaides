<?php
namespace Simulaides\Domain\SessionObject\Simulation;

use Simulaides\Constante\RegleVariableConstante;

/**
 * Classe TravauxSimul
 * Classe de stockage des données d'un produit dans le cadre de l'exécution d'une simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class TravauxSimul
{
    /** @var string code */
    private $code;

    /** @var string intitule */
    private $intitule;

    /** @var float coutHtTo */
    private $coutHtTo;

    /** @var float coutHtMo */
    private $coutHtMo;

    /** @var float coutHtFo */
    private $coutHtFo;

    /** @var float coutTtcTo */
    private $coutTtcTo;

    /** @var float coutTtcMo */
    private $coutTtcMo;

    /** @var float coutTtcFo */
    private $coutTtcFo;

    /** @var boolean estExcluCEE */
    private $estExcluCEE;

    private $typesExclus = array();

    private $typesInclus = array();

    /** @var array produits */
    private $produits = array();

    /**
     * Constructeur
     *
     * @param String $code
     * @param string $intitule
     */
    public function __construct($code, $intitule)
    {
        $this->code        = $code;
        $this->intitule    = $intitule;
        $this->coutHtTo    = 0.0;
        $this->coutHtMo    = 0.0;
        $this->coutHtFo    = 0.0;
        $this->coutTtcTo   = 0.0;
        $this->coutTtcMo   = 0.0;
        $this->coutTtcFo   = 0.0;
        $this->estExcluCEE = false;
    }

    /**
     * Fait appel aux calculs des coûts pour chacun des produits du travaux
     */
    public function calculCouts()
    {
        /** @var ProduitSimul $produit */
        foreach ($this->produits as $produit) {
            $coutProduit = $produit->calculCouts();

            $this->coutHtTo  = $this->coutHtTo + $coutProduit->getCoutHtTo();
            $this->coutHtMo  = $this->coutHtMo + $coutProduit->getCoutHtMo();
            $this->coutHtFo  = $this->coutHtFo + $coutProduit->getCoutHtFo();
            $this->coutTtcTo = $this->coutTtcTo + $coutProduit->getCoutTtcTo();
            $this->coutTtcMo = $this->coutTtcMo + $coutProduit->getCoutTtcMo();
            $this->coutTtcFo = $this->coutTtcFo + $coutProduit->getCoutTtcFo();

        }
    }

    /**
     * Méthode d'évaluation d'une variable TRAVAUX (remplacement par sa valeur)
     *
     * @param string $variable
     * @param string $index
     *
     * @return float|int|boolean|string $resultat
     */
    public function evaluerVariableTravaux($variable, $index)
    {

        switch ($variable) {

            case RegleVariableConstante::VARIABLE_LIT_COUT_HT_TOTAL:
                return $this->coutHtTo;
                break;
            case RegleVariableConstante::VARIABLE_LIT_COUT_TTC_TOTAL:
                return $this->coutTtcTo;
                break;
            case RegleVariableConstante::VARIABLE_LIT_COUT_HT_MO:
                return $this->coutHtMo;
                break;
            case RegleVariableConstante::VARIABLE_LIT_COUT_TTC_MO:
                return $this->coutTtcMo;
                break;
            case RegleVariableConstante::VARIABLE_LIT_COUT_HT_FO:
                return $this->coutHtFo;
                break;
            case RegleVariableConstante::VARIABLE_LIT_COUT_TTC_FO:
                return $this->coutTtcFo;
                break;
            case RegleVariableConstante::VARIABLE_LIT_PARAM_TECH:
                $codeParamTech = $index;
                return $this->getValeurParamTech($codeParamTech);
                break;

            default:
                echo("variable [" . $variable . "] inconnue !\n");
                break;
        }
    }

    /**
     * Retourne un Produit du travaux à partir de son code s'il existe et null sinon
     *
     * @param string $codeProduit
     *
     * @return ProduitSimul $produit
     */
    public function getProduitByCode($codeProduit)
    {

        if (isset($this->produits[$codeProduit]) === true) {
            return $this->produits[$codeProduit];
        }
        return null;
    }

    /**
     * Ajoute un travaux au projet
     *
     * @param ProduitSimul $produit
     */
    public function addProduit($produit)
    {

        $codeProduit                  = $produit->getCode();
        $this->produits[$codeProduit] = $produit;
    }

    /**
     * Retourne la valeur d'un parametre technique du travaux
     *
     * @param string $codeParam
     *
     * @return int|boolean|string
     */
    public function getValeurParamTech($codeParam)
    {

        $valeurParam = null;
        /** @var ProduitSimul $produit */
        foreach ($this->produits as $produit) {
            $resultat = $produit->getValeurParametreByCode($codeParam);
            if ($resultat !== null) {
                if($resultat === 'OUI'){
                    $valeurParam = true;
                }else if($resultat === 'NON'){
                    $valeurParam = false;
                }else{
                    $valeurParam = $resultat;
                }
            }
        }
        return $valeurParam;
    }

    //-------------  Accesseur -----------------
    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param float $coutHtTo
     */
    public function setCoutHtTo($coutHtTo)
    {
        $this->coutHtTo = $coutHtTo;
    }

    /**
     * @return float
     */
    public function getCoutHtTo()
    {
        return $this->coutHtTo;
    }

    /**
     * @param float $coutHtMo
     */
    public function setCoutHtMo($coutHtMo)
    {
        $this->coutHtMo = $coutHtMo;
    }

    /**
     * @return float
     */
    public function getCoutHtMo()
    {
        return $this->coutHtMo;
    }

    /**
     * @param float $coutHtFo
     */
    public function setCoutHtFo($coutHtFo)
    {
        $this->coutHtFo = $coutHtFo;
    }

    /**
     * @return float
     */
    public function getCoutHtFo()
    {
        return $this->coutHtFo;
    }

    /**
     * @param float $coutTtcTo
     */
    public function setCoutTtcTo($coutTtcTo)
    {
        $this->coutTtcTo = $coutTtcTo;
    }

    /**
     * @return float
     */
    public function getCoutTtcTo()
    {
        return $this->coutTtcTo;
    }

    /**
     * @param float $coutTtcMo
     */
    public function setCoutTtcMo($coutTtcMo)
    {
        $this->coutTtcMo = $coutTtcMo;
    }

    /**
     * @return float
     */
    public function getCoutTtcMo()
    {
        return $this->coutTtcMo;
    }

    /**
     * @param float $coutTtcFo
     */
    public function setCoutTtcFo($coutTtcFo)
    {
        $this->coutTtcFo = $coutTtcFo;
    }

    /**
     * @return float
     */
    public function getCoutTtcFo()
    {
        return $this->coutTtcFo;
    }

    /**
     * @param boolean $estExcluCEE
     */
    public function setEstExcluCEE($estExcluCEE)
    {
        $this->estExcluCEE = $estExcluCEE;
    }

    /**
     * @return boolean
     */
    public function getEstExcluCEE()
    {
        return $this->estExcluCEE;
    }

    /**
     * @param string $type - type d'aide exclu du calcul d'aides apportées à ce travaux
     */
    public function setEstExclu($type){
        $this->typesExclus[$type] = true;
    }

    /**
     * @param string $type
     * @return boolean - true si le type de dispositif $type est exclu du calcul d'aides apportées à ce travaux
     */
    public function getEstExclu($type){
        return (isset($this->typesExclus[$type]) && $this->typesExclus[$type]);
    }

    /**
     * @return array - liste des types de dispositif exclus du calcul d'aides apportées
     */
    public function getTypesExclus(){
        return array_keys($this->typesExclus);
    }

    public function setEstInclu($type){
        $this->typesInclus[$type] = true;
    }

    public function getEstInclu($type){
        return (isset($this->typesInclus[$type]) && $this->typesInclus[$type]);
    }

    /**
     * @return array - liste des types de dispositif ayant été déjà pris en compte dans le calcul d'aides apportées
     */
    public function getTypesInclus(){
        return array_keys($this->typesInclus);
    }

    /**
     * @param array $produits
     */
    public function setProduits($produits)
    {
        $this->produits = $produits;
    }

    /**
     * @return array
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }
}
