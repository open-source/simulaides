<?php
namespace Simulaides\Domain\SessionObject\Simulation;

/**
 * Classe SimulationResultat
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class SimulationResultat
{
    /** @var string */
    private $modeExecution;

    /** @var array */
    private $dispositifs = array();

    /** @var float */
    private $montantTotal;

    /** @var  boolean */
    private $existDispositifElligible;

    /** @var float */
    private $coutTotalTravaux;

    /** @var  float */
    private $mtConseillerDispositif;

    /**
     * @param array $dispositifs
     */
    public function setDispositifs($dispositifs)
    {
        $this->dispositifs = $dispositifs;
    }

    /**
     * @return array
     */
    public function getDispositifs()
    {
        return $this->dispositifs;
    }

    /**
     * @param boolean $existDispositifElligible
     */
    public function setExistDispositifElligible($existDispositifElligible)
    {
        $this->existDispositifElligible = $existDispositifElligible;
    }

    /**
     * @return boolean
     */
    public function getExistDispositifElligible()
    {
        return $this->existDispositifElligible;
    }

    /**
     * @param string $modeExecution
     */
    public function setModeExecution($modeExecution)
    {
        $this->modeExecution = $modeExecution;
    }

    /**
     * @return string
     */
    public function getModeExecution()
    {
        return $this->modeExecution;
    }

    /**
     * @param float $montantTotal
     */
    public function setMontantTotal($montantTotal)
    {
        $this->montantTotal = $montantTotal;
    }

    /**
     * @return float
     */
    public function getMontantTotal()
    {
        return $this->montantTotal;
    }

    /**
     * @param float $coutTotalTravaux
     */
    public function setCoutTotalTravaux($coutTotalTravaux)
    {
        $this->coutTotalTravaux = $coutTotalTravaux;
    }

    /**
     * @return float
     */
    public function getCoutTotalTravaux()
    {
        return $this->coutTotalTravaux;
    }

    /**
     * @param float $mtConseillerDispositif
     */
    public function setMtConseillerDispositif($mtConseillerDispositif)
    {
        $this->mtConseillerDispositif = $mtConseillerDispositif;
    }

    /**
     * @return float
     */
    public function getMtConseillerDispositif()
    {
        return $this->mtConseillerDispositif;
    }
}
