<?php
namespace Simulaides\Domain\SessionObject\Simulation;

use Simulaides\Service\CoutsProduitService;
use Simulaides\Service\ProduitService;

/**
 * Classe ProduitSimul
 * Classe de stockage des données d'un Produit dans le cadre de l'exécution d'une simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class ProduitSimul
{
    /** @var string code */
    private $code;

    /** @var float coutTo */
    private $coutTo;

    /** @var float coutMo */
    private $coutMo;

    /** @var float coutFo */
    private $coutFo;

    /** @var int  nombreUnites */
    private $nombreUnites;

    /** @var boolean outre_mer */
    private $outre_mer;

    /** @var array de ParametreSimul parametres */
    private $parametres = array();

    /** @var  ProduitService */
    private $produitService;

    /**
     * Constructeur
     *
     * @param string         $code
     * @param ProduitService $pdtService
     */
    public function __construct($code, $pdtService)
    {
        $this->code           = $code;
        $this->coutTo         = null;
        $this->coutMo         = null;
        $this->coutFo         = null;
        $this->nombreUnites   = null;
        $this->produitService = $pdtService;
    }

    /**
     * Retourne les coûts d'un produit
     *
     * @return CoutsProduit
     */
    public function calculCouts()
    {
        $coutsProduit = $this->produitService->getCouts(
            $this->code,
            $this->coutTo,
            $this->coutMo,
            $this->coutFo,
            $this->nombreUnites,
            $this->outre_mer
        );
        return $coutsProduit;
    }

    /**
     * Retourne un paramètre technique du produit à partir de son code s'il existe et null sinon
     *
     * @param string $codeParametre
     *
     * @return ParametreSimul $parametre
     */
    public function getParametreByCode($codeParametre)
    {
        if (isset($this->parametres[$codeParametre]) === true) {
            return $this->parametres[$codeParametre];
        }
        return null;
    }

    /**
     * Retourne la valeur d'un paramètre technique du produit à partir de son code s'il existe et null sinon
     *
     * @param string $codeParametre
     *
     * @return int|boolean|string
     */
    public function getValeurParametreByCode($codeParametre)
    {
        if (isset($this->parametres[$codeParametre]) === true) {
            /** @var ParametreSimul $param */
            $param = $this->parametres[$codeParametre];
            return $param->getValeur();
        }
        return null;
    }

    /**
     * Ajoute un paramètre technique au produit
     *
     * @param ParametreSimul $parametre
     */
    public function addParametre($parametre)
    {

        $codeParametre                    = $parametre->getCode();
        $this->parametres[$codeParametre] = $parametre;
    }

    //-------------  Accesseur -----------------

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param float $coutTo
     */
    public function setCoutTo($coutTo)
    {
        $this->coutTo = $coutTo;
    }

    /**
     * @return float
     */
    public function getCoutTo()
    {
        return $this->coutTo;
    }

    /**
     * @param float $coutMo
     */
    public function setCoutMo($coutMo)
    {
        $this->coutMo = $coutMo;
    }

    /**
     * @return float
     */
    public function getCoutMo()
    {
        return $this->coutMo;
    }

    /**
     * @param float $coutFo
     */
    public function setCoutFo($coutFo)
    {
        $this->coutFo = $coutFo;
    }

    /**
     * @return float
     */
    public function getCoutFo()
    {
        return $this->coutFo;
    }

    /**
     * @param int $nombreUnites
     */
    public function setNombreUnites($nombreUnites)
    {
        $this->nombreUnites = (int)$nombreUnites;
    }

    /**
     * @return int
     */
    public function getNombreUnites()
    {
        return $this->nombreUnites;
    }

    /**
     * @param boolean $outre_mer
     */
    public function setOutre_mer($outre_mer)
    {
        $this->outre_mer = (int)$outre_mer;
    }

    /**
     * @return boolean
     */
    public function getOutre_mer()
    {
        return $this->outre_mer;
    }
}
