<?php
namespace Simulaides\Domain\SessionObject\Simulation;

/**
 * Classe CoutsProduit
 * Classe contenant les coûts calculés d'un produit
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class CoutsProduit extends CoutsSimul
{
    /** @var float prixUserTo */
    private $prixUserTo;

    /** @var float prixUserMo */
    private $prixUserMo;

    /** @var float prixUserFo */
    private $prixUserFo;

    /**
     * Constructeur
     */
    public function __construct()
    {
        parent::__construct();

        // Les prixUser sont les prix unitaires qui peuvent être obtenus
        //à partir des couts saisis par l'utilisateur (administration du référentiel des prix)
        $prixUserTo = null;
        $prixUserMo = null;
        $prixUserFo = null;
    }

    //-------------  Accesseur -----------------

    /**
     * @param float $prixUserTo
     */
    public function setPrixUserTo($prixUserTo)
    {
        $this->prixUserTo = $prixUserTo;
    }

    /**
     * @return float
     */
    public function getPrixUserTo()
    {
        return $this->prixUserTo;
    }

    /**
     * @param float $prixUserMo
     */
    public function setPrixUserMo($prixUserMo)
    {
        $this->prixUserMo = $prixUserMo;
    }

    /**
     * @return float
     */
    public function getPrixUserMo()
    {
        return $this->prixUserMo;
    }

    /**
     * @param float $prixUserFo
     */
    public function setPrixUserFo($prixUserFo)
    {
        $this->prixUserFo = $prixUserFo;
    }

    /**
     * @return float
     */
    public function getPrixUserFo()
    {
        return $this->prixUserFo;
    }
}
