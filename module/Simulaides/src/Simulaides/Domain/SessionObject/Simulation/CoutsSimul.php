<?php
namespace Simulaides\Domain\SessionObject\Simulation;

/**
 * Classe CoutsSimul
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class CoutsSimul
{
    /** @var float coutHtTo */
    private $coutHtTo;

    /** @var float coutHtMo */
    private $coutHtMo;

    /** @var float coutHtFo */
    private $coutHtFo;

    /** @var float coutTtcTo */
    private $coutTtcTo;

    /** @var float coutTtcMo */
    private $coutTtcMo;

    /** @var float coutTtcFo */
    private $coutTtcFo;

    /**
     * Constructeur
     */
    public function __construct()
    {
        $this->coutHtTo  = 0.0;
        $this->coutHtMo  = 0.0;
        $this->coutHtFo  = 0.0;
        $this->coutTtcTo = 0.0;
        $this->coutTtcMo = 0.0;
        $this->coutTtcFo = 0.0;
    }

    //-------------  Accesseur -----------------

    /**
     * @param float $coutHtTo
     */
    public function setCoutHtTo($coutHtTo)
    {
        $this->coutHtTo = $coutHtTo;
    }

    /**
     * @return float
     */
    public function getCoutHtTo()
    {
        return $this->coutHtTo;
    }

    /**
     * @param float $coutHtMo
     */
    public function setCoutHtMo($coutHtMo)
    {
        $this->coutHtMo = $coutHtMo;
    }

    /**
     * @return float
     */
    public function getCoutHtMo()
    {
        return $this->coutHtMo;
    }

    /**
     * @param float $coutHtFo
     */
    public function setCoutHtFo($coutHtFo)
    {
        $this->coutHtFo = $coutHtFo;
    }

    /**
     * @return float
     */
    public function getCoutHtFo()
    {
        return $this->coutHtFo;
    }

    /**
     * @param float $coutTtcTo
     */
    public function setCoutTtcTo($coutTtcTo)
    {
        $this->coutTtcTo = $coutTtcTo;
    }

    /**
     * @return float
     */
    public function getCoutTtcTo()
    {
        return $this->coutTtcTo;
    }

    /**
     * @param float $coutTtcMo
     */
    public function setCoutTtcMo($coutTtcMo)
    {
        $this->coutTtcMo = $coutTtcMo;
    }

    /**
     * @return float
     */
    public function getCoutTtcMo()
    {
        return $this->coutTtcMo;
    }

    /**
     * @param float $coutTtcFo
     */
    public function setCoutTtcFo($coutTtcFo)
    {
        $this->coutTtcFo = $coutTtcFo;
    }

    /**
     * @return float
     */
    public function getCoutTtcFo()
    {
        return $this->coutTtcFo;
    }
}
