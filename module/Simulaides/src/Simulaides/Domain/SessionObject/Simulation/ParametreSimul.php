<?php
namespace Simulaides\Domain\SessionObject\Simulation;

use Simulaides\Constante\ParametreTechConstante;

/**
 * Classe ParametreSimul
 * Classe de stockage des données d'un Parametre technique dans le cadre de l'exécution d'une simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class ParametreSimul
{
    /** @var string  code */
    private $code;

    /** @var string valeur */
    private $valeur;

    /** @var string typeData */
    private $typeData;

    /**
     * Constructeur
     *
     * @param string $code
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    //-------------  Accesseur -----------------
    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $typeData
     */
    public function setTypeData($typeData)
    {
        $this->typeData = $typeData;
    }

    /**
     * @return string
     */
    public function getTypeData()
    {
        return $this->typeData;
    }

    /**
     * @param string $valeur
     */
    public function setValeur($valeur)
    {
        switch ($this->typeData) {
            case ParametreTechConstante::TYPE_DATA_ENTIER:
                $this->valeur = (integer)$valeur;
                break;
            case ParametreTechConstante::TYPE_DATA_BOOLEEN:
                $this->valeur = (bool)$valeur;
                break;
            case ParametreTechConstante::TYPE_DATA_BOOLEEN_RADIO:
                $this->valeur = (bool)$valeur;
                break;
            case ParametreTechConstante::TYPE_DATA_LISTE:
                $this->valeur = $valeur;
                break;
        }
    }

    /**
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }
}
