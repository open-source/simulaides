<?php
namespace Simulaides\Domain\SessionObject\Simulation;

use Simulaides\Logger\LoggerSimulation;
use Simulaides\Service\EvaluateurService;

/**
 * Classe RegleSimul
 * Classe de stockage d'une regle d'un dispositif ou d'une aide travaux dans le cadre de l'exécution d'une simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class RegleSimul
{
    /** @var string type */
    private $type;

    /** @var string condition */
    private $condition;

    /** @var string expression */
    private $expression;

    /**
     * Constructeur du dispositif à partir d'une ligne résultat
     * de la requête de récupération des dispositifs (SimulationService)
     *
     * @param $type
     * @param $condition
     * @param $expression
     */
    public function __construct($type, $condition, $expression)
    {
        $this->type       = $type;
        $this->condition  = $condition;
        $this->expression = $expression;
    }

    /**
     * Calcul d'une règle
     *
     * @param int    $idDispositif
     * @param string $codeTravaux
     *
     * @return boolean|float
     */
    public function calculerRegle($idDispositif, $codeTravaux)
    {
        $evaluateur = EvaluateurService::getEvaluateur();

        // Si la règle est une règle d'éligibilité elle est logique sinon elle est arithmétique
        if ($this->type == "E") {
            $resultat = true;
        } else {
            $resultat = null;
        }
        LoggerSimulation::detail("\n\t\t\t * Regle");

        // si il existe une condition sur la règle
        if (!empty($this->condition)) {

            LoggerSimulation::detail("\n\t\t\t\t[condition = " . $this->condition . " ]");

            // si la condition est remplie c'est que l'expression doit être évaluée
            if ($evaluateur->evaluerFormule($idDispositif, $codeTravaux, $this->condition) === true) {

                LoggerSimulation::detail("\n\t\t\t\t[expression = " . $this->expression . " ]");
                $resultat = $evaluateur->evaluerFormule($idDispositif, $codeTravaux, $this->expression);
            }
        } else {
            LoggerSimulation::detail("\n\t\t\t\t[Pas de condition ]");

            // il n'y a pas de condition alors l'expression doit donc être évaluée
            LoggerSimulation::detail("\n\t\t\t\t[expression = " . $this->expression . " ]");
            $resultat = $evaluateur->evaluerFormule($idDispositif, $codeTravaux, $this->expression);
        }
        return $resultat;
    }

    //-------------  Accesseur -----------------
    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param string $expression
     */
    public function setExpression($expression)
    {
        $this->expression = $expression;
    }

    /**
     * @return string
     */
    public function getExpression()
    {
        return $this->expression;
    }
}
