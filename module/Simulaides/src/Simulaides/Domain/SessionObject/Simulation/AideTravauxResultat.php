<?php
namespace Simulaides\Domain\SessionObject\Simulation;

/**
 * Classe AideTravauxResultat
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class AideTravauxResultat
{
    /** @var  string */
    private $codeTravaux;

    /** @var  String */
    private $intituleTravaux;

    /** @var string eligibiliteSpecifique */
    private $eligibiliteSpecifique;

    /** @var float montantAideTo */
    private $montantAideTo;

    /** @var float $coutTravauxHtTo */
    private $coutTravauxHtTo;

    /** @var string $categorie */
    private $categorie;

    /** @var integer $numOrdre */
    private $numOrdre;

    /**
     * @param string $eligibiliteSpecifique
     */
    public function setEligibiliteSpecifique($eligibiliteSpecifique)
    {
        $this->eligibiliteSpecifique = $eligibiliteSpecifique;
    }

    /**
     * @return string
     */
    public function getEligibiliteSpecifique()
    {
        return $this->eligibiliteSpecifique;
    }

    /**
     * @param String $intituleTravaux
     */
    public function setIntituleTravaux($intituleTravaux)
    {
        $this->intituleTravaux = $intituleTravaux;
    }

    /**
     * @return String
     */
    public function getIntituleTravaux()
    {
        return $this->intituleTravaux;
    }

    /**
     * @param float $montantAideTo
     */
    public function setMontantAideTo($montantAideTo)
    {
        $this->montantAideTo = $montantAideTo;
    }

    /**
     * @return float
     */
    public function getMontantAideTo()
    {
        return $this->montantAideTo;
    }

    /**
     * @param float $coutTravauxHtTo
     */
    public function setCoutTravauxHtTo($coutTravauxHtTo)
    {
        $this->coutTravauxHtTo = $coutTravauxHtTo;
    }

    /**
     * @return float
     */
    public function getCoutTravauxHtTo()
    {
        return $this->coutTravauxHtTo;
    }

    /**
     * @param string $codeTravaux
     */
    public function setCodeTravaux($codeTravaux)
    {
        $this->codeTravaux = $codeTravaux;
    }

    /**
     * @return string
     */
    public function getCodeTravaux()
    {
        return $this->codeTravaux;
    }

    /**
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param string $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return int
     */
    public function getNumOrdre()
    {
        return $this->numOrdre;
    }

    /**
     * @param int $numOrdre
     */
    public function setNumOrdre($numOrdre)
    {
        $this->numOrdre = $numOrdre;
    }


}
