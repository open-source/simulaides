<?php
namespace Simulaides\Domain\SessionObject\Simulation;

use Simulaides\Constante\RegleVariableConstante;
use Simulaides\Constante\TravauxConstante;
use Simulaides\Service\ProjetSimulService;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ProjetSimul
 * Projet de simulation
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class ProjetSimul
{
    /** @var int */
    private $id;

    /** @var  int */
    private $numero;

    /** @var  string */
    private $motPasse;

    /** @var  string */
    private $codeCommune;

    /** @var  string */
    private $zoneClimatique;

    /** @var  string */
    private $codeStatut;

    /** @var  string */
    private $codeTypeLogement;

    /** @var  string */
    private $codeEnergie;

    /** @var  string */
    private $codeModeChauff;

    /** @var  string */
    private $codeEnergieParent;

    /** @var  string */
    private $revenus;

    /** @var  int */
    private $nbAdultes;

    /** @var  int */
    private $nbPersonnesCharge;

    /** @var  int */
    private $nbEnfantsAlterne;

    /** @var  int */
    private $anneeConstruction;

    /** @var  int */
    private $surfaceHabitable;

    /** @var  string */
    private $codeSurface;

    /** @var  boolean */
    private $beneficePtz;

    /** @var  float */
    private $montantBeneficeCi;

    /** @var  boolean */
    private $primoAccession;

    /** @var  boolean */
    private $salarieSecteurPrive;

    /** @var  float */
    private $mtAideConseiller;

    /** @var  boolean */
    private $bbc;

    /** @var array */
    private $travaux = array();

    /** @var array */
    private $tranche = array();

    /**
     * @var float
     */
    private $nbPartsFiscales;

    /** @var float */
    private $coutTotalTravaux;

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /**
     * Constructeur
     *
     * @param int $idProjet
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct($idProjet, ServiceLocatorInterface $serviceLocator)
    {
        $this->id               = $idProjet;
        $this->serviceLocator   = $serviceLocator;
        $this->coutTotalTravaux = 0.0;
    }

    /**
     * Calcul des coûts pour chacun des travaux du projet
     */
    public function calculerCoutsTravaux()
    {
        /** @var TravauxSimul $travaux */
        foreach ($this->travaux as $travaux) {
            $travaux->calculCouts();
            $this->coutTotalTravaux = $this->coutTotalTravaux + $travaux->getCoutTtcTo();
        }
    }

    /**
     * Méthode d'évaluation d'une variable FOYER (remplacement par sa valeur)
     *
     * @param string $variable
     * @param string $index
     *
     * @return string|boolean|float $resultat
     */
    public function evaluerVariableFoyer($variable, $index)
    {
        switch ($variable) {
            case RegleVariableConstante::VARIABLE_LIT_STATUT_FOYER:
                return $this->codeStatut;
                break;
            case RegleVariableConstante::VARIABLE_LIT_NB_ADULTE_FOYER:
                return $this->nbAdultes;
                break;
            case RegleVariableConstante::VARIABLE_LIT_NB_PERS_CHARGE_FOYER:
                return $this->nbPersonnesCharge;
                break;
            case RegleVariableConstante::VARIABLE_LIT_NB_ENF_GARDE_ALT_FOYER:
                return $this->nbEnfantsAlterne;
                break;
            case RegleVariableConstante::VARIABLE_LIT_REVENU_FOYER:
                return $this->revenus;
                break;
            case RegleVariableConstante::VARIABLE_LIT_PTZ_FOYER:
                return $this->beneficePtz;
                break;
            case RegleVariableConstante::VARIABLE_LIT_PRIMO_ACCESSION_FOYER:
                return $this->primoAccession;
                break;
            case RegleVariableConstante::VARIABLE_LIT_SALARIE_SECTEUR_PRIVE_FOYER:
                return $this->salarieSecteurPrive;
                break;
            case RegleVariableConstante::VARIABLE_LIT_MT_CREDIT_IMPOT_FOYER:
                return $this->montantBeneficeCi;
                break;
            case RegleVariableConstante::VARIABLE_LIT_TRANCHE_FOYER:
                $groupeTranche = $index;
                $codeTranche   = $this->getCodeTranchePourGroupe($groupeTranche);
                return $codeTranche;
                break;
            case RegleVariableConstante::VARIABLE_LIT_NB_PARTS_FISC:
                return $this->nbPartsFiscales;
            default:
                echo("variable [" . $variable . "] inconnue !\n");
                break;
        }
    }

    /**
     * Méthode d'évaluation d'une variable LOGEMENT (remplacement par sa valeur)
     *
     * @param string $variable
     *
     * @return string|boolean|int $resultat
     */
    public function evaluerVariableLogement($variable)
    {

        switch ($variable) {

            case RegleVariableConstante::VARIABLE_LIT_TYPE_LOGEMENT:
                return $this->codeTypeLogement;
                break;
            case RegleVariableConstante::VARIABLE_LIT_AGE_LOGEMENT:
                return $this->getAgeLogement();
                break;
            case RegleVariableConstante::VARIABLE_LIT_ANNEE_LOGEMENT:
                return $this->getAnneeConstruction();
                break;
            case RegleVariableConstante::VARIABLE_LIT_SURFACE_LOGEMENT:
                return $this->surfaceHabitable;
                break;
            case RegleVariableConstante::VARIABLE_LIT_BBC_LOGEMENT:
                return $this->bbc;
                break;
            case RegleVariableConstante::VARIABLE_LIT__ENERGIE_CHAUFF:
                return $this->getCodeEnergieParent();
                break;
            case RegleVariableConstante::VARIABLE_LIT__ENERGIE_CHAUFF_DETAIL:
                return $this->getCodeEnergie();
            case RegleVariableConstante::VARIABLE_LIT_MODE_CHAUFFAGE:
                return $this->codeModeChauff;
                break;
            default:
                echo("variable [" . $variable . "] inconnue !\n");
                break;
        }
    }

    /**
     * Méthode d'évaluation d'une variable TRAVAUX (remplacement par sa valeur)
     *
     * @param $variable
     *
     * @return int
     */
    public function evaluerVariableSpecifique($variable)
    {

        switch ($variable) {

            case RegleVariableConstante::VARIABLE_LIT_NB_FENETRE:
                $travauxFenetre = $this->getTravauxByCode(TravauxConstante::TRAVAUX_T01_CHGE_FENETRE);
                if ($travauxFenetre === null) {
                    return 0;
                } else {
                    $nbFenetresAlu  = (int)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_NB_FEN_ALU
                    );
                    $nbFenetresBois = (int)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_NB_FEN_BOIS
                    );
                    $nbFenetresPvc  = (int)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_NB_FEN_PVC
                    );
                    return ($nbFenetresAlu + $nbFenetresBois + $nbFenetresPvc);
                }
                break;

            case RegleVariableConstante::VARIABLE_LIT_NB_PORTE_FENETRE:
                $travauxFenetre = $this->getTravauxByCode(TravauxConstante::TRAVAUX_T01_CHGE_FENETRE);
                if ($travauxFenetre === null) {
                    return 0;
                } else {
                    $nbPortesFenetresAlu  = (int)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_NB_POR_FEN_ALU
                    );
                    $nbPortesFenetresBois = (int)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_NB_POR_FEN_BOIS
                    );
                    $nbPortesFenetresPvc  = (int)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_NB_POR_FEN_PVC
                    );
                    return ($nbPortesFenetresAlu + $nbPortesFenetresBois + $nbPortesFenetresPvc);
                }
                break;

            case RegleVariableConstante::VARIABLE_LIT_SURF_FENETRE:
                $travauxFenetre = $this->getTravauxByCode(TravauxConstante::TRAVAUX_T01_CHGE_FENETRE);
                if ($travauxFenetre === null) {
                    return 0;
                } else {
                    $surfFenetresAlu  = (float)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURF_FEN_ALU
                    );
                    $surfFenetresBois = (float)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURF_FEN_BOIS
                    );
                    $surfFenetresPvc  = (float)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURF_FEN_PVC
                    );
                    return ($surfFenetresAlu + $surfFenetresBois + $surfFenetresPvc);
                }
                break;

            case RegleVariableConstante::VARIABLE_LIT_SURF_PORTE_FENETRE:
                $travauxFenetre = $this->getTravauxByCode(TravauxConstante::TRAVAUX_T01_CHGE_FENETRE);
                if ($travauxFenetre === null) {
                    return 0;
                } else {
                    $surfPortesFenetresAlu  = (float)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURF_POR_FEN_ALU
                    );
                    $surfPortesFenetresBois = (float)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURF_POR_FEN_BOIS
                    );
                    $surfPortesFenetresPvc  = (float)$travauxFenetre->getValeurParamTech(
                        RegleVariableConstante::VAR_PARAM_TECH_SURF_POR_FEN_PVC
                    );
                    return ($surfPortesFenetresAlu + $surfPortesFenetresBois + $surfPortesFenetresPvc);
                }
                break;

            default:
                echo("variable [" . $variable . "] inconnue !\n");
                break;
        }
    }

    /**
     * Calcul l'age du logement
     *
     * @return mixed
     */
    public function getAgeLogement()
    {
        $today = getDate();
        $age   = $today["year"] - $this->anneeConstruction;
        return $age;
    }

    /**
     * Retourne le code de tranche pour un groupe de tranche en fonction des revenus du projet
     *
     * @param string $groupeTranche
     *
     * @return string
     */
    public function getCodeTranchePourGroupe($groupeTranche)
    {
        // Si cette donnée n'est pas déjà présente dans le projet on va la chercher en bdd
        if (isset($this->tranche[$groupeTranche]) === false) {
            $this->getProjetSimulService()->chargerCodeTranchePourGroupe($this, $groupeTranche);
        }
        return $this->tranche[$groupeTranche];
    }

    /**
     * @return ProjetSimulService
     */
    private function getProjetSimulService()
    {
        return $this->serviceLocator->get(ProjetSimulService::SERVICE_NAME);
    }

    /**
     * Affecte le code de tranche pour un groupe de tranche
     *
     * @param string $groupeTranche
     * @param string $codeTranche
     */
    public function setCodeTranchePourGroupe($groupeTranche, $codeTranche)
    {
        $this->tranche[$groupeTranche] = $codeTranche;
    }

    /**
     * Retourne un travaux du projet à partir de son code s'il existe et null sinon
     *
     * @param string $codeTrav
     *
     * @return TravauxSimul $travaux
     */
    public function getTravauxByCode($codeTrav)
    {

        if (isset($this->travaux[$codeTrav]) === true) {
            return $this->travaux[$codeTrav];
        }
        return null;
    }

    /**
     * Ajoute un travaux au projet
     *
     * @param TravauxSimul $travaux
     */
    public function addTravaux($travaux)
    {

        $codeTrav                 = $travaux->getCode();
        $this->travaux[$codeTrav] = $travaux;
    }

    /**
     * Retourne un tableau contenant la liste des codes travaux du projet
     *
     * @return array
     */
    public function getCodesTravaux()
    {
        $tabCodesTrav = array();
        foreach ($this->travaux as $codeTrav => $trav) {
            $tabCodesTrav[] = $codeTrav;
        }
        return $tabCodesTrav;
    }

    //-------------  Accesseur -----------------
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $codeCommune
     */
    public function setCodeCommune($codeCommune)
    {
        $this->codeCommune = $codeCommune;
    }

    /**
     * @return string
     */
    public function getCodeCommune()
    {
        return $this->codeCommune;
    }

    /**
     * @param string $zoneClimatique
     */
    public function setZoneClimatique($zoneClimatique)
    {
        $this->zoneClimatique = $zoneClimatique;
    }

    /**
     * @return string
     */
    public function getZoneClimatique()
    {
        return $this->zoneClimatique;
    }

    /**
     * @param string $codeStatut
     */
    public function setCodeStatut($codeStatut)
    {
        $this->codeStatut = $codeStatut;
    }

    /**
     * @return string
     */
    public function getCodeStatut()
    {
        return $this->codeStatut;
    }

    /**
     * @return float
     */
    public function getNbPartsFiscales()
    {
        return $this->nbPartsFiscales;
    }

    /**
     * @param float $nbPartsFiscales
     */
    public function setNbPartsFiscales($nbPartsFiscales)
    {
        $this->nbPartsFiscales = $nbPartsFiscales;
    }

    /**
     * @param string $codeTypeLogement
     */
    public function setCodeTypeLogement($codeTypeLogement)
    {
        $this->codeTypeLogement = $codeTypeLogement;
    }

    /**
     * @return string
     */
    public function getCodeTypeLogement()
    {
        return $this->codeTypeLogement;
    }

    /**
     * @param string $codeEnergie
     */
    public function setCodeEnergie($codeEnergie)
    {
        $this->codeEnergie = $codeEnergie;
    }

    /**
     * @return string
     */
    public function getCodeEnergie()
    {
        return $this->codeEnergie;
    }

    /**
     * @param string $codeModeChauffage
     */
    public function setModeChauffage($codeModeChauffage)
    {
        $this->codeModeChauff = $codeModeChauffage;
    }

    /**
     * @return string
     */
    public function getModeChauffage()
    {
        return $this->codeModeChauff;
    }

    /**
     * @return string
     */
    public function getCodeEnergieParent()
    {
        return $this->codeEnergieParent;
    }

    /**
     * @param string $codeEnergieParent
     */
    public function setCodeEnergieParent($codeEnergieParent)
    {
        $this->codeEnergieParent = $codeEnergieParent;
    }

    /**
     * @param float $revenus
     */
    public function setRevenus($revenus)
    {
        $this->revenus = (float)$revenus;
    }

    /**
     * @return float
     */
    public function getRevenus()
    {
        return $this->revenus;
    }

    /**
     * @param int $nbAdultes
     */
    public function setNbAdultes($nbAdultes)
    {
        $this->nbAdultes = (int)$nbAdultes;
    }

    /**
     * @return int
     */
    public function getNbAdultes()
    {
        return $this->nbAdultes;
    }

    /**
     * @param int $nbPersonnesCharge
     */
    public function setNbPersonnesCharge($nbPersonnesCharge)
    {
        $this->nbPersonnesCharge = (int)$nbPersonnesCharge;
    }

    /**
     * @return int
     */
    public function getNbPersonnesCharge()
    {
        return $this->nbPersonnesCharge;
    }

    /**
     * @param int $nbEnfantsAlterne
     */
    public function setNbEnfantsAlterne($nbEnfantsAlterne)
    {
        $this->nbEnfantsAlterne = (int)$nbEnfantsAlterne;
    }

    /**
     * @return int
     */
    public function getNbEnfantAlterne()
    {
        return $this->nbEnfantsAlterne;
    }

    /**
     * @param int $anneeConstruction
     */
    public function setAnneeConstruction($anneeConstruction)
    {
        $this->anneeConstruction = (int)$anneeConstruction;
    }

    /**
     * @return int
     */
    public function getAnneeConstruction()
    {
        return $this->anneeConstruction;
    }

    /**
     * @param int $surfaceHabitable
     */
    public function setSurfaceHabitable($surfaceHabitable)
    {
        $this->surfaceHabitable = (int)$surfaceHabitable;

        if ($surfaceHabitable < 35) {
            $this->codeSurface = "S1";
        } else {
            if ($surfaceHabitable >= 35 && $surfaceHabitable < 60) {
                $this->codeSurface = "S2";
            } else {
                if ($surfaceHabitable >= 60 && $surfaceHabitable < 70) {
                    $this->codeSurface = "S3";
                } else {
                    if ($surfaceHabitable >= 70 && $surfaceHabitable < 90) {
                        $this->codeSurface = "S4";
                    } else {
                        if ($surfaceHabitable >= 90 && $surfaceHabitable < 110) {
                            $this->codeSurface = "S5";
                        } else {
                            if ($surfaceHabitable >= 110 && $surfaceHabitable <= 130) {
                                $this->codeSurface = "S6";
                            } else {
                                if ($surfaceHabitable > 130) {
                                    $this->codeSurface = "S7";
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getSurfaceHabitable()
    {
        return $this->surfaceHabitable;
    }

    /**
     * @param string $codeSurface
     */
    public function setCodeSurface($codeSurface)
    {
        $this->codeSurface = (int)$codeSurface;
    }

    /**
     * @return string
     */
    public function getCodeSurface()
    {
        return $this->codeSurface;
    }

    /**
     * @param boolean $beneficePtz
     */
    public function setBeneficePtz($beneficePtz)
    {
        $this->beneficePtz = (bool)$beneficePtz;
    }

    /**
     * @return boolean
     */
    public function getBeneficePtz()
    {
        return $this->beneficePtz;
    }

    /**
     * @param float $montantBeneficeCi
     */
    public function setMontantBeneficeCi($montantBeneficeCi)
    {
        $this->montantBeneficeCi = (float)$montantBeneficeCi;
    }

    /**
     * @return int
     */
    public function getMontantBeneficeCi()
    {
        return $this->montantBeneficeCi;
    }

    /**
     * @param boolean $primoAccession
     */
    public function setPrimoAccession($primoAccession)
    {
        $this->primoAccession = (bool)$primoAccession;
    }

    /**
     * @return boolean
     */
    public function getPrimoAccession()
    {
        return $this->primoAccession;
    }

    /**
     * @param boolean $salarieSecteurPrive
     */
    public function setSalarieSecteurPrive($salarieSecteurPrive)
    {
        $this->salarieSecteurPrive = (bool)$salarieSecteurPrive;
    }

    /**
     * @return boolean
     */
    public function getSalarieSecteurPrive()
    {
        return $this->salarieSecteurPrive;
    }

    /**
     * @param boolean $bbc
     */
    public function setBbc($bbc)
    {
        $this->bbc = (bool)$bbc;
    }

    /**
     * @return int
     */
    public function getBbc()
    {
        return $this->bbc;
    }

    /**
     * @param array $travaux
     */
    public function setTravaux($travaux)
    {
        $this->travaux = $travaux;
    }

    /**
     * @return array
     */
    public function getTravaux()
    {
        return $this->travaux;
    }

    /**
     * @param float $coutTotalTravaux
     */
    public function setCoutTotalTravaux($coutTotalTravaux)
    {
        $this->coutTotalTravaux = $coutTotalTravaux;
    }

    /**
     * @return float
     */
    public function getCoutTotalTravaux()
    {
        return $this->coutTotalTravaux;
    }

    /**
     * @param int $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param string $motPasse
     */
    public function setMotPasse($motPasse)
    {
        $this->motPasse = $motPasse;
    }

    /**
     * @return string
     */
    public function getMotPasse()
    {
        return $this->motPasse;
    }

    /**
     * @param float $mtAideConseiller
     */
    public function setMtAideConseiller($mtAideConseiller)
    {
        $this->mtAideConseiller = $mtAideConseiller;
    }

    /**
     * @return float
     */
    public function getMtAideConseiller()
    {
        return $this->mtAideConseiller;
    }


    /**
     * @return array
     */
    public function getTranche($groupeTranche)
    {
        return isset($this->tranche[$groupeTranche]) ? $this->tranche[$groupeTranche] : null;
    }
}
