<?php
namespace Simulaides\Domain\SessionObject\Simulation;

use DateTime;
use Simulaides\Domain\Entity\Oblige;
use Simulaides\Domain\Filter\Date;

/**
 * Classe DispositifResultat
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject\Simulation
 */
class DispositifResultat
{
    /** @var int id */
    private $id;

    /** @var string intitule */
    private $intitule;

    /** @var string descriptif */
    private $descriptif;

    /** @var string financeur */
    private $financeur;

    /** @var float montant */
    private $montant;

    /** @var boolean evaluerRegle */
    private $evaluerRegle;

    /** @var boolean eligibilite */
    private $eligibilite;

    /** @var Oblige oblige */
    private $oblige;

    /** @var DateTime */
    private $debutValidite;

    /** @var array aidesTravaux */
    private $aidesTravaux = array();

    /** @var string image du dispositif*/
    private $image;

    /** @var string credit image du dispositif*/
    private $credit;

    /** @var string */
    private $siteWeb;

    /** @var string */
    private $type;

    /**
     * @param array $aidesTravaux
     */
    public function setAidesTravaux($aidesTravaux)
    {
        $this->aidesTravaux = $aidesTravaux;
    }

    /**
     * @return array
     */
    public function getAidesTravaux()
    {
        return $this->aidesTravaux;
    }

    /**
     * @param string $descriptif
     */
    public function setDescriptif($descriptif)
    {
        $this->descriptif = $descriptif;
    }

    /**
     * @return string
     */
    public function getDescriptif()
    {
        return $this->descriptif;
    }

    /**
     * @param string $financeur
     */
    public function setFinanceur($financeur)
    {
        $this->financeur = $financeur;
    }

    /**
     * @return string
     */
    public function getFinanceur()
    {
        return $this->financeur;
    }

    /**
     * @param string $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param float $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }

    /**
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param boolean $evaluerRegle
     */
    public function setEvaluerRegle($evaluerRegle)
    {
        $this->evaluerRegle = $evaluerRegle;
    }

    /**
     * @return boolean
     */
    public function getEvaluerRegle()
    {
        return $this->evaluerRegle;
    }

    /**
     * @param boolean $eligibilite
     */
    public function setEligibilite($eligibilite)
    {
        $this->eligibilite = $eligibilite;
    }

    /**
     * @return boolean
     */
    public function getEligibilite()
    {
        return $this->eligibilite;
    }

    /**
     * @return Oblige
     */
    public function getOblige()
    {
        return $this->oblige;
    }

    /**
     * @param Oblige $oblige
     */
    public function setOblige($oblige)
    {
        $this->oblige = $oblige;
    }

    /**
     * @return DateTime
     */
    public function getDebutValidite()
    {
        return $this->debutValidite;
    }

    /**
     * @param DateTime $debutValidite
     */
    public function setDebutValidite($debutValidite)
    {
        $this->debutValidite = $debutValidite;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
    * @param string $image
    */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    /**
     * @param string $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    }

    /**
     * @return string
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * @param string $siteWeb
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $siteWeb
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}
