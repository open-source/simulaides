<?php
namespace Simulaides\Domain\SessionObject;

/**
 * Classe ProjetSession
 * Cette classe permet de gérer un projet en session
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject
 */
class ProjetSession
{
    /**
     * Identifiant du projet
     *
     * @var int
     */
    private $id;

    /**
     * Date d'évaluation de la simulation
     *
     * @var \DateTime
     */
    private $dateEval;

    /**
     * Revenu du foyer
     *
     * @var int
     */
    private $revenus;

    /**
     * Nombre d'adulte dans le foyer
     *
     * @var int
     */
    private $nbAdultes;

    /**
     * Code de la commune
     *
     * @var string
     */
    private $codeCommune;

    /**
     * Code postal de la commune
     *
     * @var string
     */
    private $codePostal;

    /**
     * Code du statut
     *
     * @var string
     */
    private $codeStatut;

    /**
     * Nombre de parts fiscales
     *
     * @var string
     */
    private $nbPartsFiscales;

    /**
     * Code du type de logement
     *
     * @var string
     */
    private $codeTypeLogement;

    /**
     * Code de l'énergie
     *
     * @var string
     */
    private $codeEnergie;

    /**
     * Nombre de personne à charge
     * @var integer
     */
    private $nbPersonnesCharge;

    /**
     * Nombre d'enfant en garde alternée
     *
     * @var integer
     */
    private $nbEnfantAlterne;

    /**
     * Année de construction du logement
     *
     * @var integer
     */
    private $anneeConstruction;

    /**
     * Numéro du projet
     *
     * @var int
     */
    private $numero;

    /**
     * Mot de passe du projet
     *
     * @var string
     */
    private $motPasse;

    /**
     * Surface Habitable
     *
     * @var int
     */
    private $surfaceHabitable;

    /**
     * Projet BBC
     *
     * @var boolean
     */
    private $bbc;

    /**
     * Bénéfice d'un prêt à taux 0
     *
     * @var boolean
     */
    private $beneficePtz;

    /**
     * Primo-accédant
     *
     * @var boolean
     */
    private $primoAccession;

    /**
     * Salarié secteur privé
     *
     * @var  boolean
     */
    private $salarieSecteurPrive;

    /**
     * Montant des travaux ayant bénéficié d'un crédit d'impôt
     *
     * @var int
     */
    private $montantBeneficeCi;

    /**
     * Intitulé de l'aide conseiller
     *
     * @var string
     */
    private $intituleAideConseiller;

    /**
     * Descriptif de l'aide conseiller
     *
     * @var string
     */
    private $descriptifAideConseiller;

    /**
     * Montant de l'aide conseiller
     *
     * @var float
     */
    private $mtAideConseiller;

    /**
     * Autre commentaire de l'aide conseiller
     *
     * @var string
     */
    private $autreCommentaireConseiller;

    /**
     * Nom du conseiller PRIS ayant réalisé la simulation
     *
     * @var string
     */
    private $prisNom;

    /**
     * Email du conseiller PRIS ayant réalisé la simulation
     *
     * @var string
     */
    private $prisEmail;

    /**
     * Téléphone du conseiller PRIS ayant réalisé la simulation
     *
     * @var string
     */
    private $prisTel;
    /**
     * Code du mode chauffage
     *
     * @var string
     */
    private $codeModeChauff;

    /**
     * @param string $codeModeChauff
     */
    public function setCodeModeChauff($codeModeChauffage)
    {
        $this->codeModeChauff = $codeModeChauffage;
    }
    /**
     * @return mixed
     */
    public function getCodeModeChauff()
    {
        return $this->codeModeChauff;
    }
    /**
     * @param mixed $anneeConstruction
     */
    public function setAnneeConstruction($anneeConstruction)
    {
        $this->anneeConstruction = $anneeConstruction;
    }

    /**
     * @return mixed
     */
    public function getAnneeConstruction()
    {
        return $this->anneeConstruction;
    }

    /**
     * @param mixed $bbc
     */
    public function setBbc($bbc)
    {
        $this->bbc = $bbc;
    }

    /**
     * @return mixed
     */
    public function getBbc()
    {
        return $this->bbc;
    }

    /**
     * @param mixed $beneficePtz
     */
    public function setBeneficePtz($beneficePtz)
    {
        $this->beneficePtz = $beneficePtz;
    }

    /**
     * @return mixed
     */
    public function getBeneficePtz()
    {
        return $this->beneficePtz;
    }

    /**
     * @param mixed $primoAccession
     */
    public function setPrimoAccession($primoAccession)
    {
        $this->primoAccession = $primoAccession;
    }

    /**
     * @return mixed
     */
    public function getPrimoAccession()
    {
        return $this->primoAccession;
    }

    /**
     * @return bool
     */
    public function getSalarieSecteurPrive()
    {
        return $this->salarieSecteurPrive;
    }

    /**
     * @param mixed $salarieSecteurPrive
     */
    public function setSalarieSecteurPrive($salarieSecteurPrive)
    {
        $this->salarieSecteurPrive = $salarieSecteurPrive;
    }



    /**
     * @param mixed $codeCommune
     */
    public function setCodeCommune($codeCommune)
    {
        $this->codeCommune = $codeCommune;
    }

    /**
     * @return mixed
     */
    public function getCodeCommune()
    {
        return $this->codeCommune;
    }

    /**
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * @param string $codePostal
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;
    }

    /**
     * @param mixed $codeEnergie
     */
    public function setCodeEnergie($codeEnergie)
    {
        $this->codeEnergie = $codeEnergie;
    }

    /**
     * @return mixed
     */
    public function getCodeEnergie()
    {
        return $this->codeEnergie;
    }

    /**
     * @param mixed $codeStatut
     */
    public function setCodeStatut($codeStatut)
    {
        $this->codeStatut = $codeStatut;
    }

    /**
     * @return mixed
     */
    public function getCodeStatut()
    {
        return $this->codeStatut;
    }

    /**
     * @return string
     */
    public function getNbPartsFiscales()
    {
        return $this->nbPartsFiscales;
    }

    /**
     * @param string $nbPartsFiscales
     */
    public function setNbPartsFiscales($nbPartsFiscales)
    {
        $this->nbPartsFiscales = $nbPartsFiscales;
    }

    /**
     * @param mixed $codeTypeLogement
     */
    public function setCodeTypeLogement($codeTypeLogement)
    {
        $this->codeTypeLogement = $codeTypeLogement;
    }

    /**
     * @return mixed
     */
    public function getCodeTypeLogement()
    {
        return $this->codeTypeLogement;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $montantBeneficeCi
     */
    public function setMontantBeneficeCi($montantBeneficeCi)
    {
        $this->montantBeneficeCi = $montantBeneficeCi;
    }

    /**
     * @return mixed
     */
    public function getMontantBeneficeCi()
    {
        return $this->montantBeneficeCi;
    }

    /**
     * @param mixed $motPasse
     */
    public function setMotPasse($motPasse)
    {
        $this->motPasse = $motPasse;
    }

    /**
     * @return mixed
     */
    public function getMotPasse()
    {
        return $this->motPasse;
    }

    /**
     * @param mixed $nbAdultes
     */
    public function setNbAdultes($nbAdultes)
    {
        $this->nbAdultes = $nbAdultes;
    }

    /**
     * @return mixed
     */
    public function getNbAdultes()
    {
        return $this->nbAdultes;
    }

    /**
     * @param mixed $nbEnfantAlterne
     */
    public function setNbEnfantAlterne($nbEnfantAlterne)
    {
        $this->nbEnfantAlterne = $nbEnfantAlterne;
    }

    /**
     * @return mixed
     */
    public function getNbEnfantAlterne()
    {
        return $this->nbEnfantAlterne;
    }

    /**
     * @param mixed $nbPersonnesCharge
     */
    public function setNbPersonnesCharge($nbPersonnesCharge)
    {
        $this->nbPersonnesCharge = $nbPersonnesCharge;
    }

    /**
     * @return mixed
     */
    public function getNbPersonnesCharge()
    {
        return $this->nbPersonnesCharge;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $revenus
     */
    public function setRevenus($revenus)
    {
        $this->revenus = $revenus;
    }

    /**
     * @return mixed
     */
    public function getRevenus()
    {
        return $this->revenus;
    }

    /**
     * @param mixed $surfaceHabitable
     */
    public function setSurfaceHabitable($surfaceHabitable)
    {
        $this->surfaceHabitable = $surfaceHabitable;
    }

    /**
     * @return mixed
     */
    public function getSurfaceHabitable()
    {
        return $this->surfaceHabitable;
    }

    /**
     * @param mixed $autreCommentaireConseiller
     */
    public function setAutreCommentaireConseiller($autreCommentaireConseiller)
    {
        $this->autreCommentaireConseiller = $autreCommentaireConseiller;
    }

    /**
     * @return mixed
     */
    public function getAutreCommentaireConseiller()
    {
        return $this->autreCommentaireConseiller;
    }

    /**
     * @param mixed $dateEval
     */
    public function setDateEval($dateEval)
    {
        $this->dateEval = $dateEval;
    }

    /**
     * @return mixed
     */
    public function getDateEval()
    {
        return $this->dateEval;
    }

    /**
     * @param mixed $descriptifAideConseiller
     */
    public function setDescriptifAideConseiller($descriptifAideConseiller)
    {
        $this->descriptifAideConseiller = $descriptifAideConseiller;
    }

    /**
     * @return mixed
     */
    public function getDescriptifAideConseiller()
    {
        return $this->descriptifAideConseiller;
    }

    /**
     * @param mixed $intituleAideConseiller
     */
    public function setIntituleAideConseiller($intituleAideConseiller)
    {
        $this->intituleAideConseiller = $intituleAideConseiller;
    }

    /**
     * @return mixed
     */
    public function getIntituleAideConseiller()
    {
        return $this->intituleAideConseiller;
    }

    /**
     * @param mixed $mtAideConseiller
     */
    public function setMtAideConseiller($mtAideConseiller)
    {
        $this->mtAideConseiller = $mtAideConseiller;
    }

    /**
     * @return mixed
     */
    public function getMtAideConseiller()
    {
        return $this->mtAideConseiller;
    }

    /**
     * @param mixed $prisEmail
     */
    public function setPrisEmail($prisEmail)
    {
        $this->prisEmail = $prisEmail;
    }

    /**
     * @return mixed
     */
    public function getPrisEmail()
    {
        return $this->prisEmail;
    }

    /**
     * @param mixed $prisNom
     */
    public function setPrisNom($prisNom)
    {
        $this->prisNom = $prisNom;
    }

    /**
     * @return mixed
     */
    public function getPrisNom()
    {
        return $this->prisNom;
    }

    /**
     * @param mixed $prisTel
     */
    public function setPrisTel($prisTel)
    {
        $this->prisTel = $prisTel;
    }

    /**
     * @return mixed
     */
    public function getPrisTel()
    {
        return $this->prisTel;
    }
}
