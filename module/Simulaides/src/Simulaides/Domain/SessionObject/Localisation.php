<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 18/02/15
 * Time: 10:54
 */

namespace Simulaides\Domain\SessionObject;


/**
 * Class Localisation
 * @package Simulaides\Domain\SessionObject
 */
class Localisation
{
    /**
     * @var string
     */
    private $codeRegion;

    /**
     * @var string
     */
    private $codeDepartement;

    /**
     * @var string
     */
    private $codeInsee;

    /**
     * @var string
     */
    private $codePostal;

    /**
     * @param string $codeDepartement
     */
    public function setCodeDepartement($codeDepartement)
    {
        $this->codeDepartement = $codeDepartement;
    }

    /**
     * @return string
     */
    public function getCodeDepartement()
    {
        return $this->codeDepartement;
    }

    /**
     * @param string $codeInsee
     */
    public function setCodeInsee($codeInsee)
    {
        $this->codeInsee = $codeInsee;
    }

    /**
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * @param string $codePostal
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;
    }

    /**
     * @return string
     */
    public function getCodeInsee()
    {
        return $this->codeInsee;
    }

    /**
     * @param string $codeRegion
     */
    public function setCodeRegion($codeRegion)
    {
        $this->codeRegion = $codeRegion;
    }

    /**
     * @return string
     */
    public function getCodeRegion()
    {
        return $this->codeRegion;
    }
}
