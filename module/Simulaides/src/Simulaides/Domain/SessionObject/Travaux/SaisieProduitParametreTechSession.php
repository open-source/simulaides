<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 27/02/15
 * Time: 16:56
 */

namespace Simulaides\Domain\SessionObject\Travaux;


/**
 * Class SaisieProduitParametreTechSession
 * @package Simulaides\Domain\SessionObject\TravauxSession
 */
class SaisieProduitParametreTechSession extends SaisieProduitSession
{
    /**
     * @var string $codeParametre
     */
    private $codeParametre;

    /**
     * @var string $valeur
     */
    private $valeur;

    /**
     * @var string $paramType
     */
    private $paramType;

    /**
     * @var  string $paramLibelle
     */
    private $paramLibelle;

    /**
     * @var  string $paramCodeListe
     */
    private $paramCodeListe;


    /**
     * @param string $codeParametre
     */
    public function setCodeParametre($codeParametre)
    {
        $this->codeParametre = $codeParametre;
    }

    /**
     * @return string
     */
    public function getCodeParametre()
    {
        return $this->codeParametre;
    }

    /**
     * @param string $valeur
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    }

    /**
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * @param string $paramType
     */
    public function setParamType($paramType)
    {
        $this->paramType = $paramType;
    }

    /**
     * @param string $paramCodeListe
     */
    public function setParamCodeListe($paramCodeListe)
    {
        $this->paramCodeListe = $paramCodeListe;
    }

    /**
     * @return string
     */
    public function getParamCodeListe()
    {
        return $this->paramCodeListe;
    }

    /**
     * @return string
     */
    public function getParamType()
    {
        return $this->paramType;
    }

    /**
     * @param string $paramLibelle
     */
    public function setParamLibelle($paramLibelle)
    {
        $this->paramLibelle = $paramLibelle;
    }

    /**
     * @return string
     */
    public function getParamLibelle()
    {
        return $this->paramLibelle;
    }
}
