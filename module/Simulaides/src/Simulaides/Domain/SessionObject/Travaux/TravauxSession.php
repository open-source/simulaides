<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 27/02/15
 * Time: 16:40
 */

namespace Simulaides\Domain\SessionObject\Travaux;

use Zend\Stdlib\ArrayObject;

/**
 * Class TravauxSession
 * @package Simulaides\Domain\SessionObject\TravauxSession
 */
class TravauxSession
{
    /**
     * Code du travaux
     * @var string $codeTravaux
     */
    private $codeTravaux;

    /**
     * Libellé du travaux
     * @var string $libelle
     */
    private $libelle;

    /**
     * Liste de ProduitSession
     * @var ArrayObject $produits
     */
    private $produits;

    /**
     * @var string $choixOffre
     */
    private $choixOffre;

    /**
     *
     */
    public function __construct()
    {
        $this->produits = new ArrayObject();
    }

    /**
     * @param string $codeTravaux
     */
    public function setCodeTravaux($codeTravaux)
    {
        $this->codeTravaux = $codeTravaux;
    }

    /**
     * @return string
     */
    public function getCodeTravaux()
    {
        return $this->codeTravaux;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param ArrayObject $produits
     */
    public function setProduits(ArrayObject $produits)
    {
        $this->produits = $produits;
    }

    /**
     * @return \Zend\Stdlib\ArrayObject
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * @param ProduitSession $produit
     */
    public function addProduit(ProduitSession $produit)
    {
        $this->produits->append($produit);
    }

    /**
     * Suppresion de tous les produits du travaux
     */
    public function removeAllProduits()
    {
        $this->produits = new ArrayObject();
    }

    /**
     * @return string
     */
    public function getChoixOffre()
    {
        return $this->choixOffre;
    }

    /**
     * @param string $choixOffre
     */
    public function setChoixOffre($choixOffre)
    {
        $this->choixOffre = $choixOffre;
    }

}
