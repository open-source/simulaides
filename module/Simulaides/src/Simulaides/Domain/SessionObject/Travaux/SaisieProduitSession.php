<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 27/02/15
 * Time: 16:52
 */

namespace Simulaides\Domain\SessionObject\Travaux;


/**
 * Class SaisieProduitSession
 * @package Simulaides\Domain\SessionObject\TravauxSession
 */
abstract class SaisieProduitSession
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $codeProduit;
    /**
     * @var int
     */
    private $idProjet;

    /**
     * @param string $codeProduit
     */
    public function setCodeProduit($codeProduit)
    {
        $this->codeProduit = $codeProduit;
    }

    /**
     * @return string
     */
    public function getCodeProduit()
    {
        return $this->codeProduit;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $idProjet
     */
    public function setIdProjet($idProjet)
    {
        $this->idProjet = $idProjet;
    }

    /**
     * @return int
     */
    public function getIdProjet()
    {
        return $this->idProjet;
    }
}
