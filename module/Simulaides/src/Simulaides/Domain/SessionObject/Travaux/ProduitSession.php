<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 27/02/15
 * Time: 16:50
 */

namespace Simulaides\Domain\SessionObject\Travaux;

use Zend\Stdlib\ArrayObject;

/**
 * Class ProduitSession
 * @package Simulaides\Domain\SessionObject\TravauxSession
 */
class ProduitSession
{
    /**
     * Code du produit
     * @var string $codeProduit
     */
    private $codeProduit;

    /**
     * Libellé du produit
     * @var string $codeLibelle
     */
    private $libelle;

    /**
     * Liste de SaisieProduitSession
     * @var ArrayObject $saisieProduit
     */
    private $saisiesProduit;

    /**
     * Texte d'aide à la saisie des coûts
     * @var String $infosSaisieCout
     */
    private $infosSaisieCout;

    /**
     * Texte d'aide à la saisie des coûts
     * @var String $infosSaisieCout
     */
    private $infosSaisieCoutOu;

    /**
     *
     */
    public function __construct()
    {
        $this->saisiesProduit = new ArrayObject();
    }

    /**
     * @param string $codeProduit
     */
    public function setCodeProduit($codeProduit)
    {
        $this->codeProduit = $codeProduit;
    }

    /**
     * @return string
     */
    public function getCodeProduit()
    {
        return $this->codeProduit;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param \Zend\Stdlib\ArrayObject $saisiesProduit
     */
    public function setSaisiesProduit(ArrayObject $saisiesProduit)
    {
        $this->saisiesProduit = $saisiesProduit;
    }

    /**
     * @return \Zend\Stdlib\ArrayObject
     */
    public function getSaisiesProduit()
    {
        return $this->saisiesProduit;
    }

    /**
     * @param SaisieProduitSession $saisieProduit
     */
    public function addSaisiesProduit(SaisieProduitSession $saisieProduit)
    {
        $this->saisiesProduit->append($saisieProduit);
    }

    /**
     * @return String
     */
    public function getInfosSaisieCout()
    {
        return $this->infosSaisieCout;
    }

    /**
     * @param String $infosSaisieCout
     */
    public function setInfosSaisieCout($infosSaisieCout)
    {
        $this->infosSaisieCout = $infosSaisieCout;
    }

    /**
     * @return String
     */
    public function getInfosSaisieCoutOu()
    {
        return $this->infosSaisieCoutOu;
    }

    /**
     * @param String $infosSaisieCoutOu
     */
    public function setInfosSaisieCoutOu($infosSaisieCoutOu)
    {
        $this->infosSaisieCoutOu = $infosSaisieCoutOu;
    }

}
