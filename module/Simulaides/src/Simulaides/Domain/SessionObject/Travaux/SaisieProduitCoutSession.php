<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 27/02/15
 * Time: 16:54
 */

namespace Simulaides\Domain\SessionObject\Travaux;

use Simulaides\Constante\SessionConstante;

/**
 * Class SaisieProduitCoutSession
 * Classe des couts saisis pour un produit
 * @package Simulaides\Domain\SessionObject\TravauxSession
 */
class SaisieProduitCoutSession extends SaisieProduitSession
{
    /**
     * Type du cout
     * @var string $typeCout
     */
    private $typeCout;

    /**
     * @var float $cout
     */
    private $cout;

    /**
     * @var float $coutCalculé
     */
    private $coutCalcule;
    /**
     * @var float $coutUser
     */
    private $coutUser;

    /**
     * @param float $cout
     */
    public function setCout($cout)
    {
        $this->cout = $cout;
    }

    /**
     * @return float
     */
    public function getCout()
    {
        return $this->cout;
    }

    /**
     * @param string $typeCout
     */
    public function setTypeCout($typeCout)
    {
        $this->typeCout = $typeCout;
    }

    /**
     * @return string
     */
    public function getTypeCout()
    {
        return $this->typeCout;
    }

    /**
     * @param float $coutCalcule
     */
    public function setCoutCalcule($coutCalcule)
    {
        $this->coutCalcule = $coutCalcule;
    }

    /**
     * @return float
     */
    public function getCoutCalcule()
    {
        return $this->coutCalcule;
    }

    /**
     * @return float
     */
    public function getCoutUser()
    {
        return $this->coutUser;
    }

    /**
     * @param float $coutUser
     */
    public function setCoutUser($coutUser)
    {
        $this->coutUser = $coutUser;
    }

    /**
     * Permet de retourner le libellé du type de montant HT
     *
     * @return string
     */
    public function getLabelTypeMontantHT()
    {
        $labelTypeMt = '';
        switch ($this->typeCout){
            case SessionConstante::COUT_TOTAL:
                $labelTypeMt = 'Coût total HT';
                break;
            case SessionConstante::COUT_MO:
                $labelTypeMt = "Coût de main d'oeuvre HT";
                break;
            case SessionConstante::COUT_FO:
                $labelTypeMt = 'Coût de fournitures HT';
                break;
        }

        return $labelTypeMt;
    }

    /**
     * Permet de retourner le libellé du type de montant HT estimé
     *
     * @return string
     */
    public function getLabelTypeMontantHTEstime()
    {
        $labelTypeMt = '';
        switch ($this->typeCout){
            case SessionConstante::COUT_TOTAL:
                $labelTypeMt = 'Coût total HT estimé';
                break;
            case SessionConstante::COUT_MO:
                $labelTypeMt = "Coût de main d'oeuvre HT estimé";
                break;
            case SessionConstante::COUT_FO:
                $labelTypeMt = 'Coût de fournitures HT estimé';
                break;
        }

        return $labelTypeMt;
    }
}
