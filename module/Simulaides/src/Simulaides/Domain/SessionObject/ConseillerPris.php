<?php
namespace Simulaides\Domain\SessionObject;

use Simulaides\Constante\PrisConstante;

/**
 * Classe ConseillerPris
 * Objet représentant un user PRIS
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Domain\SessionObject
 */
class ConseillerPris
{
    /**
     * Identifiant du user dans la base
     *
     * @var integer
     */
    private $identifiant;

    /**
     * Email du user dans la base
     *
     * @var string
     */
    private $mail;

    /**
     * Nom du conseiller
     *
     * @var string
     */
    private $nom;

    /**
     * Prénom du conseiller
     *
     * @var string
     */
    private $prenom;

    /**
     * Code de la région
     *
     * @var string|null
     */
    private $codeRegion;

    /**
     * Numéro de téléphone du conseiller
     *
     * @var string
     */
    private $tel;

    /**
     * Liste des rôles du conseiller
     *
     * @var array
     */
    private $listRole = [];

    /**
     * Structure du conseiller
     *
     * @var string
     */
    private $structure;

    /**
     * @param int $identifiant
     */
    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;
    }

    /**
     * @return int
     */
    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    /**
     * @param null|string $codeRegion
     */
    public function setCodeRegion($codeRegion)
    {
        $this->codeRegion = $codeRegion;
    }

    /**
     * @return null|string
     */
    public function getCodeRegion()
    {
        return $this->codeRegion;
    }

    /**
     * @param array $listRole
     */
    public function setListRole($listRole)
    {
        $this->listRole = $listRole;
    }

    /**
     * @return array
     */
    public function getListRole()
    {
        return $this->listRole;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Permet de savoir si le conseiller est PRIS
     *
     * @return bool
     */
    public function estConseillerPRIS()
    {
        return in_array(PrisConstante::ROLE_PRIS, $this->getListRole());
    }

    /**
     * @param string $structure
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;
    }

    /**
     * @return string
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * Permet de savoir si le conseiller est ADEME
     *
     * @return bool
     */
    public function estConseillerADEME()
    {
        $estAdemeDr = false;
        $estAdeme   = in_array(PrisConstante::ROLE_ADEME, $this->getListRole());
        if (!$estAdeme) {
            $estAdemeDr = in_array(PrisConstante::ROLE_ADEME_DR, $this->getListRole());
        }

        return ($estAdeme || $estAdemeDr);
    }

    public function aRoleADEME(){
        return in_array(PrisConstante::ROLE_ADEME, $this->getListRole());
    }

    public function aRoleDrADEME(){
        return in_array(PrisConstante::ROLE_ADEME_DR, $this->getListRole());
    }
}
