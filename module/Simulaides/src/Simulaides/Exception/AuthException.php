<?php
namespace Simulaides\Exception;

/**
 * Classe AuthException
 *
 * Projet : Simul'Aides

 *
 * @author
 * @package   ${NAMESPACE}
 */
class AuthException extends \Exception
{
    /**
     * AuthException constructor.
     */
    public function __construct()
    {
        parent::__construct("Votre login et/ou votre mot de passe sont inconnus. Merci de vous rapprocher de votre Direction Régionale de l'ADEME");
    }
}
