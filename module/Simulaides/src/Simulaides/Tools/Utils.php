<?php

namespace Simulaides\Tools;

use Simulaides\Constante\ListeOuiNonConstante;
use Simulaides\Constante\MainConstante;
use Zend\Math\Rand;

/**
 * Classe Utils
 * Fonctions utilitaires
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Tools
 */
class Utils
{

    /**
     * Permet de formater une date en chaine
     *
     * @param null|\DateTime $date
     * @param null           $format
     *
     * @return mixed
     */
    public static function formatDateToString($date, $format = null)
    {
        if (empty($format)) {
            $format = MainConstante::FORMAT_DATE_DEFAUT;
        }
        $dateFormatee = $date;
        if (!empty($date)) {
            $dateFormatee = $date->format($format);
        }

        return $dateFormatee;
    }

    /**
     * Retourne une liste de valeur Oui/Non
     *
     * @return array
     */
    public static function getListeOuiNon()
    {
        $tabValues[] = [
            'label' => ListeOuiNonConstante::getValeur(ListeOuiNonConstante::VALUE_OUI),
            'value' => ListeOuiNonConstante::VALUE_OUI
        ];

        $tabValues[] = [
            'label' => ListeOuiNonConstante::getValeur(ListeOuiNonConstante::VALUE_NON),
            'value' => ListeOuiNonConstante::VALUE_NON
        ];

        return $tabValues;
    }

    /**
     * Retourne un numéric formaté
     *
     * @param float $number    Nombre a formatter
     * @param int   $nbDecimal Nombre de décimal
     *
     * @return string
     */
    public static function formatNumberToString($number, $nbDecimal)
    {
        $numberFormate = $number;

        if ($number !== "") {
            $numberFormate = number_format(
                $number,
                $nbDecimal,
                MainConstante::DECIMAL_SEPARATOR,
                MainConstante::THOUSAND_SEPARATOR
            );
        }

        return $numberFormate;
    }

    /**
     * Permet de retourner une couleur au format RGB
     * à partir d'un code HEXA
     *
     * @param $hex
     *
     * @return array
     */
    public static function hex2rgb($hex)
    {
        $hex = str_replace("#", "", $hex);

        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        $rgb = [$r, $g, $b];

        return $rgb; // returns an array with the rgb values
    }

    /**
     * Permet de savoir si la date du jour est comprise dans la période
     *
     * @param \DateTime $dateDebut Date de début de la période
     * @param \DateTime $dateFin   Date de fin de la période
     *
     * @return bool
     */
    public static function estPeriodeValideNow($dateDebut, $dateFin)
    {
        $estValide = true;
        $dateJour  = new \DateTime('now');

        if (!empty($dateDebut) && !empty($dateFin)) {
            //Les deux dates sont renseignées, la date du jour doit être comprise dans la période
            if ($dateJour < $dateDebut || $dateJour > $dateFin) {
                $estValide = false;
            }
        } elseif (!empty($dateDebut)) {
            //La date de début seule est renseignée
            if ($dateJour < $dateDebut) {
                $estValide = false;
            }
        }

        return $estValide;
    }

    /**
     * Permet de retourner une valeur litteral pour un booléen
     *
     * @param $valeur
     *
     * @return string
     */
    public static function getBooleanLitteral($valeur)
    {
        if ($valeur) {
            return 'oui';
        } else {
            return 'non';
        }
    }

    /**
     * Permet de retourner le nom du serveur
     *
     * @param $conteneur
     *
     * @return mixed
     */
    public static function getServerNameConteneur($conteneur)
    {
        $conteneur  = str_replace(["http://", "https://"], "", $conteneur);
        $tabPart    = explode('/', $conteneur);
        $serverName = $tabPart[0];

        return $serverName;
    }

    /**
     * Permet de récupérer l'url du parent
     *
     * @param $conteneurAppelant
     *
     * @return string
     */
    public static function getConteneurParentFromPageAppelante($conteneurAppelant)
    {
        $isHttps           = (strpos($conteneurAppelant, "https://") > -1);
        $isHttp            = (strpos($conteneurAppelant, "http://") > -1);
        $sHttp             = '';
        $conteneurSansHttp = $conteneurAppelant;
        if ($isHttp) {
            $conteneurSansHttp = str_replace("http://", "", $conteneurAppelant);
            $sHttp             = 'http://';
        } elseif ($isHttps) {
            $conteneurSansHttp = str_replace("https://", "", $conteneurAppelant);
            $sHttp             = 'https://';
        }

        $tabPart = explode('/', $conteneurSansHttp);
        if (isset($tabPart[0])) {
            $conteneurAppelant = $sHttp . $tabPart[0];
        }

        return $conteneurAppelant;
    }

    /**
     * Permet de retourner une valeur numérique pour le booléen
     *
     * @param $valeur
     *
     * @return string
     */
    public static function getBooleanNumeric($valeur)
    {
        if ($valeur) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Génère le mot de passe sur le projet
     */
    public static function genereMotDePasse()
    {
        //construction du mot de passe
        $mdp = Rand::getString(3, 'abcdefghijklmnopqrstuvwxyz');
        $mdp .= Rand::getString(2, '0123456789');

        return $mdp;
    }

}
