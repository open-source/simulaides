<?php
namespace Simulaides\Tools;

use TCPDF;

/**
 * Classe ResultatPDF
 * Classe de génération de PDF
 *
 * Projet : SimulAides 2015-2015
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Tools
 */
class ResultatPDF extends TCPDF
{

    public function Header()
    {
        $image = __DIR__ . '/../../../../../public/images/entete_pdf.png';
        $this->Image($image,14,5,'182', 17);
    }

    /**
     * Génère le pied de page
     * Override
     */
    public function Footer()
    {
        $this->SetY(-15);
        //Logo ADEME
        $image = __DIR__ . '/../../../../../public/images/logo_ademe.jpg';
        $this->Image($image, $this->GetX(), $this->GetY(), 8, 10);
        //Texte ADEME
        $textAdeme = 'L\'outil Simul\'Aides vous est proposé par <a href="http://www.ademe.fr">l\'ADEME</a>';
        $this->SetFont('', '', 6);
        $this->writeHTMLCell(30, 0, $this->GetX() + 12, $this->GetY(), $textAdeme);
        //Phrase d'impression
        $this->SetFont('', 'I', 6);
        $this->SetTextColor(34, 177, 76);
        $textImpression = "Protéger la planète, n'imprimer ce document que si nécessaire.";
        $this->writeHTMLCell(0, 0, $this->GetX() + 50, $this->GetY(), $textImpression, 0, 0, false, true, 'R');
    }
}
