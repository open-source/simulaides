<?php
namespace Simulaides\Form\ReferentielProduit;

use Zend\Form\Form;

/**
 * Classe RefPrixAideCEEForm
 * Formulaire de saisie du coef des aides CEE
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\ReferentielProduit
 */
class RefPrixAideCEEForm extends Form
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //coefficient de conversion
        $this->add(
            [
                'name'       => 'coefficient',
                'options'    => [
                    'label' => 'Coefficient de conversion des kWh cumacs en Euros'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        //Bouton Enregistrer
        $this->add(
            [
                'name'       => 'btnAideCEE',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Enregistrer',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'submitFormCEE(); return false;'
                ]
            ]
        );
    }
}
