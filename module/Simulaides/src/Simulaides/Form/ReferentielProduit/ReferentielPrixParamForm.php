<?php
namespace Simulaides\Form\ReferentielProduit;

use Zend\Form\Form;

/**
 * Classe ReferentielPrixParamForm
 * Formulaire de paramétrage du référentiel des prix
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\ReferentielProduit
 */
class ReferentielPrixParamForm extends Form
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        /**
         * *************** MISE A JOUR DU REFERENTIEL DES PRIX
         */

        //Liste des travaux
        $this->add(
            [
                'name'       => 'travaux',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => 'Travaux ',
                    'empty_option' => 'Tous',
                    'target_class' => 'Simulaides\Domain\Entity\Travaux',
                    'property'     => 'intitule',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListTravauxTrieParIntitule',
                    ],
                ],
                'attributes' => [
                    'class'   => 'form-control referentiel-search',
                    'onchange' => 'changeTravauxRefPrix();'
                ]
            ]
        );
        //Liste des territoires
        $this->add(
            [
                'name'       => 'territoire',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => 'Territoire',
                    'value_options' => [
                        '0' => 'Métropole',
                        '1' => 'Outremer'
                    ],
                    'property'     => 'intitule',
                    'is_method'    => true
                ],
                'attributes' => [
                    'id' => 'territoire',
                    'class'   => 'form-control referentiel-search',
                    'onchange' => 'changeTravauxRefPrix();'
                ]
            ]
        );
    }
}
