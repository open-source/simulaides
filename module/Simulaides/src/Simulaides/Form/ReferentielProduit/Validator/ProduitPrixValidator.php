<?php
namespace Simulaides\Form\ReferentielProduit\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Classe ProduitPrixValidator
 * Validateur pour la fiche de saisie de sprix d'un produit
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Travaux\Validator
 */
class ProduitPrixValidator extends AbstractValidator
{

    /** Somme égale à 0 */
    const SOMME_ZERO = 'SOMME_ZERO';

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::SOMME_ZERO => "Le prix total du produit ne peut être égal à 0.",
    ];

    /**
     * @param mixed $value
     * @param array $context
     *
     * @return bool
     */
    public function isValid($value, $context = [])
    {
        $prixMo = $context['prixUMo'];
        $prixFo = $context['prixUFourniture'];
        if ($prixFo + $prixMo == 0) {
            $this->error(self::SOMME_ZERO);
            return false;
        }
        return true;
    }
}
