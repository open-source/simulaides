<?php
namespace Simulaides\Form\ReferentielProduit;

use Zend\Form\Form;
use Zend\I18n\Validator\Float;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

/**
 * Classe ReferentielProduitForm
 * Formulaire de modification des prix d'un produit
 *
 * Projet : SimulAides 2015-2015
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\ReferentielProduit
 */
class ReferentielProduitForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Code du produit
        $this->add(
            [
                'name' => 'code',
                'type' => 'hidden'
            ]
        );

        //Code du travaux
        $this->add(
            [
                'name' => 'codeTravaux',
                'type' => 'hidden'
            ]
        );

        // Intitulé
        $this->add(
            [
                'name'       => 'intitule',
                'options'    => [
                    'label' => "Intitulé"
                ],
                'attributes' => [
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Prix de la main d'oeuvre
        $this->add(
            [
                'name'       => 'prixUMo',
                'options'    => [
                    'label' => "Main d'oeuvre"
                ],
                'attributes' => [
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Prix des fournitures
        $this->add(
            [
                'name'       => 'prixUFourniture',
                'options'    => [
                    'label' => 'Fourniture'
                ],
                'attributes' => [
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //TVA
        $this->add(
            [
                'name'       => 'tva',
                'options'    => [
                    'label' => 'TVA'
                ],
                'attributes' => [
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Aide à la saisie
        $this->add(
            [
                'name'       => 'infosSaisieCouts',
                'type'       => 'WisiwigElement',
                'attributes' => [
                    'id' => 'infosSaisieCouts'
                ],
                'options'    => [
                    'label' => 'Aide à la saisie'
                ],
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        //Bouton "Enregistrer"
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'    => 'button',
                    'value'   => 'Enregistrer',
                    'class'   => "small-btn simul-btn pull-right",
                    'onclick' => 'submitformRefPrix(); return false;'
                ]
            ]
        );

        //Bouton "Annuler"
        $this->add(
            [
                'name'       => 'close',
                'attributes' => [
                    'type'    => 'button',
                    'value'   => 'Annuler',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'closeModalForm(); return false;'
                ]
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function getInputFilterSpecification()
    {
        return [
            'intitule' => [
                'required'    => true,
                'allow_empty' => false,

                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "L'intitulé ne peut être vide"
                            ],
                            'type'     => 'string',
                        ]
                    ],
                    [
                        'name'                   => 'StringLength',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'max'      => 150,
                            'messages' => [
                                StringLength::TOO_LONG => "L'intitulé ne peut dépasser 150 caractères."
                            ]
                        ]
                    ]
                ],
            ],
            //Prix de la main d'oeuvre
            'prixUMo'  => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Le montant ne peut être vide'
                            ],
                            'type'     => 'string',
                        ]
                    ],
                    [
                        'name'    => 'Float',
                        'options' => [
                            'locale'   => 'en_US',
                            'messages' => [
                                Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Simulaides\Form\ReferentielProduit\Validator\ProduitPrixValidator',
                        'break_chain_on_failure' => true,
                    ]
                ],
                'filters'     => [
                    [
                        'name'    => 'NumberFormat',
                        'options' => [
                            'locale' => 'fr_FR'
                        ]
                    ]
                ]
            ],
            //Prix des fournitures
            'prixUFourniture' => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Le montant ne peut être vide'
                            ],
                            'type'     => 'string',
                        ]
                    ],
                    [
                        'name'    => 'Float',
                        'options' => [
                            'locale'   => 'en_US',
                            'messages' => [
                                Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name'    => 'NumberFormat',
                        'options' => [
                            'locale' => 'fr_FR'
                        ]
                    ]
                ]
            ],
            //TVA
            'tva'             => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'La TVA ne peut être vide'
                            ]
                        ]
                    ],
                    [
                        'name'    => 'Float',
                        'options' => [
                            'locale'   => 'en_US',
                            'messages' => [
                                Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name'    => 'NumberFormat',
                        'options' => [
                            'locale' => 'fr_FR'
                        ]
                    ]
                ]
            ]
        ];
    }
}
