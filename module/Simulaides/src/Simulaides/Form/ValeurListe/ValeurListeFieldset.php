<?php
namespace Simulaides\Form\ValeurListe;

use Simulaides\Domain\Entity\ValeurListe;
use Zend\Form\Fieldset;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * Classe ValeurListeFieldset
 * Fieldset pour afficher une valeur liste
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\ValeurListe
 */
class ValeurListeFieldset extends Fieldset
{
    /**
     * Définition des éléments du fieldset
     */
    public function init()
    {
        parent::init();

        //Définition de l'hydrator sur l'entité Dispositif
        $classObject = new ClassMethods(false);
        $this->setHydrator($classObject);
        $this->setObject(new ValeurListe());

        //Code de la valeur
        $this->add(
            [
                'name'       => 'code',
                'options'    => [
                    'label' => 'Code'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ]
            ]
        );

        //libellé de la valeur
        $this->add(
            [
                'name'       => 'libelle',
                'options'    => [
                    'label' => 'Libellé'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ]
            ]
        );
    }
}
