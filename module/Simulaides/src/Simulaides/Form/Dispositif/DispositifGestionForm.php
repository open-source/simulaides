<?php
namespace Simulaides\Form\Dispositif;

/**
 * Classe DispositifGestionForm
 * Formulaire pour l'onglet "Gestion dispositif" de la fiche dispositif
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class DispositifGestionForm extends DispositifForm
{
    /**
     *
     */
    public function init()
    {
        parent::init();

        //Code de la région d'affichage de la liste
        $this->add(
            [
                'name' => 'codeRegionListe',
                'type' => 'hidden'
            ]
        );
        /**
         * Ajout des éléments du dispositif
         */
        $this->add(
            [
                'name' => 'dispositif',
                'type' => 'DispositifFieldset',
            ]
        );

        /**
         * Ajout des éléments du périmètre géographique
         */
        $this->add(
            [
                'name' => 'perimetregeo',
                'type' => 'PerimetreGeoFieldset',
            ]
        );

        //bouton Enregistrer
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Enregistrer',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'submitFormGestion(); return false;'
                ]
            ]
        );

        //bouton Supprimer
        $this->add(
            [
                'name'       => 'delete',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Supprimer ce dispositif',
                    'class'   => "small-btn simul-btn pull-left disp-btn",
                    'onclick' => 'supprimeDispositif(); return false;'
                ]
            ]
        );

        //bouton dupliquer
        $this->add(
            [
                'name'       => 'duplicate',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Dupliquer ce dispositif',
                    'class'   => "small-btn simul-btn pull-left disp-btn",
                    'onclick' => 'dupliqueDispositif(); return false;'
                ]
            ]
        );
        //bouton dupliquer
        $this->add(
            [
                'name'       => 'testing',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Tester ce dispositif',
                    'class'   => "small-btn simul-btn pull-left disp-btn",
                    'onclick' => 'testerDispositif(); return false;'
                ]
            ]
        );

        //bouton Désactiver
        $this->add(
            [
                'name'       => 'desactive',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Désactiver ce dispositif',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'changeEtatDesactive(); return false;'
                ]
            ]
        );

        //bouton Demander validation
        $this->add(
            [
                'name'       => 'tovalide',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Demander la validation de ce dispositif',
                    'class'   => "small-btn simul-btn pull-left disp-btn",
                    'onclick' => 'changeEtatAValider(); return false;'
                ]
            ]
        );

        //bouton Valider
        $this->add(
            [
                'name'       => 'valide',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Valider ce dispositif',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'changeEtatActive(); return false;'
                ]
            ]
        );
    }
}
