<?php
namespace Simulaides\Form\Dispositif;

use Simulaides\Tools\Utils;

/**
 * Classe DispositifTravauxForm
 * Formulaire pour la fiche travaux d'un dispositif
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class DispositifTravauxForm extends DispositifForm
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Bouton radio de prise en charge du dispositif
        $this->add(
            [
                'name'       => 'prisEnCharge',
                'type'       => 'hidden'

            ]
        );

        //Fieldset du DispositifTravaux
        $this->add(
            [
                'name' => 'dispositifTravaux',
                'type' => 'DispositifTravauxFieldset'
            ]
        );

        //Bouton Enregistrer
        $this->add(
            [
                'name'       => 'submit',
                'type'       => 'submit',
                'attributes' => [
                    'value'   => 'Enregistrer',
                    'onclick' => 'javascript:submitFormTravaux(0); return false;',
                    'class'   => 'small-btn simul-btn pull-right'
                ]
            ]
        );

    }
}
