<?php
namespace Simulaides\Form\Dispositif;


/**
 * Classe DispositifTravauxRegleForm
 * Formulaire de la fiche editeur des règles du travaux du dispositif
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class DispositifTravauxRegleForm extends DispositifForm
{
    /**
     *
     */
    public function init()
    {
        parent::init();

        //Identifiant du travaux dans le dispositif
        $this->add(
            [
                'name' => 'idDispositifTravaux',
                'type' => 'hidden'
            ]
        );

        //Regle du travaux
        $this->add(
            [
                'name' => 'regle',
                'type' => 'EditeurRegleFieldset',
            ]
        );

        //Gestion des focus et éléments sélectionnés
        $this->add(
            [
                'name'       => 'activeElt',
                'type'       => 'hidden',
                'attributes' => [
                    'id' => 'activeElt'
                ]
            ]
        );

        //Nombre pour l'expression
        $this->add(
            [
                'name'       => 'nombreExpr',
                'options'    => [
                    'label' => 'Nombre'
                ],
                'attributes' => [
                    'class'       => 'editor-number form-control',
                    'placeholder' => 'Nombre',
                    'id'          => 'nombreExpr'
                ]
            ]
        );

        //Nombre pour la condition
        $this->add(
            [
                'name'       => 'nombreCond',
                'options'    => [
                    'label' => 'Nombre'
                ],
                'attributes' => [
                    'class'       => 'editor-number form-control',
                    'placeholder' => 'Nombre',
                    'id'          => 'nombreCond'
                ]
            ]
        );

        //Bouton OK sur le nombre pour l'expression
        $this->add(
            [
                'name'       => 'btnOkNombreExpr',
                'type'       => 'submit',
                'attributes' => [
                    'value'   => 'OK',
                    'onclick' => 'addNumberExprInElement();return false;',
                    'class'   => 'small-btn simul-btn pull-right'
                ]
            ]
        );

        //Bouton OK sur le nombre pour la condition
        $this->add(
            [
                'name'       => 'btnOkNombreCond',
                'type'       => 'submit',
                'attributes' => [
                    'value'   => 'OK',
                    'onclick' => 'addNumberCondInElement();return false;',
                    'class'   => 'small-btn simul-btn pull-right'
                ]
            ]
        );
    }
}
