<?php
namespace Simulaides\Form\Dispositif\Validator;

use Simulaides\Constante\MainConstante;
use Simulaides\Form\Dispositif\DispositifFieldset;
use Zend\Validator\AbstractValidator;

/**
 * Classe DateValiditeValidator
 * Validateur sur les dates de validité d'un dispositif
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif\Validator
 */
class DateValiditeValidator extends AbstractValidator
{
    /** Date de fin inférieure ou égale à date début  */
    const DATE_FIN_INF = "dateFinInf";

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::DATE_FIN_INF => "La date de fin de validité doit être supérieure à la date de début.",
    ];

    /**
     * @param mixed $value
     * @param array $context
     *
     * @return bool
     */
    public function isValid($value, $context = [])
    {
        $dateDebut = $this->getDateValue($context, DispositifFieldset::NAME_DATE_DEBUT);
        $dateFin   = $this->getDateValue($context, DispositifFieldset::NAME_DATE_FIN);

        //Vérification que la date de début < date de fin
        if (isset($dateDebut) && isset($dateFin)) {
            if (($dateFin == $dateDebut) || ($dateFin < $dateDebut)) {
                $this->error(self::DATE_FIN_INF);
                return false;
            }
        }

        return true;
    }

    /**
     * Retourne la valeur saisie
     *
     * @param $context
     * @param $key
     *
     * @return null
     */
    private function getDateValue($context, $key)
    {
        $date = null;
        if (isset($context[$key]) && !empty($context[$key])) {
            $date = \DateTime::createFromFormat(MainConstante::FORMAT_DATE_DEFAUT, $context[$key]);
        }
        return $date;
    }
}
