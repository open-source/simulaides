<?php
namespace Simulaides\Form\Dispositif;

use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\Form\Form;

/**
 * Classe DispositifForm
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class DispositifForm extends Form
{
    /**
     * Initialisation du formulaire
     */
    public function init()
    {
        parent::init();

        //Champ caché pour la validation avant changement d'onglet
        $this->add(
            [
                'name'       => 'existeChangement',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => 'false'
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );
    }

    /**
     * Permet de mettre la fonction sur le onChange de chacun des éléments
     * Cette fonction permet de savoir si un élément a été modifié
     * pour le changement d'onglet
     */
    public function setChangeFonctionJs()
    {
        //Parcours des fieldset
        /** @var Fieldset $fieldSet */
        foreach ($this->getFieldsets() as $fieldSet) {
            //Parcours des éléments du fieldset
            foreach ($fieldSet->getElements() as $element) {
                $this->setChangeFunctionToElement($element);
            }
        }

        foreach ($this->getElements() as $element) {
            $this->setChangeFunctionToElement($element);
        }
    }

    /**
     * Affecte la fonction js de changement
     *
     * @param Element $element
     */
    private function setChangeFunctionToElement($element)
    {
        if ($element->getName() !== 'existeChangement') {
            if (!($element instanceof Element\Hidden)) {
                $onChange = $element->getAttribute('onchange');
                if (!empty($onChange)) {
                    $onChange .= 'changeElementDispositif(this);';
                } else {
                    $onChange .= 'javascript:changeElementDispositif(this);';
                }
                $element->setAttribute('onchange', $onChange);
            }
        }
    }
}
