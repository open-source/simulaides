<?php
namespace Simulaides\Form\Dispositif;

use Simulaides\Tools\Utils;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\Date;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

/**
 * Classe DispositifFieldset
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class DispositifFieldset extends Fieldset implements InputFilterProviderInterface
{
    /** Nom du champ date de début */
    const NAME_DATE_DEBUT = 'debutValidite';
    /** Nom du champ date de fin */
    const NAME_DATE_FIN = 'finValidite';

    /**
     * Définition des éléments du fieldset
     */
    public function init()
    {
        parent::init();

        /******************* INFORMATIONS GENERALES *****************************/

        //Id du dispositif
        $this->add(
            [
                'name' => 'id',
                'type' => 'hidden'
            ]
        );

        //Intitulé public
        $this->add(
            [
                'name'       => 'intitule',
                'options'    => [
                    'label' => 'Intitulé public'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Intitulé conseiller
        $this->add(
            [
                'name'       => 'intituleConseiller',
                'options'    => [
                    'label' => 'Intitulé conseiller'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Financeur
        $this->add(
            [
                'name'       => 'financeur',
                'options'    => [
                    'label' => 'Financeur'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        //##Type de dispositif (toujours inaccessible)
        /*$this->add(
            [
                'name'    => 'typeDispositif',
                'type'    => 'ValeurListeFieldset',
                'options' => [
                    'label' => 'Type de dispositif'
                ]
            ]
        );*/

        $this->add(
            [
                'name'       => 'typeDispositif',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Types de dispositif :",
                    'empty_option' => 'Tous les types',
                    'allow_empty' => true,
                    'target_class' => 'Simulaides\Domain\Entity\ValeurListe',
                    'property'     => 'libelle',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListeTypeDispositif',
                    ],
                ],
                'attributes' => [
                    'class'     => 'chosen-select change-select form-control'
                ]
            ]
        );

        $this->add(
            [
                'name' => 'codeTypeDispositif',
                'type' => 'hidden'
            ]
        );

        $this->add(
            [
                'name' => 'dispositifRequis',
                'type' => 'hidden',
                'options'    => [
                    'allow_empty' => true,
                    'required' => false    ]
            ]
        );

        // Descriptif WYSIWYG
        $this->add(
            [
                'name'       => 'descriptif',
                'type'       => 'WisiwigElement',
                'options'    => [
                    'label' => 'Descriptif'
                ],
                'attributes' => [
                    'id'       => 'descriptif',
                    'required' => true,
                    'class'    => 'form-control',
                    'rows'     => '15'
                ]
            ]
        );

        //Type de périmètre
        $this->add(
            [
                'name' => 'typePerimetre',
                'type' => 'hidden'
            ]
        );

        /******************* PARAMETRAGE SIMULATION *****************************/

        //Etat du dispositif (toujours inaccessible)
        $this->add(
            [
                'name'    => 'etat',
                'type'    => 'ValeurListeFieldset',
                'options' => [
                    'label' => 'Etat'
                ]
            ]
        );
        $this->add(
            [
                'name' => 'codeEtat',
                'type' => 'hidden'
            ]
        );

        //Début de validité
        $this->add(
            [
                'name'       => 'debutValidite',
                'type'       => 'DatePicker',
                'options'    => [
                    'label' => 'du',
                ],
                'attributes' => [
                    'id'       => 'debutValidite',
                    'required' => true
                ]
            ]
        );

        //Fin de validité
        $this->add(
            [
                'name'       => 'finValidite',
                'type'       => 'DatePicker',
                'options'    => [
                    'label' => 'au',
                ],
                'attributes' => [
                    'id' => 'finValidite'
                ]
            ]
        );

        //Calculer le dispositif
        $this->add(
            [
                'name'    => 'evaluerRegle',
                'type'    => 'radio',
                'options' => [
                    'label_attributes' => [
                        'class' => 'radio-inline'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ]
            ]
        );

        //Exclu le dispositif CEE
        $this->add(
            [
                'name'    => 'exclutCEE',
                'type'    => 'radio',
                'options' => [
                    'label_attributes' => [
                        'class' => 'radio-inline pull-right exclutForm'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ]
            ]
        );

        //Exclu les dispositifs de type Anah
        $this->add(
            [
                'name'    => 'exclutAnah',
                'type'    => 'radio',
                'options' => [
                    'label_attributes' => [
                        'class' => 'radio-inline pull-right exclutForm'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ]
            ]
        );

        //Exclu le dispositif
        $this->add(
            [
                'name'    => 'exclutEPCI',
                'type'    => 'radio',
                'options' => [
                    'label_attributes' => [
                        'class' => 'radio-inline pull-right exclutForm'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ]
            ]
        );

        //Exclu le dispositif
        $this->add(
            [
                'name'    => 'exclutRegion',
                'type'    => 'radio',
                'options' => [
                    'label_attributes' => [
                        'class' => 'radio-inline pull-right exclutForm'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ]
            ]
        );

        //Exclu le dispositif
        $this->add(
            [
                'name'    => 'exclutCommunal',
                'type'    => 'radio',
                'options' => [
                    'label_attributes' => [
                        'class' => 'radio-inline pull-right exclutForm'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ]
            ]
        );

        //Exclu le dispositif
        $this->add(
            [
                'name'    => 'exclutNational',
                'type'    => 'radio',
                'options' => [
                    'label_attributes' => [
                        'class' => 'radio-inline pull-right exclutForm'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ]
            ]
        );

        //Exclu le dispositif
        $this->add(
            [
                'name'    => 'exclutDepartement',
                'type'    => 'radio',
                'options' => [
                    'label_attributes' => [
                        'class' => 'radio-inline pull-right exclutForm'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ]
            ]
        );

         //Exclu le dispositif
        $this->add(
            [
                'name'    => 'exclutCoupDePouce',
                'type'    => 'radio',
                'options' => [
                    'label_attributes' => [
                        'class' => 'radio-inline pull-right exclutForm'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ]
            ]
        );

    }

    /**
     * Permet de retourner les validateurs sur les éléments
     * du formulaire
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            //Intitulé public
            'intitule'           => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "L'intitulé ne peut être vide."
                            ]
                        ]
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'max'      => 150,
                            'messages' => [
                                StringLength::TOO_LONG => "L'intitulé ne doit pas dépasser %max% caractères."
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Zend\Filter\StripTags',
                    ]
                ]
            ],
            //Intitulé conseiller
            'intituleConseiller' => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "L'intitulé ne peut être vide."
                            ]
                        ]
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'max'      => 150,
                            'messages' => [
                                StringLength::TOO_LONG => "L'intitulé ne doit pas dépasser %max% caractères."
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Zend\Filter\StripTags',
                    ]
                ]
            ],
            //Financeur
            'financeur'          => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Le financeur ne peut être vide."
                            ]
                        ]
                    ],
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'max'      => 150,
                            'messages' => [
                                StringLength::TOO_LONG => "Le financeur ne doit pas dépasser %max% caractères."
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Zend\Filter\StripTags',
                    ]
                ]
            ],
            //Descriptif
            'descriptif'         => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Le descriptif ne peut être vide."
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Zend\Filter\StripTags',
                    ]
                ]
            ],
            //Début validité
            'debutValidite'      => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "La date de début ne peut être vide."
                            ]
                        ]
                    ],
                    [
                        'name'    => 'Date',
                        'options' => [
                            'messages' => [
                                Date::INVALID_DATE => "La date n'est pas valide."
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Simulaides\Domain\Filter\Date'
                    ]
                ]
            ],
            //Fin validité
            'finValidite'        => [
                'required'    => false,
                'allow_empty' => true,
                'validators'  => [
                    [
                        'name'    => 'Date',
                        'options' => [
                            'messages' => [
                                Date::INVALID_DATE => "La date n'est pas valide."
                            ]
                        ]
                    ],
                    [
                        'name' => 'Simulaides\Form\Dispositif\Validator\DateValiditeValidator',
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Simulaides\Domain\Filter\Date'
                    ]
                ]
            ],
        ];
    }
}
