<?php
namespace Simulaides\Form\Dispositif;

use Zend\Form\Fieldset;

/**
 * Classe DispositifTravauxFieldset
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class DispositifTravauxFieldset extends Fieldset
{
    /**
     * Définition des éléments du fieldset
     */
    public function init()
    {
        parent::init();

        //Code travaux
        $this->add(
            [
                'name' => 'codeTravaux',
                'type' => 'hidden'
            ]
        );

        //Id du dispositif
        $this->add(
            [
                'name' => 'idDispositif',
                'type' => 'hidden'
            ]
        );

        //Eligibilité spécifique
        $this->add(
            [
                'name'       => 'eligibiliteSpecifique',
                'type'       => 'textArea',
                'options'    => [
                    'label' => "Exigences techniques"
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'id'    => 'eligibiliteSpecifique'
                ]
            ]
        );
    }
}
