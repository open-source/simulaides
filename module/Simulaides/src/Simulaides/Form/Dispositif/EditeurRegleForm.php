<?php

namespace Simulaides\Form\Dispositif;

use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable;
use Zend\Validator\NotEmpty;

/**
 * Classe EditeurRegleForm
 * Formulaire sur l'éditeur d'une règle de dispositif/travaux
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class EditeurRegleForm extends DispositifForm implements InputFilterProviderInterface
{
    /**
     * Définition des éléments du fieldset
     */
    public function init()
    {
        parent::init();

        //Définition de l'hydrator sur un tableau
        $this->setHydrator(new ArraySerializable());

        //Identifiant du parent auquel ajouter la règle
        $this->add(
            [
                'name' => 'idParent',
                'type' => 'hidden'
            ]
        );

        //Identifiant de la règle du dispositif/travaux
        $this->add(
            [
                'name' => 'id',
                'type' => 'hidden'
            ]
        );

        //Type de la règle
        $this->add(
            [
                'name'       => 'type',
                'type'       => 'Select',
                'options'    => [
                    'empty_option'              => '-- Type de règle --',
                    'disable_inarray_validator' => true
                ],
                'attributes' => [
                    'class'    => 'chosen-select form-control',
                    'onchange' => 'javascript:unsetElementActif();'
                ]
            ]
        );

        //Expression littérale (toujours inaccessible)
        $this->add(
            [
                'name'       => 'expressionLitteral',
                'type'       => 'Zend\Form\Element\Textarea',
                'options'    => [
                    'label' => 'Expression'
                ],
                'attributes' => [
                    'class'     => 'form-control expressionLitteral disabled-editeur',
                    'rows'      => 5,
                    'onclick'   => 'setElementActif(this); return false;',
                    'onkeyup'   => 'javascript:manageKeyUpAndFocus(event, this);',
                    'onkeydown' => 'javascript:manageKeyDown(event);return false;'
                ]
            ]
        );

        //Expression
        $this->add(
            [
                'name'       => 'expression',
                'type'       => 'hidden',
                'attributes' => [
                    'class' => 'expression',
                ]
            ]
        );

        //Condition (toujours inaccessible)
        $this->add(
            [
                'name'       => 'conditionRegleLitteral',
                'type'       => 'Zend\Form\Element\Textarea',
                'options'    => [
                    'label' => 'Condition'
                ],
                'attributes' => [
                    'class'     => 'form-control conditionRegleLitteral disabled-editeur',
                    'onclick'   => 'setElementActif(this); return false;',
                    'onkeyup'   => 'javascript:manageKeyUpAndFocus(event, this);',
                    'onkeydown' => 'javascript:manageKeyDown(event);return false;'
                ]
            ]
        );

        //Condition
        $this->add(
            [
                'name'       => 'conditionRegle',
                'type'       => 'hidden',
                'attributes' => [
                    'class' => 'conditionRegle',
                ]
            ]
        );

        //Nombre pour l'expression
        $this->add(
            [
                'name'       => 'nombreExpr',
                'options'    => [
                    'label' => 'Nombre'
                ],
                'attributes' => [
                    'class'       => 'editor-number form-control nombreExpr',
                    'placeholder' => 'Nombre'
                ]
            ]
        );

        //Nombre pour la condition
        $this->add(
            [
                'name'       => 'nombreCond',
                'options'    => [
                    'label' => 'Nombre'
                ],
                'attributes' => [
                    'class'       => 'editor-number form-control nombreCond',
                    'placeholder' => 'Nombre',
                ]
            ]
        );

        //Bouton OK sur le nombre pour l'expression
        $this->add(
            [
                'name'       => 'btnOkNombreExpr',
                'type'       => 'submit',
                'attributes' => [
                    'value'   => 'OK',
                    'onclick' => 'addNumberExprInElement();return false;',
                    'class'   => 'small-btn simul-btn pull-right'
                ]
            ]
        );

        //Bouton OK sur le nombre pour la condition
        $this->add(
            [
                'name'       => 'btnOkNombreCond',
                'type'       => 'submit',
                'attributes' => [
                    'value'   => 'OK',
                    'onclick' => 'addNumberCondInElement();return false;',
                    'class'   => 'small-btn simul-btn pull-right'
                ]
            ]
        );

        //Gestion des focus et éléments sélectionnés
        $this->add(
            [
                'name'       => 'activeElt',
                'type'       => 'hidden',
                'attributes' => [
                    'class' => 'activeElt'
                ]
            ]
        );

        //Bouton Enregistrer
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Enregistrer',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'submitFormRegle(); return false;'
                ]
            ]
        );

    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            //Type de règle
            'type'               => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Veuillez renseigner le type de votre règle'
                            ]
                        ]
                    ]
                ]
            ],
            //Expression littérale
            'expressionLitteral' => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Veuillez renseigner la partie "Expression" de votre règle'
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
