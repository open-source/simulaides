<?php
namespace Simulaides\Form\Dispositif;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Classe ResultatDispositifPDFForm
 * Formulaire pour la génération du résultat en PDF
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class ResultatDispositifPDFForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //champ caché Url téléchargement PDF
        $this->add(
            [
                'name'       => 'urlPDF',
                'type'       => 'hidden',
                'attributes' => [
                    'id' => 'urlPDF'
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrfPDF'
            ]
        );


        //Bouton Télécharger le PDF
        $this->add(
            [
                'name'       => 'exporter',
                'attributes' => [
                    'type'  => 'submit',
                    'title' => 'Exporter les dispositifs actifs',
                    'value' => "Exporter les dispositifs actifs",
                    'class' => "small-btn simul-btn acces-submit",
                ]
            ]
        );
    }
}
