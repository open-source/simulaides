<?php

namespace Simulaides\Form\Dispositif;

use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Service\SessionContainerService;
use Zend\Form\Form;

/**
 * Classe FiltreDispositifForm
 *
 * Projet : ADEME Simul'Aides
 *
 * @author
 */
class FiltreDispositifForm extends Form
{
    public function init()
    {
        parent::init();

        $this->add(
            [
                'name'       => 'codeRegion',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Région d'administration des dispositifs",
                    'empty_option' => 'Sélectionner une région',
                    'target_class' => 'Simulaides\Domain\Entity\Region',
                    'property'     => 'nom',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListeRegions',
                    ],
                ],
                'attributes' => [
                    'id'    => 'codeRegion',
                    'class' => 'chosen-select change-select form-control'
                ]
            ]
        );



        // Intitulé
        $this->add(
            [
                'name'       => 'intitule',
                'options'    => [
                    'label' => 'Intitulé'
                ],
                'attributes' => [
                    'class' => 'full-width',
                ]
            ]
        );

        //Financeur
        $this->add(
            [
                'name'       => 'financeur',
                'options'    => [
                    'label' => 'Financeur'
                ],
                'attributes' => [
                    'class' => 'full-width',
                ]
            ]
        );

        $this->add(
            [
                'name'       => 'etatDisp',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Etat",
                    'empty_option' => 'Tous les états',
                    'allow_empty'  => true,
                    'target_class' => 'Simulaides\Domain\Entity\ValeurListe',
                    'property'     => 'libelle',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListeEtatDispositif',
                    ],
                ],
                'attributes' => [
                    'id'       => 'filtre-etat',
                    'class'    => 'chosen-select change-select form-control'
                ]
            ]
        );

        $this->add(
            [
                'name'       => 'typeDisp',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Type de dispositif",
                    'empty_option' => 'Tous les types',
                    'allow_empty'  => true,
                    'target_class' => 'Simulaides\Domain\Entity\ValeurListe',
                    'property'     => 'libelle',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListeTypeDispositif',
                    ],
                ],
                'attributes' => [
                    'id'       => 'filtre-dispositif',
                    'class'    => 'chosen-select change-select form-control'
                ]
            ]
        );

        $this->add(
            [
                'name'       => 'codeTravaux',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Travaux",
                    'empty_option' => 'Tous',
                    'allow_empty'  => true,
                    'target_class' => 'Simulaides\Domain\Entity\Travaux',
                    'property'     => 'intitule',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListTravauxForFiltre',
                    ],
                ],
                'attributes' => [
                    'id'       => 'filtre-travaux',
                    'class'    => 'chosen-select form-control'
                ]
            ]
        );

        //Case à cocher validité
        $this->add(
            [
                'name'    => 'validite',
                'id'       => 'filtre-validite',
                'type'    => 'Zend\Form\Element\Checkbox',
                'options' => [
                    'label'              => "Dispositif en cours de validité",
                    'use_hidden_element' => false,
                    'checked_value'      => 1,
                    'unchecked_value'    => 0
                ]
            ]
        );

        //Bouton
        $this->add(
            [
                'name'       => 'rechercher',
                'attributes' => [
                    'id'      => 'rechercher',
                    'type'    => 'submit',
                    'value'   => 'Rechercher',
                    'class'   => 'simul-btn small-btn',
                    'onclick' => 'reloadRegion(); return false;',
                ]
            ]
        );
    }
}

