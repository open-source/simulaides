<?php
namespace Simulaides\Form\Dispositif;

use Zend\Form\Form;

/**
 * Classe RegionAdminForm
 * Formulaire pour le choix de la région
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class RegionAdminForm extends Form
{
    public function init()
    {
        parent::init();

        $this->add(
            [
                'name'       => 'codeRegion',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Région d'administration des dispositifs",
                    'empty_option' => 'Sélectionner une région',
                    'target_class' => 'Simulaides\Domain\Entity\Region',
                    'property'     => 'nom',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListeRegions',
                    ],
                ],
                'attributes' => [
                    'id'    => 'codeRegion',
                    'class' => 'chosen-select change-select form-control'
                ]
            ]
        );

        $this->add(
            [
                'name'       => 'typeDisp',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Type de dispositif :",
                    'empty_option' => 'Tous les types',
                    'allow_empty' => true,
                    'target_class' => 'Simulaides\Domain\Entity\ValeurListe',
                    'property'     => 'libelle',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListeTypeDispositif',
                    ],
                ],
                'attributes' => [
                    'id'        => 'filtre-dispositif',
                    'class'     => 'chosen-select change-select form-control',
                    'disabled'  => 'disabled'
                ]
            ]
        );

        $this->add(
            [
                'name'       => 'etatDisp',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => "Etat :",
                    'empty_option' => 'Tous les états',
                    'allow_empty' => true,
                    'target_class' => 'Simulaides\Domain\Entity\ValeurListe',
                    'property'     => 'libelle',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListeEtatDispositif',
                    ],
                ],
                'attributes' => [
                    'id'    => 'filtre-etat',
                    'class' => 'chosen-select change-select form-control',
                    'disabled'  => 'disabled'
                ]
            ]
        );
    }
}
