<?php
namespace Simulaides\Form\Dispositif;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable;

/**
 * Classe PerimetreGeoFieldset
 * Permet de gére le périmètre géographique dans la fiche dispositif
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class PerimetreGeoFieldset extends Fieldset implements InputFilterProviderInterface
{

    /**
     * Définition des éléments du fieldset
     */
    public function init()
    {
        parent::init();

        //Définition de l'hydrator sur un tableau
        $this->setHydrator(new ArraySerializable());

        //Liste des régions
        $this->add(
            [
                'name'       => 'codeRegion',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => 'Région',
                    'target_class' => 'Simulaides\Domain\Entity\Region',
                    'property'     => 'nom',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListeRegions',
                    ],
                ],
                'attributes' => [
                    'id'               => 'select-region',
                    'class'            => 'chosen-select form-control change-select',
                    'multiple'         => true,
                    'data-placeholder' => "Choisissez votre région"
                ]
            ]
        );

        //Liste des départements
        $this->add(
            [
                'name'       => 'codeDepartement',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => 'Département',
                    'target_class' => 'Simulaides\Domain\Entity\Departement',
                    'property'     => 'nom',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getDepartements',
                    ],
                ],
                'attributes' => [
                    'id'               => 'select-departement',
                    'class'            => 'chosen-select form-control change-select',
                    'multiple'         => true,
                    'data-placeholder' => "Choisissez votre département"
                ]
            ]
        );

        //Liste des communes
        $this->add(
            [
                'name'       => 'codeCommune',
                'type'       => 'Select',
                'options'    => [
                    'label' => 'Commune',
                ],
                'attributes' => [
                    'id'               => 'select-commune',
                    'class'            => 'chosen-select form-control',
                    'multiple'         => true,
                    'data-placeholder' => "Choisissez votre commune",
                ]
            ]
        );

        //Liste des epci
        $this->add(
            [
                'name'       => 'codeEpci',
                'type'       => 'Select',
                'options'    => [
                    'label' => 'Intercommunalité',
                ],
                'attributes' => [
                    'id'               => 'select-epci',
                    'class'            => 'chosen-select form-control',
                    'multiple'         => true,
                    'data-placeholder' => "Choisissez votre intercommunalité",
                ]
            ]
        );
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            //Région
            'codeRegion'      => [
                'required'    => false,
                'allow_empty' => true,
            ],
            //Département
            'codeDepartement' => [
                'required'    => false,
                'allow_empty' => true,
            ],
            //EPCI
            'codeEpci'        => [
                'required'    => false,
                'allow_empty' => true,
            ],
            //Commune
            'codeCommune'     => [
                'required'    => false,
                'allow_empty' => true,
            ],
        ];
    }
}
