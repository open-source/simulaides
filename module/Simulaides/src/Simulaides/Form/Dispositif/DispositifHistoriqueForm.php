<?php
namespace Simulaides\Form\Dispositif;

use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\NotEmpty;

/**
 * Classe DispositifHistoriqueForm
 * Formulaire pour la saisie d'une note de l'historique du dispositif
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Dispositif
 */
class DispositifHistoriqueForm extends DispositifForm implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Identifiant du dispositif
        $this->add(
            [
                'name' => 'idDispositif',
                'type' => 'hidden'
            ]
        );

        //Note
        $this->add(
            [
                'name'       => 'note',
                'type'       => 'textarea',
                'options'    => [
                    'label' => 'Note'
                ],
                'attributes' => [
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );


        //Bouton "Enregistrer"
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => "Ajouter la note à l'historique",
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'submitFormHistorique(); return false;'
                ]
            ]
        );
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            //Note
            'note' => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Veuillez renseigner la note'
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }
}
