<?php
namespace Simulaides\Form\Statistique;

use Simulaides\Constante\MainConstante;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\Date;
use Zend\Validator\NotEmpty;

/**
 * Classe StatistiqueForm
 * Formulaire de critère de filtre pour les statistiques
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Statistique
 */
class StatistiqueForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Date de début
        $this->add(
            [
                'name'       => MainConstante::NAME_DATE_DEBUT_PERIODE,
                'type'       => 'DatePicker',
                'options'    => [
                    'label' => 'Pour la période du'
                ],
                'attributes' => [
                    'id'       => 'debutPeriode',
                    'required' => true
                ]
            ]
        );

        //Date de fin
        $this->add(
            [
                'name'       => MainConstante::NAME_DATE_FIN_PERIODE,
                'type'       => 'DatePicker',
                'options'    => [
                    'label' => 'au'
                ],
                'attributes' => [
                    'id' => 'finPeriode'
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );
        
        //Bouton Afficher
        $this->add(
            [
                'name'       => 'affiche',
                'attributes' => [
                    'type'  => 'submit',
                    'value' => 'Afficher',
                    'class' => "small-btn simul-btn"
                ]
            ]
        );
    }

    /**
     * Permet de retourner les validateurs sur les éléments
     * du formulaire
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            //Début de période
            MainConstante::NAME_DATE_DEBUT_PERIODE => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "La date de début est obligatoire."
                            ]
                        ]
                    ],
                    [
                        'name'    => 'Date',
                        'options' => [
                            'messages' => [
                                Date::INVALID_DATE => "La date n'est pas valide."
                            ]
                        ]
                    ],
                    [
                        'name' => 'Simulaides\Domain\Validator\PeriodeDateValidator',
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Simulaides\Domain\Filter\Date'
                    ]
                ]
            ],
            //Fin de période
            MainConstante::NAME_DATE_FIN_PERIODE   => [
                'required'    => false,
                'allow_empty' => true,
                'validators'  => [
                    [
                        'name'    => 'Date',
                        'options' => [
                            'messages' => [
                                Date::INVALID_DATE => "La date n'est pas valide."
                            ]
                        ]
                    ],
                ],
                'filters'     => [
                    [
                        'name' => 'Simulaides\Domain\Filter\Date'
                    ]
                ]
            ],
        ];
    }
}
