<?php
namespace Simulaides\Form\ReferentielPrix\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Classe ProduitPrixOuValidator
 * Validateur pour la fiche de saisie de sprix d'un produit
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Travaux\Validator
 */
class ProduitPrixOuValidator extends AbstractValidator
{

    /** Somme égale à 0 */
    const SOMME_ZERO = 'SOMME_ZERO';

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::SOMME_ZERO => "Le prix total du produit ne peut être égal à 0.",
    ];

    /**
     * @param mixed $value
     * @param array $context
     *
     * @return bool
     */
    public function isValid($value, $context = [])
    {
        $prixMoOu = $context['prixUMoOu'];
        $prixFoOu = $context['prixUFournitureOu'];
        if ($prixFoOu + $prixMoOu == 0) {
            $this->error(self::SOMME_ZERO);
            return false;
        }
        return true;
    }
}