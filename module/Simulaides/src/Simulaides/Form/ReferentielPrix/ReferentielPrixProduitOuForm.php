<?php
namespace Simulaides\Form\ReferentielPrix;

use Zend\Form\Form;
use Zend\I18n\Validator\Float;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\NotEmpty;

/**
 * Classe ReferentielPrixProduitOuForm
 * Formulaire de modification des prix d'un produit pour l'Outremer
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\ReferentielPrix
 */
class ReferentielPrixProduitOuForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Code du produit
        $this->add(
            [
                'name' => 'code',
                'type' => 'hidden'
            ]
        );

        //Code du travaux
        $this->add(
            [
                'name' => 'codeTravaux',
                'type' => 'hidden'
            ]
        );

        // Intitulé
        $this->add(
            [
                'name'       => 'intitule',
                'options'    => [
                    'label' => "Intitulé"
                ],
                'attributes' => [
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Prix de la main d'oeuvre
        $this->add(
            [
                'name'       => 'prixUMoOu',
                'options'    => [
                    'label' => "Main d'oeuvre"
                ],
                'attributes' => [
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Prix des fournitures
        $this->add(
            [
                'name'       => 'prixUFournitureOu',
                'options'    => [
                    'label' => 'Fourniture'
                ],
                'attributes' => [
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //TVA
        $this->add(
            [
                'name'       => 'tvaOu',
                'options'    => [
                    'label' => 'TVA'
                ],
                'attributes' => [
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Aide à la saisie
        $this->add(
            [
                'name'       => 'infosSaisieCoutsOu',
                'type'       => 'WisiwigElement',
                'attributes' => [
                    'id' => 'infosSaisieCoutsOu'
                ],
                'options'    => [
                    'label' => 'Aide à la saisie'
                ],
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        //Bouton "Enregistrer"
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'    => 'button',
                    'value'   => 'Enregistrer',
                    'class'   => "small-btn simul-btn pull-right",
                    'onclick' => 'submitformRefPrix(); return false;'
                ]
            ]
        );

        //Bouton "Annuler"
        $this->add(
            [
                'name'       => 'close',
                'attributes' => [
                    'type'    => 'button',
                    'value'   => 'Annuler',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'closeModalForm(); return false;'
                ]
            ]
        );
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
         return [
                    'prixUMoOu'         => [
                        'required'    => true,
                        'allow_empty' => false,
                        'validators'  => [
                            [
                                'name'                   => 'NotEmpty',
                                'break_chain_on_failure' => true,
                                'options'                => [
                                    'messages' => [
                                        NotEmpty::IS_EMPTY => 'Le montant ne peut être vide'
                                    ],
                                    'type'     => 'string',
                                ]
                            ],
                            [
                                'name'    => 'Float',
                                'options' => [
                                    'locale'   => 'en_US',
                                    'messages' => [
                                        Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                                    ]
                                ]
                            ],
                            [
                                'name'                   => 'Simulaides\Form\ReferentielPrix\Validator\ProduitPrixOuValidator',
                                'break_chain_on_failure' => true,
                            ]
                        ],
                        'filters'     => [
                            [
                                'name'    => 'NumberFormat',
                                'options' => [
                                    'locale' => 'fr_FR'
                                ]
                            ]
                        ]
                    ],
                    'prixUFournitureOu' => [
                        'required'    => true,
                        'allow_empty' => false,
                        'validators'  => [
                            [
                                'name'                   => 'NotEmpty',
                                'break_chain_on_failure' => true,
                                'options'                => [
                                    'messages' => [
                                        NotEmpty::IS_EMPTY => 'Le montant ne peut être vide'
                                    ],
                                    'type'     => 'string',
                                ]
                            ],
                            [
                                'name'    => 'Float',
                                'options' => [
                                    'locale'   => 'en_US',
                                    'messages' => [
                                        Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                                    ]
                                ]
                            ],
                        ],
                        'filters'     => [
                            [
                                'name'    => 'NumberFormat',
                                'options' => [
                                    'locale' => 'fr_FR'
                                ]
                            ]
                        ]
                    ],
                    'tvaOu'           => [
                        'required'    => true,
                        'allow_empty' => false,
                        'validators'  => [
                            [
                                'name'                   => 'NotEmpty',
                                'break_chain_on_failure' => true,
                                'options'                => [
                                    'messages' => [
                                        NotEmpty::IS_EMPTY => 'La TVA ne peut être vide'
                                    ]
                                ]
                            ],
                            [
                                'name'    => 'Float',
                                'options' => [
                                    'locale'   => 'en_US',
                                    'messages' => [
                                        Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                                    ]
                                ]
                            ],
                        ],
                        'filters'     => [
                            [
                                'name'    => 'NumberFormat',
                                'options' => [
                                    'locale' => 'fr_FR'
                                ]
                            ]
                        ]
                    ]
                ];
    }
}
