<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 02/03/15
 * Time: 15:50
 */

namespace Simulaides\Form\Travaux\Validator;


use Simulaides\Constante\SessionConstante;
use Zend\Session\Container;
use Zend\Stdlib\ArrayUtils;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

/**
 * Class ParamValidator
 *
 * @package Simulaides\Form\Travaux\Validator
 */
class ParamValidator extends AbstractValidator
{

    /**
     *
     */
    const PARAMETER_REQUIRED = "Parameter";

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::PARAMETER_REQUIRED => "Attention, vous devez remplir toutes les caractéristiques.",
    ];


    /**
     * Tableau des paramètres de type "liste"
     *
     * @var array
     */
    protected $tabListParam = [];
    /**
     * Tableau des paramètres de type "booléen"
     *
     * @var array
     */
    protected $tabBooleanParam = [];
    /**
     * Tableau des paramètres de type "valeur"
     *
     * @var array
     */
    protected $tabValueParam = [];


    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @param  array $context
     *
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value, $context = [])
    {
        $session = new Container(SessionConstante::CONTAINER_NAME);
        if ($session->offsetExists(SessionConstante::NB_PARAMETER)) {
            $produitSelected = $session->offsetGet(SessionConstante::NB_PARAMETER);
            $produitSelected--;
            $session->offsetSet(SessionConstante::NB_PARAMETER, $produitSelected);
        }

        // Reprise de la logique pré-existante avec ajout des boucles pour que la généricité prévue au départ fonctionne. Je ne cautionne pas. ..moi non plus.
        // + le validateur est appelé pour chaque paramètre alors qu'il traite tout le fieldset

        if (empty($this->tabBooleanParam)) {
            // S'il n'existe pas de paramètre "booléen", les paramètres "liste" doivent être renseignés
            foreach ($this->tabListParam as $listParam) {
                if (is_string($context[$listParam]) && (strlen($context[$listParam]) === 0)) {
                    $this->error(self::PARAMETER_REQUIRED);
                    return false;
                }
            }
        } else {
            // S'il existe un paramètre "booléen" à vrai, les autres paramètres "liste" doivent être renseignés
            foreach ($this->tabBooleanParam as $booleanParam) {
                if ($context[$booleanParam] === "1") {
                    foreach ($this->tabListParam as $listParam) {
                        if (is_string($context[$listParam]) && (strlen($context[$listParam]) === 0)) {
                            $this->error(self::PARAMETER_REQUIRED);
                            return false;
                        }
                    }
                }
            }
        }

        foreach ($this->tabValueParam as $valueParam) {
            if (is_string($context[$valueParam]) && (strlen($context[$valueParam]) == 0)) {
                // Cas spécifique pour les fenêtres : le 2nd paramètre est optionnel
                if (preg_match('/^valeur(_[A-Z]+)*_FEN_[A-Z]+_2$/', $valueParam) !== 1) {
                    $this->error(self::PARAMETER_REQUIRED);
                    return false;
                }
            }
        }

        foreach ($this->tabBooleanParam as $booleanParam) {
            if (is_string($context[$booleanParam]) && (strlen($context[$booleanParam]) === 0)) {
                $this->error(self::PARAMETER_REQUIRED);
                return false;
            } else {
                if ($context[$booleanParam] === "0") {
                    $produitSelected = $session->offsetGet(SessionConstante::NB_PARAMETER);
                    $produitSelected++;
                    $session->offsetSet(SessionConstante::NB_PARAMETER, $produitSelected);
                }
            }
        }

        return true;
    }

    /**
     * @param array $tabListParam
     */
    public function setTabListParam($tabListParam)
    {
        $this->tabListParam = $tabListParam;
    }

    /**
     * @param array $tabBooleanParam
     */
    public function setTabBooleanParam($tabBooleanParam)
    {
        $this->tabBooleanParam = $tabBooleanParam;
    }

    /**
     * @param array $tabValueParam
     */
    public function setTabValueParam($tabValueParam)
    {
        $this->tabValueParam = $tabValueParam;
    }
}
