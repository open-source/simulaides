<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 02/03/15
 * Time: 17:11
 */

namespace Simulaides\Form\Travaux\Validator;

use Simulaides\Constante\SessionConstante;
use Zend\Validator\AbstractValidator;

/**
 * Class CoutValidator
 * @package Simulaides\Form\Travaux\Validator
 */
class CoutValidator extends AbstractValidator
{
    /** Somme des couts MO + FO inégale avec Cout Total */
    const SOMME_INEGALE = "sommeInegale";

    /** Le cout total est supérieur au cout MO */
    const TOTAL_INF_MO = "totalInfMo";

    /** Le cout total est supérieur au cout FO */
    const TOTAL_INF_FO = "totalInfFo";

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::SOMME_INEGALE => "Le coût total doit être égal à la somme des coûts de main d'oeuvre et de fourniture.",
        self::TOTAL_INF_MO  => "Le coût total doit être supérieur au coût de main d'oeuvre.",
        self::TOTAL_INF_FO  => "Le coût total doit être supérieur au coût de fourniture."
    ];

    /**
     * @param mixed $value
     * @param array $context
     *
     * @return bool
     */
    public function isValid($value, $context = [])
    {
        $coutTotal = $this->getCoutValue($context, SessionConstante::COUT_TOTAL);
        $coutMo    = $this->getCoutValue($context, SessionConstante::COUT_MO);
        $coutFo    = $this->getCoutValue($context, SessionConstante::COUT_FO);

        if (!empty($coutTotal)) {
            //Vérification que le Cout MO < Cout total
            if (!empty($coutMo)) {
                if ($coutMo > $coutTotal) {
                    $this->error(self::TOTAL_INF_MO);
                    return false;
                }
            }

            //Vérification que le Cout FO < Cout total
            if (!empty($coutFo)) {
                if ($coutFo > $coutTotal) {
                    $this->error(self::TOTAL_INF_FO);
                    return false;
                }
            }

            //Vérification que cout MO + Cout FO = Cout total
            if (!empty($coutFo) && ($coutFo > 0) && !empty($coutMo) && ($coutMo > 0)) {
                //Utilisation de bccomp pour comparer les décimal, sinon, ca ne fonctionne pas

                $coutFo = round(floatval($coutFo), 2);
                $coutMo = round(floatval($coutMo), 2);
                $coutTotal = round(floatval($coutTotal), 2);

                if (round(($coutFo + $coutMo),2) !== $coutTotal) {
                    $this->error(self::SOMME_INEGALE);
                    return false;
                }
            }



        }
        return true;
    }

    /**
     * Retourne la valeur saisie
     *
     * @param $context
     * @param $key
     *
     * @return null
     */
    private function getCoutValue($context, $key)
    {
        $cout = null;
        if (isset($context[$key]) && !empty($context[$key])) {
            $cout = str_replace(",", ".", $context[$key]);
        }
        return $cout;
    }
}
