<?php
namespace Simulaides\Form\Travaux;

use Simulaides\Tools\Utils;
use Zend\Form\Form;

/**
 * Classe BbcForm
 *
 * Projet : Instm Ent 2013-2015

 *
 * @copyright Copyright © Institut Mines Télécom 2013-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Travaux
 */
class BbcForm extends Form
{
    /**
     * This function is automatically called when creating element with factory. It
     * allows to perform various operations (add elements...)
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->add([
            'name'    => 'bbc',
            'type'    => 'radio',
            'options' => [
                'value_options'    => Utils::getListeOuiNon()
            ],
            'attributes' => [
                'id' => 'bbc',
            ]
        ]);

        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        $this->add([
            'name'       => 'submit',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'Lancer la simulation',
                'id'    => 'main-submit',
                'class' => 'btn btn-success pull-right'
            ]
        ]);
    }


}
