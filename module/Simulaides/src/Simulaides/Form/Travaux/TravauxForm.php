<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 15:38
 */

namespace Simulaides\Form\Travaux;

use Simulaides\Domain\Entity\Projet;
use Simulaides\Constante\SessionConstante;
use Simulaides\Domain\SessionObject\Travaux\ProduitSession;
use Simulaides\Service\LocalisationService;
use Zend\Form\Form;
use Zend\I18n\Validator\Float;
use Zend\Validator\GreaterThan;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class TravauxForm
 * @package Simulaides\Form\TravauxSession
 */
class TravauxForm extends Form //implements InputFilterProviderInterface
{
    /**
     * Constante
     */
    const PRODUIT_LABEL_CLASS = 'produit-label';


    protected $errorMessages = [];

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param array $errorMessages
     */
    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    /**
     * @var string
     */
    protected $label;

    /**
     * Liste de ProduitSession
     * @var array
     */
    protected $displayProduits;

    /**
     * @var Projet $projet
     */
    protected $projet;

    /**
     * Indique si le produit est de la catégorie de travaux 'Etude"
     * @var boolean
     */
    protected $isEtude;
    /**
     * @var string
     */
    protected $codeTravaux;

    /**
     * Indique si les couts calculés sont affichés
     * @var boolean
     */
    private $avecCoutCalcule;

    private $isNew;

    /**
     * @param null $name
     * @param array $options
     */
    public function __construct($name = null, $options = [])
    {
        parent::__construct('travaux', $options);
        $this->label             = $options['label'];
        $this->codeTravaux       = $options['codeTravaux'];
        $this->projet            = $options['projet'];
        $this->displayProduits   = $options['produits'];
        $this->avecCoutCalcule   = $options['avecCoutCalcule'];
        $this->isEtude           = $options['isEtude'];
        $this->isNew             = $options['isNew'];
        $this->isOutreMer        = $options['isOutreMer'];
    }

    /**
     *
     */
    public function init()
    {
        $this->add(
            [
                'name'       => 'codeTravaux',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $this->codeTravaux
                ]
            ]
        );

        /** @var ProduitSession $produit */
        foreach ($this->displayProduits as $produit) {
            /** @var SaisieProduitFieldset $saisieProduitFieldset */
            $saisieProduitFieldset = $this->factory->getFormElementManager()->get(
                'SaisieProduitFieldset',
                [
                    'produit'           => $produit,
                    'avecCoutCalcule'   => $this->avecCoutCalcule,
                    'isEtude'           => $this->isEtude,
                    'isNew'             => $this->isNew,
                    'isOutreMer'        => $this->isOutreMer
                ]
            );
            $saisieProduitFieldset->setName($produit->getCodeProduit());
            $this->add($saisieProduitFieldset);
        }

        $this->setWrapElements(true);
    }

    /**
     * @return mixed
     */
    public function getDisplayProduits()
    {
        return $this->displayProduits;
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    /*public function getInputFilterSpecification()
    {
        $inputFilterSpecification = [];

        return $inputFilterSpecification;
    }*/
}
