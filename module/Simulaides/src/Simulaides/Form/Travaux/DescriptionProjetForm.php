<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 15:38
 */

namespace Simulaides\Form\Travaux;

use Simulaides\Tools\Utils;
use Zend\Form\Form;

/**
 * Class DescriptionProjetForm
 *
 * @package   Simulaides\Form\Travaux
 */
class DescriptionProjetForm extends Form
{
    /**
     * @var string
     */
    protected $listeTravaux;

    /**
     * @param null $name
     * @param array $options
     */
    public function __construct($name = null, $options = [])
    {
        parent::__construct('DescriptionProjet', $options);
        $this->listeTravaux = $options['listeTravaux'];
        $value_options = [];

        foreach($this->listeTravaux as $categorie => $travaux) {
            foreach ($travaux as $t) {
                $value_options[] = [
                    'value' => $t['code'],
                    'label' => $t['intitule']
                ];
            }
        }

        $this->add([
            'type'      => '\Zend\Form\Element\MultiCheckbox',
            'name'      => 'listeTravaux',
            'options'   => [
                'label'         => false,
                'value_options' => $value_options
            ]
        ]);




        $this->add([
            'name'    => 'bbc',
            'type'    => 'radio',
            'options'    => [
                'label_attributes' => [
                    'class' => 'radio-inline'
                ],
                'value_options' => [
                    [
                        'value'            => 1,
                        'label'            => 'Oui',
                        'attributes'       => [
                            'id' => 'bbc-oui'
                        ],
                        'label_attributes' => [
                            'class' => 'radio-inline',
                            'for'   => 'bbc-oui'
                        ]

                    ],
                    [
                        'value'            => 0,
                        'label'            => 'Non / Je ne sais pas',
                        'attributes'       => [
                            'id' => 'bbc-non'
                        ],
                        'label_attributes' => [
                            'class' => 'radio-inline',
                            'for'   => 'bbc-non'
                        ]
                    ]
                ]
            ],
            'attributes' => [
                'id' => 'bbc',
            ]
            
        ]);

        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        $this->add([
            'name'       => 'submit',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'Suivant',
                'id'    => 'main-submit',
                'class' => 'simul-btn small-btn'
            ]
        ]);
    }
}
