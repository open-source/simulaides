<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 26/02/15
 * Time: 11:25
 */

namespace Simulaides\Form\Travaux;

use Simulaides\Constante\ListeOuiNonConstante;
use Simulaides\Constante\ParametreTechConstante;
use Simulaides\Constante\SessionConstante;
use Simulaides\Domain\SessionObject\Travaux\ProduitSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitCoutSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitParametreTechSession;
use Simulaides\Tools\Utils;
use Zend\Form\Fieldset;
use Zend\I18n\Validator\Float;
use Zend\I18n\Validator\Int;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Session\Container;
use Zend\Validator\GreaterThan;

/**
 * Class SaisieProduitFieldset
 *
 * @package Simulaides\Form\TravauxSession
 */
class SaisieProduitFieldset extends Fieldset implements InputFilterProviderInterface
{
    /**
     * Constante
     */
    const PRODUIT_LABEL_CLASS = 'produit-label';
    /*pour les boutons radio*/
    const PRODUIT_RADIO_CLASS = 'produit-radio';

    /** @var  ProduitSession $produit */
    protected $produit;

    /**
     * Indique si le produit est de la catégorie de travaux 'Etude"
     *
     * @var boolean
     */
    protected $isEtude;

    /**
     * Indique si les couts calculés sont affichés
     *
     * @var boolean
     */
    protected $avecCoutCalcule;

    /**
     * Tableau des paramètres de type "liste"
     *
     * @var array
     */
    protected $tabListParam = [];
    /**
     * Tableau des paramètres de type "booléen"
     *
     * @var array
     */
    protected $tabBooleanParam = [];
    /**
     * Tableau des paramètres de type "valeur"
     *
     * @var array
     */
    protected $tabValueParam = [];


    protected $nbParamTech = 0;

    protected $isNew = true;

    protected $isOutreMer = false;


    /**
     * @param null  $name
     * @param array $options
     */
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        //Affectation du produit de la session au fieldSet
        $this->produit = $options['produit'];
        //Affectation de l'information d'affichage des couts calculés
        $this->avecCoutCalcule   = $options['avecCoutCalcule'];
        $this->isEtude           = $options['isEtude'];
        $this->isNew             = $options['isNew'];
        $this->isOutreMer        = $options['isOutreMer'];
    }


    /**
     *
     */
    public function init()
    {
        parent::init();

        //Code du produit
        $this->add(
            [
                'name'       => 'codeProduit',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $this->produit->getCodeProduit()
                ]
            ]
        );
        //Texte d'aide du produit
        $this->add(
            [
                'name'       => 'texteAide',
                'attributes' => [
                    'value' => ($this->isOutreMer)?  $this->produit->getInfosSaisieCoutOu() : $this->produit->getInfosSaisieCout(),
                    'class' => 'help-block-produit',
                ],
                'options'    => [
                    'type' => 'texteAide'
                ]
            ]
        );


        $nbParamTech    = 0;
        $saisieProduits = $this->produit->getSaisiesProduit();
        foreach ($saisieProduits as $saisieProduit) {
            if ($saisieProduit instanceof SaisieProduitCoutSession) {
                $this->add(
                    [
                        'name'    => $saisieProduit->getTypeCout(),
                        'options' => [
                            'label'            => $saisieProduit->getLabelTypeMontantHT(),
                            'type'             => 'cout',
                            'label_attributes' => [
                                'class' => self::PRODUIT_LABEL_CLASS
                            ],
                        ],

                        'attributes' => [
                            'id'    => $saisieProduit->getTypeCout() . '_' . $saisieProduit->getCodeProduit(),
                            'class' => 'form-control input-travaux',
                        ]
                    ]
                );
                if ($this->avecCoutCalcule) {
                    //Affichage des couts calculés
                    $this->add(
                        [
                            'name'       => $saisieProduit->getTypeCout() . '_calcule',
                            'options'    => [
                                'label'            => $saisieProduit->getLabelTypeMontantHTEstime(),
                                'type'             => 'cout_calcule',
                                'label_attributes' => [
                                    'class' => self::PRODUIT_LABEL_CLASS
                                ],
                            ],
                            'attributes' => [
                                'class'    => 'form-control input-travaux',
                                'disabled' => true
                            ],

                        ]
                    );
                }
            } elseif ($saisieProduit instanceof SaisieProduitParametreTechSession) {
                $nbParamTech++;

                $value = $saisieProduit->getCodeParametre();
                $label = $saisieProduit->getParamLibelle();
                $name  = $saisieProduit->getCodeProduit() . '_' . $nbParamTech;

                switch ($saisieProduit->getParamType()) {

                    case ParametreTechConstante::TYPE_DATA_LISTE:
                        $codeListe = $saisieProduit->getParamCodeListe();
                        $this->addListParam($value, $label, $name, $codeListe);
                        break;
                    case ParametreTechConstante::TYPE_DATA_BOOLEEN:
                        $this->addBooleanParam($value, $label, $name);
                        break;
                    case ParametreTechConstante::TYPE_DATA_BOOLEEN_RADIO:
                        $this->addBooleanRadioParam($value, $label, $name);
                        break;
                    default:
                        $this->addValueParam($value, $label, $name);
                        break;
                }
            }
        }


        /*if ($this->isEtude) {
            $label = 'Je ne connais aucun coût associé à cette étude, la simulation sera basée sur une estimation.';
        } else {
            $label = 'Je ne connais aucun coût associé à ces travaux, la simulation sera basée sur une estimation.';
        }
        $this->add(
            [
                'name'       => 'devisUser',
                'type'       => 'radio',
                'options'    => [
                    'value_options'    => [
                        '0' => $label,
                        '1' => 'Je saisis ci-dessous le ou les coûts connus en €, Hors taxes:',
                    ],
                    'type'             => 'cout',
                    'label_attributes' => [
                        'class' => self::PRODUIT_LABEL_CLASS
                    ]
                ],
                'attributes' => [
                    'class' => 'devis-user ' . $this->produit->getCodeProduit(),
                    'value' => 0
                ],

            ]
        );*/
        $this->add(
            [
                'name'       => 'devisUser',
                'type'       => 'checkbox',
                'options'    => [
                    'label' => 'Je souhaite saisir le ou les coûts connus en €, Hors Taxes',
                    'value' => '1',
                    'type'  => 'cout',
                    'label_attributes' => [
                        'class' => self::PRODUIT_LABEL_CLASS
                    ]
                ],
                'attributes' => [
                    'class' => 'devis-user ' . $this->produit->getCodeProduit(),
                ],

            ]
        );
        $session = new Container(SessionConstante::CONTAINER_NAME);

        $this->nbParamTech = $nbParamTech;
    }

    /**
     * @return int
     */
    public function getNbParamTech()
    {
        return $this->nbParamTech;
    }

    /**
     * @param int $nbParamTech
     */
    public function setNbParamTech($nbParamTech)
    {
        $this->nbParamTech = $nbParamTech;
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $inputFilterSpecification = [
            SessionConstante::COUT_MO    => [
                'allow_empty' => true,
                'validators'  => [
                    [
                        'name'                   => 'Float',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'locale'   => 'en_US',
                            'messages' => [
                                Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'GreaterThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => -1,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Ce nombre doit etre positif.'
                            ]
                        ]
                    ]
                ],
                'filters'     => [
                    [
                        'name'    => 'NumberFormat',
                        'options' => [
                            'locale' => 'fr_FR'
                        ]
                    ]
                ]
            ],
            SessionConstante::COUT_TOTAL => [
                'allow_empty' => true,
                'validators'  => [
                    [
                        'name'                   => 'Float',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'locale'   => 'en_US',
                            'messages' => [
                                Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'GreaterThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => -1,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Ce nombre doit etre positif.'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Simulaides\Form\Travaux\Validator\CoutValidator',
                        'break_chain_on_failure' => true,
                    ]
                ],
                'filters'     => [
                    [
                        'name'    => 'NumberFormat',
                        'options' => [
                            'locale' => 'fr_FR'
                        ]
                    ]
                ]
            ],
            SessionConstante::COUT_FO    => [
                'allow_empty' => true,
                'validators'  => [
                    [
                        'name'                   => 'Float',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'locale'   => 'en_US',
                            'messages' => [
                                Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                            ]
                        ]
                    ],
                    [
                        'name'    => 'GreaterThan',
                        'options' => [
                            'min'      => -1,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Ce nombre doit etre positif.'
                            ]
                        ]
                    ]
                ],
                'filters'     => [
                    [
                        'name'    => 'NumberFormat',
                        'options' => [
                            'locale' => 'fr_FR'
                        ]
                    ]
                ]
            ]
        ];

        foreach ($this->tabListParam as $listParam) {
            $inputFilterSpecification[$listParam] = [
                'required'    => true,
                'allow_empty' => true,
                'validators'  => [
                    [
                        'name'                   => 'Simulaides\Form\Travaux\Validator\ParamValidator',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'tabListParam'    => $this->tabListParam,
                            'tabBooleanParam' => $this->tabBooleanParam,
                            'tabValueParam'   => $this->tabValueParam
                        ]
                    ]
                ]
            ];
        }

        foreach ($this->tabBooleanParam as $booleanParam) {
            $inputFilterSpecification[$booleanParam] = [
                'required'    => true,
                'allow_empty' => true,
                'validators'  => [
                    [
                        'name'                   => 'Simulaides\Form\Travaux\Validator\ParamValidator',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'tabListParam'    => $this->tabListParam,
                            'tabBooleanParam' => $this->tabBooleanParam,
                            'tabValueParam'   => $this->tabValueParam
                        ]
                    ]
                ]
            ];
        }

        foreach ($this->tabValueParam as $valueParam) {
            $inputFilterSpecification[$valueParam] = [
                'required'    => true,
                'allow_empty' => true,
                'validators'  => [
                    [
                        'name'                   => 'Simulaides\Form\Travaux\Validator\ParamValidator',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'tabListParam'    => $this->tabListParam,
                            'tabBooleanParam' => $this->tabBooleanParam,
                            'tabValueParam'   => $this->tabValueParam
                        ]
                    ],
                    [
                        'name'                   => 'Int',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'locale'   => 'en_US',
                            'messages' => [
                                Int::NOT_INT => 'Ce nombre doit être un entier'
                            ]
                        ]
                    ],
                ]
            ];
        }

        return $inputFilterSpecification;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->produit->getLibelle();
    }

    /**
     * @return boolean
     */
    public function getIsEtude()
    {
        return $this->isEtude;
    }

    /**
     * Ajoute un champ paramètre de type liste
     *
     * @param string $value     Valeur
     * @param string $label     Label
     * @param string $name      Nom
     * @param string $codeListe Code de la liste de valeurs
     */
    private function addListParam($value, $label, $name, $codeListe)
    {
        $this->add(
            [
                'name'       => 'hidden_liste_' . $name,
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $value
                ]
            ]
        );

        $fieldName            = 'valeur_liste_' . $name;
        $this->tabListParam[] = $fieldName;
        $this->add(
            [
                'name'       => $fieldName,
                'type'       => 'ValeurListeObjectSelect',
                'options'    => [
                    'label'            => $label,
                    'empty_option'     => 'Choisissez une valeur',
                    'find_method'      => [
                        'name'   => 'getListeValuesByCodeListe',
                        'params' => [
                            'codeListe' => $codeListe
                        ],
                    ],
                    'technique'        => true,
                    'label_attributes' => [
                        'class' => self::PRODUIT_LABEL_CLASS
                    ],
                ],
                'attributes' => [
                    'id'    => $fieldName,
                    'class' => 'chosen-select form-control'
                ]
            ]
        );
    }

    /**
     * Ajoute un champ paramètre de type booléen
     *
     * @param string $value Valeur
     * @param string $label Label
     * @param string $name  Nom
     */
    private function addBooleanParam($value, $label, $name)
    {
        $this->add(
            [
                'name'       => 'hidden_check_' . $name,
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $value
                ]
            ]
        );
        $fieldName = 'valeur_check_' . $name;
        $this->tabBooleanParam[] = $fieldName;
        $this->add(
            [
                'name'       => $fieldName,
                'type'       => 'Zend\Form\Element\Checkbox',
                'options'    => [
                    'label'            => $label,
                    'technique'        => true,
                    'check'            => true,
                    'label_attributes' => [
                        'class' => self::PRODUIT_LABEL_CLASS
                    ],
                ],
                'attributes' => [
                    'id' => $fieldName
                ],

            ]
        );
    }

    /**
     * Ajoute un champ paramètre de type booléen
     *
     * @param string $value Valeur
     * @param string $label Label
     * @param string $name  Nom
     */
    private function addBooleanRadioParam($value, $label, $name)
    {
        $this->add(
            [
                'name'       => 'hidden_radio_check_' . $name,
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $value
                ]
            ]
        );

        $fieldName = 'valeur_radio_check_' . $name;

        $this->add(
            [
                'name'       => $fieldName,
                'type'       => 'radio',
                'options'    => [
                    'label'            => $label,
                    'technique'        => true,
                    'check'            => true,
                    'label_attributes' => [
                        'class' => self::PRODUIT_LABEL_CLASS
                    ],
                    'value_options'    => Utils::getListeOuiNon(),
                ],
                'attributes' => [
                    'id' => $fieldName,
                    'value' => ListeOuiNonConstante::VALUE_NON,
                    'class' => self::PRODUIT_RADIO_CLASS
                ],

            ]
        );

    }

    /**
     * Ajoute un champ paramètre de type valeur
     *
     * @param string $value Valeur
     * @param string $label Label
     * @param string $name  Nom
     */
    private function addValueParam($value, $label, $name)
    {
        $this->add(
            [
                'name'       => 'hidden_valeur_' . $name,
                'type'       => 'hidden',
                'attributes' => [
                    'value'            => $value,
                    'label_attributes' => [
                        'class' => self::PRODUIT_LABEL_CLASS
                    ],
                ]
            ]
        );

        $fieldName             = 'valeur_' . $name;
        $this->tabValueParam[] = $fieldName;
        $this->add(
            [
                'name'       => $fieldName,
                'options'    => [
                    'label'            => $label,
                    'technique'        => true,
                    'label_attributes' => [
                        'class' => self::PRODUIT_LABEL_CLASS.' display-block'
                    ],
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'id'    => $fieldName
                ],

            ]
        );
    }
}
