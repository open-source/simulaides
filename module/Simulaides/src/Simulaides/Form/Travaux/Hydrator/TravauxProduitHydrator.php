<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 02/03/15
 * Time: 09:05
 */

namespace Simulaides\Form\Travaux\Hydrator;

use Simulaides\Constante\ParametreTechConstante;
use Simulaides\Domain\SessionObject\Travaux\ProduitSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitCoutSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitParametreTechSession;
use Simulaides\Domain\SessionObject\Travaux\TravauxSession;
use Simulaides\Tools\Utils;
use Zend\Stdlib\Hydrator\HydratorInterface;

/**
 * Class TravauxProduitHydrator
 *
 * @package Simulaides\Form\Travaux\Hydrator
 */
class TravauxProduitHydrator implements HydratorInterface
{
    /**
     * Extract values from an object
     *
     * @param  object $object
     *
     * @return array
     */
    public function extract($object)
    {
        $datas = [];
        /**
         * @var TravauxSession $object
         */
        if ($object->getCodeTravaux() != null) {
            $datas['codeTravaux'] = $object->getCodeTravaux();
            $produits             = $object->getProduits();
            $datas['addTravaux']  = 1;
            /** @var ProduitSession $produit */
            foreach ($produits as $produit) {
                $saisies      = $produit->getSaisiesProduit();
                $produitArray = [];
                $nbParamTech  = 1;
                foreach ($saisies as $saisie) {
                    if ($saisie instanceof SaisieProduitCoutSession) {
                        $coutUser = $saisie->getCoutUser();
                        if(!is_null($coutUser)){
                            $produitArray['devisUser'] = 1;
                        }
                        $produitArray[$saisie->getTypeCout()]              = Utils::formatNumberToString($saisie->getCout(), 2);
                        $produitArray[$saisie->getTypeCout() . '_calcule'] = Utils::formatNumberToString($saisie->getCoutCalcule(), 2);

                    } else {
                        $name = $saisie->getCodeProduit() . '_' . $nbParamTech;
                        /** @var SaisieProduitParametreTechSession $saisie */
                        switch ($saisie->getParamType()) {
                            case ParametreTechConstante::TYPE_DATA_LISTE:
                                $produitArray['valeur_liste_' . $name] = $saisie->getValeur();
                                $produitArray['hidden_liste_' . $name] = $saisie->getCodeParametre();
                                break;
                            case ParametreTechConstante::TYPE_DATA_BOOLEEN:
                                  $produitArray['valeur_check_' . $name] = $saisie->getValeur();
                                  $produitArray['hidden_check_' . $name] = $saisie->getCodeParametre();
                                break;
                            case ParametreTechConstante::TYPE_DATA_BOOLEEN_RADIO:
                                $produitArray['valeur_' . $name] = $saisie->getValeur();
                                $produitArray['hidden_valeur_' . $name] = $saisie->getCodeParametre();
                                break;
                            default:
                                $produitArray['valeur_' . $name]        = $saisie->getValeur();
                                $produitArray['hidden_valeur_' . $name] = $saisie->getCodeParametre();
                                break;
                        }
                        $nbParamTech++;
                    }
                }
                $datas[$produit->getCodeProduit()] = $produitArray;
            }
        }

        return $datas;
    }

    /**
     * Hydrate $object with the provided $data.
     *
     * @param  array  $data
     * @param  object $object
     *
     * @return object
     */
    public function hydrate(array $data, $object)
    {
        $object->removeAllProduits();
        foreach ($data as $code => $prod) {
            if (!is_array($prod)) {
                if ($code == 'codeTravaux') {
                    $object->setCodeTravaux($prod);
                }
            } else {
                $produit = new ProduitSession();
                $produit->setCodeProduit($code);

                $produitEstVide = false;

                foreach ($prod as $fieldName => $field) {
                    if (explode('_', $fieldName)[0] == "COUT") {
                        //Saisie des couts HT du produit
                        $saisie = new SaisieProduitCoutSession();
                        $saisie->setCodeProduit($prod['codeProduit']);
                        $saisie->setTypeCout($fieldName);
                        $saisie->setCout($field);
                        $produit->addSaisiesProduit($saisie);
                        unset($saisie);
                    } else {
                        //Saisie des paramètres techniques du produit
                        $saisie = new SaisieProduitParametreTechSession();
                        $saisie->setCodeProduit($prod['codeProduit']);
                        $saisie->setValeur($field);

                        if (strpos($fieldName, 'valeur_liste') === 0) {
                            $saisie->setCodeParametre($prod[str_replace('valeur_liste', 'hidden_liste', $fieldName)]);
                            $saisie->setParamType(ParametreTechConstante::TYPE_DATA_LISTE);
                            $produit->addSaisiesProduit($saisie);
                        } elseif (strpos($fieldName, 'valeur_check') === 0) {
                            $saisie->setCodeParametre($prod[str_replace('valeur_check', 'hidden_check', $fieldName)]);
                            $saisie->setParamType(ParametreTechConstante::TYPE_DATA_BOOLEEN);
                            $produit->addSaisiesProduit($saisie);
                            if(!$saisie->getValeur()){
                                $produitEstVide = true;
                            }
                        } elseif (strpos($fieldName, 'valeur_radio_check_') === 0) {
                            $saisie->setCodeParametre($prod[str_replace('valeur_radio_check_', 'hidden_radio_check_',
                                                                        $fieldName)]);
                            $saisie->setParamType(ParametreTechConstante::TYPE_DATA_BOOLEEN_RADIO);
                            $produit->addSaisiesProduit($saisie);

                        } elseif (strpos($fieldName, 'valeur') === 0) {
                            $saisie->setCodeParametre($prod[str_replace('valeur', 'hidden_valeur', $fieldName)]);
                            $saisie->setParamType(ParametreTechConstante::TYPE_DATA_ENTIER);
                            $produit->addSaisiesProduit($saisie);
                        }
                        unset($saisie);
                    }
                }

                if(!$produitEstVide) {
                    $object->addProduit($produit);
                }
                unset($produit);
            }
        }
        return $object;
    }
}
