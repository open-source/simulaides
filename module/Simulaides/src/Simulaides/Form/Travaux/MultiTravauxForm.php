<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 15:38
 */

namespace Simulaides\Form\Travaux;

use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\SessionObject\Travaux\ProduitSession;
use Zend\Form\Form;
use Simulaides\Constante\ValeurListeConstante;

/**
 * Class MultiTravauxForm
 * @package Simulaides\Form\Travaux
 */
class MultiTravauxForm extends Form
{
    /**
     * @var string
     */
    protected $label;

    /**
     * Liste de Produit Par travaux
     * @var array
     */
    protected $produitsParTravaux;

    /**
     * @var string
     */
    protected $codesTravaux;

    /**
     * Indique si les couts calculés sont affichés
     * @var boolean
     */
    private $avecCoutCalcule;

    /**
     *
     */
    private $travaux;

    private $bbc;

    /**
     * @param null $name
     * @param array $options
     */
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->label                = $options['label'];
        $this->bbc                  = isset($options['bbc'])?$options['bbc']:0;
    }

    public function init()
    {
        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'  => 'submit',
                    'value' => 'Calculer',
                    'id'    => 'calculer',
                    'class' => 'simul-btn small-btn'
                ]
            ]
        );
    }
}
