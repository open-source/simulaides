<?php

namespace Simulaides\Form\OffresCee;

use Zend\Form\Element;
use Zend\Form\Form;

/**
 * Class OffresCeeForm
 * @package Simulaides\Form\OffresCee
 */
class OffresCeeForm extends Form
{

    public $listeTravauxAvecOffre;

    /**
     * @param null $name
     * @param array $listeTravauxAvecOffre
     */
    public function __construct($name = null, $listeTravauxAvecOffre)
    {
        parent::__construct('travaux', $listeTravauxAvecOffre);
        $this->listeTravauxAvecOffre = $listeTravauxAvecOffre;
    }

    /**
     *
     */
    public function init()
    {
        $this->setAttribute('method', 'post');

        foreach ($this->listeTravauxAvecOffre as $travaux => $offres) {
            $options = [];
            foreach ($offres as $offre) {
                $options[] = [
                    'value'            => $offre->getId(),
                    'label_attributes' => [
                        'class' => 'radio-inline',
                        'for'   => 'bbc-non'
                    ]
                ];
            }

            $this->add(
                [
                    'name' => $travaux,
                    'type' => 'radio',
                    'options' => [
                        'value_options' => $options
                    ]
                ]
            );
        }
    }

}
