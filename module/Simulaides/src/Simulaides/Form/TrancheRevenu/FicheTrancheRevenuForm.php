<?php
namespace Simulaides\Form\TrancheRevenu;

use Simulaides\Domain\Entity\TrancheRevenu;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * Classe FicheTrancheRevenuForm
 * Formulaire pour la fiche d'une tranche de revenu
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\TrancheRevenu
 */
class FicheTrancheRevenuForm extends Form
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        $classObject = new ClassMethods(false);
        $this->setHydrator($classObject);
        $this->setObject(new TrancheRevenu());

        //Code de la tranche
        $this->add(
            [
                'name'       => 'codeTranche',
                'options'    => [
                    'label' => 'Code de la tranche'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control',
                    'id'       => 'codeTranche'
                ]
            ]
        );

        //Intitulé de la tranche
        $this->add(
            [
                'name'       => 'intitule',
                'options'    => [
                    'label' => 'Intitulé de la tranche'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control',
                    'id'       => 'intitule'
                ]
            ]
        );

        //Champ N personne
        for ($i = 1; $i <= 6; $i++) {
            $this->add($this->getElementNPersonne($i));
        }

        //Champ Ajout par personne supplémentaire
        $this->add(
            [
                'name'       => 'mtPersonneSupp',
                'options'    => [
                    'label' => 'Ajout par personne supplémentaire'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Champ code du groupe
        $this->add(
            [
                'name' => 'codeGroupe',
                'type' => 'hidden'
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        //Bouton "Enregistrer"
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'    => 'button',
                    'value'   => 'Enregistrer',
                    'class'   => "small-btn simul-btn pull-right",
                    'onclick' => 'submitModalForm(); return false;'
                ]
            ]
        );

        //Bouton "Annuler"
        $this->add(
            [
                'name'       => 'close',
                'attributes' => [
                    'type'    => 'button',
                    'value'   => 'Annuler',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'closeModalForm(); return false;'
                ]
            ]
        );
    }

    /**
     * Permet de créer un élément text pour les champs N personne
     *
     * @param int $numPersonne numéro de la personne
     *
     * @return array
     */
    private function getElementNPersonne($numPersonne)
    {
        $name = 'mt' . $numPersonne . 'personne';
        if ($numPersonne == 1) {
            $intitule = $numPersonne . ' personne';
        } else {
            $intitule = $numPersonne . ' personnes';
        }

        return [
            'name'       => $name,
            'options'    => [
                'label' => $intitule
            ],
            'attributes' => [
                'required' => true,
                'class'    => 'form-control'
            ]
        ];
    }
}
