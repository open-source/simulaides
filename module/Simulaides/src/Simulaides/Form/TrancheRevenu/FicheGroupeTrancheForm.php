<?php
namespace Simulaides\Form\TrancheRevenu;

use Simulaides\Constante\ModaleConstante;
use Simulaides\Domain\Entity\TrancheGroupe;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * Classe FicheGroupeTrancheForm
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\TrancheRevenu
 */
class FicheGroupeTrancheForm extends Form
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        $classObject = new ClassMethods(false);
        $this->setHydrator($classObject);
        $this->setObject(new TrancheGroupe());

        //Code du groupe
        $this->add(
            [
                'name'       => 'codeGroupe',
                'options'    => [
                    'label' => 'Code du groupe'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Intitulé du groupe
        $this->add(
            [
                'name'       => 'intitule',
                'options'    => [
                    'label' => 'Intitulé du groupe'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        //Bouton "Enregistrer"
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'  => 'button',
                    'value' => 'Enregistrer',
                    'class' => "small-btn simul-btn pull-right",
                    'onclick'    => 'submitModalForm(); return false;'
                ]
            ]
        );

        //Bouton "Annuler"
        $this->add(
            [
                'name'       => 'close',
                'attributes' => [
                    'type'    => 'button',
                    'value'   => 'Annuler',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'closeModalForm(); return false;'
                ]
            ]
        );
    }
}
