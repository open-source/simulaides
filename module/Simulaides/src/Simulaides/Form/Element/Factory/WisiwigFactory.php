<?php
namespace Simulaides\Form\Element\Factory;

use Zend\Form\Element\Textarea;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe WisiwigFactory
 * Element Wisiwig : textArea avec Summernote
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Element\Factory
 */
class WisiwigFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var Textarea $textAreaElement */
        $textAreaElement = $serviceLocator->get('Zend\Form\Element\Textarea');

        $textAreaElement->setAttributes(
            [
                'class' => 'summernote form-control',
                'rows'  => 3
            ]
        );

        return $textAreaElement;
    }
}
