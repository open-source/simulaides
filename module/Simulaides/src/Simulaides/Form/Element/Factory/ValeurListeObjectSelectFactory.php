<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 16/02/15
 * Time: 14:15
 */

namespace Simulaides\Form\Element\Factory;

use DoctrineModule\Form\Element\ObjectSelect;
use Zend\Form\FormElementManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ValeurListeObjectSelectFactory
 * @package Simulaides\Form\Element\Factory
 */
class ValeurListeObjectSelectFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var ObjectSelect $objectSelectElement */
        $objectSelectElement = $serviceLocator->get('DoctrineModule\Form\Element\ObjectSelect');

        $objectSelectElement->setOptions(
            [
                'target_class' => 'Simulaides\Domain\Entity\ValeurListe',
                'property'     => 'libelle',
                'is_method'    => true
            ]
        );

        return $objectSelectElement;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceManager(ServiceLocatorInterface $serviceLocator)
    {
        if (!$serviceLocator instanceof FormElementManager) {
            return $serviceLocator;
        }

        return $serviceLocator->getServiceLocator();

    }
}
