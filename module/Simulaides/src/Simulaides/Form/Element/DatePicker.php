<?php
namespace Simulaides\Form\Element;

use Simulaides\Constante\MainConstante;
use Zend\Form\Element;

/**
 * Classe DatePicker
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Element
 */
class DatePicker extends Element
{

    /**
     * Constructeur
     */
    public function __construct()
    {
        $this->setAttribute('type', 'datepicker');
        $this->setAttribute('class', 'date-picker');
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        if (parent::getValue() instanceof \DateTime) {
            return parent::getValue()->format(MainConstante::FORMAT_DATE_DEFAUT);
        } else {
            return parent::getValue();
        }
    }
}
