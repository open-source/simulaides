<?php

namespace Simulaides\Form\Comparatif;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\Digits;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

/**
 * Classe AccesComparatifForm
 *
 * Projet : ADEME Simul'Aides
 *
 * @author
 */
class AccesComparatifForm extends Form implements InputFilterProviderInterface
{

    /**
     * Nom du champ "Numéro du comparatif"
     */
    const CHAMP_NUM_COMPARATIF = 'idComparatif';

    /**
     * Nom du champ "Code d'accès"
     */
    const CHAMP_CODE_ACCES = 'motPasse';

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        //Numéro du comparatif
        $this->add(
            [
                'name'       => self::CHAMP_NUM_COMPARATIF,
                'options'    => [
                    'label' => 'Numéro du comparatif'
                ],
                'attributes' => [
                    'id'       => self::CHAMP_NUM_COMPARATIF,
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Code d'accès
        $this->add(
            [
                'name'       => self::CHAMP_CODE_ACCES,
                'type'       => 'Zend\Form\Element\Password',
                'options'    => [
                    'label' => "Code d'accès"
                ],
                'attributes' => [
                    'id'       => self::CHAMP_CODE_ACCES,
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Bouton Valider
        $this->add(
            [
                'name'       => 'btnValide',
                'attributes' => [
                    'type'  => 'submit',
                    'value' => 'Valider',
                    'class' => "simul-btn small-btn",
                ]
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function getInputFilterSpecification()
    {
        return [
            //Numéro du comparatif
            self::CHAMP_NUM_COMPARATIF => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Le numéro du comparatif ne peut être vide'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Digits',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Digits::NOT_DIGITS => 'Le numéro du comparatif ne doit contenir que des chiffres'
                            ]
                        ],
                    ],
                    [
                        'name'                   => 'StringLength',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'max'      => 10,
                            'messages' => [
                                StringLength::TOO_LONG => 'Le numéro du comparatif ne doit pas dépasser %max% chiffres'
                            ]
                        ]
                    ]
                ]
            ],
            //Code d'accès
            self::CHAMP_CODE_ACCES     => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Le code d'accès ne peut être vide"
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
