<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 14:09
 */

namespace Simulaides\Form\Caracteristique;

use Zend\Form\Element;
use Zend\Form\Form;

/**
 * Class CaracteristiqueForm
 * @package Simulaides\Form\Caracteristique
 */
class CaracteristiqueForm extends Form
{
    /**
     *
     */
    public function init()
    {
        $this->setAttribute('method', 'post');

        /** LOGEMENT */

        /** Localisation (région / Département / Commune */
        $this->add(
            [
                'name' => 'localisation',
                'type' => 'LocalisationFieldset',

            ]
        );

        /** Toutes les informations Logement et Foyer du projet */
        $this->add(
            [
                'name' => 'projet',
                'type' => 'ProjetFieldset',
            ]
        );

        $this->add(
            [
                'type' => 'Zend\Form\Element\Hidden',
                'name' => 'codeCommune',
            ]
        );

        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );
    }
}
