<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 17/02/15
 * Time: 16:16
 */

namespace Simulaides\Form\Caracteristique;

use Simulaides\Constante\SessionConstante;
use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Session\Container;
use Zend\Validator\NotEmpty;
use Zend\I18n\Validator\Int;
use Zend\Validator\StringLength;

/**
 * Class LocalisationFieldset
 * @package Simulaides\Form\Caracteristique
 */
class LocalisationFieldset extends Fieldset implements InputFilterProviderInterface
{
    /**
     * Message d'erreur quand l'input n'est pas un chiffre
     */
    const NOT_NUMBER = "Ce champ doit être un entier";

    /**
     * Message d'erreur quand l'input n'est pas renseigné
     */
    const IS_EMPTY = "Ce champ ne peut être vide";

    /**
     * @var bool
     */
    private $jsActive;

    /**
     *
     */
    public function init()
    {
        parent::init();

        //Récupération de l'information du javascript actif ou non
        $jscontainer    = new Container(SessionConstante::CONTAINER_NAME);
        $this->jsActive = $jscontainer->offsetGet(SessionConstante::JS_ACTIVE);

        //Liste des régions
        $this->add(
            [
                'name'       => 'codeRegion',
                'type'       => 'Zend\Form\Element\Hidden',
                /*'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => 'Région',
                    'required'     => true,
                    'empty_option' => 'Choisissez votre région',
                    'target_class' => 'Simulaides\Domain\Entity\Region',
                    'property'     => 'nom',
                    'is_method'    => true,
                    'find_method'  => [
                        'name' => 'getListeRegions',
                    ],
                ],*/
                'attributes' => [
                    'id'    => 'select-region',
                    //'class' => 'form-control change-select',
                ]
            ]
        );
        //Bouton d'ajout de région (si js non actif)
        /*if (!$this->jsActive) {
            $this->add(
                [
                    'name'       => 'submitRegion',
                    'attributes' => [
                        'type'  => 'submit',
                        'value' => 'Ajouter',
                        'id'    => 'add-region',
                        'class' => 'small-btn simul-btn'
                    ]
                ]
            );
        }*/

        //Liste des départements
        /*$this->add(
            [
                'name'       => 'codeDepartement',
                'type'       => 'Select',
                'options'    => [
                    'label'                     => 'Département',
                    'disable_inarray_validator' => true,
                    'required'                  => true
                ],
                'attributes' => [
                    'id'               => 'select-departement',
                    'class'            => 'form-control change-select',
                    'data-placeholder' => "Choisissez votre département",

                ]
            ]
        );
        //Bouton d'ajout de département (si js non actif)
        if (!$this->jsActive) {
            $this->add(
                [
                    'name'       => 'submitDept',
                    'attributes' => [
                        'type'  => 'submit',
                        'value' => 'Ajouter',
                        'id'    => 'add-departement',
                        'class' => 'small-btn simul-btn'
                    ]
                ]
            );
        }*/

        //Champs de code postal
        $this->add(
            [
                'name'       => 'codePostal',
                'options'    => [
                    'label'                     => 'Code postal',
                    'required'                  => false
                ],
                'attributes' => [
                    'id'        => 'text-codePostal',
                    'class'     => 'form-control',
                    'maxlength' => 5
                ]
            ]
        );
        //Bouton d'ajout de code postal (si js non actif)
        if (!$this->jsActive) {
            $this->add(
                [
                    'name'       => 'submitCodePostal',
                    'attributes' => [
                        'type'  => 'submit',
                        'value' => 'Filtrer',
                        'id'    => 'add-code-postal',
                        'class' => 'small-btn simul-btn'
                    ]
                ]
            );
        }



        $this->add(
            [
                'name'       => 'codeCommune',
                'type'       => 'Select',
                'options'    => [
                    'label'                     => 'Commune',
                    'disable_inarray_validator' => true,
                    'required'                  => true
                ],
                'attributes' => [
                    'id'               => 'select-commune',
                    'class'            => 'form-control',
                    'data-placeholder' => "Choisissez votre commune",
                ]

            ]
        );

    }


    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'codeCommune'     => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Ce champ ne peut être vide'
                            ]
                        ]
                    ],
                ]
            ],
            /*'codeRegion'      => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Ce champ ne peut être vide'
                            ]
                        ]
                    ]
                ]
            ],
            'codeDepartement' => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Ce champ ne peut être vide'
                            ]
                        ]
                    ]
                ]
            ]*/
        ];
    }
}
