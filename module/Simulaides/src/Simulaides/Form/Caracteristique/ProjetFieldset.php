<?php
namespace Simulaides\Form\Caracteristique;

use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Tools\Utils;
use Zend\Form\Fieldset;
use Zend\I18n\Validator\Float;
use Zend\I18n\Validator\Int;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\Between;
use Zend\Validator\GreaterThan;
use Zend\Validator\LessThan;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

/**
 * Classe ProjetFieldset
 * Fieldset sur le projet
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Caracteristique
 */
class ProjetFieldset extends Fieldset implements InputFilterProviderInterface
{
    /**
     * Message d'erreur quand l'input n'est pas un chiffre
     */
    const NOT_NUMBER = "Ce champ doit être un entier";
    /**
     * Message d'erreur quand l'input n'est pas un décimal
     */
    const NOT_DECIMAL = "Ce champ doit être un décimal";

    /**
     * Message d'erreur quand l'input n'est pas renseigné
     */
    const IS_EMPTY = "Ce champ ne peut être vide";

    /**
     * Initialisation du Fieldset
     */
    public function init()
    {
        parent::init();

        /** LOGEMENT */

        // Type de logement
        $this->add(
            [
                'name'       => 'codeTypeLogement',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => 'M'
                ]
            ]
        );

        //Année de construction
        $this->add(
            [
                'name'       => 'anneeConstruction',
                'options'    => [
                    'label'    => 'Année de construction (AAAA)',
                    'required' => true
                ],
                'attributes' => [
                    'id'    => 'anneeConstruction',
                    'class' => 'form-control'
                ]
            ]
        );

        //Surface habitable
        $this->add(
            [
                'name'       => 'surfaceHabitable',
                'options'    => [
                    'label'    => 'Surface Habitable (m²)',
                    'required' => true
                ],
                'attributes' => [
                    'id'    => 'surfaceHabitable',
                    'class' => 'form-control'
                ]
            ]
        );

        //energie de chauffage
        $this->add(
            [
                'name'       => 'codeEnergie',
                'type'       => 'ValeurListeObjectSelect',
                'options'    => [
                    'label'        => 'Energie de chauffage principale',
                    'required'     => true,
                    'empty_option' => 'Choisissez votre énergie',
                    'find_method'  => [
                        'name'   => 'getListeValuesByCodeListe',
                        'params' => [
                            'codeListe' => ValeurListeConstante::LISTE_ENERGIE_CHAUFFAGE_DETAIL
                        ]
                    ],
                ],
                'attributes' => [
                    'id'               => 'codeEnergie',
                    'class'            => 'form-control',
                    'data-placeholder' => " ",
                ]
            ]
        );

        //Mode de chauffage
        $this->add(
            [
                'name'       => 'codeModeChauff',
                'type'       => 'ValeurListeObjectSelect',
                'options'    => [
                    'label'        => 'Mode de chauffage',
                    'required'     => true,
                    'empty_option' => 'Choisissez votre mode de chauffage ',
                    'find_method'  => [
                        'name'   => 'getListeValuesByCodeListe',
                        'params' => [
                            'codeListe' => ValeurListeConstante::L_MODE_CHAUFFAGE
                        ]
                    ],
                ],
                'attributes' => [
                    'id'               => 'codeModeChauff',
                    'class'            => 'form-control',
                    'data-placeholder' => " ",
                ]
            ]
        );

        /** FOYER */

        //Nombre d'adulte
        $this->add(
            [
                'name'       => "nbAdultes",
                'options'    => [
                    'label'    => "Nombre d'adultes",
                    'required' => true
                ],
                'attributes' => [
                    'id'    => "nbAdultes",
                    'class' => 'form-control'
                ]
            ]
        );

        //Nombre de personne à charge
        $this->add(
            [
                'name'       => "nbPersonnesCharge",
                'options'    => [
                    'label'    => "Nombre de personnes à charge",
                    'required' => true
                ],
                'attributes' => [
                    'id'    => "nbPersonnesCharge",
                    'class' => 'form-control'
                ]
            ]
        );

        //Nombre d'enfant en garde alternée
        $this->add(
            [
                'name'       => "nbEnfantAlterne",
                'options'    => [
                    'label'    => "Nombre d'enfants en garde alternée",
                    'required' => true
                ],
                'attributes' => [
                    'id'    => "nbEnfantAlterne",
                    'class' => 'form-control'
                ]
            ]
        );

        //Nombre de parts fiscales
        $this->add(
            [
                'name'       => 'nbPartsFiscales',
                'options'    => [
                    'label'         => "Nombre de parts fiscales",
                    'required'      => true
                ],
                'attributes' => [
                    'id'    => 'nbPartsFiscales',
                    'class' => 'form-control',
                    'placeholder' => '0,0'
                ]
            ]
        );

        // Statut logement
        $this->add(
            [
                'name'       => 'codeStatut',
                'type'       => 'ValeurListeObjectSelect',
                'options'    => [
                    'label'        => 'Statut',
                    'required'     => true,
                    'empty_option' => 'Choisissez votre statut',
                    'find_method'  => [
                        'name' => 'getListeStatutDemandeur',
                    ],
                ],
                'attributes' => [
                    'id'               => 'codeStatut',
                    'class'            => 'form-control',
                    'data-placeholder' => " ",
                ]
            ]
        );

        //Primo-accédant
        $this->add(
            [
                'name'       => 'primoAccession',
                'type'       => 'radio',
                'options'    => [
                    'required'     => true,
                    'label_attributes' => [
                        'class' => 'radio-inline'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ],
                'attributes' => [
                    'id' => 'primoAccesssion',
                ]
            ]
        );
        //Salarié du secteur privé
        $this->add(
            [
                'name'       => 'salarieSecteurPrive',
                'type'       => 'radio',
                'options'    => [
                    'required'     => true,
                    'label_attributes' => [
                        'class' => 'radio-inline'
                    ],
                    'value_options'    => Utils::getListeOuiNon()
                ],
                'attributes' => [
                    'id' => 'salarieSecteurPrive',
                ]
            ]
        );
        // Revenu fiscal de référence
        $this->add(
            [
                'name'       => 'revenus',
                'options'    => [
                    'label'    => 'Revenu fiscal de référence en €',
                    'required' => true
                ],
                'attributes' => [
                    'id'    => 'revenus',
                    'class' => 'form-control'
                ]
            ]
        );

        //Bénéficie d'un taux à taux 0
        $this->add(
            [
                'name'       => 'beneficePtz',
                'type'       => 'radio',
                'options'    => [
                    'label_attributes' => [
                        'class' => 'radio-inline'
                    ],
                    'value_options'    => Utils::getListeOuiNon(),

                ],
                'attributes' => [
                    'id' => 'beneficePtz',
                ]
            ]
        );

        //Montant des travaux ayant déjà bénéficié du prêt à taux 0
        /*$this->add(
            [
                'name'       => 'montantBeneficeCi',
                'options'    => [
                    'label'    => "Montant des travaux ayant déjà bénéficié d'un Crédit d'Impôt",
                    'required' => true
                ],
                'attributes' => [
                    'id'    => 'montantBeneficeCi',
                    'class' => 'form-control',
                ]
            ]
        );*/
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'codeTypeLogement'  => [
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ]
                ]
            ],
            'revenus'           => [
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Int',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Int::NOT_INT => self::NOT_NUMBER
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'GreaterThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => 0,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Ce nombre doit etre positif.'
                            ]
                        ]
                    ]
                ],
            ],
            'nbAdultes'         => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Int',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Int::NOT_INT => self::NOT_NUMBER
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'GreaterThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => 0,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Ce nombre doit etre positif.'
                            ]
                        ]
                    ]
                ]
            ],
            'nbPersonnesCharge' => [
                'break_chain_on_failure' => true,
                'required'               => true,
                'validators'             => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Int',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Int::NOT_INT => self::NOT_NUMBER
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Between',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => 0,
                            'max'      => 20,
                            'messages' => [
                                Between::NOT_BETWEEN => "Ce nombre doit être compris en %min% et %max%"
                            ]
                        ]
                    ]
                ]
            ],
            'nbEnfantAlterne'   => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Int',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Int::NOT_INT => self::NOT_NUMBER
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Between',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => 0,
                            'max'      => 20,
                            'messages' => [
                                Between::NOT_BETWEEN => "Ce nombre doit être compris en %min% et %max%"
                            ]
                        ]
                    ]
                ]
            ],
            'anneeConstruction' => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Int',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Int::NOT_INT => self::NOT_NUMBER
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'StringLength',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => 4,
                            'max'      => 4,
                            'messages' => [
                                StringLength::TOO_LONG  => "L'année doit être sur 4 chiffres",
                                StringLength::TOO_SHORT => "L'année doit être sur 4 chiffres"
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'LessThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'max'       => date('Y', time()),
                            'inclusive' => true,
                            'messages'  => [
                                LessThan::NOT_LESS_INCLUSIVE => "L'année doit être inférieure où égale %max%"
                            ]
                        ]
                    ]
                ]
            ],
            'codeEnergie'       => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                ]
            ],
            'nbPartsFiscales'   => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Float',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'locale'   => 'en_US',
                            'messages' => [
                                Float::NOT_FLOAT => self::NOT_DECIMAL
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'GreaterThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => 0,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Ce nombre doit supérieur à %min%.'
                            ]
                        ]
                    ]
                ],
                'filters'    => [
                    [
                        'name'    => 'NumberFormat',
                        'options' => [
                            'locale' => 'fr_FR'
                        ]
                    ]
                ]

            ],
            'codeStatut'        => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                ]
            ],
            'surfaceHabitable'  => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Int',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Int::NOT_INT => self::NOT_NUMBER
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'GreaterThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => 0,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Ce nombre doit supérieur à %min%.'
                            ]
                        ]
                    ]
                ]
            ],
            /*'montantBeneficeCi' => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Int',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Int::NOT_INT => self::NOT_NUMBER
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'GreaterThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => -1,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Ce nombre doit supérieur à %min%.'
                            ]
                        ]
                    ]
                ]
            ],*/
            'codeModeChauff' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => self::IS_EMPTY
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
