<?php
namespace Simulaides\Form\Simulation;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\Digits;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

/**
 * Classe AccesSimulationForm
 * Formulaire pour l'accès à une simulation
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Simulation
 */
class AccesSimulationForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Numéro de la simulation
        $this->add(
            [
                'name'       => 'numSimulation',
                'options'    => [
                    'label' => 'Numéro de la simulation',
                    'label_attributes' => array(
                        'class' => 'label-accueil',
                    ),
                ],
                'attributes' => [
                    'id'       => 'numSimulation',
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Code d'accès
        $this->add(
            [
                'name'       => 'codeAcces',
                'type'       => 'Zend\Form\Element\Password',
                'options'    => [
                    'label' => "Code d'accès",
                    'label_attributes' => array(
                        'class' => 'label-accueil',
                    ),
                ],
                'attributes' => [
                    'id'       => 'codeAcces',
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        //Bouton Valider
        $this->add(
            [
                'name'       => 'btnValide',
                'id'         => 'btnValide',
                'attributes' => [
                    'type'  => 'submit',
                    'value' => 'Valider',
                    'class' => "simul-btn small-btn",
                ]
            ]
        );
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            //Numéro de la simulation
            'numSimulation' => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Le numéro de la simulation ne peut être vide'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'Digits',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Digits::NOT_DIGITS => 'Le numéro de simulation ne doit contenir que des chiffres'
                            ]
                        ],
                    ],
                    [
                        'name'                   => 'StringLength',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'max'      => 10,
                            'messages' => [
                                StringLength::TOO_LONG => 'Le numéro de simulation ne doit pas dépasser %max% chiffres'
                            ]
                        ]
                    ]
                ]
            ],
            //Code d'accès
            'codeAcces'     => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Le code d'accès ne peut être vide"
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
