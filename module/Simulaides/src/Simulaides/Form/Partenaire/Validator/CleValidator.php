<?php
namespace Simulaides\Form\Partenaire\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Classe Clevalidator
 * Validateur pour que la clé IFrame ou la clé API soient l'une ou l'autre renseignée
 *
 */
class CleValidator extends AbstractValidator
{

    /** Clé vide */
    const EMPTY_KEY = 'EMPTY_KEY';

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::EMPTY_KEY => "Ce champ ne peut être vide",
    ];

    /**
     * @param mixed $value
     * @param array $context
     *
     * @return bool
     */
    public function isValid($value, $context = [])
    {

        $cleIFrame = $context['cle'];
        $cleApi = $context['cleApi'];
        if (empty($cleIFrame) && empty($cleApi)) {
            $this->error(self::EMPTY_KEY);
            return false;
        }

        return true;
    }
}
