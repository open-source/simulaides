<?php
namespace Simulaides\Form\Partenaire;

use Doctrine\ORM\Internal\Hydration\ObjectHydrator;
use Simulaides\Constante\ModaleConstante;
use Simulaides\Domain\Entity\Partenaire;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Validator\NotEmpty;

/**
 * Classe PartenaireForm
 *
 * @package   Simulaides\Form\Partenaire
 */
class PartenaireForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //$this->setObject(new Partenaire());

        $this->add(
            [
                'name' => 'id',
                'type' => 'Hidden'
            ]
        );

        //Url
        $this->add(
            [
                'name'       => 'url',
                'options'    => [
                    'label' => 'Nom de domaine'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Nom
        $this->add(
            [
                'name'       => 'nom',
                'options'    => [
                    'label' => 'Nom du partenaire'
                ],
                'attributes' => [
                    'required' => false,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Email
        $this->add(
            [
                'name'       => 'email',
                'options'    => [
                    'label' => 'Email de contact du partenaire'
                ],
                'attributes' => [
                    'required' => false,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Clé IFrame
        $this->add(
            [
                'name'       => 'cle',
                'options'    => [
                    'label' => 'Clé IFrame'
                ],
                'attributes' => [
                    'required' => false,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Clé Api
        $this->add(
            [
                'name'       => 'cleApi',
                'options'    => [
                    'label' => 'Clé API'
                ],
                'attributes' => [
                    'required' => false,
                    'class'    => 'form-control'
                ]
            ]
        );

        //Région
        $this->add(
            [
                'name'       => 'region',
                'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
                'options'    => [
                    'label'        => 'Région',
                    'required'     => false,
                    'empty_option' => 'National',
                    'allow_empty' => true,
                    'target_class' => 'Simulaides\Domain\Entity\Region',
                    'property'     => 'nom',
                    'is_method'    => true,
                    'unselected_value' => '',
                    'find_method'  => [
                        'name' => 'getListeRegions',
                    ]
                ],
                'attributes' => [
                    'class' => 'chosen-select change-select form-control'
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        //Bouton "Enregistrer"
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'  => 'button',
                    'value' => 'Enregistrer',
                    'class' => "small-btn simul-btn pull-right",
                    'onclick'    => '_submitModalePartenaire(); return false;'
                ]
            ]
        );

        //Bouton "Annuler"
        $this->add(
            [
                'name'       => 'close',
                'attributes' => [
                    'type'    => 'button',
                    'value'   => 'Annuler',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'closeModalForm(); return false;'
                ]
            ]
        );
    }

    public function getInputFilterSpecification() {
        return [
            'region'   => [
                'required'    => false,
                'allow_empty' => true,
            ],
            'cle'   => [
                'required'    => false,
                'allow_empty' => false,
                'continue_if_empty' => true,
                'validators' => [
                    [
                        'name'                   => 'Simulaides\Form\Partenaire\Validator\CleValidator',
                        'break_chain_on_failure' => true,
                    ]                
                ]
            ],
            'cleApi'   => [
                'required'    => false,
                'allow_empty' => false,
                'continue_if_empty' => true,
                'validators' => [
                    [
                        'name'                   => 'Simulaides\Form\Partenaire\Validator\CleValidator',
                        'break_chain_on_failure' => true,
                    ]            
                ]
            ],
            'url'   => [
                'required'    => true,
                'allow_empty' => false,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Ce champ ne peut être vide'
                            ]
                        ]
                    ],
                ]
            ],

        ];
    }
}
