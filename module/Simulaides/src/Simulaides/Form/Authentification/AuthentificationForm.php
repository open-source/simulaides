<?php
namespace Simulaides\Form\Authentification;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\NotEmpty;

/**
 * Classe AuthentificationForm
 * Formulaire d'authentification pour la partie conseiller
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Authentification
 */
class AuthentificationForm extends Form implements InputFilterProviderInterface
{

    public function init()
    {
        parent::init();

        // Login
        $this->add(
            [
                'name'       => 'login',
                'options'    => [
                    'label'    => 'Login',
                    'required' => true,
                ],
                'attributes' => [
                    'class' => 'form-control'
                ]
            ]
        );

        // Mot de passe
        $this->add(
            [
                'name'       => 'motPasse',
                'type'       => 'Zend\Form\Element\Password',
                'options'    => [
                    'label'    => 'Mot de passe',
                    'required' => true,
                ],
                'attributes' => [
                    'class' => 'form-control'
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        //Bouton "Valider"
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'  => 'submit',
                    'value' => 'Valider',
                    'class' => "small-btn simul-btn pull-right"
                ]
            ]
        );
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'login'    => [
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'La saisie du login est obligatoire'
                            ]
                        ]
                    ]
                ]
            ],
            'motPasse' => [
                'allow_empty' => false,
                'required'    => true,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'La saisie du mot de passe est obligatoire'
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }
}
