<?php
namespace Simulaides\Form\Contenu;

use Simulaides\Constante\ValeurListeConstante;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\NotEmpty;

/**
 * Classe ContenuParamForm
 * Formulaire de paramétrage des contenus
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Contenu
 */
class ContenuParamForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Liste des contenus
        $this->add(
            [
                'name'       => 'code',
                'type'       => 'ValeurListeObjectSelect',
                'options'    => [
                    'label'                     => 'Contenu',
                    'empty_option'              => '',
                    'disable_inarray_validator' => true,
                    'find_method'               => [
                        'name'   => 'getListeValuesByCodeListe',
                        'params' => [
                            'codeListe' => ValeurListeConstante::TEXT_ADMINISTRABLE
                        ]
                    ],
                ],
                'attributes' => [
                    'class'            => 'chosen-select change-select form-control',
                    'data-placeholder' => " "
                ]
            ]
        );

        //Saisie du contenu en wisiwig
        $this->add(
            [
                'name'       => 'descriptif',
                'type'       => 'WisiwigElement',
                'attributes' => [
                    'id' => 'descriptif'
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );

        //Bouton Enregistrer
        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'    => 'submit',
                    'value'   => 'Enregistrer',
                    'class'   => "small-btn simul-btn pull-left",
                    'onclick' => 'submitFormContenu(); return false;'
                ]
            ]
        );
    }

    /**
     * Permet de retourner les validateurs sur les éléments
     * du formulaire
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'code' => [
                'required'   => true,
                'validators' => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Le contenu est obligatoire'
                            ]
                        ]
                    ],
                ]
            ]
        ];
    }
}
