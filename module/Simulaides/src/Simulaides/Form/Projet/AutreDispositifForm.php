<?php
namespace Simulaides\Form\Projet;

use Zend\Form\Form;
use Zend\I18n\Validator\Float;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\GreaterThan;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

/**
 * Classe AutreDispositifForm
 * Formulaire de saisie des infos d'un autre dispositif dans le résultat de la simulation
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Projet
 */
class AutreDispositifForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Nom du dispositif
        $this->add(
            [
                'name'       => 'intituleAideConseiller',
                'options'    => [
                    'label' => 'Nom du dispositif',
                ],
                'attributes' => [
                    'id'       => 'intituleAideConseiller',
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Montant de l'aide
        $this->add(
            [
                'name'       => 'mtAideConseiller',
                'options'    => [
                    'label' => "Montant de l'aide",

                ],
                'attributes' => [
                    'id'       => 'mtAideConseiller',
                    'class'    => 'form-control',
                    'required' => true
                ]
            ]
        );

        //Commentaire
        $this->add(
            [
                'name'       => 'descriptifAideConseiller',
                'type'       => 'textarea',
                'options'    => [
                    'label' => 'Commentaire'
                ],
                'attributes' => [
                    'id'       => 'descriptifAideConseiller',
                    'class'    => 'form-control',
                    'required' => true,
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrfDispo'
            ]
        );


        //Bouton Ajouter
        $this->add(
            [
                'name'       => 'ajoutDispositif',
                'attributes' => [
                    'type'    => 'submit',
                    'title'   => 'Ajouter',
                    'value'   => "Ajouter",
                    'class'   => "small-btn simul-btn pull-right",
                    'onclick' => 'submitFormAutreDispositif(); return false;'
                ]
            ]
        );
    }

    /**
     * Permet de retourner les validateurs sur les éléments
     * du formulaire
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            //Nom dispositif
            'intituleAideConseiller'   => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'max'      => 250,
                            'messages' => [
                                StringLength::TOO_LONG => 'Le nom du dispositif ne doit pas dépasser %max% caractères.'
                            ]
                        ]
                    ],
                    [
                        'name'    => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Veuillez renseigner le nom'
                            ]
                        ]
                    ]
                ]
            ],
            //Montant dispositif
            'mtAideConseiller'         => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'Float',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'locale'   => 'fr_FR',
                            'messages' => [
                                Float::NOT_FLOAT => 'Ce nombre doit être un décimal'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Veuillez renseigner le nom'
                            ]
                        ]
                    ],
                    [
                        'name'                   => 'GreaterThan',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'min'      => 0,
                            'messages' => [
                                GreaterThan::NOT_GREATER => 'Le montant doit être supérieur à %min%'
                            ]
                        ]
                    ]
                ]
            ],
            //Commentaire
            'descriptifAideConseiller' => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Veuillez renseigner le commentaire'
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }
}
