<?php
namespace Simulaides\Form\Projet;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\EmailAddress;
use Zend\Validator\NotEmpty;

/**
 * Classe ResultatPDFForm
 * Formulaire pour la génération du résultat en PDF
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Projet
 */
class ResultatPDFForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Email
        $this->add(
            [
                'name'       => 'email',
                'options'    => [
                    'label'    => "Recevoir les codes par mail",
                    'required' => true
                ],
                'attributes' => [
                    'id'    => 'email',
                    'class' => 'form-control',
                    'onkeyup' => 'disableInput()'
                ]
            ]
        );

        //champ cahcé Url téléchargement PDF
        $this->add(
            [
                'name'       => 'urlPDF',
                'type'       => 'hidden',
                'attributes' => [
                    'id' => 'urlPDF'
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrfPDF'
            ]
        );


        //Bouton Envoyer
        $this->add(
            [
                'name'       => 'envoyer',
                'attributes' => [
                    'type'  => 'submit',
                    'title' => 'Envoyer l\'email',
                    'value' => "Envoyer",
                    'class' => "small-btn simul-btn ",
                ]
            ]
        );

        //Bouton Télécharger le PDF
        /*$this->add(
            [
                'name'       => 'telecharger',
                'attributes' => [
                    'type'  => 'submit',
                    'title' => 'Télécharger la synthèse (PDF)',
                    'value' => "Télécharger la synthèse (PDF)",
                    'class' => "small-btn simul-btn acces-submit",
                ]
            ]
        );*/
    }

    /**
     * Permet de retourner les validateurs sur les éléments
     * du formulaire
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            //email
            'email' => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'                   => 'NotEmpty',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Veuillez renseigner l'e-mail"
                            ]
                        ]
                    ],
                    [
                        'name'    => 'EmailAddress',
                        'options' => [
                            'messages' => [
                                EmailAddress::INVALID_FORMAT => "L'email saisi n'est pas un email valide."
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
