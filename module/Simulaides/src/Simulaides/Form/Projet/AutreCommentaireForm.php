<?php
namespace Simulaides\Form\Projet;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\NotEmpty;

/**
 * Classe AutreCommentaireForm
 * Formulaire de saisie "Autre commentaire" dans le résultat de la simulation
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Projet
 */
class AutreCommentaireForm extends Form implements InputFilterProviderInterface
{
    /**
     * Initialisation
     */
    public function init()
    {
        parent::init();

        //Commentaire
        $this->add(
            [
                'name'       => 'autreCommentaireConseiller',
                'type'       => 'textarea',
                'options'    => [
                    'label' => 'Commentaire'
                ],
                'attributes' => [
                    'id'       => 'autreCommentaireConseiller',
                    'class'    => 'form-control',
                    'required' => true,
                ]
            ]
        );

        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrfComm'
            ]
        );

        //Bouton Ajouter
        $this->add(
            [
                'name'       => 'ajoutCommentaire',
                'attributes' => [
                    'type'    => 'submit',
                    'title'   => "Ajouter",
                    'value'   => "Ajouter",
                    'class'   => "small-btn simul-btn pull-right",
                    'onclick' => 'submitFormAutreCommentaire(); return false;'
                ]
            ]
        );
    }

    /**
     * Permet de retourner les validateurs sur les éléments
     * du formulaire
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            //Commentaire
            'autreCommentaireConseiller' => [
                'required'    => true,
                'allow_empty' => false,
                'validators'  => [
                    [
                        'name'    => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => 'Veuillez renseigner le commentaire'
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }
}
