<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 18:24
 */

namespace Simulaides\Form\Projet;

use Zend\Form\Element;
use Zend\Form\Form;

/**
 * Class AccessProjetForm
 * @package Simulaides\Form\Projet
 */
class AccessProjetForm extends Form
{
    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->add(
            [
                'name'       => 'numero',
                'options'    => [
                    'label' => 'Numéro de la simulation'
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        $this->add(
            [
                'name'       => 'motPasse',
                'type'       => 'Zend\Form\Element\Password',
                'options'    => [
                    'label' => "Code d'accès"
                ],
                'attributes' => [
                    'required' => true,
                    'class'    => 'form-control'
                ]
            ]
        );

        $this->add(
            [
                'name'       => 'submit',
                'attributes' => [
                    'type'  => 'submit',
                    'title' => 'Valider',
                    'value' => 'Valider',
                    'class' => "small-btn simul-btn pull-right"
                ]
            ]
        );
    }
}
