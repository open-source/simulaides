<?php
namespace Simulaides\Form\Accueil;

use Simulaides\Components\MathCaptcha;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\Digits;

/**
 * Classe ConditionGeneraleForm
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Form\Accueil
 */
class ConditionGeneraleForm extends Form implements InputFilterProviderInterface
{
    public function init()
    {
        parent::init();
        $tabConfig = $this->getFormFactory()->getFormElementManager()->getServiceLocator()->get('ApplicationConfig');

        //Captcha
        $this->add(
            [
                'name'    => 'matchCaptcha',
                'type'    => 'captcha',
                'options' => [
                    'captcha' => [
                        'class' => 'Simulaides\Components\MathCaptcha',
                        'options' => [
                            'publicKey'     => $tabConfig['recaptcha-keys']['public'],
                            'privateKey'    => $tabConfig['recaptcha-keys']['private']
                        ]
                    ]
                ]
            ]
        );

        //Case à cocher "J'accepte les CGU"
       /*$this->add(
            [
                'name'       => 'cgu',
                'type'       => 'Zend\Form\Element\Checkbox',
                'options'    => [
                    'label'              => "J'accepte les conditions générales d'utilisation",
                    'required'           => true,
                    'use_hidden_element' => true,
                    'checked_value'      => 1,
                    'unchecked_value'    => 'no'
                ],
                'attributes' => [
                    'id'       => 'cgu',
                    'required' => true,
                ]
            ]
        );*/

        //Bouton "Réaliser une simulation"
        $this->add(
            [
                'name'       => 'start_simul',
                'attributes' => [
                    'id'    => "start_simul",
                    'type'  => 'hidden',
                    'value' => 1
                ]
            ]
        );

        $this->add(
            [
                'name'       => 'codeTypeLogement',
                'attributes' => [
                    'id'    => "codeTypeLogement",
                    'type'  => 'hidden',
                    'value' => ''
                ]
            ]
        );


        //Element de sécurité des formulaires
        $this->add(
            [
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf'
            ]
        );
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'cgu' => [
                'validators' => [
                    [
                        'name'                   => 'Digits',
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'messages' => [
                                Digits::NOT_DIGITS => 'Vous devez valider les conditions générales
                                d’utilisation pour réaliser une simulation'
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }
}
