<?php
namespace Simulaides\Components;

use Zend\Captcha\AbstractWord;

/**
 * Classe MathCaptcha
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Components
 */
class MathCaptcha extends AbstractWord
{
    /**
     * @var string
     */
    private $publicKey;

    /**
     * @var string
     */
    private $privateKey;

    /**
     * Error messages
     * @var array
     */
    protected $messageTemplates = array(
        self::BAD_CAPTCHA   => 'Vous êtes un robot',
    );
    /**
     * Constructeur
     *
     * @param null $options
     */
    public function __construct($options = null)
    {
        parent::__construct($options);
        $this->publicKey = isset($options['publicKey'])?$options['publicKey']:'';
        $this->privateKey = isset($options['privateKey'])?$options['privateKey']:'';
    }

    /**
     * @return string
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * @param string $publicKey
     */
    public function setPublicKey($publicKey)
    {
        $this->publicKey = $publicKey;
    }


    /**
     * @return string
     */
    public function getPrivateKey()
    {
        return $this->publicKey;
    }

    /**
     * @param string $privateKey
     */
    public function setPrivateKey($privateKey)
    {
        $this->publicKey = $privateKey;
    }

    /**
     * Permet de valider le captcha
     *
     * @param mixed $value
     * @param null  $context
     *
     * @return bool|void
     */
    public function isValid($value, $context = null)
    {
        if (!is_array($value)) {
            if (!is_array($context)) {
                $this->error(self::MISSING_VALUE);
                return false;
            }
            $value = $context;
        }

        $name = $this->getName();

        if (isset($value[$name])) {
            $value = $value[$name];
        }

        if (!isset($value['g-recaptcha-response'])) {
            $this->error(self::BAD_CAPTCHA);
            return false;
        }

        $response = $value['g-recaptcha-response'];

        //////////////////////////
        $remoteip = $_SERVER["REMOTE_ADDR"];
        $url = "https://www.google.com/recaptcha/api/siteverify";
        // Curl Request
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array(
            'secret' => $this->privateKey,
            'response' => $response,
            'remoteip' => $remoteip
        ));
        $curlData = curl_exec($curl);
        curl_close($curl);

        // Parse data
        $recaptcha = json_decode($curlData, true);
        ///////////////////////////////////

        if ($recaptcha["success"]) {
            return true;
        } else {
            $this->error(self::BAD_CAPTCHA);
            return false;
        }
    }
}
