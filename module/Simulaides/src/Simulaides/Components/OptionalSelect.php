<?php

namespace Simulaides\Components;

use Zend\Form\Element\Select;

/**
 * Classe OptionalSelect
 *
 * Projet : Instm Ent 2013-2015
 
 *
 * @copyright Copyright © Institut Mines Télécom 2013-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Components
 */
class OptionalSelect extends Select {

    public function getInputSpecification() {
        $inputSpecification = parent::getInputSpecification();
        $inputSpecification['required'] = isset($this->attributes['required']) && $this->attributes['required'];
        return $inputSpecification;
    }

}
