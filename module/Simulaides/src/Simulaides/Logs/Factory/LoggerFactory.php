<?php
namespace Simulaides\Logs\Factory;

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe LoggerFactory
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Logs\Factory
 */
class LoggerFactory implements FactoryInterface
{
    /** Nom du service */
    const SERVICE_NAME = 'LoggerService';

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Récupération des fichiers de log par défaut
        $tabConfig = $serviceLocator->get('ApplicationConfig');

        $tabLog = $tabConfig['log'];
        //Création du logger
        $logger = new Logger($tabLog);

        //Création d'un writter
        $writer = new Stream($tabLog['default_logfile']);
        $logger->addWriter($writer);

        return $logger;
    }
}
