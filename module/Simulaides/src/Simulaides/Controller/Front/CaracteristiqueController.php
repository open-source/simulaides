<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 11/02/15
 * Time: 14:32
 */

namespace Simulaides\Controller\Front;

use Exception;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\TrancheConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\TrancheRevenu;
use Simulaides\Domain\SessionObject\ProjetSession;
use Simulaides\Form\Caracteristique\CaracteristiqueForm;
use Simulaides\Form\Caracteristique\LocalisationFieldset;
use Simulaides\Form\Caracteristique\ProjetFieldset;
use Simulaides\Service\LocalisationService;
use Simulaides\Service\ProjetService;
use Simulaides\Service\ProjetSessionContainer;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\TrancheRevenuService;
use Simulaides\Service\ValeurListeService;
use Zend\Form\Element\Select;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class CaracteristiqueController
 * @package Simulaides\Controller\Front
 */
class CaracteristiqueController extends SessionController implements IFrontController
{
    /** @var  ProjetService */
    private $projetService;

    /** @var  ProjetSessionContainer */
    private $projetSessionContainer;

    /** @var  ValeurListeService */
    private $valeurListeService;

    /** @var  LocalisationService */
    private $localisationService;

    /**
     * Action appelée sur l'affichage des caractéristiques
     *
     * @return array|Response|ViewModel
     * @throws Exception
     */
    public function indexAction()
    {
        //S'il s'agit d'une nouvelle simulation
        if ($this->estNouvelleSimulationParticulier()) {
            //vérification que le particulier a cocher les CGU
            $this->verifieCGU();
        }

        //Tout est ok, on gère la page des caractéristiques
        return $this->getCaracteristiqueProcess();
    }

    /**
     * Permet de gérer le process sur l'écran des caractéristiques
     *
     * @return Response|ViewModel|JsonModel
     * @throws Exception
     */
    private function getCaracteristiqueProcess()
    {
        /** @var Request $request */
        $request = $this->getRequest();



        //Récupération du projet en session
        $projetSession = $this->getProjetSessionContainer()->getProjet();
        //Récupération des informations de localisation en session
        $localisation = $this->getLocalisationService()->getLocalisationSession()->getLocalisation();
        //Récupération du formulaire des caractéristiques
        /** @var CaracteristiqueForm $caracteristiqueForm */
        $caracteristiqueForm = $this->getProjetService()->getCaracteristiqueForm();
        //Bind du formulaire
        $caracteristiqueForm = $this->getProjetService()->bindCaracteristiqueForm(
            $caracteristiqueForm,
            $projetSession,
            $localisation
        );

        /** @var SessionContainerService $session */
        $session = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);

        if ($session->offsetExists(SessionConstante::REGION_PARTICULIER)) {
            $codeRegion = $session->offsetGet(SessionConstante::REGION_PARTICULIER);

            /** @var LocalisationFieldset $localisationFieldset */
            $localisationFieldset = $caracteristiqueForm->get('localisation');
            $localisationFieldset->get('codeRegion')->setAttribute('disabled', true);
            //Suppression pour evité de doubler le fieldset
            $caracteristiqueForm->remove('localisation');
            $caracteristiqueForm->add($localisationFieldset);
        }

        $isComparatif = $this->params()->fromRoute('isComparatif') || $request->getQuery('isComparatif');
        if ($isComparatif !== true && $request->isPost()) {
            //Le formulaire a été validé
            //Récupération des data retournées par le formulaire
            $data         = $request->getPost();
            $isComparatif = isset($data['isComparatif']) ? true : false;
            $noValidation = isset($data['noValidation']) ? true : false;
            if (!$noValidation) {
                if (isset($codeRegion)) {
                    $localisation               = $data['localisation'];
                    $localisation['codeRegion'] = $codeRegion;
                    if (isset($data['localisation']['codeCommune'])) {
                        $departementDeCommune = $this->getLocalisationService()->getDepartementPourCommune($localisation['codeCommune']);
                        if ($departementDeCommune) {
                            $regionDeCommune = $this->getLocalisationService()->getRegionParCode($departementDeCommune->getCodeRegion());
                            if ($codeRegion != $regionDeCommune->getCode()) {
                                unset($localisation['codeCommune']);
                            }
                        } else {
                            unset($localisation['codeCommune']);
                        }
                    }
                    $data['localisation'] = $localisation;
                }
                $codeCommune = (isset($data['localisation']) && isset($data['localisation']['codeCommune'])) ? $data['localisation']['codeCommune'] : null;

                $outre_mer = $this->getLocalisationService()->estOutreMer($codeCommune);
                if($data["projet"]["codeTypeLogement"] === ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON || $outre_mer){
                    $projetFieldSet                = $data->get('projet');
                    if ($outre_mer) {
                        $projetFieldSet["codeEnergie"] = ValeurListeConstante::ENERG_ELECTRICITE;
                        $data->set("projet", $projetFieldSet);
                    }
                    //Mode chauffage indviduel si maison
                    if($data["projet"]["codeTypeLogement"] === ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON){
                        $projetFieldSet["codeModeChauff"]=ValeurListeConstante::MODE_CHAUFFAGE_INDIVIDUEL;
                        $data->set("projet", $projetFieldSet);
                    }
                }

                //Pour savoir si le submit a été déclenché pour le changement de codePostal (en cas de js désactivé)
                $submitCodePostal = isset($data['localisation']['submitCodePostal']);

                //Sauvegarde en session des données de localisation choisies par le particulier
                $this->getProjetService()->saveLocalisationSession($data);

                //Mise à jour du formulaire avec les données saisies
                $caracteristiqueForm->setData($data);

                if ($submitCodePostal) {
                    //Le formulaire ne doit pas être validé, on ne gère que le changement de région/département
                    //Les données déjà saisies sont tout de même mémorisées

                } else {
                    //Validation du formulaire et sauvegarde du projet
                    if ($caracteristiqueForm->isValid()) {
                        /** @var ProjetFieldset $projetFieldset */
                        $projetFieldset = $caracteristiqueForm->getFieldsets()['projet'];
                        /** @var ProjetSession $projetFieldSet */
                        $projetFieldSet = $projetFieldset->getObject();
                        //Récupération de la commune sélectionnée
                        $dataLocalisation = $this->getLocalisationService()->getLocalisationSession()->getLocalisation();
                        $codeCommune      = $dataLocalisation->getCodeInsee();
                        $codePostal       = $dataLocalisation->getCodePostal();
                        $projetFieldSet->setCodePostal($codePostal);
                        $projetFieldSet->setCodeCommune($codeCommune);
                        //todo voir pourquoi la valeur codeModeChauff n'est pas dans le $projetFieldSet
                        $data         = $request->getPost();
                        $project=$data->get('projet');
                        $projetFieldSet->setCodeModeChauff($project['codeModeChauff']);

                     //   $projetFieldSet->setCodeModeChauffage($data['']);
                        //Mémorisation du projet en session
                        $this->getProjetSessionContainer()->saveProjet($projetFieldSet);

                        //Redirection vers l'étape suivante des travaux
                        if ($isComparatif) {
                            return $this->forward()->dispatch('Simulaides\Controller\Front\Travaux', ['action' => 'index', 'isComparatif' => true]);
                        }

                        return $this->redirect()->toRoute(
                            $this->getParentRoute() . '/simulation/travaux',
                            [],
                            ['query' => ['PHPSESSID' => $session->getManager()->getId()]]
                        );
                    }
                }
            }
        }

        //Chargement des listes "Département" et "Commune" du formulaire de localisation
        $caracteristiqueForm = $this->chargeListeLocalisation($caracteristiqueForm);
        $caracteristiqueForm = $this->selectionAffichageTypeLogement($caracteristiqueForm);
        /** @var LocalisationFieldset $localisationFieldset */
        $localisationFieldset = $caracteristiqueForm->get('localisation');
        $communes             = $localisationFieldset->get('codeCommune')->getValueOptions();
        if (count($communes) > 1) {
            $codeCommunes = array_keys($communes);
            $outre_mer    = $this->getLocalisationService()->estOutreMer($codeCommunes[1]);
        } else {
            $outre_mer = false;
        }

        //Récupération du texte administrable de l'aide
        $textAide = $this->getValeurListeService()->getTextAdministrable(ValeurListeConstante::TEXTE_AIDE_MA_SITUATION);

        //Modification de l'accessibilité des éléments de localisation en fonction des valeurs
        $this->disabledLocalisation($caracteristiqueForm);


        /** @var ValeurListeService $vl */
        $vl           = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);

        // Récupération des textes pour les popup MaPrimRenov
        $textPopUpMaPrimeRenovTresModeste= $vl->getTextAdministrable
        (ValeurListeConstante::TEXTE_POPUP_MAPRIMRENOV_TRESMODESTE);
        $textPopUpMaPrimeRenovModeste= $vl->getTextAdministrable
        (ValeurListeConstante::TEXTE_POPUP_MAPRIMRENOV_MODESTE);
        $textPopUpMaPrimeRenovIntermediaire= $vl->getTextAdministrable
        (ValeurListeConstante::TEXTE_POPUP_MAPRIMRENOV_INTREMEDIAIRE);
        $textPopUpMaPrimeRenovSuperieur= $vl->getTextAdministrable
        (ValeurListeConstante::TEXTE_POPUP_MAPRIMRENOV_SUPERIEUR);

        /* tranche de revenu  this->getTrancheRevenuService()->getTrancheRevenusParGroupe($codeGroupe) */
        /** @var TrancheRevenuService $trancheRevenuService */
        $trancheRevenuService          = $this->getServiceLocator()->get(TrancheRevenuService::SERVICE_NAME);
        $listeTrancheMPR= $trancheRevenuService->getTrancheRevenusParGroupe('MPR');
        $listeTrancheMPRIDF= $trancheRevenuService->getTrancheRevenusParGroupe('MPR_IDF');
        $listeTrancheTM = '';
        $listeTrancheM = '';
        $listeTrancheI = '';
        $listeTrancheIDFTM = '';
        $listeTrancheIDFM = '';
        $listeTrancheIDFI = '';
        foreach($listeTrancheMPR as $listeTranche){
            /** @var TrancheRevenu $listeTranche*/
            $liste = $listeTranche->getMt1personne().','.$listeTranche->getMt2personne().','.
                     $listeTranche->getMt3personne().','.$listeTranche->getMt4personne().','.
                     $listeTranche->getMt5personne().','. $listeTranche->getMt6personne().','.
                     $listeTranche->getMtPersonneSupp();
            if($listeTranche->getCodeTranche() == TrancheConstante::TRES_MOD_MPR){
                $listeTrancheTM = $liste;
            }else if($listeTranche->getCodeTranche() == TrancheConstante::MODESTE_MPR){
                $listeTrancheM =  $liste;
            }else{
                $listeTrancheI =  $liste;
            }
        }

        foreach($listeTrancheMPRIDF as $listeTranche){
            /** @var TrancheRevenu $listeTranche*/
            $liste = $listeTranche->getMt1personne().','.$listeTranche->getMt2personne().','.
                     $listeTranche->getMt3personne().','.$listeTranche->getMt4personne().','.
                     $listeTranche->getMt5personne().','. $listeTranche->getMt6personne().','.
                     $listeTranche->getMtPersonneSupp();
            if($listeTranche->getCodeTranche() == TrancheConstante::TRES_MOD_MPR_I){
                $listeTrancheIDFTM = $liste;
            }else if($listeTranche->getCodeTranche() == TrancheConstante::MODESTE_MPR_IDF){
                $listeTrancheIDFM =  $liste;
            }else{
                $listeTrancheIDFI =  $liste;
            }
        }
        /** @var SessionContainerService $session */

        //Création de la vue
        $selfRoute = $this->url()->fromRoute($this->getParentRoute() . '/simulation/caracteristique');
        $model     = new ViewModel(
            [
                'form'         => $caracteristiqueForm,
                'textAide'     => $textAide,
                'urlAction'    => $selfRoute,
                'urlParent'    => $this->url()->fromRoute(
                    $this->getParentRoute() . '/simulation/caracteristique',
                    ['action' => 'set-localisation'],
                    ['query' => ['PHPSESSID' => $session->getManager()->getId()]]
                ),
                'jsActive'     => $session->estJsActif(),
                'outre_mer'    => $outre_mer,
                'isComparatif' => $isComparatif,
                'textPopUpMaPrimeRenovTresModeste' => $textPopUpMaPrimeRenovTresModeste,
                'textPopUpMaPrimeRenovModeste' => $textPopUpMaPrimeRenovModeste,
                'textPopUpMaPrimeRenovIntermediaire' => $textPopUpMaPrimeRenovIntermediaire,
                'textPopUpMaPrimeRenovSuperieur' => $textPopUpMaPrimeRenovSuperieur,
                'listeTrancheTM' => $listeTrancheTM,
                'listeTrancheM' => $listeTrancheM,
                'listeTrancheI' => $listeTrancheI,
                'listeTrancheIDFTM' => $listeTrancheIDFTM,
                'listeTrancheIDFM' => $listeTrancheIDFM,
                'listeTrancheIDFI' => $listeTrancheIDFI,
                'isConnectee' =>  $session->estConseillerConnecte(),
            ]
        );
        //On définie le template, car si on vient de la partie admin du conseiller, le template ne peut pas être trouvé
        $model->setTemplate('simulaides/caracteristique/index.phtml');

        if ($isComparatif) {
            $model->setTerminal(true);
            $data = ['htmlView' => $this->getServiceLocator()->get('ViewRenderer')->render($model)];

            return new JsonModel($data);
        }

        return $model;
    }

    /**
     * @param CaracteristiqueForm $form
     */
    private function disabledLocalisation($form)
    {
        $eltLocalisation = $form->get('localisation');

        /** @var Select $eltCommune */
        $eltCommune = $eltLocalisation->get('codeCommune');

        if (count($eltCommune->getValueOptions()) < 2) {
            $eltCommune->setAttribute('disabled', 'disabled');
        }
    }

    /**
     *
     * @param CaracteristiqueForm $caracteristiqueForm
     *
     * @return mixed
     * @throws Exception
     */
    private function selectionAffichageTypeLogement($caracteristiqueForm)
    {
        /** @var projetFieldset $projetFieldset */
        $projetFieldset = $caracteristiqueForm->get('projet');
        $session = $this->serviceLocator->get(SessionContainerService::SERVICE_NAME);
        $isConnectee = $session->estConseillerConnecte();
        if($isConnectee){
            $elt = $projetFieldset->get('codeTypeLogement');
            $typeLogement = $elt->getValue('M');
            $projetFieldset->remove('codeTypeLogement');
            $projetFieldset->add(
                [
                    'name'       => 'codeTypeLogement',
                    'type'       => 'ValeurListeObjectSelect',
                    'options'    => [
                        'label'        => 'Type de logement',
                        'required'     => true,
                        'empty_option' => 'Choisissez votre type de logement',
                        'find_method'  => [
                            'name'   => 'getListeValuesByCodeListe',
                            'params' => [
                                'codeListe' => ValeurListeConstante::LISTE_TYPE_LOGEMENT
                            ]
                        ],
                    ],
                    'attributes' => [
                        'id'               => 'codeTypeLogement',
                        'class'            => 'form-control',
                        'data-placeholder' => " "
                    ]
                ]
            );
            $elt = $projetFieldset->get('codeTypeLogement');
            $elt->setValue($typeLogement);

        }
        return $caracteristiqueForm;
    }

    /**
     * Permet de charger les listes des départements et des communes
     * selon les données sélectionnées
     *
     * @param CaracteristiqueForm $caracteristiqueForm
     *
     * @return mixed
     * @throws Exception
     */
    private function chargeListeLocalisation($caracteristiqueForm)
    {
        $dataLocalisation = $this->getLocalisationService()->getLocalisationSession()->getLocalisation();
        $codeRegion       = $dataLocalisation->getCodeRegion();
        $codePostal       = $dataLocalisation->getCodePostal();
        $codeCommune      = $dataLocalisation->getCodeInsee();
        /** @var LocalisationFieldset $localisationFieldset */
        $localisationFieldset = $caracteristiqueForm->get('localisation');
        if (!empty($codeRegion)) {
            if (empty($codePostal)) {
                //Une région est sélectionnée, il faut charger la liste des communes de cette région
                $listCommune = $this->getLocalisationService()->getCommunePourListeRegion($codeRegion);
                //Construction de la liste des options-values
                $listOptions = $this->getLocalisationService()->getListOptionsValuePourCommune($listCommune);
                /** @var Select $eltCommune */
                $eltCommune = $localisationFieldset->get('codeCommune');
                $eltCommune->setValueOptions($listOptions);
                $eltCommune->setValue($codeCommune);

            } else {
                //Une région et un code postal sont sélectionnés, il faut charger la liste des communes de cette région et ce code postal
                $listCommune = $this->getLocalisationService()->getCommunePourCodePostal($codePostal, $codeRegion);
                //Construction de la liste des options-values
                $listOptions = $this->getLocalisationService()->getListOptionsValuePourCommune($listCommune);

                $eltCodePostal = $localisationFieldset->get('codePostal');
                $eltCodePostal->setValue($codePostal);

                /** @var Select $eltCommune */
                $eltCommune = $localisationFieldset->get('codeCommune');
                $eltCommune->setValueOptions($listOptions);
                $eltCommune->setValue($codeCommune);
            }

        } else {
            if (!empty($codePostal)) {
                //Un code postal est sélectionné, il faut charger la liste des communes de ce code postal
                $listCommune = $this->getLocalisationService()->getCommunePourCodePostal($codePostal);
                //Construction de la liste des options-values
                $listOptions = $this->getLocalisationService()->getListOptionsValuePourCommune($listCommune);

                $eltCodePostal = $localisationFieldset->get('codePostal');
                $eltCodePostal->setValue($codePostal);
                /** @var Select $eltCommune */
                $eltCommune = $localisationFieldset->get('codeCommune');
                $eltCommune->setValueOptions($listOptions);
                $eltCommune->setValue($codeCommune);
            } else {
                //Un code postal est sélectionné, il faut charger la liste des communes de ce code postal
                $listCommune = [];//$this->getLocalisationService()->getAllCommunes();
                //Construction de la liste des options-values
                $listOptions = $this->getLocalisationService()->getListOptionsValuePourCommune($listCommune);

                $eltCodePostal = $localisationFieldset->get('codePostal');
                $eltCodePostal->setValue($codePostal);

                $eltCommune = $localisationFieldset->get('codeCommune');
                $eltCommune->setValueOptions($listOptions);
                $eltCommune->setValue($codeCommune);
            }
        }

        return $caracteristiqueForm;
    }

    /**
     * Action appelée sur le changement d'une région/département
     * Permet de mettre à jour les autres listes selon la sélection
     *
     * @return JsonModel
     */
    public function setLocalisationAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $params = $request->getPost();
            try {
                $this->getLocalisationService()->setLocalisationSession($params['type'], $params['value']);
                $options = $this->getLocalisationService()->getOptions($params['type'], $params['value']);

                return new JsonModel($options);
            } catch (Exception $e) {
                return new JsonModel(['status' => false, 'msg' => $e->getMessage()]);
            }
        }

        return new JsonModel(['status' => false, 'msg' => 'Vous ne pouvez pas faire ça.']);

    }

    /**
     * @return array|object|LocalisationService
     */
    private function getLocalisationService()
    {
        if (empty($this->localisationService)) {
            $this->localisationService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
        }

        return $this->localisationService;
    }

    /**
     * Retourne le service sur Valeur Liste
     *
     * @return array|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);
        }

        return $this->valeurListeService;
    }

    /**
     * @return ProjetSessionContainer
     */
    private function getProjetSessionContainer()
    {
        if (empty($this->projetSessionContainer)) {
            $this->projetSessionContainer = $this->getServiceLocator()->get(ProjetSessionContainer::SERVICE_NAME);
        }

        return $this->projetSessionContainer;
    }

    /**
     * @return ProjetService
     */
    private function getProjetService()
    {
        if (empty($this->projetService)) {
            $this->projetService = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        }

        return $this->projetService;
    }
}
