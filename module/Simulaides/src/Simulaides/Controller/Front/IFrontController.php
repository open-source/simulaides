<?php
namespace Simulaides\Controller\Front;

/**
 * Classe IFrontController
 * Permet de séparer les couches front des couches back office
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Front
 */
interface IFrontController
{
    /**
     * Permet de retourner le serviceLocator
     * @return mixed
     */
    public function getServiceLocator();
}
