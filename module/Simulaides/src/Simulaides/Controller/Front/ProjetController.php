<?php

namespace Simulaides\Controller\Front;

use Exception;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\SessionConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\SessionObject\ProjetSession;
use Simulaides\Service\GenerePdfService;
use Simulaides\Service\ProjetService;
use Simulaides\Service\ProjetSimulService;
use Simulaides\Service\ResultatViewService;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\SimulationService;
use Simulaides\Service\TravauxService;
use Zend\Http\Header\Referer;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Stdlib\ResponseInterface;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ModelInterface;
use Zend\View\Model\ViewModel;
Use Simulaides\Service\OffresCeeService;

/**
 * Classe ProjetController
 *
 * Projet : simulaides

 *
 * @copyright Copyright simulaides © 2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Front
 */
class ProjetController extends SessionController implements IFrontController
{
    /**
     * @var ResultatViewService
     */
    protected $resultatViewService;

    /** @var  SessionContainerService */
    private $sessionContainerService;

    /** @var  ProjetService */
    private $projetService;

    /**
     * Action permettant de rendre accessible le js
     */
    public function enableJsAction()
    {
        /** @var SessionContainerService $session */
        $session = $this->getSessionContainerService();
        $session->offsetSet(SessionConstante::JS_ACTIVE, true);
        /** @var Request $request */
        $request = $this->getRequest();
        /** @var Referer $url */
        $url = $request->getHeader('referer');
        $url = $url->getUri();
        $this->redirect()->toUrl($url);
    }

    /**
     * Action permettant de rendre inacessible le js
     */
    public function disableJsAction()
    {
        /** @var SessionContainerService $session */
        $session = $this->getSessionContainerService();
        $session->offsetSet(SessionConstante::JS_ACTIVE, false);

        /** @var Request $request */
        $request = $this->getRequest();
        /** @var  Referer $url */
        $url = $request->getHeader('referer');
        $url = $url->getUri();
        $this->redirect()->toUrl($url);
    }

    /**
     * Action sur le formulaire d'un autre dispositif
     * dans le résultat d'une simulation
     *
     * @return ResponseInterface|JsonModel
     */
    public function autreDispositifAction()
    {
        try {
            $data = $this->autreDispositifProcess();

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());
        }

        return $this->response;
    }

    /**
     * Action sur le formulaire d'un autre commentaire
     * dans le résultat d'une simulation
     *
     * @return ResponseInterface|JsonModel
     */
    public function autreCommentaireAction()
    {
        try {
            $data = $this->autreCommentaireProcess();

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->getResponse();
        }
    }

    /**
     * Génère le PDF et propose l'enregistrement à l'utilisateur
     *
     * @return Response|Response\Stream
     * @throws Exception
     */
    public function generePdfAction()
    {
        try {
            /** @var ProjetService $projetService */
            $projetService = $this->getProjetService();
            /** @var ProjetSession $projetSession */
            $projetSession = $projetService->getProjetSession()->getProjet();
            /** @var Projet $projet */
            $projet = $this->getProjetService()->getProjetParId($projetSession->getId());

            /** @var GenerePdfService $pdfService */
            $pdfService = $this->getServiceLocator()->get(GenerePdfService::SERVICE_NAME);
            $fileName   = $pdfService->genereResultatPdf($projet);
            $response   = new Response\Stream();
            if (file_exists($fileName)) {
                $fileContents = file_get_contents($fileName);
                unlink($fileName);
                /** @var Response $response */
                $response = $this->getResponse();
                $response->setContent($fileContents);
                $headers = $response->getHeaders();
                $headers->clearHeaders()
                        ->addHeaderLine('Content-Type', 'application/pdf;charset=UTF-8')
                        ->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($fileName) . '"')
                        ->addHeaderLine('Content-Length', strlen($fileContents));
            } else {
                $response->setStatusCode(404);
            }

            return $response;
        } catch (Exception $oException) {
            //Gestion des exception
            throw ($oException);
        }
    }

    /**
     * Action d'envoi du PDF par mail
     *
     * @return ResponseInterface|JsonModel
     */
    public function envoiMailPDFAction()
    {
        $session = $this->getSessionContainerService();
        try {
            $data = $this->envoiMailPDFProcess();
            if ($session->estJsActif()) {
                return new JsonModel($data);
            } else {
                //JS desactivé, on retourne le model complet
                return $data;
            }
        } catch (Exception $oException) {
            if ($session->estJsActif()) {
                //Gestion des exception remontée pour être affichées en js
                $this->response->setContent($oException->getMessage());

                return $this->getResponse();
            }
        }
    }

    /**
     * Fait la gestion du formulaire d'envoi du PDF par mail
     *
     * @return array|ResponseInterface|ModelInterface
     * @throws Exception
     */
    private function envoiMailPDFProcess()
    {
        $data  = [];
        $pdfOK = false;

        /** @var Request $request */
        $request = $this->getRequest();

        //Récupère le formulaire
        $form    = $this->getResultatViewService()->getPDFFormAndBind($this->getParentRoute());
        $msgOk   = '';
        $session = $this->getSessionContainerService();
        if ($request->isPost()) {
            $dataPost = $request->getPost();

            // Permet de savoir quelle action a été demandée (js désactivé)
            $telechargePdf = isset($dataPost['telecharger']);
            $emailPdf      = isset($dataPost['envoyer']);

            if ($telechargePdf) {
                //En js désactivé, on demande à générer le PDF, redirection vers l'action dédiée
                return $this->redirect()->toRoute(
                    $this->getParentRoute() . '/simulation/projet',
                    [
                        'action' => 'generePdf'
                    ],
                    [
                        'query' => [
                            'PHPSESSID' => $session->getManager()->getId()
                        ]
                    ]
                );
            }
            if ($emailPdf) {
                unset($dataPost['envoyer']);
                $form->setData($dataPost);

                if ($form->isValid()) {
                    //Génération du PDF
                    $projetService = $this->getProjetService();
                    $projetSession = $projetService->getProjetSession()->getProjet();
                    /** @var Projet $projet */
                    $projet = $this->getProjetService()->getProjetParId($projetSession->getId());
                    /** @var GenerePdfService $pdfService */
                    $pdfService = $this->getServiceLocator()->get(GenerePdfService::SERVICE_NAME);
                    //$pdfService->genereMailRestulatPdf($dataPost['email'], $projet);
                    $pdfService->genereMailWithSmallPdf($dataPost['email'], $projet);
                    //$pdfService->genereMailSansPdf($dataPost['email'], $projet);
                    $msgOk = 'Le résultat a été envoyé par mail.';
                }
            }
        }

        if ($session->estJsActif()) {
            //Création de la vue à retourner pour l'intégrer dans le resultat
            $model            = $this->getResultatViewService()->getViewModelResultatPDF($this->getParentRoute(), $form, $msgOk);

            $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

            return $data;
        } else {
            //Recréation de la vue complète pour afficher les messages
            $this->getResultatViewService()->msgFormPdf = $msgOk;
            $this->getResultatViewService()->formPdf    = $form;
            $model                                      = $this->processResultatAction();

            return $model;
        }
    }

    /**
     * Action appelée sur "Lancer simulation"
     * Vérification qu'au moins un travaux a été ajouté
     *
     * @return Response|JsonModel
     * @throws Exception
     */
    public function saveProjetTravauxAction()
    {
        $isComparatif = $this->params()->fromRoute('isComparatif');
        //Vérification qu'au moins un travaux a été sélectionné
        /** @var SessionContainerService $sessionContainer */
        $sessionContainer = $this->getSessionContainerService();
        $selectedTravaux  = $sessionContainer->getSelectedTravaux();
        if (empty($selectedTravaux)) {
            $this->flashMessenger()->setNamespace('travauxError')->addMessage(
                'Vous devez sélectionner au moins un type de travaux pour exécuter une simulation.'
            );

            if ($isComparatif) {
                return $this->forward()->dispatch('Simulaides\Controller\Front\Travaux', ['action' => 'index', 'isComparatif' => true]);
            }

            //Redirection vers la page de sélection des travaux du projet
            return $this->redirect()->toRoute(
                $this->getParentRoute() . '/simulation/travaux',
                [],
                ['query' => ['PHPSESSID' => $sessionContainer->getManager()->getId()]]
            );
        }

        $bbc = $sessionContainer->offsetGet(SessionConstante::BBC);
        //Récupération du projet en cours de saisie en session
        $projetSession = $this->getProjetService()->getProjetSession()->getProjet();

        //Savoir si le projet est nouveau
        $isNewProjet = ($projetSession->getId() === null);
        //Affectation de la données BBC au projet (bouton radio)
        $projetSession->setBbc(intval($bbc));
        //Affectation de la date du jour
        $projetSession->setDateEval(new \DateTime());
        //Affectation des données du conseiller
        $this->setDataPrisConseiller($projetSession);
        /** @var TravauxService $travauxService */
        $travauxService = $this->getServiceLocator()->get(TravauxService::SERVICE_NAME);
        //Récupération de la liste des travaux/produits/saisies produits sélectionnés
        $travaux = $travauxService->getSelectedProduits();
        //Mémorisation en base du projet avec les données sélectionnées et saisies
        $simulHisto = $this->getProjetService()->saveProjetEnBase($projetSession, $travaux); //###
        //Mémorisation du projet ainsi modifié en session
        $this->getProjetService()->getProjetSession()->saveProjet($projetSession);

        //Lancement de l'algo de simulation
        $this->getProjetService()->lanceSimulation($projetSession->getId(), $simulHisto);

        if ($isNewProjet) {
            //Mémorisation du calcul des coûts
            $travauxService->memoriseCoutProduitPourProjet($projetSession->getId());
        }

        if ($isComparatif) {
            /** @var ResultatViewService $resultatViewService */
            $resultatViewService = $this->getServiceLocator()->get(ResultatViewService::SERVICE_NAME);
            $offreCeeService=$this->getServiceLocator()->get(OffresCeeService::SERVICE_NAME);
            $model = $resultatViewService->getViewModelResultat(
                $this->getProjetService()->getProjetParId($projetSession->getId()),
                $this->getParentRoute(),
                $offreCeeService,
                true
            );
            $model->setVariable('urlRetour', $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux', ['action' => 'saisies']).'?PHPSESSID='.$this->getSessionContainerService()->getManager()->getId());

            return new JsonModel(
                [
                    'htmlView'     => $this->getServiceLocator()->get('ViewRenderer')->render($model),
                    'idSimulation' => $projetSession->getId()
                ]
            );
        }

        return $this->redirect()->toRoute(
            $this->getParentRoute() . '/simulation/projet',
            [
                'action' => 'resultat',
                'id' => $projetSession->getId()
            ],
            [
                'query' => [
                    'PHPSESSID' => $sessionContainer->getManager()->getId()
                ]
            ]
        );

    }

    /**
     * Permet d'affecter les données PRIS au projet
     *  - Si un conseiller est connecté, ce sont ses données qui sont prises
     *  - Si c'est un particulier, on laisse tel quel
     *
     * @param ProjetSession $projet
     *
     * @return mixed
     */
    private function setDataPrisConseiller($projet)
    {
        /** @var SessionContainerService $sessionService */
        //#delete $sessionService = $this->getSessionContainerService();
        /** @var SimulationService $simulationService */
        $projetSimulService = $this->getServiceLocator()->get(ProjetSimulService::SERVICE_NAME);
        $codeCommune        = $projet->getCodeCommune();
        $codeTranche        = $this->getResultatViewService()->getCodeTranche($codeCommune, $projet);
        $coordPris          = $projetSimulService->getCoordonneesPris($codeCommune, $codeTranche);

        //if ($sessionService->estConseillerConnecte()) {
        //si un conseiller est connecté, on récupère les informations
        //    $conseiller = $sessionService->getConseiller();

        //Affectation des informations au projet
        $nom = isset($coordPris['nom']) ? $coordPris['nom'] : "";
        $projet->setPrisNom($nom);
        $email = isset($coordPris['email']) ? $coordPris['email'] : "";
        $projet->setPrisEmail($email);
        $num = isset($coordPris['num_tel_local']) ? $coordPris['num_tel_local'] : "";
        $projet->setPrisTel($num);
        //}
    }

    /**
     * Résultat de la simulation
     * Affiché lorsque la simulation a été réalisée
     *
     * @return ModelInterface
     * @throws Exception
     */
    public function resultatAction()
    {
        return $this->processResultatAction();
    }

    /**
     * Permet de générer la vue sur le résultat
     *
     * @return ModelInterface
     * @throws Exception
     */
    private function processResultatAction()
    {
        $projetService = $this->getProjetService();
        $projetSession = $projetService->getProjetSession()->getProjet();
        /** @var Projet $projet */
        $projet = $this->getProjetService()->getProjetParId($projetSession->getId());

        $model = new ViewModel();
        $model->setTemplate('simulaides/projet/resultat.phtml');
        $offreCeeService=$this->getServiceLocator()->get(OffresCeeService::SERVICE_NAME);
        $modelCorps = $this->getResultatViewService()->getViewModelResultat($projet, $this->getParentRoute(),$offreCeeService,false);
        $modelCorps->setVariable('urlRetour', $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux',
                                                                      ['action' => 'saisies']).'?PHPSESSID='.$this->getSessionContainerService()->getManager()->getId());

        $model->setVariable(
            'resultat',
            $this->getServiceLocator()->get('ViewRenderer')->render($modelCorps)
        );

        //Url de retour
        $urlRetour = $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux', ['action' => 'saisies']);
        $model->setVariable('urlRetour', $urlRetour);

        $urlPastille1Retour = $this->url()->fromRoute($this->getParentRoute() . '/simulation/caracteristique');
        $model->setVariable('urlPastille1Retour', $urlPastille1Retour);

        $urlPastille2Retour = $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux');
        $model->setVariable('urlPastille2Retour', $urlPastille2Retour);

        $urlPastille3Retour = $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux', ['action' => 'saisies']);
        $model->setVariable('urlPastille3Retour', $urlPastille3Retour);

        $urlAccueil = $this->url()->fromRoute($this->getParentRoute());
        if (substr($urlAccueil, -5) == "admin") {
            $urlAccueil .= '/simulation-conseiller';
        }
        $model->setVariable('urlAccueil', $urlAccueil);
        return $model;
    }
    /**
     * Permet de faire la gestion de l'action sur le formulaire
     *
     * @return array
     * @throws Exception
     */
    private function autreDispositifProcess()
    {
        $data  = [];
        $msgOK = null;

        /** @var Request $request */
        $request = $this->getRequest();

        //Récupération du projet dans la session
        $projetSession = $this->getProjetService()->getProjetSession()->getProjet();
        //Récupération du projet
        $projet = $this->getProjetService()->getProjetParId($projetSession->getId());

        //Récupération et bind du formulaire
        $form = $this->getResultatViewService()->getAutreDispositifFormAndBind($projet);

        if ($request->isPost()) {
            $dataPost = $request->getPost();

            if ($dataPost['submit']) {
                unset($dataPost['submit']);
                //Le formulaire a été validé
                $form->setData($dataPost);
                if ($form->isValid()) {
                    /** @var Projet $projet */
                    $projet = $form->getObject();
                    //Mémorisation du projet en base
                    $this->getProjetService()->sauveProjetEnBdd($projet);
                    //Mémorisation dans le projet en session
                    $projetSession->setIntituleAideConseiller($projet->getIntituleAideConseiller());
                    $projetSession->setMtAideConseiller($projet->getMtAideConseiller());
                    $projetSession->setDescriptifAideConseiller($projet->getDescriptifAideConseiller());
                    //Mémorisation dans le résultat de simulation
                    $simulationResultat = $this->getSessionContainerService()->getSimulationResultat();
                    $simulationResultat->setMtConseillerDispositif($projet->getMtAideConseiller());
                    //Indication que l'autre dispositif a été sauvegardé
                    $msgOK = "Le dispositif a été sauvegardé.";
                }
            }
        }
        $model            = $this->getResultatViewService()->getViewModelResultatAutreDispositif($projet, $this->getParentRoute(), $form, $msgOK);
        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);
        //Recalcul de l'aide accordé avec le montant saisi
        /** @var SessionContainerService $session */
        $session       = $this->getSessionContainerService();
        $estConseiller = $session->estConseillerConnecte();
        $data['aide']  = $this->getProjetService()->getResultatAide($estConseiller);

        return $data;
    }

    /**
     * Permet de faire la gestion de l'action sur le formulaire
     *
     * @return array
     * @throws Exception
     */
    private function autreCommentaireProcess()
    {
        $data  = [];
        $msgOK = null;

        /** @var Request $request */
        $request = $this->getRequest();

        //Récupération du projet dans la session
        $projetSession = $this->getProjetService()->getProjetSession()->getProjet();
        //Récupération du projet en base
        /** @var Projet $projet */
        $projet = $this->getProjetService()->getProjetParId($projetSession->getId());

        //Récupération et bind du formulaire
        $form = $this->getResultatViewService()->getAutreCommentaireFormAndBind($projet);

        if ($request->isPost()) {
            $dataPost = $request->getPost();

            if ($dataPost['submit']) {
                unset($dataPost['submit']);
                //Le formulaire a été validé
                $form->setData($dataPost);
                if ($form->isValid()) {
                    /** @var Projet $projet */
                    $projet = $form->getObject();
                    //Mémorisation du projet en base
                    $this->getProjetService()->sauveProjetEnBdd($projet);
                    //Mémorisation dans le projet en session
                    $projetSession->setAutreCommentaireConseiller($projet->getAutreCommentaireConseiller());
                    //Indication que le commentaire a été sauvegardé
                    $msgOK = "La sauvegarde du commentaire a été réalisée.";
                }
            }
        }
        $model            = $this->getResultatViewService()->getViewModelResultatAutreCommentaire($projet, $this->getParentRoute(), $form, $msgOK);
        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return $data;
    }

    /**
     * Permet d'afficher le fichier de log lié à la simulation en cours
     * @return void
     * @throws Exception
     */
    public function chargeLogSimulationAction()
    {
        //Récupération du projet
        $projetSession = $this->getProjetService()->getProjetSession()->getProjet();
        $projet        = $this->getProjetService()->getProjetParId($projetSession->getId());
        if (!empty($projet)) {
            $fileName = $projet->getNumero() . '.log';
            //Construction de l'entête du fichier
            header("Content-Type: text/log;charset=UTF-8");
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            header("Content-Transfer-Encoding: binary");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo MainConstante::CARACTERE_BOM;

            //Création d'un fichier physique dans tmp
            fopen(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName, 'w');
            //chargement du contenu du log dans ce fichier
            file_put_contents(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName, $projet->getFichierLog());

            //Lecture du fichier
            readfile(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName);
            exit;
        }
    }

    /**
     * Retourne le service de session
     *
     * @return array|object|SessionContainerService
     */
    private function getSessionContainerService()
    {
        if (empty($this->sessionContainerService)) {
            $this->sessionContainerService = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        }

        return $this->sessionContainerService;
    }

    /**
     * Retourne le service de projet
     *
     * @return ProjetService
     */
    private function getProjetService()
    {
        if (empty($this->projetService)) {
            $this->projetService = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        }

        return $this->projetService;
    }

    /**
     * Retourne le service de la vue résultat
     *
     * @return ResultatViewService
     */
    private function getResultatViewService()
    {
        if (empty($this->resultatViewService)) {
            $this->resultatViewService = $this->getServiceLocator()->get(ResultatViewService::SERVICE_NAME);
        }

        return $this->resultatViewService;
    }
}
