<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 13/02/15
 * Time: 14:35
 */

namespace Simulaides\Controller\Front;

use Simulaides\Constante\AccesSimulationFormConstante;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Form\Accueil\ConditionGeneraleForm;
use Simulaides\Form\Simulation\AccesSimulationForm;
use Simulaides\Logger\LoggerSimulation;
use Simulaides\Service\AccessSimulationFormService;
use Simulaides\Service\ArchivageLogService;
use Simulaides\Service\LocalisationService;
use Simulaides\Service\PartenaireService;
use Simulaides\Service\ProjetService;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\ValeurListeService;
use Simulaides\Service\VersionService;
use Simulaides\Tools\Utils;
use Zend\Form\Element\Button;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Http\Response\Stream;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\ResponseInterface;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ModelInterface;
use Zend\View\Model\ViewModel;

/**
 * Class AccueilController
 * @package Simulaides\Controller\Front
 */
class AccueilController extends AbstractActionController implements IFrontController
{
    /**
     * Service sur le formulaire d'accès à la simulation
     *
     * @var AccessSimulationFormService
     */
    private $accessSimulationFormService;

    /** @var  SessionContainerService */
    private $sessionContainerService;

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $cle  = $this->getEvent()->getRouteMatch()->getParam('cle');
        $colorN1  = $this->getEvent()->getRouteMatch()->getParam('colorN1');
        $codeRegion  = $this->getEvent()->getRouteMatch()->getParam('codeRegion');

        /** @var SessionContainerService $session */
        $session = $this->getSessionService();
        /** @var VersionService $versionService */
        $versionService = $this->getServiceLocator()->get(VersionService::SERVICE_NAME);
        $version = $versionService->getVersion();
        $session->addOffset(SessionConstante::VERSION, $version);

        //Zone de flou, on détermine s'il s'agit de l'ancien modèle d'intégration ou du nouveau
        if(strlen($codeRegion) > 2){//Ancien modèle
            return $this->redirect()->toRoute('front-home', array(
                'cle'     => $cle,
                'colorN1' => $colorN1,
            ));
        }

        //##Dans le cas où une région est définie en base de donnée pour le partenaire, celle indiquée dans l'url est ignorée au profit de celle en base
        /** @var PartenaireService $partenaireService */
        $partenaireService = $this->getServiceLocator()->get(PartenaireService::SERVICE_NAME);

        $partenaire = null;

        if(isset($_SERVER['HTTP_REFERER'])) {
            $conteneurParent = Utils::getConteneurParentFromPageAppelante($_SERVER['HTTP_REFERER']);
            $partenaire = $partenaireService->getPartenaireByUrl($conteneurParent);
        }

        if($partenaire){
            $region = $partenaire->getRegion();
            if($region){
                $codeRegion = $region->getCode();
                $this->getEvent()->getRouteMatch()->setParam('codeRegion', $codeRegion);
            }
        }
        
        //Archivage du log applicatif
        /** @var ArchivageLogService $archiveService */
        $archiveService = $this->serviceLocator->get(ArchivageLogService::SERVICE_NAME);
        $archiveService->archiveLog();

        //Récupère les couleurs et les mémorise dans la session
        $this->setColorInSession();

        //Récupère le code de la région dans la session
        $this->setRegionInSession();

        //Paramètre de perte de session
        $perteSession = $this->getEvent()->getRouteMatch()->getParam('perteSession');
        $msgSession   = null;
        if ($perteSession) {
            $msgSession = 'La session a été perdue.';
        }

        return $this->getViewModelAccueil($msgSession);
    }

    /**
     * Controller Assurant la rétrocompatibilité avec l'intégration des iframes
     * @return ViewModel
     */
    public function indexLegacyAction()
    {
        $cle        = $this->getEvent()->getRouteMatch()->getParam('cle');
        $colorN1    = $this->getEvent()->getRouteMatch()->getParam('colorN1');
        $codeRegion = $this->getEvent()->getRouteMatch()->getParam('codeRegion');
        
        return $this->redirect()->toRoute('front-home', array(
            'cle'     => $cle,
            'colorN1' => $colorN1,
            'codeRegion' => $codeRegion,
        ));
    }

    /**
     * Action de vérification des CGU
     *
     * @return ResponseInterface | ModelInterface
     */
    public function checkCguAction()
    {
        $session = $this->getSessionContainerService();


        try {
            $data = $this->processCheckCGU();
            if ($session->estJsActif()) {
                //Javascript actif, gestion en AJAX
                if (!empty($data)) {
                    return new JsonModel($data);
                }
                $this->response->setStatusCode(Response::STATUS_CODE_200);

                return $this->response;
            } else {
                return $data;
            }
        } catch (\Exception $oException) {
            if ($session->estJsActif()) {
                //Gestion des exception remontée pour être affichées en js
                $this->response->setContent($oException->getMessage());
            }
        }
    }

    /**
     * Permet de faire la vérification des CGU
     *
     * @return array|\Zend\Http\Response|ViewModel
     */
    private function processCheckCGU()
    {
        $data    = [];
        $isOkCgu = false;

        /** @var Request $request */
        $request = $this->getRequest();

        $dataReturn['checkCguOK'] = true;

        //Récupère le formulaire et fait le bind
        /** @var ProjetService $projetService */
        $projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
        $formCGU       = $projetService->getCguForm();
        $formCGU->add(
            [
                'name'       => 'PHPSESSID',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionContainerService()->getManager()->getId()
                ]
            ]
        );
        $session = $this->getSessionContainerService();
        if ($request->isPost()) {
            $dataPost = $request->getPost();
            $formCGU->setData($dataPost);
            //if ($formCGU->isValid()) {
                $isOkCgu = true;
                //Mémorise en session que les CGU sont validées
                $session->addOffset(SessionConstante::CGU, $isOkCgu);
            //}
        }
        if ($isOkCgu) {
            //Initialisation d'un nouveau projet pour la nouvelle simulation
            $projetService->initialiseNouveauProjet($dataPost);
            //Mémorisation en session qu'il s'agit d'une simulation de production
            $session->setModeExecution(SimulationConstante::MODE_PRODUCTION);
        }
        if ($session->estJsActif()) {
            //Création de la vue
            $model = $this->getViewModelCgu($formCGU);
            $model->setTerminal(true);

            //Données de retour du JSon
            $data['htmlView']   = $this->getServiceLocator()->get('ViewRenderer')->render($model);
            $data['checkCguOK'] = $isOkCgu;
            $data['url']        = $this->url()->fromRoute(
                'front/simulation/caracteristique',
                [],
                ['query' => ['PHPSESSID' => $session->getManager()->getId()]]
            );


            return $data;
        } else {
            //On est en javascript désactivé
            if ($isOkCgu) {
                //Tout est OK, on affiche la page des caractéristiques
                return $this->redirect()->toRoute(
                    'front/simulation/caracteristique',
                    [],
                    ['query' => ['PHPSESSID' => $session->getManager()->getId()]]
                );
            } else {
                //Il y a eu une erreur, création de la vue complète de la page d'accueil
                $model = $this->getViewModelAccueil(null, null, $formCGU);

                return $model;
            }
        }
    }

    /**
     * Retourne la vue de la page d'accueil
     *
     * @param null $msgSession
     * @param null $msgError
     * @param null $cguForm
     *
     * @return ViewModel
     */
    private function getViewModelAccueil($msgSession = null, $msgError = null, $cguForm = null)
    {
        // Récupération du texte paramétré de l'accueil
        /** @var ValeurListeService $vl */
        $vl           = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);
        $textAccueil1 = $vl->getTextAdministrable(ValeurListeConstante::TEXTE_ACCEUIL);
        $textAccueil2 = $vl->getTextAdministrable(ValeurListeConstante::TEXTE_ACCEUIL_2);
        $textAccueil3 = $vl->getTextAdministrable(ValeurListeConstante::TEXTE_ACCEUIL_3);
        $textAccueil4 = $vl->getTextAdministrable(ValeurListeConstante::TEXTE_ACCUEIL_PREREQUIS);
        $textAccueilChapoHome = $vl->getTextAdministrable(ValeurListeConstante::TEXTE_ACCUEIL_CHAPO_HOME);

        /** @var ProjetService $projetService */
        $projetService = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        //Récupération du formulaire d'accès à la simulation
        $accessForm = $projetService->getAccessSimulationForm();

        // Réinitialisation à false du test des CGU
        $session = $this->getSessionContainerService();
        $this->getSessionContainerService()->offsetUnset(SessionConstante::CGU);

        $isRegionKO = false;
        if ($session->offsetExists(SessionConstante::REGION_PARTICULIER)) {
            $codeRegion = $session->offsetGet(SessionConstante::REGION_PARTICULIER);
            if (!empty($codeRegion)) {
                /* @var $localisationService LocalisationService */
                $localisationService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
                $region              = $localisationService->getRegionParCode($codeRegion);
                $isRegionKO          = ($region === null);
            }
        }


        $model = new ViewModel(
            [
                'texteAccueil1' => $textAccueil1,
                'texteAccueil2' => $textAccueil2,
                'texteAccueil3' => $textAccueil3,
                'texteAccueil4' => $textAccueil4,
                'textAccueilChapoHome' => $textAccueilChapoHome,
                'cgu'          => $session->offsetGet(SessionConstante::CGU),
                'msgSession'   => $msgSession,
                'isRegionKO'   => $isRegionKO,
            ]
        );
        $model->setTemplate('simulaides/accueil/index.phtml');
        $model->addChild($this->getViewModelCgu($cguForm), 'cguFormulaire');
        $model->addChild($this->getViewModelAccesSimulation($accessForm, $msgError), 'formAccesSimulation');

        return $model;
    }

    /**
     * Retourne la vue pour le formulaire des CGU
     *
     * @param ConditionGeneraleForm $form
     *
     * @return ViewModel
     */
    private function getViewModelCgu($form = null)
    {
        /** @var ProjetService $projetService */
        $projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);

        $model = new ViewModel(['isPageAccueil' => true]);
        $model->setTemplate('simulaides/accueil/cgu.phtml');

        if (empty($form)) {
            $form = $projetService->getCguForm();
        }
        $form->add(
            [
                'name'       => 'PHPSESSID',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionContainerService()->getManager()->getId()
                ]
            ]
        );
        //Définition du onclick du bouton Valider
        /** @var Button $btnValide */
        $btnValide = $form->get('start_simul');
        $session   = $this->getSessionContainerService();
        /*if ($session->estJsActif()) {
            $onClick = 'submitFormAccueil();return false;';
            $btnValide->setAttribute('onclick', $onClick);
        }*/
        $model->setVariable('form', $form);
        $model->setVariable('phpsessid', $session->getManager()->getId());

        return $model;
    }

    /**
     * Permet d'ouvrir le fichier des conditions
     */
    public function readCguAction()
    {
        //Définition du lien vers les conditions générales
        $tabConfig          = $this->serviceLocator->get('ApplicationConfig');
        $tabCgu             = $tabConfig['condition-generale'];
        $fileNameFichierCgu = $tabCgu['cgu_utilisateur'];
        $response           = new Stream();

        if (file_exists($fileNameFichierCgu)) {
            $fileContents = file_get_contents($fileNameFichierCgu);
            /** @var Response $response */
            $response = $this->getResponse();
            $response->setContent($fileContents);
            $headers = $response->getHeaders();
            $headers->clearHeaders()
                ->addHeaderLine('Content-Type', 'application/pdf;charset=UTF-8')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($fileNameFichierCgu) . '"')
                ->addHeaderLine('Content-Length', strlen($fileContents));
        } else {
            $response->setStatusCode(404);
        }

        return $response;
    }

    /**
     * Permet de récupérer le code région passée dans l'url et le
     * mémoriser dans la session
     */
    private function setRegionInSession()
    {
        $codeRegion = null;


        //Récupération du service de session
        $session = $this->getSessionContainerService();

        if (isset($_SERVER['HTTP_REFERER'])) {
            $conteneurParent = Utils::getConteneurParentFromPageAppelante($_SERVER['HTTP_REFERER']);

            /** @var PartenaireService $partenaireService */
            $partenaireService = $this->getServiceLocator()->get(PartenaireService::SERVICE_NAME);
            $partenaire = $partenaireService->getPartenaireByUrl($conteneurParent);

            if($partenaire) {
                $region = $partenaire->getRegion();
                if($region) {
                    $codeRegion = $region->getCode();
                }
                $session->addOffset(SessionConstante::PARTENAIRE, $partenaire->getId());
            }
        }

        if(!$codeRegion) {
            //récupération du code région dans l'url
            $codeRegion = $this->getEvent()->getRouteMatch()->getParam('codeRegion');
        }

        $session->addOffset(SessionConstante::REGION_PARTICULIER, $codeRegion);
    }

    /**
     * Permet de récupérer les couleurs dans l'url et les mémoriser dans la session
     */
    private function setColorInSession()
    {
        //récupération des couleurs dans l'url
        $colorN1      = $this->getEvent()->getRouteMatch()->getParam('colorN1');
        $colorN2      = $this->getEvent()->getRouteMatch()->getParam('colorN2');
        $vl           = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);
        $colorN3      = $vl->getTextAdministrable(ValeurListeConstante::COULEUR_TEXTE_ACCEUIL_2);
        $colorBtn     = $this->getEvent()->getRouteMatch()->getParam('colorBtn');

        //Récupération du service de session
        $session = $this->getSessionContainerService();

        //Indication que l'on n'est pas dans l'espace conseiller
        $session->addOffset(SessionConstante::ACCESS_PRIS, false);

        //Ajout des couleurs dans la session, si elles sont définies
        //Elles sont dans l'url uniquement lors de l'appel de l'appli
        /*if (!empty($colorN1)) {
            if (substr($colorN1, 1, 1) != '#') {
                $colorN1 = '#' . $colorN1;
            }
            $session->setCouleurFondTitreN1($colorN1);
        }

        if (!empty($colorN2)) {
            if (substr($colorN2, 1, 1) != '#') {
                $colorN2 = '#' . $colorN2;
            }
            $session->setCouleurFondTitreN2($colorN2);
        }
        
        if (!empty($colorN3)) {
            if (substr($colorN3, 1, 1) != '#') {
                $colorN3 = '#' . $colorN3;
            }
            $session->setCouleurFondTitreN3($colorN3);
        }

        if (!empty($colorBtn)) {
            if (substr($colorBtn, 1, 1) != '#') {
                $colorBtn = '#' . $colorBtn;
            }
            $session->setCouleurFondBouton($colorBtn);
        }*/
    }

    /**
     * Permet de lancer une simulation
     *
     * @return array | ResponseInterface | ModelInterface
     */
    public function lanceSimulationAction()
    {
        $session = $this->getSessionContainerService();
        try {
            $data = $this->processSimulation();
            if ($session->estJsActif()) {
                //Javascript actif, gestion en AJAX
                if (!empty($data)) {
                    return new JsonModel($data);
                }
                $this->response->setStatusCode(Response::STATUS_CODE_200);

                return $this->response;
            } else {
                return $data;
            }

        } catch (\Exception $oException) {
            if ($session->estJsActif()) {
                //Gestion des exception remontée pour être affichées en js
                $this->response->setContent($oException->getMessage());
            }

            return $this->response;
        }
    }

    /**
     * Permet de faire le traitement de la simulation
     *
     * @return array | ResponseInterface | ModelInterface
     */
    private function processSimulation()
    {
        $data = [];
        /** @var Request $request */
        $request = $this->getRequest();

        $dataReturn['msgError']     = null;
        $dataReturn['simulationOK'] = false;
        //Récupère le formulaire et fait le bind
        $formAccesSimulation = $this->getAccessSimulationFormService()->getAndBindFormAccessSimulation();

        if ($request->isPost()) {
            $dataPost = $request->getPost();
            //Récupération des données du formulaire
            $numSimulation = $dataPost[AccesSimulationFormConstante::CHAMP_NUM_SIMULATION];
            $codeAcces     = $dataPost[AccesSimulationFormConstante::CHAMP_CODE_ACCES];
            $dateDebut = new \DateTime();
           //Vérifie le projet, valide le formulaire et charge le projet en session
            $dataReturn = $this->getAccessSimulationFormService()->processSimulationApresVerifProjet(
                $formAccesSimulation,
                $numSimulation,
                $codeAcces,
                $dataPost
            );

        }
        $session = $this->getSessionContainerService();
        if ($session->estJsActif()) {

            //Création de la vue
            $model = $this->getViewModelAccesSimulation($formAccesSimulation, $dataReturn['msgError']);
            $model->setTerminal(true);

            //Données de retour du JSon
            $data['htmlView']     = $this->getServiceLocator()->get('ViewRenderer')->render($model);
            $data['simulationOk'] = $dataReturn['simulationOK'];
            $data['url']          = $this->url()->fromRoute(
                'front/simulation/caracteristique',
                [],
                [
                    'query' => [
                        'PHPSESSID' => $session->getManager()->getId()
                    ]
                ]
            );

            return $data;
        } else {
            //On est en javascript désactivé
            if ($dataReturn['simulationOK']) {
                //Tout est OK, on affiche la page ds caractéristiques
                return $this->redirect()->toRoute(
                    'front/simulation/caracteristique',
                    [],
                    ['query' => ['PHPSESSID' => $session->getManager()->getId()]]
                );
            } else {
                //Il y a eu une erreur, création de la vue complète de la page d'accueil
                $model = $this->getViewModelAccueil(null, $dataReturn['msgError']);

                return $model;
            }
        }
    }

    /**
     * Retourne la vue du formulaire d'accès à la simulation
     *
     * @param AccesSimulationForm $formAccesSimulation
     * @param                     $msgError
     *
     * @return ViewModel
     */
    private function getViewModelAccesSimulation($formAccesSimulation, $msgError = null)
    {
        //Définition du onclick du bouton Valider
        /** @var Button $btnValide */
        $btnValide = $formAccesSimulation->get('btnValide');
        $formAccesSimulation->add(
            [
                'name'       => 'PHPSESSID',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionContainerService()->getManager()->getId()
                ]
            ]
        );
        $session = $this->getSessionContainerService();
        if ($session->estJsActif()) {
            $onClick = 'submitFormAccesSimulationAccueil();return false;';
            $btnValide->setAttribute('onclick', $onClick);
        }
        $model = new ViewModel(
            [
                'form'       => $formAccesSimulation,
                'formName'   => 'frmAcces',
                'formAction' => $this->url()->fromRoute('front/simulation-particulier-lance'),
                'msgError'   => $msgError
            ]
        );
        $model->setTemplate('simulaides/acces-simulation.phtml');

        return $model;
    }

    /**
     * Retourne le service deu formulaire d'accès à la simulation
     *
     * @return AccessSimulationFormService
     */
    private function getAccessSimulationFormService()
    {
        if (empty($this->accessSimulationFormService)) {
            $this->accessSimulationFormService = $this->getServiceLocator()->get(
                AccessSimulationFormService::SERVICE_NAME
            );
        }

        return $this->accessSimulationFormService;
    }

    /**
     * Retourne le service sur la session
     *
     * @return array|object|SessionContainerService
     */
    private function getSessionContainerService()
    {
        if (empty($this->sessionContainerService)) {
            $this->sessionContainerService = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        }

        return $this->sessionContainerService;
    }

    /**
     * @return mixed|\Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Retourne le service de session
     *
     * @return SessionContainerService
     */
    private function getSessionService()
    {
        /** @var SessionContainerService $session */
        $session = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        return $session;
    }
}
