<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 23/02/15
 * Time: 10:24
 */

namespace Simulaides\Controller\Front;

use Exception;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Constante\TravauxConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\SessionObject\Travaux\TravauxSession;
use Simulaides\Form\Travaux\DescriptionProjetForm;
use Simulaides\Service\LocalisationService;
use Simulaides\Service\ProjetService;
use Simulaides\Service\ProjetSessionContainer;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\TravauxService;
use Simulaides\Service\ValeurListeService;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

/**
 * Class TravauxController
 *
 * @package Simulaides\Controller\Front
 */
class TravauxController extends SessionController implements IFrontController
{
    /** @var  ValeurListeService */
    private $valeurListeService;

    /** @var  TravauxService */
    private $travauxService;

    /** @var  LocalisationService */
    private $localisationService;

    /**
     * Permet d'afficher l'écran de sélection des travaux
     * pour un projet
     *
     * @return Response|ViewModel|JsonModel
     * @throws Exception
     */
    public function indexAction()
    {
        //S'il s'agit d'une nouvelle simulation
        if ($this->estNouvelleSimulationParticulier()) {
            //vérification que le particulier a cocher les CGU
            $this->verifieCGU();
        }

        $request = $this->getRequest();
        $session = $this->getSessionContainer();

        $isComparatif = $this->params()->fromRoute('isComparatif') || $request->getQuery('isComparatif');

        if($request->isPost()){
            $data         = $request->getPost();
            if (empty($data['listeTravaux']) && !$isComparatif) {
                $this->getSessionContainer()->offsetSet(SessionConstante::CODES_TRAVAUX, null);
                $this->getSessionContainer()->offsetSet(SessionConstante::TRAVAUX, null);
            }
        }


        //Récupération de la liste des travaux par catégorie pour le type de logement du projet
        /** @var TravauxService $travauxService */
        $travauxService = $this->getTravauxService();
        /** @var ProjetSessionContainer $projet */
        $projet              = $this->getServiceLocator()->get(ProjetSessionContainer::SERVICE_NAME);
        $localisationService = $this->getLocalisationService();
        $outre_mer           = $localisationService->estOutreMer($projet->getProjet()->getCodeCommune());
        $isMayotte           = $localisationService->estMayotte($projet->getProjet()->getCodeCommune());
        $isReunion           = $localisationService->estLaReunion($projet->getProjet()->getCodeCommune());
        $isMartinique        = $localisationService->estLaMartinique($projet->getProjet()->getCodeCommune());
        $isGuadeloupe        = $localisationService->estLaGuadeloupe($projet->getProjet()->getCodeCommune());
        //ajout pour DOMTOM
        $condition['outre_mer'] = $outre_mer;
        $condition['isMayotte'] = $isMayotte;
        $condition['isReunion'] = $isReunion;
        $condition['isMartinique'] = $isMartinique;
        $condition['isGuadeloupe'] = $isGuadeloupe;

        $travaux             = $travauxService->getTravauxByTypeLogementEtRegion($projet->getProjet()->getCodeTypeLogement(), $condition);

        $urlAction = $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux');

        $descriptionProjetForm = new DescriptionProjetForm(
            "descPjt", [
            'listeTravaux' => $travaux
        ]
        );

        $codeSession = $session->offsetGet(SessionConstante::CODES_TRAVAUX);
        if ($session->offsetExists(SessionConstante::CODES_TRAVAUX) && !empty($codeSession)) {
            $codesTravauxSelected = $session->offsetGet(SessionConstante::CODES_TRAVAUX);
            $formData             = [
                'listeTravaux' => $codesTravauxSelected,
                'bbc'          => ($projet->getProjet()->getBbc()) ? $projet->getProjet()->getBbc() : 0
            ];
            $descriptionProjetForm->setData($formData);
        } elseif ($session->offsetExists(SessionConstante::TRAVAUX)) {
            $codesTravauxSelected = array_map(
                static function ($t) {
                    /* @var TravauxSession $t */
                    return $t->getCodeTravaux();
                },
                $session->offsetGet(SessionConstante::TRAVAUX)
            );
            $formData             = [
                'listeTravaux' => $codesTravauxSelected,
                'bbc'          => ($projet->getProjet()->getBbc()) ? $projet->getProjet()->getBbc() : 0
            ];
            $descriptionProjetForm->setData($formData);
        }else{
            $formData             = [
                'bbc'          => 0
            ];
            $descriptionProjetForm->setData($formData);
        }

        //Texte de l'aide affiché au dessus
        $textAide = $this->getValeurListeService()->getTextAdministrable(
            ValeurListeConstante::TEXTE_AIDE_SELECTION_TRAVAUX
        );

        //$descriptionProjetForm->setHydrator(new ArraySerializable());

        $descriptionProjetForm->setAttribute('method', 'post');

        $descriptionProjetForm->setAttribute('action', $urlAction);

        $descriptionProjetForm->setAttribute('outreMer', $outre_mer);


        if ($isComparatif !== true && $request->isPost()) {
            $data         = $request->getPost();
            $isComparatif = isset($data['isComparatif']) ? true : false;
            if (!isset($data['bbc'])) {
                $data['bbc'] = 0;
            }
            $descriptionProjetForm->setData($data);

            if ($descriptionProjetForm->isValid()) {
                $listeTravaux = $descriptionProjetForm->get('listeTravaux')->getValue();

                $this->getSessionContainer()->offsetSet(SessionConstante::CODES_TRAVAUX, $listeTravaux);

                $bbc = $descriptionProjetForm->get('bbc')->getValue();

                $this->getSessionContainer()->offsetSet(SessionConstante::BBC, $bbc);

                //Redirection vers l'étape suivante des travaux
                if ($isComparatif) {
                    return $this->forward()->dispatch('Simulaides\Controller\Front\Travaux', ['action' => 'saisies', 'isComparatif' => true]);
                }

                return $this->redirect()->toRoute(
                    $this->getParentRoute() . '/simulation/travaux',
                    ['action' => 'saisies'],
                    ['query' => ['PHPSESSID' => $session->getManager()->getId()]]
                );
            }
            if (empty($data['listeTravaux'])) {
                $errorMessage = 'Vous devez sélectionner au moins un type de travaux pour exécuter une simulation.';
                /*$this->flashMessenger()->setNamespace('travauxError')->addMessage(
                    'Vous devez sélectionner au moins un type de travaux pour exécuter une simulation.'
                );*/
            }
        }

        $flashMessage = $this->flashMessenger()->getMessagesFromNamespace('travauxError');

        //tranche d'aide MPR
        $projetService = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        $trancheRevenueMPR = $projetService->getTrancheRevenuMPR($projet->getProjet());


        $model = new ViewModel(
            [
                'textAide'              => $textAide,
                'travaux'               => $travaux,
                'flashMessages'         => $flashMessage,
                'urlRetour'             => $this->url()->fromRoute($this->getParentRoute() . '/simulation/caracteristique').'?PHPSESSID='.$session->getManager()->getId(),
                'urlPastille1Retour'    => $this->url()->fromRoute($this->getParentRoute() . '/simulation/caracteristique'),
                'partUrlTravaux'        => $this->getParentRoute() . '/simulation/travaux',
                'descriptionProjetForm' => $descriptionProjetForm,
                'errorMessage'          => isset($errorMessage) ? $errorMessage : '',
                'isComparatif'          => $isComparatif,
                'codeTranche'   => $trancheRevenueMPR,
                'statut'    => $projet->getProjet()->getCodeStatut(),
                'typeLogement'    => $projet->getProjet()->getCodeTypeLogement(),
                'isOutreMer' => $outre_mer
            ]
        );

        if ($isComparatif) {
            //On définie le template, car si on vient de la partie admin du conseiller, le template ne peut pas être trouvé
            $model->setTemplate('simulaides/travaux/index.phtml');
            $model->setTerminal(true);
            $data = ['htmlView' => $this->getServiceLocator()->get('ViewRenderer')->render($model)];

            return new JsonModel($data);
        }

        return $model;
    }

    /**
     * Appelée sur le click d'un travaux
     * => accès à la fiche de saisie
     * => validation de la fiche "enregistrer" ou "Ajouter le travaux"
     *
     * @return ViewModel|Response
     * @throws Exception
     */
    public function saisiesAction()
    {
        /** @var Request $request */
        $request      = $this->getRequest();
        $flashMessage = '';

        /** @var TravauxService $travauxService */
        $travauxService = $this->getTravauxService();
        /* @var ProjetSessionContainer $projet */
        $projet = $this->getServiceLocator()->get(ProjetSessionContainer::SERVICE_NAME);
        // recherche si region = reunion
        $localisationService= $this->getLocalisationService();
        $estReunion = $localisationService->estLaReunion($projet->getProjet()->getCodeCommune());
        /** @var SessionContainerService $session */
        $session     = $this->getSessionContainer();
        $codeSession = $session->offsetGet(SessionConstante::CODES_TRAVAUX);
        if ($session->offsetExists(SessionConstante::CODES_TRAVAUX) && !empty($codeSession)) {
            $codesTravaux = $session->offsetGet(SessionConstante::CODES_TRAVAUX);
        } elseif ($session->offsetExists(SessionConstante::TRAVAUX) && !empty($codeSession)) {
            $travauxSessions = $session->offsetGet(SessionConstante::TRAVAUX);
            $codesTravaux    = array_map(
                static function ($t) {
                    /* @var TravauxSession $t */
                    return $t->getCodeTravaux();
                },
                $travauxSessions
            );
        } else {
            $codesTravaux = $session->offsetGet(SessionConstante::CODES_TRAVAUX);
        }

        $travaux = array_map(
            function ($cT) {
                return $this->getTravauxService()->getTravauxByCode($cT);
            },
            $codesTravaux
        );
        $result[0] = array();
        $result[1] = array();
        $result[2] = array();
        $result[3] = array();
        $result[4] = array();

        foreach ($travaux as $travail) {
            switch ($travail->getCategorie()) {
                case ValeurListeConstante::CAT_TRAVAUX_ETUDE:
                    $result[0][] = $travail;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_ISOLATION:
                    $result[1][] = $travail;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_EQUIPEMENT:
                    $result[2][] = $travail;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_ENERGIES_RENOUVELABLES:
                    $result[3][] = $travail;
                    break;
                case ValeurListeConstante::CAT_TRAVAUX_ELECTROMENAGER:
                    $result[4][] = $travail;
                    break;
            }
        }

        $travaux = [];
        for($i=0;$i<5;$i++){
            $travaux = array_merge($travaux,$result[$i]);
        }



        $formLabel = 'travaux';

        //Savoir si la colonne "calculé" est affichée
        $avecColoneCalculee = $session->estConseillerConnecte();

        $localisationService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
        $outre_mer = $localisationService->estOutreMer($projet->getProjet()->getCodeCommune());

        $form = $travauxService->getMultiTravauxForm(
            $projet->getProjet(),
            $travaux,
            $projet->getProjet()->getCodeTypeLogement(),
            $this->estNouvelleSimulationParticulier(),
            true,
            $outre_mer
        );

        $isComparatif = $this->params()->fromRoute('isComparatif') || $request->getQuery('isComparatif');
        if ($isComparatif !== true && $request->isPost()) {
            $data         = $request->getPost();
            $isComparatif = isset($data['isComparatif']) ? true : false;
            try {
                //Vérification du formulaire et sauvegarde du travaux
                if ($travauxService->processForm($form, $request->getPost())) {
                    if ($isComparatif) {
                        return $this->forward()->dispatch(
                            'Simulaides\Controller\Front\Projet',
                            ['action' => 'save-projet-travaux', 'isComparatif' => true]
                        );
                    }

                    return $this->redirect()->toRoute(
                        $this->getParentRoute() . '/simulation/projet',
                        ['action' => 'save-projet-travaux'],
                        ['query' => ['PHPSESSID' => $session->getManager()->getId()]]
                    );
                }
            } catch (Exception $e) {
                echo $e->getMessage();
                //Ajout de l'erreur pour l'affichage dans le formulaire
                $flashMessage = $e->getMessage();
            }
        }

        //Sélection du texte d'aide à la saisie
        $textAideEtude   = $this->getValeurListeService()->getTextAdministrable(ValeurListeConstante::TEXTE_AIDE_SAISIE_TRAVAUX_ETUDE);
        $textAideFenetre = $this->getValeurListeService()->getTextAdministrable(ValeurListeConstante::TEXTE_AIDE_SAISIE_TRAVAUX_FENETRE);
        $textAideTravaux = $this->getValeurListeService()->getTextAdministrable(ValeurListeConstante::TEXTE_AIDE_SAISIE_TRAVAUX);

        //tranche d'aide MPR
        $projetService = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        $trancheRevenueMPR = $projetService->getTrancheRevenuMPR($projet->getProjet());


        $model = new ViewModel(
            [
                'textAideTravaux'    => $textAideTravaux,
                'textAideEtude'      => $textAideEtude,
                'textAideFenetre'    => $textAideFenetre,
                'form'               => $form,
                'travaux'            => $travaux,
                'flashMessages'      => $flashMessage,
                'avecColonneCalcule' => $avecColoneCalculee,
                'urlRetour'          => $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux').'?PHPSESSID='.$session->getManager()->getId(),
                'urlPastille1Retour' => $this->url()->fromRoute($this->getParentRoute() . '/simulation/caracteristique'),
                'urlPastille2Retour' => $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux'),
                'partUrlTravaux'     => $this->getParentRoute() . '/simulation/travaux',
                'isComparatif'       => $isComparatif,
                'estReunion'         => $estReunion,
                'codeTranche'   => $trancheRevenueMPR,
                'statut'    => $projet->getProjet()->getCodeStatut(),
                'typeLogement'    => $projet->getProjet()->getCodeTypeLogement(),
                'adminConseiller' => $session->estConseillerConnecte()
            ]
        );
        if ($isComparatif) {
            //On définie le template, car si on vient de la partie admin du conseiller, le template ne peut pas être trouvé
            $model->setTemplate('simulaides/travaux/saisies.phtml');
            $model->setTerminal(true);
            $data = ['htmlView' => $this->getServiceLocator()->get('ViewRenderer')->render($model)];

            return new JsonModel($data);
        }

        return $model;
    }

    /**
     * Supprime un travaux
     *
     * @return Response
     * @throws Exception
     */
    public function deleteTravauxAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isGet()) {
            //Récupération du code du travaux à supprimer
            $code = $this->getEvent()->getRouteMatch()->getParam('code');
            //Suppression des prix calculés pour les produits et le projet
            $this->getTravauxService()->deletePrixAdminPourTravaux($code);
            //Suppression du travaux dans la session
            $this->getSessionContainer()->deleteTravaux($code);

            //Redirection vers la liste des travaux
            return $this->redirect()->toRoute(
                $this->getParentRoute() . '/simulation/travaux',
                [],
                ['query' => ['PHPSESSID' => $this->getSessionContainer()->getManager()->getId()]]
            );
        }
    }

    /**
     * @return SessionContainerService
     */
    public function getSessionContainer()
    {
        return $this->getServiceLocator()->get('SessionContainer');
    }

    /**
     * Retourne le service sur les listes de valeurs
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);
        }

        return $this->valeurListeService;
    }

    /**
     * Retourne le service sur les travaux
     *
     * @return array|object|TravauxService
     */
    private function getTravauxService()
    {
        if (empty($this->travauxService)) {
            $this->travauxService = $this->getServiceLocator()->get(TravauxService::SERVICE_NAME);
        }

        return $this->travauxService;
    }

    /**
     * @return LocalisationService
     */
    private function getLocalisationService()
    {
        if (empty($this->localisationService)) {
            $this->localisationService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
        }

        return $this->localisationService;
    }
}
