<?php

namespace Simulaides\Controller\Front;

use Exception;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\Entity\ProjetChoixDispositif;
use Simulaides\Service\OffresCeeService;
use Simulaides\Service\ProjetService;
use Simulaides\Service\ResultatViewService;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\TravauxService;
use Simulaides\Service\ValeurListeService;
use Zend\Http\Response;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class OffresCeeController
 *
 * @package Simulaides\Controller\Front
 */
class OffresCeeController extends SessionController implements IFrontController
{

    /** @var  OffresCeeService */
    private $offresCeeService;

    private $msgErreur = '';

    /** @var ProjetService */
    private $projetService;


    public function saveCdpAction()
    {
        $session = $this->getSessionContainerService();
        $request = $this->getRequest();
        $projetService = $this->getProjetService();
        $projetSession = $projetService->getProjetSession()->getProjet();
        /** @var Projet $projet */
        $projet = $this->getProjetService()->getProjetParId($projetSession->getId());
        if ($request->isPost()) {
            $data = $request->getPost();
            $offreCeeService = $this->getServiceLocator()->get(OffresCeeService::SERVICE_NAME);
            $offreCeeService->insertCoupDePouceFromPopup($projet->getId(), $projet, $data['dispositifCdp'], $data['codetravaux']);
        }
        if ($data['isComparatif'] === 'true') {
            return $this->forward()->dispatch(

                'Simulaides\Controller\Back\Simulation\Comparatif',
                ['action' => 'consulterSimulation', 'isComparatif' => true]
            );
        }
        return $this->redirect()->toRoute(
            $this->getParentRoute() . '/simulation/projet',
            ['action' => 'save-projet-travaux'],
            ['query' => ['PHPSESSID' => $session->getManager()->getId()]]
        );


    }

    /**
     * Permet d'afficher l'écran de sélection des offres Cee
     * pour un projet
     *
     * @return Response|ViewModel|JsonModel
     * @throws Exception
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        $urlPastille1Retour = $this->url()->fromRoute($this->getParentRoute() . '/simulation/caracteristique');
        $urlPastille2Retour = $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux');
        $urlPastille3Retour = $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux', ['action' => 'saisies']);

        if ($request->isPost()) {
            $data = $request->getPost();

            if (!$this->getOffresCeeService()->isFormValid($data)) {
                $this->msgErreur = 'Vous devez sélectionner au moins une des offres auxquelles vous êtes éligible.';
            } else {

                $this->getOffresCeeService()->insertOffresSelectionnees($data);

                /** @var SessionContainerService $sessionContainer */
                $sessionContainer = $this->getSessionContainerService();

                $projetSession = $this->getProjetService()->getProjetSession()->getProjet();

                /** @var TravauxService $travauxService */
                $travauxService = $this->getServiceLocator()->get(TravauxService::SERVICE_NAME);
                //Récupération de la liste des travaux/produits/saisies produits sélectionnés
                $travaux = $travauxService->getSelectedProduits();

                //Mémorisation en base du projet avec les données sélectionnées et saisies
                $simulHisto = $this->getProjetService()->saveProjetEnBase($projetSession, $travaux);

                // Mise en session des choix
                unset($data['PHPSESSID']);
                $this->getSessionContainer()->offsetSet(SessionConstante::OFFRES, $data);


                $this->getProjetService()->lanceSimulation($projetSession->getId(), $simulHisto);
                $isComparatif = $this->params()->fromRoute('isComparatif');

                if ($isComparatif) {
                    /** @var ResultatViewService $resultatViewService */
                    $resultatViewService = $this->getServiceLocator()->get(ResultatViewService::SERVICE_NAME);
                    $offreCeeService=$this->getServiceLocator()->get(OffresCeeService::SERVICE_NAME);
                    $model = $resultatViewService->getViewModelResultat(
                        $this->getProjetService()->getProjetParId($projetSession->getId()),
                        $this->getParentRoute(),
                        $offreCeeService,
                        true
                    );
                    $model->setVariable('urlRetour', $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux', ['action' => 'saisies']).'?PHPSESSID='.$this->getSessionContainerService()->getManager()->getId());

                    return new JsonModel(
                        [
                            'htmlView'     => $this->getServiceLocator()->get('ViewRenderer')->render($model),
                            'idSimulation' => $projetSession->getId()
                        ]
                    );
                }

                return $this->redirect()->toRoute(
                    $this->getParentRoute() . '/simulation/projet',
                    [
                        'action' => 'resultat',
                        'id' => $projetSession->getId()
                    ],
                    [
                        'query' => [
                            'PHPSESSID' => $sessionContainer->getManager()->getId()
                        ]
                    ]
                );

            }
        }

        //Texte de l'aide affiché au dessus
        $textAide = $this->getValeurListeService()->getTextAdministrable(
            ValeurListeConstante::TEXTE_AIDE_OFFRES_CEE
        );

        $session = $this->getSessionContainerService();
        $simulation = $session->offsetGet(SessionConstante::SIMULATION);

        $listeTravauxAvecOffre = array();
        $listeTravaux = array();

        foreach ($simulation->getDispositifs() as $dispositif) {
            foreach ($dispositif->getAidesTravaux() as $aidesTravaux) {
                $listeTravauxAvecOffre[$aidesTravaux->getCodeTravaux()] = array();
                $listeTravaux[$aidesTravaux->getCodeTravaux()] = $aidesTravaux->getIntituleTravaux();
            }
        }

        foreach ($simulation->getDispositifs() as $dispositif) {
            foreach ($dispositif->getAidesTravaux() as $aidesTravaux) {
                $listeTravauxAvecOffre[$aidesTravaux->getCodeTravaux()][] = $dispositif;
            }
        }

        // Tri alphabétique
        foreach ($listeTravauxAvecOffre as $travaux => $offres) {
             usort($listeTravauxAvecOffre[$travaux], function($a, $b) {
                return strcasecmp($a->getIntitule(), $b->getIntitule()) >= 0;
            });
        }

        /** @var ProjetChoixDispositif $projetChoixDispositif */
        $projetChoixDispositifs = $this->getOffresCeeService()->getOffresSelectionnees();
        $choix = array();
        foreach ($projetChoixDispositifs as $projetChoixDispositif) {
            $choix[$projetChoixDispositif->getTravaux()->getCode()] = $projetChoixDispositif->getDispositif()->getId();
        }
        $this->getSessionContainer()->offsetSet(SessionConstante::OFFRES, $choix);

        $form = $this->getOffresCeeService()->getOffresCeeForm($listeTravauxAvecOffre);

        /** arrondi des montants */
        foreach ($listeTravauxAvecOffre as $travaux => $offres) {
            foreach ($offres as $offre) {
                foreach ($offre->getAidesTravaux() as $aidesTravaux) {
                    $aidesTravaux->setMontantAideTo(ceil($aidesTravaux->getMontantAideTo() / 10) * 10);
                }
            }
        }

        return new ViewModel([
            'textAide' => $textAide,
            'urlRetour' => $this->url()->fromRoute($this->getParentRoute() . '/simulation/travaux'),
            'listeTravauxAvecOffre' => $listeTravauxAvecOffre,
            'listeTravaux' => $listeTravaux,
            'form' => $form,
            'msgErreur' => $this->msgErreur,
            'choixSelectionnes' => $choix,
            'urlPastille1Retour' => $urlPastille1Retour,
            'urlPastille2Retour'=> $urlPastille2Retour,
            'urlPastille3Retour'=> $urlPastille3Retour
        ]);

    }

    /**
     * @return SessionContainerService
     */
    public function getSessionContainer()
    {
        return $this->getServiceLocator()->get('SessionContainer');
    }

    /**
     * Retourne le service sur les listes de valeurs
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);
        }

        return $this->valeurListeService;
    }

    /**
     * Retourne le service de session
     *
     * @return array|object|SessionContainerService
     */
    private function getSessionContainerService()
    {
        if (empty($this->sessionContainerService)) {
            $this->sessionContainerService = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        }

        return $this->sessionContainerService;
    }

    /**
     * Retourne le service sur les offres cee
     *
     * @return array|object|OffresCeeService
     */
    private function getOffresCeeService()
    {
        if (empty($this->offresCeeService)) {
            $this->offresCeeService = $this->serviceLocator->get(OffresCeeService::SERVICE_NAME);
        }
        return $this->offresCeeService;
    }

    /**
     * Retourne le service de projet
     *
     * @return ProjetService
     */
    private function getProjetService()
    {
        if (empty($this->projetService)) {
            $this->projetService = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        }

        return $this->projetService;
    }

}
