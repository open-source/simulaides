<?php
namespace Simulaides\Controller\Back;

use Simulaides\Constante\TableConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\TrancheGroupe;
use Simulaides\Domain\Entity\TrancheRevenu;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\TrancheGroupeService;
use Simulaides\Service\TrancheRevenuService;
use Zend\Config\Writer\Json;
use Zend\Form\Element;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Classe TrancheRevenuController
 * Permet de gérer le paramétrage des tranches de revenu
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class TrancheRevenuController extends SessionController implements IBackController
{

    /** @var  TrancheGroupeService */
    private $trancheGroupeService;

    /** @var  TrancheRevenuService */
    private $trancheRevenuService;

    /**
     * Permet de retourner la vue pour la liste des groupes de tranche
     * @return ViewModel
     */
    public function groupeListeAction()
    {
        $model = new ViewModel(
            [
                'tabEntete'   => $this->getGroupeTrancheEnteteTableau(),
                'tabLignes'   => $this->getGroupeTrancheLignesTableau(),
                'isUserAdeme' => $this->isUserAdeme()
            ]
        );
        $model->setTerminal(true);
        $model->setTemplate('simulaides/tranche-revenu/groupe-liste.phtml');

        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Retourne les informations de l'entête du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des groupes de tranche
     *
     * @return array
     */
    private function getGroupeTrancheEnteteTableau()
    {
        //Certains onglets sont visibles si l'utilisateur est ADEME


        $tabEntete = [];
        //Colonne code
        $tabEntete[] = new TableCellule('Code');
        //Colonne intitulé
        $tabEntete[] = new TableCellule('Intitulé');
        //Colonne Modifier
        $tabEntete[] = new TableCellule('Visualiser/Modifier');
        //Colonne Supprimer
        $tabEntete[] = new TableCellule('Supprimer');

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des groupes de tranche
     *
     * @return array
     */
    private function getGroupeTrancheLignesTableau()
    {
        $tabLignes        = [];
        $tabTrancheGroupe = $this->getTrancheGroupeService()->getTrancheGroupes();

        /** @var TrancheGroupe $groupeTranche */
        foreach ($tabTrancheGroupe as $groupeTranche) {
            $ligne = new TableLigne();
            $ligne->setId($groupeTranche->getCodeGroupe());

            //Cellule du code
            $cellule = new TableCellule($groupeTranche->getCodeGroupe(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            //Cellule de l'intitulé
            $lien    = "javascript:modifFicheGroupeTranche('" . $groupeTranche->getCodeGroupe() . "');";
            $cellule = new TableCellule($groupeTranche->getIntitule(), TableConstante::TYPE_LIEN, $lien);
            $ligne->addCellule($cellule);
            if (($groupeTranche->getCodeGroupe() !== 'ANAH') || ($this->isUserAdeme())) {
                //Cellule de la modification
                $lien    = "javascript:afficheListeTrancheRevenu('" . $groupeTranche->getCodeGroupe() . "');";
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_MODIF, $lien);
                $ligne->addCellule($cellule);
                //Cellule de la suppression
                $lien    = "javascript:deleteGroupeTranche('" . $groupeTranche->getCodeGroupe() . "');";
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_SUPP, $lien);
                $ligne->addCellule($cellule);
            } else {
                //Cellule de la modification
                $lien    = "javascript:afficheListeTrancheRevenu('" . $groupeTranche->getCodeGroupe() . "');";
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_VISUALISER, $lien);
                $ligne->addCellule($cellule);
                //Cellule de la suppression
                $cellule = new TableCellule('', TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);
            }
            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;
        }

        return $tabLignes;
    }

    /**
     * Suppression d'un groupe de tranche de revenu
     *
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function groupeSuppAction()
    {
        try {
            $data = $this->groupeSupprime();
            return new JsonModel($data);
        } catch (\Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());
            return $this->response;
        }
    }

    /**
     * Permet de réaliser la suppression du groupe
     * @return null
     */
    private function groupeSupprime()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $data    = null;
        if ($request->isPost()) {
            //Récupération des data envoyée
            $dataPost   = $request->getPost();
            $codeGroupe = $dataPost->get('codeGroupe');

            //-----------------Suppression des tranches de revenu du groupe
            $this->getTrancheRevenuService()->deleteTrancheRevenuParGroupe($codeGroupe);

            //-----------------Suppression du groupe
            $this->getTrancheGroupeService()->deleteTrancheGroupe($codeGroupe);

            $data['ok'] = true;
        }

        return $data;
    }

    /**
     * Action d'ajout d'un groupe de tranche
     *
     * @return ViewModel
     */
    public function ajoutGroupeFicheAction()
    {
        //Indique que le formulaire est valide
        $formIsValid = false;

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            //Récupération des informations envoyées par la requête
            $data     = $request->getPost();
            $isSubmit = isset($data['submit']);

            if (!$isSubmit) {
                //Création du formulaire et hydratation avec le groupe à modifier
                $trancheGroupeForm = $this->getTrancheGroupeService()->getTrancheGroupeForm(false);
                $trancheGroupeForm->bind(new TrancheGroupe());

            } else {
                //Envoi du formulaire par le bouton submit

                //Création du formulaire et hydratation avec le groupe modifié
                $trancheGroupeForm = $this->getTrancheGroupeService()->getTrancheGroupeForm();
                //Suppression des informations inutiles au remplissage du formulaire
                unset($data['submit']);
                //Remplissage du formulaire avec les données saisies
                $trancheGroupeForm->setData($data);

                if ($trancheGroupeForm->isValid()) {
                    /** @var TrancheGroupe $trancheGroupe */
                    $trancheGroupe = $trancheGroupeForm->getObject();

                    //Recherche que le code ajouté n'existe pas déjà
                    $trancheGroupeExistant = $this->getTrancheGroupeService()->getTrancheGroupe(
                        $trancheGroupe->getCodeGroupe()
                    );
                    if (!empty($trancheGroupeExistant)) {
                        //Le groupe existe déjà
                        /** @var Element $eltCodeGroupe */
                        $eltCodeGroupe = $trancheGroupeForm->get('codeGroupe');
                        $eltCodeGroupe->setMessages(["Le code du groupe existe déjà."]);
                    } else {
                        //Sauvegarde du groupe de tranche
                        $this->getTrancheGroupeService()->saveTrancheGroupe($trancheGroupe);
                        $formIsValid = true;
                    }
                }
            }
            //Création du formulaire
            $viewModel = new ViewModel(['form' => $trancheGroupeForm, 'formIsValid' => $formIsValid]);
            $viewModel->setTerminal(true);

            $viewModel->setTemplate('simulaides/tranche-revenu/ajout-groupe-fiche.phtml');
            $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($viewModel);

            return new JsonModel($data);
        }

        return $this->response;
    }

    /**
     * Action de modification d'un groupe de tranche
     *
     * @return ViewModel
     */
    public function modifGroupeFicheAction()
    {
        //Indique que le formulaire est valide
        $formIsValid = false;

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            //Récupération des informations envoyées par la requête
            $data     = $request->getPost();
            $isSubmit = isset($data['submit']);

            if (!$isSubmit) {
                //Appel de l'affichage de ma modale
                $codeGroupe = $data['codeGroupe'];

                //Recherche du groupe que l'on souhaite afficher dans la modale
                $trancheGroupe = $this->getGroupeModifier($codeGroupe);

                //Création du formulaire et hydratation avec le groupe à modifier
                $trancheGroupeForm = $this->getTrancheGroupeService()->getTrancheGroupeForm(true);
                $trancheGroupeForm->bind($trancheGroupe);
            } else {
                //Envoi du formulaire par le bouton submit

                //Création du formulaire et hydratation avec le groupe modifié
                $trancheGroupeForm = $this->getTrancheGroupeService()->getTrancheGroupeForm();
                //Suppression des informations inutiles au remplissage du formulaire
                unset($data['submit']);
                //Remplissage du formulaire avec les données saisies
                $trancheGroupeForm->setData($data);

                if ($trancheGroupeForm->isValid()) {
                    //Sauvegarde du groupe de tranche
                    $trancheGroupe = $trancheGroupeForm->getObject();
                    $this->getTrancheGroupeService()->saveTrancheGroupe($trancheGroupe);
                    $formIsValid = true;
                } else {
                    //Passer le formulaire en modification si nécessaire
                    $trancheGroupeForm = $this->getTrancheGroupeService()->setTrancheGroupeFormInModification(
                        $trancheGroupeForm
                    );
                }
            }
            //Création du formulaire
            $viewModel = new ViewModel(['form' => $trancheGroupeForm, 'formIsValid' => $formIsValid]);
            $viewModel->setTerminal(true);

            $viewModel->setTemplate('simulaides/tranche-revenu/modif-groupe-fiche.phtml');
            $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($viewModel);

           return new JsonModel($data);
        }

        return $this->response;
    }

    /**
     * Récupère le groupe à modifier
     *
     * @param $codeGroupe
     *
     * @return TrancheGroupe
     */
    private function getGroupeModifier($codeGroupe)
    {
        /** @var TrancheGroupe $groupe */
        $groupe = new TrancheGroupe();
        if (!empty($codeGroupe)) {
            //Récupération des informations du groupe
            $groupe = $this->getTrancheGroupeService()->getTrancheGroupe($codeGroupe);
        }

        return $groupe;
    }

    /**
     * Permet de retourner la liste des tranches de revenu du groupe
     * @return ViewModel
     */
    public function trancheListeAction()
    {
        //Récupération du code du groupe
        $codeGroupe = $this->getEvent()->getRouteMatch()->getParam('codeGroupe');
        //Récupération du groupe pour avoir son intitulé
        $trancheGroupe  = $this->getTrancheGroupeService()->getTrancheGroupe($codeGroupe);
        $intituleGroupe = '';
        if (!empty($trancheGroupe)) {
            $intituleGroupe = $trancheGroupe->getIntitule();
        }

        $model = new ViewModel(
            [
                'tabEntete'   => $this->getTrancheRevenuEnteteTableau(),
                'tabLignes'   => $this->getTrancheRevenuLignesTableau($codeGroupe),
                'codeGroupe'  => $codeGroupe,
                'libGroupe'   => $intituleGroupe,
                'isUserAdeme' => $this->isUserAdeme()
            ]
        );
        $model->setTerminal(true);

        $model->setTemplate('simulaides/tranche-revenu/tranche-liste.phtml');
        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Retourne les informations de l'entête du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des tranches de revenu
     *
     * @return array
     */
    private function getTrancheRevenuEnteteTableau()
    {
        $tabEntete = [];
        //Colonne code
        $tabEntete[] = new TableCellule('Code');
        //Colonne intitulé
        $tabEntete[] = new TableCellule('Intitulé');
        //Colonne 1 personne
        $tabEntete[] = new TableCellule('1 pers.');
        //Colones 2 -> 6 personnes
        for ($i = 2; $i <= 6; $i++) {
            $tabEntete[] = new TableCellule($i . ' pers.');
        }
        //Colonne Ajout par personne suuplémentaire
        $tabEntete[] = new TableCellule('Ajout par pers. supp.');

        //Colonne Modifier
        $tabEntete[] = new TableCellule('Modifier');
        //Colonne Supprimer
        $tabEntete[] = new TableCellule('Supprimer');

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des tranches de revenu
     *
     * @param string $codeGroupe Code du groupe de tranche
     *
     * @return array
     */
    private function getTrancheRevenuLignesTableau($codeGroupe)
    {
        $tabLignes = [];
        //Sélection des tranches de revenu du groupe
        $tabTrancheRevenu = $this->getTrancheRevenuService()->getTrancheRevenusParGroupe($codeGroupe);

        /** @var TrancheRevenu $trancheRevenu */
        foreach ($tabTrancheRevenu as $trancheRevenu) {
            $ligne = new TableLigne();
            $ligne->setId($trancheRevenu->getCodeTranche());

            //Cellule du code
            $cellule = new TableCellule($trancheRevenu->getCodeTranche(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            //Cellule de l'intitulé
            $cellule = new TableCellule($trancheRevenu->getIntitule(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            //Cellule 1 personne
            $cellule = new TableCellule($trancheRevenu->getMt1personne(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            //Cellule 2 personnes
            $cellule = new TableCellule($trancheRevenu->getMt2personne(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            //Cellule 3 personnes
            $cellule = new TableCellule($trancheRevenu->getMt3personne(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            //Cellule 4 personnes
            $cellule = new TableCellule($trancheRevenu->getMt4personne(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            //Cellule 5 personnes
            $cellule = new TableCellule($trancheRevenu->getMt5personne(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            //Cellule 6 personnes
            $cellule = new TableCellule($trancheRevenu->getMt6personne(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            //Cellule personne supplémentaire
            $cellule = new TableCellule($trancheRevenu->getMtPersonneSupp(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);
            if (($trancheRevenu->getCodeGroupe() !== 'ANAH') || ($this->isUserAdeme())) {
                //Cellule de la modification
                $lien    = "javascript:modifFicheTrancheRevenu('" . $trancheRevenu->getCodeTranche() . "');";
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_MODIF, $lien);
                $ligne->addCellule($cellule);
                //Cellule de la suppression
                $lien    = "javascript:deleteTrancheRevenu('" . $trancheRevenu->getCodeTranche() . "');";
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_SUPP, $lien);
                $ligne->addCellule($cellule);
            } else {
                //Cellule de la modification
//                $lien    = "javascript:modifFicheTrancheRevenu('" . $trancheRevenu->getCodeTranche() . "');";
                $cellule = new TableCellule('', TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);
                //Cellule de la suppression
//                $lien    = "javascript:deleteTrancheRevenu('" . $trancheRevenu->getCodeTranche() . "');";
                $cellule = new TableCellule('', TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);
            }
            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;
        }

        return $tabLignes;
    }

    /**
     * Action d'ajout d'une tranche de revenu
     *
     * @return ViewModel
     */
    public function ajoutTrancheFicheAction()
    {
        //Indique que le formulaire est valide
        $formIsValid = false;

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            //Récupération des informations envoyées par la requête
            $data       = $request->getPost();
            $codeGroupe = $data['codeGroupe'];
            $isSubmit   = isset($data['submit']);

            if (!$isSubmit) {
                //Création du formulaire et hydratation avec la tranche
                $trancheRevenuForm = $this->getTrancheRevenuService()->getTrancheRevenuForm(false);
                $trancheRevenu     = new TrancheRevenu();
                //Affectation du groupe de tranche
                $trancheRevenu->setCodeGroupe($codeGroupe);
                $trancheRevenuForm->bind($trancheRevenu);

            } else {
                //Envoi du formulaire par le bouton submit

                //Création du formulaire et hydratation avec le groupe modifié
                $trancheRevenuForm = $this->getTrancheRevenuService()->getTrancheRevenuForm();
                //Suppression des informations inutiles au remplissage du formulaire
                unset($data['submit']);
                //Remplissage du formulaire avec les données saisies
                $trancheRevenuForm->setData($data);

                if ($trancheRevenuForm->isValid()) {
                    /** @var TrancheRevenu $trancheRevenu */
                    $trancheRevenu = $trancheRevenuForm->getObject();

                    //Vérification qu'aucune autre tranche n'existe avec cet identifiant
                    $trancheExistante = $this->getTrancheRevenuService()->getTrancheRevenu(
                        $trancheRevenu->getCodeTranche()
                    );
                    if (!empty($trancheExistante)) {
                        /** @var Element $eltCodeTranche */
                        $eltCodeTranche = $trancheRevenuForm->get('codeTranche');
                        $eltCodeTranche->setMessages(["L'identifiant de la tranche existe déjà."]);
                    } else {
                        //Sauvegarde du groupe de tranche

                        $groupeTranche = $this->getTrancheGroupeService()->getTrancheGroupe($codeGroupe);
                        $trancheRevenu->setGroupe($groupeTranche);
                        $this->getTrancheRevenuService()->saveTrancheRevenu($trancheRevenu);
                        $formIsValid = true;
                    }
                }
            }
            //Création du formulaire
            $viewModel = new ViewModel(
                [
                    'formIsValid' => $formIsValid,
                    'codeGroupe'  => $codeGroupe
                ]
            );
            $viewModel->setTemplate('simulaides/tranche-revenu/ajout-tranche-fiche.phtml');

            $viewModel->addChild(
                $this->getViewModelFicheTrancheRevenu($trancheRevenuForm, $codeGroupe),
                'ficheTrancheRevenu'
            );
            $viewModel->setTerminal(true);

            /** @var PhpRenderer $renderer */
            $renderer = $this->getServiceLocator()->get('ViewRenderer');
            //Indique qu'il y a des enfants dont le render sera fait dans la vue de la mère (renderChildModel)
            //Car le render sur la mère ne fait pas le render sur les enfants
            $renderer->setCanRenderTrees(true);

            $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($viewModel);

            return new JsonModel($data);
        }

        return $this->response;
    }

    /**
     * Action de modification d'une tranche de revenu
     *
     * @return ViewModel
     */
    public function modifTrancheFicheAction()
    {
        //Indique que le formulaire est valide
        $formIsValid = false;

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            //Récupération des informations envoyées par la requête
            $data       = $request->getPost();
            $codeGroupe = $data['codeGroupe'];
            $isSubmit   = isset($data['submit']);

            if (!$isSubmit) {
                //Appel de l'affichage de ma modale
                $codeTranche = $data['codeTranche'];

                //Recherche de la tranche que l'on souhaite afficher dans la modale
                $trancheRevenu = $this->getTrancheRevenuModifier($codeTranche);

                //Création du formulaire et hydratation avec la tranche à modifier
                $trancheRevenuForm = $this->getTrancheRevenuService()->getTrancheRevenuForm(true);
                $trancheRevenuForm->bind($trancheRevenu);
            } else {
                //Envoi du formulaire par le bouton submit

                //Création du formulaire et hydratation avec le groupe modifié
                $trancheRevenuForm = $this->getTrancheRevenuService()->getTrancheRevenuForm();
                //Suppression des informations inutiles au remplissage du formulaire
                unset($data['submit']);
                //Remplissage du formulaire avec les données saisies
                $trancheRevenuForm->setData($data);

                if ($trancheRevenuForm->isValid()) {
                    //Sauvegarde de la tranche de revenu
                    $trancheRevenu = $trancheRevenuForm->getObject();
                    $this->getTrancheRevenuService()->saveTrancheRevenu($trancheRevenu);
                    $formIsValid = true;
                } else {
                    //Passer le formulaire en modification si nécessaire
                    $trancheRevenuForm = $this->getTrancheRevenuService()->setTrancheRevenuFormInModification(
                        $trancheRevenuForm
                    );
                }
            }
            //Création du formulaire
            $viewModel = new ViewModel(
                [
                    'formIsValid' => $formIsValid,
                    'codeGroupe'  => $codeGroupe
                ]
            );
            $viewModel->addChild(
                $this->getViewModelFicheTrancheRevenu($trancheRevenuForm, $codeGroupe),
                'ficheTrancheRevenu'
            );
            $viewModel->setTerminal(true);
            $viewModel->setTemplate('simulaides/tranche-revenu/modif-tranche-fiche.phtml');

            /** @var PhpRenderer $renderer */
            $renderer = $this->getServiceLocator()->get('ViewRenderer');
            //Indique qu'il y a des enfants dont le render sera fait dans la vue de la mère (renderChildModel)
            //Car le render sur la mère ne fait pas le render sur les enfants
            $renderer->setCanRenderTrees(true);

            $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($viewModel);

            return new JsonModel($data);
        }

        return $this->response;
    }

    /**
     * Retourne la vue de la fiche de tranche de revenu
     *
     * @param $trancheRevenuForm
     * @param $codeGroupe
     *
     * @return ViewModel
     */
    private function getViewModelFicheTrancheRevenu($trancheRevenuForm, $codeGroupe)
    {
        $model = new ViewModel(
            [
                'form'       => $trancheRevenuForm,
                'codeGroupe' => $codeGroupe
            ]
        );
        $model->setTemplate('simulaides/tranche-revenu/tranche-fiche.phtml');

        return $model;
    }

    /**
     * Suppression d'une tranche de revenu
     *
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function trancheSuppAction()
    {
        try {
            $data = $this->trancheSupprime();
           return new JsonModel($data);
        } catch (\Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());
            return $this->response;
        }
    }

    /**
     * Permet de réaliser la suppression de la tranche
     * @return null
     */
    private function trancheSupprime()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $data    = null;
        if ($request->isPost()) {
            //Récupération des data envoyée
            $dataPost    = $request->getPost();
            $codeTranche = $dataPost->get('codeTranche');

            //Récupération de la tranche de revenu
            /** @var TrancheRevenu $trancheRevenu */
            $trancheRevenu = $this->getTrancheRevenuService()->getTrancheRevenu($codeTranche);

            //Récupération du code groupe pour le réaffichage de l'onglet
            $codeGroupe = $trancheRevenu->getCodeGroupe();

            //Suppression de la tranche de revenu
            $this->getTrancheRevenuService()->deleteTrancheRevenu($trancheRevenu);

            $data['ok']         = true;
            $data['codeGroupe'] = $codeGroupe;
        }

        return $data;
    }

    /**
     * Récupère la tranche à modifier
     *
     * @param $codeTranche
     *
     * @return array|TrancheRevenu
     */
    private function getTrancheRevenuModifier($codeTranche)
    {
        /** @var TrancheGroupe $groupe */
        $tranche = new TrancheRevenu();
        if (!empty($codeTranche)) {
            //Récupération des informations de la tranche
            $tranche = $this->getTrancheRevenuService()->getTrancheRevenu($codeTranche);
        }

        return $tranche;
    }

    /**
     * Retourne le service de groupe de tranche
     *
     * @return TrancheGroupeService
     */
    private function getTrancheGroupeService()
    {
        if (empty($this->trancheGroupeService)) {
            $this->trancheGroupeService = $this->getServiceLocator()->get(TrancheGroupeService::SERVICE_NAME);
        }

        return $this->trancheGroupeService;
    }

    /**
     * Retourne le service de groupe de tranche
     *
     * @return TrancheRevenuService
     */
    private function getTrancheRevenuService()
    {
        if (empty($this->trancheRevenuService)) {
            $this->trancheRevenuService = $this->getServiceLocator()->get(TrancheRevenuService::SERVICE_NAME);
        }

        return $this->trancheRevenuService;
    }

    /**
     * Sert a svoir si l'utilisateur a les droits Ademe
     * @return bool
     */
    private function isUserAdeme()
    {
        /** @var SessionContainerService $session */
        $session    = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        $conseiller = $session->getConseiller();

        return $conseiller->estConseillerADEME();
    }
}
