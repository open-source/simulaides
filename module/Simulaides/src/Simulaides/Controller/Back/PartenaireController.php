<?php
/**
 * Created by PhpStorm.
 * User: etude
 * Date: 4/11/18
 * Time: 9:07 AM
 */

namespace Simulaides\Controller\Back;


use Doctrine\ORM\Internal\Hydration\ObjectHydrator;
use Simulaides\Constante\TableConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Partenaire;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Service\PartenaireService;
use Simulaides\Service\SessionContainerService;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class PartenaireController extends SessionController implements IBackController
{

    function indexAction(){
        if(!$this->aRoleADEME()){
            $model = new ViewModel();
            $model->setTemplate('error/403-admin');
            return $model;
        }
        return new JsonModel([
            'htmlView' => $this->renderPartenaireTable('simulaides/partenaire/index.phtml')
        ]);
    }

    public function addAction(){
        if(!$this->aRoleADEME()){
            $model = new ViewModel();
            $model->setTemplate('error/403-admin');
            return $model;
        }
        $request = $this->getRequest();
        /** @var PartenaireService $partenaireService */
        $partenaireService = $this->getServiceLocator()->get(PartenaireService::SERVICE_NAME);

        $partenaire = new Partenaire();

        $form = $partenaireService->getPartenaireForm();
        $form->bind($partenaire);

        if($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $partenaireService->save($partenaire);

                $data = [
                    'htmlView' => $this->renderPartenaireTable(),
                    'formIsValid'=> true
                ];

                return new JsonModel($data);
            }
        }

        $form->get('submit')->setValue("Enregistrer");
        $model = new ViewModel([
            'form' => $form
        ]);
        $model->setTemplate('simulaides/partenaire/ajout-partenaire-modal.phtml');
        $model->setTerminal(true);

        $data = [
            'htmlView' => $this->getServiceLocator()->get('ViewRenderer')->render($model),
            'formIsValid'=> false
        ];


        return new JsonModel($data);
    }

    public function editAction(){
        if(!$this->aRoleADEME()){
            $model = new ViewModel();
            $model->setTemplate('error/403-admin');
            return $model;
        }
        //Récupération de l'id dispositif à modifier
        $idPartenaire = $this->getEvent()->getRouteMatch()->getParam('idPartenaire');

        $request = $this->getRequest();
        /** @var PartenaireService $partenaireService */
        $partenaireService = $this->getServiceLocator()->get(PartenaireService::SERVICE_NAME);

        $partenaire = $partenaireService->getPartenaireById($idPartenaire);

        $form = $partenaireService->getPartenaireForm($partenaire);
        $form->bind($partenaire);


        if($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $partenaireService->getEntityManager()->flush();

                $data = [
                    'htmlView' => $this->renderPartenaireTable(),
                    'formIsValid'=>true
                ];

                return new JsonModel($data);
            }
        }


        $form->get('submit')->setValue("Enregistrer");
        $model = new ViewModel([
            'form' => $form,
        ]);
        $model->setTemplate('simulaides/partenaire/ajout-partenaire-modal.phtml');
        $model->setTerminal(true);

        $data = [
            'htmlView' => $this->getServiceLocator()->get('ViewRenderer')->render($model),
            'formIsValid'=> false
        ];

        return new JsonModel($data);
    }

    public function deleteAction(){
        if(!$this->aRoleADEME()){
            $model = new ViewModel();
            $model->setTemplate('error/403-admin');
            return $model;
        }
        //Récupération de l'id dispositif à modifier
        $idPartenaire = $this->getEvent()->getRouteMatch()->getParam('idPartenaire');

        /** @var PartenaireService $partenaireService */
        $partenaireService = $this->getServiceLocator()->get(PartenaireService::SERVICE_NAME);


        /** @var Partenaire $partenaire */
        $partenaire = $partenaireService->getPartenaireById($idPartenaire);

        $partenaireService->delete($partenaire);

        $data = ['htmlView' => $this->renderPartenaireTable()];

        return new JsonModel($data);
    }


    public function getPartenaireTable()
    {
        /** @var PartenaireService $partenaireService */
        $partenaireService = $this->getServiceLocator()->get(PartenaireService::SERVICE_NAME);
        $partenaires = $partenaireService->getAllPartenaire();

        $tabEntete = [];
        $tabEntete[] = new TableCellule('Nom');
        $tabEntete[] = new TableCellule('Email');
        $tabEntete[] = new TableCellule('Domaine');
        $tabEntete[] = new TableCellule('Clé IFrame');
        $tabEntete[] = new TableCellule('Clé API');
        $tabEntete[] = new TableCellule('Région');
        $tabEntete[] = new TableCellule('Modifier');
        $tabEntete[] = new TableCellule('Supprimer');


        $tabLignes = [];
        foreach ($partenaires as $partenaire) {
            $ligne = new TableLigne();

            $cellule = new TableCellule($partenaire->getNom(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            $cellule = new TableCellule($partenaire->getEmail(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            $cellule = new TableCellule($partenaire->getUrl(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            $cellule = new TableCellule($partenaire->getCle(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            $cellule = new TableCellule($partenaire->getCleApi(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            $cellule = new TableCellule($partenaire->getRegion() ? $partenaire->getRegion()->getNom() : "", TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule de la modification
            $lien = "javascript:modifPartenaire(" . $partenaire->getId() . ")";
            $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_MODIF, $lien);
            $ligne->addCellule($cellule);

            //Cellule de suppression
            $lien = "javascript:deletePartenaire(" . $partenaire->getId() . ")";
            $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_SUPP, $lien);
            $ligne->addCellule($cellule);


            $tabLignes[] = $ligne;
        }

        return [
            'tabEntete' => $tabEntete,
            'tabLignes' => $tabLignes
        ];
    }

    function renderPartenaireTable($template = 'simulaides/partenaire/table-partenaire.phtml'){
        $table = $this->getPartenaireTable();
        $model = new ViewModel($table);

        $model->setTemplate($template);
        $model->setTerminal(true);

        return $this->getServiceLocator()->get('ViewRenderer')->render($model);
    }

    function aRoleADEME(){
        //Certains onglets sont visibles si l'utilisateur est ADEME
        /** @var SessionContainerService $session */
        $session     = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        $conseiller  = $session->getConseiller();
        return $conseiller->aRoleADEME();
    }
}