<?php
namespace Simulaides\Controller\Back;

use Exception;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\TableConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\Groupe;
use Simulaides\Domain\Entity\PerimetreGeo;
use Simulaides\Domain\Entity\Region;
use Simulaides\Domain\Entity\ValeurListe;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Form\Dispositif\DispositifFieldset;
use Simulaides\Form\Dispositif\DispositifGestionForm;
use Simulaides\Form\Dispositif\ResultatDispositifPDFForm;
use Simulaides\Service\DispositifService;
use Simulaides\Service\GenerePdfService;
use Simulaides\Service\GroupeService;
use Simulaides\Service\LocalisationService;
use Simulaides\Service\PerimetreGeoService;
use Simulaides\Service\TravauxService;
use Simulaides\Service\ValeurListeService;
use Simulaides\Tools\Utils;
use Zend\Form\Element;
use Zend\Form\Element\Select;
use Zend\Form\Form;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Stdlib\ArrayObject;
use Zend\Stdlib\ResponseInterface;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;


/**
 * Classe DispositifGestionController
 * Permet de gérer le paramétrage des dispositifs => Liste / Fiche / Onglet Gestion
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class DispositifGestionController extends SessionController implements IBackController
{
    /** @var  DispositifService */
    private $dispositifService;

    /** @var  GroupeService */
    private $groupeService;

    /** @var  LocalisationService */
    private $localisationService;

    /** @var  PerimetreGeoService */
    private $perimetreGeoService;

    /** @var  ValeurListeService */
    private $valeurListeService;

    /** @var  String */
    private $msgOK;

    /** @var  boolean */
    private $isCommitAdd;

    /** @var  int */
    private $idDispositifAdded;

    /** @var  ResultatDispositifPDFForm */
    private $formPdf;

    /**
     * Permet de retourner l'affichage de la liste des dispositifs
     * @return array|ViewModel
     * @throws Exception
     */
    public function dispositifListeAction()
    {
        //Récupération de la région du conseiller pour initialiser la liste
        $session = $this->getSessionService();
        /** @var Request $request */
        $request = $this->getRequest();

        $onlyTableau    = false;
        $intitule       = null;
        $financeur      = null;
        $typeDispositif = null;
        $etatDispositif = null;
        $validite       = null;
        $codeTravaux    = null;
        $commune        = null;
        $departement    = null;

        if ($request->isPost()) {
            $dataPost = $request->getPost();
            //Récupération de la région sélectionnée dans la liste
            $codeRegion = $dataPost['codeRegion'];
            //Récupération de l'information de rafraîchissement du tableau uniquement

            $onlyTableau    = isset($dataPost['onlyTableau']) ? $dataPost['onlyTableau'] : false;
            $intitule       = isset($dataPost['intitule']) ? $dataPost['intitule'] : null;
            $financeur      = isset($dataPost['financeur']) ? $dataPost['financeur'] : null;
            $typeDispositif = isset($dataPost['typeDisp']) ? $dataPost['typeDisp'] : null;
            $etatDispositif = isset($dataPost['etatDisp']) ? $dataPost['etatDisp'] : null;
            $validite       = isset($dataPost['validite']) ? $dataPost['validite'] : null;
            $codeTravaux    = isset($dataPost['codeTravaux']) ? $dataPost['codeTravaux'] : null;

            $codeDepartement    = isset($dataPost['codeDepartement']) ? $dataPost['codeDepartement'] : null;
            $codeCommune        = isset($dataPost['codeCommune']) ? $dataPost['codeCommune'] : null;

            $session->setFiltresDispositifRegion($codeRegion);
            $session->setFiltresDispositifIntitule($intitule);
            $session->setFiltresDispositifFinanceur($financeur);
            $session->setFiltresDispositifType($typeDispositif);
            $session->setFiltresDispositifEtat($etatDispositif);
            $session->setFiltresDispositifValidite($validite);
            $session->setFiltresDispositifCodeTravaux($codeTravaux);
            $session->setFiltresDispositifCodeDepartement($codeDepartement);
            $session->setFiltresDispositifCodeCommune($codeCommune);
        } else {
            //Récupération du code région en cours d'affichage si on vient de la fiche
            $codeRegion = $this->getEvent()->getRouteMatch()->getParam('codeRegion');
            if (empty($codeRegion)) {
                $codeRegion = $session->getFiltresDispositifRegion();
                if (empty($codeRegion)) {
                    $codeRegion = $session->getConseiller()->getCodeRegion();
                }
            }

            $intitule       = $session->getFiltresDispositifIntitule();
            $financeur      = $session->getFiltresDispositifFinanceur();
            $typeDispositif = $session->getFiltresDispositifType();
            $etatDispositif = $session->getFiltresDispositifEtat();
            $validite       = $session->getFiltresDispositifValidite();
            $codeTravaux    = $session->getFiltresDispositifCodeTravaux();
            $codeDepartement = $session->getFiltresDispositifCodeDepartement();
            $codeCommune = $session->getFiltresDispositifCodeCommune();
        }

        //Recherche de la région
        /** @var LocalisationService $localService */
        $localService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
        $region       = $localService->getRegionParCode($codeRegion);
        if($codeDepartement){
            $departement  = $localService->getDepartementParCode($codeDepartement);
        }
        if($codeCommune){
            $commune      = $localService->getCommuneParCode($codeCommune);
        }

        if (!$onlyTableau) {
            /** @var ValeurListeService $lovService */
            $lovService = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);
            /** @var TravauxService $travauxService */
            $travauxService = $this->getServiceLocator()->get(TravauxService::SERVICE_NAME);

            //Affectation de la région du conseiller au formulaire
            $data = [
                'codeRegion' => $region,
                'intitule'   => $intitule,
                'financeur'  => $financeur,
                'etatDisp'   => $lovService->getValeurPourCodeListeEtCode(ValeurListeConstante::L_ETAT_DISPOSITIF, $etatDispositif),
                'typeDisp'   => $lovService->getValeurPourCodeListeEtCode(ValeurListeConstante::L_TYPE_DISPOSITIF, $typeDispositif),
                'travaux'    => $travauxService->getTravauxByCode($codeTravaux),
                'validite'   => $validite,
                'codeDepartement' => $departement,
                'codeCommune' => $commune

            ];
            $form = $this->getDispositifService()->getFiltreDispositifForm();
            $form->setData($data);

            //Création de la vue
            $model = new ViewModel(
                [
                    'form' => $form
                ]
            );
            $model->addChild(
                $this->getViewModelTableau(
                    $region,
                    $intitule,
                    $financeur,
                    $typeDispositif,
                    $etatDispositif,
                    $validite,
                    $codeTravaux,
                    $codeDepartement,
                    $codeCommune
                ),
                'tableDispositif'
            );

            return $model;
        } else {
            $model = $this->getViewModelTableau($region, $intitule, $financeur, $typeDispositif, $etatDispositif,
                                                $validite,$codeTravaux,$codeDepartement,$codeCommune);
            $model->setTerminal(true);
            $data['model'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

            return new JsonModel($data);
        }
    }

    /**
     * Permet de retourner la vue du tableau
     *
     * @param Region $region
     *
     * @param null   $intitule
     * @param null   $financeur
     * @param null   $typeDispositif
     * @param null   $etatDispositif
     * @param null   $validite
     * @param null   $codeTravaux
     *
     * @return ViewModel
     * @throws Exception
     */
    private function getViewModelTableau(
        $region,
        $intitule = null,
        $financeur = null,
        $typeDispositif = null,
        $etatDispositif = null,
        $validite = null,
        $codeTravaux = null,
        $codeDepartement = null,
        $codeCommune = null
    ) {
        if (!empty($region)) {
            $codeRegion = $region->getCode();
            $nomRegion  = $region->getNom();
        } else {
            $codeRegion = '';
            $nomRegion  = '';
        }

        $conseiller = $this->getSessionService()->getConseiller();
        $lignes     = $this->getDispositifLignesTableau($codeRegion, $intitule, $financeur, $typeDispositif,
                                                        $etatDispositif, $validite,$codeTravaux,$codeDepartement,
                                                        $codeCommune);
        //Création de la vue
        $model = new ViewModel(
            [
                'region'             => $nomRegion,
                'codeRegion'         => $codeRegion,
                'typeDispositif'     => $typeDispositif,
                'etatDispositif'     => $etatDispositif,
                'estConseillerADEME' => $conseiller->estConseillerADEME(),
                'tabEntete'          => $this->getDispositifEnteteTableau(),
                'tabLignes'          => $lignes,
                'phpsessid'          => $this->getSessionService()->getManager()->getId(),
                'codeTravaux'        => $codeTravaux,
                'codeCommune'        => $codeCommune,
                'codeDepartement'    => $codeDepartement
            ]
        );
        $model->setTemplate('simulaides/dispositif-gestion/dispositif-tableau.phtml');

        return $model;
    }

    /**
     * Retourne les informations de l'entête du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des dispositifs
     *
     * @return array
     */
    private function getDispositifEnteteTableau()
    {
        $tabEntete = [];
        //Colonne Case à cocher
        $caseSelectAll = new TableCellule('select-deselect-tout', TableConstante::TYPE_CASE_COCHER);
        $caseSelectAll->setTitle('Sélectionner/Désélectionner tout');
        $tabEntete[] = $caseSelectAll;
        //Colonne Identifiant
        $tabEntete[] = new TableCellule('N°');
        //Colonne Intitulé
        $tabEntete[] = new TableCellule('Intitulé');
        //Colonne Financeur
        $tabEntete[] = new TableCellule('Financeur');
        //Colonne Etat
        $tabEntete[] = new TableCellule('Etat');
        //Colonne Etat
        $tabEntete[] = new TableCellule('Type de dispositif');
        //Colonne Début validité
        $tabEntete[] = new TableCellule('Début validité');
        //Colonne Fin validité
        $tabEntete[] = new TableCellule('Fin validité');
        //Colonne Modifier
        $tabEntete[] = new TableCellule('Modifier');
        //Colonne Désactiver/Soummettre à validation/Valider
        $tabEntete[] = new TableCellule('Action');

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des dispositifs
     *
     * @param string $codeRegion Code de la région choisie
     *
     * @param null   $intitule
     * @param null   $financeur
     * @param null   $typeDispositif
     * @param null   $etatDispositif
     * @param null   $validite
     * @param null   $codeTavaux
     * @param null   $codeDepartement
     * @param null   $codeCommune
     *
     * @return array
     * @throws Exception
     */
    private function getDispositifLignesTableau(
        $codeRegion,
        $intitule = null,
        $financeur = null,
        $typeDispositif = null,
        $etatDispositif = null,
        $validite = null,
        $codeTavaux = null,
        $codeDepartement = null,
        $codeCommune = null
    ) {
        $tabLignes = [];
        //Sélection des dispositifs de la région
        $tabDispositif = $this->getDispositifService()->searchDispositif(
            $codeRegion,
            $intitule,
            $financeur,
            $typeDispositif,
            $etatDispositif,
            $validite,
            $codeTavaux,
            $codeDepartement,
            $codeCommune
        );
        if (!empty($typeDispositif) || !empty($etatDispositif)) {
            usort(
                $tabDispositif,
                function ($a, $b) {
                    return strcmp($a->getIntitule(), $b->getIntitule());
                }
            );
        }
        /** @var Dispositif $dispositif */
        foreach ($tabDispositif as $dispositif) {
            if ((empty($typeDispositif) || $dispositif->getTypeDispositif()->getCode() === $typeDispositif)
                && (empty($etatDispositif) || $dispositif->getEtat()->getCode() === $etatDispositif)) {
                $ligne = new TableLigne();
                $ligne->setId($dispositif->getId());

                //Cellule de la case à cocher
                $cellule = new TableCellule('', TableConstante::TYPE_CASE_COCHER);
                $ligne->addCellule($cellule);
                //Cellule de l'indentifiant
                $cellule = new TableCellule($dispositif->getId(), TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);
                //Cellule de l'intitulé
                $cellule = new TableCellule($dispositif->getIntitule(), TableConstante::TYPE_TEXTE);
                $cellule->setTitle($dispositif->getIntitule());
                $ligne->addCellule($cellule);

                //Cellule du financeur
                $cellule = new TableCellule($dispositif->getFinanceur(), TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule de l'état
                $etat = $dispositif->getEtat();
                if (!empty($etat)) {
                    $etat = $etat->getLibelle();
                }
                $cellule = new TableCellule($etat, TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule du type
                $type = $dispositif->getTypeDispositif();
                if (!empty($type)) {
                    $type = $type->getLibelle();
                }
                $cellule = new TableCellule($type, TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule du début de validité
                $dateDebut = Utils::formatDateToString($dispositif->getDebutValidite());
                $cellule   = new TableCellule($dateDebut, TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule de fin de validité
                $dateFin = Utils::formatDateToString($dispositif->getFinValidite());
                $cellule = new TableCellule($dateFin, TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule de la modification
                $lien    = $this->url()->fromRoute(
                    'admin/dispositif-modif',
                    ['codeRegion' => $codeRegion, 'idDispositif' => $dispositif->getId()],
                    [
                        'query' => [
                            'PHPSESSID' => $this->getSessionService()->getManager()->getId()
                        ]
                    ]
                );
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_MODIF, $lien);
                $ligne->addCellule($cellule);

                //Cellule du bouton Désactiver/Soummettre à validation/Valider
                //Vérification que le conseiller est ADEME
                $conseiller       = $this->getSessionService()->getConseiller();
                $isUserAdeme      = $conseiller->estConseillerADEME();
                $aRoleAdeme       = $conseiller->aRoleADEME();
                $aDroitValidation = ($aRoleAdeme || (
                        ($dispositif->getCodeTypeDispositif() != ValeurListeConstante::TYPE_DISPOSITIF_NATIONAL) && $isUserAdeme));

                /** @var ValeurListe $etat */
                $etat             = $dispositif->getEtat();
                $actionDispositif = '';
                $lien             = '';
                if (!empty($etat)) {
                    $codeEtat = $etat->getCode();
                    if (($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_ACTIVE) && $aDroitValidation) {
                        $lien             = "javascript:changeEtatDesactive(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $actionDispositif = 'Désactiver';
                        $cellule          = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    } elseif (($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_AVALIDER) && $aDroitValidation) {
                        $lienActive    = "javascript:changeEtatActive(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $lienDesactive = "javascript:changeEtatDesactive(" . $dispositif->getId() . ", " . $codeRegion . ");";

                        $texteLiens = '<a href="' . $lienActive . '">Activer</a><a></a><br><a href="' . $lienDesactive . '">Désactiver</a>';
                        $cellule    = new TableCellule($texteLiens, TableConstante::TYPE_TEXTE);
                    } elseif ($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_AVALIDER) {
                        $lien             = "javascript:changeEtatDesactive(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $actionDispositif = 'Désactiver';
                        $cellule          = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    } elseif (($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE) && $aDroitValidation) {
                        $lien             = "javascript:changeEtatActive(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $actionDispositif = 'Valider';
                        $cellule          = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    } elseif ($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE) {
                        $lien             = "javascript:changeEtatAValider(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $actionDispositif = 'Demander la validation';
                        $cellule          = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    } else {
                        $cellule = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    }
                } else {
                    $cellule = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                }

                $ligne->addCellule($cellule);

                //ajout de la ligne à la liste des lignes
                $tabLignes[] = $ligne;
            }
        }

        return $tabLignes;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des dispositifs
     *
     * @param string $codeRegion Code de la région choisie
     *
     * @return array
     */
    private function getDispositifsForExport($codeRegion, $typeDispositif = null, $etatDispositif)
    {
        $tabLignes = [];
        //Sélection des dispositifs de la région
        $tabDispositif = $this->getDispositifService()->getDispositifParCodeRegion($codeRegion,true);
        if(!empty($typeDispositif)||!empty($etatDispositif)){
            usort($tabDispositif, function($a, $b) {
                return strcmp($a->getIntitule(), $b->getIntitule());
            });
        }
        /** @var Dispositif $dispositif */
        foreach ($tabDispositif as $dispositif) {
            if((empty($typeDispositif)||$dispositif->getTypeDispositif()->getCode() === $typeDispositif)
                && (empty($etatDispositif)||$dispositif->getEtat()->getCode()           === $etatDispositif)) {
                $ligne = new TableLigne();
                $ligne->setId($dispositif->getId());

                //Cellule de l'intitulé
                $cellule = new TableCellule($dispositif->getIntitule(), TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule du financeur
                $cellule = new TableCellule($dispositif->getFinanceur(), TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule de l'état
                $etat = $dispositif->getEtat();
                if (!empty($etat)) {
                    $etat = $etat->getLibelle();
                }
                $cellule = new TableCellule($etat, TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule du type
                $type = $dispositif->getTypeDispositif();
                if (!empty($type)) {
                    $type = $type->getLibelle();
                }
                $cellule = new TableCellule($type, TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule du début de validité
                $dateDebut = Utils::formatDateToString($dispositif->getDebutValidite());
                $cellule   = new TableCellule($dateDebut, TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule de fin de validité
                $dateFin = Utils::formatDateToString($dispositif->getFinValidite());
                $cellule = new TableCellule($dateFin, TableConstante::TYPE_TEXTE);
                $ligne->addCellule($cellule);

                //Cellule de la modification
                $lien    = $this->url()->fromRoute(
                    'admin/dispositif-modif',
                    ['codeRegion' => $codeRegion, 'idDispositif' => $dispositif->getId()],
                    [
                        'query' => [
                            'PHPSESSID' => $this->getSessionService()->getManager()->getId()
                        ]
                    ]
                );
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_MODIF, $lien);
                $ligne->addCellule($cellule);

                //Cellule du bouton Désactiver/Soummettre à validation/Valider
                //Vérification que le conseiller est ADEME
                $conseiller = $this->getSessionService()->getConseiller();
                $isUserAdeme = $conseiller->estConseillerADEME();
                $aRoleAdeme = $conseiller->aRoleADEME();
                $aDroitValidation = ($aRoleAdeme || (($dispositif->getCodeTypeDispositif() != ValeurListeConstante::TYPE_DISPOSITIF_NATIONAL) && $isUserAdeme));


                /** @var ValeurListe $etat */
                $etat = $dispositif->getEtat();
                $actionDispositif = '';
                $lien = '';
                if (!empty($etat)) {
                    $codeEtat = $etat->getCode();
                    if (($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_ACTIVE) && $aDroitValidation) {
                        $lien = "javascript:changeEtatDesactive(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $actionDispositif = 'Désactiver';
                        $cellule = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    } elseif (($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_AVALIDER) && $aDroitValidation) {
                        $lienActive = "javascript:changeEtatActive(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $lienDesactive = "javascript:changeEtatDesactive(" . $dispositif->getId() . ", " . $codeRegion . ");";

                        $texteLiens = '<a href="' . $lienActive . '">Activer</a><a> / </a><a href="' . $lienDesactive . '">Désactiver</a>';
                        $cellule = new TableCellule($texteLiens, TableConstante::TYPE_TEXTE);
                    } elseif ($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_AVALIDER) {
                        $lien = "javascript:changeEtatDesactive(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $actionDispositif = 'Désactiver';
                        $cellule = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    } elseif (($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE) && $aDroitValidation) {
                        $lien = "javascript:changeEtatActive(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $actionDispositif = 'Valider';
                        $cellule = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    } elseif ($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE) {
                        $lien = "javascript:changeEtatAValider(" . $dispositif->getId() . ", " . $codeRegion . ");";
                        $actionDispositif = 'Demander la validation';
                        $cellule = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    } else{
                        $cellule = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                    }
                } else {
                    $cellule = new TableCellule($actionDispositif, TableConstante::TYPE_LIEN, $lien);
                }

                $ligne->addCellule($cellule);

                //ajout de la ligne à la liste des lignes
                $tabLignes[] = $ligne;
            }
        }

        return $tabLignes;
    }

    /**
     * Permet de mémoriser le changement d'ordre du dispositif
     *
     * @return ResponseInterface|JsonModel
     */
    public function dispositifChangeOrdreAction()
    {
        try {
            //Vérification qu'un conseiller est connecté
            $data = $this->changeOrdre();

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }

    }

    /**
     * Permet de réaliser le changement de l'ordre
     *
     * @return null
     * @throws Exception
     */
    private function changeOrdre()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $data    = null;
        if ($request->isPost()) {
            //Récupération des data envoyée
            $dataPost     = $request->getPost();
            $idDispositif = $dataPost['idDispositif'];
            $gap          = $dataPost['gap'];
            $codeRegion   = $dataPost['codeRegion'];

            //Récupération du dispositif déplacé
            /** @var Groupe $groupeInit */
            $groupeInit = $this->getGroupeService()->getGroupeParCodeRegionEtId($codeRegion, $idDispositif);
            if (!empty($groupeInit)) {
                $oldOrder = $groupeInit->getNumeroOrdre();
                $newOrder = $oldOrder + $gap;

                //Modification du groupe initial avec son nouveau numéro d'ordre
                $groupeInit->setNumeroOrdre($newOrder);
                $this->getGroupeService()->saveGroupe($groupeInit);

                //Modification des autres dispositifs dans le groupe
                $this->getGroupeService()->changeOrdreAutreGroupe(
                    $codeRegion,
                    $idDispositif,
                    $gap,
                    $oldOrder,
                    $newOrder
                );
            }
            $data['ok'] = true;
        }

        return $data;
    }

    /**
     * Action appelée pour exporter les dispositifs
     *
     * @return Response|Response\Stream
     * @throws Exception
     */
    public function exportDispositifAction()
    {
        /** @var Request $request */
        $request    = $this->getRequest();
        $codeRegion = $this->getEvent()->getRouteMatch()->getParam('codeRegion');

        if (!empty($codeRegion) && $request->isPost()) {
            $dataPost      = $request->getPost();
            $idDispositifs = array_keys($dataPost->toArray());

            if (!empty($idDispositifs)) {

                /** @var LocalisationService $localService */
                $localService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
                $region       = $localService->getRegionParCode($codeRegion);

                /* Appel au service PDF */
                /** @var GenerePdfService $pdfService */
                $pdfService = $this->getServiceLocator()->get(GenerePdfService::SERVICE_NAME);
                $fileName   = $pdfService->genereResultatDispositifPdf($idDispositifs, $region);
                $response   = new Response\Stream();
                if (file_exists($fileName)) {
                    $fileContents = file_get_contents($fileName);
                    unlink($fileName);
                    /** @var Response $response */
                    $response = $this->getResponse();
                    $response->setContent($fileContents);
                    $headers = $response->getHeaders();
                    $headers->clearHeaders()
                        ->addHeaderLine('Content-Type', 'application/pdf;charset=UTF-8')
                        ->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($fileName) . '"')
                        ->addHeaderLine('Content-Length', strlen($fileContents));
                } else {
                    $response->setStatusCode(404);
                }

                return $response;
            }
        }
    }

    /**
     * Action appelée sur l'ajout d'un dispositif
     *
     * @return ViewModel
     */
    public function ajoutDispositifFicheAction()
    {
        //Récupération de la région en cours
        $codeRegion = $this->getRegionPourFicheDispositif();

        $model = new ViewModel(
            ['codeRegion' => $codeRegion, 'phpsessid' => $this->getSessionService()->getManager()->getId()]
        );
        $model->addChild($this->getViewModelFicheEnteteDispositif($codeRegion), 'ficheDispositifEntete');

        return $model;
    }

    /**
     * Action appelée sur la modification d'un dispositif
     *
     * @return ViewModel
     * @throws Exception
     */
    public function modifDispositifFicheAction()
    {
        //Récupération de la région en cours
        $codeRegion = $this->getRegionPourFicheDispositif();
        //Récupération du dispositif à modifier
        $idDispositif = $this->getEvent()->getRouteMatch()->getParam('idDispositif');
        /** @var Dispositif $dispositif */
        $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);

        //si le dispositif est à l'état "Désactivé" => modification
        //sinon => consultation.
        $modeConsultation = false;
        $etatDispositif   = $dispositif->getEtat();
        if (!empty($etatDispositif)) {
            $modeConsultation = ($etatDispositif->getCode() != ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE);
        }

        //Construction de la vue
        $model = new ViewModel(
            [
                'codeRegion'     => $codeRegion,
                'idDispositif'   => $idDispositif,
                'isConsultation' => ($modeConsultation ? '1' : '0'),
                'phpsessid'      => $this->getSessionService()->getManager()->getId()
            ]
        );
        //Ajout de la vue enfant pour l'entête
        $model->addChild($this->getViewModelFicheEnteteDispositif($codeRegion, $dispositif), 'ficheDispositifEntete');

        return $model;
    }

    /**
     * Permet de créer la vue de la fiche
     *
     * @param string $codeRegion Code de la région en cours de visualisation
     * @param null|Dispositif $dispositif Dispositif en cours de modif
     *
     * @return ViewModel
     */
    private function getViewModelFicheEnteteDispositif($codeRegion, $dispositif = null)
    {
        $titre = "Ajout d'un dispositif";

        if (!empty($dispositif)) {
            //Dans le cas d'une modification, récupération du dispositif pour le titre
            $titre = $dispositif->getIntitule();
        }

        $model = new ViewModel(
            [
                'codeRegion' => $codeRegion,
                'titre'      => $titre,
                'phpsessid'  => $this->getSessionService()->getManager()->getId()
            ]
        );

        $model->setTemplate('simulaides/dispositif-gestion/dispositif-fiche-entete.phtml');

        return $model;
    }

    /**
     * Permet de récupérer le code de la région
     * pour l'affichage de la fiche
     *
     * @return mixed|string
     */
    private function getRegionPourFicheDispositif()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataPost   = $request->getPost();
            $codeRegion = $dataPost['codeRegion'];
        } else {
            $codeRegion = $this->getEvent()->getRouteMatch()->getParam('codeRegion');
        }

        return $codeRegion;
    }

    /**
     * Action appelée sur l'affichage de l'onglet "Gestion dispositif"
     *
     * @return ViewModel
     * @throws Exception
     */
    public function dispositifFicheGestionAction()
    {
        set_time_limit(1800);
        /** @var Request $request */
        $request      = $this->getRequest();
        $isConsult    = false;
        $isSubmit     = false;
        $isPost       = $request->isPost();
        $idDispositif = null;
        $model        = null;
        $data         = [];

        //Récupération du formulaire
        $form = $this->getDispositifService()->getDispositifGestionForm();
        //ajout du champ phpsessid
        $form->add(
            [
                'name'       => 'PHPSESSID',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionService()->getManager()->getId()
                ]
            ]
        );

        //
        //Récupération des paramètres
        if ($isPost) {
            $dataPost     = $request->getPost();
            $isSubmit     = isset($dataPost['submit']);
            $idDispositif = $dataPost['dispositif']['id'];
            //Code de la région d'affichage de la liste
            $codeRegionListe = $dataPost['codeRegionListe'];
        } else {
            //Récupération de l'info de consultation au chargement
            $isConsult = $this->getEvent()->getRouteMatch()->getParam('isConsult');
            //Récupération de l'id dispositif à modifier
            $idDispositif = $this->getEvent()->getRouteMatch()->getParam('idDispositif');
            //Code de la région d'affichage de la liste
            $codeRegionListe = $this->getEvent()->getRouteMatch()->getParam('codeRegion');
        }
        //Mode modification : si un id est indiqué et si on est pas en consultation (état = DESACTIVE)
        $isModif = (!empty($idDispositif) && !$isConsult);

        $groupeInit = $this->getGroupeService()->getGroupeParCodeRegionEtId($codeRegionListe, $idDispositif);

        //récuperation des dispostifs inféreieurs.
        $listeDispositif = [];
        if($groupeInit){
            $listeDispositif = $this->getDispositifService()->getListeDispositifRequis($codeRegionListe,
                                                                                       $groupeInit->getNumeroOrdre());
        }
        //ajout du champs dispositif requis
        $fieldsets = $form->getFieldsets();
        $fieldsetsDispositif = $fieldsets['dispositif'];
        $fieldsetsDispositif->add(
            [
                'name'       => 'idDispositifRequis',
                'type'       => 'Simulaides\Components\OptionalSelect',
                'required'    => false,
                'allow_empty' => true,
                'options'    => [
                    'label'        => "Dispositif requis",
                    'empty_option' => 'Sélectionner un dispositif',
                    'allow_empty' => true,
                    'required'     => false,
                    'disable_inarray_validator' => true,
                    'value_options' => $listeDispositif

                ],
                'attributes' => [
                    'id'    => 'idDispositifRequis',
                    'class' => 'chosen-select form-control',
                    'required' => false
                ]
            ]
        );
        $fieldsets['dispositif'] = $fieldsetsDispositif;


        //Bind du formulaire
        $form = $this->processBindGestionForm($idDispositif, $form, $isConsult, $codeRegionListe);

        //Le formulaire a été envoyé
        if ($isSubmit) {
            $form = $this->processSubmitGestionForm($form);
        }

        if ($this->isCommitAdd) {
            //L'ajout est réalisé, redirection vers la fiche en modification
            $data['ajoutOK']   = true;
            $data['urlAction'] = $this->url()->fromRoute(
                'admin/dispositif-modif',
                ['codeRegion' => $codeRegionListe, 'idDispositif' => $this->idDispositifAdded],
                ['query' => ['PHPSESSID' => $this->getSessionService()->getManager()->getId()]]
            );
        } else {
            if($isConsult){
                $form->get('dispositif')->get('typeDispositif')->setAttribute('disabled','disabled');
                $form->get('dispositif')->get('idDispositifRequis')->setAttribute('disabled','disabled');
            }
            $tabChangeEtat = $this->getBtnChangeEtatVisibilite($idDispositif);
            //Affichage de l'onglet ou la modification est réalisée, ou une erreur est survenue
            //Affichage du formulaire
            $model = new ViewModel(
                [
                    'form'             => $form,
                    'isConsultation'   => $isConsult,
                    'isModification'   => $isModif,
                    'withBtnDesactive' => $tabChangeEtat['withBtnDesactive'],
                    'withBtnValide'    => $tabChangeEtat['withBtnValide'],
                    'withBtnAValide'   => $tabChangeEtat['withBtnAValide'],
                    'msgOK'            => $this->msgOK,
                    'phpsessid'        => $this->getSessionService()->getManager()->getId()
                ]
            );
            $model->setTerminal(true);
            $model->setTemplate('simulaides/dispositif-gestion/dispositif-fiche-gestion.phtml');

            $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);
        }

        //dans le cas d'un appel ajax en POST, on retourne les data en JSon
        return new JsonModel($data);
    }

    /**
     * Permet de connaitre les visibilités des boutons de changement d'état
     *
     * @param int $idDispositif Id Dispositif de la fiche
     *
     * @return array
     * @throws Exception
     */
    private function getBtnChangeEtatVisibilite($idDispositif)
    {
        $tabEtat['withBtnDesactive'] = false;
        $tabEtat['withBtnValide']    = false;
        $tabEtat['withBtnAValide']   = false;

        if (!empty($idDispositif)) {
            /** @var Dispositif $dispositif */
            $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);

            //Vérification que le conseiller est ADEME
            $conseiller = $this->getSessionService()->getConseiller();

            $isUserAdeme      = $conseiller->estConseillerADEME();
            $aRoleAdeme       = $conseiller->aRoleADEME();
            $aDroitValidation = ($aRoleAdeme || (($dispositif->getCodeTypeDispositif(
                        ) != ValeurListeConstante::TYPE_DISPOSITIF_NATIONAL) && $isUserAdeme));

            /** @var ValeurListe $etat */
            $etat = $dispositif->getEtat();
            if (!empty($etat)) {
                $codeEtat = $etat->getCode();

                $tabEtat['withBtnDesactive'] = ($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_AVALIDER) || ($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_ACTIVE && $aDroitValidation);
                $tabEtat['withBtnValide']    = (($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_AVALIDER || $codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE) && $aDroitValidation);
                $tabEtat['withBtnAValide']   = ($codeEtat == ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE && !$aDroitValidation);

            }
        }

        return $tabEtat;
    }

    /**
     * Permet de gérer le submit du formulaire
     *
     * @param DispositifGestionForm $form
     *
     * @return mixed
     * @throws Exception
     */
    private function processSubmitGestionForm($form)
    {
        /** @var Request $request */
        $request = $this->getRequest();

        $dataPost = $request->getPost();
        unset($dataPost['submit']);

        $typeDispositif = $dataPost['dispositif']['codeTypeDispositif'];
        $typePerimetre  = $dataPost['dispositif']['typePerimetre'];

        $form->setData($dataPost);

        if ($form->isValid()) {
            /** @var DispositifFieldset $fdtDispositif */
            $fdtDispositif = $form->getFieldsets()['dispositif'];
            /** @var Dispositif $dispositif */
            $dispositif = $fdtDispositif->getObject();

            $typeDispositif = $dispositif->getTypeDispositif() ? $dispositif->getTypeDispositif()->getCode() : $typeDispositif;
            $dispositif->setCodeTypeDispositif($typeDispositif);
            $dispositif->setDescriptif($dataPost['dispositif']['descriptif']);

            $idDispositifAvantSubmit = $dispositif->getId();

            //Définition du type de périmètre du dispositif (National, Départemental, communal..)
            $perimetreGeo = [];
            if (isset($dataPost['perimetregeo'])) {
                $perimetreGeo = $dataPost['perimetregeo'];
            }
            $typePerimetre = $this->getTypePerimetreDispositif($perimetreGeo);
            $dispositif->setTypePerimetre($typePerimetre);

            //Sélection des régions auxquelles ajouter le dispositif
            $tabRegionGoupe = $this->getRegionsGroupe($typePerimetre, $perimetreGeo);

            //Sauvegarde des données
            $this->getDispositifService()->saveFicheGestionDispositif(
                $dispositif,
                //#delete $this->getPerimetreGeoToSave($perimetreGeo, $typePerimetre),
                $perimetreGeo,
                $tabRegionGoupe
            );

            //Message de confirmation de la sauvegarde
            $this->msgOK = "L'enregistrement du dispositif a été réalisé.";

            //L'ajout a été réalisé
            if (empty($idDispositifAvantSubmit)) {
                $this->isCommitAdd       = true;
                $this->idDispositifAdded = $dispositif->getId();
            }

            //Remettre à "False" le champ caché d'indication de modification de données
            /** @var Element $eltChange */
            $eltChange = $form->get('existeChangement');
            $eltChange->setValue("false");
        } else {
            $typePerimetre = $this->getTypePerimetreDispositif($dataPost['perimetregeo']);
        }

        //Chargement des listes options des EPCI/Commune si nécessaire
        $form = $this->chargeListeFormPerimetreGeo($form, $typePerimetre, $dataPost['perimetregeo']);

        //Repositionnement à disabled des éléments qui doivent l'être
        //(on est pas en consultation ici car on arrive du submit)
        $disabledLoc = $this->getDisabledLocalisation($typePerimetre);
        $form        = $this->getDispositifService()->setDispositifGestionFormDisabled(
            $form,
            false,
            $typeDispositif,
            $disabledLoc
        );

        return $form;
    }

    /**
     * Retourne l'information de rendre inaccessible les listes communes et epci de la localisation
     *
     * @param $typePerimetre
     *
     * @return bool
     */
    private function getDisabledLocalisation($typePerimetre)
    {
        return ($typePerimetre != ValeurListeConstante::TYPE_PERIM_GEO_EPCI && $typePerimetre != ValeurListeConstante::TYPE_PERIM_GEO_COMMUNAL);
    }

    /**
     * Permet de retourner la liste des codes régions auxquelles il faut ajouter
     * Le dispositif dans le groupe de la région
     *
     * @param $typePerimetre
     * @param $tabPerimetreGeo
     *
     * @return array
     * @throws Exception
     */
    private function getRegionsGroupe($typePerimetre, $tabPerimetreGeo)
    {
        $tabRegions = [];
        /** @var LocalisationService $localisationService */
        $localisationService = $this->getLocalisationService();

        switch ($typePerimetre) {
            case ValeurListeConstante::TYPE_PERIM_GEO_NATIONAL:
                //toutes les régions sont concernées
                $tabRegions = $localisationService->getListeRegions();
                break;
            case ValeurListeConstante::TYPE_PERIM_GEO_REGIONAL:
                //Les régions du périmètre géo sont concernées
                $tabRegions = $localisationService->getRegionsParListeCodeRegion($tabPerimetreGeo['codeRegion']);
                break;
            case ValeurListeConstante::TYPE_PERIM_GEO_DEPARTEMENTAL:
                //Les régions contenant les départements du périmètre géo sont concernées
                $tabRegions = $localisationService->getRegionsParListeCodeDepartement(
                    $tabPerimetreGeo['codeDepartement']
                );
                break;
            case ValeurListeConstante::TYPE_PERIM_GEO_EPCI:
                //Les régions contenant les départements des communes
                //composant les EPCI du périmètre géo sont concernées
                $tabRegions = $localisationService->getRegionsParListeCodeEpci($tabPerimetreGeo['codeEpci']);
                break;
            case ValeurListeConstante::TYPE_PERIM_GEO_COMMUNAL:
                //Les régions contenant les départements des communes du périmètre géo sont concernées
                $tabRegions = $localisationService->getRegionsParListeCodeCommune($tabPerimetreGeo['codeCommune']);
                break;
        };

        return $tabRegions;
    }

    /**
     * Retourne les infos à mémoriser en base selon le type de périmètre
     *
     * @param $tabPerimetreGeo
     * @param $typePerimetre
     *
     * @return array
     */
    private function getPerimetreGeoToSave($tabPerimetreGeo, $typePerimetre)
    {
        $tabReturn = [];

        switch ($typePerimetre) {
            case ValeurListeConstante::TYPE_PERIM_GEO_NATIONAL:
                $tabReturn = [];
                break;
            case ValeurListeConstante::TYPE_PERIM_GEO_REGIONAL:
                $tabReturn = $tabPerimetreGeo['codeRegion'];
                break;
            case ValeurListeConstante::TYPE_PERIM_GEO_DEPARTEMENTAL:
                $tabReturn = $tabPerimetreGeo['codeDepartement'];
                break;
            case ValeurListeConstante::TYPE_PERIM_GEO_EPCI:
                $tabReturn = $tabPerimetreGeo['codeEpci'];
                break;
            case ValeurListeConstante::TYPE_PERIM_GEO_COMMUNAL:
                $tabReturn = $tabPerimetreGeo['codeCommune'];
                break;
        };

        return $tabReturn;
    }

    /**
     * Permet de définir le périmètre géographique du dispositif
     *
     * @param array $perimetreGeo Data du périmètre retourné par le formulaire
     *
     * @return string
     */
    private function getTypePerimetreDispositif($perimetreGeo)
    {
        //Par défaut : périmètre NATIONAL
        $typePerimetre = ValeurListeConstante::TYPE_PERIM_GEO_NATIONAL;
        if (!empty($perimetreGeo)) {
            if (isset($perimetreGeo['codeCommune'])) {
                $typePerimetre = ValeurListeConstante::TYPE_PERIM_GEO_COMMUNAL;
            } elseif (isset($perimetreGeo['codeEpci'])) {
                $typePerimetre = ValeurListeConstante::TYPE_PERIM_GEO_EPCI;
            } elseif (isset($perimetreGeo['codeDepartement'])) {
                $typePerimetre = ValeurListeConstante::TYPE_PERIM_GEO_DEPARTEMENTAL;
            } elseif (isset($perimetreGeo['codeRegion'])) {
                $typePerimetre = ValeurListeConstante::TYPE_PERIM_GEO_REGIONAL;
            }
        }

        return $typePerimetre;
    }

    /**
     * Permet de charger et hydrater le formulaire
     *
     * @param int                   $idDispositif
     * @param DispositifGestionForm $form
     * @param boolean               $isConsult       Indique si on est en mode consultation
     * @param string                $codeRegionListe Code de la région d'affichage de la liste
     *
     * @return mixed
     * @throws Exception
     */
    private function processBindGestionForm($idDispositif, $form, $isConsult, $codeRegionListe)
    {
        if (!empty($idDispositif)) {
            //Récupération du dispositif
            /** @var Dispositif $dispositif */
            $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);

            //Récupération des périmètres géographiques
            $tabPerimetreGeo = $this->getListeElementPerimetreGeo($dispositif);

            $typeDispositif = $dispositif->getTypeDispositif()->getCode();
            $typePerimetre  = $dispositif->getTypePerimetre();
            $disabledLoc    = $this->getDisabledLocalisation($typePerimetre);

            //Chargement des liste options des EPCI/Commune si nécessaire
            $form = $this->chargeListeFormPerimetreGeo($form, $typePerimetre, $tabPerimetreGeo);
        } else {
            //en mode ajout, mettre les valeurs par défaut

            /** @var Dispositif $dispositif */
            $dispositif = new Dispositif();

            //Dispositif Public par défaut
            $dispositif = $this->setTypeDispositif($dispositif, ValeurListeConstante::TYPE_DISPOSITIF_PUBLIC);

            //Etat DESACTIVE Par défaut
            $dispositif = $this->setEtatDispositif($dispositif, ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE);

            //Calculer le dispositif => Non par défaut
            $dispositif->setEvaluerRegle(false);
            //Exclut du calcul CEE => Non par défaut
            $dispositif->setExclutCEE(false);
            $dispositif->setExclutAnah(false);
            $dispositif->setExclutEPCI(false);
            $dispositif->setExclutRegion(false);
            $dispositif->setExclutCommunal(false);
            $dispositif->setExclutNational(false);
            $dispositif->setExclutDepartement(false);
            $dispositif->setExclutCoupDePouce(false);

            $tabPerimetreGeo = [];
            $typeDispositif  = SimulationConstante::TYPE_AIDE_PUBLIC;
            $disabledLoc     = true;
        }

        //Passage des champs en consultation
        $form = $this->getDispositifService()->setDispositifGestionFormDisabled(
            $form,
            $isConsult,
            $typeDispositif,
            $disabledLoc
        );

        $data['codeRegionListe'] = $codeRegionListe;
        $data['dispositif']      = $dispositif;
        $data['perimetregeo']    = $tabPerimetreGeo;
        //Hydratation du formulaire
        $form->bind(new ArrayObject($data));

        return $form;
    }

    /**
     * Permet de setter l'état au dispositif
     *
     * @param Dispositif $dispositif Dispositif à modifier
     * @param String     $codeEtat   Code de l'état
     *
     * @return mixed
     * @throws Exception
     */
    private function setEtatDispositif($dispositif, $codeEtat)
    {
        $etat = $this->getValeurListeService()->getValeurPourCodeListeEtCode(
            ValeurListeConstante::L_ETAT_DISPOSITIF,
            $codeEtat
        );
        $dispositif->setEtat($etat);
        $dispositif->setCodeEtat($codeEtat);

        return $dispositif;
    }

    /**
     * Permet de setter le type du dispositif
     *
     * @param Dispositif $dispositif Dispositif à modifier
     * @param string     $codeType   Code du type
     *
     * @return mixed
     * @throws Exception
     */
    private function setTypeDispositif($dispositif, $codeType)
    {
        $typeDispositif = $this->getValeurListeService()->getValeurPourCodeListeEtCode(
            ValeurListeConstante::L_TYPE_DISPOSITIF,
            $codeType
        );
        $dispositif->setCodeTypeDispositif($codeType);
        $dispositif->setTypeDispositif($typeDispositif);

        return $dispositif;
    }

    /**
     * Permet de remplir les select des EPCI/Commune si le périmètre est Dept/EPCI/Commune
     *     => Ces deux select sont vides par défaut car dépende de la liste des départements
     * Permet de remplir le select des Départements si le périmètre est Région
     *
     * @param Form   $form
     * @param string $typePerimetre
     * @param        $tabPerimetreGeo
     *
     * @return mixed
     * @throws Exception
     */
    private function chargeListeFormPerimetreGeo($form, $typePerimetre, $tabPerimetreGeo)
    {
        $chargeEpci    = ($typePerimetre != ValeurListeConstante::TYPE_PERIM_GEO_REGIONAL);
        $chargeCommune = ($typePerimetre != ValeurListeConstante::TYPE_PERIM_GEO_REGIONAL);
        $chargeDept    = ($typePerimetre == ValeurListeConstante::TYPE_PERIM_GEO_REGIONAL);

        if ($chargeEpci) {
            //Sélection de la liste des EPCI des départements sélectionnés
            $listEpci = $this->getLocalisationService()->getEpciPourListeDepartement(
                $tabPerimetreGeo['codeDepartement']
            );
            //Construction de la liste des options-values
            $listOptions = $this->getLocalisationService()->getListOptionsValuePourEpci($listEpci);
            /** @var Select $eltEpci */
            $eltEpci = $form->get('perimetregeo')->get('codeEpci');
            $eltEpci->setValueOptions($listOptions);

        }
        if ($chargeCommune) {
            //Sélection de la liste des communes des départements sélectionnés
            $listComunes = $this->getLocalisationService()->getCommunePourListeDepartement(
                $tabPerimetreGeo['codeDepartement']
            );
            //Construction de la liste des options-values
            $listOptions = $this->getLocalisationService()->getListOptionsValuePourCommune($listComunes);
            /** @var Select $eltCommune */
            $eltCommune = $form->get('perimetregeo')->get('codeCommune');
            $eltCommune->setValueOptions($listOptions);
        }
        if ($chargeDept) {
            //Sélection de la liste des départements des régions sélectionnées
            $listDept = $this->getLocalisationService()->getDepartementsPourListeRegion(
                $tabPerimetreGeo['codeRegion']
            );
            //Construction de la liste des options-values
            $listOptions = $this->getLocalisationService()->getListOptionsValuePourDepartement($listDept);
            /** @var Select $eltDept */
            $eltDept = $form->get('perimetregeo')->get('codeDepartement');
            $eltDept->setValueOptions($listOptions);
        }

        return $form;
    }

    /**
     * Retourne la liste des départements si le périmètre est
     *   - EPCI ou COMMUNAL
     * Il faut charger l'objet Select avec les départements des ECPI/Commune
     * Sinon l'affichage ne se fera pas pour les objet Select EPCI/Commune
     *   -> ces deux listes sont inactives et vides si aucun département
     *
     * @param Dispositif $dispositif
     * @param array      $tabPerimetreGeo
     *
     * @return mixed
     * @throws Exception
     */
    private function chargeListeDepartement($dispositif, $tabPerimetreGeo)
    {
        //Si le type de périmètre est Commune ou EPCI
        //Récupération des départements pour pouvoir réinitialiser la liste
        if ($dispositif->getTypePerimetre() == ValeurListeConstante::TYPE_PERIM_GEO_EPCI) {
            //Sélection des départements des EPCI choisies
            $tabPerimetreGeo['codeDepartement'] = $this->getLocalisationService()->getDepartementsPourListeEpci(
                $tabPerimetreGeo['codeEpci']
            );
        } elseif ($dispositif->getTypePerimetre() == ValeurListeConstante::TYPE_PERIM_GEO_COMMUNAL) {
            //Sélection des départements des communes choisies
            $tabPerimetreGeo['codeDepartement'] = $this->getLocalisationService()->getDepartementsPourListeCommune(
                $tabPerimetreGeo['codeCommune']
            );
        }

        return $tabPerimetreGeo;
    }

    /**
     * Retourne les éléments du périmètre géographique
     *
     * @param Dispositif $dispositif
     *
     * @return array
     * @throws Exception
     */
    public function getListeElementPerimetreGeo($dispositif)
    {
        $tabPerimetreGeo                    = [];
        $tabPerimetreGeo['codeRegion']      = [];
        $tabPerimetreGeo['codeDepartement'] = [];
        $tabPerimetreGeo['codeCommune']     = [];
        $tabPerimetreGeo['codeEpci']        = [];

        //Récupération des éléments du périmètre géographique du dispositif
        $result = $this->getPerimetreGeoService()->getPerimetreGeoPourDispositif($dispositif->getId());
        /** @var PerimetreGeo $perimetreGeo */
        foreach ($result as $perimetreGeo) {
            $codeRegion      = $perimetreGeo->getCodeRegion();
            $codeCommune     = $perimetreGeo->getCodeCommune();
            $codeDepartement = $perimetreGeo->getCodeDepartement();
            $codeEpci        = $perimetreGeo->getCodeEpci();
            //Seul un des éléments est renseigné pour une ligne de périmètre géographique
            if (!empty($codeRegion)) {
                $tabPerimetreGeo['codeRegion'][] = $codeRegion;
            } elseif (!empty($codeCommune)) {
                $tabPerimetreGeo['codeCommune'][] = $codeCommune;
            } elseif (!empty($codeDepartement)) {
                $tabPerimetreGeo['codeDepartement'][] = $codeDepartement;
            } elseif (!empty($codeEpci)) {
                $tabPerimetreGeo['codeEpci'][] = $codeEpci;
            }
        }

        //Dans le cas d'un périmètre sur EPCI/Commune, il faut récupérer les départements
        $tabPerimetreGeo = $this->chargeListeDepartement($dispositif, $tabPerimetreGeo);

        return $tabPerimetreGeo;
    }

    /**
     * Action appelée sur le changement d'une valeur département/région
     *
     * @return JsonModel
     */
    public function changeLocalisationAction()
    {
        $data = [];
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            //Récupération des paramètres
            $params = $request->getPost();
            //type de l'objet select qui a été modifié (région / département)
            $typeSelect = $params['type'];
            //Liste des valeurs qui ont été sélectionnées
            $tabValueSelect = $params['tabValue'];
            //Liste des select cible
            $tabTarget = $params['target'];

            $data = [];
            //Pour chacune des cibles à remplir, sélection des données
            foreach ($tabTarget as $target) {
                $data[] = $this->getLocalisationService()->getOptionsForMultiSelect(
                    $typeSelect,
                    $tabValueSelect,
                    $target
                );
            }

        }

        return new JsonModel($data);
    }

    /**
     * Action de suppression du dispositif
     *
     * @return ResponseInterface|JsonModel
     */
    public function dispositifSupprimeAction()
    {
        try {
            //Vérification qu'un conseiller est connecté
            $data = $this->dispositifSupprime();

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Permet de réaliser la suppression
     *
     * @return array
     * @throws Exception
     */
    private function dispositifSupprime()
    {
        $data = [];

        /** @var Request $request */
        $request = $this->getRequest();

        //Récupération des paramètres
        $dataPost        = $request->getPost();
        $idDispositif    = $dataPost['idDispositif'];
        $codeRegionListe = $dataPost['codeRegionListe'];

        /** @var Dispositif $dispositif */
        $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);

        //Suppression du dispositif
        $this->getDispositifService()->deleteDispositif($dispositif);

        //Redirection vers la listedes dispositifs pour la région sélectionnées
        $data['url'] = $this->url()->fromRoute(
            'admin/dispositif',
            ['codeRegion' => $codeRegionListe],
            [
                'query' => [
                    'PHPSESSID' => $this->getSessionService()->getManager()->getId()
                ]
            ]
        );

        return $data;
    }

    /**
     * Action de désactivation du dispositif
     *
     * @return ResponseInterface|JsonModel
     */
    public function dispositifDesactiveAction()
    {
        try {
            $data = $this->dispositifChangeEtat(ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE);

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Action de validation du dispositif
     *
     * @return ResponseInterface|JsonModel
     */
    public function dispositifValideAction()
    {
        try {
            $data = $this->dispositifChangeEtat(ValeurListeConstante::ETAT_DISPOSITIF_ACTIVE);

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Action de demande de validation du dispositif
     *
     * @return ResponseInterface|JsonModel
     */
    public function dispositifAvaliderAction()
    {
        try {
            //Vérification de la période de validité
            if ($this->verifieValiditeDispositif()) {
                $data = $this->dispositifChangeEtat(ValeurListeConstante::ETAT_DISPOSITIF_AVALIDER);
            } else {
                $data['periodeKO'] = true;
            }

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Permet de vérifier la date de validité
     *
     * @return bool
     * @throws Exception
     */
    private function verifieValiditeDispositif()
    {
        /** @var Request $request */
        $request = $this->getRequest();

        //Récupération des paramètres
        $dataPost     = $request->getPost();
        $idDispositif = $dataPost['idDispositif'];

        /** @var Dispositif $dispositif */
        $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);

        $dateDebut = $dispositif->getDebutValidite();

        return !empty($dateDebut);

    }

    /**
     * Permet de réaliser le changement d'état
     *
     * @param string $codeEtat Code du nouvel état
     *
     * @return array
     * @throws Exception
     */
    private function dispositifChangeEtat($codeEtat)
    {
        $data = [];

        /** @var Request $request */
        $request = $this->getRequest();

        //Récupération des paramètres
        $dataPost        = $request->getPost();
        $idDispositif    = $dataPost['idDispositif'];
        $codeRegionListe = $dataPost['codeRegionListe'];

        /** @var Dispositif $dispositif */
        $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);

        //Modification de l'état du dispositif
        $this->getDispositifService()->changeEtatDispositif($dispositif, $codeEtat);

        //Redirection vers la fiche du dispositif modifié
        $data['url'] = $this->url()->fromRoute(
            'admin/dispositif-modif',
            ['codeRegion' => $codeRegionListe, 'idDispositif' => $idDispositif],
            [
                'query' => [
                    'PHPSESSID' => $this->getSessionService()->getManager()->getId()
                ]
            ]
        );

        return $data;
    }

    /**
     * Action de duplication du dispositif
     *
     * @return ResponseInterface|JsonModel
     */
    public function dispositifDupliqueAction()
    {
        try {
            $data = $this->dispositifDuplique();

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Réalise la duplication du dispositif
     *
     * @return array
     * @throws Exception
     */
    private function dispositifDuplique()
    {
        $data = [];

        /** @var Request $request */
        $request = $this->getRequest();

        //Récupération des paramètres
        $dataPost        = $request->getPost();
        $idDispositif    = $dataPost['idDispositif'];
        $codeRegionListe = $dataPost['codeRegionListe'];

        /** @var Dispositif $dispositifInit */
        $dispositifInit = $this->getDispositifService()->getDispositifParId($idDispositif);

        //Création d'un nouveau dispositif
        /** @var Dispositif $dispositifNew */
        $dispositifNew = new Dispositif();

        $this->getDispositifService()->dupliqueDispositif($dispositifInit, $dispositifNew);

        //Redirection vers la fiche du nouveau dispositif
        $data['url'] = $this->url()->fromRoute(
            'admin/dispositif-modif',
            ['codeRegion' => $codeRegionListe, 'idDispositif' => $dispositifNew->getId()],
            [
                'query' => [
                    'PHPSESSID' => $this->getSessionService()->getManager()->getId()
                ]
            ]
        );

        return $data;
    }

    /**
     * Retourne le service sur les dispositifs
     *
     * @return array|object|DispositifService
     */
    private function getDispositifService()
    {
        if (empty($this->dispositifService)) {
            $this->dispositifService = $this->getServiceLocator()->get(DispositifService::SERVICE_NAME);
        }

        return $this->dispositifService;
    }

    /**
     * Retourne le service sur les groupes des dispositifs
     *
     * @return array|object|GroupeService
     */
    private function getGroupeService()
    {
        if (empty($this->groupeService)) {
            $this->groupeService = $this->getServiceLocator()->get(GroupeService::SERVICE_NAME);
        }

        return $this->groupeService;
    }

    /**
     * Retourne le service sur les localisation
     *
     * @return array|object|LocalisationService
     */
    private function getLocalisationService()
    {
        if (empty($this->localisationService)) {
            $this->localisationService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
        }

        return $this->localisationService;
    }

    /**
     * Retourne le service sur les périmètres géographique
     *
     * @return array|object|PerimetreGeoService
     */
    private function getPerimetreGeoService()
    {
        if (empty($this->perimetreGeoService)) {
            $this->perimetreGeoService = $this->getServiceLocator()->get(PerimetreGeoService::SERVICE_NAME);
        }

        return $this->perimetreGeoService;
    }

    /**
     * Retourne le service sur les valeur liste
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);
        }

        return $this->valeurListeService;
    }

       /**
     * Action appelée pour exporter les dispositifs
     *
     * @return bool
     * @throws string
     */

    public function exportDispositifVerificationAction()
    {

        try {

            $codeRegion = $this->getEvent()->getRouteMatch()->getParam('codeRegion');
            $tabDispositif = $this->getDispositifsForExport($codeRegion,null, "ACTIVE");

            if(count($tabDispositif)){
                $options['status'] = true;
            }else{
                $options['status'] = false;
            }
            return new JsonModel($options);
        } catch (\Exception $oException) {
            //Gestion des exception
            throw ($oException);
        }
    }
}
