<?php

namespace Simulaides\Controller\Back\Simulation;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use Simulaides\Constante\AccesSimulationFormConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Controller\Back\IBackController;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Comparatif;
use Simulaides\Form\Comparatif\AccesComparatifForm as AccesForm;
use Simulaides\Service\AccessSimulationFormService;
use Simulaides\Service\ComparatifPdfService;
use Simulaides\Service\ComparatifService;
use Simulaides\Service\OffresCeeService;
use Simulaides\Service\ProjetService;
use Simulaides\Service\ResultatViewService;
use Simulaides\Service\SessionContainerService;
use Zend\Form\Element\Button;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Stdlib\ResponseInterface;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Classe ComparatifController
 *
 * Projet : simulaides-php
 *
 * @author
 */
class ComparatifController extends SessionController implements IBackController
{
    /**
     * Crée un comparatif de simulations
     *
     * @return ViewModel
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function modifierAction()
    {
        /** @var ComparatifService $comparatifService */
        $comparatifService = $this->getServiceLocator()->get(ComparatifService::SERVICE_NAME);
        $comparatif        = new Comparatif();

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            if (!empty($postData[AccesForm::CHAMP_NUM_COMPARATIF]) && !empty($postData[AccesForm::CHAMP_CODE_ACCES])) {
                $comparatif = $comparatifService->getComparatifByIdEtMdp(
                    $postData[AccesForm::CHAMP_NUM_COMPARATIF],
                    $postData[AccesForm::CHAMP_CODE_ACCES]
                );
            }
        }

        return $comparatifService->renderSimulationView($comparatif);
    }

    /**
     * Consulte un comparatif
     *
     * @return ViewModel
     * @throws NonUniqueResultException
     */
    public function consulterAction()
    {
        // Récupère le formulaire et fait le bind
        /** @var ComparatifService $comparatifService */
        $comparatifService = $this->getServiceLocator()->get(ComparatifService::SERVICE_NAME);

        $formAccesComparatif = $comparatifService->getAndBindFormAccesComparatif();

        $model = new ViewModel();
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();

            $formAccesComparatif->setData($postData);
            //Vérification que le formulaire soit valide
            if ($formAccesComparatif->isValid()) {
                $comparatif = $comparatifService->getComparatifByIdEtMdp(
                    $postData[AccesForm::CHAMP_NUM_COMPARATIF],
                    $postData[AccesForm::CHAMP_CODE_ACCES]
                );

                if (null === $comparatif) {
                    $model->setVariable('msgError', "Aucun comparatif ne correspond au numéro et au code d'accès saisis.");
                } else {
                    return $comparatifService->renderSimulationView($comparatif);
                }
            }
        }

        $model->setVariable('form', $formAccesComparatif);
        $model->setTemplate('simulaides/comparatif/charger.phtml');

        return $model;
    }

    /**
     * Retourne la vue de simulation au format JSON
     * @return JsonModel
     * @throws Exception
     */
    public function simulerAction()
    {
        /** @var SessionContainerService $session */
        $session = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);

        //Initialisation d'un nouveau projet pour la nouvelle simulation
        /** @var ProjetService $projetService */
        $projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);


        /** @var Request $request */
        $request = $this->getRequest();
        $projetService->initialiseNouveauProjet($request->getPost());
        if ($request->isPost()) {
            $dataPost         = $request->getPost();
            /** @var ProjetService $projetService */
            $projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
            $projetService->chargeProjetDansSession($projetService->getProjetParId($dataPost['idSimulation']));
        }

        //Mémorisation en session qu'il s'agit d'une simulation de production
        $session->setModeExecution(SimulationConstante::MODE_PRODUCTION);

        return $this->forward()->dispatch('Simulaides\Controller\Front\Caracteristique', ['action' => 'index', 'isComparatif' => true]);
    }

    /**
     * Enregistre le comparatif
     * @return void|JsonModel
     * @throws OptimisticLockException
     */
    public function enregistrerAction()
    {
        $data = [];
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData      = $request->getPost();
            $idSimulation1 = $postData['idSimulation1'];
            $idSimulation2 = $postData['idSimulation2'];
            $idSimulation3 = $postData['idSimulation3'];
            $idComparatif  = $postData[AccesForm::CHAMP_NUM_COMPARATIF];

            if (!empty($idSimulation1)) {
                /** @var ComparatifService $comparatifService */
                $comparatifService = $this->getServiceLocator()->get(ComparatifService::SERVICE_NAME);
                $comparatif        = $comparatifService->saveComparatif($idComparatif, $idSimulation1, $idSimulation2, $idSimulation3);

                $data = [
                    AccesForm::CHAMP_NUM_COMPARATIF => $comparatif->getId(),
                    AccesForm::CHAMP_CODE_ACCES     => $comparatif->getMotPasse(),
                    'urlConsultation'               => $this->url()->fromRoute('admin/simulation-comparatif-consulter')
                ];
            }
        }

        return new JsonModel($data);
    }

    /**
     * Génère le PDF et propose l'enregistrement à l'utilisateur
     *
     * @return Response
     * @throws Exception
     */
    public function telechargerAction()
    {
        try {
            $idComparatif = $this->getRequest()->getQuery(AccesForm::CHAMP_NUM_COMPARATIF);
            $motPasse     = $this->getRequest()->getQuery(AccesForm::CHAMP_CODE_ACCES);

            /** @var ComparatifService $comparatifService */
            $comparatifService = $this->getServiceLocator()->get(ComparatifService::SERVICE_NAME);
            $comparatif        = $comparatifService->getComparatifByIdEtMdp($idComparatif, $motPasse);

            /** @var ComparatifPdfService $comparatifPdfService */
            $comparatifPdfService = $this->getServiceLocator()->get(ComparatifPdfService::SERVICE_NAME);
            $fileName             = $comparatifPdfService->generePdf($comparatif);

            $response = new Response\Stream();
            if (file_exists($fileName)) {
                $fileContents = file_get_contents($fileName);
                unlink($fileName);
                /** @var Response $response */
                $response = $this->getResponse();
                $response->setContent($fileContents);
                $headers = $response->getHeaders();
                $headers->clearHeaders()
                    ->addHeaderLine('Content-Type', 'application/pdf;charset=UTF-8')
                    ->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($fileName) . '"')
                    ->addHeaderLine('Content-Length', strlen($fileContents));
            } else {
                $response->setStatusCode(404);
            }

            return $response;
        } catch (Exception $oException) {
            //Gestion des exception
            throw ($oException);
        }
    }

    /**
     * Consulte une simulation
     * @return JsonModel|ResponseInterface|void
     * @throws Exception
     */
    public function consulterSimulationAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataPost      = $request->getPost();
            $numSimulation = $dataPost[AccesSimulationFormConstante::CHAMP_NUM_SIMULATION];
            $codeAcces     = $dataPost[AccesSimulationFormConstante::CHAMP_CODE_ACCES];
            /** @var ProjetService $projetService */
            $projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
            try {
                $projetSimulation = $projetService->getProjetParNumeroEtCodeAcces($numSimulation, $codeAcces);
                if (null === $projetSimulation) {
                    //Le projet n'existe pas
                    $dataReturn['msgError'] = "Aucune simulation ne correspond au numéro et au code d'accès que vous avez saisis";
                } else {
                    $this->getSessionService()->setModeExecution(SimulationConstante::MODE_PRODUCTION);

                    $projetService->chargeProjetDansSession($projetSimulation);
                    $projetService->lanceSimulation($projetSimulation->getId());

                    /** @var ResultatViewService $resultatViewService */
                    $resultatViewService = $this->getServiceLocator()->get(ResultatViewService::SERVICE_NAME);
                    $offreCeeService=$this->getServiceLocator()->get(OffresCeeService::SERVICE_NAME);
                    $model               = $resultatViewService->getViewModelResultat($projetSimulation, $this->getParentRoute(), $offreCeeService,true);

                    return new JsonModel(
                        [
                            'htmlView'     => $this->getServiceLocator()->get('ViewRenderer')->render($model),
                            'idSimulation' => $projetSimulation->getId()
                        ]
                    );
                }
            } catch (Exception $oException) {
                //Gestion des exception remontée pour être affichées en js
                $this->response->setContent($oException->getMessage());

                return $this->response;
            }
        }
    }

    /**
     * Charge une simulation
     *
     * @return JsonModel|ResponseInterface
     * @throws Exception
     */
    public function chargerSimulationAction()
    {
        // Récupère le formulaire et fait le bind
        /* @var AccessSimulationFormService $accessSimulationFormService */
        $accessSimulationFormService = $this->getServiceLocator()->get(AccessSimulationFormService::SERVICE_NAME);
        $formAccesSimulation         = $accessSimulationFormService->getAndBindFormAccessSimulation();

        $model = new ViewModel(
            [
                'formName'   => 'frmAcces',
                'formAction' => $this->url()->fromRoute('admin/simulation-comparatif-charger-simulation'),
                'msgError'   => null
            ]
        );

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            if (!empty($postData['csrf'])) {
                try {
                    $dataReturn = $accessSimulationFormService->processSimulation($request);
                } catch (Exception $oException) {
                    //Gestion des exception remontée pour être affichées en js
                    $this->response->setContent($oException->getMessage());

                    return $this->response;
                }

                //Mémorisation en session qu'il s'agit d'une simulation de production
                $this->getSessionService()->setModeExecution(SimulationConstante::MODE_PRODUCTION);

                if ($dataReturn['simulationOK'] === true) {
                    return $this->forward()->dispatch(
                        'Simulaides\Controller\Back\Simulation\Comparatif',
                        ['action' => 'consulterSimulation', 'isComparatif' => true]
                    );
                }

                $model->setVariable('msgError', $dataReturn['msgError']);
                $formAccesSimulation = $dataReturn['formAccesSimulation'];
            }
        }

        //Définition du onclick du bouton Valider
        /** @var Button $btnValide */
        $btnValide = $formAccesSimulation->get('btnValide');
        $formAccesSimulation->add(
            [
                'name'       => 'PHPSESSID',
                'type'       => 'hidden',
                'attributes' => ['value' => $this->getSessionService()->getManager()->getId()]
            ]
        );
        $btnValide->setAttribute('onclick', 'submitFormResultatSimulation("frmAcces");return false;');

        $model->setVariable('form', $formAccesSimulation);
        $model->setTemplate('simulaides/comparatif/charger-simulation.phtml');
        $model->setTerminal(true);

        return new JsonModel(['htmlView' => $this->getServiceLocator()->get('ViewRenderer')->render($model)]);
    }

    /**
     * Retourne le résumé d'une simulation
     * @throws Exception
     */
    public function resumerSimulationAction()
    {
        /** @var ProjetService $projetService */
        $projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
        /** @var ComparatifService $comparatifService */
        $comparatifService = $this->getServiceLocator()->get(ComparatifService::SERVICE_NAME);
        $data              = [];
        $request           = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            if (!empty($postData['idSimulation'])) {
                $model = $comparatifService->getResumeSimulation($projetService->getProjetParId($postData['idSimulation']));
                $data  = $this->getServiceLocator()->get('ViewRenderer')->render($model);
            }
        }

        return new JsonModel(['htmlView' => $data]);
    }

    /**
     * Supprime une simulation de comparatif
     * @throws OptimisticLockException
     */
    public function supprimerSimulationAction()
    {
        $data = [];
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData     = $request->getPost();
            $idSimulation = $postData['idSimulation'];
            $idComparatif = $postData[AccesForm::CHAMP_NUM_COMPARATIF];

            if (!empty($idSimulation) && !empty($idComparatif)) {
                /** @var ComparatifService $comparatifService */
                $comparatifService = $this->getServiceLocator()->get(ComparatifService::SERVICE_NAME);
                $comparatif        = $comparatifService->deleteSimulationComparatif($idComparatif, $idSimulation);

                $data = [
                    AccesForm::CHAMP_NUM_COMPARATIF => $comparatif->getId(),
                    AccesForm::CHAMP_CODE_ACCES     => $comparatif->getMotPasse(),
                    'url'                           => $this->url()->fromRoute('admin/simulation-comparatif-modifier')
                ];
            }
        }

        return new JsonModel($data);
    }

    /**
     * Duplique une simulation
     *
     * Attention : ne la lie pas en bdd au comparatif !
     */
    public function dupliquerSimulationAction()
    {
        $data = [];
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData     = $request->getPost();
            $idSimulation = $postData['idSimulation'];

            try {
                if (!empty($idSimulation)) {
                    /** @var ProjetService $projetService */
                    $projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
                    $projetSession = $projetService->copySimulation($idSimulation);

                    if ($projetSession !== null) {
                        /** @var ResultatViewService $resultatViewService */
                        $resultatViewService = $this->getServiceLocator()->get(ResultatViewService::SERVICE_NAME);
                        $offreCeeService=$this->getServiceLocator()->get(OffresCeeService::SERVICE_NAME);
                        $model               = $resultatViewService->getViewModelResultat(
                            $projetService->getProjetParId($projetSession->getId()),
                            $this->getParentRoute(),
                            $offreCeeService,
                            true
                        );

                        return new JsonModel(
                            [
                                'htmlView'     => $this->getServiceLocator()->get('ViewRenderer')->render($model),
                                'idSimulation' => $projetSession->getId()
                            ]
                        );
                    }
                }
            } catch (Exception $oException) {
                //Gestion des exception remontée pour être affichées en js
                $this->response->setContent($oException->getMessage());

                return $this->response;
            }
        }

        return new JsonModel($data);
    }
}
