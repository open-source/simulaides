<?php

namespace Simulaides\Controller\Back\Simulation;

use Exception;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Controller\Back\IBackController;
use Simulaides\Controller\SessionController;
use Simulaides\Form\Simulation\AccesSimulationForm;
use Simulaides\Service\AccessSimulationFormService;
use Zend\Form\Element\Button;
use Zend\Http\Request;
use Zend\Stdlib\ResponseInterface;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ModelInterface;
use Zend\View\Model\ViewModel;

/**
 * Classe RepriseSimulationController
 *
 * Projet : simulaides-php
 *
 * @author
 */
class RepriseSimulationController extends SessionController implements IBackController
{
    /**
     * Service sur le formulaire d'accès à la simulation
     *
     * @var AccessSimulationFormService
     */
    private $accessSimulationFormService;

    /**
     * Retourne la vue de la simulation conseiller
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $model = new ViewModel();

        //Récupère le formulaire et fait le bind
        $formAccesSimulation = $this->getAccessSimulationFormService()->getAndBindFormAccessSimulation();
        //Ajout de la vue fille sur les formulaires d'accès à la simulation
        $model->addChild($this->getViewModelAccesSimulation($formAccesSimulation), 'formAccesSimulation');

        return $model;
    }

    /**
     * Permet de lancer une simulation
     *
     * @return ModelInterface|ResponseInterface
     */
    public function lanceSimulationAction()
    {
        try {
            /** @var Request $request */
            $request    = $this->getRequest();
            $dataReturn = $this->getAccessSimulationFormService()->processSimulation($request);

            //Mémorisation en session qu'il s'agit d'une simulation de test
            $this->getSessionService()->setModeExecution(SimulationConstante::MODE_PRODUCTION);

            //Création de la vue
            $model = $this->getViewModelAccesSimulation($dataReturn['formAccesSimulation'], $dataReturn['msgError']);
            $model->setTerminal(true);

            //Données de retour du JSon
            return new JsonModel(
                [
                    'htmlView'     => $this->getServiceLocator()->get('ViewRenderer')->render($model),
                    'simulationOk' => $dataReturn['simulationOK']
                ]
            );
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Retourne la vue du formulaire d'accès à la simulation
     *
     * @param AccesSimulationForm $formAccesSimulation
     * @param                     $msgError
     *
     * @return ViewModel
     */
    private function getViewModelAccesSimulation($formAccesSimulation, $msgError = null)
    {
        //Définition du onclick du bouton Valider
        /** @var Button $btnValide */
        $btnValide = $formAccesSimulation->get('btnValide');
        $formAccesSimulation->add(
            [
                'name'       => 'PHPSESSID',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionService()->getManager()->getId()
                ]
            ]
        );
        $btnValide->setAttribute('onclick', 'submitFormAccesSimulation();return false;');

        $model = new ViewModel(
            [
                'form'       => $formAccesSimulation,
                'formName'   => 'frmAcces',
                'formAction' => $this->url()->fromRoute('admin/simulation-conseiller-lance'),
                'msgError'   => $msgError
            ]
        );
        $model->setTemplate('simulaides/acces-simulation.phtml');

        return $model;
    }

    /**
     * Retourne le service deu formulaire d'accès à la simulation
     *
     * @return AccessSimulationFormService
     */
    private function getAccessSimulationFormService()
    {
        if (empty($this->accessSimulationFormService)) {
            $this->accessSimulationFormService = $this->getServiceLocator()->get(
                AccessSimulationFormService::SERVICE_NAME
            );
        }

        return $this->accessSimulationFormService;
    }
}
