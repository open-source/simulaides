<?php

namespace Simulaides\Controller\Back\Simulation;

use Exception;
use Simulaides\Constante\AccesSimulationFormConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\TableConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Controller\Back\IBackController;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\Region;
use Simulaides\Domain\Entity\ValeurListe;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Form\Dispositif\RegionAdminForm;
use Simulaides\Form\Simulation\AccesSimulationForm;
use Simulaides\Service\AccessSimulationFormService;
use Simulaides\Service\DispositifService;
use Simulaides\Service\LocalisationService;
use Simulaides\Service\ProjetService;
use Simulaides\Tools\Utils;
use Zend\Form\Element\Button;
use Zend\Http\Request;
use Zend\Stdlib\ResponseInterface;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Classe SimulationConseillerController
 * Controller pour l'onglet "Simulation" dans la partie conseiller
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class TestDispositifController extends SessionController implements IBackController
{
    /**
     * Service sur le formulaire d'accès à la simulation
     *
     * @var AccessSimulationFormService
     */
    private $accessSimulationFormService;

    /**
     * Service sur le projet
     * @var ProjetService
     */
    private $projetService;

    /**
     * Service sur le dispositif
     * @var DispositifService
     */
    private $dispositifService;

    /**
     * Service sur la localisation
     * @var LocalisationService
     */
    private $localisationService;

    /**
     * Retourne la vue de la simulation conseiller
     *
     * @return array|ViewModel
     * @throws Exception
     */
    public function indexAction()
    {
        //Récupération du formulaire sur les régions
        $codeRegion = '';
        $region     = $this->getRegionPourInitialiser();
        if (!empty($region)) {
            $codeRegion = $region->getCode();
        }
        $formRegion = $this->getFormulaireRegion($region);

        $model = new ViewModel(['formRegion' => $formRegion]);

        //Récupère le formulaire et fait le bind
        $formAccesSimulationTest = $this->getAccessSimulationFormService()->getAndBindFormAccessSimulation();
        //Ajout de la vue fille sur les formulaires d'accès à la simulation
        $model->addChild($this->getViewModelAccesSimulationTest($formAccesSimulationTest), 'formAccesSimulationTest');

        //Ajout de la vue fille sur la liste des dispositifs
        if (!empty($region)) {
            $model->addChild($this->getViewModelListeDispositif($codeRegion), 'listeDispositifRegion');
        }

        return $model;
    }

    /**
     * Retourne la vue de la simulation conseiller
     *
     * @return array|ViewModel
     * @throws Exception
     */
    public function unDispositifAction()
    {
        //Récupération du dispositif
        $request = $this->getRequest();
        $idDispositif = $request->getQuery('idDispositif');

        $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);


        $model = new ViewModel(['dispositif' => $dispositif]);

        //Récupère le formulaire et fait le bind
        $formAccesSimulationTest = $this->getAccessSimulationFormService()->getAndBindFormAccessSimulation();
        //Ajout de la vue fille sur les formulaires d'accès à la simulation
        $model->addChild($this->getViewModelAccesSimulationTest($formAccesSimulationTest), 'formAccesSimulationTest');

        return $model;
    }

    /**
     * Action de changement de la région dans la liste des dispositifs
     *
     * @return ViewModel|ResponseInterface
     */
    public function changeRegionAction()
    {
        try {
            $data = $this->processChangeRegion();

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Changement de région
     *
     * @return array
     * @throws Exception
     */
    private function processChangeRegion()
    {
        $data = [];

        /** @var Request $request */
        $request = $this->getRequest();

        $codeRegion = '';
        if ($request->isPost()) {
            $dataPost = $request->getPost();
            //Récupération du code de la région sélectionné
            $codeRegion = $dataPost['codeRegion'];
        }

        $model = $this->getViewModelListeDispositif($codeRegion);
        $model->setTerminal(true);

        //Données de retour du JSon
        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return $data;
    }

    /**
     * Permet de lancer une simulation de test sur un projet existant
     *
     * @return ViewModel|ResponseInterface
     */
    public function lanceSimulationTestExistantAction()
    {
        try {
            $data = $this->processSimulationTestExistant();

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Permet de lancer la simulation sur un projet existant
     *
     * @return mixed
     */
    private function processSimulationTestExistant()
    {
        $dataReturn['simulationOk'] = false;
        $dataReturn['msgError']     = null;

        //Récupère le formulaire et fait le bind
        $formAccesSimulation = $this->getAccessSimulationFormService()->getAndBindFormAccessSimulation();

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            //Récupération des données envoyées
            $dataPost = $request->getPost();
            //Récupération des données du formulaire
            $numSimulation = $dataPost[AccesSimulationFormConstante::CHAMP_NUM_SIMULATION];
            $codeAcces     = $dataPost[AccesSimulationFormConstante::CHAMP_CODE_ACCES];
            //On supprime du dataPost pour ne plus avoir que la liste des Id dispositifs
            $dataForm[AccesSimulationFormConstante::CHAMP_CODE_ACCES]     = $dataPost[AccesSimulationFormConstante::CHAMP_CODE_ACCES];
            $dataForm[AccesSimulationFormConstante::CHAMP_NUM_SIMULATION] = $dataPost[AccesSimulationFormConstante::CHAMP_NUM_SIMULATION];
            $dataForm[AccesSimulationFormConstante::CHAMP_CSRF]           = $dataPost[AccesSimulationFormConstante::CHAMP_CSRF];
            unset($dataPost[AccesSimulationFormConstante::CHAMP_CODE_ACCES]);
            unset($dataPost[AccesSimulationFormConstante::CHAMP_NUM_SIMULATION]);
            unset($dataPost[AccesSimulationFormConstante::CHAMP_CSRF]);
            //Liste des dispositifs sélectionnés
            $arrayData = $dataPost->toArray();
            unset($arrayData['PHPSESSID']);
            $tabIdDispositif = array_keys($arrayData);

            //Vérifie le projet, valide le formulaire et charge le projet en session
            $dataReturn = $this->getAccessSimulationFormService()->processSimulationApresVerifProjet(
                $formAccesSimulation,
                $numSimulation,
                $codeAcces,
                $dataForm
            );

            $session = $this->getSessionService();
            //Sauvegarde de la liste des dispositifs
            $session->setTabIdDispositifTest($tabIdDispositif);
            //Indique qu'il s'agit d'une simulation de test
            $session->setModeExecution(SimulationConstante::MODE_TEST);
        }

        $model = $this->getViewModelAccesSimulationTest($formAccesSimulation, $dataReturn['msgError']);
        $model->setTerminal(true);

        //Données de retour du JSon
        $data['simulationOk'] = $dataReturn['simulationOK'];
        $data['htmlView']     = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return $data;
    }

    /**
     * Permet de lancer une simulation de test sur un nouveau projet
     *
     * @return ViewModel|ResponseInterface
     */
    public function lanceSimulationTestNouveauAction()
    {
        try {
            $this->processSimulationNouveauTest();

            return new JsonModel(['ok' => true]);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Permet de lancer la simulation sur un nouveau projet
     * @throws Exception
     */
    private function processSimulationNouveauTest()
    {
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            //Récupération des données envoyées
            $dataPost = $request->getPost();
            //Liste des dispositifs sélectionnés
            $arrayData = $dataPost->toArray();
            unset($arrayData['PHPSESSID']);
            $tabIdDispositif = array_keys($dataPost->toArray());

            //Création et mémorisation en session d'un projet vierge
            $this->getProjetService()->initialiseNouveauProjet($dataPost);
            $session = $this->getSessionService();
            //Mémorisation en session de la liste des dispositifs
            $session->setTabIdDispositifTest($tabIdDispositif);
            //Mémorisation en session qu'il s'agit d'une simulation de test
            $session->setModeExecution(SimulationConstante::MODE_TEST);
        }
    }

    /**
     * Retourne la vue pour la liste des dispositifs de la région sélectionnée
     *
     * @param string $codeRegion Code de la région à afficher
     *
     * @return ViewModel
     * @throws Exception
     */
    private function getViewModelListeDispositif($codeRegion)
    {
        $model = new ViewModel(
            [
                'tabEntete' => $this->getDispositifEnteteTableau(),
                'tabLignes' => $this->getDispositifLignesTableau($codeRegion)
            ]
        );
        $model->setTemplate('simulaides/test-dispositif/liste-dispositif.phtml');

        return $model;
    }

    /**
     * Retourne les informations de l'entête du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des dispositifs
     *
     * @return array
     */
    private function getDispositifEnteteTableau()
    {
        $tabEntete = [];
        //Colonne Case à cocher
        $caseSelectAll = new TableCellule('select-deselect-tout', TableConstante::TYPE_CASE_COCHER);
        $caseSelectAll->setTitle('Sélectionner/Désélectionner tout');
        $tabEntete[] = $caseSelectAll;
        //Colonne Ordre
        $tabEntete[] = new TableCellule('Ordre');
        //Colonne Intitulé
        $tabEntete[] = new TableCellule('Intitulé');
        //Colonne Financeur
        $tabEntete[] = new TableCellule('Financeur');
        //Colonne Etat
        $tabEntete[] = new TableCellule('Etat');
        //Colonne Début validité
        $tabEntete[] = new TableCellule('Début validité');
        //Colonne Fin validité
        $tabEntete[] = new TableCellule('Fin validité');

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des dispositifs
     *
     * @param string $codeRegion Code de la région choisie
     *
     * @return array
     * @throws Exception
     */
    private function getDispositifLignesTableau($codeRegion)
    {
        $tabLignes = [];
        $ordre     = 0;
        //Sélection des dispositifs de la région
        $tabDispositif = $this->getDispositifService()->getDispositifParCodeRegion($codeRegion);

        /** @var Dispositif $dispositif */
        foreach ($tabDispositif as $dispositif) {
            $ligne = new TableLigne();
            $ligne->setId($dispositif->getId());
            $ordre++;

            /** @var ValeurListe $etat */
            $etat     = $dispositif->getEtat();
            $estCoche = false;
            $etatLib  = '';
            if (!empty($etat)) {
                //Coché si l'état est "Activé" et si le dispositif est dans sa période de validité
                $etatLib      = $etat->getLibelle();
                $estEtatActif = ($etat->getCode() == ValeurListeConstante::ETAT_DISPOSITIF_ACTIVE);
                $estValide    = Utils::estPeriodeValideNow(
                    $dispositif->getDebutValidite(),
                    $dispositif->getFinValidite()
                );
                $estCoche     = $estEtatActif && $estValide;
            }

            //Cellule de la case à cocher
            $cellule = new TableCellule('', TableConstante::TYPE_CASE_COCHER);
            $cellule->setIsChecked($estCoche); //Par défaut cochée si état ACTIVER
            $ligne->addCellule($cellule);

            //Cellule de l'ordre
            //mis en dur car la liste des dipositifs est trié sur le numéro d'ordre
            //et ce numéro n'est pas disponible sur l'entité Dispositif mais sur Groupe
            $cellule = new TableCellule($ordre, TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule de l'intitulé
            $cellule = new TableCellule($dispositif->getIntitule(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule du financeur
            $cellule = new TableCellule($dispositif->getFinanceur(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule de l'état
            $cellule = new TableCellule($etatLib, TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule du début de validité
            $dateDebut = Utils::formatDateToString($dispositif->getDebutValidite());
            $cellule   = new TableCellule($dateDebut, TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule de fin de validité
            $dateFin = Utils::formatDateToString($dispositif->getFinValidite());
            $cellule = new TableCellule($dateFin, TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;
        }

        return $tabLignes;
    }

    /**
     * Retourne la région à mettre en initialisation
     *
     * @return null|Region
     * @throws Exception
     */
    private function getRegionPourInitialiser()
    {
        //Récupération de la région du conseiller pour initialiser la liste
        $codeRegion = $this->getSessionService()->getConseiller()->getCodeRegion();
        //Recherche de la région
        return $this->getLocalisationService()->getRegionParCode($codeRegion);
    }

    /**
     * Retourne le formulaire sur la région
     *
     * @param Region $region
     *
     * @return RegionAdminForm
     */
    private function getFormulaireRegion($region)
    {
        $data = ['codeRegion' => $region];

        $formRegion = $this->getDispositifService()->getRegionAdminForm();
        $formRegion->setData($data);

        return $formRegion;
    }

    /**
     * Retourne la vue du formulaire d'accès à la simulation de test
     *
     * @param AccesSimulationForm $formAccesSimulation
     * @param                     $msgError
     *
     * @return ViewModel
     */
    private function getViewModelAccesSimulationTest($formAccesSimulation, $msgError = null)
    {
        //Définition du onclick du bouton Valider
        /** @var Button $btnValide */
        $btnValide = $formAccesSimulation->get('btnValide');
        $formAccesSimulation->add(
            [
                'name'       => 'PHPSESSID',
                'type'       => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionService()->getManager()->getId()
                ]
            ]
        );
        $btnValide->setAttribute('onclick', $this->getOnClickButtonValideFormAccesSimulation(true));

        $model = new ViewModel(
            [
                'form'       => $formAccesSimulation,
                'formName'   => 'frmAccesTest',
                'formAction' => $this->url()->fromRoute('admin/simulation-conseiller-test-existant'),
                'msgError'   => $msgError
            ]
        );
        $model->setTemplate('simulaides/acces-simulation.phtml');

        return $model;
    }

    /**
     * Retourne la fonction a appeler sur le click du bouton Valider
     * du formulaire d'accès à la simulation
     *
     * @param bool $pourTest Indique si c'est pour le formulaire de test
     *
     * @return string
     */
    private function getOnClickButtonValideFormAccesSimulation($pourTest)
    {
        if ($pourTest) {
                $onClick = 'submitFormAccesSimulationTest();return false;';
       } else {
            $onClick = 'submitFormAccesSimulation();return false;';
        }

        return $onClick;
    }

    /**
     * Retourne le service sur les projets
     *
     * @return array|object|ProjetService
     */
    private function getProjetService()
    {
        if (empty($this->projetService)) {
            $this->projetService = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        }

        return $this->projetService;
    }

    /**
     * Retourne le service sur les dispositifs
     *
     * @return array|object|DispositifService
     */
    private function getDispositifService()
    {
        if (empty($this->dispositifService)) {
            $this->dispositifService = $this->getServiceLocator()->get(DispositifService::SERVICE_NAME);
        }

        return $this->dispositifService;
    }

    /**
     * Retourne le service de session
     *
     * @return LocalisationService
     */
    private function getLocalisationService()
    {
        if (empty($this->localisationService)) {
            $this->localisationService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
        }

        return $this->localisationService;
    }

    /**
     * Retourne le service deu formulaire d'accès à la simulation
     *
     * @return AccessSimulationFormService
     */
    private function getAccessSimulationFormService()
    {
        if (empty($this->accessSimulationFormService)) {
            $this->accessSimulationFormService = $this->getServiceLocator()->get(
                AccessSimulationFormService::SERVICE_NAME
            );
        }

        return $this->accessSimulationFormService;
    }
}
