<?php
namespace Simulaides\Controller\Back;

use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\ValeurListe;
use Simulaides\Service\ParametrageService;
use Simulaides\Service\ValeurListeService;
use Zend\Http\Request;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Classe ContenuController
 * Controller permettant de gérer l'affichage du paramétrage des contenus
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class ContenuController extends SessionController implements IBackController
{
    /**
     * Service des paramètres techniques
     *
     * @var ParametrageService
     */
    private $paramService;

    /**
     * Service des valeurs de liste
     * @var ValeurListeService
     */
    private $valeurListeService;

    /**
     * Permet de retourner l'affichage de l'onglet Contenu
     * @return array|ViewModel
     */
    public function indexAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $msgOK   = '';

        //Création du formulaire
        $contenuForm = $this->getParametrageService()->getContenuForm();
        $contenuForm->add(
            [
                'name' => 'PHPSESSID',
                'type' => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionService()->getManager()->getId()
                ]
            ]);
        if ($request->isPost()) {
            //Récupération des informations envoyées par la requête
            $data          = $request->getPost();
            $changeContenu = isset($data['changeContenu']);
            $isSubmit      = isset($data['submitContenu']);

            if ($changeContenu) {
                //La valeur de la liste de contenu a été changée, rechargement de la valeur
                unset($data['changeContenu']);

                //Récupération de la valeur en base
                /** @var ValeurListe $valeurContenu */
                $valeurContenu = $this->getValeurContenu($data['code']);
                $libContenu    = '';
                if (!empty($valeurContenu)) {
                    $libContenu = $valeurContenu->getDescriptif();
                }
                $data['descriptif'] = $libContenu;

                //Passage des données récupérers au formulaire
                $contenuForm->setData($data);

            } elseif ($isSubmit) {
                //Demande de sauvegarde du contenu
                unset($data['submitContenu']);

                //Passage des données récupérers au formulaire
                $contenuForm->setData($data);

                if ($contenuForm->isValid()) {
                    //Récupération de la valeur en base
                    /** @var ValeurListe $valeurContenu */
                    $valeurContenu = $this->getValeurContenu($data['code']);
                    //Modification du descriptif avec la valeur saisie
                    $data['descriptif'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['descriptif']);

                    $valeurContenu->setDescriptif($data['descriptif']);

                    $this->getValeurListeService()->saveValeurListe($valeurContenu);
                    $contenuForm->setData($data);
                    $msgOK = 'Le contenu a bien été enregistré.';
                }
            }
        }

        $model = new ViewModel(['form' => $contenuForm, 'msgOK' => $msgOK]);
        $model->setTemplate('simulaides/contenu/index.phtml');
        $model->setTerminal(true);

        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);


        return new JsonModel($data);
    }

    /**
     * Retourne le service du paramétrage
     *
     * @return array|object|ParametrageService
     */
    private function getParametrageService()
    {
        if (empty($this->paramService)) {
            $this->paramService = $this->getServiceLocator()->get(ParametrageService::SERVICE_NAME);
        }
        return $this->paramService;
    }

    /**
     * Retourne le service des valeurs liste
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);
        }
        return $this->valeurListeService;
    }

    /**
     * Permet de retourner la valeur du contenu en base
     *
     * @param $code
     *
     * @return null|\Simulaides\Domain\Entity\ValeurListe
     */
    private function getValeurContenu($code)
    {
        $valeurListe = null;
        //Recherche de la valeur
        $valeurListe = $this->getValeurListeService()->getValeurPourCodeListeEtCode(
            ValeurListeConstante::TEXT_ADMINISTRABLE,
            $code
        );

        return $valeurListe;
    }
}
