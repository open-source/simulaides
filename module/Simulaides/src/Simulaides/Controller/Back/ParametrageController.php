<?php
namespace Simulaides\Controller\Back;

use Simulaides\Constante\SessionConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Service\SessionContainerService;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

/**
 * Classe ParametrageController
 * Permet de gérer l'affichage de l'écran de paramétrage avec les onglets
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class ParametrageController extends SessionController implements IBackController
{
    /**
     * Permet de retourner la vue
     * @return ViewModel
     */
    public function indexAction()
    {
        //Certains onglets sont visibles si l'utilisateur est ADEME
        /** @var SessionContainerService $session */
        $session     = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        $conseiller  = $session->getConseiller();
        $isUserAdeme = $conseiller->estConseillerADEME();
        $aRoleAdeme  = $conseiller->aRoleADEME();

        return new ViewModel([
            'isUserAdeme' => $isUserAdeme,
            'aRoleAdeme'  => $aRoleAdeme
        ]);
    }
}
