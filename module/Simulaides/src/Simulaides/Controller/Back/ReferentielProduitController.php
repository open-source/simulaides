<?php
namespace Simulaides\Controller\Back;

use Simulaides\Constante\MainConstante;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\TableConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Produit;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Service\ListeService;
use Simulaides\Service\ParametrageService;
use Simulaides\Service\ProduitService;
use Simulaides\Service\ProjetService;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\TravauxService;
use Simulaides\Service\ValeurListeService;
use Simulaides\Tools\Utils;
use Zend\Http\Request;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Classe ReferentielProduitController
 * Controller permettant de gérer le réféentiel des prix
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class ReferentielProduitController extends SessionController implements IBackController
{
    /**
     * Service des paramètres techniques
     *
     * @var ParametrageService
     */
    private $paramService;

    /**
     * Service des valeurs de liste
     * @var ValeurListeService
     */
    private $valeurListeService;

    /**
     * Service des liste
     * @var ListeService
     */
    private $listeService;

    /**
     * Service sur les travaux
     * @var TravauxService
     */
    private $travauxService;

    /**
     * Service sur les produits
     * @var ProduitService
     */
    private $produitService;

    /**
     * Service de gestion de la session
     * @var SessionContainerService
     */
    private $sessionContainerService;

    /**
     * Service sur les projets
     *
     * @var ProjetService
     */
    private $projetService;

    /**
     * Permet de retourner l'affichage de l'onglet Référentiel des prix
     * @return array|ViewModel
     */
    public function indexAction()
    {
        /** @var Request $request */
        $request     = $this->getRequest();
        $codeTravaux = null; //Par défaut, tous les travaux sont sélectionnés
        $codeTerritoire = "" ; //Par défaut, les travaux de la métropole sont sélectionnés

        //Création du formulaire
        $refPrixForm = $this->getParametrageService()->getReferentielPrixForm();
        $refPrixForm->add(
            [
                'name' => 'PHPSESSID',
                'type' => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionService()->getManager()->getId()
                ]
            ]);
        if ($request->isPost()) {
            //Récupération des informations envoyées par la requête
            $data        = $request->getPost();
            $codeTravaux = $data['travaux']; //Code du travaux sélectionné dans la liste
            $codeTerritoire = $data['territoire']; //Code du territoire sélectionné dans la liste
        }
        $model = new ViewModel([
            'form' => $refPrixForm,
            'phpsessid' => $this->getSessionService()->getManager()->getId()
        ]);
        $model->setTemplate('simulaides/referentiel-prix/index.phtml');

        /** @var PhpRenderer $renderer */
        $renderer = $this->getServiceLocator()->get('ViewRenderer');
        //Indique qu'il y a des enfants dont le render sera fait dans la vue de la mère (renderChildModel)
        //Car le render sur la mère ne fait pas le render sur les enfants
        $renderer->setCanRenderTrees(true);

        $model->addChild($this->getViewModelTableauProduit($codeTerritoire,$codeTravaux), 'tabProduits');
        $model->addChild($this->getViewModelAideCEE(), 'aideCEE');

        $model->setTerminal(true);

        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Retourne la vue sur le formulaire des aides CEE
     *
     * @return ViewModel
     */
    private function getViewModelAideCEE()
    {
        //Création du model
        $model = new ViewModel();
        $model->setTemplate('simulaides/referentiel-prix/aide-cee.phtml');

        $formAideCEE         = $this->getParametrageService()->getReferentielPrixAideCEEForm();
        $formAideCEE->add(
            [
                'name' => 'PHPSESSID',
                'type' => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionService()->getManager()->getId()
                ]
            ]);
        $data['coefficient'] = $this->getValeurListeService()->getCoefficientKwhCumac();
        $formAideCEE->setData($data);

        //Affectation du formulaire
        $model->setVariable('form', $formAideCEE);

        return $model;
    }

    /**
     * Action sur la validation du formulaire Aide CEE
     *
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function aideCeeAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();

        $msgOK = '';

        $form = $this->getParametrageService()->getReferentielPrixAideCEEForm();
        $form->add(
            [
                'name' => 'PHPSESSID',
                'type' => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionService()->getManager()->getId()
                ]
            ]);
        if ($request->isPost()) {
            //Récupération des informations envoyées par la requête
            $data = $request->getPost();

            $form->setData($data);

            if ($form->isValid()) {
                //Le coefficient est valide, mémorisation
                $valCoef = $data['coefficient'];
                $liste   = $this->getListeService()->getListeParCode(
                    ValeurListeConstante::LISTE_NUM_ADMINISTRABLE
                );
                $this->getValeurListeService()->saveValeurListePourCodeListeEtCode(
                    $liste,
                    ValeurListeConstante::CONVERSION_KWH_EUROS,
                    $valCoef
                );
                $msgOK = "Le coefficient a bien été enregistré";
            }
        }
        $model = $this->getViewModelAideCEE();
        $model->setTerminal(true);
        $model->setVariable('form', $form);
        $model->setVariable('msgOK', $msgOK);

        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);
        return new JsonModel($data);
    }

    /**
     * Permet de faire le changement du travaux
     * et retourne la vue du tableau uniquement
     *
     * @return null|ViewModel
     */
    public function changeTravauxAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();

        $model = null;

        if ($request->isPost()) {
            //Récupération des informations envoyées par la requête
            $data = $request->getPost();

            $codeTravaux = $data['travaux']; //Code du travaux sélectionné dans la liste
            $codeTerritoire = $data['territoire']; //Code du territoire sélectionné dans la liste
            $model       = $this->getViewModelTableauProduit($codeTerritoire,$codeTravaux);
            $model->setTerminal(true);

            $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

            return new JsonModel($data);
        }

        return $this->response;
    }

    /**
     * Permet d'afficher la modale de la fiche de modification des prix d'un produit
     *
     * @return ViewModel
     */
    public function ficheModifAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $territoire = "";
        $libTerritoire = "";
        if ($request->isPost()) {
            $dataPost = $request->getPost();
            $territoire = $dataPost['territoire'];
        }
        //Récupération du formulaire
        $form        = $this->getParametrageService()->getReferentielProduitForm();
        $libTerritoire = "Métropole";
        if($territoire == "1"){
            //outre mer
            $form        = $this->getParametrageService()->getReferentielPrixProduitOuForm();
            $libTerritoire = "Outremer";
        }
        $form->add(
            [
                'name' => 'PHPSESSID',
                'type' => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionService()->getManager()->getId()
                ]
            ]);
        $formIsValid = false;
        if ($request->isPost()) {
            $dataPost = $request->getPost();
            $isSubmit = isset($dataPost['submit']);
            //Récupération du code produit pour faire le bind du formulaire
            $codeProduit = $dataPost['code'];
            $produit     = $this->getProduitService()->getProduitParCodeProduit($codeProduit);
            $form->bind($produit);
            $nbProduitPourTravaux = count($this->getProduitsLignesTableau('',$produit->getCodeTravaux()));
            if ($isSubmit) {
                unset($dataPost['submit']);
                //Le formulaire a été enregistré
                $form->setData($dataPost);
                if ($form->isValid()) {
                    $formIsValid = true;
                    //Sauvegarde des modifications
                    /** @var Produit $produit */
                    $produit = $form->getObject();
                    //Mise à jour du montant total = Prix MO + Prix FO
                    $produit->setPrixUTotal($produit->getPrixUFourniture() + $produit->getPrixUMo());
                    $produit->setPrixUTotalOu($produit->getPrixUFournitureOu() + $produit->getPrixUMoOu());
                    //Sauvegarde du produit
                    $this->getProduitService()->saveProduit($produit);
                }
            }
        }

        $model = new ViewModel([
            'form'        => $form,
            'libTerritoire' => $libTerritoire,
            'formIsValid' => $formIsValid,
            'nbProduitPourTravaux' => $nbProduitPourTravaux
        ]);
        $model->setTemplate('simulaides/referentiel-prix/fiche-produit-ref-prix.phtml');
        $model->setTerminal(true);

        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Action de lancement de l'export des prix
     *
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function exportAction()
    {
        // ----------- /!\ La gestion du conseiller est différente ici car le "exit" efface la redirection

        //Définition et initialisation du fichier d'export
        $fileName = $this->initialiseFileExport();

        //Sélection des prix
        $tabPrixAdmin = $this->getProduitService()->getAllPrixAdminPourExport();

        foreach ($tabPrixAdmin as $infosProduit) {
            $tabLine                                             = [];
            $dateProjet                                          = $infosProduit['dateSaisie'];
            $codeProduit                                         = $infosProduit['codeProduit'];
            //$territoire                                          = $infosProduit['territoire'];
            $tabLine[$codeProduit]                               = [];
            $tabLine[$codeProduit]['date']                       = Utils::formatDateToString($dateProjet);
            $tabLine[$codeProduit]['codePdt']                    = $codeProduit;
            $tabLine[$codeProduit]['nomPdt']                     = $infosProduit['intitule'];
            $tabLine[$codeProduit][SessionConstante::COUT_TOTAL] = $infosProduit['prixTotal'];
            $tabLine[$codeProduit][SessionConstante::COUT_MO]    = $infosProduit['prixMO'];
            $tabLine[$codeProduit][SessionConstante::COUT_FO]    = $infosProduit['prixFourniture'];

            //Ecriture de la ligne du projet
            $this->ecritLignePourProjet($fileName, $tabLine);
        }

        //Lecture du fichier
        $this->readFileExport($fileName);
        exit;
    }

    /**
     * Lance la lecture du fichier avec ouverture d'une fenêtre
     * de demande de sauvegarde ou de lecture
     *
     * @param $fileName
     */
    private function readFileExport($fileName)
    {
        //Construction de l'entête du fichier
        header("Content-Type: text/csv;charset=UTF-8");
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        header("Content-Transfer-Encoding: binary");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo MainConstante::CARACTERE_BOM;

        //Lecture du fichier
        readfile($fileName);
    }

    /**
     * Permet de retourner le nom du fichier d'export
     * Et de le supprimer si il existe déjà
     *
     * @return string
     */
    private function initialiseFileExport()
    {
        //Définition du nom du fichier avec le nom du conseiller
        $conseiller = $this->getSessionContainerService()->getIdentiteConseiller();
        $fileName   = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'export_referentiel_prix_' . $conseiller . '.csv';

        //Suppression du fichier si il existe déjà
        if (file_exists($fileName)) {
            unlink($fileName);
        }

        //Ajout de l'entête dans le fichier
        $this->ecritLigneEntete($fileName);

        return $fileName;
    }

    /**
     * Permet d'écrire dans le fichier les lignes de produit du projet
     *
     * @param $fileName
     * @param $tabProjet
     */
    private function ecritLignePourProjet($fileName, $tabProjet)
    {
        foreach ($tabProjet as $objProduit) {
            $tabLine   = [];
            $tabLine[] = $this->getElementLigneExport($objProduit, 'date');
            $tabLine[] = $this->getElementLigneExport($objProduit, 'codePdt');
            $tabLine[] = $this->getElementLigneExport($objProduit, 'nomPdt');
            $tabLine[] = $this->getElementLigneExport($objProduit, SessionConstante::COUT_TOTAL);
            $tabLine[] = $this->getElementLigneExport($objProduit, SessionConstante::COUT_MO);
            $tabLine[] = $this->getElementLigneExport($objProduit, SessionConstante::COUT_FO);

            file_put_contents($fileName, implode(";", $tabLine) . "\n", FILE_APPEND | LOCK_EX);
        }
    }

    /**
     * Permet d'écrire dans le fichier la ligne d'entête
     *
     * @param $fileName
     */
    private function ecritLigneEntete($fileName)
    {
        $tabEntete [] = 'Date de simulation';
        $tabEntete [] = 'Code du produit';
        $tabEntete [] = 'Intitulé du produit';
        $tabEntete [] = 'Prix Unitaire Total (HT)';
        $tabEntete [] = "Prix Unitaire de la main d'oeuvre (HT)";
        $tabEntete [] = 'Prix Unitaire des fournitures (HT)';

        file_put_contents($fileName, implode(";", $tabEntete) . "\n", FILE_APPEND | LOCK_EX);
    }

    /**
     * Retourne la valeur de l'élément
     *
     * @param $tabElement
     * @param $nameIndex
     *
     * @return string
     */
    private function getElementLigneExport($tabElement, $nameIndex)
    {
        $line = '';
        if (isset($tabElement[$nameIndex])) {
            $line = $tabElement[$nameIndex];
        }
        return $line;
    }

    /**
     * Permet de générer la vue du tableau des produits
     *
     * @param string $codeTravaux Code du travaux
     * @param string $codeTerritoire Code du territoire
     *
     * @return ViewModel
     */
    private function getViewModelTableauProduit($codeTerritoire,$codeTravaux)
    {
        $model = new ViewModel([
            'tabEntete' => $this->getProduitsEnteteTableau(),
            'tabLignes' => $this->getProduitsLignesTableau($codeTerritoire,$codeTravaux)
        ]);
        $model->setTemplate('simulaides/referentiel-prix/produits-tableau.phtml');

        return $model;
    }

    /**
     * Retourne les informations de l'entête du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des produits
     *
     * @return array
     */
    private function getProduitsEnteteTableau()
    {
        $tabEntete = [];
        //Colonne Intitulé
        $tabEntete[] = new TableCellule('Intitulé du produit');
        //Colonne Total
        $tabEntete[] = new TableCellule('Total');
        //Colonne Fourniture
        $tabEntete[] = new TableCellule('Fourniture');
        //Colonne Main d'oeuvre
        $tabEntete[] = new TableCellule("Main d'oeuvre");
        //Colonne TVA
        $tabEntete[] = new TableCellule('TVA');
        //Colonne Editer
        $tabEntete[] = new TableCellule('Modifier');

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des produits
     *
     * @param string $codeTerritoire Code du territoire ("" si "Métropole" sélectionné)
     * @param string $codeTravaux Code du travaux (Null si "Tous" sélectionné)
     *
     * @return array
     */
    private function getProduitsLignesTableau($codeTerritoire,$codeTravaux)
    {
        $tabLignes = [];
        //Sélection des produits du travaux
        if (!empty($codeTravaux)) {
            $tabProduits = $this->getTravauxService()->getProduitsParCodeTravaux($codeTravaux);
        } else {
            $tabProduits = $this->getTravauxService()->getTousProduitsTrieParCodeTravaux();
        }

        /** @var Produit $produit */
        foreach ($tabProduits as $produit) {
            $ligne = new TableLigne();
            $ligne->setId($produit->getCode());

            //Cellule de l'intitulé
            $cellule = new TableCellule($produit->getIntitule(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule du Total
            if(!$codeTerritoire == '1'){
                $montantLib = Utils::formatNumberToString($produit->getPrixUTotal(), 2);
                $cellule    = new TableCellule($montantLib, TableConstante::TYPE_TEXTE);
                $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                $ligne->addCellule($cellule);
            } else {
                //Outremer
                $montantLib = Utils::formatNumberToString($produit->getPrixUTotalOu(), 2);
                $cellule    = new TableCellule($montantLib, TableConstante::TYPE_TEXTE);
                $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                $ligne->addCellule($cellule);
            }
            //Cellule Fourniture
            if(!$codeTerritoire == '1'){
                $montantLib = Utils::formatNumberToString($produit->getPrixUFourniture(), 2);
                $cellule    = new TableCellule($montantLib, TableConstante::TYPE_TEXTE);
                $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                $ligne->addCellule($cellule);
            } else {
                //Outremer
                $montantLib = Utils::formatNumberToString($produit->getPrixUFournitureOu(), 2);
                $cellule = new TableCellule($montantLib, TableConstante::TYPE_TEXTE);
                $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                $ligne->addCellule($cellule);
            }
            //Cellule Main d'oeuvre
            if(!$codeTerritoire == '1'){
                $montantLib = Utils::formatNumberToString($produit->getPrixUMo(), 2);
                $cellule    = new TableCellule($montantLib, TableConstante::TYPE_TEXTE);
                $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                $ligne->addCellule($cellule);
            } else {
                //Outremer
                $montantLib = Utils::formatNumberToString($produit->getPrixUMoOu(), 2);
                $cellule    = new TableCellule($montantLib, TableConstante::TYPE_TEXTE);
                $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                $ligne->addCellule($cellule);
            }


            //Cellule TVA
            if(!$codeTerritoire == '1'){
                $tvaLib  = Utils::formatNumberToString($produit->getTva(), 2);
                $cellule = new TableCellule($tvaLib, TableConstante::TYPE_TEXTE);
                $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                $ligne->addCellule($cellule);
            }
            else {
                //Outremer
                $tvaLib  = Utils::formatNumberToString($produit->getTvaOu(), 2);
                $cellule = new TableCellule($tvaLib, TableConstante::TYPE_TEXTE);
                $cellule->setAlign(TableConstante::ALIGN_RIGHT);
                $ligne->addCellule($cellule);
            }
            //Cellule de la modification
            //$lien    = "javascript:modifFicheRefPrixProduit('" . $codeTerritoire . "','" . $produit->getCode() . "');";
            $lien    = "javascript:modifFicheRefPrixProduit('" . $produit->getCode() . "');";
            $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_MODIF, $lien);
            $ligne->addCellule($cellule);

            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;
        }

        return $tabLignes;
    }

    /**
     * Retourne le service du paramétrage
     *
     * @return array|object|ParametrageService
     */
    private function getParametrageService()
    {
        if (empty($this->paramService)) {
            $this->paramService = $this->getServiceLocator()->get(ParametrageService::SERVICE_NAME);
        }
        return $this->paramService;
    }

    /**
     * Retourne le service des valeurs liste
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->getServiceLocator()->get(ValeurListeService::SERVICE_NAME);
        }
        return $this->valeurListeService;
    }

    /**
     * Retourne le service des listes
     *
     * @return array|object|ListeService
     */
    private function getListeService()
    {
        if (empty($this->listeService)) {
            $this->listeService = $this->getServiceLocator()->get(ListeService::SERVICE_NAME);
        }
        return $this->listeService;
    }

    /**
     * Retourne le service des travaux
     *
     * @return array|object|TravauxService
     */
    private function getTravauxService()
    {
        if (empty($this->travauxService)) {
            $this->travauxService = $this->getServiceLocator()->get(TravauxService::SERVICE_NAME);
        }
        return $this->travauxService;
    }

    /**
     * Retourne le service des produits
     *
     * @return array|object|ProduitService
     */
    private function getProduitService()
    {
        if (empty($this->produitService)) {
            $this->produitService = $this->getServiceLocator()->get(ProduitService::SERVICE_NAME);
        }
        return $this->produitService;
    }

    /**
     * Retourne le service de la session
     *
     * @return array|object|SessionContainerService
     */
    private function getSessionContainerService()
    {
        if (empty($this->sessionContainerService)) {
            $this->sessionContainerService = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        }
        return $this->sessionContainerService;
    }

    /**
     * Retourne le service des projets
     *
     * @return array|object|ProjetService
     */
    private function getProjetService()
    {
        if (empty($this->projetService)) {
            $this->projetService = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        }
        return $this->projetService;
    }
}
