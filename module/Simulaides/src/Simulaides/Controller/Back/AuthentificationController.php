<?php

namespace Simulaides\Controller\Back;

use Simulaides\Constante\SessionConstante;
use Simulaides\Exception\AuthException;
use Simulaides\Service\AuthentificationService;
use Simulaides\Service\SessionContainerService;
use Simulaides\Service\VersionService;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Http\Response\Stream;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Classe AuthentificationController
 * Controller pour le formulaire d'authentification du conseiller
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class AuthentificationController extends AbstractActionController implements IBackController
{

    /**
     * Action appelée sur l'affichage de l'authentification
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $errMessage = '';

        /** @var SessionContainerService $session */
        $session = $this->getSessionService();
        /** @var VersionService $versionService */
        $versionService = $this->getServiceLocator()->get(VersionService::SERVICE_NAME);
        $version = $versionService->getVersion();
        $session->addOffset(SessionConstante::VERSION, $version);

        // On indique que l'accès à PRIS est demandé
        $session->addOffset(SessionConstante::ACCESS_PRIS, true);
        //Mise en place des couleurs PRIS
        $session->setCouleurFondTitreN1(SessionConstante::FOND_TITRE_N1_PRIS);
        $session->setCouleurFondTitreN2(SessionConstante::FOND_TITRE_N2_PRIS);
        $session->setCouleurFondTitreN3(SessionConstante::FOND_TITRE_N3_PRIS);
        $session->setCouleurFondBouton(SessionConstante::FOND_BOUTON_PRIS);

        /** @var AuthentificationService $authService */
        $authService = $this->getServiceLocator()->get(AuthentificationService::SERVICE_NAME);
        $form        = $authService->getAuthentificationForm();

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            //Validation de la saisie => Vérification du login/mot de passe
            //Affectation des données postées au formulaire
            $data = $request->getPost();
            $form->setData($data);

            if ($form->isValid()) {
                try {
                    $conseiller = $authService->chekLogin($data['login'], $data['motPasse']);
                    $authService->updateSession($conseiller);

                    $this->redirect()->toRoute(
                        'admin/accueil',
                        [],
                        ['query' => ['PHPSESSID' => $this->getSessionService()->getManager()->getId()]]
                    );
                } catch (AuthException $authException) {
                    // Le conseiller n'est pas dans la base, affichage d'un message
                    $errMessage = $authException->getMessage();
                }
            }
        }

        return new ViewModel(
            [
                'form'       => $form,
                'errMessage' => $errMessage
            ]
        );
    }

    /**
     * Accueil de l'application
     *
     * @return ViewModel | Response
     */
    public function accueilAction()
    {
        /** @var AuthentificationService $authService */
        $authService = $this->getServiceLocator()->get(AuthentificationService::SERVICE_NAME);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            // Acceptation des CGU
            $postData = $request->getPost();
            if (!empty($postData['cgu']) && $postData['cgu'] == 1 && isset($postData['OK'])) {
                $authService->setCGUAcctepte();
            } else {
                return $this->redirect()->toRoute(
                    'admin/deconnecte',
                    [],
                    ['query' => ['PHPSESSID' => $this->getSessionService()->getManager()->getId()]]
                );
            }
        }

        return new ViewModel(['isCGUAccepte' => $authService->isCGUAccepte()]);
    }

    /**
     * Permet de réaliser la déconnexion du conseiller
     *
     * @return Response
     */
    public function deconnecteAction()
    {
        /** @var SessionContainerService $session */
        $session = $this->getSessionService();

        //On vide les éléments de session du conseiller
        $session->deconnecteConseiller();

        $session->clearColor();
        $session->getManager()->destroy();
        //Rappel de la page d'authentification
        return $this->redirect()->toRoute('admin');
    }

    /**
     * Perte de la session sur le projet
     *
     * @return ViewModel
     */
    public function perteSessionProjetAction()
    {
        return new ViewModel(['msgSession' => "La session sur le projet a été perdue. Fermer l'onglet."]);
    }

    /**
     * Télécharge le manuel utilisateur
     */
    public function manuelAction()
    {
        $tabConfig = $this->serviceLocator->get('ApplicationConfig');
        $tabCgu    = $tabConfig['condition-generale'];
        $fileName  = $tabCgu['manuel'];
        $response  = new Stream();

        if (file_exists($fileName)) {
            $fileContents = file_get_contents($fileName);
            /** @var Response $response */
            $response = $this->getResponse();
            $response->setContent($fileContents);
            $headers = $response->getHeaders();
            $headers->clearHeaders()
                ->addHeaderLine('Content-Type', 'application/pdf;charset=UTF-8')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($fileName) . '"')
                ->addHeaderLine('Content-Length', strlen($fileContents));
        } else {
            $response->setStatusCode(404);
        }
        return $response;
    }

    /**
     * Retourne le service de session
     *
     * @return SessionContainerService
     */
    private function getSessionService()
    {
        /** @var SessionContainerService $session */
        $session = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
        return $session;
    }

    /**
     * Permet d'ouvrir le fichier des conditions générales d'utilisation conseiller
     */
    public function readCguAction()
    {
        //Définition du lien vers les conditions générales
        $tabConfig          = $this->serviceLocator->get('ApplicationConfig');
        $tabCgu             = $tabConfig['condition-generale'];
        $fileNameFichierCgu = $tabCgu['cgu_conseiller'];
        $response           = new Stream();

        if (file_exists($fileNameFichierCgu)) {
            $fileContents = file_get_contents($fileNameFichierCgu);
            /** @var Response $response */
            $response = $this->getResponse();
            $response->setContent($fileContents);
            $headers = $response->getHeaders();
            $headers->clearHeaders()
                ->addHeaderLine('Content-Type', 'application/pdf;charset=UTF-8')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($fileNameFichierCgu) . '"')
                ->addHeaderLine('Content-Length', strlen($fileContents));
        } else {
            $response->setStatusCode(Response::STATUS_CODE_404);
        }

        return $response;
    }
}
