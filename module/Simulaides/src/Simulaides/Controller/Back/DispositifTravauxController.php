<?php

namespace Simulaides\Controller\Back;

use Simulaides\Constante\ListeOuiNonConstante;
use Simulaides\Constante\TableConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\DispositifTravaux;
use Simulaides\Domain\Entity\DispTravauxRegle;
use Simulaides\Domain\Entity\Travaux;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Form\Dispositif\DispositifTravauxFieldset;
use Simulaides\Form\Dispositif\DispositifTravauxForm;
use Simulaides\Form\Dispositif\EditeurRegleForm;
use Simulaides\Service\DispositifService;
use Simulaides\Service\DispositifTravauxService;
use Simulaides\Service\RegleService;
use Simulaides\Service\TravauxService;
use Zend\Form\Element;
use Zend\Http\Request;
use Zend\Stdlib\ArrayObject;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Classe DispositifTravauxController
 * Permet de gérer l'onglet "Travaux" de la fiche dispositif
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class DispositifTravauxController extends SessionController implements IBackController
{
    /** @var  TravauxService */
    private $travauxService;

    /** @var  DispositifService */
    private $dispositifService;

    /** @var  DispositifTravauxService */
    private $dispositifTravauxService;

    /** @var  RegleService */
    private $regleService;

    /**
     * Retourne la vue pour la liste des travaux
     * de la fiche du dispositif
     *
     * @return ViewModel
     */
    public function dispositifListeTravauxAction()
    {
        //Récupération de l'id dispositif à modifier
        $idDispositif = $this->getEvent()->getRouteMatch()->getParam('idDispositif');
        //Récupération de l'info de consultation au chargement
        $isConsult = $this->getEvent()->getRouteMatch()->getParam('isConsult');

        /** @var Request $request */
        $request = $this->getRequest();
        $mode = $request->getQuery()->get('mode');
        $phpsessid = $request->getQuery()->get('PHPSESSID');

        //Récupération du code travaux dont on revient (cas d'un retour à partir de la fiche travaux)
        $codeTravaux = $this->getEvent()->getRouteMatch()->getParam('codeTravaux');

        //Récupération de la liste des travaux à afficher
        $tabTravaux = $this->getTravauxService()->getListTravauxPourFicheDispositif();

        //Récupération des travaux du dispositif
        $tabCodeTravauxSelected = $this->getDispositifTravauxService()->getCodeTravauxPourDispositif($idDispositif);

        //Récupération du travaux dont on revient
        $codeCategorie = '';
        if (!empty($codeTravaux)) {
            $travaux       = $this->getTravauxService()->getTravauxByCode($codeTravaux);
            $codeCategorie = $travaux->getCategorie();
        }

        if(!count($tabCodeTravauxSelected) && $mode == 'liste'){
            $mode = 'modification' ;
        }

        $model = new ViewModel(
            [
                'tabTravaux'               => $tabTravaux,
                'tabCodeTravauxDispositif' => $tabCodeTravauxSelected,
                'isConsult'                => $isConsult,
                'idDispositif'             => $idDispositif,
                'mode'                     => $mode,
                'categOpened'              => $codeCategorie,
                'phpsessid'                => $phpsessid
            ]
        );
        $model->setTerminal(true);
        $model->setTemplate('simulaides/dispositif-travaux/dispositif-liste-travaux.phtml');
        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Permet d'afficher la page de règle du travaux du dispositif
     *
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function dispositifTravauxModifAction()
    {
        $titre             = '';
        $idDispositif      = null;
        $dispositifTravaux = null;
        $codeTravaux       = null;
        $isConsult         = false;
        $confirmChange     = false;
        $retourListe       = false;
        $msgOK             = '';

        /** @var Request $request */
        $request = $this->getRequest();

        $form = $this->getDispositifService()->getDispositifTravauxForm();

        if ($request->isPost()) {
            $dataPost = $request->getPost();

            //Récupération des paramètres
            $isSubmit      = $dataPost['submit'];
            $deleteTravaux = isset($dataPost['deleteTravaux']);

            if ($isSubmit || $deleteTravaux) {
                //Récupération des identifiants dans le formulaire
                $idDispositif = $dataPost['dispositifTravaux']['idDispositif'];
                $codeTravaux  = $dataPost['dispositifTravaux']['codeTravaux'];
            } else {
                //Récupération des identifiants dans l'url
                $idDispositifTravaux = $dataPost['idDispositifTravaux'];
                if (!empty($idDispositifTravaux)) {
                    //On vient de la fiche de l'éditeur d'une règle d'un travaux, seul idDispositifTravaux est transmis
                    $dataInfos    = $this->getDispositifEtTravauxPourFiche($idDispositifTravaux);
                    $idDispositif = $dataInfos['idDispositif'];
                    $codeTravaux  = $dataInfos['codeTravaux'];
                } else {
                    $idDispositif = $dataPost['idDispositif'];
                    $codeTravaux  = $dataPost['codeTravaux'];
                }
                $isConsult = $dataPost['isConsult'];
            }

            //Met en place l'inaccessibilité des champs
            $form = $this->setFormTravauxAccessibilite($form, $isConsult);

            //Récupère le travaux
            $travaux = $this->getTravauxService()->getTravauxByCode($codeTravaux);
            if (!empty($travaux)) {
                $titre = $travaux->getIntitule();
            }

            //Bind du formulaire
            /** @var DispositifTravauxForm $form */
            $form = $this->processBindFormDispositifTravaux($form, $idDispositif, $travaux);
            /** @var DispositifTravaux $dispositifTravaux */
            $dispositifTravaux = $form->get('dispositifTravaux')->getObject();

            if ($isSubmit) {
                //L'enregistrement a été demandé

                //Récupération de la valeur de "Pris en charge" avant le bind
                $prisEnChargeAvant = $this->getEstPrisEnchargeAvant($form);

                unset($dataPost['submit']);
                $form->setData($dataPost);

                //Validation du formulaire
                if ($form->isValid()) {
                    if ($dataPost['prisEnCharge'] == ListeOuiNonConstante::VALUE_NON) {
                        //L'utilisateur a indiqué que le travaux n'est pas pris en charge
                        //Demande de confirmation si le travaux était pris en charge avant
                        $confirmChange = $prisEnChargeAvant;
                    } else {
                        //L'utilisateur a indiqué que le dispositif est pris en charge => ajout/modif de l'association
                        /** @var DispositifTravauxFieldset $fdtDispositifTravaux */
                        $fdtDispositifTravaux = $form->get('dispositifTravaux');
                        /** @var DispositifTravaux $dispositifTravaux */
                        $dispositifTravaux = $fdtDispositifTravaux->getObject();
                        //Sauvegarde
                        $this->getDispositifTravauxService()->saveDispositifTravaux($dispositifTravaux);
                        //Affichage d'un message indiquant que la sauvegarde à fonctionné
                        $msgOK = "Le type de travaux a été sauvegardé";
                        //Remettre à "False" le champ caché d'indication de modification de données
                        /** @var Element $eltChange */
                        $eltChange = $form->get('existeChangement');
                        $eltChange->setValue("false");
                    }
                }
            } elseif ($deleteTravaux) {
                //La confirmation de la suppression du travaux au dispositif a été donné
                /** @var DispositifTravauxFieldset $fdtDispositifTravaux */
                $fdtDispositifTravaux = $form->get('dispositifTravaux');
                /** @var DispositifTravaux $dispositifTravaux */
                $dispositifTravaux = $fdtDispositifTravaux->getObject();
                //Suppression
                $this->getDispositifTravauxService()->deleteDispositifTravaux($dispositifTravaux);
                $retourListe = true;
            }
        }

        $model = new ViewModel(
            [
                'titre'        => $titre,
                'form'         => $form,
                'idDispositif' => $idDispositif,
                'codeTravaux'  => $codeTravaux,
                'isConsult'    => ($isConsult ? 1 : 0),
                'msgOK'        => $msgOK
            ]
        );
        $model->setTemplate('simulaides/dispositif-travaux/dispositif-travaux-modif.phtml');

        //Ajout de la liste des règles du tableau
        $model->addChild($this->getViewModelTravauxRegle($dispositifTravaux, $isConsult), 'listRegle');
        $model->setTerminal(true);

        /** @var PhpRenderer $renderer */
        $renderer = $this->getServiceLocator()->get('ViewRenderer');
        //Indique qu'il y a des enfants dont le render sera fait dans la vue de la mère (renderChildModel)
        //Car le render sur la mère ne fait pas le render sur les enfants
        $renderer->setCanRenderTrees(true);

        $data['htmlView']      = $this->getServiceLocator()->get('ViewRenderer')->render($model);
        $data['confirmChange'] = $confirmChange;
        $data['retourListe']   = $retourListe;
        $data['idDispositif']  = $idDispositif;
        $data['isConsult']     = ($isConsult ? 1 : 0);
        $data['codeTravaux']   = $codeTravaux;

        return new JsonModel($data);
    }

    /**
     * Permet de récupérer les infos du dispositif et du travaux
     * pour afficher la fiche
     *
     * @param int $idDispositifTravaux Identifiant du travaux dans le dispositif
     *
     * @return array
     */
    private function getDispositifEtTravauxPourFiche($idDispositifTravaux)
    {
        $data = [];
        if (!empty($idDispositifTravaux)) {
            $dispositifTravaux    = $this->getDispositifTravauxService()->getDispositifTravauxParId(
                $idDispositifTravaux
            );
            $data['idDispositif'] = $dispositifTravaux->getIdDispositif();
            $data['codeTravaux']  = $dispositifTravaux->getCodeTravaux();
        }

        return $data;
    }

    /**
     * Retourne la vue de la liste des règles du travaux dans le dispositif
     *
     * @param DispositifTravaux $dispositifTravaux Travaux dans le dispositif
     * @param boolean           $isConsult         Indique que la fiche est en consultation
     *
     * @return ViewModel
     */
    private function getViewModelTravauxRegle($dispositifTravaux, $isConsult)
    {
        //Création de la vue
        $model = new ViewModel(
            [
                'tabEntete'           => $this->getDispositifTravauxRegleEnteteTableau($isConsult),
                'tabLignes'           => $this->getDispositifTravauxRegleLignesTableau($dispositifTravaux, $isConsult),
                'isConsultation'      => $isConsult,
                'idDispositifTravaux' => $dispositifTravaux->getId()
            ]
        );
        $model->setTemplate('simulaides/dispositif-travaux/dispositif-travaux-liste-regle.phtml');

        return $model;
    }

    /**
     * Retourne l'entête du tableau des règles du travaux
     *
     * @param boolean $isConsult Indique que la fiche est en consultation
     *
     * @return array
     */
    private function getDispositifTravauxRegleEnteteTableau($isConsult)
    {
        $tabEntete = [];
        //Colonne Type
        $tabEntete[] = new TableCellule('Type');
        //Colonne Formule
        $tabEntete[] = new TableCellule('Formule');
        if (!$isConsult) {
            //Colonne Modifier
            $tabEntete[] = new TableCellule('Modifier');
            //Colonne Supprimer
            $tabEntete[] = new TableCellule('Supprimer');
        }

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des règles d'un travaux dans un dispositif
     *
     * @param DispositifTravaux $dispositifTravaux Travaux dans le dispositif
     * @param boolean           $isConsult         Indique que la fiche est en consultation
     *
     * @return array
     */
    private function getDispositifTravauxRegleLignesTableau($dispositifTravaux, $isConsult)
    {
        $tabLignes     = [];
        $idDispTravaux = $dispositifTravaux->getId();

        /** @var DispTravauxRegle $regle */
        foreach ($dispositifTravaux->getListeTravauxRegles() as $regle) {
            $ligne = new TableLigne();
            $ligne->setId($regle->getId());

            //Celulle du type
            $libelle = $this->getDispositifTravauxService()->getLibelleTypeRegleTravaux($regle->getType());
            $cellule = new TableCellule($libelle, TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule de la formule
            $formule   = '';
            $condition = $regle->getConditionRegle();
            if (!empty($condition)) {
                $conditionLitterale = $this->getRegleService()->getChaineLitterale($condition, false);
                $formule            = "<b>Si</b> " . $conditionLitterale . " <b>Alors</b> <br>";
            }

            $formule .= $this->getRegleService()->getChaineLitterale($regle->getExpression(), false);
            $cellule = new TableCellule($formule, TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            if (!$isConsult) {
                $idRegle = $regle->getId();

                //Cellule de la modification
                $lien    = "javascript:modifFicheRegleTravauxDispositif(" . $idRegle . "," . $idDispTravaux . ");";
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_MODIF, $lien);
                $ligne->addCellule($cellule);

                //Cellule de la suppression
                $lien    = "javascript:deleteRegleTravauxDispositif(" . $idRegle . "," . $idDispTravaux . ");";
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_SUPP, $lien);
                $ligne->addCellule($cellule);
            }

            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;
        }

        return $tabLignes;
    }

    /**
     * Permet de savoir si le travaux est pris en charge
     * avant les modifications de l'utilisateur
     *
     * @param DispositifTravauxForm $form
     *
     * @return bool
     */
    private function getEstPrisEnchargeAvant($form)
    {
        /** @var DispositifTravauxFieldset $fdtDispositifTravaux */
        $fdtDispositifTravaux = $form->get('dispositifTravaux');
        /** @var DispositifTravaux $dispositifTravauxAvant */
        $dispositifTravauxAvant = $fdtDispositifTravaux->getObject();
        $idDispositifTravaux    = $dispositifTravauxAvant->getId();
        //Si le travaux est déjà associé au dispositif en base, il est actuellement pris en charge
        $prisEnChargeAvant = (!empty($idDispositifTravaux));

        return $prisEnChargeAvant;
    }

    /**
     * Permet de réaliser le bind sur le formulaire
     *
     * @param DispositifTravauxForm $form
     * @param                       $idDispositif
     * @param Travaux               $travaux
     *
     * @return mixed
     */
    private function processBindFormDispositifTravaux($form, $idDispositif, $travaux)
    {
        $tabData      = [];
        $prisEnCharge = true;

        //Recherche du DispositifTravaux
        $dispositifTravaux = $this->getDispositifTravauxService()->getDispositifTravauxPourIdDispositifEtCodeTravaux(
            $idDispositif,
            $travaux->getCode()
        );

        //Si le travauxc n'existe pas dans le dispositif => création
        if (empty($dispositifTravaux)) {
            $prisEnCharge      = false;
            $dispositifTravaux = new DispositifTravaux();
            //Récupération du dispositif
            $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);
            $dispositifTravaux->setDispositif($dispositif);
            $dispositifTravaux->setIdDispositif($idDispositif);
            $dispositifTravaux->setTravaux($travaux);
            $dispositifTravaux->setCodeTravaux($travaux->getCode());
        }

        $tabData['prisEnCharge']      = $prisEnCharge;
        $tabData['dispositifTravaux'] = $dispositifTravaux;

        //Hydratation du formulaire
        $form->bind(new ArrayObject($tabData));

        return $form;
    }

    /**
     * Action de suppression d'un travaux
     *
     * @return null|ViewModel
     */
    public function dispositifTravauxSupprimeAction()
    {
        $data = [];
        /** @var Request $request */
        $request = $this->getRequest();
        $dataPost = $request->getPost();
        //Récupération des variables
        $idDispositif   = $dataPost['idDispositif'];
        $codeTravaux    = $dataPost['codeTravaux'];

        //Récupération des travaux du dispositif
        $tabCodeTravauxSelected = $this->getDispositifTravauxService()->getCodeTravauxPourDispositif($idDispositif);

        if(in_array($codeTravaux,$tabCodeTravauxSelected)){
            //Suppression
            $dispositifTravauxService = $this->getDispositifTravauxService();
            $dispositifTravaux = $dispositifTravauxService->getDispositifTravauxPourIdDispositifEtCodeTravaux
            ($idDispositif,$codeTravaux);
            $this->getDispositifTravauxService()->deleteDispositifTravaux($dispositifTravaux);
        }

        $data['suppression']=  'ok';
        return new JsonModel($data);
    }


    /**
     * Action de suppression d'un travaux
     *
     * @return null|ViewModel
     */
    public function dispositifTravauxAjoutAction()
    {
        $data = [];
        /** @var Request $request */
        $request = $this->getRequest();
        $dataPost = $request->getPost();
        //Récupération des variables
        $idDispositif       = $dataPost['idDispositif'];
        $listeCodeTravaux   = $dataPost['listeCodeTravaux'];

        //Récupération des travaux du dispositif
        $tabCodeTravauxSelected = $this->getDispositifTravauxService()->getCodeTravauxPourDispositif($idDispositif);

        foreach ($listeCodeTravaux as $codeTravaux){
            if(!in_array($codeTravaux,$tabCodeTravauxSelected)){
                $dispositifTravaux = new DispositifTravaux();
                $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);
                $travaux = $this->getTravauxService()->getTravauxByCode($codeTravaux);
                $dispositifTravaux->setCodeTravaux($codeTravaux);
                $dispositifTravaux->setTravaux($travaux);
                $dispositifTravaux->setIdDispositif($idDispositif);
                $dispositifTravaux->setDispositif($dispositif);
                $this->getDispositifTravauxService()->saveDispositifTravaux($dispositifTravaux);
            }

        }
        $phpsessid = $request->getQuery()->get('PHPSESSID');
        $data['url']=  $this->url()->fromRoute(
            'admin/dispositif-travaux',
            [
                'idDispositif'  => $idDispositif,
                'isConsult'     => 0
            ],
            ['query' => ['PHPSESSID' => $dataPost['PHPSESSID'], 'mode' => 'liste']]);
        return new JsonModel($data);
    }

    /**
     * Action de suppression d'une règle dans un travaux
     *
     * @return null|ViewModel
     */
    public function dispositifTravauxRegleSupprimeAction()
    {
        $model = null;
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dataPost = $request->getPost();
            //Récupération des variables
            $idDispositifTravaux = $dataPost['idTravauxDispositif'];
            $idRegleTravaux      = $dataPost['idRegleTravaux'];
            $isConsult           = false; //On est en suppression, donc la modification est possible

            //Récupération du travaux du dispositif
            $travauxDispositif = $this->getDispositifTravauxService()->getDispositifTravauxParId(
                $idDispositifTravaux
            );

            //Récupération de la règle de travaux
            $regleTravaux = $this->getDispositifTravauxService()->getDispTravauxRegleParId($idRegleTravaux);

            //Suppression de la règle dans le travaux du dispositif
            $travauxDispositif->removeDispositifTravauxRegle($regleTravaux);
            //Sauvegarde du travaux dans le dispositif
            $this->getDispositifTravauxService()->saveDispositifTravaux($travauxDispositif);

            //Re-Contruction du model pour raffraichissement de la liste
            $model = $this->getViewModelTravauxRegle($travauxDispositif, $isConsult);
            $model->setTerminal(true);

            $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

            return new JsonModel($data);
        }

        return $this->response;
    }

    /**
     * Affiche l'éditeur des règles en modification
     *
     * @return ViewModel
     */
    public function dispositifTravauxRegleModifAction()
    {
        /** @var Request $request */
        $request  = $this->getRequest();
        $isPost   = $request->isPost();
        $isSubmit = false;

        if ($isPost) {
            $dataPost       = $request->getPost();
            $idRegleTravaux = $dataPost['id'];
            $isSubmit       = $dataPost['submit'];
        } else {
            //Récupération de la règle du travaux dans le dispositif à modifier
            $idRegleTravaux = $this->getEvent()->getRouteMatch()->getParam('id');
        }
        /** @var DispTravauxRegle $regle */
        $regle               = $this->getDispositifTravauxService()->getDispTravauxRegleParId($idRegleTravaux);
        $idDispositifTravaux = $regle->getIdDispTravaux();

        //Définition de l'url du formulaire
        $urlAction = $this->url()->fromRoute('admin/dispositif-travaux-regle-modif');

        $model = new ViewModel(
            [
                'idDispositifTravaux' => $idDispositifTravaux,
                'titre'               => $this->getTitreTravauxEditeur($idDispositifTravaux)
            ]
        );
        $model->setTemplate('simulaides/dispositif-travaux/dispositif-travaux-regle-modif.phtml');

        /** @var PhpRenderer $renderer */
        $renderer = $this->getServiceLocator()->get('ViewRenderer');
        //Indique qu'il y a des enfants dont le render sera fait dans la vue de la mère (renderChildModel)
        //Car le render sur la mère ne fait pas le render sur les enfants
        $renderer->setCanRenderTrees(true);

        $model->addChild(
            $this->getViewModelRegleEditeur($idDispositifTravaux, $urlAction, $isSubmit, $regle),
            'editeurRegle'
        );
        $model->setTerminal(true);

        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Affiche l'éditeur des règles en ajout
     *
     * @return ViewModel
     */
    public function dispositifTravauxRegleAjoutAction()
    {
        /** @var Request $request */
        $request  = $this->getRequest();
        $isPost   = $request->isPost();
        $isSubmit = false;

        if ($isPost) {
            $dataPost            = $request->getPost();
            $idDispositifTravaux = $dataPost['idParent'];
            $isSubmit            = $dataPost['submit'];
        } else {
            //Récupération du travaux dans le dispositif auquel ajouter la règle
            $idDispositifTravaux = $this->getEvent()->getRouteMatch()->getParam('idDispositifTravaux');
        }
        //Définition de l'url du formulaire
        $urlAction = $this->url()->fromRoute('admin/dispositif-travaux-regle-ajout');

        $model = new ViewModel(
            [
                'idDispositifTravaux' => $idDispositifTravaux,
                'titre'               => $this->getTitreTravauxEditeur($idDispositifTravaux)
            ]
        );
        $model->setTemplate('simulaides/dispositif-travaux/dispositif-travaux-regle-ajout.phtml');

        /** @var PhpRenderer $renderer */
        $renderer = $this->getServiceLocator()->get('ViewRenderer');
        //Indique qu'il y a des enfants dont le render sera fait dans la vue de la mère (renderChildModel)
        //Car le render sur la mère ne fait pas le render sur les enfants
        $renderer->setCanRenderTrees(true);

        $model->addChild(
            $this->getViewModelRegleEditeur($idDispositifTravaux, $urlAction, $isSubmit),
            'editeurRegle'
        );
        $model->setTerminal(true);
        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Permet de retourner le titre de l'éditeur
     *
     * @param $idDispositifTravaux
     *
     * @return string
     */
    private function getTitreTravauxEditeur($idDispositifTravaux)
    {
        $libelleTitre = '';

        $dispositifTravaux = $this->getDispositifTravauxService()->getDispositifTravauxParId($idDispositifTravaux);
        if (!empty($dispositifTravaux)) {
            $travaux = $dispositifTravaux->getTravaux();
            if (!empty($travaux)) {
                $libelleTitre = $travaux->getIntitule();
            }
        }

        return $libelleTitre;
    }

    /**
     * Permet de retourner la vue de la fiche de l'éditeur
     *
     * @param                  $idDispositifTravaux
     * @param                  $urlAction
     * @param                  $isSubmit
     * @param DispTravauxRegle $regle
     *
     * @return ViewModel
     */
    private function getViewModelRegleEditeur($idDispositifTravaux, $urlAction, $isSubmit, $regle = null)
    {
        $msgOk = '';
        $msgKo = '';

        /** @var EditeurRegleForm $form */
        $form = $this->getDispositifService()->getEditeurRegleFormPourTravaux();

        $form = $this->processBindDispositifTravauxRegleForm($idDispositifTravaux, $regle, $form);

        if ($isSubmit) {
            /** @var Request $request */
            $request  = $this->getRequest();
            $dataPost = $request->getPost();
            unset($dataPost['submit']);

            $form->setData($dataPost);

            if ($form->isValid()) {
                //Le formulaire est valide
                //Bind de l'objet DispositifRegle
                $regle = $this->bindObjectRegle($idDispositifTravaux, $dataPost['id']);
                $regle->setType($dataPost['type']);
                $regle->setConditionRegle($dataPost['conditionRegle']);
                $regle->setExpression($dataPost['expression']);
                //sauvegarde de la règle
                $this->getDispositifService()->saveDispositifRegle($regle);
                //Affichage d'un message de confirmation de l'enresgitrement
                $msgOk = "L'enregistrement de la règle a bien été réalisé";
                //Affichage du formulaire en création => avec règle vide
                $form = $this->processBindDispositifTravauxRegleForm($idDispositifTravaux, null, $form);
                //Remettre à "False" le champ caché d'indication de modification de données
                /** @var Element $eltChange */
                $eltChange = $form->get('existeChangement');
                $eltChange->setValue("false");
            }
        }

        //création du modèle d'éditeur
        return $this->getTravauxService()->getEditeurRegleViewModel(
            $idDispositifTravaux,
            $form,
            $msgOk,
            $msgKo,
            $urlAction,
            'formRegleTravauxEditeur'
        );
    }

    /**
     * Permet de créer ou récupérer l'objet DispTravauxRegle
     *
     * @param $idDispositifTravaux
     * @param $idRegle
     *
     * @return null|DispTravauxRegle
     */
    private function bindObjectRegle($idDispositifTravaux, $idRegle)
    {
        if (empty($idRegle)) {
            $regle             = new DispTravauxRegle();
            $dispositifTravaux = $this->getDispositifTravauxService()->getDispositifTravauxParId($idDispositifTravaux);
            $regle->setDispTravaux($dispositifTravaux);
        } else {
            $regle = $this->getDispositifTravauxService()->getDispTravauxRegleParId($idRegle);
        }

        return $regle;
    }

    /**
     * Permet de faire le bind du formulaire
     *
     * @param int              $idDispositifTravaux
     * @param DispTravauxRegle $regle
     * @param EditeurRegleForm $form
     *
     * @return mixed
     */
    private function processBindDispositifTravauxRegleForm($idDispositifTravaux, $regle, $form)
    {
        if (empty($regle)) {
            //En ajout, création d'une règle vierge
            $regle = new DispTravauxRegle();
            /** @var DispositifTravaux $dispositifTravaux */
            $dispositifTravaux = $this->getDispositifTravauxService()->getDispositifTravauxParId($idDispositifTravaux);
            $regle->setDispTravaux($dispositifTravaux);
        }
        //Construction des données d'hydratation du formulaire de l'éditeur
        $data = [
            'idParent'               => $idDispositifTravaux,
            'id'                     => $regle->getId(),
            'type'                   => $regle->getType(),
            'expressionLitteral'     => $this->getRegleService()->getChaineLitterale($regle->getExpression()),
            'expression'             => $regle->getExpression(),
            'conditionRegleLitteral' => $this->getRegleService()->getChaineLitterale($regle->getConditionRegle()),
            'conditionRegle'         => $regle->getConditionRegle()
        ];

        //Hydratation du formulaire
        $form->bind(new ArrayObject($data));

        return $form;
    }

    /**
     * Permet de rendre accssible ou non les champs selon le mode
     *
     * @param DispositifTravauxForm $form
     * @param boolean               $isConsult
     *
     * @return mixed
     */
    private function setFormTravauxAccessibilite($form, $isConsult)
    {
        $form->get('prisEnCharge')->setAttribute('disabled', $isConsult);
        /** @var DispositifTravauxFieldset $fdtDispositifTravaux */
        $fdtDispositifTravaux = $form->get('dispositifTravaux');
        $fdtDispositifTravaux->get('eligibiliteSpecifique')->setAttribute('readonly', $isConsult);

        return $form;
    }

    /**
     * Permet de retourner le service sur les travaux
     * @return array|object|TravauxService
     */
    private function getTravauxService()
    {
        if (empty($this->travauxService)) {
            $this->travauxService = $this->getServiceLocator()->get(TravauxService::SERVICE_NAME);
        }

        return $this->travauxService;
    }

    /**
     * Permet de retourner le service sur les dispositifs
     * @return array|object|DispositifService
     */
    private function getDispositifService()
    {
        if (empty($this->dispositifService)) {
            $this->dispositifService = $this->getServiceLocator()->get(DispositifService::SERVICE_NAME);
        }

        return $this->dispositifService;
    }

    /**
     * Permet de retourner le service sur les travaux des dispositifs
     * @return array|object|DispositifTravauxService
     */
    private function getDispositifTravauxService()
    {
        if (empty($this->dispositifTravauxService)) {
            $this->dispositifTravauxService = $this->getServiceLocator()->get(DispositifTravauxService::SERVICE_NAME);
        }

        return $this->dispositifTravauxService;
    }

    /**
     * Retourne le service sur les règles desdispositifs
     *
     * @return array|object|RegleService
     */
    private function getRegleService()
    {
        if (empty($this->regleService)) {
            $this->regleService = $this->getServiceLocator()->get(RegleService::SERVICE_NAME);
        }

        return $this->regleService;
    }
}
