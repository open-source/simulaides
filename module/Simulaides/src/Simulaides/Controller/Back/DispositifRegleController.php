<?php

namespace Simulaides\Controller\Back;

use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Simulaides\Constante\TableConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\DispositifRegle;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Form\Dispositif\EditeurRegleForm;
use Simulaides\Service\DispositifService;
use Simulaides\Service\RegleService;
use Zend\Form\Element;
use Zend\Http\Request;
use Zend\Stdlib\ArrayObject;
use Zend\Stdlib\ResponseInterface;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Classe DispositifRegleController
 * Permet de gérer le paramétrage des dispositifs => Onglet Règles
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class DispositifRegleController extends SessionController implements IBackController
{

    /** @var  DispositifService */
    private $dispositifService;

    /** @var  RegleService */
    private $regleService;

    /**
     * Permet de retourner la liste des règles d'un dispositif
     * Affichage de l'onglet
     *
     * @return ViewModel
     * @throws Exception
     */
    public function dispositifListeRegleAction()
    {
        $data = [];
        //Récupération de l'id dispositif à modifier
        $idDispositif = $this->getEvent()->getRouteMatch()->getParam('idDispositif');
        //Récupération de l'info de consultation au chargement
        $isConsult = $this->getEvent()->getRouteMatch()->getParam('isConsult');

        $model = new ViewModel(
            [
                'tabEntete'      => $this->getDispositifRegleEnteteTableau($isConsult),
                'tabLignes'      => $this->getDispositifRegleLignesTableau($idDispositif, $isConsult),
                'isConsultation' => $isConsult,
                'idDispositif'   => $idDispositif
            ]
        );
        $model->setTerminal(true);
        $model->setTemplate('simulaides/dispositif-regle/dispositif-liste-regle.phtml');
        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Retourne l'entête du tableau
     *
     * @param boolean $isConsult Indique que la fiche est en consultation
     *
     * @return array
     */
    private function getDispositifRegleEnteteTableau($isConsult)
    {
        $tabEntete = [];
        //Colonne Type
        $tabEntete[] = new TableCellule('Type');
        //Colonne Formule
        $tabEntete[] = new TableCellule('Formule');
        if (!$isConsult) {
            //Colonne Modifier
            $tabEntete[] = new TableCellule('Modifier');
            //Colonne Supprimer
            $tabEntete[] = new TableCellule('Supprimer');
        }

        return $tabEntete;

    }

    /**
     * * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des règles d'un dispositif
     *
     * @param int     $idDispositif Identifiant du dispositif
     * @param boolean $isConsult    Indique que la fiche est en consultation
     *
     * @return array
     * @throws Exception
     */
    private function getDispositifRegleLignesTableau($idDispositif, $isConsult)
    {
        $tabLignes = [];

        /** @var Dispositif $dispostif */
        $dispostif = $this->getDispositifService()->getDispositifParId($idDispositif);

        //Trie de la liste des règles des dispositifs sur le type de règle
        $tabDispositifRegle = $dispostif->getListDispositifRegles();
        if (count($tabDispositifRegle) > 1) {
            //Utilisation d'un iterator pour pouvoir trier une ArrayCollection doctrine
            $iterator = $tabDispositifRegle->getIterator();
            $iterator->uasort(
                function ($dispRegle1, $dispRegle2) {
                    return strcmp($dispRegle1->getType(), $dispRegle2->getType());
                }
            );
            $tabDispositifRegle = new ArrayCollection(iterator_to_array(($iterator)));
        }

        /** @var DispositifRegle $regle */
        foreach ($tabDispositifRegle as $regle) {
            $ligne = new TableLigne();
            $ligne->setId($regle->getId());

            //Celulle du type
            $libelle = $this->getDispositifService()->getLibelleTypeRegleDispositif($regle->getType());
            $cellule = new TableCellule($libelle, TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule de la formule
            $formule   = '';
            $condition = $regle->getConditionRegle();
            if (!empty($condition)) {
                $conditionLitterale = $this->getRegleService()->getChaineLitterale($condition, false);
                $formule            = "<b>Si</b> " . $conditionLitterale . " <b>Alors</b> <br>";
            }

            $formule .= $this->getRegleService()->getChaineLitterale($regle->getExpression(), false);
            $cellule = new TableCellule($formule, TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            if (!$isConsult) {
                //Cellule de la modification
                $lien    = "javascript:modifFicheRegleDispositif('" . $regle->getId() . "');";
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_MODIF, $lien);
                $ligne->addCellule($cellule);

                //Cellule de la suppression
                $lien    = "javascript:deleteRegleDispositif('" . $regle->getId() . "');";
                $cellule = new TableCellule(null, TableConstante::TYPE_BOUTON_SUPP, $lien);
                $ligne->addCellule($cellule);
            }

            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;
        }

        return $tabLignes;
    }

    /**
     * Retourne le service sur les dispositifs
     *
     * @return array|object|DispositifService
     */
    private function getDispositifService()
    {
        if (empty($this->dispositifService)) {
            $this->dispositifService = $this->getServiceLocator()->get(DispositifService::SERVICE_NAME);
        }

        return $this->dispositifService;
    }

    /**
     * Retourne le service sur les règles desdispositifs
     *
     * @return array|object|RegleService
     */
    private function getRegleService()
    {
        if (empty($this->regleService)) {
            $this->regleService = $this->getServiceLocator()->get(RegleService::SERVICE_NAME);
        }

        return $this->regleService;
    }

    /**
     * Permet de supprimer la règle d'un dispositif
     *
     * @return ResponseInterface
     */
    public function dispositifRegleSupprimeAction()
    {
        try {
            $data = $this->dispositifRegleSupprime();

            return new JsonModel($data);
        } catch (Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $this->response->setContent($oException->getMessage());

            return $this->response;
        }
    }

    /**
     * Réalise la suppression de la règle du dispositif
     *
     * @return array
     * @throws Exception
     */
    private function dispositifRegleSupprime()
    {
        $data = [];

        /** @var Request $request */
        $request = $this->getRequest();

        //Récupération des paramètres
        $dataPost          = $request->getPost();
        $idRegleDispositif = $dataPost['idRegleDispositif'];

        $regle = $this->getDispositifService()->getDispositifRegleParId($idRegleDispositif);

        $this->getDispositifService()->deleteDispositifRegle($regle);

        $data['ok']  = true;
        $data['url'] = $this->url()->fromRoute(
            'admin/dispositif-regle',
            ['idDispositif' => $regle->getIdDispositif(), 'isConsult' => 0],
            [
                'query' => [
                    'PHPSESSID' => $this->getSessionService()->getManager()->getId()
                ]
            ]
        );

        return $data;
    }

    /**
     * Action d'ajout d'une règle à un dispositif
     *
     * @return ViewModel
     * @throws Exception
     */
    public function dispositifRegleAjoutAction()
    {
        /** @var Request $request */
        $request  = $this->getRequest();
        $isPost   = $request->isPost();
        $isSubmit = false;

        if ($isPost) {
            $dataPost     = $request->getPost();
            $idDispositif = $dataPost['idParent'];
            $isSubmit     = $dataPost['submit'];
        } else {
            //Récupération du dispositif auquel ajouter la règle
            $idDispositif = $this->getEvent()->getRouteMatch()->getParam('idDispositif');
        }
        //Définition de l'url du formulaire
        $urlAction = $this->url()->fromRoute('admin/dispositif-regle-ajout');

        $model = new ViewModel(
            [
                'idDispositif' => $idDispositif
            ]
        );
        $model->setTemplate('simulaides/dispositif-regle/dispositif-regle-ajout.phtml');
        $model->addChild($this->getViewModelRegleEditeur($idDispositif, $urlAction, $isSubmit), 'editeurRegle');
        $model->setTerminal(true);

        /** @var PhpRenderer $renderer */
        $renderer = $this->getServiceLocator()->get('ViewRenderer');
        //Indique qu'il y a des enfants dont le render sera fait dans la vue de la mère (renderChildModel)
        //Car le render sur la mère ne fait pas le render sur les enfants
        $renderer->setCanRenderTrees(true);

        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Action de modification d'une règle de dispositif
     *
     * @return ViewModel
     * @throws Exception
     */
    public function dispositifRegleModifAction()
    {
        /** @var Request $request */
        $request  = $this->getRequest();
        $isPost   = $request->isPost();
        $isSubmit = false;

        if ($isPost) {
            $dataPost          = $request->getPost();
            $idRegleDispositif = $dataPost['id'];
            $isSubmit          = $dataPost['submit'];
        } else {
            //Récupération de la règle du dispositif à modifier
            $idRegleDispositif = $this->getEvent()->getRouteMatch()->getParam('idDispositifRegle');
        }
        /** @var DispositifRegle $regle */
        $regle        = $this->getDispositifService()->getDispositifRegleParId($idRegleDispositif);
        $idDispositif = $regle->getIdDispositif();

        //Définition de l'url du formulaire
        $urlAction = $this->url()->fromRoute('admin/dispositif-regle-modif');

        $model = new ViewModel(
            [
                'idDispositif' => $idDispositif
            ]
        );
        $model->setTemplate('simulaides/dispositif-regle/dispositif-regle-modif.phtml');

        /** @var PhpRenderer $renderer */
        $renderer = $this->getServiceLocator()->get('ViewRenderer');
        //Indique qu'il y a des enfants dont le render sera fait dans la vue de la mère (renderChildModel)
        //Car le render sur la mère ne fait pas le render sur les enfants
        $renderer->setCanRenderTrees(true);

        $model->addChild(
            $this->getViewModelRegleEditeur($idDispositif, $urlAction, $isSubmit, $regle),
            'editeurRegle'
        );
        $model->setTerminal(true);

        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Permet de retourner la vue de la fiche de l'éditeur
     *
     * @param                 $idDispositif
     * @param                 $urlAction
     * @param boolean         $isSubmit
     * @param DispositifRegle $regle
     *
     * @return ViewModel
     * @throws Exception
     */
    private function getViewModelRegleEditeur($idDispositif, $urlAction, $isSubmit, $regle = null)
    {
        $msgOk = '';
        $msgKo = '';

        /** @var EditeurRegleForm $form */
        $form = $this->getDispositifService()->getEditeurRegleFormPourDispositif();

        $form = $this->processBindDispositifRegleForm($idDispositif, $regle, $form);

        if ($isSubmit) {
            /** @var Request $request */
            $request  = $this->getRequest();
            $dataPost = $request->getPost();
            unset($dataPost['submit']);

            $form->setData($dataPost);

            if ($form->isValid()) {
                //Le formulaire est valide
                //Bind de l'objet DispositifRegle
                $regle = $this->bindObjectRegle($idDispositif, $dataPost['id']);
                $regle->setType($dataPost['type']);
                $regle->setConditionRegle($dataPost['conditionRegle']);
                $regle->setExpression($dataPost['expression']);
                //sauvegarde de la règle
                $this->getDispositifService()->saveDispositifRegle($regle);
                //Affichage d'un message de confirmation de l'enresgitrement
                $msgOk = "L'enregistrement de la règle a bien été réalisé";
                //Affichage du formulaire en création => avec règle vide
                $form = $this->processBindDispositifRegleForm($idDispositif, null, $form);
                //Remettre à "False" le champ cahcé d'indication de modification de données
                /** @var Element $eltChange */
                $eltChange = $form->get('existeChangement');
                $eltChange->setValue("false");
            }
        }

        //création du modèle d'éditeur
        return $this->getDispositifService()->getEditeurRegleViewModel($form, $msgOk, $msgKo, $urlAction, 'formRegleEditeur');
    }

    /**
     * Permet de créer ou récupérer l'objet DispositifRegle
     *
     * @param $idDispositif
     * @param $idRegle
     *
     * @return null|DispositifRegle
     * @throws Exception
     */
    private function bindObjectRegle($idDispositif, $idRegle)
    {
        if (empty($idRegle)) {
            $regle      = new DispositifRegle();
            $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);
            $regle->setDispositif($dispositif);
        } else {
            $regle = $this->getDispositifService()->getDispositifRegleParId($idRegle);
        }

        return $regle;
    }

    /**
     * Permet de faire le bind du formulaire
     *
     * @param int              $idDispositif
     * @param DispositifRegle  $regle
     * @param EditeurRegleForm $form
     *
     * @return mixed
     * @throws Exception
     */
    private function processBindDispositifRegleForm($idDispositif, $regle, $form)
    {
        if (empty($regle)) {
            //En ajout, création d'une règle vierge
            $regle = new DispositifRegle();
            /** @var Dispositif $dispositif */
            $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);
            $regle->setDispositif($dispositif);
        }
        //Construction des données d'hydratation du formulaire de l'éditeur
        $data = [
            'idParent'               => $idDispositif,
            'id'                     => $regle->getId(),
            'type'                   => $regle->getType(),
            'expressionLitteral'     => $this->getRegleService()->getChaineLitterale($regle->getExpression()),
            'expression'             => $regle->getExpression(),
            'conditionRegleLitteral' => $this->getRegleService()->getChaineLitterale($regle->getConditionRegle()),
            'conditionRegle'         => $regle->getConditionRegle()
        ];

        //Hydratation du formulaire
        $form->bind(new ArrayObject($data));

        return $form;
    }

}
