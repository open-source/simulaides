<?php
namespace Simulaides\Controller\Back;

/**
 * Classe IBackController
 * Permet de séparer les couches front des couches back office
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
interface IBackController
{

}
