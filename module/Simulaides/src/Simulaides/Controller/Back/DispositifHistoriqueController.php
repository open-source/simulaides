<?php
namespace Simulaides\Controller\Back;

use Simulaides\Constante\TableConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\DispositifHistorique;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Form\Dispositif\DispositifHistoriqueForm;
use Simulaides\Service\DispositifHistoriqueService;
use Simulaides\Service\DispositifService;
use Simulaides\Tools\Utils;
use Zend\Form\Element;
use Zend\Http\Request;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Classe DispositifHistoriqueController
 * Controller pour la gestion de l'onglet "Historique" de la fiche dispositif
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class DispositifHistoriqueController extends SessionController implements IBackController
{
    /** @var  DispositifService */
    private $dispositifService;

    /** @var DispositifHistoriqueService */
    private $dispositifHistoriqueService;

    /**
     * Retourne la vue pour l'onglet "Historique du dispositif"
     *
     * @return ViewModel
     */
    public function dispositifFicheHistoriqueAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();

        $dispositif = null;
        $isSubmit   = false;

        $form = $this->getDispositifService()->getDispositifHistoriqueForm();

        if ($request->isPost()) {
            $dataPost     = $request->getPost();
            $isSubmit     = isset($dataPost['submit']);
            $idDispositif = $dataPost['idDispositif'];
            unset($dataPost['submit']);
        } else {
            $idDispositif = $this->getEvent()->getRouteMatch()->getParam('idDispositif');
        }

        //Récupération du dispositif
        $dispositif = $this->getDispositifService()->getDispositifParId($idDispositif);

        //Bind du formulaire avec un historique vierge
        $form = $this->processBindFormHistorique($form, $dispositif);

        //Le formulaire a été validé
        if ($isSubmit) {
            $form->setData($dataPost);

            if ($form->isValid()) {
                //Ajout de la note par sauvegarde de l'historique
                $dispositifHistorique = $form->getObject();
                $this->getDispositifHistoriqueService()->saveDispositifHistorique($dispositifHistorique);
                //Bind du formulaire avec un historique vierge
                $form = $this->processBindFormHistorique($form, $dispositif);
            }
        }

        $model = new ViewModel(
            [
                'form'      => $form,
                'tabEntete' => $this->getHistoriqueEnteteTableau(),
                'tabLignes' => $this->getHistoriqueLignesTableau($dispositif)
            ]
        );
        $model->setTerminal(true);

        $model->setTemplate('simulaides/dispositif-historique/dispositif-fiche-historique.phtml');
        $data['htmlView'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

        return new JsonModel($data);
    }

    /**
     * Permet de faire le bind du formulaire
     *
     * @param DispositifHistoriqueForm $form
     * @param Dispositif               $dispositif Dispositif
     *
     * @return mixed
     */
    private function processBindFormHistorique($form, $dispositif)
    {
        //Initialisation d'un nouvel historique
        $dispositifhistorique = new DispositifHistorique();
        $dispositifhistorique->setDateNote(new \DateTime());
        $dispositifhistorique->setIdDispositif($dispositif->getId());
        $dispositifhistorique->setDispositif($dispositif);

        //Bind du formulaire
        $form->bind($dispositifhistorique);
        $form->add(
            [
                'name' => 'PHPSESSID',
                'type' => 'hidden',
                'attributes' => [
                    'value' => $this->getSessionService()->getManager()->getId()
                ]
            ]
        );
        //Remettre à "False" le champ cahcé d'indication de modification de données
        /** @var Element $eltChange */
        $eltChange = $form->get('existeChangement');
        $eltChange->setValue("false");

        return $form;
    }

    /**
     * Retourne le service sur les dispositifs
     *
     * @return array|object|DispositifService
     */
    private function getDispositifService()
    {
        if (empty($this->dispositifService)) {
            $this->dispositifService = $this->getServiceLocator()->get(DispositifService::SERVICE_NAME);
        }
        return $this->dispositifService;
    }

    /**
     * Retourne le service sur les dispositifs
     *
     * @return array|object|DispositifHistoriqueService
     */
    private function getDispositifHistoriqueService()
    {
        if (empty($this->dispositifHistoriqueService)) {
            $this->dispositifHistoriqueService = $this->getServiceLocator()->get(
                DispositifHistoriqueService::SERVICE_NAME
            );
        }
        return $this->dispositifHistoriqueService;
    }

    /**
     * Retourne les informations de l'entête du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des historiques
     *
     * @return array
     */
    private function getHistoriqueEnteteTableau()
    {
        $tabEntete = [];
        //Colonne date
        $tabEntete[] = new TableCellule('Date');
        //Colonne Note
        $tabEntete[] = new TableCellule('Note');

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des historique
     *
     * @param Dispositif $dispositif Dispositif
     *
     * @return array
     */
    private function getHistoriqueLignesTableau($dispositif)
    {
        $tabLignes = [];
        //Sélection des historiques du dispositif
        $tabHistorique = $this->getDispositifHistoriqueService()->getListHistoriquePourDispositif($dispositif);

        /** @var DispositifHistorique $historique */
        foreach ($tabHistorique as $historique) {
            $ligne = new TableLigne();
            $ligne->setId($historique->getId());

            //Cellule de la date
            $dateCellule = Utils::formatDateToString($historique->getDateNote());
            $cellule     = new TableCellule($dateCellule, TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule de la note
            $cellule = new TableCellule($historique->getNote(), TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;
        }

        return $tabLignes;
    }
}
