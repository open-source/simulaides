<?php
namespace Simulaides\Controller\Back;

use Simulaides\Constante\TableConstante;
use Simulaides\Controller\SessionController;
use Simulaides\Domain\Entity\Departement;
use Simulaides\Domain\TableObject\TableCellule;
use Simulaides\Domain\TableObject\TableLigne;
use Simulaides\Service\LocalisationService;
use Simulaides\Service\ParametrageService;
use Simulaides\Service\PartenaireService;
use Simulaides\Service\ProjetService;
use Zend\Http\Request;
use Zend\Stdlib\ArrayObject;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Classe StatistiqueController
 * Controller pour la page de statistique
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller\Back
 */
class StatistiqueController extends SessionController implements IBackController
{
    /**
     * Date de début de la période
     *
     * @var \DateTime
     */
    private $dateDebut;

    /**
     * Date de fin de la période
     *
     * @var \DateTime
     */
    private $dateFin;

    /**
     * Permet d'afficher la page de statistique
     *
     * @return array|ViewModel
     */
    public function indexAction()
    {
        /** @var Request $request */
        $request = $this->request;

        //Récupération du formulaire
        $form = $this->getEtInitFormulaireStatistique();

        if ($request->isPost()) {
            //Le filtre sur la période a été activé
            $dataPost = $request->getPost();
            //Set les datas au formulaire
            $form->setData($dataPost);
            if ($form->isValid()) {
                $dataObject = $form->getObject();
                //Le fomrulaire est valide, on recharge les données du tableau
                $this->dateDebut = $dataObject['debutPeriode'];
                $this->dateFin = $dataObject['finPeriode'];
            }
        }

        /** @var LocalisationService $localisationService */
        $localisationService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
        $regions = $localisationService->getListeRegions();
        $departements = $localisationService->getDepartementsPourListeRegion(["84"]);

        $model = new ViewModel([
            'form' => $form,
            'regions' => $regions,
            'departements' => $departements,
            'statsNational'=>$this->getStatsNational()
        ]);
        $model->addChild($this->getViewModelTableau(), 'tabStat');
        $model->addChild($this->getStatParPartenaire(), 'tabStatPartenaire');
        $model->addChild($this->getStatParEPCI("01"), 'tabStatEPCI');

        return $model;
    }

    /**
     * Retourne le formulaire et fait le bind des datas
     *
     * @return \Simulaides\Form\Statistique\StatistiqueForm
     */
    private function getEtInitFormulaireStatistique()
    {
        /** @var ParametrageService $paramService */
        $paramService = $this->getServiceLocator()->get(ParametrageService::SERVICE_NAME);
        $form         = $paramService->getStatistiqueForm();

        //Définition des dates de la période initiale
        $currentYear = date('Y');
        //Par défaut on prend du 1er janvier de l'année jusqu'à la date du jour
        $this->dateDebut = new \DateTime('01/01/' . $currentYear);
        $this->dateFin   = new \DateTime('now');

        //Bind du formulaire
        $data['debutPeriode'] = $this->dateDebut;
        $data['finPeriode']   = $this->dateFin;
        $form->bind(new ArrayObject($data));

        return $form;
    }

    /**
     * Permet de retourner la vue du tableau
     *
     * @return ViewModel
     */
    private function getViewModelTableau()
    {
        //Création de la vue
        $model = new ViewModel([
            'tabEntete' => $this->getStatistiqueEnteteTableau(),
            'tabLignes' => $this->getStatistiqueLignesTableau()
        ]);
        $model->setTemplate('simulaides/statistique/statistique-tableau.phtml');

        return $model;
    }


    /**
     * Retourne les informations de l'entête du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des statistiques
     *
     * @return array
     */
    private function getStatistiqueEnteteTableau()
    {
        $tabEntete = [];
        //Colonne Région
        $tabEntete[] = new TableCellule('Région');
        //Colonne Nombre de simulation
        $tabEntete[] = new TableCellule('Nombre de simulations');

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des statistiques
     *
     * @return array
     */
    private function getStatistiqueLignesTableau()
    {
        $tabLignes = [];
        //Sélection des statistiques par région
        /** @var ProjetService $projetService */
        $projetService  = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        $tabStatistique = $projetService->getNbSimulationParRegion($this->dateDebut, $this->dateFin);
        $totalNbProjet = 0;

        foreach ($tabStatistique as $statistique) {
            $ligne = new TableLigne();

            //Cellule de la région
            $cellule = new TableCellule($statistique['nom'], TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule du nb de simulation
            $cellule = new TableCellule($statistique['nbProjet'], TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;


            //Cellule du nb de simulation
            $totalNbProjet += $statistique['nbProjet']?$statistique['nbProjet']:0;
        }


        $ligne = new TableLigne();

        //Cellule de la région
        $cellule = new TableCellule('Total', TableConstante::TYPE_TEXTE);
        $ligne->addCellule($cellule);

        $cellule = new TableCellule($totalNbProjet, TableConstante::TYPE_TEXTE);
        $ligne->addCellule($cellule);

        $tabLignes[] = $ligne;

        return $tabLignes;
    }



    /**
     * Permet de retourner la vue du tableau de stat par partenaire
     *
     * @return ViewModel
     */
    private function getStatParPartenaire()
    {
        //Création de la vue
        $model = new ViewModel([
            'tabEntete' => $this->getStatParPartenaireEnteteTableau(),
            'tabLignes' => $this->getStatParPartenaireLignesTableau()
        ]);
        $model->setTemplate('simulaides/statistique/statistique-tableau.phtml');

        return $model;
    }

    /**
     * Permet de retourner la vue du tableau de stat
     *
     * @return ViewModel
     */
    private function getStatsNational()
    {
        $projetService  = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        $nationalStats = $projetService->getNbSimulationNational($this->dateDebut, $this->dateFin);

        if(isset($nationalStats[0])) return $nationalStats[0];
        else return null;
    }

    /**
     * Retourne les informations de l'entête du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des statistiques
     *
     * @return array
     */
    private function getStatParPartenaireEnteteTableau()
    {
        $tabEntete = [];
        //Colonne Nom du partenaire
        $tabEntete[] = new TableCellule('Partenaire');
        //Colonne Domaine
        $tabEntete[] = new TableCellule('Domaine');
        //Colonne Nombre de simulations
        $tabEntete[] = new TableCellule('Nombre de simulations');

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des statistiques
     *
     * @return array
     */
    private function getStatParPartenaireLignesTableau()
    {
        $tabLignes = [];
        //Sélection des statistiques par région
        /** @var PartenaireService $partenaireService */
        $projetService  = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        $tabStatistique = $projetService->getNbSimulationParPartenaire($this->dateDebut, $this->dateFin);

        $totalNbProjet = 0;
        foreach ($tabStatistique as $statistique) {
            $ligne = new TableLigne();

            //Cellule du Nom du partenaire
            $cellule = new TableCellule($statistique['nom'], TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule du Domaine
            $cellule = new TableCellule($statistique['url'], TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule du nb de simulation
            $totalNbProjet += $statistique['nbProjet']?$statistique['nbProjet']:0;
            $cellule = new TableCellule($statistique['nbProjet'], TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;
        }
        $ligne = new TableLigne();

        //Cellule du Nom du partenaire
        $cellule = new TableCellule('', TableConstante::TYPE_TEXTE);
        $ligne->addCellule($cellule);

        //Cellule du Domaine
        $cellule = new TableCellule('Total', TableConstante::TYPE_TEXTE);
        $ligne->addCellule($cellule);

        //Cellule du nb de simulation
        $cellule = new TableCellule($totalNbProjet, TableConstante::TYPE_TEXTE);
        $ligne->addCellule($cellule);

        $tabLignes[] = $ligne;

        return $tabLignes;
    }


    /**
     * Permet de retourner la vue du tableau de stat par EPCI
     *
     * @return ViewModel
     */
    private function getStatParEPCI($departement=null)
    {
        //Création de la vue
        $model = new ViewModel([
            'tabEntete' => $this->getStatParEPCIEnteteTableau(),
            'tabLignes' => $this->getStatParEPCILignesTableau($departement)
        ]);
        $model->setTemplate('simulaides/statistique/statistique-tableau.phtml');

        return $model;
    }


    /**
     * Retourne les informations de l'entête du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des statistiques
     *
     * @return array
     */
    private function getStatParEPCIEnteteTableau()
    {
        $tabEntete = [];
        //Colonne Nom de l'EPCI
        $tabEntete[] = new TableCellule('EPCI');
        //Colonne Montant travaux moyen
        $tabEntete[] = new TableCellule('Montant travaux moyen (€)');
        //Colonne Taux d'aide moyen
        $tabEntete[] = new TableCellule('Taux d\'aide moyen (%)');
        //Colonne Nombre de simulations
        $tabEntete[] = new TableCellule('Nombre de simulations');

        return $tabEntete;
    }

    /**
     * Retourne les lignes du tableau formatées pour le render "Table"
     * Pour le tableau de la liste des statistiques
     *
     * @return array
     */
    private function getStatParEPCILignesTableau($departement=null)
    {
        $tabLignes = [];
        //Sélection des statistiques par région
        /** @var ProjetService $projetService */
        $projetService  = $this->getServiceLocator()->get(ProjetService::SERVICE_NAME);
        $tabStatistique = $projetService->getNbSimulationParEPCI($this->dateDebut, $this->dateFin, $departement);

        $totalMontantMoyen = 0;
        $totalTauxMoyen = 0;
        $totalNbProjet = 0;

        foreach ($tabStatistique as $statistique) {
            $ligne = new TableLigne();

            //Cellule de la région
            $cellule = new TableCellule($statistique['nom'], TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule du montant d'aide moyen
            $totalMontantMoyen += $statistique['montantMoyen']?$statistique['montantMoyen']:0;
            $cellule = new TableCellule($statistique['montantMoyen']?round($statistique['montantMoyen'],2):$statistique['montantMoyen'], TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule du taux d'aide moyen
            $totalTauxMoyen += $statistique['tauxMoyen']?$statistique['tauxMoyen']:0;
            $cellule = new TableCellule($statistique['tauxMoyen']?round($statistique['tauxMoyen'],2):$statistique['tauxMoyen'], TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //Cellule du nb de simulation
            $totalNbProjet += $statistique['nbProjet']?$statistique['nbProjet']:0;
            $cellule = new TableCellule($statistique['nbProjet'], TableConstante::TYPE_TEXTE);
            $ligne->addCellule($cellule);

            //ajout de la ligne à la liste des lignes
            $tabLignes[] = $ligne;
        }
        $ligne = new TableLigne();

        //Cellule de la région
        $cellule = new TableCellule('Total', TableConstante::TYPE_TEXTE);
        $ligne->addCellule($cellule);

        //Cellule du montant d'aide moyen
        $cellule = new TableCellule(round($totalMontantMoyen,2), TableConstante::TYPE_TEXTE);
        $ligne->addCellule($cellule);

        //Cellule du taux d'aide moyen
        $cellule = new TableCellule(round($totalTauxMoyen, 2), TableConstante::TYPE_TEXTE);
        $ligne->addCellule($cellule);

        //Cellule du nb de simulation
        $cellule = new TableCellule($totalNbProjet, TableConstante::TYPE_TEXTE);
        $ligne->addCellule($cellule);

        $tabLignes[] = $ligne;


        return $tabLignes;
    }


    public function getDepartementsAction(){
        $region = $this->getEvent()->getRouteMatch()->getParam('idRegion');

        /** @var LocalisationService $localisationService */
        $localisationService = $this->getServiceLocator()->get(LocalisationService::SERVICE_NAME);
        /** @var Departement[] $departements */
        $departements = $localisationService->getDepartementsPourListeRegion([$region]);

        $data = [];
        foreach ($departements as $departement){
            $data[]=["code" => $departement->getCode(),"nom" => $departement->getNom()];
        }
        return new JsonModel($data);
    }

    public function getEpciStatsAction(){

        $departement = $this->getEvent()->getRouteMatch()->getParam('idDepartement');

        $viewRender = $this->getServiceLocator()->get('ViewRenderer');

        //Récupération du formulaire
        $form = $this->getEtInitFormulaireStatistique();

        /** @var Request $request */
        $request = $this->request;
        if ($request->isPost()) {
            //Le filtre sur la période a été activé
            $dataPost = $request->getPost();
            //Set les datas au formulaire
            $form->setData($dataPost);
            if ($form->isValid()) {
                $dataObject = $form->getObject();
                //Le fomrulaire est valide, on recharge les données du tableau
                $this->dateDebut = $dataObject['debutPeriode'];
                $this->dateFin = $dataObject['finPeriode'];
            }
        }

        return new JsonModel(["htmlView" => $viewRender->render($this->getStatParEPCI($departement))]);
    }
}
