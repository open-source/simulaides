<?php
namespace Simulaides\Controller;

use Simulaides\Domain\SessionObject\ProjetSession;
use Simulaides\Service\ProjetSessionContainer;
use Simulaides\Service\SessionContainerService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Classe SessionController
 * Controller permettant de gérer les éléments de la session
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Simulaides\Controller
 */
class SessionController extends AbstractActionController
{
    /**
     * Permet de vérifier que le particulier a validé les CGU
     *
     * @return \Zend\Http\Response
     */
    public function verifieCGU()
    {
        /** @var SessionContainerService $session */
        $session = $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);

        //Si aucun conseiller est connecté, c'est qu'il s'agit d'un particulier => vérification
        if (!$session->estConseillerConnecte()) {
            //si les CGU ne sont pas validées, retour à la page d'accueil
            if (!$session->checkCGU()) {
                $this->flashMessenger()->setNamespace('checkCGU')->addMessage(
                    'Vous devez valider les conditions générales d’utilisation pour réaliser une simulation'
                );
                return $this->redirect()->toRoute('front-home');
            }
        }
    }

    /**
     * Indique si une nouvelle simulation pour le particulier est lancée
     *
     * @return bool
     */
    public function estNouvelleSimulationParticulier()
    {
        $idProjet = null;

        /** @var ProjetSessionContainer $session */
        $session = $this->getServiceLocator()->get(ProjetSessionContainer::SERVICE_NAME);

        /** @var ProjetSession $projetSession */
        $projetSession = $session->getProjet();
        if (!empty($projetSession)) {
            //Si un projet est chargé (ancienne simulation), alors l'Id est renseigné
            $idProjet = $projetSession->getId();
        }

        return (empty($idProjet));
    }

    /**
     * Retourne le parent de la route (admin ou front)
     *
     * @return mixed
     */
    public function getParentRoute()
    {
        //Route appelante
        $selfRoute = $this->getEvent()->getRouteMatch()->getMatchedRouteName();
        //Récupération de la première partie
        $tabPart = explode('/', $selfRoute);
        $parent  = $tabPart[0];

        return $parent;
    }

    /**
     * @return SessionContainerService
     */
    protected function getSessionService() {
        return $this->getServiceLocator()->get(SessionContainerService::SERVICE_NAME);
    }

}
