/**
 * Created by yoan.durand on 04/03/15.
 */

/**
 * Permet de supprimer un travaux
 */
$(document).ready(function () {


    $(document).on('click', '#delete-travaux', function (e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            titre = "Suppression du type de travaux",
            message = "La suppression de ce type de travaux entraîne la suppression des données que vous avez pu" +
                " saisir. " +
                "Souhaitez-vous continuer ?";

        showModalConfirm(titre, message, "window.location.replace('" + url + "')");
    });

    $(document).on('change', '.devis-user', function (e) {

        var classes = $(this).attr('class').split(' ');
        if (!$(this).is(':checked')) {
        //if ($(this).val() === '0') {
            $('.hidable-' + classes[1] + ' input:text').val('0');
            $('.hidable-' + classes[1]).hide();
        } else {
            $('.hidable-' + classes[1]).show();
        }
    });

    /**
     * Ajoute un astérisque rouge pour indiquer que le nombre de fenêtres est à saisir
     */
    $('form[action$="T01"] input[type="text"]').each(function () {
        var id = $(this).attr('id');
        if (typeof id !== "undefined" && id.match('^valeur(_[A-Z]+)*_[A-Z]+_[A-Z]+_1$') !== null) {
            $('label[for="' + id + '"]').append('<span class="required"> * </span>');
        }
    });

});

