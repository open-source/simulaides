/*
 Fichier js de l'écran de paramétrage du référentiel des prix
 */

/**
 * Fonction de simulation du submit du formulaire d'aide CEE
 */
function submitFormCEE() {
    var options = {},
        form = $('#formCEE'),
        url = form.attr('action');

    options.data = form.serializeArray();
    options.error = exceptionMessage;
    options.success = _successSubmitFormCEE;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}
/**
 * Mise à jour de l'onglet avec le formulaire retourné
 *
 * @param data
 * @private
 */
function _successSubmitFormCEE(data) {
    var pnl = $('#pnlAideCEE');

    pnl.html(data.htmlView);
}

/**
 * Fonction appelée sur le changement de valeur de la liste des travaux
 */
function changeTravauxRefPrix() {
    var options = {},
        form = $('#formRefPrix'),
        url = form.attr('action');

    options.data = form.serializeArray();
    options.error = exceptionMessage;
    options.success = _successChangeTravaux;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}
/**
 * Fonction appelée sur le success du changement de travaux
 *
 * @param data
 * @private
 */
function _successChangeTravaux(data) {
    $("#divTabProduits").html(data.htmlView);
}

/**
 * Appelée sur la modification des prix d'un produit
 * @param codeProduit
 */
function modifFicheRefPrixProduit(codeProduit) {

    var options = {};
    options.data = { territoire: $('[name="territoire"]').val(), code: codeProduit, PHPSESSID: phpsessid, };
    options.error = exceptionMessage;
    options.success = _afficheModaleRefPrixProduit;

    executeAjaxRequest('/admin/parametrage-ref-produit-modif', options);
}

/**
 * Succès de la demande d'affichage du formulaire de prix d'un produit
 * Affichage de la modale avec le formulaire retourné
 * @param data
 * @private
 */
function _afficheModaleRefPrixProduit(data) {
    var titre = "Modifier le produit";
    showModalForm(data.htmlView, titre);
    //Affichage de l'éditeur wisiwig
    afficheEditeurWisiwig('infosSaisieCouts');
    afficheEditeurWisiwig('infosSaisieCoutsOu');
}

/**
 * Permet de faire le submit de la modale
 * de
 */
function submitformRefPrix() {
    console.log("submitformRefPrix");
    updateEditeurWisiwigValeurSaisie('infosSaisieCouts');
    updateEditeurWisiwigValeurSaisie('infosSaisieCoutsOu');
    submitModalForm();
    //Suppression de l'éditeur Wisiwig
    //Important car des event sont ajoutés sur l'éditeur et empêche
    //l'exécution de js lors de l'affichage de la modale
    deleteEditeurWisiwig('infosSaisieCouts');
    deleteEditeurWisiwig('infosSaisieCoutsOu');
}

