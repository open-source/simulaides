/**
 * Permet de masquer ou afficher le tableau du détail des travaux
 *
 * @param idLien
 * @param idPanel
 */
function afficheOuMasqueDetailTravaux(idLien, idPanel) {
    var panel = $('#' + idPanel).get(0);

    if (panel.style.display == 'none') {
        panel.style.display = 'block';
        $('a#' + idLien).text('Masquer les détails par travaux');
    } else {
        panel.style.display = 'none';
        $('a#' + idLien).text('Voir les détails par travaux');
    }
}

/**
 * Permet de faire le submit du formulaire "Autre dispositif"
 */
function submitFormAutreDispositif() {
    var options = {}, form = $('#formAutreDispositif'), url = form.attr('action');

    options.data = form.serializeArray();
    options.data.push({name: "PHPSESSID", value: phpsessid});
    options.data.push({name: "submit", value: true});

    options.success = _successSubmitAutreDispositif;
    options.error   = exceptionMessage;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Appelée sur le succès de la validation du formulaire
 * d'un autre dispositif
 *
 * @param data
 * @private
 */
function _successSubmitAutreDispositif(data) {
    var panel = $('#pnlAutreDispositif'), divAide = $('#resultat-aide');
    //Mise à jour du formulaire
    panel.html(data.htmlView);
    //Mise à jour du montant de l'aide
    divAide.html(data.aide);
}

/**
 * Permet de faire le submit du formulaire "Autre commentaire"
 */
function submitFormAutreCommentaire() {
    var options = {}, form = $('#formAutreCommentaire'), url = form.attr('action');

    options.data = form.serializeArray();
    options.data.push({name: "PHPSESSID", value: phpsessid});
    options.data.push({name: "submit", value: true});

    options.success = _successSubmitAutreCommentaire;
    options.error   = exceptionMessage;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Appelée sur le succès de la validation du formulaire
 * d'un autre commentaire
 *
 * @param data
 * @private
 */
function _successSubmitAutreCommentaire(data) {
    var panel = $('#pnlAutreCommentaire');
    panel.html(data.htmlView);
}

/**
 * Permet de faire le submit du formulaire "Génration PDF"
 */
function submitFormGenerePdf() {

    var options = {}, form = $('#pnlPDF #formPDF'), url = form.attr('action');
    addWaiting();
    options.data = form.serializeArray();
    options.data.unshift({name: "PHPSESSID", value: phpsessid});
    options.data.push({name: "envoyer", value: true});
    options.success = _successSubmitPDFParMail;
    options.error   = exceptionMessage;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

function generePDF() {
    var form = $('#formPDF');

    window.location = form.find('#urlPDF').val();
}

/**
 * Appelée sur le succès de la validation du formulaire
 * d'un autre commentaire
 *
 * @param data
 * @private
 */
function _successSubmitPDFParMail(data) {
    var panel = $('#pnlPDF');
    panel.html(data.htmlView);
    removeWaiting();
}

function triggerDlPDF() {
    $('#formPDF input[name="telecharger"]').trigger('click');
}



/**
 * Afficher / masquer la bulle d'aide pour le bouton flottant "Besoin d'un conseiller ?"
 */
function toggleHelpBlock() {

    let x = document.getElementById('help-block');

    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
    /*window.open("https://www.faire.gouv.fr/trouver-un-conseiller","_blank");*/

}

/**
 * Activer / désactiver le bouton valider envoi email sur page Mes Résultats selon si champ adresse mail vide ou non
 */
function disableInput() {
    if(document.getElementById("email").value==="") {
        document.getElementById('btn-email').disabled = true;
    } else {
        document.getElementById('btn-email').disabled = false;
    }
}
