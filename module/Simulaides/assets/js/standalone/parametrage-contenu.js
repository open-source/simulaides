/**
 * Fonction a appelée sur le choix d'un contenu
 */
function chargeContenu() {
    var options = {},
        form = $('#formContenu'),
        url = form.attr('action');

    options.data = form.serializeArray();
    options.data.push({name: "changeContenu", value: true});
    options.error = exceptionMessage;
    options.success = _successChangeSaveContenu;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

function initObjectSelect() {
    //Initialisation du plugin chosen
    initSelectChosen();

    //Implémentation du onChange sur les objectSelect
    //qui ont la classe change-select uniquement
    $('.change-select').chosen().change(
        function () {
            chargeContenu();
        }
    );

    //Empêcher que le clic sur le "Entree" ne valide le formulaire
    $('#formContenu').on('keyup keypress', function(e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });
}

function submitFormContenu() {
    var options = {},
        form = $('#formContenu'),
        url = form.attr('action');

    updateEditeurWisiwigValeurSaisie('descriptif');
    options.data = form.serializeArray();
    options.data.push({name: "submitContenu", value: true});
    options.error = exceptionMessage;
    options.success = _successChangeSaveContenu;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

function updateColor(){
    $('.note-editable').html($('#html5colorpicker').val().substr(1));
}

/**
 * Sur le succès du changement/sauvegarde de contenu
 *
 * @param data
 * @private
 */
function _successChangeSaveContenu(data) {
    chargeTabAvecHtml('contenuLien', data.htmlView);
    if($('.chosen-select').val()=='COULEUR_TEXTE_ACCUEIL_2'){
        $('.note-toolbar').html('Sélectionnez une couleur : <input type="color" id="html5colorpicker" value="#'+$('.note-editable').html()+'">');
        $('#html5colorpicker').spectrum({
            change: function(color) {
                        $('.note-editable').html(color.toHexString().substr(1));
                    }
        });
    }
}
