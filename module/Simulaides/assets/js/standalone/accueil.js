/**
 * Fait le submit du formulaire d'accès à une simulation
 */
function submitFormAccesSimulationAccueil() {
    postScrollTopMessage();
    var options = {},
        form = $('#frmAcces'),
        url = form.attr('action');

    options.data = form.serializeArray();
    options.error = exceptionMessage;
    options.success = _successSubmitAccesSimulationAccueil;

    //Lancement de la requête ajax
    addWaiting();
    executeAjaxRequest(url, options);
}

/**
 * Fonction appelée sur le succes du submit du formulaire
 * d'accès à la simulation
 * @param data
 * @private
 */
function _successSubmitAccesSimulationAccueil(data) {
    if (data.simulationOk == true) {
        //Le projet existe, Ouverture de la simulation avec les caractéristiques
        window.location = data.url;
    } else {
        removeWaiting();
        $("#divAccesAccueil").html(data.htmlView);
    }
}

/**
 * Fait le submit du formulaire de vérification des CGU
 */
function submitFormCheckCguAccueil() {
    var options = {},
        form = $('#frmCgu'),
        url = form.attr('action');

    options.data = form.serializeArray();
    options.data.push({name:'matchCaptcha[g-recaptcha-response]', value: $('#g-recaptcha-response').val()});
    options.error = exceptionMessage;
    options.success = _successSubmitCheckCguAccueil;
    $('#start_simul').prop('disabled', 'disabled');
    $('#start_simul span').removeClass('chevron');
    $('#start_simul span').addClass('loader');
    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Fonction appelée sur le succes du submit du formulaire
 * de vérification des CGU
 * @param data
 * @private
 */
function _successSubmitCheckCguAccueil(data) {
    if (data.checkCguOK == true) {
        //Le projet existe, Ouverture d'une nouvelle simulation
        window.location = data.url;
    } else {
        var htmlResponse = $(data.htmlView);
        $('#cgu').parent('.col-xs-12').html(htmlResponse.find('#cgu').parent('.col-xs-12').html());
        $('#start_simul').siblings(".alert-danger").remove();
        $('#start_simul').parent('.col-xs-12').append(htmlResponse.find('#start_simul').siblings(".alert-danger"));
        grecaptcha.reset();
        $('#start_simul span').removeClass('loader');
        $('#start_simul span').addClass('chevron');
        $('#start_simul').prop('disabled', false);
    }
}

/**
 * Fait le submit du formulaire
 */
function submitFormAccueil(typeLogement) {
    var options = {},
        form = $('#frmCgu'),
        url = form.attr('action');
    $('#codeTypeLogement').val(typeLogement);

    options.data = form.serializeArray();
    options.error = exceptionMessage;
    options.success = _successSubmitAccueil;
    $('#start_simul').prop('disabled', 'disabled');
    $('#start_simul span').removeClass('chevron');
    $('#start_simul span').addClass('loader');
    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Fonction appelée sur le succes du submit du formulaire
 * @param data
 * @private
 */
function _successSubmitAccueil(data) {
    if (data.checkCguOK == true) {
        //Le projet existe, Ouverture d'une nouvelle simulation
        window.location = data.url;
    }
}


// tesnhf v<byrfbhsildwyuqdgmzi
function initAccueil() {
    /*$('#divAccesAccueil').addClass('hidden');

    $('#affiche-divAccesAccueil').click(function(evt){
        $('#divAccesAccueil').removeClass('hidden');
        evt.preventDefault();
    });*/
}




