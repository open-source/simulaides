var comparatif;

/**
 * Appel Ajax
 *
 * @param url URL à appeler
 * @param options Options de l'appel AJAX
 *
 */
function executeAjaxRequestComparatif(url, options,) {
    options.data.push({name: "PHPSESSID", value: phpsessid});
    options.data.push({name: "isComparatif", value: true});
    executeAjaxRequest(url, options);
}

/* -------------------------------------------------------------------------------------------------------------------------------
   ----------------------------------------------------- GESTION DES MODALES -----------------------------------------------------
   ------------------------------------------------------------------------------------------------------------------------------- */

/**
 * Affiche la modale de confirmation pour l'annulation de la simulation dans le comparatif
 */
function showModalAnnulerSimulation() {
    showModalPerteSimulation(_annulerSimulation);
}

/**
 * Affiche la modale de perte de données de la simulation dans le comparatif
 */
function showModalPerteSimulation(callback, target) {
    if ($('#tabs>.active').hasClass('dirty')) {
        showModalConfirmStdCallback('Annuler', 'Cette action va entraîner la perte des données renseignées. Voulez-vous continuer ?', callback, target)
    } else {
        callback(target);
    }
    return false;
}

/**
 * Affiche la modale de changement d'onglet dans le comparatif
 */
function showModalChangeTab(callback, target, isForced) {
    if (isForced === true || !target.closest('li').hasClass('active')) {
        if ($('#tabs>.active').hasClass('dirty')) {
            showModalConfirmStdCallback("Changer d'onglet", "Vous allez quitter l'onglet sans enregistrer vos modifications. Voulez-vous continuer ?", callback, target)
        } else {
            callback(target);
        }
    }
    return false;
}

/**
 * Affiche la modale de confirmation pour la suppression d'une simulation
 */
function showModalSupprimerSimulation(target) {
    let message;
    if (hasUniqueSimulation()) {
        message = "Il s’agit de la dernière simulation du comparatif. Cette action va entraîner la dissociation de la simulation et du comparatif ; mais également la suppression du comparatif. Voulez-vous continuer ?";
    } else {
        message = "Cette action va entraîner la dissociation de la simulation et du comparatif. Voulez-vous continuer ?";
    }

    showModalConfirmStdCallback('Supprimer', message, supprimerSimulation, target);

    return false;
}

/* -------------------------------------------------------------------------------------------------------------------------------
   ----------------------------------------------------- GESTION DES ONGLETS -----------------------------------------------------
   ------------------------------------------------------------------------------------------------------------------------------- */

/**
 * Retourne d'id de la simulation correspondant à l'index
 * @param index
 */
function getIdSimulationByTabIndex(index) {
    switch (index) {
        case 0:
            return comparatif.idSimulation1;
        case 1:
            return comparatif.idSimulation2;
        case 2:
            return comparatif.idSimulation3;
    }
}

/**
 * Retourne d'index correspondant à l'id de la simulation
 * @param idSimulation
 */
function getTabIndexByIdSimulation(idSimulation) {
    switch (idSimulation) {
        case comparatif.idSimulation1:
            return 0;
        case comparatif.idSimulation2:
            return 1;
        case comparatif.idSimulation3:
            return 2;
    }
}

/**
 * Affiche le contenu de l'onglet de simulation et ajoute les écouteurs
 * @param data
 * @private
 */
function _showTabSimuContent(data) {
    $('#simulation').html(data.htmlView).addClass('active');
    $('.stepper-compartif').on('click', 'a', changeEtapeSimulation);
    $('.fleche-retour').on('click', 'a', changeEtapeSimulation);
    $("#simulation form").on('change', ':input', function () {
        $('#tabs>.active').addClass('dirty');
    });
    removeWaiting();
}

/**
 * Active/désactive les liens des onglets
 * @param target
 * @private
 */
function _toggleLinksTabComparatif(target) {
    target.parent().parent().find('a.disabled').removeClass('disabled');
    target.addClass('disabled');
}

/**
 * Recharge le contenu de l'onglet
 * @param target
 */
function onClickTabComparatif(target) {
    if (!target.hasClass('disabled')) {
        $('#enregistrerComparatif').prop('disabled', true);
        if (target.closest('li').hasClass('active')) {
            showModalPerteSimulation(loadTabComparatif, target);
        } else {
            showModalChangeTab(loadTabComparatif, target);
        }
    }
    return false;
}

/**
 * Charge le contenu de l'onglet comparatif
 */
function loadTabComparatif(target, noValidation) {
    const url        = target.attr('href'),
          dataTypeLogement = target.attr('data-type_logement');
          tabs       = $('#tabs>li'),
          currentTab = target.closest('li'),
          options    = {data: [{name: 'noValidation', value: noValidation}], error: _showTabSimuContent, success: _showTabSimuContent};
          if(dataTypeLogement){
              options.data.push({name: "codeTypeLogement", value: dataTypeLogement});
          }
    addWaiting();
    executeAjaxRequestComparatif(url, options);

    tabs.removeClass('active');
    currentTab.addClass('active');

    if (getIdSimulationByTabIndex(tabs.index(currentTab)) === '') {
        $('#titre-form').text(target.attr('title')).removeClass('hide');
    }

    _toggleLinksTabComparatif(target);
}

/**
 * Déverrouille l'onglet suivant s'il existe
 */
function unlockNextTab() {
    const nextTab = $('#tabs li.active').next();
    nextTab.removeClass('disabled');
    nextTab.find('a').each(function (index) {
        $(this).removeClass('disabled');
    });
    $('#enregistrerComparatif').prop('disabled', false)
}

/**
 * Affiche le résumé
 */
function afficheResumeTab(idSimulation, callback) {
    if (idSimulation !== undefined) {
        const activeTab = $('#tabs>li.active'), options = {}, url = 'simulation-comparatif-resumer-simulation', waitResume = 'waitResume';
        options.data    = [{name: "idSimulation", value: idSimulation}];
        options.error   = exceptionMessage;
        options.success = function (data) {
            if (data.htmlView !== '') {
                const resume = activeTab.find('.panel-body .resume');
                resume.replaceWith(data.htmlView);
                activeTab.find('.panel-body > .text-modif-simu').addClass('hide');
                activeTab.find('.panel-body > .text-choix-simu').addClass('hide');
                activeTab.find('.simul-titre-panel>.hide').removeClass('hide'); // Affiche les icônes
                activeTab.removeClass('dirty');
                updateComparatif(activeTab, activeTab.find('.panel-body .resume .id-simulation').text());
            }
            $('#titre-form').addClass('hide');
            if (callback !== undefined) {
                callback();
            }
            removeWaiting(waitResume);
        };
        addWaiting(0.5, waitResume);
        //Lancement de la requête ajax
        executeAjaxRequestComparatif(url, options);
    }
}

/**
 * Charge la simulation correspondant à l'onglet
 * @param target
 */
function onClickTabConsult(target) {
    if (target.closest('a').length === 0) {
        const currentTab = target.closest('li'),
              numero     = currentTab.find('.numero-simulation').text(),
              motPasse   = currentTab.find('.mdp-simulation').text(),
              url        = currentTab.parent().data('url');

        if (numero !== '' && motPasse !== '') {
            const prevTab = $('#tabs>li.active'),
                  options = {
                      type:    'POST',
                      data:    [{name: 'numSimulation', value: numero}, {name: 'codeAcces', 'value': motPasse}],
                      error:   exceptionMessage,
                      success: function (data) {
                          $("#simulation").html(data.htmlView);
                          $('#enregistrerComparatif').prop('disabled', false);

                          prevTab.removeClass('active');
                          currentTab.addClass('active');

                          removeWaiting();
                      },
                  };

            if (prevTab.length === 1) {
                // Dans le cas où l'onglet activé précédemment n'affichait pas de résumé, on va le récupérer
                afficheResumeTab(
                    getIdSimulationByTabIndex($('#tabs>li').index(prevTab)),
                    function () {
                        addWaiting();
                        executeAjaxRequestComparatif(url, options);
                    }
                );
            } else {
                addWaiting();
                executeAjaxRequestComparatif(url, options);
            }


        }
    }

    return false;
}


/* -------------------------------------------------------------------------------------------------------------------------------
   --------------------------------------------------- GESTION DES SIMULATIONS ---------------------------------------------------
   ------------------------------------------------------------------------------------------------------------------------------- */

/**
 * Change d'étape de simulation
 * @param e
 */
function changeEtapeSimulation(e) {
    e.preventDefault();
    e.stopImmediatePropagation();   //Nécessaire car l'event est dispatché 2 fois
    loadTabComparatif($(e.target).closest('a'), true);
    return false;
}

/**
 * Fait le submit du formulaire d'une simulation
 * @param idForm
 * @param callback
 */
function submitFormSimulation(idForm, callback) {
    if (callback === undefined) {
        callback = _showTabSimuContent;
    }
    const options = {}, form = $('#' + idForm), url = form.attr('action');

    options.data    = form.serializeArray();
    options.error   = exceptionMessage;
    options.success = callback;
    addWaiting();
    //Lancement de la requête ajax
    executeAjaxRequestComparatif(url, options);
}
/**
 * Fait le submit du formulaire de la popup cdp comparatif
 * @param idForm
 * @param callback
 */
function submitFormPopupCdpSimulation(idForm) {
    boutonChecked = $('input[name=dispositifCdp]:checked').val();
    if(boutonChecked == undefined){
        alert('Au moins Coup de Pouce doit être séléctionné');
        return false;
    }else{
        const options = {}, form = $('#' + idForm), url = form.attr('action');
        numero     = currentTab.find('.numero-simulation').text();
        motPasse   = currentTab.find('.mdp-simulation').text();
        options.data    = form.serializeArray();
        options.data.push({name:'numSimulation', value: numero});
        options.data.push({name:'codeAcces', value: motPasse});
        options.data.push({name:'isComparatif', value: true});

        options.error   = exceptionMessage;
        options.success = _showTabSimuContent;
        addWaiting();
        //Lancement de la requête ajax
        executeAjaxRequestComparatif(url, options);
    }

}
/**
 * Fait le submit du formulaire des caractéristiques d'une simulation
 * @param informationEligibilite bool
 */
function submitFormSimulationCarac(informationEligibilite) {
    if (informationEligibilite) {
        submitFormSimulation('frmCaracteristique');
    }
}

/**
 * Fait le submit du formulaire d'une simulation
 * @param idForm
 */
function submitFormResultatSimulation(idForm) {
    submitFormSimulation(idForm, function (data) {
        if (data.idSimulation !== undefined) {
            afficheResumeTab(data.idSimulation);
            unlockNextTab();
        }
        _showTabSimuContent(data);
    })
}

/**
 * Annule de la simulation dans le comparatif
 * @private
 */
function _annulerSimulation() {
    const tabs = $('#tabs>li'), currentTab = $('#tabs>li.active'), indexActiveTab = tabs.index(currentTab);
    if (comparatif.idComparatif === null || comparatif.idComparatif === '') {
        // En mode création de comparatif
        if (indexActiveTab === 0) {
            // Si on est sur la 1ere simulation
            location.reload();
        } else {
            // Réactivation des liens désactivés
            currentTab.find('a.disabled').removeClass('disabled');
            // Consultation de l'onglet précédent
            tabs.eq(indexActiveTab - 1).click();
        }
    } else {
        // En mode modification de comparatif, on affiche la consultation du bloc courant
        onClickTabConsult(currentTab);
    }
}

/**
 * Charge la modification de simulation
 * @param target
 */
function modifierSimulation(target) {
    const currentTab      = target.closest('li'),
          tabs            = $('#tabs>li'),
          indexCurrentTab = tabs.index(currentTab),
          prevTab         = $('#tabs>li.active'),
          idSimulation    = getIdSimulationByTabIndex(indexCurrentTab),
          url             = target.attr('href'),
          options         = {
              'data':    [{name: 'idSimulation', value: idSimulation}],
              'error':   exceptionMessage,
              'success': _showTabSimuContent
          };
    $('#enregistrerComparatif').prop('disabled', true);

    function callbackModifierSimulation() {
        prevTab.removeClass('active');
        currentTab.addClass('active');

        if (idSimulation !== '') {
            currentTab.find('.panel-body > .text-modif-simu').removeClass('hide');
            currentTab.find('.panel-body > .resume').addClass('hide');
        }
        $('#titre-form').text(target.attr('title')).removeClass('hide');

        addWaiting();
        //Lancement de la requête ajax
        executeAjaxRequestComparatif(url, options);
    }

    if (prevTab.length === 1) {
        afficheResumeTab(getIdSimulationByTabIndex(tabs.index(prevTab)), callbackModifierSimulation);
    } else {
        callbackModifierSimulation();
    }

}

/**
 * Supprime la simulation du comparatif
 */
function supprimerSimulation(target) {
    const idSimulation = getIdSimulationByTabIndex($('#tabs>li').index(target.closest('li'))),
          url          = target.attr('href');
    const options      = {
        data:    [{name: 'idSimulation', value: idSimulation}, {name: 'idComparatif', value: comparatif.idComparatif}],
        error:   exceptionMessage,
        success: function (data) {
            if (data.idComparatif === undefined || data.idComparatif === null) {
                location.reload();
            } else {
                modifierComparatif(data.url, data.idComparatif, data.motPasse);
            }
        },
    };
    addWaiting();
    //Lancement de la requête ajax
    executeAjaxRequestComparatif(url, options);
}

/**
 * Duplique la simulation
 * Attention : ne la lie pas en bdd au comparatif !
 */
function dupliquerSimulation(target) {
    if (!isFullSimulation()) {
        const tabs         = $('#tabs>li'),
              idSimulation = getIdSimulationByTabIndex(tabs.index(target.closest('li'))),
              url          = target.attr('href'),
              prevTab      = $('#tabs>li.active'),
              options      = {
                  data:    [{name: 'idSimulation', value: idSimulation}, {name: 'idComparatif', value: comparatif.idComparatif}],
                  error:   exceptionMessage,
                  success: function (data) {
                      const idSimulation = data.idSimulation;

                      if (comparatif.idSimulation2 === '') {
                          comparatif.idSimulation2 = idSimulation
                      } else if (comparatif.idSimulation3 === '') {
                          comparatif.idSimulation3 = idSimulation
                      }
                      const currentTab = $('#tabs>li').eq(getTabIndexByIdSimulation(idSimulation));
                      $("#simulation").html(data.htmlView);
                      currentTab.addClass('active');
                      afficheResumeTab(idSimulation); // Affiche le résumer pour l'onglet courant
                      unlockNextTab();
                      removeWaiting();
                  }
              };

        // Récupération du résumé de l'onglet précédent
        if (prevTab.length === 1) {
            afficheResumeTab(
                getIdSimulationByTabIndex($('#tabs>li').index(prevTab)),
                function () {
                    addWaiting();
                    executeAjaxRequestComparatif(url, options);
                }
            );
            prevTab.removeClass('active');
        } else {
            addWaiting();
            executeAjaxRequestComparatif(url, options);
        }


    }// Sinon, aucun emplacement de simulation n'est libre, le bouton ne fait rien
    return false;
}

/**
 * Détermine s'il n'y a qu'une simulation dans le comparatif actuel
 */
function hasUniqueSimulation() {
    return comparatif.idSimulation2 === '' && comparatif.idSimulation3 === '';
}

/**
 * Détermine si tous les emplacements de simulation sont pris
 */
function isFullSimulation() {
    return comparatif.idSimulation2 !== '' && comparatif.idSimulation3 !== '';
}


/* -------------------------------------------------------------------------------------------------------------------------------
   ---------------------------------------------------- GESTION DU COMPARATIF ----------------------------------------------------
   ------------------------------------------------------------------------------------------------------------------------------- */
/**
 * Initialise le comparatif
 * @param idComparatif
 * @param idSimulation1
 * @param idSimulation2
 * @param idSimulation3
 */
function initComparatif(idComparatif, idSimulation1, idSimulation2, idSimulation3) {
    if (idComparatif === undefined) {
        idComparatif = null;
    }
    if (idSimulation1 === undefined) {
        idSimulation1 = null;
    }
    if (idSimulation2 === undefined) {
        idSimulation2 = null;
    }
    if (idSimulation3 === undefined) {
        idSimulation3 = null;
    }
    comparatif = {
        'idComparatif':  idComparatif,
        'idSimulation1': idSimulation1,
        'idSimulation2': idSimulation2,
        'idSimulation3': idSimulation3
    };
}

/**
 * Redirige vers la page de modification du comparatif
 * @param urlModification
 * @param idComparatif
 * @param motPasse
 */
function modifierComparatif(urlModification, idComparatif, motPasse) {
    if (idComparatif === undefined) {
        idComparatif = '';
    }
    if (motPasse === undefined) {
        motPasse = '';
    }
    const form = $(
        '<form action="' + urlModification + '" name="consulter" method="post" style="display:none;">' +
        '<input type="text" name="idComparatif" value="' + idComparatif + '" />' +
        '<input type="text" name="motPasse" value="' + motPasse + '" />' +
        '<input type="text" name="PHPSESSID" value="' + phpsessid + '" />' +
        '</form>'
    );
    $('body').append(form);
    form.submit();
}

/**
 * Met à jour le comparatif
 * @param activeTab
 * @param idSimulation
 */
function updateComparatif(activeTab, idSimulation) {
    const indexActiveTab = $('#tabs>li').index(activeTab);

    switch (indexActiveTab) {
        case 0:
            comparatif.idSimulation1 = idSimulation;
            break;
        case 1:
            comparatif.idSimulation2 = idSimulation;
            break;
        case 2:
            comparatif.idSimulation3 = idSimulation;
            break;
    }
}

/**
 * Tente l'enregistrement du comparatif de simulations
 * @param url string
 */
function submitComparatif(url) {
    const options = {};

    options.data    = [
        {name: 'idSimulation1', value: comparatif.idSimulation1},
        {name: 'idSimulation2', value: comparatif.idSimulation2},
        {name: 'idSimulation3', value: comparatif.idSimulation3},
        {name: 'idComparatif', value: comparatif.idComparatif}
    ];
    options.error   = function (xhr, status, error) {
        showModalAlert('Comparatif', 'L’enregistrement du comparatif a échoué. Veuillez réessayer ! ');
        removeWaiting();
    };
    options.success = function (data) {
        removeWaiting();
        if (data.length === 0) {
            showModalAlert('Comparatif', 'L’enregistrement du comparatif a échoué. Veuillez réessayer ! ');
        } else {
            showModalAlert('Comparatif', 'Votre comparatif a été enregistré avec succès.');
            setTimeout(function () {
                const form = $(
                    '<form action="' + data.urlConsultation + '" name="consulter" method="post" style="display:none;">' +
                    '<input type="text" name="idComparatif" value="' + data.idComparatif + '" />' +
                    '<input type="text" name="motPasse" value="' + data.motPasse + '" />' +
                    '<input type="text" name="PHPSESSID" value="' + phpsessid + '" />' +
                    '</form>'
                );
                $('body').append(form);
                form.submit();
            }, 3000);
        }
    };
    addWaiting();
    //Lancement de la requête ajax
    executeAjaxRequestComparatif(url, options);
}
