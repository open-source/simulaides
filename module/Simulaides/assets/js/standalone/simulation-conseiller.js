/**
 * Fonction appelée sur le changement de valeur de la liste des régions
 */
function changeRegionSimulation() {
    var options = {}, form = $('#formRegion'), url = form.attr('action');

    options.data    = form.serializeArray();
    options.error   = exceptionMessage;
    options.success = _successChangeRegion;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Permet l'initialisation de la liste déroulante
 */
function initObjectSelect() {
    //Initialisation du plugin chosen
    initSelectChosen();

    //Implémentation du onChange sur les objectSelect
    //qui ont la classe change-select uniquement
    $('.change-select').chosen().change(function () {
        changeRegionSimulation();
    });

    if ($('#codeRegion').val() !== '') {
        $('[name="conseiller-executer-test-board"]').show();
    }

    //Empêcher que le clic sur le "Entree" ne valide le formulaire
    $('#formRegion').on('keyup keypress', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });
}

/**
 * Permet de gérer le fonctionnement de la case selectionner tout
 */
function initSelectDeselectAllCheckbox() {
    $('#tableDisp #select-deselect-tout').change(function () {
            if ($(this).prop('checked')) {
                $('#tableDisp td input[type="checkbox"]').prop('checked', true);
            } else {
                $('#tableDisp td input[type="checkbox"]').prop('checked', false);
            }
        }
    );

    $('#tableDisp td input[type="checkbox"]').change(function () {
        if (!$(this).prop('checked')) {
            $('#tableDisp #select-deselect-tout').prop('checked', false);
        } else if ($('#tableDisp td input[type="checkbox"]:not(:checked)').length === 0) {
            $('#tableDisp #select-deselect-tout').prop('checked', true);
        }
    });
}

/**
 * Appelée sur le succes du changement de région dans la liste déroulante
 * @param data
 * @private
 */
function _successChangeRegion(data) {
    $("#tableDisp").html(data.htmlView);
    if ($('#codeRegion').val() === '') {
        $('[name="conseiller-executer-test-board"]').hide();
    } else {
        $('[name="conseiller-executer-test-board"]').show();
    }
    initSelectDeselectAllCheckbox();
}

/**
 * Fait le submit du formulaire d'accès à une simulation
 */
function submitFormAccesSimulation() {
    var options = {}, form = $('#frmAcces'), url = form.attr('action');

    options.data = form.serializeArray();
    options.data.push({name: "PHPSESSID", value: phpsessid});
    options.error   = exceptionMessage;
    options.success = _successSubmitAccesSimulation;

    //Lancement de la requête ajax
    addWaiting()
    executeAjaxRequest(url, options);
}

/**
 * Fonction appelée sur le succes du submit du formulaire
 * d'accès à la simulation
 * @param data
 * @private
 */
function _successSubmitAccesSimulation(data) {
    if (data.simulationOk == true) {
        //Le projet existe, ouverture d'un nouvel onglet avec les caractéristiques
        removeWaiting();
        ouvreSimulationDansNouvelOnglet('/admin/simulation/caracteristique?PHPSESSID=' + phpsessid);
    } else {
        //Le projet n'existe pas, affichage du formulaire avec l'erreur
        removeWaiting();
        $("#divAcces").html(data.htmlView);
    }
}

/**
 * Fait le submit du formulaire de test avec une nouvelle simulation
 */
function submitFormNouvelleSimulationTest(codeTypeLogement) {
    var options = {}, formDispositif = getFormListeDispositif();
    if (verifieSelectionRegion()) {
        if (verifieSelectionDispositif()) {
            options.data = $(formDispositif).serializeArray();
            options.data.push({name: 'PHPSESSID', value: phpsessid});
            options.data.push({name: 'codeTypeLogement', value: codeTypeLogement});
            options.error   = exceptionMessage;
            options.success = _successSubmitNouvelleSimulationTest;
            //Lancement de la requête ajax
            executeAjaxRequest('/admin/simulation-conseiller-test-nouveau', options);
        } else {
            showModalAlert('Simulation', 'Veuillez sélectionner au moins un dispositif.');
        }
    } else {
        showModalAlert('Simulation', 'Veuillez sélectionner une région.');
    }

}

/**
 * Fonction appelée sur le succes du lancement de la simulation de test
 * avec un projet vierge
 * @private
 */
function _successSubmitNouvelleSimulationTest() {
    //Ouverture d'un nouvel onglet avec les caractéristiques
    ouvreSimulationDansNouvelOnglet('/admin/simulation/caracteristique?PHPSESSID=' + phpsessid);
}

/**
 * Permet de vérifier si au moins un dispositif ets renseigné
 *
 * @returns {boolean}
 */
function verifieSelectionDispositif() {
    var formDispositif = getFormListeDispositif(), tabDispositif = $(formDispositif).serializeArray();

    return (tabDispositif.length > 0);
}

/**
 * Permet de vérifier si une région est sélectionnée
 *
 * @returns {boolean}
 */
function verifieSelectionRegion() {
    var formRegion = $('#formRegion').get(0), codeRegion = $(formRegion).find('#codeRegion').val();

    return (codeRegion != "");
}

/**
 * Retourne le formulaire de la liste des dispositifs
 * @returns {*|jQuery|HTMLElement}
 */
function getFormListeDispositif() {
    return $('#formSimulDispositif').get(0);
}

/**
 * Fait le submit du formulaire d'accès à une simulation de test
 */
function submitFormAccesSimulationTest() {
    var options = {},idDispositif=0;
    var idDispositifEval = $("#idDispositif");
    if(idDispositifEval !== undefined){
        idDispositif =  idDispositifEval.val();
    }
    if(idDispositif){
        options.data   = $('#frmAccesTest').serializeArray();
        options.data.push({name: idDispositif, value: 'on'});
        options.data.push({name: 'PHPSESSID', value: phpsessid});
        options.error   = exceptionMessage;
        options.success = _successSubmitAccesSimulationTest;
        //Lancement de la requête ajax
        executeAjaxRequest('/admin/simulation-conseiller-test-existant', options);

    }else{
        var form = $('#frmAccesTest'), formDispositif = getFormListeDispositif(), dataAcces, dataDispositif;

        if (verifieSelectionRegion()) {
            if (verifieSelectionDispositif()) {
                dataAcces      = form.serializeArray();
                dataDispositif = $(formDispositif).serializeArray();
                options.data   = $('#frmAccesTest,#formSimulDispositif').serializeArray();
                options.data.push({name: "PHPSESSID", value: phpsessid});
                options.error   = exceptionMessage;
                options.success = _successSubmitAccesSimulationTest;

                //Lancement de la requête ajax
                executeAjaxRequest('/admin/simulation-conseiller-test-existant', options);
            } else {
                showModalAlert('Simulation', 'Veuillez sélectionner au moins un dispositif.');
            }
        } else {
            showModalAlert('Simulation', 'Veuillez sélectionner une région.');
        }
    }
}

/**
 * Fonction appelée sur le succes du submit du formulaire
 * d'accès à la simulation de test
 * @param data
 * @private
 */
function _successSubmitAccesSimulationTest(data) {
    if (data.simulationOk == true) {
        //Le projet existe, Ouverture d'un nouvel onglet avec les caractéristiques
        ouvreSimulationDansNouvelOnglet('/admin/simulation/caracteristique?PHPSESSID=' + phpsessid);
    } else {
        $("#divAccesTest").html(data.htmlView);
    }
}


/**
* Fait le submit du formulaire de test avec une nouvelle simulation
*/
function submitTestSimulationTest(codeTypeLogement) {
    var  options = {data: []};
    var idDispositif = $("#idDispositif").val();
    options.data.push({name: idDispositif, value: 'on'});
    options.data.push({name: 'PHPSESSID', value: phpsessid});
    options.data.push({name: 'codeTypeLogement', value: codeTypeLogement});
    options.error   = exceptionMessage;
    options.success = _successSubmitNouvelleSimulationTest;
    //Lancement de la requête ajax
    executeAjaxRequest('/admin/simulation-conseiller-test-nouveau', options);
}
