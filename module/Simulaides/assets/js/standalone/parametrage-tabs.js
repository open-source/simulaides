/////////////////////////////////////////////////
// Fonction appelée sur le clic des onglets
/////////////////////////////////////////////////
function ajouteClickTab() {
    $('#tabs').on('click', 'a', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();   //Nécessaire car l'event est dispatché 2 fois
        var $this = $(this),
            loadurl = $this.attr('href'),
            targ = $this.attr('data-target'),
            options = {};

        options.error = exceptionMessage;
        options.success = function (data) {
            $(targ).html(data.htmlView);
            afficheEditeurWisiwig('descriptif');
        };
        executeAjaxRequest(loadurl, options);

        $this.tab('show');

        return false;
    });
}

/**
 * Affichage de l'onglet et de son contenu
 * à partir de l'url de l'onglet
 *
 * @param idTab identifiant de l'onglet à afficher
 */
function afficheTab(idTab) {
    var obja = $('#tabs a[id="' + idTab + '"]').tab('show'),
        loadurl = obja.attr('href'),
        targ = obja.attr('data-target'),
        options = {};

    options.error = exceptionMessage;
    options.success = function (data) {
        $(targ).html(data.htmlView);
    };
    executeAjaxRequest(loadurl, options);

    obja.tab('show');

    return false;
}

/**
 * Permet de charger un onglet à partir d'une url donnée
 *
 * @param idTab identifiant de l'onglet à afficher
 * @param url url servant à charger l'onglet
 * @returns {boolean}
 */
function chargeTabAvecUrl(idTab, url) {

    var obja = $('#tabs a[id="' + idTab + '"]').tab('show'),
        targ = obja.attr('data-target'),
        options = {};

    options.type = 'GET';
    options.error = exceptionMessage;
    options.success = function (data) {
        $(targ).html(data.htmlView);
        removeWaiting();
    };
    executeAjaxRequest(url, options);

    obja.tab('show');

    return false;
}

/**
 * Permet de charger le contenu html d'un onglet
 *
 * @param idTab Identifiant de l'onglet à afficher
 * @param html Contenu HTML
 * @returns {boolean}
 */
function chargeTabAvecHtml(idTab, html) {
    var obja = $('#tabs a[id="' + idTab + '"]').tab('show'),
        targ = obja.attr('data-target');

    $(targ).html(html);
    deleteEditeurWisiwig('descriptif')
    afficheEditeurWisiwig('descriptif');

    return false;
}
