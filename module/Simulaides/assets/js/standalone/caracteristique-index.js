/**
 * FICHE DE CARACTERISTIQUE
 */

var codePostalUpdateRequest = null;
/**
 * Initialisation des object Select
 */
function initCaracteristiqueObjectSelect(urlParent) {
    $('#text-codePostal').keyup(function(){
        changeCodePostal(urlParent);
    });

    $('#nbAdultes').keyup(function(){
        calculPartsFiscales();
    });

    $('#nbPersonnesCharge').keyup(function(){
        calculPartsFiscales();
    });

    $('#nbEnfantAlterne').keyup(function(){
        calculPartsFiscales();
    });

}

function changeCodePostal(urlParent){
    var selectRegion = $('#select-region'),
        options = {};

    var codeP = $('#text-codePostal');
    if(codeP.val().length !== 5){
        options = '<option value="" selected>Choisissez votre commune</option>';
        var target = $('#select-commune');
        target.prop('disabled', true);
        target.empty();
        target.append(options);
        target.trigger('chosen:updated');
    }else{

        options.data = {type: 'codePostal', value: {codePostal: codeP.val(), region: selectRegion.val()}};

        options.error = exceptionMessage;
        options.success = _successChangeCodePostal;

        var optionLoading = '<option value="" selected>Recherche de commune...</option>';
        var target = $('#select-commune');
        target.prop('disabled', true);
        target.empty();
        target.append(optionLoading);
        target.trigger('chosen:updated');

        if(codePostalUpdateRequest != null){
            codePostalUpdateRequest.abort('cancelRequest');
            codePostalUpdateRequest = executeAjaxRequest(urlParent, options);
        }else{
            codePostalUpdateRequest = executeAjaxRequest(urlParent, options);
        }
    }
}
/**
 * Fonction appelée sur le succès du changement du code postal
 *
 * @param data
 * @private
 */
function _successChangeCodePostal(data){
    if (data.status) {
        var options = '';
        if(data.options.length === 2){
            options += '<option value="' + data.options[0].code + '">' + data.options[0].nom + '</option>';
            options += '<option value="' + data.options[1].code + '" selected>' + data.options[1].nom + '</option>';
        }else{
            for (var i = 0; i < data.options.length; i++) {
                options += '<option value="' + data.options[i].code + '">' + data.options[i].nom + '</option>';
            }
        }
        var target = $('#select-' + data.target);
        if (data.options.length > 1) {
            target.removeAttr('disabled');
        }
        target.empty();
        target.append(options);
        target.trigger('chosen:updated');
        if(data.outre_mer){
            $('#block-codeenergie').hide();
        }else{
            var divToShow = $('#block-codeenergie');
            if(divToShow.is(":hidden")){
                $('#codeEnergie').val("");
                $('#block-codeenergie').show();
            }
        }
        //Si le code postal saisi correspond à une commune ne faisant pas partie du département sélectionné
        if(data.options.length === 1){
            if(data.codePostalValide){
                showModalAlert('Simulation', 'Votre commune n’est pas dans la région ciblée ('+ data.currentRegion +').<br>Pour plus d’informations ou pour contacter un Conseiller du réseau Rénovation Info Service, rendez-vous sur <a href="http:\\\\www.renovation-info-service.gouv.fr" target="_blank">www.renovation-info-service.gouv.fr</a>', true);
            }else{
                showModalAlert('Simulation', 'Le code postal saisi est inconnu.<br>Pour plus d’informations ou pour contacter un Conseiller du réseau Rénovation Info Service, rendez-vous sur <a href="http:\\\\www.renovation-info-service.gouv.fr" target="_blank">www.renovation-info-service.gouv.fr</a>', true);
            }
        }
    }
}


function informationEligibilite(isComparatif){
    if(!isComparatif){
        postScrollTopMessage();
    }
    var selectCommune = $('#select-commune').val();
    var codeTypeLogement = $('#codeTypeLogement').val();
    var codeModeChauffage = $('#codeModeChauff').val();
    var anneeConstruction = $('#anneeConstruction').val();
    if (anneeConstruction.indexOf('.') != -1) {
        anneeConstruction = null;
    }else if (anneeConstruction.indexOf(',') != -1) {
        anneeConstruction = null;
    }else{
        anneeConstruction = Number($('#anneeConstruction').val());
    }
    var surfaceHabitable = $('#surfaceHabitable').val();
    if (surfaceHabitable.indexOf('.') != -1) {
        surfaceHabitable = null;
    }else if (surfaceHabitable.indexOf(',') != -1) {
        surfaceHabitable = null;
    }else{
        surfaceHabitable = Number($('#surfaceHabitable').val());
    }
    var codeEnergie = $('#codeEnergie').val();

    var nbAdultes = $('#nbAdultes').val();
    if (nbAdultes.indexOf('.') != -1) {
        nbAdultes = null;
    }else  if (nbAdultes.indexOf(',') != -1) {
        nbAdultes = null;
    }else{
        nbAdultes = Number($('#nbAdultes').val());
    }
    var nbEnfantAlterne = $('#nbEnfantAlterne').val();
    if (nbEnfantAlterne.indexOf('.') != -1) {
        nbEnfantAlterne = null;
    }else if(nbEnfantAlterne.indexOf(',') != -1) {
        nbEnfantAlterne = null;
    }else{
        nbEnfantAlterne = Number($('#nbEnfantAlterne').val());
    }
    var nbPersonnesCharge = $('#nbPersonnesCharge').val();
    if (nbPersonnesCharge.indexOf('.') != -1) {
        nbPersonnesCharge = null;
    }else if(nbPersonnesCharge.indexOf(',') != -1) {
        nbPersonnesCharge = null;
    }else{
        nbPersonnesCharge = Number($('#nbPersonnesCharge').val());
    }
    var nbPartsFiscales = parseFloat($('#nbPartsFiscales').val());
    var codeStatut = $('#codeStatut').val();
    var revenus = $('#revenus').val();
    if (revenus.indexOf('.') != -1) {
        revenus = null;
    }else if(revenus.indexOf(',') != -1) {
        revenus = null;
    }else{
        revenus = Number($('#revenus').val());
    }


    var ladate = new Date();
    var AnneeYear = ladate.getFullYear()
    var textmaprimerenovelegible = "Pour vous aider à vous sentir bien chez vous, améliorer votre confort tout en réduisant vos factures d’énergie, l’Etat vous accompagne : vous pouvez bénéficier de <b>Ma Prime Rénov’</b>, une aide juste et simple pour la rénovation énergétique. Vous pouvez la demander facilement <a href='https://www.maprimerenov.gouv.fr/prweb/PRAuth/BPNVwCpLW8TKW49zoQZpAw%5B%5B*/!STANDARD' target='_blank' title='https://www.maprimerenov.gouv.fr/prweb/PRAuth/BPNVwCpLW8TKW49zoQZpAw%5B%5B*/!STANDARD'>en ligne</a> (maprimerenov.gouv.fr) et l’aide vous sera rapidement versée par l’État à la fin de vos travaux.<br>\n" +
        "Dès mai 2020, une avance de frais pourra vous être accordée. Cela vous aidera à régler l’acompte de vos travaux.<br>\n" +
        "<br>\n" +
        "Pour plus de renseignements, n’hésitez à contacter un conseiller « FAIRE » au <b>0 808 800 700</b> ou rubrique « <a href='https://www.faire.fr/trouver-un-conseiller' target='_blank' title='https://www.faire.fr/trouver-un-conseiller'>trouver un conseiller</a> ».<br>\n" +
        "Pour connaître les autres dispositifs mobilisables en fonction de votre projet, continuez votre simulation.\n";
    var textmaprimerenovnonelegible = "Pour vous aider à améliorer votre confort tout en réduisant votre facture, en 2020 l’Etat vous permet de bénéficier du <a href='https://www.faire.fr/aides-de-financement/credit-impot-transition-energetique' target='_blank' title='https://www.faire.fr/aides-de-financement/credit-impot-transition-energetique'>Crédit d’Impôt pour la Transition Energétique (CITE)</a>.<br>\n" +
        "Vos travaux participent à réduire les émissions de gaz à effet de serre. Un bon geste pour la planète ! <br>\n" +
        "Pour connaître les autres dispositifs mobilisables en fonction de votre projet, continuez votre simulation.<br>\n";
    var titre = 'Ma Prime Rénov’';

    if(selectCommune != ""
        && codeTypeLogement !=""
        && anneeConstruction > 999 &&  anneeConstruction <= AnneeYear
        && surfaceHabitable > 0
        && nbAdultes > 0
        && nbPartsFiscales > 0
        && codeStatut != ""
        && revenus > 0
        && nbEnfantAlterne >=0
        && nbEnfantAlterne != null
        && nbPersonnesCharge >=0
        && nbPersonnesCharge != null


    ){
        if(codeStatut != 'PROP_RES_1' && codeStatut != 'PROP_BAIL'){
            return true; //pas de POP-IN
        }
        // pn vérifi le code mode chauffage si le type logement est appartement
        if(codeTypeLogement != "A" && codeModeChauffage == "")
        {
            return true; //pas de POP-IN
        }
        titre = 'MaPrimeRénov’';
        typeFoyer = getStatutFoyer(nbAdultes+nbPersonnesCharge+nbEnfantAlterne,revenus,selectCommune);
        var messageAffiche = $("#textPopUpMaPrimeRenovSuperieur").html();
        if(typeFoyer == 'TRES_MODESTE') {
            messageAffiche = $("#textPopUpMaPrimeRenovTresModeste").html();
        }else if(typeFoyer == 'MODESTE') {
            messageAffiche = $("#textPopUpMaPrimeRenovModeste").html();
        }else if(typeFoyer == 'INTERMEDIAIRE') {
            messageAffiche = $("#textPopUpMaPrimeRenovIntermediaire").html();
        }
        return showModalMessageElegibilite(titre, messageAffiche, true, isComparatif);
    }else{
        return true;
    }

}

function getStatutFoyer(nbPersonnes,revenus,selectCommune){
    var dept = selectCommune.substr(0, 2);
    if(parseInt(dept) == 75
        || parseInt(dept) == 77
        || parseInt(dept) == 78
        || parseInt(dept) == 91
        || parseInt(dept) == 92
        || parseInt(dept) == 93
        || parseInt(dept) == 94
        || parseInt(dept) == 95){
        if(nbPersonnes < 7){
            if(revenus <= listeTrancheIDFTM[nbPersonnes-1]){
                return 'TRES_MODESTE';
            }else if(revenus <= listeTrancheIDFM[nbPersonnes-1]){
                return 'MODESTE';
            }else if(revenus <= listeTrancheIDFI[nbPersonnes-1]){
                return 'INTERMEDIAIRE';
            }else{
                return 'SUPERIEUR';
            }
        }else{
            revenusMaxTM = listeTrancheIDFTM[5] + listeTrancheIDFTM[6]*(nbPersonnes-6);
            revenusMaxM = listeTrancheIDFM[5] + listeTrancheIDFM[6]*(nbPersonnes-6);
            revenusMaxI = listeTrancheIDFI[5] + listeTrancheIDFI[6]*(nbPersonnes-6);

            if(revenus <= revenusMaxTM){
                return 'TRES_MODESTE';
            }else if(revenus <= revenusMaxM){
                return 'MODESTE';
            }else if(revenus <= revenusMaxI){
                return 'INTERMEDIAIRE';
            }else{
                return 'SUPERIEUR';
            }
       }


    }else{
        if(nbPersonnes < 7){
            if(revenus <= listeTrancheTM[nbPersonnes-1]){
                return 'TRES_MODESTE';
            }else if(revenus <= listeTrancheM[nbPersonnes-1]){
                return 'MODESTE';
            }else if(revenus <= listeTrancheI[nbPersonnes-1]){
                return 'INTERMEDIAIRE';
            }else{
                return 'SUPERIEUR';
            }
        }else{
            revenusMaxTM = listeTrancheTM[5] + listeTrancheTM[6]*(nbPersonnes-6);
            revenusMaxM = listeTrancheM[5] + listeTrancheM[6]*(nbPersonnes-6);
            revenusMaxI = listeTrancheI[5] + listeTrancheI[6]*(nbPersonnes-6);

            if(revenus <= revenusMaxTM){
                return 'TRES_MODESTE';
            }else if(revenus <= revenusMaxM){
                return 'MODESTE';
            }else if(revenus <= revenusMaxI){
                return 'INTERMEDIAIRE';
            }else{
                return 'SUPERIEUR';
            }
        }
    }
}

function estEligible(codeStatut,anneeConstruction,nbPersonnes,revenus,selectCommune){
    var ladate = new Date();
    var AnneeYearPivot = ladate.getFullYear()-2;
    var revenusMax = 0;
    var revenusMax2 = 0;
    if(codeStatut != 'PROP_RES_1' && codeStatut != 'PROP_RES_1_SCI'){
        return ''; //pas de POP-IN
    }
    if(anneeConstruction >= AnneeYearPivot){
        return ''; //pas de POP-IN
    }
    var dept = selectCommune.substr(0, 2);
    if(parseInt(dept) == 75
        || parseInt(dept) == 77
        || parseInt(dept) == 78
        || parseInt(dept) == 91
        || parseInt(dept) == 92
        || parseInt(dept) == 93
        || parseInt(dept) == 94
        || parseInt(dept) == 95){
        if(nbPersonnes == 1){
            if(revenus <= 25067){
                return 'MPR';
            }else if(revenus <= 27706){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 2){
            if(revenus <= 36791){
                return 'MPR';
            }else if(revenus <= 44124){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 3){
            if(revenus <= 44187){
                return 'MPR';
            }else if(revenus <= 50281){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 4){
            if(revenus <= 51596){
                return 'MPR';
            }else if(revenus <= 56438){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 5){
            if(revenus <= 59025){
                return 'MPR';
            }else if(revenus <= 68752){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 6){
            if(revenus <= 66447){
                return 'MPR';
            }else if(revenus <= 81066){
                return 'CITE';
            }else{
                return '';
            }
        }
        revenusMax = 66447 + 7422*(nbPersonnes-6);
        revenusMax2 = 81066 + 12314*(nbPersonnes-6);
        if(nbPersonnes > 6){
            if(revenus <= revenusMax){
                return 'MPR';
            }else if(revenus <= revenusMax2){
                return 'CITE';
            }else{
                return '';
            }
        }

    }else{
        if(nbPersonnes == 1){
            if(revenus <= 19073){
                return 'MPR';
            }else if(revenus <= 27706){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 2){
            if(revenus <= 27895){
                return 'MPR';
            }else if(revenus <= 44124){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 3){
            if(revenus <= 33546){
                return 'MPR';
            }else if(revenus <= 50281){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 4){
            if(revenus <= 39191){
                return 'MPR';
            }else if(revenus <= 56438){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 5){
            if(revenus <= 44859){
                return 'MPR';
            }else if(revenus <= 68752){
                return 'CITE';
            }else{
                return '';
            }
        }
        if(nbPersonnes == 6){
            if(revenus <= 50510){
                return 'MPR';
            }else if(revenus <= 81066){
                return 'CITE';
            }else{
                return '';
            }
        }
        revenusMax = 50510 + 5651*(nbPersonnes-6);
        revenusMax2 = 81066 + 12314*(nbPersonnes-6);
        if(nbPersonnes > 6){
            if(revenus <= revenusMax){
                return 'MPR';
            }else if(revenus <= revenusMax2){
                return 'CITE';
            }else{
                return '';
            }
        }
    }
}

function calculPartsFiscales(){
    var NA = 0; //nombre d'adultes
    var NEE = 0; //nombre d'enfants en garde exclusive
    var NGA = 0; //nombre d'enfants en garde alternée
    var PA = 0; //nombre de part fiscale lié aux adultes
    var PEE = 0; //nombre de parts dues aux enfants en résidence exclusive
    var PGA = 0; //nombre de parts dues aux enfants en résidence alternée

    //nombre de part fiscale lié aux adultes
    if($('#nbAdultes').val()){
        NA = parseFloat($('#nbAdultes').val());
        PA = NA;
    }

    //nombre de parts dues aux enfants en résidence exclusive
    if($('#nbPersonnesCharge').val()){
        NEE = parseFloat($('#nbPersonnesCharge').val());
        if(NEE > 0) {
            if(NEE >= 3){
                PEE = NEE-1;
            }else{
                PEE = NEE * 0.5;
            }
        }
    }

    //nombre de parts dues aux enfants en résidence alternée
    if($('#nbEnfantAlterne').val()){
        NGA =parseFloat($('#nbEnfantAlterne').val());
        if(NGA > 0) {
            if (NEE == 0) {
                if (NGA < 2) {
                    PGA = 0.25 * NGA;
                } else {
                    PGA = (NGA / 2) - 0.5;
                }

            } else if (NEE == 1) {
                if (NGA == 1) {
                    PGA = 0.25;
                } else {
                    PGA = (NGA / 2) - 0.25;
                }
            } else {
                PGA = NGA * 0.5;
            }
        }
    }

    var partsFiscales = PA + PEE + PGA ;

    $('#nbPartsFiscales').val(partsFiscales.toString().replace('.', ','));
}
