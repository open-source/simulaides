//**********************************************************
//          LISTE DES DISPOSITIFS
//**********************************************************

$(function () {
    setJsSorterOnTable();
});

function initFilters() {
    if ($('#codeRegion').val() !== '') {
        unlockFilters();
        afficheListeDepartement();
    } else {
        lockFilters();
    }
}

function afficheListeDepartement() {
    $('#fieldset-filtres-departement :input').prop("disabled", false);
    $('#codeDepartement').trigger('chosen:updated');

    if ($('#codeDepartement').val() !== '' && $('#codeDepartement').val() !== undefined) {
        $('#fieldset-filtres-commune :input').prop("disabled", false);
        $('#codeCommune').trigger('chosen:updated');
    }else{
        $('#fieldset-filtres-commune :input').prop("disabled", true);
        $('#codeCommune').trigger('chosen:updated');

    }
}


function lockFilters() {
    $('#fieldset-filtres :input').prop("disabled", true);
    $('#fieldset-filtres-departement :input').prop("disabled", true);

    $('#filtre-dispositif').trigger('chosen:updated');
    $('#filtre-etat').trigger('chosen:updated');
    $('#filtre-travaux').trigger('chosen:updated');
    $('#codeDepartement').trigger('chosen:updated');

    $('#fieldset-filtres-commune :input').prop("disabled", true);
    $('#codeCommune').trigger('chosen:updated');

}

function unlockFilters() {

    $('#fieldset-filtres :input').prop("disabled", false);
    $('#fieldset-filtres-departement :input').prop("disabled", false);

    $('#filtre-dispositif').trigger('chosen:updated');
    $('#filtre-etat').trigger('chosen:updated');
    $('#filtre-travaux').trigger('chosen:updated');
    /*$('#codeDepartement').trigger('chosen:updated');*/

    if ($('#codeDepartement').val() !== '' && $('#codeDepartement').val() !== undefined) {
        $('#fieldset-filtres-commune :input').prop("disabled", false);
        $('#codeCommune').trigger('chosen:updated');
    }
}


/**
 * Appelée sur le succes du changement de filtre
 * @param data
 * @private
 */
function _successReloadRegion(data) {
    let table = $(data.model).find('table');

    $("#tableDisp table").html(table.html());
    setJsSorterOnTable();
    initSelectDeselectAllCheckbox();

    if ($('#locktableButton').children('.fa').hasClass('fa-unlock')) {
        $(".table tr").removeClass('nodrag');
    }

    removeWaiting();
}

/**
 * Fonction appelée sur le changement de valeur de filtre
 */
function reloadRegion(onlyTableau) {
    if (onlyTableau === undefined) {
        onlyTableau = true;
    }
    var options = {}, form = $('#formRegion'), url = form.attr('action');

    options.data = form.serializeArray();
    options.data.push({name: "onlyTableau", value: onlyTableau});
    options.error   = exceptionMessage;
    options.success = _successReloadRegion;
    addWaiting();
    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Réinitialise les filtres de recherche des dispositifs
 */
function reinitialiserFiltres() {

    $('#fieldset-filtres :input:not(:submit)').val(null);
    $('#filtre_etat_chosen span').text('Tous les états');
    $('#filtre_dispositif_chosen span').text('Tous les types');
    $('#filtre_travaux_chosen span').text('Tous');
    $('#fieldset-filtres input:checked[name=validite]').prop('checked',false);


    reloadRegion();
}


function lockTable(button) {
    if (button.children('.fa').hasClass('fa-lock')) {
        lockFilters();

        button.children('.fa').removeClass('fa-lock');
        button.children('.fa').addClass('fa-unlock');
        button.children('#lock-text').text("Interdire la modification de l'ordre de calcul des dispositifs");
        reloadRegion();
    } else {
        button.children('.fa').removeClass('fa-unlock');
        button.children('.fa').addClass('fa-lock');
        button.children('#lock-text').text("Autoriser la modification de l'ordre de calcul des dispositifs");
        $(".table tr").addClass('nodrag');

        unlockFilters();
    }
}

/**
 * Fonction de gestion du drag and drop du tableau
 */
function setJsSorterOnTable() {
    $(".table tr").addClass('nodrag');
    $(".table").rowSorter({
        onDrop: function (tbody, row, index, oldIndex) {
            var gap = index - oldIndex;
            moveDispositif(row['id'], gap);
        }, handler: "td.sorter"
    });
}

/**
 * Fonction permettant de mémoriser le changement d'ordre
 *
 * @param idToMove Identifiant de la ligne déplacée
 * @param gap Ecart à appliquer à l'ordre
 */
function moveDispositif(idToMove, gap) {
    var options = {}, form = $('#formRegion');

    options.data = form.serializeArray();
    options.data.push({name: "idDispositif", value: idToMove});
    options.data.push({name: "gap", value: gap});
    options.error = exceptionMessage;

    //Lancement de la requête ajax
    executeAjaxRequest('/admin/dispositif-change-ordre', options);
}

/**
 * Fonction appelée sur le changement de valeur de la liste des régions
 */
function changeRegion() {
    var options = {}, form = $('#formRegion'), url = form.attr('action');
    $("#codeDepartement").val('');
    $("#codeCommune").val('');
    form.submit();
    /*options.data = form.serializeArray();
    options.data.push({name: "onlyTableau", value: false});
    options.error = exceptionMessage;
    options.success = _successChangeRegion;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);*/
}

/**
 * Fonction appelée sur le changement de valeur de la liste des régions
 */
function changeDepartement() {
    var options = {}, form = $('#formRegion'), url = form.attr('action');
    $("#codeCommmune").val('');
    form.submit();
    /*options.data = form.serializeArray();
    options.data.push({name: "onlyTableau", value: false});
    options.error = exceptionMessage;
    options.success = _successChangeRegion;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);*/
}

/**
 * Fonction appelée sur le changement de valeur de la liste des régions
 */
function changeCommune() {
    var options = {}, form = $('#formRegion'), url = form.attr('action');
    form.submit();
    /*options.data = form.serializeArray();
    options.data.push({name: "onlyTableau", value: false});
    options.error = exceptionMessage;
    options.success = _successChangeRegion;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);*/
}

function initObjectRegionSelect() {

    //Initialisation du plugin chosen
    initSelectChosen();

    $('#fieldset-filtres-departement :input').prop("disabled", false);
    $('#codeDepartement').trigger('chosen:updated');

    //Implémentation du onChange sur les objectSelect
    //qui ont la classe change-select uniquement
    $('.change-select:not(#filtre-dispositif):not(#filtre-etat)').chosen().change(function () {
        changeRegion();
    });

    $('.change-selectDep:not(#filtre-dispositif):not(#filtre-etat)').chosen().change(function () {
        changeDepartement();
    });

    $('.change-selectCom:not(#filtre-dispositif):not(#filtre-etat)').chosen().change(function () {
        changeCommune();
    });

    //Empêcher que le clic sur le "Entree" ne valide le formulaire
    $('#formRegion').on('keyup keypress', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });
}

/**
 * Appelée sur le succes du changement de région dans la liste déroulante
 * @param data
 * @private
 */
function _successChangeRegion(data) {
    $("#tableDisp").html(data.model);
    setJsSorterOnTable();
    initFilters();
    if(data.model === ''){
        lockFilters();
    }else{
        unlockFilters();
    }
}

/**
 * Fonction de retour à la liste à partir de la fiche
 *
 * @param $codeRegion
 */
function retourListeDispositif($codeRegion) {
    var options = {};
    options.dataType = 'html';
    options.data = {
        onlyTableau: false, codeRegion: $codeRegion, PHPSESSID: phpsessid
    };
    options.error = exceptionMessage;
    options.success = _successChangeRegion;

    //Lancement de la requête ajax
    executeAjaxRequest('/admin/dispositif', options);
}

//**********************************************************
//          FICHE DES DISPOSITIFS
//**********************************************************

function initObjectSelect() {
    //Initialisation du plugin chosen
    initSelectChosen();
    var selectCommune = $('#select-commune');
    var selectDepartement = $('#select-departement');

    //Implémentation du onChange sur les objectSelect
    //qui ont la classe change-select uniquement
    $('.change-select').chosen().change(function () {
        var id = $(this).attr('id').split('-')[1];
        var tabVal = $(this).val();
        var target = [];
        var selectCommune = $('#select-commune');
        var selectEpci = $('#select-epci');
        if (id == 'departement' && !selectCommune.is('[disabled]')) {
            //Si on change le département, on rend inaccessible les communes
            selectCommune.attr('disabled', 'disabled');
            selectCommune.find('option[value=""]').prop('selected', true);
            selectCommune.trigger('chosen:updated');
        }
        if (id == 'departement' && !selectEpci.is('[disabled]')) {
            //Si on change le département, on rend inaccessible les epci
            selectEpci.attr('disabled', 'disabled');
            selectEpci.find('option[value=""]').prop('selected', true);
            selectEpci.trigger('chosen:updated');
        }
        if (id == 'departement') {
            target = ['commune', 'epci'];
        } else if (id == 'region') {
            target = ['departement'];
        }
        //envoi de la requête ajax pour récupérer les informations
        var options = {};
        options.data = {
            PHPSESSID: phpsessid,
            type: id,
            tabValue: tabVal,
            target: target
        };
        options.success = loadSelectFromAjax;
        options.error = exceptionMessage;
        executeAjaxRequest('/admin/dispositif-change-localisation', options);
    });

    if(selectDepartement.attr('disabled') != 'disabled'){
        selectCommune.attr('disabled', false);
        var selectDepartementChosen = $('#select_commune_chosen');
        selectDepartementChosen.removeClass('chosen-disabled');
    }
}

/**
 * Fonction appelée sur le succes du changement d'un objectSelect
 *
 * @param data
 */
function loadSelectFromAjax(data) {
    var options = '', target = '';
    for (var i = 0; i < data.length; i++) {
        //Parcours de toutes les listes à mettre à jour
        options = '';
        for (var z = 0; z < data[i].options.length; z++) {
            options += '<option value="' + data[i].options[z].code + '">' + data[i].options[z].nom + '</option>';
        }
        target = $('#select-' + data[i].target);
        if (data[i].options.length > 0) {
            target.removeAttr('disabled');
        }
        //Vide la liste
        target.empty();
        //Remplit avec nouvelle liste
        target.append(options);
        //Demande au choosen de se mettre à jour
        target.trigger('chosen:updated');
        majListApresChangement(data[i].target);
    }
}

/**
 * Mise à jour des liste après changement
 *
 * @param target
 */
function majListApresChangement(target) {
    if (target == 'departement') {
        //Si la liste qui vient d'être mise à jour est "département", la/les valeur(s) sélectionnées ont été supprimées
        //Il faut donc vider les listes et les valeurs de epci et commune pour ne pas avoir d'incohérence
        var targetEpci = $('#select-epci');
        var targetCommune = $('#select-commune');

        targetEpci.empty();
        targetEpci.attr('disabled', 'disabled');
        targetEpci.trigger('chosen:updated');
        targetCommune.empty();
        targetCommune.attr('disabled', 'disabled');
        targetCommune.trigger('chosen:updated');
    }
}

/**
 * Permet de faire le submit du formulaire
 */
function submitFormGestion() {
    var options = {}, form = $('#formGestion'), url = form.attr('action');

    //Suppression du disabled pour récupérer l'information dans le submit => nécessaire
    $("input[type=radio][name='dispositif[exclutCEE]']").prop('disabled', false);
    $("input[type=radio][name='dispositif[exclutAnah]']").prop('disabled', false);
    $("input[type=radio][name='dispositif[exclutEPCI]']").prop('disabled', false);
    $("input[type=radio][name='dispositif[exclutRegion]']").prop('disabled', false);
    $("input[type=radio][name='dispositif[exclutCommunal]']").prop('disabled', false);
    $("input[type=radio][name='dispositif[exclutNational]']").prop('disabled', false);
    $("input[type=radio][name='dispositif[exclutDepartement]']").prop('disabled', false);
    $("input[type=radio][name='dispositif[exclutCoupDePouce]']").prop('disabled', false);


    // Mise à jour de la valeur saisie dans le WYSIWYG
    updateEditeurWisiwigValeurSaisie('descriptif');

    options.data = form.serializeArray();
    options.data.push({name: "submit", value: true});

    options.error = exceptionMessage;
    options.success = _successChangeSaveGestion;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Appelée sur le bouton "Désactiver ce dispositif"
 */
function changeEtatDesactive(idDispositif, codeRegionListe) {
    //Demande de confirmation de la désactivation
    var titre = "Désactivation d'un dispositif",
        message = "Cette action va entraîner la désactivation du dispositif." + "Voulez-vous continuer ?";

    if ((typeof(idDispositif) !== 'undefined') && (typeof(codeRegionListe) !== 'undefined')) {
        showModalConfirm(titre, message, "_confirmDesactiveDispositif(" + idDispositif + ", " + codeRegionListe + ")");
    }else{
        showModalConfirm(titre, message, "_confirmDesactiveDispositif()");
    }
}

/**
 * Sur le succès du changement/sauvegarde du formulaire
 *
 * @param data
 * @private
 */
function _successChangeSaveGestion(data) {
    if (data.ajoutOK) {
        //Redirection vers la fiche en modification
        window.location = data.urlAction;
    } else {
        //Affichage de l'onglet
        chargeTabAvecHtml('gestionLien', data.htmlView);
        //Rechargement du titre de la page
        var titre = $("input[type=text][name='dispositif[intitule]']").val();
        $("h3").html(titre);
    }
}

/**
 * Appelée sur la confirmation de la désactivation
 *
 * @private
 */
function _confirmDesactiveDispositif(_idDispositif, _codeRegionListe) {
    var idDispositif = (typeof(_idDispositif) !== 'undefined') ? _idDispositif : $("input[type=hidden][name='dispositif[id]']").val();
    var codeRegionListe = (typeof(_codeRegionListe) !== 'undefined') ? _codeRegionListe : $("input[type=hidden][name='codeRegionListe']").val();
    var options = {};

    options.data = {
        idDispositif: idDispositif,
        codeRegionListe: codeRegionListe,
        PHPSESSID: phpsessid
    };
    options.error = exceptionMessage;

    if($("#codeRegion_chosen").length == 0){
        options.success = _successActionDispositif;
    }else{
        options.success = reloadRegion;
    }

    executeAjaxRequest('/admin/dispositif-desactive', options);
}

/**
 * Appelée sur le bouton "Demander la validation"
 */
function changeEtatAValider(idDispositif, codeRegionListe) {
    //Demande de confirmation de la demande de validation
    var titre = "Demande de validation d'un dispositif",
        message = "Cette action va entraîner la demande de validation du dispositif." + "Voulez-vous continuer ?";

    if ((typeof(idDispositif) !== 'undefined') && (typeof(codeRegionListe) !== 'undefined')) {
        showModalConfirm(titre, message, "_confirmAValiderDispositif(" + idDispositif + ", " + codeRegionListe + ")");
    }else{
        showModalConfirm(titre, message, "_confirmAValiderDispositif()");
    }
}

/**
 * Appelée sur la confirmation de la demande de validation
 *
 * @private
 */
function _confirmAValiderDispositif(_idDispositif, _codeRegionListe) {
    var idDispositif = (typeof(_idDispositif) !== 'undefined') ? _idDispositif : $("input[type=hidden][name='dispositif[id]']").val();
    var codeRegionListe = (typeof(_codeRegionListe) !== 'undefined') ? _codeRegionListe : $("input[type=hidden][name='codeRegionListe']").val();
    var options = {};

    options.data = {
        idDispositif: idDispositif, codeRegionListe: codeRegionListe, PHPSESSID: phpsessid
    };
    options.error = exceptionMessage;
    if($("#codeRegion_chosen").length == 0){
        options.success = _successActionDispositif;
    }else{
        options.success = reloadRegion;
    }

    executeAjaxRequest('/admin/dispositif-avalider', options);
}

function _successAValiderDispositif(data) {
    if (data.periodeKO != null && data.periodeKO == true) {
        var titre = "Demande de validation d'un dispositif",
            message = "Veuillez renseigner au moins la date de début de la période de validité du dispositif.";
        showModalAlert(titre, message);
    } else {
        _successActionDispositif(data);
    }
}

/**
 * Appelée sur le bouton "Valider ce dispositif"
 */
function changeEtatActive(idDispositif, codeRegionListe) {
    //Demande de confirmation de l'activation
    var titre = "Activation d'un dispositif",
        message = "Cette action va entraîner l'activation du dispositif." + "Voulez-vous continuer ?";

    if ((typeof(idDispositif) !== 'undefined') && (typeof(codeRegionListe) !== 'undefined')) {
        showModalConfirm(titre, message, "_confirmActiveDispositif(" + idDispositif + ", " + codeRegionListe + ")");
    }else{
        showModalConfirm(titre, message, "_confirmActiveDispositif()");
    }
}

/**
 * Permet de lancer l'activation du dispositif
 *
 * @private
 */
function _confirmActiveDispositif(_idDispositif, _codeRegionListe) {
    var idDispositif = (typeof(_idDispositif) !== 'undefined') ? _idDispositif : $("input[type=hidden][name='dispositif[id]']").val();
    var codeRegionListe = (typeof(_codeRegionListe) !== 'undefined') ? _codeRegionListe : $("input[type=hidden][name='codeRegionListe']").val();
    var options = {};

    options.data = {
        idDispositif: idDispositif, codeRegionListe: codeRegionListe, PHPSESSID: phpsessid
    };
    options.error = exceptionMessage;
    if($("#codeRegion_chosen").length == 0){
        options.success = _successActionDispositif;
    }else{
        options.success = reloadRegion;
    }

    executeAjaxRequest('/admin/dispositif-valider', options);
}

/**
 * Appelée sur le bouton "Dupliquer ce dispositif"
 */
function dupliqueDispositif() {
    //Demande de confirmation de la duplication
    var titre = "Duplication d'un dispositif",
        message = "Cette action va entraîner la duplication du dispositif." + "Voulez-vous continuer ?";

    showModalConfirm(titre, message, "_confirmDupliqueDispositif()");
}

/**
 * Appelée sur la confirmation de duplication du dispositif
 * @private
 */
function _confirmDupliqueDispositif() {
    var idDispositif = $(
        "input[type=hidden][name='dispositif[id]']").val(), codeRegionListe = $(
        "input[type=hidden][name='codeRegionListe']").val(), options = {};

    options.data = {
        idDispositif: idDispositif, codeRegionListe: codeRegionListe, PHPSESSID: phpsessid
    };
    options.error = exceptionMessage;
    options.success = _successActionDispositif;

    executeAjaxRequest('/admin/dispositif-dupliquer', options);
}

/**
 * Appelée sur le bouton "Supprimer ce dispositif"
 */
function supprimeDispositif() {
    //Demande de confirmation de la suppression
    var titre = "Suppression d'un dispositif",
        message = "Cette action va entraîner la suppression du dispositif et de toutes ses données." + "Voulez-vous continuer ?";

    showModalConfirm(titre, message, "_confirmSuppressionDispositif()");
}

/**
 * Appelée sur le success de la confirmation de supression du dispositif
 * @private
 */
function _confirmSuppressionDispositif() {
    var idDispositif = $(
        "input[type=hidden][name='dispositif[id]']").val(), codeRegionListe = $(
        "input[type=hidden][name='codeRegionListe']").val(), options = {};

    options.data = {
        idDispositif: idDispositif, codeRegionListe: codeRegionListe, PHPSESSID: phpsessid
    };
    options.error = exceptionMessage;
    options.success = _successActionDispositif;

    executeAjaxRequest('/admin/dispositif-supprime', options);
}

/**
 * Appelé sur le success d'une action sur le dispositif
 *
 * @param data
 * @private
 */
function _successActionDispositif(data) {
    window.location = data.url;
}

//**********************************************************
//          LISTE DES REGLES DU DISPOSITIF
//**********************************************************

/**
 * Fonction d'appel de modification de la règle du dispositif
 *
 * @param idRegleDispositif
 */
function modifFicheRegleDispositif(idRegleDispositif) {
    var url = '/admin/dispositif-regle-modif/' + idRegleDispositif + '?PHPSESSID=' + phpsessid;
    chargeTabAvecUrl('regleLien', url);
}

/**
 * Fonction d'appel d'ajout d'une règle de dispositif
 *
 * @param idDispositif
 */
function ajoutFicheRegleDispositif(idDispositif) {
    var url = '/admin/dispositif-regle-ajout/' + idDispositif + '?PHPSESSID=' + phpsessid;
    chargeTabAvecUrl('regleLien', url);
}

/**
 * Permet de retourner à la liste des règles du dispositif
 */
function retourListeRegle(idDispositif) {
    var url = '/admin/dispositif-regle/' + idDispositif + '?PHPSESSID=' + phpsessid;
    chargeTabAvecUrl('regleLien', url);
}

/**
 * Fonction d'appel de la suppression de la règle du dispositif
 * @param idRegleDispositif
 */
function deleteRegleDispositif(idRegleDispositif) {
    //Demande de confirmation de la suppression
    var titre = "Suppression d'une règle du dispositif",
        message = "Cette action va entraîner la suppression de la formule." + "Voulez-vous continuer ?";

    showModalConfirm(titre, message, "_confirmSuppressionRegleDispositif(" + idRegleDispositif + ")");
}

/**
 * Appelée sur la confirmation de la suppression de la règle
 * @private
 */
function _confirmSuppressionRegleDispositif(idRegleDispositif) {
    var options = {};

    options.data = {
        idRegleDispositif: idRegleDispositif, 'PHPSESSID': phpsessid
    };
    options.error = exceptionMessage;
    options.success = _successSupprimeRegleDispositif;

    executeAjaxRequest('/admin/dispositif-regle-supprime', options);
}

/**
 * Appelé sur le success de la suppression d'une règle
 *
 * @param data
 * @private
 */
function _successSupprimeRegleDispositif(data) {
    chargeTabAvecUrl('regleLien', data.url);
}

//**********************************************************
//          EDITEUR DES REGLES DU DISPOSITIF
//**********************************************************

//Variable permettant de stocker les infos de la formule
var _tabFormule = {};
var _charCursor = '¤';
var _charSeparator = '§';
var _numericTabKeyCode = [8, 37, 39, 44, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];

/**
 * Appelée sur le bouton Enregistrer du formulaire
 */
function submitFormRegle() {
    var options = {}, form = $('#' + _tabFormule.formName), url = form.attr('action');

    options.data = form.serializeArray();
    options.data.unshift({name: "PHPSESSID", value: phpsessid});
    options.data.push({name: "submit", value: true});
    options.error = exceptionMessage;
    options.success = _successChangeSaveRegle;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Sur le succès du changement/sauvegarde du formulaire
 * de la fiche des règles dispositif/travaux
 * @param data
 * @private
 */
function _successChangeSaveRegle(data) {
    //Affichage de l'onglet
    if (_tabFormule.formName == 'formRegleEditeur') {
        chargeTabAvecHtml('regleLien', data.htmlView);
    } else {
        chargeTabAvecHtml('travauxLien', data.htmlView);
    }
}

/**
 * Retourne le champ caché stockant le nom du champ actif
 * dans le formulaire en cours d'affichage
 *
 * @returns {*|jQuery}
 */
function getElementHiddenActif() {
    var form = getForm();
    return $(form).find(".activeElt").get(0);
}

/**
 * Permet de retourner le formulaire
 * @returns {*|jQuery}
 */
function getForm() {
    return $('#' + _tabFormule.formName).get(0);
}

/**
 * Permet de retourner la div contenant le formulaire
 * @returns {*|jQuery}
 */
function getDivForm() {
    return $('div[data-name="' + _tabFormule.formName + '"]').get(0);
}

/**
 * Appelée sur le click dans la zone Expression ou Condition
 * @param elementLitteral
 */
function setElementActif(elementLitteral) {
    var hiddenEltActif = getElementHiddenActif(),
        namePnl        = 'pnlOp' + elementLitteral.getAttribute('name'),
        nameDiv        = 'div' + elementLitteral.getAttribute('name'),
        divForm        = getDivForm();

    //Mémorisation du nom de l'élément actif
    $(hiddenEltActif).val(elementLitteral.getAttribute('name'));

    //Suppression du contour sur l'élément l'ayant déjà
    unsetElementActif();
    //Mise en place d'un contour pour montrer que l'élément à le focus
    $(divForm).find('div[data-name="' + nameDiv + '"]').get(0).setAttribute('class', 'has-focus');
    $(divForm).find('div[data-name="' + nameDiv + '"]').parent('div').siblings('.eraser').show();

    if (!aRoleAdeme) {
        //Positionnement du focus sur l'élément qui vient d'être activé
        positionneFocus(elementLitteral);
    }

    //Affichage des opérateurs
    afficheOperateurZone(namePnl);
    //Affichage des catégories si pas déjà fait
    $(divForm).find('div[data-name="pnlCategorie"]').get(0).style.display = 'block';
}

/**
 * Permet de désélectionner l'élément actif
 */
function unsetElementActif() {
    var divForm = getDivForm(), eltDivFocus;
    $(divForm).find('.eraser').hide();
    //Suppression du contour sur l'élément l'ayant déjà
    eltDivFocus = $(divForm).find('div.has-focus');
    if (eltDivFocus != undefined && eltDivFocus.length > 0) {
        eltDivFocus.get(0).setAttribute('class', 'form-group');
    }
    //Masque les div opérateur qui serait déjà affichés
    masqueDiv()
}

/**
 * Affichage des opérateurs
 *
 * @param namePnl Nom du panel sélectionné
 */
function afficheOperateurZone(namePnl) {
    var divForm = getDivForm(), eltTypeRegle = $(divForm).find('div[name="divPnlTypeRegle"]').find('select').get(
        0), valTypeRegle = $(eltTypeRegle).val();

    if (valTypeRegle == "E") {
        //Toujours affichage du bloc d'opérateur de la condition si "Eligibilité"
        $(divForm).find('div[data-name="pnlOpconditionRegleLitteral"]').get(0).style.display = 'block';
    } else {
        //Affichage du bloc d'opérateur de la zone
        $(divForm).find('div[data-name="' + namePnl + '"]').get(0).style.display = 'block';
    }

}

/**
 * Permet d'initialiser les tableaux des formules
 *
 * @param formName
 */
function initTabFormule(formName) {
    var form = $('#' + formName).get(0), eltCondition = $(
        form).find('.conditionRegle').get(0), eltConditionLit = $(form).find('.conditionRegleLitteral').get(
        0), eltExpression = $(form).find('.expression').get(
        0), eltExpressionLit = $(form).find('.expressionLitteral').get(
        0), tabCondition, tabConditionLit, tabExpression, tabExpressionLit;

    //transformation en tableau des chaines
    tabCondition = $(eltCondition).val().split(' ');
    tabConditionLit = $(eltConditionLit).val().split(_charSeparator);
    tabExpression = $(eltExpression).val().split(' ');
    tabExpressionLit = $(eltExpressionLit).val().split(_charSeparator);
    //Ajout du curseur en fin de tableau
    tabCondition.push(_charCursor);
    tabConditionLit.push(_charCursor);
    tabExpression.push(_charCursor);
    tabExpressionLit.push(_charCursor);

    //Mémorisation des tableaux
    _tabFormule = {
        formName: formName, conditionRegle: {
            tabData: tabCondition, positionCursor: tabCondition.length - 1
        }, conditionRegleLitteral: {
            tabData: tabConditionLit, positionCursor: tabConditionLit.length - 1
        }, expression: {
            tabData: tabExpression, positionCursor: tabExpression.length - 1
        }, expressionLitteral: {
            tabData: tabExpressionLit, positionCursor: tabExpressionLit.length - 1
        }
    };

    //Affichage des zones de saisie littérale
    dislayElement(eltConditionLit, _tabFormule.conditionRegleLitteral);
    dislayElement(eltExpressionLit, _tabFormule.expressionLitteral);
}

/**
 * Permet de gérer le changement du focus sur les touches flèches
 * @param event
 * @param elementLitteral
 */
function manageKeyUpAndFocus(event, elementLitteral) {

        var keyCode                               = ('which' in event) ? event.which : event.keyCode, tabElement, tabElementLit, newPositionCursor,
            changePosition = false, elementHidden = getElementHidden(
            elementLitteral);

        event.stopImmediatePropagation();
        tabElement    = getTabElement(elementHidden);
        tabElementLit = getTabElement(elementLitteral);

        //Recalcul de la position du curseur selon la touche
        if (keyCode == 37) {
            //Flèche gauche - Reculer le curseur si on est pas déjà au début
            if (tabElement.positionCursor > 0) {
                newPositionCursor = tabElement.positionCursor - 1;
                changePosition    = true;
            }
        } else if (keyCode == 39) {
            //flèche droite - Avancer le curseur si on est pas déjà à la fin
            if (tabElement.positionCursor < tabElement.tabData.length) {
                newPositionCursor = tabElement.positionCursor + 1;
                changePosition    = true;
            }
        }

        //Si le curseur doit être déplacé
        if (changePosition) {
            //Change la position
            changePositionElement(elementHidden, tabElement, newPositionCursor);
            changePositionElement(elementLitteral, tabElementLit, newPositionCursor);
            //Positionner le curseur dans le champ littéral
            positionneFocus(elementLitteral);
        }
}

/**
 * Permet d'empêcher la saisie de caractères dans la zone de saisie
 *
 * @param event
 */
function manageKeyDown(event) {

        var keyCode = ('which' in event) ? event.which : event.keyCode;

        if (keyCode != 37 && keyCode != 39) {
            event.stopImmediatePropagation();
        }
}

/**
 * Réalise le changement de position dans le tableau
 *
 * @param element
 * @param tabElement
 * @param newPositionCursor
 */
function changePositionElement(element, tabElement, newPositionCursor) {
    var oldPositionCursor, valOld, valNew;

    oldPositionCursor = tabElement.positionCursor;
    valOld = tabElement.tabData[oldPositionCursor];
    valNew = tabElement.tabData[newPositionCursor];
    //Inversement des valeurs dans le tableau
    tabElement.tabData[oldPositionCursor] = valNew;
    tabElement.tabData[newPositionCursor] = valOld;
    //Mémorisation de la nouvelle position du curseur
    tabElement.positionCursor = newPositionCursor;

    //Mémorisation des nouvelles positions
    saveTabElement(element, tabElement);
}

/**
 * Permet de retourner la liste des éléments de la zone non littérale
 * @param element
 * @returns {*}
 */
function getTabElement(element) {
    var tabElement, idElt = element.getAttribute('name');

    if (idElt == 'conditionRegleLitteral') {
        tabElement = _tabFormule.conditionRegleLitteral;
    } else if (idElt == 'conditionRegle') {
        tabElement = _tabFormule.conditionRegle;
    } else if (idElt == 'expressionLitteral') {
        tabElement = _tabFormule.expressionLitteral;
    } else if (idElt == 'expression') {
        tabElement = _tabFormule.expression;
    }

    return tabElement;
}

/**
 * Permet de mémoriser la modification
 *
 * @param element
 * @param tabElement
 */
function saveTabElement(element, tabElement) {
    var idElt = element.getAttribute('name');

    if (idElt == 'conditionRegleLitteral') {
        _tabFormule.conditionRegleLitteral.positionCursor = tabElement.positionCursor;
        _tabFormule.conditionRegleLitteral.tabData = tabElement.tabData;
    } else if (idElt == 'conditionRegle') {
        _tabFormule.conditionRegle.positionCursor = tabElement.positionCursor;
        _tabFormule.conditionRegle.tabData = tabElement.tabData;
    } else if (idElt == 'expressionLitteral') {
        _tabFormule.expressionLitteral.positionCursor = tabElement.positionCursor;
        _tabFormule.expressionLitteral.tabData = tabElement.tabData;
    } else if (idElt == 'expression') {
        _tabFormule.expression.positionCursor = tabElement.positionCursor;
        _tabFormule.expression.tabData = tabElement.tabData;
    }
}

/**
 * Positionne le focus dans la zone de l'élément littéral
 *
 * @param elementLitteral
 */
function positionneFocus(elementLitteral) {
    var tabElement   = getTabElement(elementLitteral),
        divForm      = getDivForm(),
        divTestFocus = $(divForm).find('div[data-name="divTestFocus"]').get(0),
        str,
        posInStr;

    //Positionner le curseur dans le champ
    str = tabElement.tabData.join(' ');

    $(divTestFocus).html(str);
    posInStr = $(divTestFocus).html().search(_charCursor);

    $(elementLitteral).caret(posInStr);
}

/**
 * Supprime un element dans l'élément actif
 */
function deleteInElement() {
    var hiddenEltActif = getElementHiddenActif(), form = getForm(), elementLit = $(form).find(
        '.' + $(hiddenEltActif).val()).get(0), elementHidden = getElementHidden(
        elementLit), tabElementLit = getTabElement(elementLit), tabElementHidden = getTabElement(elementHidden);

    //Si on est pas en début de chaine
    if (tabElementLit.positionCursor > 0) {
        //Suppression de 1 élément avant le curseur
        tabElementLit.tabData.splice(tabElementLit.positionCursor - 1, 1);
        //Suppression de 1 élément avant le curseur
        tabElementHidden.tabData.splice(tabElementHidden.positionCursor - 1, 1);
        //Mémorisation et réaffichage de la zone littérale
        saveAndDisplayElement(elementLit, tabElementLit);
        //Mémorisation et réaffichage de la zone non littérale
        saveAndDisplayElement(elementHidden, tabElementHidden);
    }
    //Repositionne le focus dans la zone littérale
    positionneFocus(elementLit);
    //Indique que le formulaire a été modifié
    changeElementDispositif(elementHidden);
}

/**
 * Retourne le champ caché en fonction de l'élément actif
 *
 * @param element
 * @returns {*|jQuery}
 */
function getElementHidden(element) {
    var divForm = getDivForm(), divElement = $(divForm).find(
        'div[data-name="div' + element.getAttribute('name') + '"]').get(0), elementHidden = $(divElement).find(
        ":hidden").get(0);

    return elementHidden;
}

/**
 * Ajoute un element dans l'élément actif
 * @param valueToAdd Valeur littérale à ajouter
 * @param codeToAdd Code valeur à ajouter
 */
function addInElement(valueToAdd, codeToAdd) {
    var hiddenEltActif = getElementHiddenActif(), form = getForm(), elementLit = $(form).find(
        '.' + $(hiddenEltActif).val()).get(0), elementHidden = getElementHidden(
        elementLit), tabElementLit = getTabElement(elementLit), tabElementHidden = getTabElement(elementHidden);

    //Ajout de la valeur dans la tableau à la position du curseur
    tabElementLit.tabData.splice(tabElementLit.positionCursor, 0, valueToAdd);
    tabElementHidden.tabData.splice(tabElementHidden.positionCursor, 0, codeToAdd);
    //Mémorisation et réaffichage de la zone littérale
    saveAndDisplayElement(elementLit, tabElementLit);
    //Mémorisation et réaffichage de la zone non littérale
    saveAndDisplayElement(elementHidden, tabElementHidden);
    //Repositionne le focus
    positionneFocus(elementLit);
}

/**
 * Permet de passer en inactif tous les boutons variable
 */
function desactiveAllVariable() {
    var classBtn, divForm = getDivForm(), pnlValeur = $(divForm).find('div[data-name="pnlValeur"]');

    //On repasse tous les boutons à inactif
    var $listBtnVariable = $('.btn-variable');
    for (var y = 0; y < $listBtnVariable.length; y++) {
        classBtn = $listBtnVariable.get(y).getAttribute('class');
        classBtn = classBtn.replace('active', '');
        $listBtnVariable.get(y).setAttribute('class', classBtn);
    }
    //On repasse toutes les div de valeur en caché
    $(pnlValeur).get(0).style.display = 'none';
    var $listDivValeur = $('.panel-valeur');
    for (var i = 0; i < $listDivValeur.length; i++) {
        $listDivValeur.get(i).style.display = 'none';
    }
}

/**
 * Permet de gérer l'ajout d'une variable
 * @param eltButton
 * @param codeVar
 * @param libVar
 * @param avecValeur
 * @param namePnl
 */
function addVariableInElement(eltButton, codeVar, libVar, avecValeur, namePnl) {
    var divForm = getDivForm(), pnlValeur = $(divForm).find('div[data-name="pnlValeur"]'), classBtn;

    //Masque les valeurs affichée et passe en inactif les boutons
    desactiveAllVariable();

    //Ajout de la variable dans l'élément actif
    addInElement(libVar, codeVar);

    //Indication de modification de la page
    changeElementDispositif(divForm);

    if (avecValeur) {
        //Dans le cas des éléments dropDown, le bouton cliqué n'est pas modifié, donc passé à Null
        if (!(eltButton == null)) {
            //On passe le bouton cliqué à actif
            classBtn = eltButton.getAttribute('class');
            classBtn = classBtn + ' active';
            eltButton.setAttribute('class', classBtn);
        }
        //Affichage du panel des valeurs
        $(pnlValeur).get(0).style.display = 'block';

        //Si cette variable permet le choix de valeur, affichage de la div des valeurs
        $(divForm).find('div[data-name="' + namePnl + '"]').get(0).style.display = 'block';
    }
}

/**
 * Permet d'ajouter une valeur de variable
 *
 * @param libVar
 * @param codeVar
 */
function addValeurInElement(libVar, codeVar) {
    //Ajout de la variable dans l'élément actif avec des quotes
    addInElement('"' + libVar + '"', '"' + codeVar + '"');
}

/**
 * fonction appelée sur l'ajout d'un nombre pour l'expression
 */
function addNumberExprInElement() {
    var divForm = getDivForm(), divElement = $(divForm).find('div[data-name="nombreExpr"]').get(0), eltValue = $(
        divElement).find(":input").get(0), valueToAdd;

    //Remplacement de la virgule par le point dans le champ hidden
    valueToAdd = $(eltValue).val();
    valueToAdd = valueToAdd.replace(",", ".");
    //ajout de la valeur dans l'élément actif
    addInElement($(eltValue).val(), valueToAdd);
    //Remise à vide de la zone de saisie
    $(eltValue).val('');
}

/**
 * fonction appelée sur l'ajout d'un nombre pour la condition
 */
function addNumberCondInElement() {
    var divForm = getDivForm(), divElement = $(divForm).find('div[data-name="nombreCond"]').get(0), eltValue = $(
        divElement).find(":input").get(0), valueToAdd;

    valueToAdd = $(eltValue).val();
    valueToAdd = valueToAdd.replace(",", ".");
    //ajout de la valeur dans l'élément actif
    addInElement($(eltValue).val(), valueToAdd);
    //Remise à vide de la zone de saisie
    $(eltValue).val('');
}

/**
 * Permet de mémoriser le nouveau tableau
 * Permet de raffraichir la zone de l'élément avec la valeur
 * @param element
 * @param tabElement
 */
function saveAndDisplayElement(element, tabElement) {
    //Recalcul de la position du curseur
    tabElement.positionCursor = tabElement.tabData.indexOf(_charCursor);

    //Affiche la chaine dans la zone
    dislayElement(element, tabElement);

    //Mémorisation des nouvelles positions
    saveTabElement(element, tabElement);
}

function dislayElement(element, tabElement) {
    var str;

    //Reconstruction de la chaine
    str = tabElement.tabData.join(' ');
    str = str.replace(_charCursor, '');
    //Affichage de la chaine dans l'élément
    if ($(element).get(0).getAttribute('type') == 'hidden') {
        $(element).val(str);
    } else {
        $(element).html(str);
    }
}

/**
 * Permet de masquer les div non visibles à l'affchage
 */
function masqueDiv() {
    var divForm = getDivForm(), divCategorie = $(divForm).find(
        'div[data-name="pnlCategorie"]').get(0), divVariable = $(divForm).find(
        'div[data-name="pnlVariable"]').get(0), divValeur = $(divForm).find(
        'div[data-name="pnlValeur"]').get(0), divTestFocus = $(divForm).find(
        'div[data-name="divTestFocus"]').get(0);

    masqueDivOperateur();
    divCategorie.style.display = 'none';
    divVariable.style.display = 'none';
    divValeur.style.display = 'none';
    divTestFocus.style.display = 'none';
}

/**
 * Permet de masquer les div contenant les opérateurs
 */
function masqueDivOperateur() {
    var divForm = getDivForm(), divOperateurCond = $(divForm).find('div[data-name="pnlOpconditionRegleLitteral"]').get(
        0), divOperateurExpr = $(divForm).find('div[data-name="pnlOpexpressionLitteral"]').get(0);

    divOperateurCond.style.display = 'none';
    divOperateurExpr.style.display = 'none';
}

/**
 * Empeche la saisie d'autre chose que des chiffres
 * Sur le champ "Nombre"
 */
function setEditorNumberValidation() {
    var form = getForm();
    $(form).find('.editor-number').on('keypress', function (event) {
        var keyCode = ('which' in event && event.which > 0) ? event.which : event.keyCode,
            indexKey = _numericTabKeyCode.indexOf(
                keyCode);
        //Si la touche n'est pas un chiffre ou déplacement ou virgule, on arrête l'évènement
        if (indexKey == -1) {
            event.preventDefault();
        }
    })
}

/**
 * Permet d'afficher le panel des variables de la catégorie
 *
 * @param eltButton
 * @param codeCategorie
 */
function displayVariable(eltButton, codeCategorie) {
    var divForm = getDivForm(), divVariableList = $(divForm).find('div[data-name="pnlVar' + codeCategorie + '"]').get(
        0), divVariable = $(divForm).find('div[data-name="pnlVariable"]').get(0), divValeur = $(divForm).find(
        'div[data-name="pnlValeur"]').get(0);

    //On repasse tous les boutons à inactif
    var $listBtnCategorie = $('.btn-categorie');
    for (var y = 0; y < $listBtnCategorie.length; y++) {
        $listBtnCategorie.get(y).setAttribute('class', 'btn-categorie btn btn-default btn-sm');
    }

    //On passe le bouton cliquer à actif
    eltButton.setAttribute('class', 'btn-categorie btn btn-default btn-sm active');

    //On repasse toutes les div de variable en caché
    var $listDivVariable = $('.panel-variable');
    for (var i = 0; i < $listDivVariable.length; i++) {
        $listDivVariable.get(i).style.display = 'none';
    }
    //On repasse toutes les div de valeur en caché
    var $listDivValeur = $('.panel-valeur');
    for (var i = 0; i < $listDivValeur.length; i++) {
        $listDivValeur.get(i).style.display = 'none';
    }
    //On affiche le panel des variables demandé
    divVariable.style.display = 'block';
    divVariableList.style.display = 'block';
    //On masque le panel des valeurs
    divValeur.style.display = 'none';
}

//**********************************************************
//          LISTE DES TRAVAUX DU DISPOSITIF
//**********************************************************

/**
 * Permet de lancer l'appel de la modification d'une règle d'une travaux
 *
 * @param idDispositif
 * @param codeTravaux
 * @param isConsult
 */
function modifFicheRegleTravaux(idDispositif, codeTravaux, isConsult) {

    var options = {};

    options.data = {
        PHPSESSID: phpsessid, idDispositif: idDispositif, codeTravaux: codeTravaux, isConsult: isConsult
    };
    options.success = _successModifFicheRegleTravaux;
    options.error = exceptionMessage;

    //Lancement de la requête ajax
    executeAjaxRequest('/admin/dispositif-travaux-modif', options);
}

/**
 * Permet de changer l'état de la zone éligibilité
 * en fonction de la valeur du bouton radio
 *
 * @param radio
 */
function changeEtatEligibilite(radio) {
    var eltEligibilite = $('#eligibiliteSpecifique').get(0), valueRadio = $(
        radio).val(), isDisabled = (valueRadio == '0');

    if (isDisabled) {
        eltEligibilite.setAttribute('disabled', 'disabled');
    } else {
        eltEligibilite.removeAttribute('disabled');
    }

}

/**
 * Appelée sur le success du chargement de la fiche modif
 * des règles du travaux
 *
 * @param data
 * @private
 */
function _successModifFicheRegleTravaux(data) {
    //Affichage de l'onglet
    chargeTabAvecHtml('travauxLien', data.htmlView);
}

/**
 * Permet d'afficher la liste des travaux du dispositif
 * depuis la fiche règle de travaux
 * @param idDispositif
 * @param isConsult
 * @param codeTravaux Code du travaux dont on revient
 */
function retourListeTravaux(idDispositif, isConsult, codeTravaux) {
    var url = '/admin/dispositif-travaux/' + idDispositif + '/' + isConsult + '/' + codeTravaux + '?PHPSESSID=' + phpsessid+'&mode=liste';
    chargeTabAvecUrl('travauxLien', url);
}

/**
 * Permet de faire le submit du formulaire
 */
function submitFormTravaux(deleteTravaux) {
    var options = {}, form = $('#formTravaux'), url = form.attr('action');

    options.data = form.serializeArray();
    options.data.unshift({name: "PHPSESSID", value: phpsessid});

    if (deleteTravaux == true) {
        options.data.push({name: "deleteTravaux", value: deleteTravaux});
    } else {
        options.data.push({name: "submit", value: true});
    }

    options.error = exceptionMessage;
    options.success = _successChangeSaveTravaux;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Sur le succès du changement/sauvegarde du formulaire
 *
 * @param data
 * @private
 */
function _successChangeSaveTravaux(data) {
    if (data.confirmChange) {
        //Confirmation de la suppression du travaux dans le dispositif
        var titre = "Suppression du type de travaux dans le dispositif",
            message = "Ce type de travaux ne fera plus partie du dispositif et ses règles vont être supprimées." + "Voulez-vous continuer ?";

        showModalConfirm(titre, message, "submitFormTravaux(true)");
    } else if (data.retourListe) {
        //Retour à la liste des travaux
        retourListeTravaux(data.idDispositif, data.isConsult, data.codeTravaux);
    } else {
        //Rechargement de l'onglet
        chargeTabAvecHtml('travauxLien', data.htmlView);
    }
}

/**
 * Fonction d'appel de la suppression de la règle du travaux dans le dispositif
 *
 * @param idRegleTravauxDispositif
 * @param idDispositifTravaux
 */
function deleteRegleTravauxDispositif(idRegleTravauxDispositif, idDispositifTravaux) {
    //Demande de confirmation de la suppression
    var titre = "Suppression d'une règle du type de travaux dans le dispositif",
        message = "Cette action va entraîner la suppression de la formule." + "Voulez-vous continuer ?";

    showModalConfirm(titre, message,
        "_confirmSuppressionRegleTravauxDispositif(" + idRegleTravauxDispositif + "," + idDispositifTravaux + ")");
}

/**
 * Fonction appelée sur la confirmation de la suppression de la règle du travaux dans le dispositif
 *
 * @param idRegleTravauxDispositif
 * @param idDispositifTravaux
 * @private
 */
function _confirmSuppressionRegleTravauxDispositif(idRegleTravauxDispositif, idDispositifTravaux) {
    var options = {};

    options.data = {
        PHPSESSID: phpsessid,
        idRegleTravaux: idRegleTravauxDispositif, idTravauxDispositif: idDispositifTravaux
    };
    options.error = exceptionMessage;
    options.success = _successSupprimeRegleTravauxDispositif;

    executeAjaxRequest('/admin/dispositif-travaux-regle-supprime', options);
}

/**
 * Appelée sur le success de la suppression d'une règle d'un travaux
 * @param data
 * @private
 */
function _successSupprimeRegleTravauxDispositif(data) {
    //Mettre à jour le tableau retourné
    $('#pnlListeTravauxRegle').html(data.htmlView);
}

/**
 * Fonction d'appel de la fiche de modification d'une règle de travaux dans le dispositif
 *
 * @param idRegleTravauxDispositif
 * @param idDispositifTravaux
 */
function modifFicheRegleTravauxDispositif(idRegleTravauxDispositif, idDispositifTravaux) {
    var url = '/admin/dispositif-travaux-regle-modif/' + idRegleTravauxDispositif + '/' + idDispositifTravaux + '?PHPSESSID=' + phpsessid;
    chargeTabAvecUrl('travauxLien', url);
}

/**
 * Fonction d'appel de la fiche d'ajout d'une règle de travaux dans le dispositif
 * @param idDispositifTravaux
 */
function ajoutFicheRegleTravauxDispositif(idDispositifTravaux) {

    var url = '/admin/dispositif-travaux-regle-ajout/' + idDispositifTravaux + '?PHPSESSID=' + phpsessid;
    chargeTabAvecUrl('travauxLien', url);
}

/**
 * Permet de revenir à la fiche du travaux depuis l'éditeur de règle
 *
 * @param idDispositifTravaux
 */
function retourFicheRegleTravaux(idDispositifTravaux) {

    var options = {};

    options.data = {
        PHPSESSID: phpsessid, idDispositifTravaux: idDispositifTravaux, isConsult: 0 //On est en consultation car on a accès à l'éditeur
    };
    options.success = _successModifFicheRegleTravaux;
    options.error = exceptionMessage;

    //Lancement de la requête ajax
    executeAjaxRequest('/admin/dispositif-travaux-modif', options);
}

//**********************************************************
//          HISTORIQUE DU DISPOSITIF
//**********************************************************

/**
 * Permet de faire le submit du formulaire
 */
function submitFormHistorique() {
    var options = {}, form = $('#formHistorique'), url = form.attr('action');

    options.data = form.serializeArray();
    options.data.push({name: "submit", value: true});
    options.error = exceptionMessage;
    options.success = _successChangeSaveHistorique;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/**
 * Sur le succès du changement/sauvegarde du formulaire
 *
 * @param data
 * @private
 */
function _successChangeSaveHistorique(data) {
    //Affichage de l'onglet
    chargeTabAvecHtml('historiqueLien', data.htmlView);
}

//**********************************************************
//          CHANGEMENT D'ONGLET ET MESSAGE
//**********************************************************

var _url;
var _idTab;
var _eltChange;

/**
 * Permet de vérifier qu'aucune modification n'a été faite
 * et d'afficher le message si besoin
 *
 * @param idTab identifiant du lien de l'onglet en cours d'affichage
 * @param url url permettant de charger l'onglet
 */
function afficheOngletDispositif(idTab, url) {
    var form = $(".tab-pane.active").find('form'), eltChange = $(form).find('input[name="existeChangement"]');
    if (eltChange.val() == "true") {
        removeWaiting();
        //Affichage du message
        var titre = "Changement d'onglet",
            message = "Vous allez quitter l'onglet sans enregistrer vos modifications." + "Voulez-vous continuer ?";
        _url = url;
        _idTab = idTab;
        _eltChange = eltChange;
        showModalConfirm(titre, message, "_chargeOnglet()");
    } else {
        addWaiting();
        chargeTabAvecUrl(idTab, url);
    }
}

/**
 * fnction réalisant le changement d'onglet
 * si on passe directement chargeTabAvecUrl avec les param,
 * ca ne fonctionne pas
 * @private
 */
function _chargeOnglet() {
    //affichage de l'onglet demandé
    chargeTabAvecUrl(_idTab, _url);
}

/**
 * Permet de gérer les modifications réalisées sur l'onglet
 *
 * @param element
 */
function changeElementDispositif(element) {
    var form = $(element).parents().find('form'), eltChange = $(form).find('input[name="existeChangement"]');

    $(eltChange).val(true);
}

//**********************************************************
//          EXPORTS DES DISPOSITIFS
//**********************************************************
function exporterDispositif() {
    const urlExport      = $('#exportDispositif').data('url-export'),
          form           = $('#formTableCheckbox'),
          serializedForm = form.serializeArray();

    if (serializedForm.length > 0) {
        form.submit();
    }
}

/**
 * Permet de gérer le fonctionnement de la case selectionner tout
 */
function initSelectDeselectAllCheckbox() {
    $('#tableDisp #select-deselect-tout').change(function () {
            if ($(this).prop('checked')) {
                $('#tableDisp td input[type="checkbox"]').prop('checked', true);
                $('#exporterDispos').show();
            } else {
                $('#tableDisp td input[type="checkbox"]').prop('checked', false);
                $('#exporterDispos').hide();
            }
        }
    );

    $('#tableDisp td input[type="checkbox"]').change(function () {
        if (!$(this).prop('checked')) {
            if ($('#tableDisp td input[type="checkbox"]:checked').length === 0){
                $('#tableDisp #select-deselect-tout').prop('indeterminate', false);
                $('#tableDisp #select-deselect-tout').prop('checked', false);
                $('#exporterDispos').hide();
            }else {
                $('#tableDisp #select-deselect-tout').prop('checked', false);
                $('#tableDisp #select-deselect-tout').prop('indeterminate', true);
                $('#exporterDispos').show();
            }
        } else if ($('#tableDisp td input[type="checkbox"]:not(:checked)').length === 0) {
            $('#tableDisp #select-deselect-tout').prop("indeterminate", false);
            $('#tableDisp #select-deselect-tout').prop('checked', true);
            $('#exporterDispos').show();

        }else{
            $('#tableDisp #select-deselect-tout').prop('indeterminate', true);
            $('#tableDisp #select-deselect-tout').prop('checked', false);
            $('#exporterDispos').show();
        }
    });
}


//**********************************************************
//          TESTER UN DISPOSITIF
//**********************************************************
/**
 * Appelée sur le bouton "Tester ce dispositif"
 */
function testerDispositif() {
    //ouverture d'un simulation avec le dispostif en test
    var idDispositif = $("input[type=hidden][name='dispositif[id]']").val();
    ouvreSimulationDansNouvelOnglet('/admin/simulation-test-le-dispositif?PHPSESSID=' + phpsessid+'&idDispositif='+idDispositif);
}

function modaleSupprimerTravauxDispositif(idDispositif,codeTravaux,phpsessid) {
    //Confirmation de la suppression du travaux dans le dispositif
    var titre   = "Suppression du type de travaux dans le dispositif",
        message = "Ce type de travaux ne fera plus partie du dispositif et ses règles vont être supprimées." + " Voulez-vous continuer ?";

    showModalConfirm(titre, message, "supprimerTravauxDispositif("+idDispositif+",'"+codeTravaux+"','"+phpsessid+"')");

}

function deselectTravaux(codeTravaux){
    $( "input[value='"+codeTravaux+"']" ).prop('checked',false);
}

/**
 * Permet de supprimer un travaux pour un dispositif
 */
function supprimerTravauxDispositif(idDispositif,codeTravaux,phpsessid) {
    var options = {};
    deselectTravaux(codeTravaux);

    options.data = {
        PHPSESSID: phpsessid, idDispositif: idDispositif, codeTravaux: codeTravaux
    };
    options.error = exceptionMessage;


    //Lancement de la requête ajax
    executeAjaxRequest('/admin/dispositif-travaux-supprime', options);
}

/**
 * Permet d'ajouter les travaux à un dispositif
 */
function ajouterTravauxDispositif(idDispositif,listeCodeTravaux,phpsessid) {
    var options = {};
    addWaiting();
    options.data = {
        PHPSESSID: phpsessid, idDispositif: idDispositif, listeCodeTravaux: listeCodeTravaux
    };
    options.error = exceptionMessage;
    options.success = _successAjoutDispositifTravaux;

    //Lancement de la requête ajax
    executeAjaxRequest('/admin/dispositif-travaux-ajout', options);
}


function _successAjoutDispositifTravaux(retour){
    /*redirection*/
    removeWaiting();
    afficheOngletDispositif('travauxLien',retour.url);
}


