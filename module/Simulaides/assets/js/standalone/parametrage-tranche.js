/*
 Fichier js de l'écran de paramétrage des groupes et des tranches de revenu
 */

/**************************************************************
 * ************ GESTION DES GROUPES DE TRANCHE *****************
 **************************************************************/

/**
 * Suppression d'un groupe de tranche de revenu
 *
 * @param codeGroupe Code du groupe à supprimer
 */
function deleteGroupeTranche(codeGroupe) {
    //Demande de confirmation de la suppression
    var titre = "Suppression d'un groupe de tranche", message = "Cette action va entraîner la suppression du groupe et de ses tranches de revenu. " + "Assurez-vous qu'elles ne sont pas utilisées dans les règles d'un dispositif. " + "Voulez-vous continuer ?";

    showModalConfirm(titre, message, "_confirmDeleteGroupeTranche('" + codeGroupe + "')");
}

/**
 * Suppression d'un groupe de tranche de revenu
 *
 * @param codeGroupe Code du groupe à supprimer
 * @private
 */
function _confirmDeleteGroupeTranche(codeGroupe) {
    var options     = {};
    options.data    = {codeGroupe: codeGroupe, PHPSESSID: phpsessid};
    options.error   = exceptionMessage;
    options.success = _successDeleteGroupeTranche;

    executeAjaxRequest('/admin/groupe-tranche-supp', options);
}

/**
 * Permet de rafraichir l'onglet des tranches de revenu
 * Après le succès de la suppression
 *
 * @param data
 * @private
 */
function _successDeleteGroupeTranche(data) {
    //La suppression est effective, rechargement de l'onglet
    afficheTab('trancheLien');
}
/**
 * Permet d'afficher l'onglet des groupes de tranche
 */
function afficheOngletTrancheGroupe() {
    afficheTab('trancheLien');
}

/**
 * Appelée sur la modification d'un groupe de tranche
 * @param codeGroupe
 */
function modifFicheGroupeTranche(codeGroupe) {
    var options     = {};
    options.data    = {codeGroupe: codeGroupe, PHPSESSID: phpsessid};
    options.error   = exceptionMessage;
    options.success = _afficheModaleGroupeTranche;

    executeAjaxRequest('/admin/groupe-tranche-fiche-modif', options);
}

/**
 * Appelée sur l'ajout d'un groupe de tranche
 */
function ajoutFicheGroupeTranche() {
    var options     = {data: {PHPSESSID: phpsessid}};
    options.error   = exceptionMessage;
    options.success = _afficheModaleGroupeTranche;

    executeAjaxRequest('/admin/groupe-tranche-fiche-ajout', options);
}

/**
 * Succès de la demande d'affichage du formulaire de groupe de tranche
 * Affichage de la modale avec le formulaire retourné
 * @param data
 * @private
 */
function _afficheModaleGroupeTranche(data) {
    var titre = 'Définir un groupe de tranches de revenu';
    showModalForm(data.htmlView, titre);
}

/**
 * Permet de lancer la requête ajax pour l'affichage dans
 * l'onglet de la liste des tranches de revenu du groupe
 *
 * @param codeGroupe
 */
function afficheListeTrancheRevenu(codeGroupe) {
    var url = '/admin/tranche-liste/' + codeGroupe + '?PHPSESSID=' + phpsessid;
    chargeTabAvecUrl('trancheLien', url);
}

/***************************************************************
 * ************ GESTION DES TRANCHES DE REVENU *****************
 ***************************************************************/

/**
 * Appelée sur l'ajout d'une tranche de revenu
 */
function ajoutFicheTrancheRevenu(codeGroupe) {
    var options     = {};
    options.data    = {codeGroupe: codeGroupe, PHPSESSID: phpsessid};
    options.error   = exceptionMessage;
    options.success = _afficheModaleTrancheRevenu;

    executeAjaxRequest('/admin/tranche-revenu-fiche-ajout', options);
}

/**
 * Appelée sur la modification d'une tranche de groupe
 * @param codeTranche
 */
function modifFicheTrancheRevenu(codeTranche) {
    var options     = {};
    options.data    = {codeTranche: codeTranche, PHPSESSID: phpsessid};
    options.error   = exceptionMessage;
    options.success = _afficheModaleTrancheRevenu;

    executeAjaxRequest('/admin/tranche-revenu-fiche-modif', options);
}

/**
 * Succès de la demande d'affichage du formulaire de tranche de revenu
 * Affichage de la modale avec le formulaire retourné
 * @param data
 * @private
 */
function _afficheModaleTrancheRevenu(data) {
    var titre = 'Définir une tranche de revenu';
    showModalForm(data.htmlView, titre);
}

/**
 * Suppression d'une tranche de revenu
 *
 * @param codeTranche Code de la tranche à supprimer
 */
function deleteTrancheRevenu(codeTranche) {
    //Demande de confirmation de la suppression
    var titre = "Suppression d'une tranche de revenu", message = "Cette action va entraîner la suppression de la tranche. " + "Assurez-vous qu'elle n'est pas utilisée dans les règles d'un dispositif. " + "Voulez-vous continuer ?";
    showModalConfirm(titre, message, "_confirmDeleteTranche('" + codeTranche + "')");
}

/**
 * Suppression d'un groupe de tranche de revenu
 *
 * @param codeTranche Code de la tranche à supprimer
 * @private
 */
function _confirmDeleteTranche(codeTranche) {
    var options     = {};
    options.data    = {codeTranche: codeTranche, PHPSESSID: phpsessid};
    options.error   = exceptionMessage;
    options.success = _successDeleteTranche;
    executeAjaxRequest('/admin/tranche-revenu-supp', options);
}

/**
 * Permet de rafraichir l'onglet des tranches de revenu
 * Après le succès de la suppression
 *
 * @param data
 * @private
 */
function _successDeleteTranche(data) {
    //La suppression est effective, rechargement de l'onglet
    afficheListeTrancheRevenu(data['codeGroupe']);
}