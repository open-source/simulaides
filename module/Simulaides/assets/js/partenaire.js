/*
 Fichier js de l'écran de paramétrage des partenaires
 */

/**************************************************************
 * ************ GESTION DES PARTENAIRES *****************
 **************************************************************/

/**
 * Suppression d'un partenaire
 *
 * @param idPartenaire Id du partenaire à supprimer
 */
function deletePartenaire(idPartenaire) {
    //Demande de confirmation de la suppression
    var titre = "Suppression d'un partenaire", message = "Cette action va entraîner la suppression du partenaire. " + "Voulez-vous continuer ?";

    showModalConfirm(titre, message, "_confirmDeletePartenaire('" + idPartenaire + "')");
}

/**
 * Suppression d'un partenaire
 *
 * @param idPartenaire id du partenaire à supprimer
 * @private
 */
function _confirmDeletePartenaire(idPartenaire) {
    var options     = {};
    options.data    = {PHPSESSID: phpsessid};
    options.error   = exceptionMessage;
    options.success = _successDeletePartenaire;

    executeAjaxRequest('/admin/partenaire-supp/'+idPartenaire, options);
}

/**
 * Permet de rafraichir
 * Après le succès de la suppression
 *
 * @param data
 * @private
 */
function _successDeletePartenaire(data) {
    $("#divPartenaireTab").html(data.htmlView);
}

/**
 * Appelée sur la modification d'un partenaire
 * @param idPartenaire
 */
function modifPartenaire(idPartenaire) {
    var options     = {};
    options.data    = {PHPSESSID: phpsessid};
    options.error   = exceptionMessage;
    options.success = _afficheModalePartenaire;
    options.type = "GET";

    executeAjaxRequest('/admin/partenaire-modif/'+idPartenaire, options);
}

/**
 * Appelée sur l'ajout d'un partenaire
 */
function ajoutPartenaire() {
    var options     = {data: {PHPSESSID: phpsessid}};
    options.error   = exceptionMessage;
    options.success = _afficheModalePartenaire;
    options.type = "GET";
    executeAjaxRequest('/admin/partenaire-ajout', options);
}

/**
 * Succès de la demande d'affichage du formulaire de partenaire
 * Affichage de la modale avec le formulaire retourné
 * @param data
 * @private
 */
function _afficheModalePartenaire(data) {
    var titre = 'Définir un partenaire';
    showModalForm(data.htmlView, titre);

    //$('#modalForm form input[name="submit"]').off('click');
    //$('#modalForm form input[name="submit"]').attr('onclick', "");
    //$('#modalForm form input[name="submit"]').click(_submitModalePartenaire);
}

function _submitModalePartenaire(){
    var form = $('#modalForm').find('form'),
        options = {},
        url = form.attr('action');

    options.data = form.serializeArray();
    options.data.push({name:"PHPSESSID", value:phpsessid});
    options.data.push({name: "submit", value: true});
    options.error = exceptionMessage;
    options.success = _responseModalePartenaire;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

function _responseModalePartenaire(data){
    if(data.formIsValid){
        //Fermeture de la modale
        closeModalForm();
        //Affichage et raffraichissement de l'onglet
        $("#divPartenaireTab").html(data.htmlView);
    }else {
        refreshModalForm(data);
    }
}