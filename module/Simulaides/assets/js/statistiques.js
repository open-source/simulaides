/*
 Fichier js de l'écran de paramétrage des stats
 */

$(document).ready(function(){


    $("#statEPCI-regionCode").chosen({no_results_text: "Aucun résultat", width: '100%'});
    $("#statEPCI-departementCode").chosen({no_results_text: "Aucun résultat", width: '100%'});

    $("#statEPCI-regionCode").chosen().change(function(){
        let region = $(this).val();
        _getDepartements(region);
    });

    function _getDepartements(region){
        let url = $("#formStatistique").attr('action');

        url = url+"/departements/"+region;

        let options = {data: []};
        options.data.push({name:"PHPSESSID", value:phpsessid});
        options.data.push({name: "submit", value: true});
        options.error = exceptionMessage;
        options.success = _responseGetDepartements;

        //Lancement de la requête ajax
        executeAjaxRequest(url, options);
    }

    function _responseGetDepartements(data){
        var statDpt =  $("#statEPCI-departementCode");
        statDpt.empty(); // remove old options
        $.each(data, function(key,value) {
            statDpt.append($("<option></option>")
                .attr("value", value.code).text(value.nom));
        });

        //Affichage et raffraichissement de l'onglet
        $("#statEPCI-departementCode").chosen().trigger("chosen:updated");

        let departement = $("#statEPCI-departementCode").val();
        _getStatsEPCI(departement);
    }

    $("#statEPCI-departementCode").chosen().change(function(){
        let departement = $(this).val();
        _getStatsEPCI(departement);
    });

    function _getStatsEPCI(departement){
        let url = $("#formStatistique").attr('action');

        url = url+"/epci/"+departement;

        var form = $('#formStatistique'),
            options = {};

        options.data = form.serializeArray();
        options.data.push({name:"PHPSESSID", value:phpsessid});
        options.data.push({name: "submit", value: true});
        options.error = exceptionMessage;
        options.success = _responsegetStatsEPCI;

        //Lancement de la requête ajax
        executeAjaxRequest(url, options);
    }

    function _responsegetStatsEPCI(data){
        $("#divTabEPCIStat").html(data.htmlView);
    }
});
