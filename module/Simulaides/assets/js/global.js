$('iframe').load(function() {
    this.style.height = this.contentWindow.document.body.offsetHeight + 'px';
});

/**
 * Ajoute un calque d'attente et une image de chargement à la page courante
 *
 * @param opacity
 *            opacité du calque (0.5 par défaut)
 * @param idCalque
 *            identifiant du calque (waiting par défaut)
 */
function addWaiting(opacity, idCalque) {
    opacity = (opacity == null || opacity == undefined) ? 0.5 : opacity;
    idCalque = (idCalque == null || idCalque == undefined) ? "waiting"
        : idCalque;

    if ($('#' + idCalque).length == 0) {
        $("body").prepend('<div id="' + idCalque + '" class="waiting all-waiting"></div>');
        $("#" + idCalque).css('opacity', opacity);
    }

    return false;
}

/**
 * Supprime le calque d'attente et la popup créés par addWaiting()
 *
 * @param idCalque
 *            identifiant du calque (waiting par défaut)
 */
function removeWaiting(idCalque) {
    idCalque = (idCalque == null || idCalque == undefined) ? "waiting"
        : idCalque;
    $('#' + idCalque).remove();
}

/********************************************************
 * FENETRE MODALE
 ********************************************************/

/**
 * Affiche une modale de confirmation
 *
 * @param titre Titre de la modale
 * @param message Message de confirmation
 * @param jsCallBack Js à exécuter sur la validation
 * @returns {boolean}
 */
function showModalConfirm(titre, message, jsCallBack) {

    //Js exécuté sur le bouton OK (fermeture modale + callBack)
    var jsOk = "javascript:$('#dataConfirmModal').modal('hide');" + jsCallBack + ";";

    //Construction de la modale
    if (!$('#dataConfirmModal').length) {
        $('body').append(_getBodyModalConfirm());
    }
    $('#modal-confirm-title').text(titre);
    $('#dataConfirmModal').find('.modal-body').text(message);
    $('#dataConfirmOK').attr('href', jsOk);
    $('#dataConfirmModal').modal({show: true});

    return false;
}

/**
 * Affiche une modale de confirmation
 *
 * @param titre Titre de la modale
 * @param message Message de confirmation
 * @param callBack Callback à exécuter sur la validation
 * @param target Element sur lequel à été déclenché la modale, passé au callback
 * @returns {boolean}
 */
function showModalConfirmStdCallback(titre, message, callBack, target) {
    let modal = $('#dataConfirmStdModal');
    //Construction de la modale
    if (!modal.length) {
        $('body').append(_getBodyModalStdConfirm());
        modal = $('#dataConfirmStdModal');
    }
    $('#modal-confirm-title').text(titre);
    modal.find('.modal-body').text(message);
    modal.modal({show: true});
    $('#dataConfirmStdOK').on('click', function () {
        modal.modal('hide');
        callBack(target);
    });

    return false;
}

/**
 * Affiche une modale d'alert
 *
 * @param titre Titre de la modale
 * @param message Message de confirmation
 * @param htmlContent Le message
 * @returns {boolean}
 */
function showModalAlert(titre, message, htmlContent) {
    htmlContent = htmlContent || false;
    //Construction de la modale
    if (!$('#dataAlertModal').length) {
        $('body').append(_getBodyModalAlert());
    }
    $('#modal-alert-title').text(titre);
    if(htmlContent){
        $('#dataAlertModal').find('.modal-body').html(message);
    }else {
        $('#dataAlertModal').find('.modal-body').text(message);
    }
    $('#dataAlertModal').modal({show: true});

    return false;
}


/**
 * Affiche une modale d'alert
 *
 * @param titre Titre de la modale
 * @param message Message de confirmation
 * @param htmlContent Le message
 * @param isComparatif bool Indique si on est en mode comparatif
 * @returns {boolean}
 */
function showModalMessageElegibilite(titre, message, htmlContent, isComparatif) {

    htmlContent = htmlContent || false;
    //Js exécuté sur le bouton OK (fermeture modale + callBack)
    var jsOk = "javascript:$('#dataModalMessageElegibilite').modal('hide'); ";
    if (isComparatif) {
        jsOk += "submitFormSimulation('frmCaracteristique');";
    } else {
        jsOk += "submitFromMaSitutation();";
    }

    var classfoyer = '';
    if(typeFoyer=='TRES_MODESTE'){
        classfoyer = 'tres_modeste_modale';
        imgSource = "MPR_Bleu.png";
    }else if(typeFoyer=='MODESTE'){
        classfoyer = 'modeste_modale';
        imgSource = "MPR_Jaune.png";
    }else if(typeFoyer=='INTERMEDIAIRE'){
        classfoyer = 'intermediaire_modale';
        imgSource = "MPR_Mauve.png";
    }else if(typeFoyer=='SUPERIEUR'){
        classfoyer = 'superieur_modale';
        imgSource = "MPR_Rose.png";
    }


    //Construction de la modale
    if (!$('#dataModalMessageElegibilite').length) {
        $('body').append(_getBodyModalMessageElegibilite());
    }
    $('#modal-alert-title').text(titre);
    if(htmlContent){
        message = '<img src="/images/'+imgSource+'" style="float:left"><div class="body-mpr">' +  message +'</div>';
        $('#dataModalMessageElegibilite').find('.modal-body').html(message);
        tdFond = $('#dataModalMessageElegibilite').find('.td-fond');
        tdFond.removeClass();
        tdFond.addClass('td-fond');
        tdFond.addClass(classfoyer);
    }else {
        $('#dataModalMessageElegibilite').find('.modal-body').text(message);
    }
    $('#dataMessageElegibiliteOK').attr('href', jsOk);
    $('#dataModalMessageElegibilite').modal({show: true});

    return false;
}

/**
 * Appel Ajax
 *
 * @param url
 *            URL à appeler
 * @param options
 *            Options de l'appel AJAX
 *
 */
function executeAjaxRequest(url, options) {
    return $.ajax({
        type: (options.type || 'POST'),
        url: url,
        data: (options.data || ''),
        dataType: (options.dataType || 'json'),
        complete: options.complete,
        error: options.error,
        success: options.success
    });
}

/**
 * * Permet d'afficher un formulaire dans une modale
 *
 * @param viewModel ViewModel retourné par le controller
 * @param titre Titre de la fenêtre
 */
function showModalForm(viewModel, titre) {

    if (!$('#modalForm').length) {
        $('body').append(_getBodyModalForm());
    }
    //Remplissage de la modale avec le formulaire
    $('#modalForm').find('.modal-body').html(viewModel);
    $('#modal-form-title').text(titre);
    $('#modalForm').modal({show: true});
}

/**
 * Permet de raffraichir le contenu de la modale
 *
 * @param data
 */
function refreshModalForm(data) {
    if (!$('#modalForm').length) {
        $('body').append(_getBodyModalForm());
    }
    //Remplissage de la modale avec le formulaire
    $('#modalForm').find('.modal-body').html(data.htmlView);

}

/*
 * Permet de gérer les exceptions retournées
 */
function exceptionMessage(xhr, status, error) {
    removeWaiting();
    if (xhr.status == 405) {
        //Utilisateur non autorisé, redirection vers l'authentification
        window.location = xhr.responseText;
    } else if(status !== "cancelRequest"){
        alert(xhr.responseText);
    }
}

/**
 * Retourne le corps de la modale de confirmation
 *
 * @returns {string}
 * @private
 */
function _getBodyModalConfirm() {

    var sBody = '<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="modal-confirm-title" aria-hidden="true">' +
        '<div class="modal-dialog">' +
        '   <div class="modal-content">' +
        '       <div class="modal-header">' +
        '           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '           <h4 id="modal-confirm-title"></h4>' +
        '       </div>' +
        '       <div class="modal-body" align="justify">' +
        '       </div>' +
        '       <div class="modal-footer">' +
        '           <button class="btn" data-dismiss="modal" aria-hidden="true">Non</button>' +
        '           <a class="btn btn-danger" id="dataConfirmOK">Oui</a>' +
        '       </div>' +
        '   </div>' +
        '</div></div>';

    return sBody;
}

/**
 * Retourne le corps de la modale de confirmation
 *
 * @returns {string}
 * @private
 */
function _getBodyModalStdConfirm() {

    return '<div id="dataConfirmStdModal" class="modal" role="dialog" aria-labelledby="modal-confirm-title" aria-hidden="true">' +
        '<div class="modal-dialog">' +
        '   <div class="modal-content">' +
        '       <div class="modal-header">' +
        '           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '           <h4 id="modal-confirm-title"></h4>' +
        '       </div>' +
        '       <div class="modal-body" align="justify">' +
        '       </div>' +
        '       <div class="modal-footer">' +
        '           <button class="btn" data-dismiss="modal" aria-hidden="true">Non</button>' +
        '           <a class="btn btn-danger" id="dataConfirmStdOK">Oui</a>' +
        '       </div>' +
        '   </div>' +
        '</div></div>';
}

/**
 * Retourne le corps de la modale d'alert
 *
 * @returns {string}
 * @private
 */
function _getBodyModalAlert() {

    var sBody = '<div id="dataAlertModal" class="modal" role="dialog" aria-labelledby="modal-alert-title" aria-hidden="true">' +
        '<div class="modal-dialog">' +
        '   <div class="modal-content">' +
        '       <div class="modal-header">' +
        '           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '           <h4 id="modal-alert-title"></h4>' +
        '       </div>' +
        '       <div class="modal-body" align="justify">' +
        '       </div>' +
        '       <div class="modal-footer">' +
        '           <button class="btn" data-dismiss="modal" aria-hidden="true">OK</button>' +
        '       </div>' +
        '   </div>' +
        '</div></div>';

    return sBody;
}

/**
 * Retourne le corps de la modale d'alert
 *
 * @returns {string}
 * @private
 */
function _getBodyModalMessageElegibilite() {

    var sBody = '<div id="dataModalMessageElegibilite" class="modal" role="dialog" aria-labelledby="modal-confirm-title" aria-hidden="true">' +
        '<div class="modal-dialog position_modal">' +
        '   <div class="modal-content">' +
        '       <table style="width: 100%"><tr><td class="td-fond"></td><td>' +
        '       <div style="display: block">' +
        '           <div class="modal-header">' +
        '              <h4 id="modal-alert-title"></h4>' +
        '           </div>' +
        '           <div class="modal-body" style="min-height: 110px;">' +
        '           </div>' +
        '           <div class="modal-footer" style="text-align: center">' +
        '               <a class="btn btn-danger" id="dataMessageElegibiliteOK">Continuer la simulation</a>' +
        '           </div>' +
        '       </div></td></tr></table>' +
        '   </div>' +
        '</div></div>';

    return sBody;
}

function submitFromMaSitutation() {
    var form = $('#frmCaracteristique');
    form.submit();
}
/*
 * Permet d'inséerer les données cdp dans la popup
 */
function insertIntoCdpPopup(idDispositif,idTravaux){
    $('#coup_de_pouce_content').html('');
    var json = $('#coup_de_pouce_content_JSON').val();
    var obj = JSON.parse(json);
    var popuopContent=obj[idDispositif];
    popuopContent = popuopContent[idTravaux];
    $('#coup_de_pouce_content').append(popuopContent);

    if($('input[name=dispositifCdp]:checked').val() != undefined){
        $('#enregistrerMonChoix').prop('disabled',false);
    }else{
        $('#enregistrerMonChoix').prop('disabled',true);
    }

}

function afficheBoutonEnregistrerMonChoix(){
    $('#enregistrerMonChoix').prop('disabled',false);
}
/*
 * Permet d'envoyer la popup au controller
 */
function sendPopupCdp(){
    var form = $('#popupCdp'),
        options = {},
        url = form.attr('action');
    addWaiting();
    options.data = form.serializeArray();
    options.data.push({name:"PHPSESSID", value:phpsessid});
    options.data.push({name: "submit", value: true});
    options.error = exceptionMessage;
    options.dataType ="html";
    options.success =   function(data){
        location.reload();
    };
    //Lancement de la requête ajax
    executeAjaxRequest(url, options);

}

/**
 * Retourne le corps de la modale de formulaire
 *
 * @returns {string}
 * @private
 */
function _getBodyModalForm() {

    var sBody = '<div class="modal fade" id="modalForm">' +
        '<div class="modal-dialog">' +
        '   <div class="modal-content">' +
        '       <div class="modal-header">' +
        '           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '           <h4 class="modal-title" id="modal-form-title"></h4>' +
        '       </div>' +
        '       <div class="modal-body">' +
        '       </div>' +
        '   </div>' +
        '</div></div>';

    return sBody;
}

/**
 * Permet de fermer la modale de formulaire
 */
function closeModalForm() {
    $('#modalForm').modal('hide');
}

/**
 * Permet de lancer le submit du formulaire en ajax
 */
function submitModalForm() {
    var form = $('#modalForm').find('form'),
        options = {},
        url = form.attr('action');

    options.data = form.serializeArray();
    options.data.push({name:"PHPSESSID", value:phpsessid});
    options.data.push({name: "submit", value: true});
    options.error = exceptionMessage;
    options.success = refreshModalForm;

    //Lancement de la requête ajax
    executeAjaxRequest(url, options);
}

/********************************************************
 * EDITEUR WISIWIG
 ********************************************************/
/**
 * Permet d'afficher l'éditeur Wisiwig summernote
 */
function afficheEditeurWisiwig(idElt) {
    $('#' + idElt).summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline','color']],
            ['fontsize', ['fontsize']],
            ['alignment', ['ul', 'ol', 'paragraph']],
            ['insert', ['link']],
            ['view', ['fullscreen']]
        ],
        height: 350,
        lang: 'fr-FR'
    });
}

/**
 * Retourne la données saisie dans l'éditeur
 *
 * @returns {*|jQuery}
 */
function getEditeurWisiwigHTML(idElt) {
    return $('#' + idElt).code();
}

function deleteEditeurWisiwig(idElt) {
    $('#' + idElt).destroy();
}
/**
 * Permet de mettre à jour la zone de texte (non visible)
 * avec le texte siasie dans la <div>
 */
function updateEditeurWisiwigValeurSaisie(idElt) {
    $('#' + idElt).html(getEditeurWisiwigHTML(idElt));
    $('#' + idElt).val(getEditeurWisiwigHTML(idElt));
}

/********************************************************
 * DATE PICKER
 ********************************************************/
function ajouteDatePicker() {
    $(".date-picker").datepicker(
        {
            showOn: "button",
            buttonText: "<span class='fa fa-calendar btn-calendar'></span>"
        }
    );
}

/**
 * Permet de rendre inaccessible les datePicker
 */
function disabledDatePicker() {
    $(".date-picker").datepicker('disable');
}

/********************************************************
 * LISTE CHOSEN
 ********************************************************/
function initSelectChosen() {
    var chosenObject = $('.chosen-select');
    chosenObject.chosen({width: "100%",no_results_text: "Aucun résultat"});
}

/********************************************************
 * SIMULATION DANS UN NOUVEL ONGLET
 ********************************************************/
var _winSimul = null;

/**
 * Permet l'ouverture de la simulation dans un nouvel onglet
 * @param url
 */
function ouvreSimulationDansNouvelOnglet(url) {
    var msg;
    if (!_verifieOngletAvantOuverture()) {
        //Un onglet est déjà ouvert, demande de confirmation
        msg = "Une simulation est déjà chargée, le chargement de celle-ci remplacera l'actuelle.";
        showModalConfirm('Simulation', msg, '_ouvreOngletSimulation("' + url + '")');
    } else {
        _ouvreOngletSimulation(url);
    }
}

/**
 * Réalise l'ouverture de l'onglet
 * @param url
 * @private
 */
function _ouvreOngletSimulation(url) {
    _winSimul = window.open(url, 'simul');
}

/**
 * Vérifie qu'un onglet de simulation n'est pas déjà ouvert
 *
 * @returns {boolean}
 * @private
 */
function _verifieOngletAvantOuverture() {
    return !(_winSimul && _winSimul.open && !_winSimul.closed);
}

/******************************************************/
/* Mentions légales
 /******************************************************/

function displayMentionsLegales() {
    var div = $('#divMention').get(0);
    if (div.style.display == 'block') {
        div.style.display = 'none'
    } else {
        div.style.display = 'block';
    }
}

function displayMentionsLegales() {
    var div = $('#divMention').get(0);
    if (div.style.display == 'block') {
        div.style.display = 'none'
    } else {
        div.style.display = 'block';
    }
}

function ouverturefermetureblock(id) {
    if ($("#fleche"+id).hasClass("is-open")) {
        $(".block"+id).hide();
        $("#fleche"+id).removeClass("is-open");
        $("#fleche"+id).addClass("is-closed");

    } else {
        $(".block"+id).removeClass('hidden');
        $(".block"+id).show();
        $("#fleche"+id).removeClass("is-closed");
        $("#fleche"+id).addClass("is-open");

    }
}

function ouverturefermeturedesc(id1, id2) {
    if ($("#fleche-" + id1 + "-" + "id2").hasClass("is-open")) {
        $(".desc" + id).hide();
        $("#fleche-" + id1 + "-" + "id2").removeClass("is-open");
        $("#fleche-" + id1 + "-" + "id2").addClass("is-closed");

    } else {
        $(".desc" + id).show();
        $(".desc" + id).removeClass('hidden');
        $("#fleche-" + id1 + "-" + "id2").removeClass("is-closed");
        $("#fleche-" + id1 + "-" + "id2").addClass("is-open");

    }
}


function returnEtape(url){
    window.open(url,'_self')
    return false;
}

/********************************************************
 * Impression d'une page
 ********************************************************/
$(window).on('load', function(){
    // Permettre l'utilisation du CSS Bootstrap au print
    $('link[href="/css/bootstrap.css"]').attr('media','screen, print');
    $('link[href="/css.css"]').attr('media','screen, print');
    $('link[type="text/css"]').filter(function() {
        return this.href.match(/^(.*)_print.css$/);
    }).attr('media', 'print');
})

function printFrame() {
    window.print()
}


// Exemple de fonction
function postScrollTopMessage(){
    if(window.parent) {
        // à utiliser tel quel. Ne pas changer le message "scrollTop".
        // Conserver le domaine sur "*" afin de pouvoir l'utiliser en préprod et prod. Un contrôle de la source sera fait côté parent
        // Conserver le domaine sur "*" afin de pouvoir l'utiliser en préprod et prod. Un contrôle de la source sera fait côté parent
        // Si simulaide fait lui même usage d'iframe et si l'appel est fait depuis un niveau de profondeur plus important, il faudra changer pour window.parent.parent....
        window.parent.postMessage('scrollTop', "*");
    }
}



$(window).bind('keydown', function(event) {
        var key = undefined;
        var possible = [event.key, event.keyIdentifier, event.keyCode, event.which];

        while (key === undefined && possible.length > 0) {
            key = possible.pop();
        }

        if (key && key == '80' && (event.ctrlKey || event.metaKey) && !(event.altKey)) {
            printFrame();
            return false;
        }
        return true;
    }
);
