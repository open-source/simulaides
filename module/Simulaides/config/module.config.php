<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

/**
 *  Adding environment management for Doctrine configuration
 */

(isset($_ENV['APP_ENV'])) 
    ? define('DOCTRINE_CONFIG_FILE', $_ENV['APP_ENV'] . '/doctrine.config.php') 
    : define('DOCTRINE_CONFIG_FILE', 'doctrine.config.php');

return [
    'controllers'           => include 'controllers.config.php',
    'service_manager'       => include 'services.config.php',
    'view_helpers'          => include 'viewhelpers.config.php',
    'router'                => include 'routes.config.php',
    'assetic_configuration' => include 'assets.config.php',
    'form_elements'         => include 'form_elements.config.php',
    'session'               => include 'session.config.php',
    'translator'            => [
        'locale'                    => 'fr_FR',
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ],
        ],
    ],
    'view_manager'          => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'layout'                   => 'layout',
        'layout/layout_back'       => 'layout/back',
        'layout/layout_accueil'       => 'layout/accueil',
        'not_autorised'            => 'error/403',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'version'                  => 'version',
        'template_map'             => [
            'layout'          => __DIR__ . '/../view/layout.phtml',
            'layout/accueil'     => __DIR__ . '/../view/layout_accueil.phtml',
            'layout/back'     => __DIR__ . '/../view/layout_back.phtml',
            'error/404'       => __DIR__ . '/../view/error/404.phtml',
            'error/403'       => __DIR__ . '/../view/error/403.phtml',
            'error/index'     => __DIR__ . '/../view/error/index.phtml',
            'menu/conseiller' => __DIR__ . '/../view/simulaides/conseiller_menu.phtml',
            'style/color'     => __DIR__ . '/../view/style-color.phtml',
            'version'         => __DIR__ . '/../view/version.phtml',
        ],
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],
        'strategies'               => [
            'ViewJsonStrategy',
        ],
    ],
    // Placeholder for console routes
    'console'               => [
        'router' => [
            'routes' => [],
        ],
    ],
    'doctrine'              => include DOCTRINE_CONFIG_FILE,
    'data-fixture'          => [
        'Simulaides_fixture' => __DIR__ . '/../src/Simulaides/Fixture'
    ]

];
