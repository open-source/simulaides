<?php

return [
    'invokables' => [
        'Simulaides\Controller\Front\Accueil'           => 'Simulaides\Controller\Front\AccueilController',
        'Simulaides\Controller\Front\Projet'            => 'Simulaides\Controller\Front\ProjetController',
        'Simulaides\Controller\Front\Caracteristique'   => 'Simulaides\Controller\Front\CaracteristiqueController',
        'Simulaides\Controller\Front\OffresCee'         => 'Simulaides\Controller\Front\OffresCeeController',
        'Simulaides\Controller\Front\Travaux'           => 'Simulaides\Controller\Front\TravauxController',
        'Simulaides\Controller\Back\Authentification'   => 'Simulaides\Controller\Back\AuthentificationController',
        'Simulaides\Controller\Back\Parametrage'        => 'Simulaides\Controller\Back\ParametrageController',
        'Simulaides\Controller\Back\TrancheRevenu'      => 'Simulaides\Controller\Back\TrancheRevenuController',
        'Simulaides\Controller\Back\ReferentielPrix' => 'Simulaides\Controller\Back\ReferentielPrixController',
        'Simulaides\Controller\Back\ReferentielProduit' => 'Simulaides\Controller\Back\ReferentielProduitController',
        'Simulaides\Controller\Back\Contenu'            => 'Simulaides\Controller\Back\ContenuController',
        'Simulaides\Controller\Back\DispositifGestion'  => 'Simulaides\Controller\Back\DispositifGestionController',
        'Simulaides\Controller\Back\DispositifRegle'    => 'Simulaides\Controller\Back\DispositifRegleController',
        'Simulaides\Controller\Back\DispositifTravaux'  => 'Simulaides\Controller\Back\DispositifTravauxController',
        'Simulaides\Controller\Back\DispositifHistorique' => 'Simulaides\Controller\Back\DispositifHistoriqueController',
        'Simulaides\Controller\Back\Statistique'        => 'Simulaides\Controller\Back\StatistiqueController',
        'Simulaides\Controller\Back\Partenaire'         => 'Simulaides\Controller\Back\PartenaireController',
        'Simulaides\Controller\Back\Simulation'         => 'Simulaides\Controller\Back\SimulationConseillerController',
        'Simulaides\Controller\Back\Simulation\RepriseSimulation' => 'Simulaides\Controller\Back\Simulation\RepriseSimulationController',
        'Simulaides\Controller\Back\Simulation\TestDispositif'    => 'Simulaides\Controller\Back\Simulation\TestDispositifController',
        'Simulaides\Controller\Back\Simulation\Comparatif'        => 'Simulaides\Controller\Back\Simulation\ComparatifController',
    ],
];
