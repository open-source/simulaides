<?php
return [
    'routes' => [
        'front-home-legacy' => [
            'type'    => 'Zend\Mvc\Router\Http\Segment',
            'options' => [
                'route'    => '/front-home/:cle/:colorN1/:colorN2/:colorBtn[/:codeRegion]',
                'defaults' => [
                    'controller' => 'Simulaides\Controller\Front\Accueil',
                    'action'     => 'indexLegacy',
                ],
            ],
        ],
        'front-home'        => [
            'type'    => 'Zend\Mvc\Router\Http\Segment',
            'options' => [
                'route'    => '/front-home/:cle[/:colorN1][/:codeRegion]',
                'defaults' => [
                    'controller' => 'Simulaides\Controller\Front\Accueil',
                    'action'     => 'index',
                ],
            ],
        ],
        'front-session'     => [
            'type'    => 'Zend\Mvc\Router\Http\Segment',
            'options' => [
                'route'    => '/front-session[/:perteSession]',
                'defaults' => [
                    'controller' => 'Simulaides\Controller\Front\Accueil',
                    'action'     => 'index',
                ],
            ],
        ],
        'front'             => [
            'type'          => 'Zend\Mvc\Router\Http\Literal',
            'options'       => [
                'route'    => '/front',
                'defaults' => [
                    'controller' => 'Simulaides\Controller\Front\Accueil',
                    'action'     => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes'  => [
                'check-cgu'                    => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/check-cgu',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Front\Accueil',
                            'action'     => 'check-cgu',
                        ],
                    ],
                ],
                'read-cgu'                     => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/read-cgu',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Front\Accueil',
                            'action'     => 'read-cgu',
                        ],
                    ],
                ],
                'simulation'                   => [
                    'type'          => 'Zend\Mvc\Router\Http\Literal',
                    'options'       => [
                        'route'    => '/simulation',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Front\Projet',
                            'action'     => 'index',
                        ],
                    ],
                    'may_terminate' => true,
                    'child_routes'  => [
                        'projet'          => [
                            'type'    => 'Zend\Mvc\Router\Http\Segment',
                            'options' => [
                                'route'    => '[/:action][/:id]',
                                'defaults' => [
                                    'controller' => 'Simulaides\Controller\Front\Projet',
                                    'action'     => 'index'
                                ]
                            ],
                        ],
                        'caracteristique' => [
                            'type'    => 'Zend\Mvc\Router\Http\Segment',
                            'options' => [
                                'route'    => '/caracteristique[/:action]',
                                'defaults' => [
                                    'controller' => 'Simulaides\Controller\Front\Caracteristique',
                                    'action'     => 'index',
                                ],
                            ],
                        ],
                        'travaux'         => [
                            'type'    => 'Zend\Mvc\Router\Http\Segment',
                            'options' => [
                                'route'    => '/travaux[/:action][/:code]',
                                'defaults' => [
                                    'controller' => 'Simulaides\Controller\Front\Travaux',
                                    'action'     => 'index',
                                ],
                            ],
                        ],
                        'offres-cee'         => [
                            'type'    => 'Zend\Mvc\Router\Http\Segment',
                            'options' => [
                                'route'    => '/offres-cee',
                                'defaults' => [
                                    'controller' => 'Simulaides\Controller\Front\OffresCee',
                                    'action'     => 'saveCdp',
                                ],
                            ],
                        ]
                    ]
                ],
                'simulation-particulier-lance' => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-particulier-lance',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Front\Accueil',
                            'action'     => 'lance-simulation',
                        ],
                    ]
                ],
            ]
        ],
        'admin-session'     => [
            'type'    => 'Zend\Mvc\Router\Http\Segment',
            'options' => [
                'route'    => '/admin-session',
                'defaults' => [
                    'controller' => 'Simulaides\Controller\Back\Authentification',
                    'action'     => 'perte-session-projet',
                ],
            ],
        ],
        'admin'             => [
            'type'          => 'Zend\Mvc\Router\Http\Segment',
            'options'       => [
                'route'    => '/admin',
                'defaults' => [
                    'controller' => 'Simulaides\Controller\Back\Authentification',
                    'action'     => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes'  => [
                'deconnecte'                          => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/deconnecte',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Authentification',
                            'action'     => 'deconnecte',
                        ],
                    ],
                ],
                'accueil'                             => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/accueil',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Authentification',
                            'action'     => 'accueil',
                        ],
                    ],
                ],
                'read-cgu'                            => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/read-cgu',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Authentification',
                            'action'     => 'read-cgu',
                        ],
                    ],
                ],
                'simulation'                          => [
                    'type'          => 'Zend\Mvc\Router\Http\Literal',
                    'options'       => [
                        'route'    => '/simulation',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Front\Projet',
                            'action'     => 'index',
                        ],
                    ],
                    'may_terminate' => true,
                    'child_routes'  => [
                        'projet'          => [
                            'type'    => 'Zend\Mvc\Router\Http\Segment',
                            'options' => [
                                'route'    => '[/:action][/:id]',
                                'defaults' => [
                                    'controller' => 'Simulaides\Controller\Front\Projet',
                                    'action'     => 'index'
                                ]
                            ],
                        ],
                        'caracteristique' => [
                            'type'    => 'Zend\Mvc\Router\Http\Segment',
                            'options' => [
                                'route'    => '/caracteristique[/:action]',
                                'defaults' => [
                                    'controller' => 'Simulaides\Controller\Front\Caracteristique',
                                    'action'     => 'index',
                                ],
                            ],
                        ],
                        'travaux'         => [
                            'type'    => 'Zend\Mvc\Router\Http\Segment',
                            'options' => [
                                'route'    => '/travaux[/:action][/:code]',
                                'defaults' => [
                                    'controller' => 'Simulaides\Controller\Front\Travaux',
                                    'action'     => 'index',
                                ],
                            ],
                        ]
                    ]
                ],
                'simulation-conseiller'               => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-conseiller',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\RepriseSimulation',
                            'action'     => 'index',
                        ],
                    ]
                ],
                'simulation-conseiller-lance'         => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-conseiller-lance',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\RepriseSimulation',
                            'action'     => 'lance-simulation',
                        ],
                    ]
                ],
                'simulation-test-dispositif'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-test-dispositif',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\TestDispositif',
                            'action'     => 'index',
                        ],
                    ]
                ],
                'simulation-test-le-dispositif'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-test-le-dispositif',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\TestDispositif',
                            'action'     => 'un-dispositif',
                        ],
                    ]
                ],
                'simulation-conseiller-test-existant' => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-conseiller-test-existant',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\TestDispositif',
                            'action'     => 'lance-simulation-test-existant',
                        ],
                    ]
                ],
                'simulation-conseiller-test-nouveau'  => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-conseiller-test-nouveau',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\TestDispositif',
                            'action'     => 'lance-simulation-test-nouveau',
                        ],
                    ]
                ],
                'simulation-conseiller-change-region' => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-conseiller-change-region',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\TestDispositif',
                            'action'     => 'change-region',
                        ],
                    ]
                ],
                'simulation-comparatif-creer'   => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-comparatif-creer',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'modifier',
                        ],
                    ]
                ],
                'simulation-comparatif-modifier'   => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-comparatif-modifier',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'modifier',
                        ],
                    ]
                ],
                'simulation-comparatif-simuler'              => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-comparatif-simuler',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'simuler',
                        ],
                    ]
                ],
                'simulation-comparatif-resumer-simulation'   => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-comparatif-resumer-simulation',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'resumer-simulation',
                        ],
                    ]
                ],
                'simulation-comparatif-consulter-simulation' => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/simulation-comparatif-consulter-simulation',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'consulter-simulation',
                        ],
                    ]
                ],
                'simulation-comparatif-charger-simulation'   => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-comparatif-charger-simulation',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'charger-simulation',
                        ],
                    ]
                ],
                'simulation-comparatif-supprimer-simulation' => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-comparatif-supprimer-simulation',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'supprimer-simulation',
                        ],
                    ]
                ],
                'simulation-comparatif-dupliquer-simulation' => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-comparatif-dupliquer-simulation',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'dupliquer-simulation',
                        ],
                    ]
                ],
                'simulation-comparatif-enregistrer'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/simulation-comparatif-enregistrer',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'enregistrer',
                        ],
                    ]
                ],
                'simulation-comparatif-consulter'            => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/simulation-comparatif-consulter',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'consulter',
                        ],
                    ]
                ],
                'simulation-comparatif-telecharger'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/simulation-comparatif-telecharger',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Simulation\Comparatif',
                            'action'     => 'telecharger',
                        ],
                    ]
                ],
                'dispositif'                                 => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif[/:codeRegion]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'dispositif-liste',
                        ],
                    ],
                ],
                'dispositif-export'                   => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-export/:codeRegion',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'export-dispositif',
                        ],
                    ],
                ],
                'export-dispositif-verification'      => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-export-verification/:codeRegion',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'export-dispositif-verification',
                        ],
                    ],
                ],
                'dispositif-ajout'                    => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-ajout/:codeRegion',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'ajout-dispositif-fiche',
                        ],
                    ],
                ],
                'dispositif-modif'                    => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-modif/:codeRegion/:idDispositif',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'modif-dispositif-fiche',
                        ],
                    ],
                ],
                'dispositif-change-ordre'             => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/dispositif-change-ordre',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'dispositif-change-ordre',
                        ],
                    ],
                ],
                'dispositif-supprime'                 => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/dispositif-supprime',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'dispositif-supprime',
                        ],
                    ],
                ],
                'dispositif-desactive'                => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/dispositif-desactive',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'dispositif-desactive',
                        ],
                    ],
                ],
                'dispositif-avalider'                 => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/dispositif-avalider',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'dispositif-avalider',
                        ],
                    ],
                ],
                'dispositif-valider'                  => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/dispositif-valider',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'dispositif-valide',
                        ],
                    ],
                ],
                'dispositif-dupliquer'                => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/dispositif-dupliquer',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'dispositif-duplique',
                        ],
                    ],
                ],
                'dispositif-gestion'                  => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-gestion[/:codeRegion][/:idDispositif][/:isConsult]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'dispositif-fiche-gestion',
                        ],
                    ],
                ],
                'dispositif-change-localisation'      => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-change-localisation',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifGestion',
                            'action'     => 'change-localisation',
                        ],
                    ],
                ],
                'dispositif-regle'                    => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-regle[/:idDispositif][/:isConsult]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifRegle',
                            'action'     => 'dispositif-liste-regle',
                        ],
                    ],
                ],
                'dispositif-regle-ajout'              => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-regle-ajout[/:idDispositif]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifRegle',
                            'action'     => 'dispositif-regle-ajout',
                        ],
                    ],
                ],
                'dispositif-regle-modif'              => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-regle-modif[/:idDispositifRegle]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifRegle',
                            'action'     => 'dispositif-regle-modif',
                        ],
                    ],
                ],
                'dispositif-regle-supprime'           => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/dispositif-regle-supprime',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifRegle',
                            'action'     => 'dispositif-regle-supprime',
                        ],
                    ],
                ],
                'dispositif-travaux'                  => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-travaux[/:idDispositif][/:isConsult][/:codeTravaux]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifTravaux',
                            'action'     => 'dispositif-liste-travaux',
                        ],
                    ],
                ],
                'dispositif-travaux-supprime'                  => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-travaux-supprime',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifTravaux',
                            'action'     => 'dispositif-travaux-supprime',
                        ],
                    ],
                ],
                'dispositif-travaux-ajout'                  => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-travaux-ajout',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifTravaux',
                            'action'     => 'dispositif-travaux-ajout',
                        ],
                    ],
                ],
                'dispositif-travaux-modif'            => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-travaux-modif[/:idDispositif][/:codeTravaux][/:isConsult]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifTravaux',
                            'action'     => 'dispositif-travaux-modif',
                        ],
                    ],
                ],
                'dispositif-travaux-regle-supprime'   => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/dispositif-travaux-regle-supprime',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifTravaux',
                            'action'     => 'dispositif-travaux-regle-supprime',
                        ],
                    ],
                ],
                'dispositif-travaux-regle-ajout'      => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-travaux-regle-ajout[/:idDispositifTravaux]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifTravaux',
                            'action'     => 'dispositif-travaux-regle-ajout',
                        ],
                    ],
                ],
                'dispositif-travaux-regle-modif'      => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-travaux-regle-modif[/:id][/:idParent]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifTravaux',
                            'action'     => 'dispositif-travaux-regle-modif',
                        ],
                    ],
                ],
                'dispositif-historique'               => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/dispositif-historique[/:idDispositif][/:isConsult]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\DispositifHistorique',
                            'action'     => 'dispositif-fiche-historique',
                        ],
                    ],
                ],
                'statistique'                         => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/statistique',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Statistique',
                            'action'     => 'index',
                        ],
                    ],
                ],
                'statistique-get-departements'        => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/statistique/departements[/:idRegion]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Statistique',
                            'action'     => 'get-departements',
                        ],
                    ],
                ],
                'statistique-get-epci-stats'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/statistique/epci[/:idDepartement]',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Statistique',
                            'action'     => 'get-epci-stats',
                        ],
                    ],
                ],
                'partenaire'                          => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/partenaire',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Partenaire',
                            'action'     => 'index',
                        ],
                    ],
                ],
                'partenaire-ajout'                    => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/partenaire-ajout',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Partenaire',
                            'action'     => 'add',
                        ],
                    ],
                ],
                'partenaire-modif'                    => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/partenaire-modif/:idPartenaire',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Partenaire',
                            'action'     => 'edit',
                        ],
                    ],
                ],
                'partenaire-supp'                     => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/partenaire-supp/:idPartenaire',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Partenaire',
                            'action'     => 'delete',
                        ],
                    ],
                ],
                'parametrage'                         => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/parametrage',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Parametrage',
                            'action'     => 'index',
                        ],
                    ]
                ],
                'parametrage-tranche'                 => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/parametrage-tranche',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\TrancheRevenu',
                            'action'     => 'groupe-liste',
                        ],
                    ]
                ],
                'groupe-tranche-supp'                 => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/groupe-tranche-supp',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\TrancheRevenu',
                            'action'     => 'groupe-supp',
                        ],
                    ]
                ],
                'groupe-tranche-fiche-ajout'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/groupe-tranche-fiche-ajout',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\TrancheRevenu',
                            'action'     => 'ajout-groupe-fiche',
                        ],
                    ]
                ],
                'groupe-tranche-fiche-modif'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/groupe-tranche-fiche-modif',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\TrancheRevenu',
                            'action'     => 'modif-groupe-fiche',
                        ],
                    ]
                ],
                'tranche-liste'                       => [
                    'type'    => 'Zend\Mvc\Router\Http\Segment',
                    'options' => [
                        'route'    => '/tranche-liste/:codeGroupe',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\TrancheRevenu',
                            'action'     => 'tranche-liste',
                        ],
                    ]
                ],
                'tranche-revenu-fiche-ajout'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/tranche-revenu-fiche-ajout',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\TrancheRevenu',
                            'action'     => 'ajout-tranche-fiche',
                        ],
                    ]
                ],
                'tranche-revenu-fiche-modif'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/tranche-revenu-fiche-modif',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\TrancheRevenu',
                            'action'     => 'modif-tranche-fiche',
                        ],
                    ]
                ],
                'tranche-revenu-supp'                 => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/tranche-revenu-supp',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\TrancheRevenu',
                            'action'     => 'tranche-supp',
                        ],
                    ]
                ],
                'parametrage-ref-prix'                => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/parametrage-ref-prix',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\ReferentielProduit',
                            'action'     => 'index',
                        ],
                    ]
                ],
                'parametrage-ref-prix-change-travaux' => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/parametrage-ref-prix-change-travaux',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\ReferentielProduit',
                            'action'     => 'change-travaux',
                        ],
                    ]
                ],
                'parametrage-ref-produit-modif'          => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/parametrage-ref-produit-modif',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\ReferentielProduit',
                            'action'     => 'fiche-modif',
                        ],
                    ]
                ],
                'parametrage-ref-prix-export'         => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/parametrage-ref-prix-export',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\ReferentielProduit',
                            'action'     => 'export',
                        ],
                    ]
                ],
                'parametrage-ref-prix-aide-cee'       => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/parametrage-ref-prix-aide-cee',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\ReferentielProduit',
                            'action'     => 'aide-cee',
                        ],
                    ]
                ],
                'parametrage-contenu'                 => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/parametrage-contenu',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Contenu',
                            'action'     => 'index',
                        ],
                    ]
                ],
                'manuel'                              => [
                    'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => [
                        'route'    => '/manuel',
                        'defaults' => [
                            'controller' => 'Simulaides\Controller\Back\Authentification',
                            'action'     => 'manuel',
                        ],
                    ]
                ]
            ]
        ]
    ]
];
