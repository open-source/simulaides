<?php
return [
    'connection' => [
        // default connection name
        'orm_default' => [
            'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
            'params'      => [
                'host'     => 'database',
                'port'     => '3306',
                'user'     => 'root',
                'password' => '3zNMXRZzF3WVDyeTtZin',
                'dbname'   => 'simulaides',
                'charset'  => 'utf8',
                'options'  => [1001 => true]
            ]
        ]
    ],
    'driver'     => [
        'doctrine_annotation_driver' => [
            'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
            'cache' => 'array',
            'paths' => [
                __DIR__ . '/../src/Simulaides/Domain/Entity'
            ],
        ],
        // default metadata driver, aggregates all other drivers into a single one.
        // Override `orm_default` only if you know what you're doing
        'orm_default'                => [
            'drivers' => [
                // register `doctrine_annotation_driver` for any entity under namespace `Simulaides\Domain\Entities`
                'Simulaides\Domain\Entity' => 'doctrine_annotation_driver'
            ]
        ],
    ]
];
