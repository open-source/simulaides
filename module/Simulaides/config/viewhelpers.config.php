<?php
return array(
    'invokables' => array(
        'formLabel'          => 'Simulaides\View\Helper\FormLabel',
        'formSelect'         => 'Simulaides\View\Helper\FormSelect',
        'table'              => 'Simulaides\View\Helper\Table',
        'formElement'        => 'Simulaides\View\Helper\FormElement',
        'formElementErrors'  => 'Simulaides\View\Helper\FormElementErrors',
        'pastilleSimulation' => 'Simulaides\View\Helper\PastilleSimulation',
        'bandeauSimulation'  => 'Simulaides\View\Helper\BandeauSimulation',
        'reglesDispositif' => 'Simulaides\View\Helper\ReglesDispositif',
        'travauxDispositif' => 'Simulaides\View\Helper\TravauxDispositif',
        'travauxDispositifResultat' => 'Simulaides\View\Helper\TravauxDispositifResultat',
        'mathCaptcha'        => 'Simulaides\View\Helper\MathCaptchaHelper',
    ),
    'factories'  => array(
        'colorParam'    => 'Simulaides\View\Helper\Factory\ColorHelperFactory',
        'bandeauEntete' => 'Simulaides\View\Helper\Factory\BandeauEnteteFactory',
        'versionApp' => 'Simulaides\View\Helper\Factory\VersionAppHelperFactory',
    )
);
