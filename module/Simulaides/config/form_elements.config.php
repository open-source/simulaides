<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 16/02/15
 * Time: 10:33
 */

return [
    'invokables' => [
        'CaracteristiqueForm'        => 'Simulaides\Form\Caracteristique\CaracteristiqueForm',
        'OffresCeeForm'              => 'Simulaides\Form\OffresCee\OffresCeeForm',
        'LocalisationFieldset'       => 'Simulaides\Form\Caracteristique\LocalisationFieldset',
        'AccessProjetForm'           => 'Simulaides\Form\Projet\AccessProjetForm',
        'TravauxForm'                => 'Simulaides\Form\Travaux\TravauxForm',
        'MultiTravauxForm'           => 'Simulaides\Form\Travaux\MultiTravauxForm',
        'SaisieMontantFieldset'      => 'Simulaides\Form\Travaux\SaisieMontantFieldset',
        'SaisieParamFieldset'        => 'Simulaides\Form\Travaux\SaisieParamFieldset',
        'SaisieProduitFieldset'      => 'Simulaides\Form\Travaux\SaisieProduitFieldset',
        'TravauxFieldset'            => 'Simulaides\Form\Travaux\TravauxFieldset',
        'AuthentificationForm'       => 'Simulaides\Form\Authentification\AuthentificationForm',
        'FicheGroupeTrancheForm'     => 'Simulaides\Form\TrancheRevenu\FicheGroupeTrancheForm',
        'FicheTrancheRevenuForm'     => 'Simulaides\Form\TrancheRevenu\FicheTrancheRevenuForm',
        'ReferentielPrixParamForm'   => 'Simulaides\Form\ReferentielProduit\ReferentielPrixParamForm',
        'RefPrixAideCEEForm'         => 'Simulaides\Form\ReferentielProduit\RefPrixAideCEEForm',
        'ReferentielProduitForm'     => 'Simulaides\Form\ReferentielProduit\ReferentielProduitForm',
        'ReferentielPrixProduitOuForm' => 'Simulaides\Form\ReferentielPrix\ReferentielPrixProduitOuForm',
        'ContenuParamForm'           => 'Simulaides\Form\Contenu\ContenuParamForm',
        'RegionAdminForm'            => 'Simulaides\Form\Dispositif\RegionAdminForm',
        'FiltreDispositifForm'       => 'Simulaides\Form\Dispositif\FiltreDispositifForm',
        'DispositifGestionForm'      => 'Simulaides\Form\Dispositif\DispositifGestionForm',
        'DispositifFieldset'         => 'Simulaides\Form\Dispositif\DispositifFieldset',
        'PerimetreGeoFieldset'       => 'Simulaides\Form\Dispositif\PerimetreGeoFieldset',
        'ValeurListeFieldset'        => 'Simulaides\Form\ValeurListe\ValeurListeFieldset',
        'EditeurRegleForm'           => 'Simulaides\Form\Dispositif\EditeurRegleForm',
        'DispositifTravauxForm'      => 'Simulaides\Form\Dispositif\DispositifTravauxForm',
        'DispositifTravauxFieldset'  => 'Simulaides\Form\Dispositif\DispositifTravauxFieldset',
        'DispositifTravauxRegleForm' => 'Simulaides\Form\Dispositif\DispositifTravauxRegleForm',
        'DispositifHistoriqueForm'   => 'Simulaides\Form\Dispositif\DispositifHistoriqueForm',
        'AccesSimulationForm'        => 'Simulaides\Form\Simulation\AccesSimulationForm',
        'ProjetFieldset'             => 'Simulaides\Form\Caracteristique\ProjetFieldset',
        'PartenaireForm'             => 'Simulaides\Form\Partenaire\PartenaireForm',
        'StatistiqueForm'            => 'Simulaides\Form\Statistique\StatistiqueForm',
        'AutreDispositifForm'        => 'Simulaides\Form\Projet\AutreDispositifForm',
        'AutreCommentaireForm'       => 'Simulaides\Form\Projet\AutreCommentaireForm',
        'ResultatPDFForm'            => 'Simulaides\Form\Projet\ResultatPDFForm',
        'ConditionGeneraleForm'      => 'Simulaides\Form\Accueil\ConditionGeneraleForm',
        'SelectionTravauxFieldset'   => 'Simulaides\Form\Travaux\TravauxParCategorieFieldset',
        'BbcForm'                    => 'Simulaides\Form\Travaux\BbcForm',
        'AccesComparatifForm'        => 'Simulaides\Form\Comparatif\AccesComparatifForm'
    ],
    'factories'  => [
        'ValeurListeObjectSelect' => 'Simulaides\Form\Element\Factory\ValeurListeObjectSelectFactory',
        'WisiwigElement'          => 'Simulaides\Form\Element\Factory\WisiwigFactory',
        'DatePicker'              => 'Simulaides\Form\Element\Factory\DatePickerFactory'
    ],
];
