<?php
/**
 * Created by PhpStorm.
 * User: yoan.durand
 * Date: 17/02/15
 * Time: 10:42
 */
return [
    'use_cookies'     => false,
    'cookie_httponly' => false,
    'use_only_cookies' => false,
    'use_trans_sid' => true,
];
