<?php
return [
    'default'        => [
        'assets'  => [
            '@jquery',
            '@bootstrap_js',
            '@bootstrap_less',
            '@js',
            '@css',
            '@less',
            '@summernote',
            '@datepicker',
            '@chosen_css',
            '@chosen_js'
        ],
        'options' => [
            'mixin' => true
        ],
        'filters' => [
            '?CssRewriteFilter' => [
                'name' => 'Assetic\Filter\CssRewriteFilter'
            ],
            '?CssMinFilter'     => [
                'name' => 'Assetic\Filter\CssMinFilter'
            ],
            '?JSMinFilter'      => [
                'name' => 'Assetic\Filter\JSMinFilter']
        ]
    ],
    'controllers'    => [
        'Simulaides\Controller\Back\Parametrage'       => [
            '@parametrage_tabs',
            '@parametrage_tranche',
            '@parametrage_ref_prix',
            '@parametrage_contenu'
        ],
        'Simulaides\Controller\Back\DispositifGestion' => [
            '@dispositif_gestion',
            '@parametrage_tabs',
        ],
        'Simulaides\Controller\Back\Simulation'        => [
            '@simulation_conseiller',
        ],
        'Simulaides\Controller\Back\Simulation\RepriseSimulation' => [
            '@simulation_conseiller',
        ],
        'Simulaides\Controller\Back\Simulation\TestDispositif' => [
            '@simulation_conseiller',
        ],
        'Simulaides\Controller\Back\Simulation\Comparatif' => [
            '@comparatif',
            '@caracteristique_index'
        ],
        'Simulaides\Controller\Front\Caracteristique'  => [
            '@caracteristique_index',
        ],
        'Simulaides\Controller\Front\Accueil' => [
            '@accueil',
        ],
        'Simulaides\Controller\Front\Projet'  => [
            '@resultat',
            '@resultat_print'
        ],

    ],
    'modules'        => [
        'jqueryui'   => [
            'root_path'   => __DIR__ . '/../../../vendor/components/jqueryui',
            'collections' => [
                'datepicker' => [
                    'assets'  => [
                        'jquery-ui.min.js',
                        'ui/minified/datepicker.min.js',
                        'ui/minified/i18n/datepicker-fr.min.js',
                    ],
                    'options' => [
                        'output' => 'js/datepicker.js'
                    ]
                ],
            ]
        ],
        'simulaides' => [
            'root_path'   => __DIR__ . '/../assets',
            'collections' => [
                'bootstrap_less'        => [
                    'assets'  => [
                        'bootstrap/less/bootstrap.less'
                    ],
                    'filters' => [
                        'LessPhp'           => [
                            'name' => 'Assetic\Filter\LessphpFilter'
                        ],
                        '?CssRewriteFilter' => [
                            'name' => 'Assetic\Filter\CssRewriteFilter'
                        ],
                        '?CssMinFilter'     => [
                            'name' => 'Assetic\Filter\CssMinFilter'
                        ],
                    ],
                    'options' => [
                        'output' => "css/bootstrap.css"
                    ]
                ],
                'bootstrap_js'          => [
                    'assets'  => [
                        'bootstrap/js/collapse.js',
                        'bootstrap/js/modal.js',
                        'bootstrap/js/tab.js',
                        'bootstrap/js/alert.js',
                        'bootstrap/js/tooltip.js',
                        'bootstrap/js/dropdown.js',
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => 'js/bootstrap.js'
                    ]
                ],
                'bootstrap_fonts'       => [
                    'assets'  => [
                        'bootstrap/fonts/*'
                    ],
                    'options' => [
                        'move_raw' => true,
                        'output'   => 'fonts/*',
                    ]
                ],
                'awesome_fonts'         => [
                    'assets'  => [
                        'fonts/*'
                    ],
                    'options' => [
                        'move_raw' => true,
                        'output'   => 'fonts/*',
                    ]
                ],
                'less'                  => [
                    'assets'  => [
                        'less/*.css.less'
                    ],
                    'filters' => [
                        'LessPhp'           => [
                            'name' => 'Assetic\Filter\LessphpFilter'
                        ],
                        '?CssRewriteFilter' => [
                            'name' => 'Assetic\Filter\CssRewriteFilter'
                        ],
                        '?CssMinFilter'     => [
                            'name' => 'Assetic\Filter\CssMinFilter'
                        ]
                    ],
                    'options' => [
                        'output' => "css/*.css"
                    ]
                ],
                'css'                   => [
                    'assets'  => [
                        'css/*.css'
                    ],
                    'filters' => [
                        '?CssRewriteFilter' => [
                            'name' => 'Assetic\Filter\CssRewriteFilter'
                        ],
                        '?CssMinFilter'     => [
                            'name' => 'Assetic\Filter\CssMinFilter'
                        ]
                    ],
                ],
                'jquery'                => [
                    'assets'  => [
                        'js/jquery/jquery-1.11.2.min.js',
                        'js/jquery/jquery.rowsorter.min.js',
                        'js/jquery/jquery.caret.js',
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                ],
                'summernote'            => [
                    'assets' => [
                        'js/summernote/summernote.min.js',
                        'js/summernote/summernote-fr-FR.js'
                    ]
                ],
                'js'                    => [
                    'assets'  => [
                        'js/*.js',
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],

                ],
                'images'                => [
                    'assets'  => [
                        'img/*.png',
                        'img/*.ico',
                    ],
                    'options' => [
                        'move_raw' => true,
                    ]
                ],
                'imagesui'              => [
                    'assets'  => [
                        'images/*',
                    ],
                    'options' => [
                        'move_raw' => true,
                    ]
                ],
                'chosen_css'            => [
                    'assets'  => [
                        'js/lib/chosen/chosen.min.css'
                    ],
                    'options' => [
                        'output' => "js/lib/chosen/*"
                    ]
                ],
                'chosen_img'            => [
                    'assets'  => [
                        'js/lib/chosen/*.png'
                    ],
                    'options' => [
                        'move_raw' => true,
                        'output'   => "js/lib/chosen/*"
                    ]
                ],
                'chosen_js'             => [
                    'assets'  => [
                        'js/lib/chosen/chosen.jquery.min.js',
                        'js/lib/chosen/chosen.proto.min.js'
                    ],
                    'options' => [
                        'output' => "js/lib/chosen/*"
                    ]
                ],
                'parametrage_tabs'      => [
                    'assets'  => [
                        'js/standalone/parametrage-tabs.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'parametrage_tranche'   => [
                    'assets'  => [
                        'js/standalone/parametrage-tranche.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'parametrage_ref_prix'  => [
                    'assets'  => [
                        'js/standalone/parametrage-ref-prix.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'parametrage_contenu'   => [
                    'assets'  => [
                        'js/standalone/spectrum.js',
                        'js/standalone/parametrage-contenu.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'dispositif_gestion'    => [
                    'assets'  => [
                        'js/standalone/dispositif-gestion.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'simulation_conseiller' => [
                    'assets'  => [
                        'js/standalone/simulation-conseiller.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'comparatif' => [
                    'assets'  => [
                        'js/standalone/comparatif.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'caracteristique_index' => [
                    'assets'  => [
                        'js/standalone/caracteristique-index.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'accueil'               => [
                    'assets'  => [
                        'js/standalone/accueil.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'resultat'              => [
                    'assets'  => [
                        'js/standalone/simulation-resultat.js'
                    ],
                    'filters' => [
                        '?JSMinFilter' => [
                            'name' => 'Assetic\Filter\JSMinFilter']
                    ],
                    'options' => [
                        'output' => "js/standalone/*"
                    ]
                ],
                'resultat_print' => [
                    'assets'  => [
                        'less/result_print.css.less'
                    ],
                    'filters' => [
                        'LessPhp'           => [
                            'name' => 'Assetic\Filter\LessphpFilter'
                        ],
                        '?CssRewriteFilter' => [
                            'name' => 'Assetic\Filter\CssRewriteFilter'
                        ],
                        '?CssMinFilter'     => [
                            'name' => 'Assetic\Filter\CssMinFilter'
                        ]
                    ],
                    'options' => [
                        'output' => "css/resultat_print.css",
                    ]
                ],
            ],
        ],
    ],
];
