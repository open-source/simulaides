<?php
namespace ServicesWeb\Controller;

use ServicesWeb\Constante\ServicesWebConstante;
use ServicesWeb\Service\DocumentationService;
use ServicesWeb\Service\ServicesWebService;
use ServicesWeb\Service\ServicesWebV2Service;
use ServicesWeb\Service\ServicesWebV3Service;
use Simulaides\Service\TravauxService;
use Simulaides\Service\PartenaireService;
use Simulaides\Tools\Utils;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Classe ServicesWebController
 *
 * Projet : SimulAides 2015-2015
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   ServicesWeb\src\ServicesWeb\Controller
 */
class ServicesWebController extends AbstractActionController implements IWsController
{
    /**
     * Version demandée
     * @var string
     */
    private $version;

    /**
     * Nom du sous-service à appeler
     * @var string
     */
    private $sousService;

    /**
     * Service de documentation
     *
     * @var DocumentationService
     */
    private $documentationService;

    /**
     * Appel du service web
     * @return array|\Zend\Stdlib\ResponseInterface
     */
    public function indexAction()
    {
        try {
            $data = $this->processAppelServiceWeb();
        } catch (\Exception $oException) {
            //Gestion des exception remontée pour être affichées en js
            $data['erreur'] = $oException->getMessage();
        }
        return new JsonModel($data);
    }

    /**
     * Permet d'afficher la page de documentation
     *
     * @return ViewModel
     */
    public function documentationAction()
    {
        //Permet de savoir de quel sous-service il faut afficher la documentation
        $sous_service = $this->getEvent()->getRouteMatch()->getParam('sous-service');

        /** @var ViewModel $model */
        $model = new ViewModel();
        $model->setVariable('documentation', $this->getDocumentationParSousService($sous_service));
        $model->setVariable('structure', $this->getStructureParamParSousService($sous_service));
        $model->setVariable(
            'textAccueil',
            'Voici la documentation des paramètres attendus en entrée du service <i>' . $sous_service . '</i>'
        );
        $model->setVariable(
            'textStructure',
            'Voici la structure des paramètres attendus en entrée du service <i>' . $sous_service . '</i>'
        );
        $model->setTemplate('servicesweb/documentation.phtml');
        $data['documentation'] = $this->getServiceLocator()->get('ViewRenderer')->render($model);

       return new JsonModel($data);
    }

    /**
     * Permet de récupérer la documentation d'un sous-service
     *
     * @param string $sousService Nom du sous-service
     *
     * @return array|void
     */
    private function getDocumentationParSousService($sousService)
    {
        $documentation = [];

        /** @var DocumentationService $serviceDoc */
        $serviceDoc = $this->getDocumentationService();

        if ($sousService == ServicesWebConstante::SW_SOUS_SERVICE_TRAVAUX) {
            $documentation = $serviceDoc->getDocumentationServiceGetTravaux();
        } elseif ($sousService == ServicesWebConstante::SW_SOUS_SERVICE_CALCULER) {
            $documentation = $serviceDoc->getDocumentationServiceCalculer();
        } elseif ($sousService == ServicesWebConstante::SW_SOUS_SERVICE_SIMULATION) {
            $documentation = $serviceDoc->getDocumentationServiceGetSimulation();
        }

        return $documentation;
    }

    /**
     * Permet de récupérer la structure des param d'un sous-service
     *
     * @param string $sousService Nom du sous-service
     *
     * @return array|void
     */
    private function getStructureParamParSousService($sousService)
    {
        $documentation = [];

        /** @var DocumentationService $serviceDoc */
        $serviceDoc = $this->getDocumentationService();

        if ($sousService == ServicesWebConstante::SW_SOUS_SERVICE_TRAVAUX) {
            $documentation = $serviceDoc->getStructureParamServiceGetTravaux();
        } elseif ($sousService == ServicesWebConstante::SW_SOUS_SERVICE_CALCULER) {
            $documentation = $serviceDoc->getStructureParamServiceCalculer();
        } elseif ($sousService == ServicesWebConstante::SW_SOUS_SERVICE_SIMULATION) {
            $documentation = $serviceDoc->getStructureParamServiceGetSimulation();
        }

        return $documentation;
    }

    /**
     * Permet de faire les vérifications et de gérer l'appel au service web
     *
     * @return array
     * @throws \Exception
     */
    private function processAppelServiceWeb()
    {
        //1 - Vérifier la version
        $tabConfig = $this->serviceLocator->get('ApplicationConfig');
        if ($this->verifieVersion()) {
            //2 - Vérifier le sous-service
            if ($this->verifieSousService()) {
                //3 - Vérifier qu'il existe de paramètres d'entrée
                if ($this->verifieParamEntree()) {
                    //4 - Vérifier le domaine + clé
                    if ((isset($tabConfig['controle-webservice']) && $tabConfig['controle-webservice']) || $this->verifieDomaineEtCle() ) {
                        //5 - Si tout est OK, appel de la bonne action
                        $data = $this->appelServiceWeb();
                        $this->response->setStatusCode(200);
                        return $data;
                    } else {
                        $this->response->setStatusCode(403);
                        throw new \Exception("Le domaine ou la clé de sécurisation ne sont pas corrects.");
                    }
                } else {
                    //si les paramètres ne sont pas renseignés alors qu'ils le devraient,
                    // affichage de la page de documentation
                    return $this->redirect()->toRoute('documentation-ws', ['sous-service' => $this->sousService]);
                }
            } else {
                throw new \Exception("Le nom du service est inconnu.");
            }
        } else {
            throw new \Exception("Le numéro de version n'est pas correct.");
        }
    }

    /**
     * Retourne la clé de sécurisation
     *
     * @return string
     */
    private function getCleSecurisation()
    {
        $cle = '';
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataPost = $request->getPost();
            if (!empty($dataPost)) {
                if (isset($dataPost[ServicesWebConstante::PRM_CLE_SECURISATION])) {
                    $cle = $dataPost[ServicesWebConstante::PRM_CLE_SECURISATION];
                }
            }
        }
        return $cle;
    }

    /**
     * Permet de faire l'appel au bon service web
     *
     * @return array
     */
    private function appelServiceWeb()
    {
        $data = [];

        switch ($this->sousService) {

            // ------ Service de récupération de la liste des travaux
            case ServicesWebConstante::SW_SOUS_SERVICE_TRAVAUX:
                /** @var TravauxService $travauxService */
                $travauxService = $this->serviceLocator->get(TravauxService::SERVICE_NAME);
                if ($this->version == ServicesWebConstante::SW_VERSION_1
                    || $this->version == ServicesWebConstante::SW_VERSION_2
                    || $this->version == ServicesWebConstante::SW_VERSION_3) {
                    $data = $travauxService->getTravauxPourServiceWeb();
                }
                break;

            // ------ Service de calcul d'une simulation
            case ServicesWebConstante::SW_SOUS_SERVICE_CALCULER:

                if ($this->version == ServicesWebConstante::SW_VERSION_1) {
                    /** @var ServicesWebService $swService */
                    $swService = $this->serviceLocator->get(ServicesWebService::SERVICE_NAME);
                }else if ($this->version == ServicesWebConstante::SW_VERSION_2) {
                    /** @var ServicesWebV2Service $swService */
                    $swService = $this->serviceLocator->get(ServicesWebV2Service::SERVICE_NAME);
                }else if ($this->version == ServicesWebConstante::SW_VERSION_3) {
                    /** @var ServicesWebV3Service $swService */
                    $swService = $this->serviceLocator->get(ServicesWebV3Service::SERVICE_NAME);
                }

                if ($this->version == ServicesWebConstante::SW_VERSION_1
                    || $this->version == ServicesWebConstante::SW_VERSION_2
                    || $this->version == ServicesWebConstante::SW_VERSION_3) {
                    $params = $this->getParamEntreeServiceWeb();
                    $data   = $swService->lanceSimulationPourServiceWeb($params);
                }
                break;

            // ------ Service de récupération des données d'une simulation
            case ServicesWebConstante::SW_SOUS_SERVICE_SIMULATION:
                if ($this->version == ServicesWebConstante::SW_VERSION_1) {
                    /** @var ServicesWebService $swService */
                    $swService = $this->serviceLocator->get(ServicesWebService::SERVICE_NAME);
                }else if ($this->version == ServicesWebConstante::SW_VERSION_2) {
                    /** @var ServicesWebV2Service $swService */
                    $swService = $this->serviceLocator->get(ServicesWebV2Service::SERVICE_NAME);
                }else if ($this->version == ServicesWebConstante::SW_VERSION_3) {
                    /** @var ServicesWebV3Service $swService */
                    $swService = $this->serviceLocator->get(ServicesWebV3Service::SERVICE_NAME);
                }

                if ($this->version == ServicesWebConstante::SW_VERSION_1
                    || $this->version == ServicesWebConstante::SW_VERSION_2
                    || $this->version == ServicesWebConstante::SW_VERSION_3) {
                    $params = $this->getParamEntreeServiceWeb();
                    $data   = $swService->getSimulationPourServiceWeb($params);
                }
                break;
        }

        return $data;
    }

    /**
     * Permet de retourner les paramètres d'entrée au service
     *
     * @return mixed
     */
    private function getParamEntreeServiceWeb()
    {
        $params = [];

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $params = $request->getPost();
        }

        return $params;
    }

    /**
     * Permet de vérifier que les paramètres d'entrée sont renseignés
     * sinon, on redirige vers la page de documentation
     *
     * @return bool|\Zend\Http\Response
     */
    private function verifieParamEntree()
    {
        $dataPost = [];
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataPost = $request->getPost();
        }
        return (!empty($dataPost) && sizeof($dataPost) > 0);
    }

    /**
     * Permet de vérifier le domaine et la clé de sécurité
     *
     * @return bool
     */
    private function verifieDomaineEtCle()
    {
        $clePost = $this->getCleSecurisation();
        $estOK   = false;
        if (!empty($clePost)) {
            $conteneurParent   = $_SERVER['HTTP_REFERER'];
            $serveurParent     = Utils::getConteneurParentFromPageAppelante($conteneurParent);
            $cle               = '';
            $partenaireService = $this->getServiceLocator()->get(PartenaireService::SERVICE_NAME);
            $tabListeBlanche   = $partenaireService->getListeUrlByCleApi($clePost);
            $domaineExist      = array_key_exists($serveurParent, $tabListeBlanche);
            if ($domaineExist) {
                $cle = $tabListeBlanche[$serveurParent];
            }
            $estOK = ($cle == $clePost);
        }

        return $estOK;
    }

    /**
     * Permet de vérifier que la version est correcte
     *
     * @return bool
     */
    private function verifieVersion()
    {
        $this->version = $this->getEvent()->getRouteMatch()->getParam('version');
        return ($this->version == ServicesWebConstante::SW_VERSION_1
                || $this->version == ServicesWebConstante::SW_VERSION_2
                || $this->version == ServicesWebConstante::SW_VERSION_3);
    }

    /**
     * Permet de vérifier que le sous-service est correct
     *
     * @return bool
     */
    private function verifieSousService()
    {
        $this->sousService = $this->getEvent()->getRouteMatch()->getParam('sous_service');
        return ($this->sousService == ServicesWebConstante::SW_SOUS_SERVICE_TRAVAUX
            || $this->sousService == ServicesWebConstante::SW_SOUS_SERVICE_CALCULER
            || $this->sousService == ServicesWebConstante::SW_SOUS_SERVICE_SIMULATION);
    }

    /**
     * Permet de retourner le service de documentation des services web
     *
     * @return array|object|DocumentationService
     */
    private function getDocumentationService()
    {
        if (empty($this->documentationService)) {
            $this->documentationService = $this->serviceLocator->get(DocumentationService::SERVICE_NAME);
        }
        return $this->documentationService;
    }
}
