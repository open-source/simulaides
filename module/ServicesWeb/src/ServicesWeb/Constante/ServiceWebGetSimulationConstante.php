<?php
namespace ServicesWeb\Constante;

/**
 * Classe ServiceWebGetSimulationConstante
 *
 * Projet : SimulAides 2015-2015
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   ServicesWeb\Constante
 */
class ServiceWebGetSimulationConstante
{
    /** Numéro de la simulation */
    const PRM_NUMERO = 'numero';

    /** Code d'accès de la simulation */
    const PRM_CODE_ACCES = 'code_acces';

    /**
     * Retourne la documentation à afficher pour le paramètre
     *
     * @param string $param Code du paramètre
     *
     * @return string
     */
    public static function getDocumentationParam($param)
    {
        $documentation = '';
        switch ($param) {
            case self::PRM_NUMERO:
                $documentation = 'Numéro de la simulation';
                break;
            case self::PRM_CODE_ACCES:
                $documentation = "Code d'accès de la simulation";
                break;
        }
        return $documentation;
    }

    /**
     * Retourne la liste des paramètres du service
     *
     * @return array
     */
    public static function getListeParametreSimulation()
    {
        $tabParam = [
            self::PRM_NUMERO,
            self::PRM_CODE_ACCES
        ];

        return $tabParam;
    }
}
