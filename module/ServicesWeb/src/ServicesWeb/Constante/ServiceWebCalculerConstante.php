<?php
namespace ServicesWeb\Constante;

/**
 * Classe ServiceWebCalculerConstante
 * Constante pour les param d'entrée
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   ServicesWeb\Constante
 */
class ServiceWebCalculerConstante
{
    /** Mon logement */
    const PRM_MON_LOGEMENT = 'mon_logement';

    /** Mon foyer */
    const PRM_MON_FOYER = 'mon_foyer';

    /** Travaux */
    const PRM_TRAVAUX = 'travaux';

    /** Produits */
    const PRM_PRODUITS = 'produits';

    /** Paramères technique */
    const PRM_PARAM_TECH = 'param_technique';

    /** Commune */
    const PRM_CODE_POSTAL = 'code_postal';

    /** Commune */
    const PRM_COMMUNE = 'commune';

    /** Type de logement */
    const PRM_TYPE_LOGEMENT = 'type_logement';

    /** Année de construction */
    const PRM_ANNEE_CONSTRUCT = 'annee_construction';

    /** Surface habitable */
    const PRM_SURF_HABITABLE = 'surface_habitable';

    /** Energie de chauffage */
    const PRM_ENERGIE_CHAUFF = 'energie_chauffage';

    /** Nombre d'adulte */
    const PRM_NB_ADULTE = 'nombre_adultes';

    /** Nombre de personne à charge */
    const PRM_NB_PERS_CHARGE = 'nombre_personnes_charge';

    /** Nombre d'enfant en garde alternée */
    const PRM_NB_ENF_GARDE_ALTERNEE = 'nombre_enfants_garde_alternee';

    /** Nombre de parts fiscales */
    const PRM_NB_PARTS_FISCALES = 'nb_parts_fiscales';

    /** Statut */
    const PRM_STATUT = 'statut';

    /** Revenu fiscal */
    const PRM_REVENU_FISCAL = 'revenu_fiscal_annee_n_moins_2';

    /** Bénéficie prêt taux zéro */
    const PRM_BENEFICIE_PTZ = 'beneficie_ptz_5_dernieres_annees';

    /** Montant crédit d'impot */
    const PRM_MT_CDT_IMPOT = 'montant_credit_impot_5_dernieres_annees';

    /** Projet rénovation BBC */
    const PRM_PRJ_RENOVATION_BBC = 'projet_renovation_bbc';

    /** Primo Accédant */
    const PRM_PRIMO_ACCESSION = 'primo_accession';

    /** code du travaux */
    const PRM_TRVX_CODE = 'code_travaux';

    /** code du produit */
    const PRM_PDT_CODE = 'code_produit';

    /** Montant total HT */
    const PRM_PDT_MT_TOTAL_HT = 'montant_total_ht';

    /** Montant fo HT */
    const PRM_PDT_MT_FO_HT = 'montant_fourniture_ht';

    /** Montant MO HT */
    const PRM_PDT_MT_MO_HT = 'montant_main-oeuvre_ht';

    /** Code du param technique */
    const PRM_PARAM_TECH_CODE = 'code_param';

    /** Valeur du param technique */
    const PRM_PARAM_TECH_VALEUR = 'valeur_param';

    /** code de la simulation */
    const RESULT_CODE_SIMUL = 'code_simulation';

    /** Mot de passe de la simulation */
    const RESULT_PASSWORD = 'mdp_simulation';

    /** Texte d'aide */
    const RESULT_TEXT_AIDE = 'texte_aide';

    /** Pourcentage global de l'aide */
    const RESULT_POURCENT_AIDE = 'pourcentage_global_aide';

    /** Montant de l'aide total */
    const MONTANT_AIDE_TOTAL = 'montant_aide_total';

    /** Reste à charge */
    const RESTE_A_CHARGE = 'reste_a_charge';

    /** Dispositif */
    const RESULT_DISPOSITIF = 'dispositif';

    /** Intitulé du dispositif */
    const RESULT_DISP_INTITULE = 'intitule';

    /** Descriptif du dispositif */
    const RESULT_DISP_DESCR = 'descriptif';

    /** Financeur du dispositif */
    const RESULT_FINANCEUR = 'financeur';

    /** Evaluer du dispositif */
    const RESULT_EVAL_REGLE = 'evaluer_regle';

    /** Travaux eligibles */
    const RESULT_TRV_ELIGIBLES = 'travaux_eligibles';

    /** Montant de l'aide */
    const MONTANT_AIDE = 'montant_aide';

    /** Cout hors taxe */
    const COUT_HT = 'cout_ht';

    /** Code du travaux */
    const RESULT_TRV_CODE = 'code_travaux';

    /** Intitulé du travaux */
    const RESULT_TRV_INTITULE = 'intitule_travaux';

    /** Critères déligibilité du travaux */
    const RESULT_TRV_CRIT_ELIGIBLE = 'criteres_specifiques_eligibilite';

    /** Rappel de précaution */
    const RESULT_RAPPEL_PRECAUTION = 'rappel_precaution';

    /** Conseiller à contacter */
    const CONTACT = 'contact';

    /**
     * Retourne la documentation à afficher pour le paramètre
     *
     * @param $param
     *
     * @return string
     */
    public static function getDocumentationParam($param)
    {
        $documentation = '';
        switch ($param) {
            case self::PRM_COMMUNE:
                $documentation = 'code commune INSEE';
                break;
            case self::PRM_TYPE_LOGEMENT:
                $documentation = 'M = maison; A = appartement';
                break;
            case self::PRM_ANNEE_CONSTRUCT:
                $documentation = 'format AAAA';
                break;
            case self::PRM_SURF_HABITABLE:
                $documentation = 'la surface habitable en m²';
                break;
            case self::PRM_ENERGIE_CHAUFF:
                $documentation = 'RESEAU = Réseau de chaleur collectif;
                      GAZ_VILLE = Gaz de ville (gaz naturel);
                      FIOUL = Fioul;
                      GAZ_BOUTEILLE = Gaz butane ou propane en bouteille;
                      GAZ_CITERNE = Gaz butane ou propane en citerne;
                      CHARBON = Charbon;
                      BOIS_BUCHES = Bois-bûches;
                      BOIS_GRANULES = Bois-granulés ou plaquettes;
                      ELECTRICITE = Électricité;
                      SOLAIRE = Énergie solaire;
                      GEOTHERMIE = Géothermie';
                break;
            case self::PRM_NB_ADULTE:
                $documentation = 'entier > 0';
                break;
            case self::PRM_NB_PERS_CHARGE:
                $documentation = 'entier >= 0';
                break;
            case self::PRM_NB_ENF_GARDE_ALTERNEE:
                $documentation = 'entier >= 0';
                break;
            case self::PRM_STATUT:
                $documentation = '"PROP_RES_1" = Propriétaire (résidence principale),
                                  "PROP_RES_2" = Propriétaire (résidence secondaire),
                                  "PROP_BAIL" = Propriétaire bailleur,
                                  "LOC" = Locataire,
                                  "OCC_GRAT" = Occupant à titre gratuit,
                                  "PROP_RES_1_SCI" = Propriétaire occupant membre d\'une SCI,
                                  "PROP_BAIL_SCI" = Propriétaire bailleur membre d\'une SCI';
                break;
            case self::PRM_REVENU_FISCAL:
                $documentation = 'valeur entière en €';
                break;
            case self::PRM_BENEFICIE_PTZ:
            case self::PRM_PRIMO_ACCESSION:
            case self::PRM_PRJ_RENOVATION_BBC:
                $documentation = '1 = oui; 0 = non';
                break;
            case self::PRM_MT_CDT_IMPOT:
                $documentation = 'valeur entière en €';
                break;
        }
        return $documentation;
    }

    /**
     * Retourne la liste des paramètres à traiter en entrée du service
     * pour les données logement
     *
     * @return array
     */
    public static function getListeParametreLogement()
    {
        $tabParam = [
            self::PRM_COMMUNE,
            self::PRM_TYPE_LOGEMENT,
            self::PRM_ANNEE_CONSTRUCT,
            self::PRM_SURF_HABITABLE,
            self::PRM_ENERGIE_CHAUFF,
        ];

        return $tabParam;
    }

    /**
     * Retourne la liste des paramètres à traiter en entrée du service
     * pour les données foyer
     *
     * @return array
     */
    public static function getListeParametreFoyer()
    {
        $tabParam = [
            self::PRM_NB_ADULTE,
            self::PRM_NB_PERS_CHARGE,
            self::PRM_NB_ENF_GARDE_ALTERNEE,
            self::PRM_NB_PARTS_FISCALES,
            self::PRM_STATUT,
            self::PRM_REVENU_FISCAL,
            self::PRM_BENEFICIE_PTZ,
            self::PRM_PRIMO_ACCESSION,
            self::PRM_MT_CDT_IMPOT,
            self::PRM_PRJ_RENOVATION_BBC
        ];

        return $tabParam;
    }

    /**
     * Retourne la liste des paramètres à traiter en entrée du service
     * pour les données produit
     *
     * @return array
     */
    public static function getListeParametreProduit()
    {
        $tabParam = [
            self::PRM_PDT_CODE,
            self::PRM_PDT_MT_TOTAL_HT,
            self::PRM_PDT_MT_FO_HT,
            self::PRM_PDT_MT_MO_HT
        ];

        return $tabParam;
    }

    /**
     * Retourne la liste des paramètres à traiter en entrée du service
     * pour les données travaux
     *
     * @return array
     */
    public static function getListeParametreTravaux()
    {
        $tabParam = [
            self::PRM_TRVX_CODE,
        ];

        return $tabParam;
    }

    /**
     * Retourne la liste des paramètres à traiter en entrée du service
     * pour les données param technique
     *
     * @return array
     */
    public static function getListeParametreParamTech()
    {
        $tabParam = [
            self::PRM_PARAM_TECH_CODE,
            self::PRM_PARAM_TECH_VALEUR
        ];

        return $tabParam;
    }
}
