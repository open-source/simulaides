<?php
namespace ServicesWeb\Constante;

/**
 * Classe ServicesWebConstante
 * Constantes liées aux services web
 *
 * Projet : SimulAides 2015-2015
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   ServicesWeb\src\ServicesWeb\Constante
 */
class ServicesWebConstante
{
    /** Version 1 */
    const SW_VERSION_1 = 'v1';

    /** Version 2 */
    const SW_VERSION_2 = 'v2';

    /** Version 3 */
    const SW_VERSION_3 = 'v3';

    /** Sous service : get_travaux */
    const SW_SOUS_SERVICE_TRAVAUX = 'get_travaux';

    /** Sous service : calculer */
    const SW_SOUS_SERVICE_CALCULER = 'calculer';

    /** Sous service : get_simulation */
    const SW_SOUS_SERVICE_SIMULATION = 'get_simulation';

    /** Clé de sécurisation */
    const PRM_CLE_SECURISATION = 'cle_securisation';
}
