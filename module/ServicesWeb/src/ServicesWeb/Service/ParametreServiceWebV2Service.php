<?php
namespace ServicesWeb\Service;

use ServicesWeb\Constante\ServiceWebV2CalculerConstante;
use ServicesWeb\Constante\ServiceWebGetSimulationConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\ParametreTech;
use Simulaides\Domain\Entity\Produit;
use Simulaides\Service\AbstractEntityService;
use Simulaides\Service\LocalisationService;
use Simulaides\Service\ProduitService;
use Simulaides\Service\TravauxService;
use Simulaides\Service\ValeurListeService;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ParametreServiceWebService
 * Service dédié à la gestion des paramètres d'entrée des services web
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   ServicesWeb\Service
 */
class ParametreServiceWebV2Service extends AbstractEntityService
{
    /** Nom du service */
    const SERVICE_NAME = 'ParametreServiceWebV2Service';

    /** @var  ServiceLocatorInterface */
    private $serviceLocator;

    /** @var  LocalisationService */
    private $localisationService;

    /** @var  ValeurListeService */
    private $valeurListeService;

    /** @var  TravauxService */
    private $travauxService;

    /** @var  ProduitService */
    private $produitService;

    /**
     * Constructeur du service
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Permet de vérifier la liste des paramètres
     * de la section Mon logement et Mon foyer
     *
     * @param array $params Liste des pramètres du service web
     *
     * @return bool
     * @throws \Exception
     */
    public function verifieListeParamFoyerLogementServiceWebCalculer(&$params)
    {
        if (!isset($params[ServiceWebV2CalculerConstante::PRM_MON_LOGEMENT])) {
            throw new \Exception("Les informations mon_logement sont manquantes.");
        }
        if (!isset($params[ServiceWebV2CalculerConstante::PRM_MON_FOYER])) {
            throw new \Exception("Les informations mon_foyer sont manquantes.");
        }

        $paramLogement = $params[ServiceWebV2CalculerConstante::PRM_MON_LOGEMENT];
        if(isset($paramLogement[ServiceWebV2CalculerConstante::PRM_COMMUNE])){
            if($this->getLocalisationService()->estOutreMer($paramLogement[ServiceWebV2CalculerConstante::PRM_COMMUNE])){
                $paramLogement[ServiceWebV2CalculerConstante::PRM_ENERGIE_CHAUFF] = ValeurListeConstante::ENERG_ELECTRICITE;
                $params[ServiceWebV2CalculerConstante::PRM_MON_LOGEMENT] = $paramLogement;
            }
        }
        //Vérification de la liste des paramètres de "mon_logement"
        foreach (ServiceWebV2CalculerConstante::getListeParametreLogement() as $codeParam) {
            $this->verifieParamServiceWebCalculer(
                $params[ServiceWebV2CalculerConstante::PRM_MON_LOGEMENT],
                $codeParam,
                true
            );
        }
        //Vérification de la liste des paramètres de "mon_foyer"
        foreach (ServiceWebV2CalculerConstante::getListeParametreFoyer() as $codeParam) {
            $this->verifieParamServiceWebCalculer(
                $params[ServiceWebV2CalculerConstante::PRM_MON_FOYER],
                $codeParam,
                true
            );
        }
        return true;
    }

    /**
     * Permet de vérifier que la liste attendue est présente et renseignée
     *
     * @param array  $params       Liste des paramètres
     * @param string $intituleCrit Intitulé du libellé de l'erreur
     * @param string $codeCrit     Code du critère de la liste à vérifier
     *
     * @return bool
     * @throws \Exception
     */
    public function verifieListeParamListeElementsServiceWebCalculer($params, $intituleCrit, $codeCrit)
    {
        //Vérification que le paramètre soit dans la liste
        if (!isset($params[$codeCrit])) {
            throw new \Exception("Le paramètre sur le " . $intituleCrit . " est manquant.");
        }
        //Vérification que la valeur du paramètre soit renseignée
        if (empty($params[$codeCrit])) {
            throw new \Exception("Le choix d'au moins un " . $codeCrit . " est obligatoire");
        }
        return true;
    }

    /**
     * Permet de vérifier un paramètre de la liste des travaux
     *
     * @param $params
     * @param $codeParam
     * @param $required
     *
     * @return bool
     */
    public function verifieParamTravauxServiceWebCalculer($params, $codeParam, $required)
    {
        //Vérification du paramètre du service web Calculer
        $this->verifieParamServiceWebCalculer($params, $codeParam, $required);
        return true;
    }

    /**
     * Permet de vérifier un paramètre de la liste des produits
     *
     * @param $codeTravaux
     * @param $params
     * @param $estCommuneAVerifier
     * @return bool
     * @throws \Exception
     */
    public function verifieParamProduitServiceWebCalculer($codeTravaux, $params,$estCommuneAVerifier = array())
    {
        //Vérification des informations du produit
        /** @var Produit $produit */
        $produit     = $this->verifieProduit($codeTravaux, $params);
        $codeProduit = $produit->getCode();

        //Vérification des montants saisis
        $estSaisiePrix = $this->verifieMontantsProduit($codeProduit, $params);

        //Vérification de la saisie des paramètres techniques du produit
        $estOK = $this->verifieParamTechProduit($produit, $params, $estSaisiePrix,$estCommuneAVerifier);

        return $estOK;
    }

    /**
     * Vérification que les param tech sont bien saisis
     *
     * @param Produit $produit       Produit sur lequel on est
     * @param array   $params        Liste des informations du produit
     * @param boolean $estSaisiePrix Indique si au moins un prix a été saisi
     * @param array $estCommuneAVerifier Indique si au moins un prix a été saisie
     *
     * @return bool
     * @throws \Exception
     */
    private function verifieParamTechProduit($produit, $params, $estSaisiePrix, $estCommuneAVerifier=array())
    {
        //Savoir si tous les paramètres ont été saisis
        $estSaisieParamTech    = true;
        $estToutRenseigneParam = true;
        $paramsParamTech       = [];
        $estOK                 = true;
        $codeProduit           = $produit->getCode();
        $codeTraveaux           = $produit->getCodeTravaux();

        //Récupération des param tech du produit
        $tabParamTech = $produit->getParamsTech();
        if (isset($params[ServiceWebV2CalculerConstante::PRM_PARAM_TECH])) {
            $paramsParamTech = $params[ServiceWebV2CalculerConstante::PRM_PARAM_TECH];
        }

        if(count($estCommuneAVerifier) > 0
           && isset($estCommuneAVerifier[$codeTraveaux])
           && $estCommuneAVerifier[$codeTraveaux] == false)
        {
            $tabParamTech = array();
            $paramsParamTech = array();
        }

        //Si le produit possède des paramètres techniques, vérification de la saisie pour chacun d'entre eux
        if (!empty($tabParamTech) && $tabParamTech->count() > 0) {
            $estSaisieParamTech = false;
            /** @var ParametreTech $paramTech */
            foreach ($tabParamTech as $paramTech) {
                $estRenseigneParam = false;
                //Recherche du paramètre dans les param saisis en entrée
                foreach ($paramsParamTech as $prmTechSaisie) {
                    if (isset($prmTechSaisie[ServiceWebV2CalculerConstante::PRM_PARAM_TECH_CODE])) {
                        if ($prmTechSaisie[ServiceWebV2CalculerConstante::PRM_PARAM_TECH_CODE] == $paramTech->getCode()) {
                            if (isset($prmTechSaisie[ServiceWebV2CalculerConstante::PRM_PARAM_TECH_VALEUR])) {
                                $valeur    = $prmTechSaisie[ServiceWebV2CalculerConstante::PRM_PARAM_TECH_VALEUR];
                                $codeListe = $paramTech->getCodeListe();
                                if (!empty($codeListe)) {
                                    //Vérification que la valeur corresponde à la liste des valeurs possibles
                                    $valeurListe = $this->getValeurListeService()->getValeurPourCodeListeEtCode(
                                        $codeListe,
                                        $valeur
                                    );
                                    if (empty($valeurListe)) {
                                        //La valeur indiquée ne fait pas partie de la liste possible
                                        $msg = "La valeur " . $valeur . " ne fait pas partie ";
                                        $msg .= "de la liste des valeurs du paramètre " . $paramTech->getCode();
                                        throw new \Exception($msg);
                                    }
                                }else{
                                    if($paramTech->getTypeData() == 'ENTIER'){
                                        if(!is_numeric($valeur)){
                                            //La valeur indiquée ne fait pas partie de la liste possible
                                            $msg = "Le paramètre technique ".$paramTech->getCode()." doit être en entier naturel.";
                                            throw new \Exception($msg);
                                        }
                                    }
                                }
                                $estRenseigneParam = !empty($valeur);
                            }else{
                                //La valeur indiquée ne fait pas partie de la liste possible
                                $msg = "Une valeur  doit être renseignée pour le paramètre technique ".$paramTech->getCode()."";
                                throw new \Exception($msg);
                            }
                            //Le paramètre a été trouvé dans la saisie, on passe au suivant du produit
                            break;
                        }
                    }
                }
                if (!$estRenseigneParam) {
                    //Tous les paramètres doivent être renseignés
                    $estToutRenseigneParam = false;
                } else {
                    //On mémorise qu'au moins un param tech est renseigné
                    if (!$estSaisieParamTech) {
                        $estSaisieParamTech = true;
                    }
                }
            }
        }
        if(!$estToutRenseigneParam) {
            //Si au moins un param tech a été saisi, tous les param doivent avoir été saisis
            $msg = "Tous les paramètres techniques du produit " . $codeProduit . " doivent être renseignés";
            throw new \Exception($msg);

        }else if (!$estSaisiePrix && !$estSaisieParamTech) {
            //si aucun prix et aucun param tech, le produit n'est pas prix en compte
            $estOK = false;
        } elseif ($estSaisiePrix && !$estToutRenseigneParam) {
            //si au moins un prix a été renseigné, les param tech doivent être renseignés aussi
            $msg = "Au moins un prix est renseigné, tous les paramètres techniques du produit ";
            $msg .= $codeProduit . " doivent être renseignés";
            throw new \Exception($msg);
        } elseif ($estSaisieParamTech && !$estToutRenseigneParam) {
            //Si au moins un param tech a été saisi, tous les param doivent avoir été saisis
            $msg = "Tous les paramètres techniques du produit " . $codeProduit . " doivent être renseignés";
            throw new \Exception($msg);
        }

        return $estOK;
    }

    /**
     * Permet de vérifier le produit saisi
     *
     * @param string $codeTravaux Code du travaux auquel est ajouté le produit
     * @param array  $params      Liste de informations du produit
     *
     * @return mixed
     * @throws \Exception
     */
    private function verifieProduit($codeTravaux, $params)
    {
        //Vérification que le code produit est renseigné
        if (!isset($params[ServiceWebV2CalculerConstante::PRM_PDT_CODE])) {
            throw new \Exception("Le paramètre " . ServiceWebV2CalculerConstante::PRM_PDT_CODE . " est manquant.");
        }
        //Vérification que le produit existe
        $codeProduit = $params[ServiceWebV2CalculerConstante::PRM_PDT_CODE];
        $produit     = $this->getProduitService()->getProduitParCodeProduit($codeProduit);
        if (empty($produit)) {
            throw new \Exception("Le produit " . $codeProduit . " n'existe pas.");
        }
        //Vérification que le produit existe pour le travaux
        if ($produit->getCodeTravaux() != $codeTravaux) {
            throw new \Exception("Le produit " . $codeProduit . " n'appartient pas au tavaux " . $codeTravaux);
        }

        return $produit;
    }

    /**
     * Permet de vérifier la saisie des montants des couts du produit
     *
     * @param string $codeProduit Code du produit en cours de vérification
     * @param array  $params      Liste des paramètres du produit
     *
     * @return bool
     * @throws \Exception
     */
    private function verifieMontantsProduit($codeProduit, $params)
    {
        $mtTotal = null;
        $mtFo    = null;
        $mtMo    = null;

        //Vérification de la présence et du format des prix
        $this->verifieMontantFormatProduit($params, ServiceWebV2CalculerConstante::PRM_PDT_MT_TOTAL_HT);
        $this->verifieMontantFormatProduit($params, ServiceWebV2CalculerConstante::PRM_PDT_MT_FO_HT);
        $this->verifieMontantFormatProduit($params, ServiceWebV2CalculerConstante::PRM_PDT_MT_MO_HT);

        if (isset($params[ServiceWebV2CalculerConstante::PRM_PDT_MT_TOTAL_HT])) {
            $mtTotal = $params[ServiceWebV2CalculerConstante::PRM_PDT_MT_TOTAL_HT];
        }
        if (isset($params[ServiceWebV2CalculerConstante::PRM_PDT_MT_FO_HT])) {
            $mtFo = $params[ServiceWebV2CalculerConstante::PRM_PDT_MT_FO_HT];
        }
        if (isset($params[ServiceWebV2CalculerConstante::PRM_PDT_MT_MO_HT])) {
            $mtMo = $params[ServiceWebV2CalculerConstante::PRM_PDT_MT_MO_HT];
        }

        //Savoir si un prix a été saisi => ce qui voudrait dire que le produit doit être ajouté
        $estSaisiePrix = (!empty($mtFo) || !empty($mtTotal) || !empty($mtMo));
        if ($estSaisiePrix) {
            if (!empty($mtTotal) && (!empty($mtFo)) && (!empty($mtMo))) {
                if ($mtFo + $mtMo != $mtTotal) {
                    $msg = "Le coût total du produit " . $codeProduit;
                    $msg .= " doit être égal à la somme des coûts de main d’œuvre et de fourniture.";
                    throw new \Exception($msg);
                }
            } elseif (!empty($mtTotal) && (!empty($mtFo))) {
                if ($mtTotal < $mtFo) {
                    $msg = "Le coût total du produit " . $codeProduit;
                    $msg .= " doit être supérieur au cout de fourniture.";
                    throw new \Exception($msg);
                }
            } elseif (!empty($mtTotal) && (!empty($mtMo))) {
                if ($mtTotal < $mtMo) {
                    $msg = "Le coût total du produit " . $codeProduit;
                    $msg .= " doit être supérieur au cout de main d'oeuvre.";
                    throw new \Exception($msg);
                }
            }
        }

        return $estSaisiePrix;
    }

    /**
     * Permet de vérifier la saisie des prix de montant de produit
     *
     * @param $params
     * @param $codeParam
     *
     * @return bool
     * @throws \Exception
     */
    private function verifieMontantFormatProduit($params, $codeParam)
    {
        if (isset($params[$codeParam])) {
            $montant = $params[$codeParam];
            if (!is_float($montant) && !is_numeric($montant)) {
                throw new \Exception("Le montant " . $montant . " n'est pas numérique.");
            }
            if ($montant <= 0) {
                throw new \Exception("Le montant " . $montant . " doit être supérieur à 0 ou non renseigné.");
            }
        }

        return true;
    }

    /**
     * Permet de vérifier le paramètre d'entrée
     *
     * @param array   $params    Liste des paramètres d'entrée du service web
     * @param string  $codeParam Code du paramètre recherché
     * @param boolean $required  Indique si la valeur est obligatoire
     *
     * @return bool
     * @throws \Exception
     */
    private function verifieParamServiceWebCalculer($params, $codeParam, $required)
    {
        if (!isset($params[$codeParam])) {
            throw new \Exception("Le paramètre " . $codeParam . " est manquant.");
        } else {
            $valParam = $params[$codeParam];
            if ($required && is_null($valParam)) {
                throw new \Exception('La valeur du paramètre ' . $codeParam . ' est obligatoire.');
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_COMMUNE) {
            //Vérification que la valeur de la commune est présente en base
            $commune = $this->getLocalisationService()->getCommuneParCode($valParam);
            if (empty($commune)) {
                throw new \Exception("Le code INSEE " . $valParam . " n'existe pas.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_TYPE_LOGEMENT) {
            //Vérification que la valeur du type de logement est M ou A
            if (!$this->verifieParamDansValeurListe(ValeurListeConstante::LISTE_TYPE_LOGEMENT, $valParam)) {
                throw new \Exception("Le type de logement " . $valParam . " n'exise pas.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_ANNEE_CONSTRUCT) {
            //Vérification que l'année est un entier sur 4 caractères et <= l'année en cours
            if (!ctype_digit($valParam)) {
                throw new \Exception("L'année " . $valParam . " n'est pas un entier.");
            }
            if (strlen($valParam) != 4) {
                throw new \Exception("L'année " . $valParam . " n'est pas sur 4 chiffres.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_SURF_HABITABLE) {
            //Vérification que la surface est un entier positif
            $this->verifieParamEntierPositif('La surface habitable', $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_ENERGIE_CHAUFF) {
            if (!$this->verifieParamDansValeurListe(ValeurListeConstante::LISTE_ENERGIE_CHAUFFAGE_DETAIL, $valParam)) {
                throw new \Exception("L'énergie de chauffage principale" . $valParam . " n'est pas correcte.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_NB_PARTS_FISCALES) {
            //Vérification que le nombre de parts fiscales est un entier positif
            $this->verifieParamNombrePositif("Le nombre de parts fiscales", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_NB_ADULTE) {
            //Vérification que le nombre est un entier positif
            $this->verifieParamEntierPositif("Le nombre d'adulte", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_NB_PERS_CHARGE) {
            //Vérification que le nombre est un entier positif
            $this->verifieParamEntierPositifEtZero("Le nombre de personnes à charge", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_NB_ENF_GARDE_ALTERNEE) {
            //Vérification que le nombre est un entier positif
            $this->verifieParamEntierPositifEtZero("Le nombre d'enfants en garde alterné", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_STATUT) {
            if (!$this->verifieParamDansValeurListe(ValeurListeConstante::LISTE_STATUT, $valParam)) {
                throw new \Exception("Le statut " . $valParam . " n'est pas correct.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_REVENU_FISCAL) {
            //Vérification que le nombre est un nombre positif
            $this->verifieParamNombrePositif("Le revenu fiscal", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_BENEFICIE_PTZ) {
            //Vérification que la valeur est 1/0
            if (!$this->verifieParamBooleen($valParam)) {
                throw new \Exception("La valeur de bénéficie prêt à taux zéro " . $valParam . " n'est pas 0 ou 1.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_PRIMO_ACCESSION) {
            //Vérification que la valeur est 1/0
            if (!$this->verifieParamBooleen($valParam)) {
                throw new \Exception("La valeur de primo-accédant " . $valParam . " n'est pas 0 ou 1.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_SALARIE_SECTEUR_PRIVE) {
            //Vérification que la valeur est 1/0
            if (!$this->verifieParamBooleen($valParam)) {
                throw new \Exception("La valeur de salarie-secteur-prive " . $valParam . " n'est pas 0 ou 1.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_PRJ_RENOVATION_BBC) {
            //Vérification que la valeur est 1/0
            if (!$this->verifieParamBooleen($valParam)) {
                throw new \Exception("La valeur de projet rénovation BBC " . $valParam . " n'est pas 0 ou 1.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_MT_CDT_IMPOT) {
            //Vérification que le nombre est un entier positif
            $this->verifieParamEntierPositif("Le montant du crédit d'impôt", $valParam, true);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_TRVX_CODE) {
            //Vérification que le code du travaux existe
            $travaux = $this->getTravauxService()->getTravauxByCode($valParam);
            if (empty($travaux)) {
                throw new \Exception("Le code du type de travaux " . $valParam . " n'exise pas.");
            }
        }

        return true;
    }
    /**
     * Permet de vérifier le paramètre d'entrée v2
     *
     * @param array   $params    Liste des paramètres d'entrée du service web
     * @param string  $codeParam Code du paramètre recherché
     * @param boolean $required  Indique si la valeur est obligatoire
     *
     * @return bool
     * @throws \Exception
     */
    private function verifieParamServiceWebCalculerV2($params, $codeParam, $required)
    {
        if (!isset($params[$codeParam])) {
            throw new \Exception("Le paramètre " . $codeParam . " est manquant.");
        } else {
            $valParam = $params[$codeParam];
            if ($required && is_null($valParam)) {
                throw new \Exception('La valeur du paramètre ' . $codeParam . ' est obligatoire.');
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_COMMUNE) {
            //Vérification que la valeur de la commune est présente en base
            $commune = $this->getLocalisationService()->getCommuneParCode($valParam);
            if (empty($commune)) {
                throw new \Exception("Le code INSEE " . $valParam . " n'existe pas.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_TYPE_LOGEMENT) {
            //Vérification que la valeur du type de logement est M ou A
            if (!$this->verifieParamDansValeurListe(ValeurListeConstante::LISTE_TYPE_LOGEMENT, $valParam)) {
                throw new \Exception("Le type de logement " . $valParam . " n'exise pas.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_ANNEE_CONSTRUCT) {
            //Vérification que l'année est un entier sur 4 caractères et <= l'année en cours
            if (!ctype_digit($valParam)) {
                throw new \Exception("L'année " . $valParam . " n'est pas un entier.");
            }
            if (strlen($valParam) != 4) {
                throw new \Exception("L'année " . $valParam . " n'est pas sur 4 chiffres.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_SURF_HABITABLE) {
            //Vérification que la surface est un entier positif
            $this->verifieParamEntierPositif('La surface habitable', $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_ENERGIE_CHAUFF) {
            if (!$this->verifieParamDansValeurListe(ValeurListeConstante::LISTE_ENERGIE_CHAUFFAGE_DETAIL, $valParam)) {
                throw new \Exception("L'énergie de chauffage principale" . $valParam . " n'est pas correcte.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_NB_PARTS_FISCALES) {
            //Vérification que le nombre de parts fiscales est un entier positif
            $this->verifieParamNombrePositif("Le nombre de parts fiscales", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_NB_ADULTE) {
            //Vérification que le nombre est un entier positif
            $this->verifieParamEntierPositif("Le nombre d'adulte", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_NB_PERS_CHARGE) {
            //Vérification que le nombre est un entier positif
            $this->verifieParamEntierPositifEtZero("Le nombre de personnes à charge", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_NB_ENF_GARDE_ALTERNEE) {
            //Vérification que le nombre est un entier positif
            $this->verifieParamEntierPositifEtZero("Le nombre d'enfants en garde alterné", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_STATUT) {
            if (!$this->verifieParamDansValeurListe(ValeurListeConstante::LISTE_STATUT, $valParam)) {
                throw new \Exception("Le statut " . $valParam . " n'est pas correct.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_REVENU_FISCAL) {
            //Vérification que le nombre est un nombre positif
            $this->verifieParamNombrePositif("Le revenu fiscal", $valParam);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_BENEFICIE_PTZ) {
            //Vérification que la valeur est 1/0
            if (!$this->verifieParamBooleen($valParam)) {
                throw new \Exception("La valeur de bénéficie prêt à taux zéro " . $valParam . " n'est pas 0 ou 1.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_PRIMO_ACCESSION) {
            //Vérification que la valeur est 1/0
            if (!$this->verifieParamBooleen($valParam)) {
                throw new \Exception("La valeur de primo-accédant " . $valParam . " n'est pas 0 ou 1.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_SALARIE_SECTEUR_PRIVE) {
            //Vérification que la valeur est 1/0
            if (!$this->verifieParamBooleen($valParam)) {
                throw new \Exception("La valeur de salarie-secteur-prive " . $valParam . " n'est pas 0 ou 1.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_PRJ_RENOVATION_BBC) {
            //Vérification que la valeur est 1/0
            if (!$this->verifieParamBooleen($valParam)) {
                throw new \Exception("La valeur de projet rénovation BBC " . $valParam . " n'est pas 0 ou 1.");
            }
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_MT_CDT_IMPOT) {
            //Vérification que le nombre est un entier positif
            $this->verifieParamEntierPositif("Le montant du crédit d'impôt", $valParam, true);
        }

        if ($codeParam == ServiceWebV2CalculerConstante::PRM_TRVX_CODE) {
            //Vérification que le code du travaux existe
            $travaux = $this->getTravauxService()->getTravauxByCode($valParam);
            if (empty($travaux)) {
                throw new \Exception("Le code du type de travaux " . $valParam . " n'exise pas.");
            }
        }

        return true;
    }

    /**
     * Vérifie que le paramètre est booléen
     *
     * @param $valParam
     *
     * @return bool
     */
    private function verifieParamBooleen($valParam)
    {
        return ($valParam == '0' || $valParam == '1');
    }

    /**
     * Vérifie que la valeur du paramètre est un entier positif
     *
     * @param string  $libelleParam Libellé du paramètre
     * @param string  $valParam     Valeur du paramètre
     * @param boolean $egalZero     Indique si la valeur peut être égale à 0
     *
     * @throws \Exception
     */
    private function verifieParamEntierPositif($libelleParam, $valParam, $egalZero = false)
    {
        if (!ctype_digit($valParam)) {
            throw new \Exception($libelleParam . ' ' . $valParam . " n'est pas un entier.");
        }
        if (!$egalZero && intval($valParam) < 1) {
            throw new \Exception($libelleParam . ' ' . $valParam . " n'est pas > 0.");
        }
        if ($egalZero && intval($valParam) < 0) {
            throw new \Exception($libelleParam . ' ' . $valParam . " n'est pas >= 0.");
        }
    }

    /**
     * Vérifie que la valeur du paramètre est un nombre positif
     *
     * @param string  $libelleParam Libellé du paramètre
     * @param string  $valParam     Valeur du paramètre
     * @param boolean $egalZero     Indique si la valeur peut être égale à 0
     *
     * @throws \Exception
     */
    private function verifieParamNombrePositif($libelleParam, $valParam, $egalZero = false)
    {
        if (!is_numeric ($valParam)) {
            throw new \Exception($libelleParam . ' ' . $valParam . " n'est pas un nombre.");
        }
        if (!$egalZero && intval($valParam) < 1) {
            throw new \Exception($libelleParam . ' ' . $valParam . " n'est pas > 0.");
        }
        if ($egalZero && intval($valParam) < 0) {
            throw new \Exception($libelleParam . ' ' . $valParam . " n'est pas >= 0.");
        }
    }

    /**
     * Vérifie que la valeur du paramètre est un entier positif ouégale à 0
     *
     * @param string $libelleParam Libellé du paramètre
     * @param string $valParam     Valeur du paramètre
     *
     * @throws \Exception
     */
    private function verifieParamEntierPositifEtZero($libelleParam, $valParam)
    {
        if (!ctype_digit($valParam)) {
            throw new \Exception($libelleParam . ' ' . $valParam . " n'est pas un entier.");
        }
        if (intval($valParam) < 0) {
            throw new \Exception($libelleParam . ' ' . $valParam . " n'est pas >= 0.");
        }
    }

    /**
     * Vérifie que la valeur du paramètre fait parti de la liste
     *
     * @param string $codeListe Code de la liste
     * @param string $valParam  Valeur de la liste
     *
     * @return bool
     */
    private function verifieParamDansValeurListe($codeListe, $valParam)
    {
        $valeurListe = $this->getValeurListeService()->getValeurPourCodeListeEtCode($codeListe, $valParam);
        return !empty($valeurListe);
    }

    /**
     * Permet de vérifier les paramètres d'entrée du service get_simulation
     *
     * @param array $params Liste des param d'entrée du service
     *
     * @return bool
     * @throws \Exception
     */
    public function verifieParamServiceWebGetSimulation($params)
    {
        if (!isset($params[ServiceWebGetSimulationConstante::PRM_NUMERO])) {
            throw new \Exception("Le numéro de la simulation est obligatoire");
        }

        if (!isset($params[ServiceWebGetSimulationConstante::PRM_CODE_ACCES])) {
            throw new \Exception("Le code d'accès est obligatoire");
        }
        return true;
    }

    /**
     * Permet de récupérer le service sur la localisation
     *
     * @return array|object|LocalisationService
     */
    private function getLocalisationService()
    {
        if (empty($this->localisationService)) {
            $this->localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
        }
        return $this->localisationService;
    }

    /**
     * Permet de récupérer le service sur les listes de valeur
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
        }
        return $this->valeurListeService;
    }

    /**
     * Permet de récupérer le service sur les travaux
     *
     * @return array|object|TravauxService
     */
    private function getTravauxService()
    {
        if (empty($this->travauxService)) {
            $this->travauxService = $this->serviceLocator->get(TravauxService::SERVICE_NAME);
        }
        return $this->travauxService;
    }

    /**
     * Permet de récupérer le service sur les travaux
     *
     * @return array|object|ProduitService
     */
    private function getProduitService()
    {
        if (empty($this->produitService)) {
            $this->produitService = $this->serviceLocator->get(ProduitService::SERVICE_NAME);
        }
        return $this->produitService;
    }
}
