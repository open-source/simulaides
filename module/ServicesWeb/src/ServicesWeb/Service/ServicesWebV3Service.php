<?php
namespace ServicesWeb\Service;

use ServicesWeb\Constante\ServiceWebV3CalculerConstante;
use ServicesWeb\Constante\ServiceWebGetSimulationConstante;
use Simulaides\Constante\GroupeTrancheConstante;
use Simulaides\Constante\MainConstante;
use Simulaides\Constante\SessionConstante;
use Simulaides\Constante\SimulationConstante;
use Simulaides\Constante\TravauxConstante;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\Projet;
use Simulaides\Domain\Entity\SaisieProduit;
use Simulaides\Domain\Entity\SaisieProduitMontant;
use Simulaides\Domain\Entity\SaisieProduitParametreTech;
use Simulaides\Domain\Entity\Travaux;
use Simulaides\Domain\Repository\CodePostalRepository;
use Simulaides\Domain\SessionObject\ProjetSession;
use Simulaides\Domain\SessionObject\Simulation\DispositifSimul;
use Simulaides\Domain\SessionObject\Simulation\ProjetSimul;
use Simulaides\Domain\SessionObject\Simulation\Simulation;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitCoutSession;
use Simulaides\Domain\SessionObject\Travaux\SaisieProduitParametreTechSession;
use Simulaides\Service\AbstractEntityService;
use Simulaides\Service\LocalisationService;
use Simulaides\Service\ProduitService;
use Simulaides\Service\ProjetService;
use Simulaides\Service\ProjetSimulService;
use Simulaides\Service\SimulationService;
use Simulaides\Service\TravauxService;
use Simulaides\Service\ValeurListeService;
use Simulaides\Tools\Utils;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ServicesWebService
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   ServicesWeb\Service
 */
class ServicesWebV3Service extends AbstractEntityService
{

    /** Nom du service */
    const SERVICE_NAME = 'ServicesWebV3Service';
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /** @var  ParametreServiceWebService */
    private $parametreServiceWebService;

    /** @var  LocalisationService */
    private $localisationService;

    /** @var  TravauxService */
    private $travauxService;

    /** @var  ValeurListeService */
    private $valeurListeService;

    /** @var  ProjetService */
    private $projetService;

    /** @var  ProduitService */
    private $produitService;

    /**
     * Constructeur du service
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Permet de réaliser une simulation  à partir des données d'un service web Calculer
     *
     * @param array $params Liste des paramètres d'entrée
     *
     * @return array
     * @throws \Exception
     */
    public function lanceSimulationPourServiceWeb($params)
    {
        try {
            //Création d'un projet session avec les données du service web
            $projetSession = new ProjetSession();
            $this->saveProjetSessionFromParamServiceWeb($projetSession, $params);
            //Création des listes de travaux avec les données du service web
            $travaux = $this->getTravauxFromParamServiceWeb($params, $projetSession->getCodeTypeLogement(), $projetSession->getCodeCommune());
            //Mémorisation du projet en base
            $this->getProjetService()->saveProjetEnBase($projetSession, $travaux);
            //Récupération de l'identifiant du projet
            $idProjet = $projetSession->getId();

            //2- Appel de la simulation
            /** @var SimulationService $simulationService */
            $simulationService = $this->serviceLocator->get(SimulationService::SERVICE_NAME);
            $simulation        = $simulationService->getSimulation(
                $idProjet,
                SimulationConstante::MODE_PRODUCTION,
                null
            );

            //3- Formatage des données de retour pour le service
            $dataResult = $this->getResultSimulationPourServiceWeb($simulation);

            return $dataResult;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Permet de mettre à jour le projet avec les param du service web
     *
     * @param ProjetSession $projetSession Projet à mettre à jour
     * @param array         $params        Liste des param d'entrée du service web
     */
    private function saveProjetSessionFromParamServiceWeb($projetSession, $params)
    {
        //Vérification des paramètres d'entrée mon foyer / mon logement
        $parametreSwService = $this->getParametreServiceWebService();
        $parametreSwService->verifieListeParamFoyerLogementServiceWebCalculer($params);

        $paramsLogement = $params[ServiceWebV3CalculerConstante::PRM_MON_LOGEMENT];
        $paramsFoyer    = $params[ServiceWebV3CalculerConstante::PRM_MON_FOYER];
        //Mise à jour des informations du projet avec les valeurs des param d'entrée
        $em = $this->serviceLocator->get('Doctrine\ORM\EntityManager');
        /** @var CodePostalRepository $codePostalRep */
        $codePostalRep = $em->getRepository('Simulaides\Domain\Entity\CodePostal');

        if(isset($paramsLogement[ServiceWebV3CalculerConstante::PRM_COMMUNE])){
            $codeInsee = $paramsLogement[ServiceWebV3CalculerConstante::PRM_COMMUNE];

            $codePostal = $codePostalRep->findOneBy(['codeInsee'=>$codeInsee]);
            if($codePostal){
                $projetSession->setCodePostal($codePostal->getCodePostal());
            }
        }

        $projetSession->setCodeCommune($paramsLogement[ServiceWebV3CalculerConstante::PRM_COMMUNE]);
        $projetSession->setCodeTypeLogement($paramsLogement[ServiceWebV3CalculerConstante::PRM_TYPE_LOGEMENT]);
        $projetSession->setAnneeConstruction($paramsLogement[ServiceWebV3CalculerConstante::PRM_ANNEE_CONSTRUCT]);
        $projetSession->setSurfaceHabitable($paramsLogement[ServiceWebV3CalculerConstante::PRM_SURF_HABITABLE]);
        $projetSession->setCodeEnergie($paramsLogement[ServiceWebV3CalculerConstante::PRM_ENERGIE_CHAUFF]);
        if(isset($paramsLogement[ServiceWebV3CalculerConstante::PRM_MODE_CHAUFF])){
            $projetSession->setCodeModeChauff($paramsLogement[ServiceWebV3CalculerConstante::PRM_MODE_CHAUFF]);
        }
        $projetSession->setNbAdultes($paramsFoyer[ServiceWebV3CalculerConstante::PRM_NB_ADULTE]);
        $projetSession->setNbPersonnesCharge($paramsFoyer[ServiceWebV3CalculerConstante::PRM_NB_PERS_CHARGE]);
        $projetSession->setNbEnfantAlterne($paramsFoyer[ServiceWebV3CalculerConstante::PRM_NB_ENF_GARDE_ALTERNEE]);
        $projetSession->setNbPartsFiscales($paramsFoyer[ServiceWebV3CalculerConstante::PRM_NB_PARTS_FISCALES]);
        $projetSession->setCodeStatut($paramsFoyer[ServiceWebV3CalculerConstante::PRM_STATUT]);
        $projetSession->setRevenus($paramsFoyer[ServiceWebV3CalculerConstante::PRM_REVENU_FISCAL]);
        $projetSession->setBeneficePtz($paramsFoyer[ServiceWebV3CalculerConstante::PRM_BENEFICIE_PTZ]);
        $projetSession->setPrimoAccession($paramsFoyer[ServiceWebV3CalculerConstante::PRM_PRIMO_ACCESSION]);
        $projetSession->setSalarieSecteurPrive($paramsFoyer[ServiceWebV3CalculerConstante::PRM_SALARIE_SECTEUR_PRIVE]);
        $projetSession->setMontantBeneficeCi($paramsFoyer[ServiceWebV3CalculerConstante::PRM_MT_CDT_IMPOT]);
        $projetSession->setBbc($paramsFoyer[ServiceWebV3CalculerConstante::PRM_PRJ_RENOVATION_BBC]);
        //Indication de la date d'évaluation
        $projetSession->setDateEval(new \DateTime('now'));
    }

    /**
     * Permet de renseigner les objets de session avec les paramètres d'entrée du service web
     *
     * @param array  $params           Liste des paramètres d'entrée
     * @param string $codeTypeLogement Code du type de logement
     *
     * @return array
     * @throws \Exception
     */
    private function getTravauxFromParamServiceWeb($params, $codeTypeLogement, $codeCommune)
    {
        //Vérification de la liste des travaux
        $parametreSwService = $this->getParametreServiceWebService();
        $parametreSwService->verifieListeParamListeElementsServiceWebCalculer(
            $params,
            "travaux",
            ServiceWebV3CalculerConstante::PRM_TRAVAUX
        );

        $tabTravaux = [
            'travaux'  => [],
            'produits' => [],
            'saisies'  => []
        ];
        $estEnReunion = $this->getLocalisationService()->estLaReunion($codeCommune);
        $estCommuneAVerifier = [TravauxConstante::TRAVAUX_T28_PAN_SOL_PHOTO => $estEnReunion];

        foreach ($params[ServiceWebV3CalculerConstante::PRM_TRAVAUX] as $paramTravaux) {
            //Vérification du paramètre code travaux
            $parametreSwService->verifieParamTravauxServiceWebCalculer(
                $paramTravaux,
                ServiceWebV3CalculerConstante::PRM_TRVX_CODE,
                true
            );
            $codeTravaux = $paramTravaux[ServiceWebV3CalculerConstante::PRM_TRVX_CODE];

            //Vérification que le travaux est compris pour le type de logement
            $travaux                = $this->getTravauxService()->getTravauxByCode($codeTravaux);
            $msg                    = "Ce type de travaux " . $codeTravaux;
            $msg                    = $msg . " n'est pas géré pour le type de logement " . $codeTypeLogement;
            $estLogementMaison      = ($codeTypeLogement == ValeurListeConstante::TYPE_LOGEMENT_CODE_MAISON);
            $estLogementAppartement = ($codeTypeLogement == ValeurListeConstante::TYPE_LOGEMENT_CODE_APPART);
            if ($estLogementMaison && !$travaux->getMaison()) {
                throw new \Exception($msg);
            } elseif ($estLogementAppartement && !$travaux->getAppartement()) {
                throw new \Exception($msg);
            }

            /*$msg = "Ce type de travaux " . $codeTravaux . " n'est pas disponible pour les départements d'Outre-mer.";
            $travauxOutreMer = $travaux->getOutreMer();
            $travauxMetropole = $travaux->getMetropole();
            if(($this->getLocalisationService()->estOutreMer($codeCommune))&&(!$travauxOutreMer)){
                throw new \Exception($msg);
            }

            $msg = "Ce type de travaux " . $codeTravaux . " n'est pas disponible pour la métropole.";

            if((!$this->getLocalisationService()->estOutreMer($codeCommune))&&(!$travauxMetropole)){
                throw new \Exception($msg);
            }*/

            $msg = "Ce type de travaux " . $codeTravaux . " n'est disponible que pour les départements d'Outre-mer.";
            if(!($this->getLocalisationService()->estOutreMer($codeCommune))&&($travaux->getOutreMer())){
                throw new \Exception($msg);
            }

            $tabTravaux['travaux'][] = $codeTravaux;

            $nbProduitAjoute = 0;
            //Parcours des produits du travaux
            if(isset($paramTravaux[ServiceWebV3CalculerConstante::PRM_PRODUITS])) {
                if ($paramTravaux[ServiceWebV3CalculerConstante::PRM_PRODUITS]) {
                    foreach ($paramTravaux[ServiceWebV3CalculerConstante::PRM_PRODUITS] as $paramProduit) {

                        //Vérification de la liste des produits
                        $parametreSwService->verifieListeParamListeElementsServiceWebCalculer(
                            $paramTravaux,
                            "produit",
                            ServiceWebV3CalculerConstante::PRM_PRODUITS
                        );

                        //Vérification des paramètres du produit
                        if ($parametreSwService->verifieParamProduitServiceWebCalculer(
                            $codeTravaux,
                            $paramProduit,
                            $estCommuneAVerifier
                        )) {
                            $nbProduitAjoute++;
                            $codeProduit = $paramProduit[ServiceWebV3CalculerConstante::PRM_PDT_CODE];
                            $tabTravaux['produits'][] = $codeProduit;

                            //Ajout du cout total du produit
                            $tabTravaux['saisies'][] = $this->getSaisieProduitCoutServiceWeb(
                                $paramProduit,
                                $codeProduit,
                                SessionConstante::COUT_TOTAL
                            );

                            //Ajout du cout fourniture du produit
                            $tabTravaux['saisies'][] = $this->getSaisieProduitCoutServiceWeb(
                                $paramProduit,
                                $codeProduit,
                                SessionConstante::COUT_FO
                            );

                            //Ajout du cout main oeuvre du produit
                            $tabTravaux['saisies'][] = $this->getSaisieProduitCoutServiceWeb(
                                $paramProduit,
                                $codeProduit,
                                SessionConstante::COUT_MO
                            );

                            //Ajout des paramètres techniques
                            if (isset($paramProduit[ServiceWebV3CalculerConstante::PRM_PARAM_TECH])) {
                                foreach ($paramProduit[ServiceWebV3CalculerConstante::PRM_PARAM_TECH] as $paramPrmTech) {
                                    $tabTravaux['saisies'][] = $this->getSaisieProduitPrmTechServiceWeb(
                                        $paramPrmTech,
                                        $codeProduit
                                    );
                                }
                            }
                        }
                    }
                }
            }
            if ($nbProduitAjoute == 0) {
                //Le choix d'au moins un produit est obligatoire
                throw new \Exception(
                    "Le choix d'au moins un produit est obligatoire pour le type de travaux " . $codeTravaux
                );
            }
        }
        return $tabTravaux;
    }

    /**
     * Permet de créer un saisieProduitCout à partir des param d'entrée du service web
     *
     * @param $params
     * @param $codeProduit
     * @param $typeCout
     *
     * @return SaisieProduitCoutSession
     */
    private function getSaisieProduitCoutServiceWeb($params, $codeProduit, $typeCout)
    {
        $mtCout     = null;
        $saisieCout = new SaisieProduitCoutSession();
        $saisieCout->setCodeProduit($codeProduit);
        $saisieCout->setTypeCout($typeCout);
        switch ($typeCout) {
            case SessionConstante::COUT_TOTAL:
                if (isset($params[ServiceWebV3CalculerConstante::PRM_PDT_MT_TOTAL_HT])) {
                    $mtCout = $params[ServiceWebV3CalculerConstante::PRM_PDT_MT_TOTAL_HT];
                }
                break;
            case SessionConstante::COUT_MO:
                if (isset($params[ServiceWebV3CalculerConstante::PRM_PDT_MT_MO_HT])) {
                    $mtCout = $params[ServiceWebV3CalculerConstante::PRM_PDT_MT_MO_HT];
                }
                break;
            case SessionConstante::COUT_FO:
                if (isset($params[ServiceWebV3CalculerConstante::PRM_PDT_MT_FO_HT])) {
                    $mtCout = $params[ServiceWebV3CalculerConstante::PRM_PDT_MT_FO_HT];
                }
                break;
        }
        $saisieCout->setCout($mtCout);

        return $saisieCout;
    }

    /**
     * Permet de créer un saisieProduitParamTechSession à partir des param d'entrée du service web
     *
     * @param $params
     * @param $codeProduit
     *
     * @return SaisieProduitParametreTechSession
     */
    private function getSaisieProduitPrmTechServiceWeb($params, $codeProduit)
    {
        $saisieParamTech = new SaisieProduitParametreTechSession();
        $saisieParamTech->setCodeProduit($codeProduit);
        $saisieParamTech->setCodeParametre($params[ServiceWebV3CalculerConstante::PRM_PARAM_TECH_CODE]);
        $saisieParamTech->setValeur($params[ServiceWebV3CalculerConstante::PRM_PARAM_TECH_VALEUR]);

        return $saisieParamTech;
    }

    /**
     * Permet de retourner les résultats de la simulation au format du service web
     *
     * @param Simulation $simulation
     *
     * @return array
     */
    private function getResultSimulationPourServiceWeb($simulation)
    {
        $data = [];

        $projetSimul = $simulation->getProjet();

        //Code et mot de passe de la simulation
        $data[ServiceWebV3CalculerConstante::RESULT_CODE_SIMUL] = $projetSimul->getNumero();
        $data[ServiceWebV3CalculerConstante::RESULT_PASSWORD]   = $projetSimul->getMotPasse();

        //Texte d'aide sur les dispositifs eligibles
        if ($simulation->getExistDispositifElligible()) {
            $textAide = $this->getValeurListeService()->getTextAdministrable(
                ValeurListeConstante::DISPOSITIFS_ELIGIBLES_DETECTES
            );
        } else {
            $textAide = $this->getValeurListeService()->getTextAdministrable(
                ValeurListeConstante::AUCUN_DISPOSITIF_ELIGIBLE
            );
        }
        $data[ServiceWebV3CalculerConstante::RESULT_TEXT_AIDE] = $textAide;

        //Calcul du pourcentage de l'aide
        $coutTotalTravaux = $projetSimul->getCoutTotalTravaux();
        $montantTotal     = $simulation->getMontantTotal();
        $division         = 0;
        if ($coutTotalTravaux > 0) {
            $division = $montantTotal / $coutTotalTravaux;
        }
        $taux                                                    = Utils::formatNumberToString($division * 100, 0);
        $data[ServiceWebV3CalculerConstante::RESULT_POURCENT_AIDE] = $taux;
        $data[ServiceWebV3CalculerConstante::MONTANT_AIDE_TOTAL]   = $montantTotal;
        $data[ServiceWebV3CalculerConstante::RESTE_A_CHARGE]       = $this->getProjetService()->getMontantResteACharge($taux, $montantTotal);

        //Liste des dispositifs d'aide
        $data[ServiceWebV3CalculerConstante::RESULT_DISPOSITIF] = [];
        /** @var DispositifSimul $dispositif */
        foreach ($simulation->getDispositifs() as $dispositif) {
            $infoDispositif = [];
            //Informations du dispositif
            $infoDispositif[ServiceWebV3CalculerConstante::RESULT_DISP_INTITULE] = $dispositif->getIntitule();
            $infoDispositif[ServiceWebV3CalculerConstante::RESULT_DISP_DESCR]    = $dispositif->getDescriptif();
            $infoDispositif[ServiceWebV3CalculerConstante::RESULT_FINANCEUR]     = $dispositif->getFinanceur();
            $infoDispositif[ServiceWebV3CalculerConstante::MONTANT_AIDE]         = $dispositif->getMontant();
            $infoDispositif[ServiceWebV3CalculerConstante::RESULT_EVAL_REGLE]    = $dispositif->getEvaluerRegle();

            foreach ($dispositif->getAidesTravaux() as $aideTravaux) {
                if ($aideTravaux->getMontantAideTo() > 0) {
                    $infoAideTravaux = [];
                    //Information de l'aide du travaux
                    $infoAideTravaux[ServiceWebV3CalculerConstante::RESULT_TRV_CODE]          = $aideTravaux->getCode();
                    $infoAideTravaux[ServiceWebV3CalculerConstante::RESULT_TRV_INTITULE]      = $aideTravaux->getTravaux()->getIntitule();
                    $infoAideTravaux[ServiceWebV3CalculerConstante::RESULT_TRV_CRIT_ELIGIBLE] = $aideTravaux->getEligibiliteSpecifique();
                    $infoAideTravaux[ServiceWebV3CalculerConstante::MONTANT_AIDE]             = $aideTravaux->getMontantAideTo();
                    $infoAideTravaux[ServiceWebV3CalculerConstante::COUT_HT]                  = $aideTravaux->getTravaux()->getCoutHtTo();
                    $infoDispositif[ServiceWebV3CalculerConstante::RESULT_TRV_ELIGIBLES][]    = $infoAideTravaux;
                }
            }

            $data[ServiceWebV3CalculerConstante::RESULT_DISPOSITIF][] = $infoDispositif;
        }
        $data[ServiceWebV3CalculerConstante::CONTACT] = $this->getResultSimulationConseiller($projetSimul);
        //Texte de rappel des précautions
        $textAide                                                    = $this->getValeurListeService()
            ->getTextAdministrable(ValeurListeConstante::RAPPEL_PRECAUTIONS);
        $data[ServiceWebV3CalculerConstante::RESULT_RAPPEL_PRECAUTION] = $textAide;

        return $data;
    }

    /**
     * Permet de retourner les informations d'une simulation
     *
     * @param $params
     *
     * @return mixed
     * @throws \Exception
     */
    public function getSimulationPourServiceWeb($params)
    {
        try {
            //Vérification des paramètres d'entrée
            $this->getParametreServiceWebService()->verifieParamServiceWebGetSimulation($params);

            //Récupération des paramètres
            $numero    = $params[ServiceWebGetSimulationConstante::PRM_NUMERO];
            $codeAcces = $params[ServiceWebGetSimulationConstante::PRM_CODE_ACCES];

            //Vérification que la simulation existe
            $projet = $this->getProjetService()->getProjetParNumeroEtCodeAcces($numero, $codeAcces);
            if (empty($projet)) {
                $msg = "Aucune simulation ne correspond au numéro et au code d’accès que vous avez indiqué.";
                throw new \Exception($msg);
            }

            //Formatage des données de retour
            $dataResult = $this->getResultPourServiceWebSimulation($projet);

            return $dataResult;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Formatage des données de retour du service web Simulation
     *
     * @param Projet $projet Projet à retourner
     *
     * @return array
     */
    private function getResultPourServiceWebSimulation($projet)
    {
        $data = [];

        //Infos "mon_logement"
        $data[ServiceWebV3CalculerConstante::PRM_MON_LOGEMENT] = $this->getResultSimulationLogement($projet);

        //Infos "mon_foyer"
        $data[ServiceWebV3CalculerConstante::PRM_MON_FOYER] = $this->getResultSimulationFoyer($projet);

        //Infos "travaux"
        $data[ServiceWebV3CalculerConstante::PRM_TRAVAUX] = $this->getResultSimulationTravaux($projet);

        return $data;
    }

    /**
     * Retourne le retour des données logement
     *
     * @param Projet $projet projet à retourner
     *
     * @return array
     */
    private function getResultSimulationLogement($projet)
    {
        $dataResult = [
            ServiceWebV3CalculerConstante::PRM_COMMUNE         => $projet->getCodeCommune(),
            ServiceWebV3CalculerConstante::PRM_TYPE_LOGEMENT   => $projet->getCodeTypeLogement(),
            ServiceWebV3CalculerConstante::PRM_ANNEE_CONSTRUCT => $projet->getAnneeConstruction(),
            ServiceWebV3CalculerConstante::PRM_SURF_HABITABLE  => $projet->getSurfaceHabitable(),
            ServiceWebV3CalculerConstante::PRM_ENERGIE_CHAUFF  => $projet->getCodeEnergie(),
            ServiceWebV3CalculerConstante::PRM_MODE_CHAUFF    => $projet->getCodeModeChauffage()
        ];

        return $dataResult;
    }

    /**
     * Retourne le retour des données foyer
     *
     * @param Projet $projet projet à retourner
     *
     * @return array
     */
    private function getResultSimulationFoyer($projet)
    {
        $dataResult = [
            ServiceWebV3CalculerConstante::PRM_NB_ADULTE             => $projet->getNbAdultes(),
            ServiceWebV3CalculerConstante::PRM_NB_PERS_CHARGE        => $projet->getNbPersonnesCharge(),
            ServiceWebV3CalculerConstante::PRM_NB_ENF_GARDE_ALTERNEE => $projet->getNbEnfantAlterne(),
            ServiceWebV3CalculerConstante::PRM_NB_PARTS_FISCALES     => $projet->getNbPartsFiscales(),
            ServiceWebV3CalculerConstante::PRM_STATUT                => $projet->getCodeStatut(),
            ServiceWebV3CalculerConstante::PRM_REVENU_FISCAL         => intval($projet->getRevenus()),
            ServiceWebV3CalculerConstante::PRM_BENEFICIE_PTZ         => Utils::getBooleanNumeric(
                $projet->getBeneficePtz()
            ),
            ServiceWebV3CalculerConstante::PRM_PRIMO_ACCESSION         => Utils::getBooleanNumeric(
                $projet->getPrimoAccession()
            ),
            ServiceWebV3CalculerConstante::PRM_SALARIE_SECTEUR_PRIVE        => Utils::getBooleanNumeric(
                $projet->getSalarieSecteurPrive()
            ),
            ServiceWebV3CalculerConstante::PRM_MT_CDT_IMPOT          => $projet->getMontantBeneficeCi(),
            ServiceWebV3CalculerConstante::PRM_PRJ_RENOVATION_BBC    => Utils::getBooleanNumeric($projet->getBbc())
        ];

        return $dataResult;
    }

    /**
     * Retourne le retour des travaux du projet
     *
     * @param Projet $projet projet à retourner
     *
     * @return array
     */
    private function getResultSimulationTravaux($projet)
    {
        $dataResult = [];

        $idProjet = $projet->getId();
        /** @var Travaux $travaux */
        foreach ($projet->getTravaux() as $travaux) {
            $trvCode     = $travaux->getCode();
            $infoTravaux = [
                ServiceWebV3CalculerConstante::PRM_TRVX_CODE => $trvCode,
                ServiceWebV3CalculerConstante::PRM_PRODUITS  => $this->getResultSimulationProduits($idProjet, $trvCode)
            ];

            $dataResult[] = $infoTravaux;
        }
        return $dataResult;
    }

    /**
     * Retourne le retour des produits du projet
     *
     * @param int    $idProjet    Identifiant du projet
     * @param String $codeTravaux Code du travaux en cours
     *
     * @return array
     */
    private function getResultSimulationProduits($idProjet, $codeTravaux)
    {
        $dataResult = [];

        //Récupération de la liste des saisie des produits du travaux
        $tabSaisieProduit = $this->getProduitService()->getSaisieProduitParIdProjetEtCodeTravaux(
            $idProjet,
            $codeTravaux
        );
        $infosProduit     = [];
        $prevCodeProduit  = '';
        /** @var SaisieProduit $saisieProduit */
        foreach ($tabSaisieProduit as $saisieProduit) {
            if ($prevCodeProduit != $saisieProduit->getCodeProduit()) {
                //Il s'agit d'un nouveau produit du travaux, réinitialisation des infos
                if (!empty($prevCodeProduit)) {
                    //Mémorisation du produit précédent
                    $dataResult[] = $infosProduit;
                }
                $infosProduit    = [
                    ServiceWebV3CalculerConstante::PRM_PDT_CODE => $saisieProduit->getCodeProduit()
                ];
                $prevCodeProduit = $saisieProduit->getCodeProduit();
            }
            if ($saisieProduit instanceof SaisieProduitMontant) {
                /** @var SaisieProduitMontant $saisieProduit */
                if ($saisieProduit->getTypeMontant() == SessionConstante::COUT_TOTAL) {
                    if ($saisieProduit->getCout() > 0) {
                        $infosProduit[ServiceWebV3CalculerConstante::PRM_PDT_MT_TOTAL_HT] = $saisieProduit->getCout();
                    }
                } elseif ($saisieProduit->getTypeMontant() == SessionConstante::COUT_MO) {
                    if ($saisieProduit->getCout() > 0) {
                        $infosProduit[ServiceWebV3CalculerConstante::PRM_PDT_MT_MO_HT] = $saisieProduit->getCout();
                    }
                } elseif ($saisieProduit->getTypeMontant() == SessionConstante::COUT_FO) {
                    if ($saisieProduit->getCout() > 0) {
                        $infosProduit[ServiceWebV3CalculerConstante::PRM_PDT_MT_FO_HT] = $saisieProduit->getCout();
                    }
                }
            } elseif ($saisieProduit instanceof SaisieProduitParametreTech) {
                /** @var SaisieProduitParametreTech $saisieProduit */
                $infosProduit[ServiceWebV3CalculerConstante::PRM_PARAM_TECH][] = [
                    ServiceWebV3CalculerConstante::PRM_PARAM_TECH_CODE   => $saisieProduit->getCodeParametreTech(),
                    ServiceWebV3CalculerConstante::PRM_PARAM_TECH_VALEUR => $saisieProduit->getValeur()
                ];
            }

        }
        if (!empty($infosProduit)) {
            //Mémorisation du dernier produit
            $dataResult[] = $infosProduit;
        }

        return $dataResult;
    }

    /**
     * Retourne le conseiller
     *
     * @param ProjetSimul $projet projet à retourner
     *
     * @return array
     */
    private function getResultSimulationConseiller($projet)
    {
        $dataResult = [];

        $codeCommune = $projet->getCodeCommune();
        $projetSimulService = $this->serviceLocator->get(ProjetSimulService::SERVICE_NAME);
        /** @var LocalisationService $localisationService */
        $localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
        $idf = $localisationService->estDansLaRegion($codeCommune,MainConstante::$LISTE_IDF);
        if($idf){
            $codeTranche = $projetSimulService->getCodeTranche($projet, GroupeTrancheConstante::ANAH_IDF);
        }else{
            $codeTranche = $projetSimulService->getCodeTranche($projet, GroupeTrancheConstante::ANAH);
        }
        $conseiller = $projetSimulService->getCoordonneesPris($codeCommune, $codeTranche);
        if($conseiller) {
            $dataResult['libelle'] = isset($conseiller['nom']) ? $conseiller['nom'] : '';
            $dataResult['telephone'] = isset($conseiller['num_tel_local']) ? $conseiller['num_tel_local'] : '';
            $dataResult['email'] = isset($conseiller['email']) ? $conseiller['email'] : '';
        }
        return $dataResult;
    }

    /**
     * Permet de récupérer le service sur les paramètres des services web
     *
     * @return array|object|ParametreServiceWebService
     */
    private function getParametreServiceWebService()
    {
        if (empty($this->parametreServiceWebService)) {
            $this->parametreServiceWebService = $this->serviceLocator->get(ParametreServiceWebV3Service::SERVICE_NAME);
        }
        return $this->parametreServiceWebService;
    }

    /**
     * Permet de récupérer le service sur la localisation
     *
     * @return array|object|LocalisationService
     */
    private function getLocalisationService()
    {
        if (empty($this->localisationService)) {
            $this->localisationService = $this->serviceLocator->get(LocalisationService::SERVICE_NAME);
        }
        return $this->localisationService;
    }

    /**
     * Permet de récupérer le service sur les travaux
     *
     * @return array|object|TravauxService
     */
    private function getTravauxService()
    {
        if (empty($this->travauxService)) {
            $this->travauxService = $this->serviceLocator->get(TravauxService::SERVICE_NAME);
        }
        return $this->travauxService;
    }

    /**
     * Permet de récupérer le service sur les listes de valeurs
     *
     * @return array|object|ValeurListeService
     */
    private function getValeurListeService()
    {
        if (empty($this->valeurListeService)) {
            $this->valeurListeService = $this->serviceLocator->get(ValeurListeService::SERVICE_NAME);
        }
        return $this->valeurListeService;
    }

    /**
     * Permet de récupérer le service sur les projets
     *
     * @return array|object|ProjetService
     */
    private function getProjetService()
    {
        if (empty($this->projetService)) {
            $this->projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);
        }
        return $this->projetService;
    }

    /**
     * Permet de récupérer le service sur les produits
     *
     * @return array|object|ProduitService
     */
    private function getProduitService()
    {
        if (empty($this->produitService)) {
            $this->produitService = $this->serviceLocator->get(ProduitService::SERVICE_NAME);
        }

        return $this->produitService;
    }
}
