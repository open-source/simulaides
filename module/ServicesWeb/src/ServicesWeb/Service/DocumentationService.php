<?php
namespace ServicesWeb\Service;

use ServicesWeb\Constante\ServicesWebConstante;
use ServicesWeb\Constante\ServiceWebCalculerConstante;
use ServicesWeb\Constante\ServiceWebGetSimulationConstante;

/**
 * Classe DocumentationService
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   ServicesWeb\Service
 */
class DocumentationService
{
    /** Nom du service */
    const SERVICE_NAME = 'documentationService';

    /**
     * Permet de retourner la documentation du sous-service "Calculer"
     * @return array
     */
    public function getDocumentationServiceCalculer()
    {
        $documentation = [];

        //clé de sécurisation
        $infosDoc        = [
            'intitule'  => "Clé de sécurisation",
            'listParam' => $this->getParamCleSecurise()
        ];
        $documentation[] = $infosDoc;

        //Données de logement et foyer
        $infosDoc        = [
            'intitule'  => "Données logement et foyer :",
            'listParam' => $this->getParamDonneeFoyerEtLogement()
        ];
        $documentation[] = $infosDoc;

        //Données travaux
        $infosDoc        = [
            'intitule'  => "Données travaux :",
            'listParam' => $this->getParamDonneeTravaux()
        ];
        $documentation[] = $infosDoc;

        return $documentation;
    }

    /**
     * Permet de retourner les param pour le foyer / logement
     * @return array
     */
    private function getParamDonneeFoyerEtLogement()
    {
        $data = [];

        $listParam = ServiceWebCalculerConstante::getListeParametreLogement();
        foreach ($listParam as $param) {
            $data[] = [
                'intitule' => $param,
                'valeur'   => ServiceWebCalculerConstante::getDocumentationParam($param)
            ];
        }

        $listParam = ServiceWebCalculerConstante::getListeParametreFoyer();
        foreach ($listParam as $param) {
            $data[] = [
                'intitule' => $param,
                'valeur'   => ServiceWebCalculerConstante::getDocumentationParam($param)
            ];
        }

        return $data;
    }

    /**
     * Permet de retourner les param pour travaux
     * @return array
     */
    private function getParamDonneeTravaux()
    {
        $data = [];

        $data[] = [
            'intitule' => 'les identifiants des travaux sont récupérés par le service de récupération des travaux',
            'valeur'   => null
        ];

        $data[] = [
            'intitule' => 'sont énumérés tous les travaux envisagés',
            'valeur'   => null
        ];

        $data[] = [
            'intitule' => "les montants s'expriment en € HT.
                           Si l'utilisateur ne connaît pas les montants, alors il peut laisser ces cases vides.",
            'valeur'   => null
        ];

        $data[] = [
            'intitule' => "Pour les paramètres techniques : 3 types sont possibles (ENTIER, BOOLEEN, LISTE).
                           Pour les listes déroulantes, saisir le code de l'élément choisi",
            'valeur'   => null
        ];

        return $data;
    }

    /**
     * Permet de retourner la structure des param d'enteée du service Calculer
     *
     * @return array
     */
    public function getStructureParamServiceCalculer()
    {
        $data = [];

        $data[] = $this->getDocumentationCleSurise();

        $data[] = [
            'intitule'  => "<b>" . ServiceWebCalculerConstante::PRM_MON_LOGEMENT . "</b>",
            'espace'    => '',
            'listParam' => ServiceWebCalculerConstante::getListeParametreLogement()
        ];
        $data[] = [
            'intitule'  => "<b>" . ServiceWebCalculerConstante::PRM_MON_FOYER . "</b>",
            'espace'    => '',
            'listParam' => ServiceWebCalculerConstante::getListeParametreFoyer()
        ];
        $data[] = [
            'intitule'  => "<b>" . ServiceWebCalculerConstante::PRM_TRAVAUX . "[0]</b>",
            'espace'    => '',
            'listParam' => ServiceWebCalculerConstante::getListeParametreTravaux()
        ];
        $data[] = [
            'intitule'  => "<b>" . ServiceWebCalculerConstante::PRM_PRODUITS . "[0]</b>",
            'espace'    => '&nbsp;&nbsp;',
            'listParam' => ServiceWebCalculerConstante::getListeParametreProduit()
        ];
        $data[] = [
            'intitule'  => "<b>" . ServiceWebCalculerConstante::PRM_PARAM_TECH . "[0]</b>",
            'espace'    => '&nbsp;&nbsp;&nbsp;&nbsp;',
            'listParam' => ServiceWebCalculerConstante::getListeParametreParamTech()
        ];
        return $data;
    }

    /**
     * Permet de retourner la documentation du sous-service "getSimulation"
     * @return array
     */
    public function getDocumentationServiceGetSimulation()
    {
        $documentation = [];

        //clé de sécurisation
        $infosDoc        = [
            'intitule'  => "Clé de sécurisation",
            'listParam' => $this->getParamCleSecurise()
        ];
        $documentation[] = $infosDoc;

        $infosDoc        = [
            'intitule'  => "Données de la simulation :",
            'listParam' => $this->getParamDonneeSimulation()
        ];
        $documentation[] = $infosDoc;

        return $documentation;
    }

    /**
     * Permet de retourner les param pour les données de simulation
     * @return array
     */
    private function getParamDonneeSimulation()
    {
        $data = [];

        $listParam = ServiceWebGetSimulationConstante::getListeParametreSimulation();
        foreach ($listParam as $param) {
            $data[] = [
                'intitule' => $param,
                'valeur'   => ServiceWebGetSimulationConstante::getDocumentationParam($param)
            ];
        }

        return $data;
    }

    /**
     * Permet de retourner la structure des param d'enteée du service getSimulation
     *
     * @return array
     */
    public function getStructureParamServiceGetSimulation()
    {
        $data = [];

        $data[] = $this->getDocumentationCleSurise();

        $data[] = [
            'intitule'  => "",
            'espace'    => '',
            'listParam' => ServiceWebGetSimulationConstante::getListeParametreSimulation()
        ];
        return $data;
    }

    /**
     * Retourne la documentation de la clé de sécurisation
     *
     * @return array
     */
    private function getDocumentationCleSurise()
    {
        $data = [
            'intitule'  => "",
            'espace'    => '',
            'listParam' => [ServicesWebConstante::PRM_CLE_SECURISATION]
        ];
        return $data;
    }

    /**
     * Permet de retourner le paramètre clé de sécurisation
     * @return array
     */
    private function getParamCleSecurise()
    {
        $data[] = [
            'intitule' => ServicesWebConstante::PRM_CLE_SECURISATION,
            'valeur'   => 'clé de sécurisation'
        ];
        return $data;
    }

    /**
     * Permet de retourner la documentation du sous-service "getTravaux"
     * @return array
     */
    public function getDocumentationServiceGetTravaux()
    {
        $documentation = [];

        //clé de sécurisation
        $infosDoc        = [
            'intitule'  => "Clé de sécurisation",
            'listParam' => $this->getParamCleSecurise()
        ];
        $documentation[] = $infosDoc;

        return $documentation;
    }

    /**
     * Permet de retourner la structure des param d'enteée du service getTavaux
     *
     * @return array
     */
    public function getStructureParamServiceGetTravaux()
    {
        $data = [];

        $data[] = $this->getDocumentationCleSurise();

        return $data;
    }
}
