<?php
namespace ServicesWeb\Service\Factory;

use ServicesWeb\Service\ServicesWebService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ServicesWebServiceFactory
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   ServicesWeb\Service\Factory
 */
class ServicesWebServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new ServicesWebService($serviceLocator);
    }
}
