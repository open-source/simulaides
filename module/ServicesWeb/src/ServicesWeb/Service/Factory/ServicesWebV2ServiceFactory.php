<?php
namespace ServicesWeb\Service\Factory;

use ServicesWeb\Service\ServicesWebV2Service;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe ServicesWebServiceFactory
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @package   ServicesWeb\Service\Factory
 */
class ServicesWebV2ServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new ServicesWebV2Service($serviceLocator);
    }
}
