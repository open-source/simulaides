<?php
return [
    'routes' => [
        'webservice' => [
            'type'    => 'Zend\Mvc\Router\Http\Segment',
            'options' => [
                'route'    => '/webservice/:version/:sous_service',
                'defaults' => [
                    'controller' => 'ServicesWeb\Controller\ServicesWeb',
                    'action'     => 'index',
                ],
            ],
        ],
        'documentation-ws' => [
            'type'    => 'Zend\Mvc\Router\Http\Segment',
            'options' => [
                'route'    => '/documentation-ws/:sous-service',
                'defaults' => [
                    'controller' => 'ServicesWeb\Controller\ServicesWeb',
                    'action'     => 'documentation',
                ],
            ],
        ],
        'test' => [
            'type'    => 'literal',
            'options' => [
                'route'    => '/test',
                'defaults' => [
                    'controller' => 'ServicesWeb\Controller\ServicesWeb',
                    'action'     => 'test',
                ],
            ],
        ],
    ]
];
