<?php
return [
    'factories'  => [
        'ParametreServiceWebService' => 'ServicesWeb\Service\Factory\ParametreServiceWebServiceFactory',
        'ServicesWebService'         => 'ServicesWeb\Service\Factory\ServicesWebServiceFactory',
        'ParametreServiceWebV2Service' => 'ServicesWeb\Service\Factory\ParametreServiceWebV2ServiceFactory',
        'ServicesWebV2Service'         => 'ServicesWeb\Service\Factory\ServicesWebV2ServiceFactory',
        'ParametreServiceWebV3Service' => 'ServicesWeb\Service\Factory\ParametreServiceWebV3ServiceFactory',
        'ServicesWebV3Service'         => 'ServicesWeb\Service\Factory\ServicesWebV3ServiceFactory',

    ],
    'invokables' => [
        'documentationService' => 'ServicesWeb\Service\DocumentationService'
    ]
];
