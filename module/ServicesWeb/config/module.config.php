<?php

return [
    'controllers'     => include 'controllers.config.php',
    'service_manager' => include 'services.config.php',
    'router'          => include 'routes.config.php',
    'view_helpers'    => array(
        'invokables' => array()
    ),
    'view_manager'    => array(
        'layout/layout_ws' => 'layout/ws',
        'template_map'     => array(
            'layout/ws' => __DIR__ . '/../view/layout-ws.phtml',
        ),
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],
    )
];
