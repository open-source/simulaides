<?php

namespace Batch\Exception;

/**
 * Classe PrisException
 *
 * Projet : Simul'Aides
 *
 * @author
 * @package   Batch\Exception
 */
class PrisException extends \Exception
{
}
