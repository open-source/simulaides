<?php

namespace Batch\Controller;

use Batch\Exception\PrisException;
use Batch\Service\PrisImportService;
use Simulaides\Domain\Entity\CodePostal;
use Simulaides\Domain\Repository\CodePostalRepository;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;

/**
 * Classe ImportDonneesPrisController
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Batch\Controller
 */
class ImportDonneesPrisController extends AbstractActionController
{
    /**
     * Action permettant de réaliser l'import des données PRIS
     *
     * @return array|null|\Zend\Stdlib\ResponseInterface
     * @throws PrisException
     */
    public function indexAction()
    {
        /** @var Logger $logger */
        $logger = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        Logger::registerErrorHandler($logger);

        try {
            /** @var PrisImportService $prisService */
            $prisService = $this->serviceLocator->get(PrisImportService::SERVICE_NAME);

            try {
                // Récupération des données
                $tabPrisData = $prisService->getDonneesPris();

                //Lancement de l'import
                $prisService->importDonneesPRIS($tabPrisData);
            } catch (\Exception $e) {
                $logger->err($e->getMessage());
            }

        } catch (ServiceNotCreatedException $exception) {
            // Pas idéal mais permet d'indiquer le cas où la connexion à la base PRIS ne se fait pas
            $logger->err("Un problème est probablement survenu au cours de la tentative de connexion à la base PRIS : " . $exception->getMessage());
        }

        $response = $this->getResponse();
        if (!empty($response)) {
            return $response;
        }
        return null;
    }

    public function tempAction(){
        set_time_limit(0);
        //var_dump(scandir(getcwd()));
        /*Ouverture du fichier en lecture seule*/
        $handle = fopen('public/codes-postaux.tri', 'r');
        /*Si on a réussi à ouvrir le fichier*/
        if ($handle)
        {
            $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $communeRep = $em->getRepository('Simulaides\Domain\Entity\Commune');

            /** @var CodePostalRepository $codePostalRep */
            $codePostalRep = $em->getRepository('Simulaides\Domain\Entity\CodePostal');
            /*Tant que l'on est pas à la fin du fichier*/
            while (!feof($handle))
            {
                /*On lit la ligne courante*/
                $buffer = fgets($handle);
                $codeInsee  = substr($buffer, 6, 5);
                $codePostal = substr($buffer, 89, 5);
                if($codeInsee!='     ' && $codePostal!='     ') {
                    $assocCICP = $codePostalRep->findOneBy(['codePostal'=>$codePostal, 'codeInsee'=>$codeInsee]);
                    if(is_null($assocCICP)) {
                        $commune = $communeRep->getCommuneParCode($codeInsee);
                        if ($commune != null) {
                            $codePostalEntity = new CodePostal();
                            $codePostalEntity->setCodeInsee($commune);
                            $codePostalEntity->setCodePostal($codePostal);
                            $em->persist($codePostalEntity);

                        }
                    }
                }
            }

            $em->flush();
            /*On ferme le fichier*/
            fclose($handle);
        }


        return null;
    }
}
