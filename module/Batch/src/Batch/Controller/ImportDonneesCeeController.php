<?php

namespace Batch\Controller;

use Batch\Exception\CeeException;
use Batch\Service\CeeImportService;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Classe ImportDonneesCeeController
 *
 * Projet : SimulAides 2015-2015
 
 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Batch\Controller
 */
class ImportDonneesCeeController extends AbstractActionController
{
    /**
     * Action permettant de réaliser l'import des données CEE
     *
     * @return array|null|\Zend\Stdlib\ResponseInterface
     * @throws CeeException
     */
    public function indexAction()
    {
        ini_set('memory_limit', '-1');
        /** @var Logger $logger */
        $logger = $this->serviceLocator->get(LoggerFactory::SERVICE_NAME);
        Logger::registerErrorHandler($logger);

        try {
            /** @var CeeImportService $ceeService */
            $ceeService = $this->serviceLocator->get(CeeImportService::SERVICE_NAME);

            // Ouverture du fichier d'import
            $ceeData = $ceeService->getCeeData();
            $ceeService->importCeeData($ceeData);

        } catch (\Exception $e) {
            $logger->err($e->getMessage());
        }


        $response = $this->getResponse();
        if (!empty($response)) {
            return $response;
        }
        return null;
    }

}
