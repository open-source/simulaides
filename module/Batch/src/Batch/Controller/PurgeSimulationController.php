<?php
namespace Batch\Controller;

use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Service\DispositifService;
use Simulaides\Service\ProduitService;
use Simulaides\Service\ProjetService;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Classe PurgeSimulationController
 * Controller pour la purge des données de simulation
 * datant de plus de 1 an
 *
 * Projet : SimulAides 2015-2015

 *
 * @copyright Copyright © ADEME 2015-2015, All Rights Reserved
 * @author
 * @package   Batch\src\Batch\Controller
 */
class PurgeSimulationController extends AbstractActionController
{
    /**
     * Permet de réaliser la purge des données de simulation
     *
     * @return array|null|\Zend\Stdlib\ResponseInterface
     */
    public function indexAction()
    {
        $this->desactivationDispositif();
        $this->purgeSimulationDonnees();
        $this->suppressionDispositif();
        $response = $this->getResponse();
        if (!empty($response)) {
            return $response;
        }
        return null;
    }

    private function purgeSimulationDonnees()
    {
        /** @var ProjetService $projetService */
        $projetService = $this->serviceLocator->get(ProjetService::SERVICE_NAME);

        //Sélection des projets ayant plus d'un an
        $date = new \DateTime('now');
        $date->modify('-6 months');
        $tabIdProjet = $projetService->getIdProjetAvantDate($date);

        //Suppression des projets ayant plus d'un an
        if (!empty($tabIdProjet)) {
            //Suppression dans Projet
            $projetService->supprimeListeProjet($tabIdProjet);
        }
    }

    private function desactivationDispositif()
    {
        /** @var DispositifService $dispositifService */
        $dispositifService = $this->serviceLocator->get(DispositifService::SERVICE_NAME);

        //Sélection des projets ayant plus d'un an
        $date = new \DateTime('now');
        //$date->modify('-1 day');
        $tabId = $dispositifService->getIdDispositifByDateDepassee($date,ValeurListeConstante::ETAT_DISPOSITIF_ACTIVE);

        if (!empty($tabId)) {
            //Suppression dans Projet
            foreach ($tabId as $cle => $id){
                $dispositifService->changeEtatDispositifBatch($id['id'],ValeurListeConstante::ETAT_DISPOSITIF_DESACTIVE);
            }
        }
    }

    private function suppressionDispositif()
    {
        /** @var DispositifService $dispositifService */
        $dispositifService = $this->serviceLocator->get(DispositifService::SERVICE_NAME);

        //Sélection des projets ayant plus d'un an
        $date = new \DateTime('now');
        $date->modify('-1 year');
        $tabId = $dispositifService->getIdDispositifByDateDepassee($date);
        if (!empty($tabId)) {
            //Suppression dans Projet
            foreach ($tabId as $cle => $id){
               $dispositif = $dispositifService->getDispositifParId($id['id']);
               if($dispositif){
                   $dispositifService->deleteDispositif($dispositif);
               }
            }
        }
    }

}
