<?php

namespace Batch\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use Simulaides\Constante\ValeurListeConstante;
use Simulaides\Domain\Entity\Dispositif;
use Simulaides\Domain\Entity\DispositifRegle;
use Simulaides\Domain\Entity\DispositifTravaux;
use Simulaides\Domain\Entity\DispTravauxRegle;
use Simulaides\Domain\Entity\Oblige;
use Simulaides\Domain\Repository\DispositifRegleRepository;
use Simulaides\Domain\Repository\DispositifRepository;
use Simulaides\Domain\Repository\DispositifTravauxRepository;
use Simulaides\Domain\Repository\DispTravauxRegleRepository;
use Simulaides\Domain\Repository\ObligeRepository;
use Zend\Log\Logger;

/**
 * Classe CeeImportService
 *
 * Projet : Simul'Aides
 * @package   Batch\Service
 */
class CeeImportService
{
    /** Nom du service */
    const SERVICE_NAME = 'CeeImportService';

    /** Input & output file paths */
    const CSV_FILE_PATH = "data/ceeData/importCee.csv";
    const ERROR_LOG_PATH = "data/ceeData/errorLog.txt";
    const COLUMN_INTITULE_OFFRE = 0;
    const COLUMN_ID_OFFRE = 1;
    const COLUMN_NOM_OBLIGE = 2;
    const COLUMN_TYPE_MENAGE = 3;
    const COLUMN_TYPE_CHAUFFAGE_ACTUEL = 4;
    const COLUMN_TYPE_PROJETS_RENOVATION_CHAUFFAGE = 5;
    const COLUMN_TYPE_CHAUFFAGE_LOGEMENT = 6;
    const COLUMN_SIREN = 7;
    const COLUMN_TELEPHONE = 8;
    const COLUMN_MAIL_CONTACT = 9;
    const COLUMN_SITE_WEB = 10;
    const COLUMN_DATE_DEBUT_OFFRE = 11;
    const COLUMN_CONDITIONS_DE_SOUSCRIPTION = 12;
    const COLUMN_MONTANT_MINIMUM_PRIME_MODESTE = 13;
    const COLUMN_MONTANT_MINIMUM_PRIME_TRES_MODESTE = 14;
    const COLUMN_MONTANT_MINIMUM_PRIME_NON_MODESTE = 15;
    const COLUMN_PRIX_PLAFOND_OFFRE = 16;
    const COLUMN_DETAIL_OFFRE = 17;
    const COLUMN_COMPLEMENT = 18;
    const COLUMN_IS_OFFRE = 19;
//    const COLUMN_LOGO = 19;
    const CODE_DELIMITER = "|";
    private $fp;

    /* EDB38_01 */
    private $csvColumns = [
        self::COLUMN_INTITULE_OFFRE => "Nom_de_l_offre",
        self::COLUMN_ID_OFFRE => "ID_offre_operateur",
        self::COLUMN_NOM_OBLIGE => "Nom_operateur",
        self::COLUMN_TYPE_MENAGE => "Type_de_menage",
        self::COLUMN_TYPE_CHAUFFAGE_ACTUEL => "Type_chauffage_actuel",
        self::COLUMN_TYPE_PROJETS_RENOVATION_CHAUFFAGE => "Type_projets_de_renovation_du_chauffage",
        self::COLUMN_TYPE_CHAUFFAGE_LOGEMENT => "Type_chauffage_du_logement",
        self::COLUMN_SIREN => "SIREN",
        self::COLUMN_TELEPHONE => "Numero_telephone_service_clients",
        self::COLUMN_MAIL_CONTACT => "Adresse_mail_contact",
        self::COLUMN_SITE_WEB => "Lien_internet",
        self::COLUMN_DATE_DEBUT_OFFRE => "Date_de_debut_de_l_offre",
        self::COLUMN_CONDITIONS_DE_SOUSCRIPTION => "Condition_souscription",
        self::COLUMN_MONTANT_MINIMUM_PRIME_MODESTE => "Montant_minimum_prime_modeste",
        self::COLUMN_MONTANT_MINIMUM_PRIME_TRES_MODESTE => "Montant_minimum_prime_tres_modeste",
        self::COLUMN_MONTANT_MINIMUM_PRIME_NON_MODESTE => "Montant_minimum_prime_non_modeste",
        self::COLUMN_PRIX_PLAFOND_OFFRE => "Prix_de_l_offre",
        self::COLUMN_DETAIL_OFFRE => "Detail_de_l_offre",
        self::COLUMN_COMPLEMENT => "Complement",
        self::COLUMN_IS_OFFRE => "Is_offre",
//        self::COLUMN_LOGO => "Logo"
    ];
    /* V_EDB38_IPD_01 */
    private $requiredFields = [
        self::COLUMN_INTITULE_OFFRE,
        self::COLUMN_ID_OFFRE,
        self::COLUMN_NOM_OBLIGE,
        self::COLUMN_SIREN,
        self::COLUMN_SITE_WEB,
        self::COLUMN_TYPE_PROJETS_RENOVATION_CHAUFFAGE,
        self::COLUMN_TYPE_CHAUFFAGE_ACTUEL,
        self::COLUMN_TYPE_MENAGE,
        self::COLUMN_MONTANT_MINIMUM_PRIME_TRES_MODESTE,
        self::COLUMN_MONTANT_MINIMUM_PRIME_MODESTE,
        self::COLUMN_MONTANT_MINIMUM_PRIME_NON_MODESTE,
        self::COLUMN_DATE_DEBUT_OFFRE,
    ];

    /* V_EDB38_IPD_02 */
    private $integerFields = [
        self::COLUMN_MONTANT_MINIMUM_PRIME_TRES_MODESTE,
        self::COLUMN_MONTANT_MINIMUM_PRIME_MODESTE,
        self::COLUMN_MONTANT_MINIMUM_PRIME_NON_MODESTE,
        self::COLUMN_PRIX_PLAFOND_OFFRE
    ];
    /*  column de type boolean*/
    private $booleanFields = [
        self::COLUMN_IS_OFFRE
    ];
    /* V_EDB38_IPD_03 */
    private $maxCharactersFields = [
        self::COLUMN_INTITULE_OFFRE => 150,
        self::COLUMN_ID_OFFRE => 60,
        self::COLUMN_NOM_OBLIGE => 150,
        self::COLUMN_SIREN => 150,
        self::COLUMN_MAIL_CONTACT => 100,
        self::COLUMN_SITE_WEB => 150,
        self::COLUMN_TYPE_PROJETS_RENOVATION_CHAUFFAGE => 3,
        self::COLUMN_TYPE_CHAUFFAGE_ACTUEL => 3,
        self::COLUMN_TYPE_MENAGE => 3,
        self::COLUMN_MONTANT_MINIMUM_PRIME_TRES_MODESTE => 5,
        self::COLUMN_MONTANT_MINIMUM_PRIME_MODESTE => 5,
        self::COLUMN_MONTANT_MINIMUM_PRIME_NON_MODESTE => 5,
        self::COLUMN_DATE_DEBUT_OFFRE => 10,
        self::COLUMN_CONDITIONS_DE_SOUSCRIPTION => 2000,
        self::COLUMN_DETAIL_OFFRE => 2000,
        self::COLUMN_COMPLEMENT => 2000
    ];
    private $codeFields = [
        self::COLUMN_TYPE_PROJETS_RENOVATION_CHAUFFAGE => [
            /** Code dans le fichier d'import => code(s) correspondant(s) dans la BDD SA */
            "234" => ["T21"],
            "248" => ["T47"],
            "235" => ["T23", "T24"],
            "236" => ["T25"],
            "237" => ["T11"],
            "238" => ["T43"],
            "239" => ["T20"]
        ],
        self::COLUMN_TYPE_CHAUFFAGE_ACTUEL => [
            /** Code dans le fichier d'import => expression correspondant pour la BDD SA */
            "229" => "\$L.energie_chauffage_detail === \"FIOUL\"",
            "230" => "\$L.energie_chauffage_detail === \"GAZ_BOUTEILLE\" || \$L.energie_chauffage_detail === \"GAZ_CITERNE\" || \$L.energie_chauffage_detail === \"GAZ_VILLE\"",
            "231" => "\$L.energie_chauffage_detail === \"CHARBON\"",
            "232" => "\$L.energie_chauffage_detail === \"ELECTRICITE\"",
            "233" => "\$L.energie_chauffage_detail != \"FIOUL\" && \$L.energie_chauffage_detail != \"GAZ_BOUTEILLE\" && \$L.energie_chauffage_detail != \"GAZ_CITERNE\" && \$L.energie_chauffage_detail != \"GAZ_VILLE\" && \$L.energie_chauffage_detail != \"CHARBON\" && \$L.energie_chauffage_detail != \"ELECTRICITE\""
        ],
        self::COLUMN_TYPE_CHAUFFAGE_LOGEMENT => [
            "response_one" => "INDIVIDUEL",
            "response_two" => "COLLECTIF"
        ],
        self::COLUMN_TYPE_MENAGE => [
            "241" => "\$F.tranche[ANAH] === \"TRES_MODESTE\"",
            "242" => "\$F.tranche[ANAH] === \"MODESTE\"",
            "243" => "( \$F.tranche[ANAH] != \"MODESTE\" && \$F.tranche[ANAH] != \"TRES_MODESTE\" )"
        ]
    ];
    private $aggregatedColumns = [
        self::COLUMN_TYPE_MENAGE,
        self::COLUMN_TYPE_CHAUFFAGE_ACTUEL
    ];

    /**
     * Constructeur du service
     *
     * @param EntityManager $entityManager
     * @param Logger $loggerService
     */
    public function __construct(EntityManager $entityManager, Logger $loggerService)
    {
        $this->entityManager = $entityManager;
        $this->loggerService = $loggerService;

        $this->fp = fopen(self::ERROR_LOG_PATH, 'w');
    }

    /**
     * Remplit un tableau de données à partir du fichier csv
     *
     * @return array $ceeData Tableau des données provenant du fichier CSV
     * @throws Exception
     */
    public function getCeeData()
    {
        $ceeData = [];

        try {
            $row = 0;

            // Builds an array of data from the csv file
            if (($handle = fopen(self::CSV_FILE_PATH, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, null, ";")) !== FALSE) {
                    $ceeData[$row] = $data;
                    $row++;
                }
                fclose($handle);
            }

            // Checks integrity of whole data
            $headersCorrect = true;
            $allLinesCorrect = true;
            foreach ($ceeData as $lineNb => $line) {
                if ($lineNb == 0) {
                    $headersCorrect = $this->checkHeadersIntegrity($line);
                } else {
                    echo "---- Traitement de la ligne " . $lineNb . "\n";
                    $lineNb++;
                    fwrite($this->fp, "---- Traitement de la ligne " . $lineNb . "\n");
                    $allLinesCorrect = $this->checkLineIntegrity($ceeData, $line, $lineNb) && $allLinesCorrect;
                }
            }
            fclose($this->fp);

            // No error was found in any line, data can be inserted in db
            if ($headersCorrect && $allLinesCorrect) {
                echo "Tout est bon, l'import va démarrer\n";
                fwrite($this->fp, "---- Tout est bon, l'import va démarrer\n");
                return $ceeData;
            } else {
                echo "Des erreurs sont présentes, l'import de sera pas fait\n";
                fwrite($this->fp, "---- Des erreurs sont présentes, l'import de sera pas fait\n");
                return null;
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function checkHeadersIntegrity($data)
    {
        $columnNb = 0;
        $allHeadersAreCorrect = true;
        if (sizeof($data) != sizeof($this->csvColumns)) {
            echo "Mauvais nombre de colonnes dans l'entête." . "\n";
            fwrite($this->fp, "Mauvais nombre de colonnes dans l'entête." . "\n");
            $allHeadersAreCorrect = false;
        }
        foreach ($data as $header) {
            if ($header != $this->csvColumns[$columnNb]) {
                echo "Mauvais nom de colonne. Attendu: " . $this->csvColumns[$columnNb] . " - Obtenu: " . $header . "\n";
                fwrite($this->fp, "Mauvais nom de colonne. Attendu: " . $this->csvColumns[$columnNb] . " - Obtenu: " . $header . "\n");
                $allHeadersAreCorrect = false;
            }
            $columnNb++;
        }
        return $allHeadersAreCorrect;
    }

    public function checkLineIntegrity($data, $line, $lineNb)
    {
        $isLineCorrect = $this->checkTotalLine($line, $lineNb);
        if ($isLineCorrect) {
            $isLineCorrect = $this->checkInteger($line, $lineNb) && $isLineCorrect;
            $isLineCorrect = $this->checkBoolean($line, $lineNb) && $isLineCorrect;
            $isLineCorrect = $this->checkMaxCharacters($line, $lineNb) && $isLineCorrect;
            $isLineCorrect = $this->checkSiren($data, $line, $lineNb) && $isLineCorrect;
//        $isLineCorrect = $this->checkPhoneNb($line, $lineNb) && $isLineCorrect;
            $isLineCorrect = $this->checkEmail($line, $lineNb) && $isLineCorrect;
            $isLineCorrect = $this->checkDate($line, $lineNb) && $isLineCorrect;
            $isLineCorrect = $this->checkCode($line, $lineNb) && $isLineCorrect;
//        $isLineCorrect = $this->checkUrl($line, $lineNb) && $isLineCorrect;
            $isLineCorrect = $this->checkImage($line, $lineNb) && $isLineCorrect;
            $isLineCorrect = $this->checkRequired($line, $lineNb) && $isLineCorrect;
        }

        return $isLineCorrect;
    }

    public function checkTotalLine($line, $lineNb)
    {
        $isLineCorrect = true;
        if (sizeof($line) != sizeof($this->csvColumns)) {
            $isLineCorrect = false;
            echo "Ligne " . $lineNb . " : Manque une ou plusieurs colonnes.\n";
            fwrite($this->fp, "Ligne " . $lineNb . " : Manque une ou plusieurs colonnes.\n");
        }
        return $isLineCorrect;
    }

    public function checkInteger($line, $lineNb)
    {
        $isLineCorrect = true;
        foreach ($this->integerFields as $columnNb) {
            if (!preg_match('/^[1-9]\d*$/', $line[$columnNb] + 0) && $line[$columnNb] != "") {
                $isLineCorrect = false;
                echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[$columnNb]
                    . " doit être un entier supérieur à 0.\n";
                fwrite($this->fp, "Ligne " . $lineNb . " : le champ " . $this->csvColumns[$columnNb]
                    . " doit être un entier supérieur à 0.\n");
            }
        }
        return $isLineCorrect;
    }

    public function checkBoolean($line, $lineNb)
    {
        $isLineCorrect = true;
        foreach ($this->booleanFields as $columnNb) {
            $value = trim($line[$columnNb]);
            if ((intval($line[$columnNb]) != 1 && intval($line[$columnNb]) != 0) || $value === '') {
                $isLineCorrect = false;
                echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[$columnNb]
                    . " doit être un entier égal à 0 si il s'agit d'un dispositif ou un entier égal à 1 si il s'agit  d'une offre.\n";
                fwrite($this->fp, "Ligne " . $lineNb . " : le champ " . $this->csvColumns[$columnNb]
                    . " doit être un entier égal à 0 si il s'agit d'un dispositif ou un entier égal à 1 si il s'agit  d'une offre.\n");
            }
        }
        return $isLineCorrect;
    }

    /* EDB38_01 */

    public function checkMaxCharacters($line, $lineNb)
    {
        $isLineCorrect = true;
        foreach ($this->maxCharactersFields as $columnNb => $maxCharacters) {
            if (in_array($columnNb, $this->aggregatedColumns)) {
                $tab = explode(self::CODE_DELIMITER, $line[$columnNb]);
                foreach ($tab as $code) {
                    if (strlen(trim($code)) > $maxCharacters) {
                        $isLineCorrect = false;
                        echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[$columnNb]
                            . " doit comporter au plus " . $maxCharacters . " caractères.\n";
                        fwrite($this->fp, "Ligne " . $lineNb . " : le champ " . $this->csvColumns[$columnNb]
                            . " doit comporter au plus " . $maxCharacters . " caractères.\n");
                    }
                }
            } else {
                if (strlen($line[$columnNb]) > $maxCharacters) {
                    $isLineCorrect = false;
                    echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[$columnNb]
                        . " doit comporter au plus " . $maxCharacters . " caractères.\n";
                    fwrite($this->fp, "Ligne " . $lineNb . " : le champ " . $this->csvColumns[$columnNb]
                        . " doit comporter au plus " . $maxCharacters . " caractères.\n");
                }
            }
        }
        return $isLineCorrect;
    }

    public function checkSiren($data, $line, $lineNb)
    {
        $checkSiren = $this->checkSirenFormat($line, $lineNb);
        if ($checkSiren) {
            $checkSiren = $this->checkSirenUnique($data, $line, $lineNb);
        }
        return $checkSiren;
    }

    /* EDB38_02 */

    public function checkSirenFormat($line, $lineNb)
    {
        $isLineCorrect = true;
        $siren = trim($line[self::COLUMN_SIREN]);

        // Checks that le siren is composed of 9 numbers
        if (!preg_match('/^[0-9]{9}$/', $siren)) {
            $isLineCorrect = false;
            echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[self::COLUMN_SIREN]
                . " doit comporter obligatoirement 9 chiffres.\n";
            fwrite($this->fp, "Ligne " . $lineNb . " : le champ " . $this->csvColumns[self::COLUMN_SIREN]
                . " doit obligatoirement comporter obligatoirement 9 chiffres.\n");
        } else {
            // Checks the special rule for siren
            $sum = 0;
            for ($i = 0; $i < 9; $i++) {
                if ($i % 2 == 1) {
                    $tmp = $siren[$i] * 2;
                    $tmp = $tmp > 9 ? $tmp - 9 : $tmp;
                } else {
                    $tmp = $siren[$i];
                }
                $sum += $tmp;
            }
            if ($sum % 10 !== 0) {
                $isLineCorrect = false;
                echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[self::COLUMN_SIREN]
                    . " n'est pas un numéro siren valide.\n";
                fwrite($this->fp, "Ligne " . $lineNb . " : le champ " . $this->csvColumns[self::COLUMN_SIREN]
                    . " n'est pas un numéro siren valide.\n");
            }
        }


        return $isLineCorrect;
    }

    /* EDB38_03 */

    public function checkSirenUnique($data, $line, $lineNb)
    {
        // Vérifie que le siren de la ligne courant n'a pas été utilisé par un obligé différent d'une ligne précédente
        for ($i = 1; $i < $lineNb; $i++) {
            if ($data[$i][self::COLUMN_SIREN] == $line[self::COLUMN_SIREN]
                && $data[$i][self::COLUMN_NOM_OBLIGE] != $line[self::COLUMN_NOM_OBLIGE]) {
                echo "Ligne " . $lineNb . " : la valeur " . $line[self::COLUMN_SIREN] . " contenue dans le champ "
                    . $this->csvColumns[self::COLUMN_SIREN]
                    . " est déjà attribuée à un autre obligé.\n";
                fwrite($this->fp, "Ligne " . $lineNb . " : la valeur " . $line[self::COLUMN_SIREN]
                    . " contenue dans le champ "
                    . $this->csvColumns[self::COLUMN_SIREN]
                    . " est déjà attribuée à un autre obligé.\n");
                return false;
            }
        }
        return true;
    }

    /* EDB38_04 */

    public function checkPhoneNb($line, $lineNb)
    {
        $isLineCorrect = true;
        if (!preg_match('/^[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}$/', $line[self::COLUMN_TELEPHONE])) {
            $isLineCorrect = false;
            echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[4]
                . " n’est pas au bon format NN-NN-NN-NN-NN.\n";
            fwrite($this->fp, "Ligne " . $lineNb . " : le champ " . $this->csvColumns[self::COLUMN_TELEPHONE]
                . " n’est pas au bon format NN-NN-NN-NN-NN.\n");
        }
        return $isLineCorrect;
    }

    /* EDB38_05 */

    public function checkEmail($line, $lineNb)
    {
        $isLineCorrect = true;
        if (!preg_match('/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/', $line[self::COLUMN_MAIL_CONTACT])
            && $line[self::COLUMN_MAIL_CONTACT] != "") {
            $isLineCorrect = false;
            echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[self::COLUMN_MAIL_CONTACT]
                . " n’est pas au bon format d'un email.\n";
            fwrite($this->fp, "Ligne " . $lineNb . " : le champ " . $this->csvColumns[self::COLUMN_MAIL_CONTACT]
                . " n’est pas au bon format d'un email.\n");
        }
        return $isLineCorrect;
    }

    /* EDB38_06 */

    public function checkDate($line, $lineNb)
    {
        $isLineCorrect = true;
        if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $line[self::COLUMN_DATE_DEBUT_OFFRE])) {
            $isLineCorrect = false;
            echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[self::COLUMN_DATE_DEBUT_OFFRE]
                . " n’est pas au bon format AAAA/DD/MM.\n";
            fwrite($this->fp, "Ligne " . $lineNb . " : le champ " . $this->csvColumns[self::COLUMN_DATE_DEBUT_OFFRE]
                . " n’est pas au bon format AAAA/DD/MM.\n");
        }
        return $isLineCorrect;
    }

    /* EDB38_07 */

    public function checkCode($line, $lineNb)
    {
        $isLineCorrect = true;
        foreach ($this->codeFields as $columnNb => $admittedCodes) {
            // Cas particulier des codes statut qui peuvent être aggrégé par des symboles |
            if (in_array($columnNb, $this->aggregatedColumns)) {
                $codeList = explode(self::CODE_DELIMITER, $line[$columnNb]);
                // Supprime les éventuels espaces avant et après les codes
                $codeList = array_map('trim', $codeList);

                foreach ($codeList as $codeInput) {
                    if (!in_array($codeInput, array_keys($admittedCodes))) {
                        $isLineCorrect = false;
                        echo "Ligne " . $lineNb . " pour le champ " . $this->csvColumns[$columnNb]
                            . " le code " . $codeInput . " n’est pas présent pour cette application.\n";
                        fwrite($this->fp, "Ligne " . $lineNb . " pour le champ " . $this->csvColumns[$columnNb]
                            . " le code " . $codeInput . " n’est pas présent pour cette application.\n");
                    }
                }
            } else {
                if (!in_array($line[$columnNb], array_keys($admittedCodes))) {
                    $isLineCorrect = false;
                    echo "Ligne " . $lineNb . " pour le champ " . $this->csvColumns[$columnNb]
                        . " le code " . $line[$columnNb] . " n’est pas présent pour cette application.\n";
                    fwrite($this->fp, "Ligne " . $lineNb . " pour le champ " . $this->csvColumns[$columnNb]
                        . " le code " . $line[$columnNb] . " n’est pas présent pour cette application.\n");
                }
            }
        }
        return $isLineCorrect;
    }

    /* EDB38_08 */

    public function checkUrl($line, $lineNb)
    {
        $isLineCorrect = true;
        if (!preg_match('/^https?:\/\//', $line[self::COLUMN_LOGO])) {
            $isLineCorrect = false;
            echo "Ligne " . $lineNb . " l'url contenue dans le champ " . $this->csvColumns[self::COLUMN_LOGO]
                . " est invalide.\n";
            fwrite($this->fp, "Ligne " . $lineNb . " l'url contenue dans le champ " . $this->csvColumns[self::COLUMN_LOGO]
                . " est invalide.\n");
        }
        return $isLineCorrect;
    }

    /* EDB38_09 */

    public function checkImage($line, $lineNb)
    {
        return true;
    }

    /* EDB38_10 */

    public function checkRequired($line, $lineNb)
    {
        $isLineCorrect = true;
        foreach ($this->requiredFields as $columnNb) {
            if ($line[$columnNb] == "") {
                $isLineCorrect = false;
                echo "Ligne " . $lineNb . " : le champ " . $this->csvColumns[$columnNb] . " est obligatoire.\n";
                fwrite($this->fp, "Ligne " . $lineNb . " : le champ "
                    . $this->csvColumns[$columnNb] . " est obligatoire.\n");
            }
        }
        return $isLineCorrect;
    }

    /* EDB38_11 */

    /**
     * Réalise l'import des données
     *
     * @param array $ceeData Tableau des données provenant du fichier
     *
     * @throws Exception
     */
    public function importCeeData($ceeData)
    {
        try {
            $em = $this->getEntityManager();
            $em->beginTransaction();
            $this->insertData($ceeData);
            $em->commit();
            echo "l'import est terminé\n";
            fwrite($this->fp, "---- l'import est terminé\n");
        } catch (Exception $exception) {
            echo $exception;
            throw $exception;
        }
    }

    /**
     * @return EntityManagerInterface
     * @throws Exception
     */
    private function getEntityManager()
    {
        try {
            return $this->entityManager;
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * Insère les nouvelles données
     *
     * @param array $ceeData Données des tables CEE
     * @throws Exception
     */
    private function insertData($ceeData)
    {
        // EDB38_ACTION_02
        // Enregistrement des données de l'obligé
        $em = $this->getEntityManager();


        try {
            foreach ($ceeData as $lineNb => $line) {

                if ($lineNb > 0) {
                    $em->beginTransaction();
                    echo "Ligne " . $lineNb . " debut insertion  \n";
                    fwrite($this->fp, "Ligne " . $lineNb . " debut insertion  \n");
                    $oblige = $this->insertOblige($line);
                    echo "debut insertion OBLIGE  \n";
                    fwrite($this->fp, "Fin insertion OBLIGE  \n");
                    echo "debut insertion DISPOSITIF  \n";
                    fwrite($this->fp, "Debut insertion DISPOSITIF  \n");
                    $dispositif = $this->insertDispositif($line, $oblige);
                    echo "Fin insertion DISPOSITIF  \n";
                    fwrite($this->fp, "Fin insertion DISPOSITIF  \n");
                    echo "Début insertion DISPOSITIF REGLE  \n";
                    fwrite($this->fp, "Début insertion  DISPOSITIF REGLE  \n");
                    $this->insertDispositifRegle($line, $dispositif);
                    echo "Fin insertion  DISPOSITIF REGLE  \n";
                    fwrite($this->fp, "Fin insertion  DISPOSITIF REGLE  \n");
                    echo "Début insertion TRAVAUX  \n";
                    fwrite($this->fp, "Fin insertion TRAVAUX  \n");
                    $this->insertDispositifTravauxAndDispositifTravauxRegle($line, $dispositif);
                    echo "Début insertion TRAVAUX  \n";
                    fwrite($this->fp, "Fin insertion TRAVAUX  \n");
                    $em->commit();
                }
            }
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * @param array $line Ligne de donnée
     * @return Oblige
     * @throws Exception
     */
    private function insertOblige(array $line)
    {
        try {
            echo "debut insertion OBLIGE  \n";
            fwrite($this->fp, "debut insertion OBLIGE  \n");

            $oblige = new Oblige();
            $oblige->setNom($line[self::COLUMN_NOM_OBLIGE]);
            $oblige->setMail($line[self::COLUMN_MAIL_CONTACT]);
            $oblige->setSiren($line[self::COLUMN_SIREN]);
            $this->getObligeRepository()->saveOblige($oblige);
            return $this->getObligeRepository()->find($oblige->getId());
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return ObligeRepository
     * @throws Exception
     */
    private function getObligeRepository()
    {
        try {
            return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\Oblige');
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * @param array $line
     * @param Oblige $oblige
     * @return Dispositif
     * @throws OptimisticLockException
     */
    private function insertDispositif(array $line, Oblige $oblige)
    {
        try {
            $dispositif = new Dispositif();
            $dispositif->setIdentifiantOffreImport($line[self::COLUMN_ID_OFFRE]);
            $dispositif->setIntitule($line[self::COLUMN_INTITULE_OFFRE]);
            $dispositif->setIntituleConseiller($line[self::COLUMN_INTITULE_OFFRE]);
            $dispositif->setSiteWeb($line[self::COLUMN_SITE_WEB]);
            $dispositif->setIsOffre($line[self::COLUMN_IS_OFFRE]);
            $debutValidite = explode("-", $line[self::COLUMN_DATE_DEBUT_OFFRE])[0] . "-"
                . explode("-", $line[self::COLUMN_DATE_DEBUT_OFFRE])[1] . "-"
                . explode("-", $line[self::COLUMN_DATE_DEBUT_OFFRE])[2];
            $date = new \DateTime($debutValidite);
            $dispositif->setDebutValidite($date);
            $descriptif = $line[self::COLUMN_DETAIL_OFFRE] . "\n" .
                $line[self::COLUMN_CONDITIONS_DE_SOUSCRIPTION];
            $dispositif->setDescriptif($descriptif);
//            $dispositif->setLogo($line[self::COLUMN_LOGO]);
            $etat = $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\ValeurListe')
                ->findOneBy(['code' => ValeurListeConstante::ETAT_DISPOSITIF_ACTIVE]);
            $dispositif->setEtat($etat);
            $dispositif->setCodeTypeDispositif(ValeurListeConstante::TYPE_DISPOSITIF_COUP_DE_POUCE);
            $dispositif->setFinanceur($oblige->getNom());
            $typeDispositif = $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\ValeurListe')
                ->findOneBy(['code' => ValeurListeConstante::TYPE_DISPOSITIF_COUP_DE_POUCE]);
            $dispositif->setTypeDispositif($typeDispositif);
            $dispositif->setEvaluerRegle(true);
            $dispositif->setTypePerimetre('N');
            $dispositif->setExclutAnah(true);
            $dispositif->setExclutCEE(true);
            $dispositif->setExclutCommunal(false);
            $dispositif->setExclutDepartement(false);
            $dispositif->setExclutEPCI(false);
            $dispositif->setExclutNational(false);
            $dispositif->setExclutRegion(false);
            $dispositif->setExclutCoupDePouce(false);
            $dispositif->setOblige($oblige);

            $this->getDispositifRepository()->saveDispositif($dispositif);
            return $this->getDispositifRepository()->find($dispositif->getId());
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return DispositifRepository
     * @throws Exception
     */
    private function getDispositifRepository()
    {
        try {
            return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\Dispositif');
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * @param array $line
     * @param Dispositif $dispositif
     * @return DispositifTravaux
     * @throws Exception
     */
    private function insertDispositifTravaux($line, $codeTravaux, Dispositif $dispositif)
    {
        $dispositifTravaux = new DispositifTravaux();
        $dispositifTravaux->setDispositif($dispositif);
        $travaux = $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\Travaux')
            ->findOneBy(['code' => $codeTravaux]);

        if ($travaux) {
            $dispositifTravaux->setTravaux($travaux);
        } else {
            throw new Exception('Pas de travaux correspondant en BDD');
        }
        $dispositifTravaux->setEligibiliteSpecifique($line[self::COLUMN_COMPLEMENT]);
        $this->getDispositifTravauxRepository()->saveDispositifTravaux($dispositifTravaux);
        return $dispositifTravaux;
    }

    /**
     * @param array $line
     * @param Dispositif $dispositif
     * @return DispositifTravaux
     * @throws Exception
     */
    private function insertDispositifTravauxAndDispositifTravauxRegle(array $line, Dispositif $dispositif)
    {
        try {
            foreach (explode(self::CODE_DELIMITER, $line[self::COLUMN_TYPE_PROJETS_RENOVATION_CHAUFFAGE]) as $typeTravaux) {
                foreach ($this->codeFields[self::COLUMN_TYPE_PROJETS_RENOVATION_CHAUFFAGE][$typeTravaux] as $codeTravaux) {
                    $dispositifTravaux = $this->insertDispositifTravaux($line, $codeTravaux, $dispositif);
                    //on ne fait plus cette insertion
                    //$this->insertDispositifTravauxRegle($dispositifTravaux);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return DispositifTravauxRepository
     * @throws Exception
     */
    private function getDispositifTravauxRepository()
    {
        try {
            return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\DispositifTravaux');
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * @param array $line
     * @param Dispositif $dispositif
     * @throws Exception
     */
    private function insertDispositifRegle(array $line, Dispositif $dispositif)
    {
        try {
            /** Colonne Type chauffage actuel */
            $dispositifRegle = new DispositifRegle();
            $dispositifRegle->setDispositif($dispositif);
            $dispositifRegle->setType("E");
            $dispositifRegle->setConditionRegle(null);
            $expression = "";
            foreach (
                explode(
                    self::CODE_DELIMITER,
                    $line[self::COLUMN_TYPE_CHAUFFAGE_ACTUEL]
                ) as $index => $typeChauffage
            ) {
                if ($index != 0) {
                    $expression .= " || ";
                }
                $expression .= $this->codeFields[self::COLUMN_TYPE_CHAUFFAGE_ACTUEL][$typeChauffage];
            }
            $dispositifRegle->setExpression($expression);

            $this->getDispositifRegleRepository()->saveDispositifRegle($dispositifRegle);

            /** Colonne Type de ménage */
            $dispositifRegle = new DispositifRegle();
            $dispositifRegle->setDispositif($dispositif);
            $dispositifRegle->setType("E");
            $dispositifRegle->setConditionRegle(null);
            $expression = "";
            foreach (explode(self::CODE_DELIMITER, $line[self::COLUMN_TYPE_MENAGE]) as $index => $typeMenage) {
                if ($index != 0) {
                    $expression .= " || ";
                }
                $expression .= $this->codeFields[self::COLUMN_TYPE_MENAGE][$typeMenage];
            }
            $dispositifRegle->setExpression($expression);
            $this->getDispositifRegleRepository()->saveDispositifRegle($dispositifRegle);

            /** Montant minimum prime modeste */
            $dispositifRegle = new DispositifRegle();
            $dispositifRegle->setDispositif($dispositif);
            $dispositifRegle->setType("M");
            $dispositifRegle->setConditionRegle("\$F.tranche[ANAH] === \"MODESTE\"");
            $dispositifRegle->setExpression($line[self::COLUMN_MONTANT_MINIMUM_PRIME_MODESTE]);
            $this->getDispositifRegleRepository()->saveDispositifRegle($dispositifRegle);

            /** Montant minimum prime très modeste */
            $dispositifRegle = new DispositifRegle();
            $dispositifRegle->setDispositif($dispositif);
            $dispositifRegle->setType("M");
            $dispositifRegle->setConditionRegle("\$F.tranche[ANAH] === \"TRES_MODESTE\"");
            $dispositifRegle->setExpression($line[self::COLUMN_MONTANT_MINIMUM_PRIME_TRES_MODESTE]);
            $this->getDispositifRegleRepository()->saveDispositifRegle($dispositifRegle);

            /** Montant minimum prime non modeste */
            $dispositifRegle = new DispositifRegle();
            $dispositifRegle->setDispositif($dispositif);
            $dispositifRegle->setType("M");
            $dispositifRegle->setConditionRegle("\$F.tranche[ANAH] === \"HORS_TRANCHE\"");
            $dispositifRegle->setExpression($line[self::COLUMN_MONTANT_MINIMUM_PRIME_NON_MODESTE]);
            $this->getDispositifRegleRepository()->saveDispositifRegle($dispositifRegle);

            /** Pris de l'offre */
            if ($line[self::COLUMN_PRIX_PLAFOND_OFFRE]) {
                $dispositifRegle = new DispositifRegle();
                $dispositifRegle->setDispositif($dispositif);
                $dispositifRegle->setType("P");
                $dispositifRegle->setConditionRegle(null);
                $dispositifRegle->setExpression($line[self::COLUMN_PRIX_PLAFOND_OFFRE]);
                $this->getDispositifRegleRepository()->saveDispositifRegle($dispositifRegle);
            }

            /** type de chauffage */
            if ($line[self::COLUMN_TYPE_CHAUFFAGE_LOGEMENT] == "response_one" ||
                $line[self::COLUMN_TYPE_CHAUFFAGE_LOGEMENT] == "response_two") {
                $typeLogement = $line[self::COLUMN_TYPE_CHAUFFAGE_LOGEMENT] ;
                $dispositifRegle = new DispositifRegle();
                $dispositifRegle->setDispositif($dispositif);
                $dispositifRegle->setType("E");
                $dispositifRegle->setConditionRegle(null);
                $dispositifRegle->setExpression("\$L.mode_chauffage === \""
                                                .$this->codeFields[self::COLUMN_TYPE_CHAUFFAGE_LOGEMENT][$typeLogement]."\"");
                $this->getDispositifRegleRepository()->saveDispositifRegle($dispositifRegle);
            }

            /** Eligibilité age > 2 */
            $dispositifRegle = new DispositifRegle();
            $dispositifRegle->setDispositif($dispositif);
            $dispositifRegle->setType("E");
            $dispositifRegle->setConditionRegle(null);
            $dispositifRegle->setExpression("\$L.age >  2");
            $this->getDispositifRegleRepository()->saveDispositifRegle($dispositifRegle);

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return DispositifRegleRepository
     * @throws Exception
     */
    private function getDispositifRegleRepository()
    {
        try {
            return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\DispositifRegle');
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }

    /**
     * @param DispositifTravaux $dispositifTravaux
     * @return DispTravauxRegle
     * @throws Exception
     */
    private function insertDispositifTravauxRegle(DispositifTravaux $dispositifTravaux)
    {
        try {
            $dispositifTravauxRegle = new DispTravauxRegle();
            $dispositifTravauxRegle->setDispTravaux($dispositifTravaux);
            $dispositifTravauxRegle->setType("E");
            $dispositifTravauxRegle->setConditionRegle(null);
            $dispositifTravauxRegle->setExpression("(\$L.age) >  2");


            $this->getDispositifTravauxRegleRepository()->saveDispositifTravauxRegle($dispositifTravauxRegle);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return DispTravauxRegleRepository
     * @throws Exception
     */
    private function getDispositifTravauxRegleRepository()
    {
        try {
            return $this->getEntityManager()->getRepository('Simulaides\Domain\Entity\DispTravauxRegle');
        } catch (Exception $e) {
            echo $e;
            throw $e;
        }
    }
}
