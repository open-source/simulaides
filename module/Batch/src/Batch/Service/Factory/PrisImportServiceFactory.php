<?php

namespace Batch\Service\Factory;

use Batch\Exception\PrisException;
use Batch\Service\PrisImportService;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManager;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\Log\Logger;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Classe PrisImportServiceFactory
 *
 * Projet : Simul'Aides
 
 *
 * @author
 * @package   Batch\Service\Factory
 */
class PrisImportServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return PrisImportService
     * @throws PrisException
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entitySimulaidesManager */
        /** @var EntityManager $entityPrisManager */
        $entitySimulaidesManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $entityPrisManager       = $serviceLocator->get('doctrine.entitymanager.pris');

        return new PrisImportService($entitySimulaidesManager, $entityPrisManager);
    }
}
