<?php

namespace Batch\Service\Factory;

use Batch\Service\CeeImportService;
use Doctrine\ORM\EntityManager;
use Simulaides\Logs\Factory\LoggerFactory;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CeeImportServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return CeeImportService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var EntityManager $entityManager */
        /** @var EntityManager $entityCeeManager */
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $loggerService = $serviceLocator->get(LoggerFactory::SERVICE_NAME);

        return new CeeImportService($entityManager, $loggerService);
    }
}
