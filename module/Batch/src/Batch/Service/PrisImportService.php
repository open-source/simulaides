<?php

namespace Batch\Service;

use Batch\Exception\PrisException;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Simulaides\Constante\PrisConstante;

/**
 * Classe PrisImportService
 *
 * Projet : Simul'Aides

 *
 * @author
 * @package   Batch\Service
 */
class PrisImportService
{
    /** Nom du service */
    const SERVICE_NAME = 'PrisImportService';

    /**
     * @var array Nom des tables à récupérer
     */
    private $tabTableFromPRIS = [
        'field_data_nom',
        'field_data_prenom',
        'field_data_region',
        'field_data_tb_pts_contact_id',
        'field_data_telephone',
        'tb_pts_contact',
        'users_roles',
        'role',
        'users'
    ];

    /**
     * Constructeur du service
     *
     * @param EntityManager $entitySimulaidesManager
     * @param EntityManager $entityPrisManager
     */
    public function __construct(EntityManager $entitySimulaidesManager, $entityPrisManager)
    {
        $this->entitySimulaidesManager = $entitySimulaidesManager;
        $this->entityPrisManager       = $entityPrisManager;
    }

    /**
     * Réalise l'import des données des tables PRIS dans la base
     *
     * @param array $tabPrisData Tableau des données provenant de la base PRIS
     *
     * @throws \Exception
     */
    public function importDonneesPRIS($tabPrisData)
    {
        $connectionSimulaides = $this->entitySimulaidesManager->getConnection();

        try {
            $connectionSimulaides->beginTransaction();

            // Purge des données courantes
            $this->deleteData($connectionSimulaides);

            // Insertion des données de PRIS
            $this->insertData($connectionSimulaides, $tabPrisData);

            $connectionSimulaides->commit();
        } catch (\Exception $exception) {
            $connectionSimulaides->rollback();
            throw $exception;
        }
    }

    /**
     * Récupère les données dans la base PRIS
     *
     * @return array Tableau dont les clés sont les noms des tables et les valeurs les données correspondantes
     *
     * @throws PrisException
     */
    public function getDonneesPris()
    {
        try {
            $connectionPRIS = $this->entityPrisManager->getConnection();
            $tabresult      = [];

            foreach ($this->tabTableFromPRIS as $tableName) {
                $sql       = 'SELECT * FROM ' . $tableName;
                $statement = $connectionPRIS->prepare($sql);
                $statement->execute();
                $tabresult[$tableName] = $statement->fetchAll();
            }
        } catch (\Exception $exception) {
            throw new PrisException("Un problème est survenu au cours de la tentative de récupération des données de la base PRIS : " . $exception->getMessage(), 0, $exception);
        }

        return $tabresult;
    }

    /**
     * Suppression des données courantes
     *
     * @param Connection $connectionSimulaides
     */
    private function deleteData(Connection $connectionSimulaides)
    {
        foreach ($this->tabTableFromPRIS as $nomTable) {
            /*if ($nomTable == 'users') {
                $sql = 'DELETE u FROM users u '
                     . 'INNER JOIN users_roles ur ON u.uid = ur.uid '
                     . 'INNER JOIN role r ON ur.rid = r.rid '
                     . 'WHERE NOT r.name = \'' . PrisConstante::ROLE_OBLIGE . '\';';
            } else if ($nomTable == 'users_roles') {
                $sql = 'DELETE ur FROM users_roles ur '
                     . 'INNER JOIN role r on ur.rid = r.rid '
                     . 'WHERE NOT r.name = \'' . PrisConstante::ROLE_OBLIGE . '\';';
            } else if ($nomTable == 'role') {
                $sql = 'DELETE FROM role WHERE NOT role.name = \'' . PrisConstante::ROLE_OBLIGE . '\';';
            } else {
                if($nomTable=='tb_pts_contact'){
                    $nomTable = 'tb_pts_contact_pris';
                }
                $sql = 'DELETE FROM ' . $nomTable;
            }*/
            if($nomTable=='tb_pts_contact'){
                $nomTable = 'tb_pts_contact_pris';
            }
            $sql = 'DELETE FROM ' . $nomTable;
            $statement = $connectionSimulaides->prepare($sql);
            $statement->execute();
        }
    }

    /**
     * Insère les nouvelles données provenant de PRIS
     *
     * @param Connection $connectionSimulaides Connection à la base
     * @param array      $tabPrisData          Données des tables PRIS
     */
    private function insertData(Connection $connectionSimulaides, $tabPrisData)
    {
        // Le tableau est inversé pour respecter les contraintes de base
        foreach (array_reverse($this->tabTableFromPRIS) as $nomTable) {
            if($nomTable == 'tb_pts_contact'){
                $nomTableSimul = 'tb_pts_contact_pris';
            }else{
                $nomTableSimul = $nomTable;
            }
            // Construction de la requête avec les identifiants uniques des valeurs
            $values = [];
            for ($i = 0; $i < count($tabPrisData[$nomTable][0]); $i++) {
                $values[] = ':' . $nomTable . $i;
            }
            $sql = 'INSERT INTO ' . $nomTableSimul . ' VALUES (' . implode(', ', $values) . ');';


            // Liaison de chaque valeur avec la requête et préparation de la requête
            foreach ($tabPrisData[$nomTable] as $tabValues) {
                $statement = $connectionSimulaides->prepare($sql);
                $i         = 0;
                foreach ($tabValues as $value) {
                    $statement->bindValue($nomTable . $i, $value);
                    $i++;
                }
                $statement->execute();
            }
        }
    }
}
