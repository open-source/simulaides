<?php
return [
    'factories' => [
        'PrisImportService' => 'Batch\Service\Factory\PrisImportServiceFactory',
        'CeeImportService' => 'Batch\Service\Factory\CeeImportServiceFactory'
    ]
];
