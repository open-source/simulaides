<?php

return [
    'controllers'     => include 'controllers.config.php',
    'service_manager' => include 'services.config.php',
    'router'          => include 'routes.config.php',
    'console'         => include 'console.config.php',
    'view_helpers'    => array(
        'invokables' => array()
    ),
];
