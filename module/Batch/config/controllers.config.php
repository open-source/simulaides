<?php
return [
    'invokables' => [
        'Batch\Controller\PurgeSimulation'   => 'Batch\Controller\PurgeSimulationController',
        'Batch\Controller\ImportDonneesPris' => 'Batch\Controller\ImportDonneesPrisController',
        'Batch\Controller\ImportDonneesCee' => 'Batch\Controller\ImportDonneesCeeController',
    ],
];
