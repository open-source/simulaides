<?php
return [
    'routes' => [
        'import-donnees-pris' => [
            'type'    => 'Zend\Mvc\Router\Http\Literal',
            'options' => [
                'route'    => '/import-donnees-pris',
                'defaults' => [
                    'controller' => 'Batch\Controller\ImportDonneesPris',
                    'action'     => 'index',
                ],
            ],
        ],
        'import-donnees-cee' => [
            'type'    => 'Zend\Mvc\Router\Http\Literal',
            'options' => [
                'route'    => '/import-donnees-cee',
                'defaults' => [
                    'controller' => 'Batch\Controller\ImportDonneesCee',
                    'action'     => 'index',
                ],
            ],
        ],
        'purge-simulation'    => [
            'type'    => 'Zend\Mvc\Router\Http\Literal',
            'options' => [
                'route'    => '/purge-simulation',
                'defaults' => [
                    'controller' => 'Batch\Controller\PurgeSimulation',
                    'action'     => 'index',
                ],
            ],
        ],
        'ajout-codes-postaux'  => [
            'type'    => 'Zend\Mvc\Router\Http\Literal',
            'options' => [
                'route'    => '/ajout-codes-postaux',
                'defaults' => [
                    'controller' => 'Batch\Controller\ImportDonneesPris',
                    'action'     => 'temp',
                ],
            ],
        ]

    ]
];
