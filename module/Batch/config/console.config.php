<?php
return [
    'router' => [
        'routes' => [
            'purge-simulation' => [
                'options' => [
                    'route' => 'purge-simulation',
                    'defaults' => [
                        'controller' => 'Batch\Controller\PurgeSimulation',
                        'action'     => 'index',
                    ]
                ]
            ],
            'import-donnees-pris' => [
                'options' => [
                    'route' => 'import-donnees-pris',
                    'defaults' => [
                        'controller' => 'Batch\Controller\ImportDonneesPris',
                        'action'     => 'index',
                    ]
                ]
            ],
            'import-donnees-cee' => [
                'options' => [
                    'route' => 'import-donnees-cee',
                    'defaults' => [
                        'controller' => 'Batch\Controller\ImportDonneesCee',
                        'action'     => 'index',
                    ]
                ]
            ],
            'ajout-codes-postaux'  => [
                'options' => [
                    'route'    => 'ajout-codes-postaux',
                    'defaults' => [
                        'controller' => 'Batch\Controller\ImportDonneesPris',
                        'action'     => 'temp',
                    ],
                ],
            ]
        ]
    ]
];
